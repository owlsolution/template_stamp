<?php
//重要なｲﾝｸﾙｰﾄ
require_once('./../../config.php');
require_once('./../ad_config.php');

// スマホでなければ非サポート表示
//if(!Util::is_smartphone ()) {
//	header('Location:./../sp/unsupport.php');
//	exit;
//}

if(!isset($_COOKIE['stamp_easy_expire']) ){ // クッキーがあればその値がカウント値
	$message = 'ログインしていません。';
	$detail_message = 'アプリの店舗情報の画面からやり直してください。';
	
	require "./sp/page/"."logout.php";
	exit;
}

if($_GET['p'] == "out"){
	unset($_SESSION['man'][FILE_NAME]);
	
	// ログイン有効期間用のクッキー削除
	setcookie('stamp_easy_expire', '', time() - 3600);
	
	$message = 'ログアウトしました。';
	require "./sp/page/"."logout.php";
	exit;
}

if (isset($_GET['p'])) {
	$_SESSION['man'][FILE_NAME]['p'] = $_GET['p'];
}

//if (isset($_GET['rally_id'])) {
//    $_SESSION['man'][FILE_NAME]['rally_id'] = $_GET['rally_id'];
//}
//if (isset($_GET['id'])) {
//    $_SESSION['man'][FILE_NAME]['id'] = $_GET['id'];
//}
//if (isset($_GET['admin_id'])) {
//    $_SESSION['man'][FILE_NAME]['admin_id'] = $_GET['admin_id'];
//}
//if (isset($_GET['shop_admin_id'])) {
//    $_SESSION['man'][FILE_NAME]['shop_admin_id'] = $_GET['shop_admin_id'];
//}

if (isset($_SESSION['man'][FILE_NAME])) {
	$p = $_SESSION['man'][FILE_NAME]['p'];
	$rally_id = $_SESSION['man'][FILE_NAME]['rally_id'];
	$ident_id = $_SESSION['man'][FILE_NAME]['id'];
	$admin_id = $_SESSION['man'][FILE_NAME]['admin_id'];
	$shop_admin_id = $_SESSION['man'][FILE_NAME]['shop_admin_id'];
	
} else {
	header('Location:./../sp/?p=shop_webview_app_v5&rally_id='.$_GET['rally_id'].'&ident=1&app=1&id='.$_GET['id'].'&admin_id='.$_GET['admin_id']);
	echo 'ログインしていません。<br>';
	echo 'アプリの店舗情報の画面からやり直してください。<br>';
	exit;
}

// phpセッションに各種情報を保存

//// rally_idのバリデーションチェック
//if (empty($rally_id) || !is_numeric($rally_id)) {
//	// 数値以外が含まれている場合、処理しない
//	error_log("attack rally_id sql injection:".$rally_id);
//	// 店舗情報画面へリダイレクト
//	header('Location:./../unsupport.php');
//	exit;
//}
//
//if(empty($ident_id) || (strlen($ident_id) >= 128)) {
//	// idが異常値の場合処理しない
//	error_log("attack id sql injection:".$ident_id);
//	header('Location:./../unsupport.php');
//	exit;
//}

//$user_id = Util::get_authenticated_user_id();
//if (empty($user_id)) {
//	// ユーザIDが無い場合、以降の処理はしない
//	error_log("exit(); userid : empty");
//	exit();
//}

if(empty($p)){
	$p = "notice";
}
switch ($p) {
	/**********    管理メニュー   ***********/
	case "top";//TOPページ
		$controller = "TopController.php";
		break;
	case "notice";  // お知らせ配信
		$controller = "NoticeController.php";
		break;
	default;//ログイン画面
	break;
}
require './sys_controller/'.$controller;
?>
