<?php
//重要なｲﾝｸﾙｰﾄ
require_once('./../../../config.php');
require_once('./../../ad_config.php');

// お知らせ部品を読込み
require_once('./../common/component/NoticeComponent.php');
require_once('./../common/model/AdminModel.php');

if (isset($_SESSION['man'][FILE_NAME])) {
	$p = $_SESSION['man'][FILE_NAME]['p'];
	$rally_id = $_SESSION['man'][FILE_NAME]['rally_id'];
	$ident_id = $_SESSION['man'][FILE_NAME]['id'];
	$admin_id = $_SESSION['man'][FILE_NAME]['admin_id'];
	$shop_admin_id = $_SESSION['man'][FILE_NAME]['shop_admin_id'];
} else {
	exit;
}

// お知らせ部品
$noticeComp = new NoticeComponent();
$adminModel = new AdminModel();

/**
 * DBアクセス結果の情報を取得する
 * @param type $data_rows
 * @return array
 */
function get_dbdata_array($data_rows) {
    $data_list = array();  // 空配列宣言
	while ($row = mysql_fetch_array($data_rows, MYSQL_ASSOC)){
//		$dateParts = Util::get_rally_of_day($row['add_date2'], $row['rally_id']);
//		// add_dateを現時点での日付に変換
//		$row['add_date'] = $dateParts[0]."-".$dateParts[1]."-".$dateParts[2];
//		
//		$dateParts = Util::get_rally_of_day($row['last_stamp_date'], $row['rally_id']);
//		$row['last_stamp_date'] = $dateParts[0]."-".$dateParts[1]."-".$dateParts[2];
//		
		$data_list[] = $row;
	}
	return $data_list;
}

$rally_date = Util::rally_information_get(ADMIN_ID);
$rally_id = $rally_date['rally_id'];

// 必須検索条件
$where = " u.user_id = r.user_id AND r.admin_id IN(".ADMIN_ID.")";

$db = db_connect();
// ログインしているadmin_idからbranch_idを取得
$branch_id = $noticeComp->get_branch_id_by_related_admin_id($db, $admin_id);
db_close($db);
// 支店の場合、さらに絞り込み条件を付加
if (!empty($branch_id)) {
	$where .= " AND r.branch_id IN( ".$branch_id.")";
}



$is_user_list = $_POST['is_user_list'];
$selection = $_POST['selection'];
if(empty($selection)) {
	exit;
}
if($selection == "all"){
	/*
	 * 一斉配信人数取得
	 */
	$db = db_connect();
	if (empty($is_user_list)) {
			$array_all = user_count($db , $where);
	} else {
		$rally_user_date = all_user_information_select($db , $where);
		$array_all = mysql_num_rows($rally_user_date);
		// ラリーユーザデータを取得
		$user_list = get_dbdata_array($rally_user_date);
	}
	db_close( $db );
} else if($selection == "simple"){
	/*
	 * 簡単配信人数取得
	 */
	$array_all = 0;
	//******************************** 誕生日 ********************************//
	$birth_date_count = 0;
	$birth_date = "";
	$birth_date_where = "";
	if($_POST['simple_terms1'] == "checked"){
		$last_month = date('-m-', strtotime('-1 month'));
		$birth_date .= "birth_date LIKE  '%".$last_month."%'";
		$birth_date_count++;
	}
	if($_POST['simple_terms2'] == "checked"){
		$now_month = date('-m-');
		if($birth_date_count != 0){
			$birth_date .= " OR ";
		}
		$birth_date .= "birth_date LIKE  '%".$now_month."%'";
		$birth_date_count++;
	}
	if($_POST['simple_terms3'] == "checked"){
		$next_month = date('-m-' , strtotime('+1 month'));
		if($birth_date_count != 0){
			$birth_date .= " OR ";
		}
		$birth_date .= "birth_date LIKE  '%".$next_month."%'";
		$birth_date_count++;
	}
	if($birth_date_count != 0){
		$birth_date_where = " AND u.user_id IN (SELECT user_id FROM user WHERE ".$birth_date.")";
	}
	//******************************** 性別 ********************************//
	$sex_count = 0;
	$sex = "";
	$sex_where = "";
	if($_POST['simple_terms4'] == "checked"){  //男生
		$sex .= "1";
		$sex_count++;
	}
	if($_POST['simple_terms5'] == "checked"){  //女性
		$sex .= "2";
		$sex_count++;
	}
	if($sex_count == 2){  //どちらでもない
		$sex = "0,1,2";
	}
	if($sex_count != 0){
		$sex_where = " AND u.sex IN (".$sex.")";
	}
	//******************************** 最終来店日 ********************************//
	$last_stamp_date_count = 0;
	$last_stamp_date = "";
	$last_stamp_date_where = "";
	if($_POST['simple_terms6'] == "checked"){
		$yesterday = date('Y-m-d H:i:s', strtotime('-1 day')); // 昨日の日付
		$last_start_time = Util::get_rally_start_time_of_day($yesterday, $rally_id);
		$last_end_time = Util::get_rally_end_time_of_day($yesterday, $rally_id);
		$last_stamp_date .= "r.last_stamp_date BETWEEN '".$last_start_time."' AND '".$last_end_time."'";
		$last_stamp_date_count++;
	}
	if($_POST['simple_terms7'] == "checked"){
		$before_month = date('Y-m-d H:i:s', strtotime('-1 month')); // 1か月前
		// １ヶ月前の日よりも前の日が対象なので
		$before_month = Util::get_rally_start_time_of_day($before_month, $rally_id);
		if($last_stamp_date_count != 0){
			$last_stamp_date .= " OR ";
		}
		$last_stamp_date .= "r.last_stamp_date <= '".$before_month."'";
		$last_stamp_date_count++;
	}
	if($_POST['simple_terms8'] == "checked"){
		$parts_date = Util::get_rally_of_day(date('Y-m-d H:i:s'), $rally_id);
		$this_month = $parts_date[0]."-".$parts_date[1]."-01 00:00:00"; // 今月の１日の初めの時間
		if($last_stamp_date_count != 0){
			$last_stamp_date .= " OR ";
		}
		$last_stamp_date .= "r.last_stamp_date >= '".$this_month."'";
		$last_stamp_date_count++;
	}
	if($last_stamp_date_count != 0){
		$last_stamp_date_where = " AND ".$last_stamp_date."";
	}
	//******************************** お知らせ形式 ********************************//
	$notice_format_where = "";
	if(!empty($_POST['format_push'])){
		$notice_format_where = " AND r.classification = 1";
	}
	if(!empty($_POST['format_mail'])){
		$notice_format_where = " AND r.classification = 2";
	}
	if(!empty($_POST['format_push']) && !empty($_POST['format_mail'])){
		$notice_format_where = "";
	}
	//******************************** DBアクセス ********************************//
	if(empty($_POST['format_push']) && empty($_POST['format_mail'])){
		$array_all = 0;
	} else if(empty($birth_date_where) && empty($sex_where) && empty($last_stamp_date_where)){
		$array_all = 0;
	} else {
		$db = db_connect();
		
		$where .= " ".$birth_date_where.$sex_where.$last_stamp_date_where.$notice_format_where;
		if (empty($is_user_list)) {
			$array_all = user_count($db , $where);
		} else {
			$rally_user_date = all_user_information_select($db , $where);
			$array_all = mysql_num_rows($rally_user_date);
			// ラリーユーザデータを取得
			$user_list = get_dbdata_array($rally_user_date);
		}
		
		db_close( $db );
	}
} else if($selection == "individual"){
	/*
	 * 個別配信人数取得
	 */
	//******************************** 性別 ********************************//
	$individual_sex = $_POST['individual_terms1'];
	$sex_count = 0;
	$sex = "";
	$sex_where = "";
	if($individual_sex == "man"){
		$sex .= "1";
		$sex_count++;
	} else if($individual_sex == "woman"){
		if($sex_count != 0){
			$sex .= ",";
		}
		$sex .= "2";
		$sex_count++;
	}
	if($sex_count != 0){
		$sex_where = " AND u.sex IN (".$sex.")";
	}
	//******************************** 誕生日 ********************************//
	$individual_birthday_start = $_POST['individual_terms2'];
	$individual_birthday_end = $_POST['individual_terms3'];
	$birth_date = "";
	$birth_date_where = "";
	for($i=$individual_birthday_start;$i<=$individual_birthday_end;$i++){
		if($i != $individual_birthday_start){
			$birth_date .= " OR ";
		}
		$s = sprintf("%02d", $i);
		$birth_date .= "birth_date LIKE  '%-".$s."-%'";
	}
	if($individual_birthday_start != 1 || $individual_birthday_end !=12){
		$birth_date_where = " AND u.user_id IN (SELECT user_id FROM user WHERE ".$birth_date.")";
	}
	//******************************** 地域 ********************************//
	$individual_region = $_POST['individual_terms4'];
	$region_where = "";
	if(!empty($individual_region)){
		$region_where = " AND u.region = '".$individual_region."'";
	}
	//******************************** 最終来店日 ********************************//
	$last_start = $_POST['individual_terms7'];
	$last_start_time = Util::get_starttime_of_day($last_start, $rally_id);
	$last_end = $_POST['individual_terms8'];
	$last_end_time = Util::get_endtime_of_day($last_end, $rally_id);
	$last_stamp_date_where = "";
	if(!empty($last_start) && !empty($last_end)){
		$last_stamp_date_where = " AND r.last_stamp_date BETWEEN '".$last_start_time."' AND '".$last_end_time."' ";
	}
	$total_stamp_num = $_POST['individual_terms9'];
	$total_stamp_terms = $_POST['individual_terms10'];
	$total_stamp_num_where = "";
	if(!empty($total_stamp_num)){
		if($total_stamp_terms == "or_more"){
			$total_terms = ">=";
		} else if($total_stamp_terms == "downward"){
			$total_terms = "<=";
		}
		$total_stamp_num_where = " AND r.total_stamp_num ".$total_terms." '".$total_stamp_num."' ";
	}
	//******************************** 店舗一覧 ********************************//
	// $_POST['branch_num'] == 0   全支店対象
	// $_POST['branch_num'] == -1   支店登録をしていないユーザーを対象
	// 上記以外は、支店登録をしているユーザーを対象
	$select_branch_num = $_POST['branch_num'];
	error_log("select_branch_num result : ".$select_branch_num);
	$select_branch_num_where = "";
	if($select_branch_num == 0){
		$select_branch_num_where = "";
	} else if($select_branch_num == -1){
		$select_branch_num_where = " AND r.branch_id IS NULL AND user_name = '' ";
	} else {
		$select_branch_num_where = " AND r.branch_id = ".$select_branch_num." ";
	}
	//******************************** キーワード検索 ********************************//
	// 顧客名・カルテでキーワード検索
	$db = db_connect();
	$keyword = mysql_real_escape_string($_POST['search_keyword']);
	db_close( $db );
	$search_keyword_where = "";
	if (!empty($keyword)) {
		$search_keyword_where = " AND (u.user_name LIKE '%".$keyword."%' OR r.karte LIKE '%".$keyword."%') ";
	}
	//******************************** お知らせ形式 ********************************//
	$notice_format_where = "";
	if(!empty($_POST['format_push'])){
		$notice_format_where = " AND r.classification = 1";
	}
	if(!empty($_POST['format_mail'])){
		$notice_format_where = " AND r.classification = 2";
	}
	if(!empty($_POST['format_push']) && !empty($_POST['format_mail'])){
		$notice_format_where = "";
	}
	if(empty($_POST['format_push']) && empty($_POST['format_mail'])){
		$array_all = 0;
	} else {
		$db = db_connect();
		$where .= " ".$sex_where.$birth_date_where.$add_date_where.$region_where.$last_stamp_date_where.$total_stamp_num_where.$select_branch_num_where.$search_keyword_where.$notice_format_where;
		error_log("NoticeDbController.php sql result : ".$where);
		
		if (empty($is_user_list)) {
			// 人数の表示のみ
			$array_all = user_count($db , $where);
			error_log("array_all user_count : ".$array_all);
		} else {
			// 人数をクリックして、ユーザ情報の一覧が必要な場合
			$rally_user_date = all_user_information_select($db , $where);
			$array_all = mysql_num_rows($rally_user_date);

			// ラリーユーザデータを取得
			$user_list = get_dbdata_array($rally_user_date);
		}
		db_close( $db );
	}
}

$usersinfo = array(
    "user_num" => $array_all, 
    "id" => USER_ID, 
    "data" =>$user_list);
print json_encode($usersinfo); // JSON出力

?>