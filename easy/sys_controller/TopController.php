<?php
/**
 * 支店機能対応のトップ画面表示コントローラー
 */
$url = "http://graph.facebook.com/".FACEBOOK_PAGE_ID."?sk=likes";
$html = file_get_contents( $url );
mb_language("Japanese");
$html = mb_convert_encoding($html, "UTF-8", "auto" );
$html = str_replace(array("\r\n","\r","\n"), '', $html);
$json = json_decode($html, true);
$number_people = $json['likes'];
if (empty($number_people)) {
    $number_people = 0;
}
$twitter_url = "https://mobile.twitter.com/".TWITTER_SCREEN_NAME."/followers";
$html = file_get_contents( $twitter_url );
mb_language("Japanese");
$html = mb_convert_encoding($html, "UTF-8", "auto" );
$html = str_replace(array("\r\n","\r","\n"), '', $html);
if ( preg_match_all( '/<span class="count">(.*?)<\/span>/s', $html, $matches1) ) {
	$str_number_people = $matches1[1];
	$twitter_number_people = $str_number_people[0];
}
$db = db_connect();
$new_day = date('Y-m-d');
$where = "display_date <= '".$new_day."' ORDER BY display_date DESC";
$management_notice_date = management_notice_select($db , $where);
while ($row = mysql_fetch_array($management_notice_date)){
	$management_notice_array[] = $row;
}

db_close( $db );
$db = db_connect();

// 支店機能 通常/オーナーはADMIN_ID ,支店権限の場合は支店のadmin_idで検索
$send_admin_id = ADMIN_ID;
if ($_SESSION["branchFlag"] == 2) {
    // 支店でログインしている場合
    $send_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
} else if ($_SESSION["branchFlag"] == 1) {
    // オーナーの場合
    if ($_SESSION["branchId"] != 0) {
        $send_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
    }
}
// 店員のadmin_idを追加
$rally_date = rally_select($db , "admin_id = ".ADMIN_ID);
$rally = mysql_fetch_array($rally_date);
$where = "rally_id = '".$rally['rally_id']."' AND status = '1'";
$staff_data = staff_select($db , $where);
while ($staff = mysql_fetch_array($staff_data)){
	$send_admin_id .= ",".$staff['admin_id'];
}

$where = "admin_id IN (".$send_admin_id.") ORDER BY notice_id DESC LIMIT 0 , 1";
$notice_date_array = notice_select($db , $where);
while ($notice = mysql_fetch_array($notice_date_array)){
	$notice_array[] = $notice;
}
db_close( $db );

if(Util::is_smartphone ()) {
	require './sp/page/top.php';
}