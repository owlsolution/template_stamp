<?php
/*
 * 画像アップ処理
 */

/**
 * 画像リサイズ
 * @param type $up_filename
 * @param type $im
 * @param type $width
 * @param type $height
 * @param type $resized_img_filename
 */
function resize_img_to_jpg($up_filename,$im,$width,$height,$resized_img_filename) {
	list($org_width,$org_height,$org_type,$org_attr)=getimagesize($up_filename);
	if (! $im) {
		imagedestroy($im);
	} else {
		$resized_img = imagecreatetruecolor($width, $height);
		imagecopyresampled($resized_img, $im, 0, 0, 0, 0, $width, $height, $org_width, $org_height);
		imagejpeg($resized_img, $resized_img_filename, 50);
		imagedestroy($im);
		imagedestroy($resized_img);
	}
}

/**
 * 画像リサイズ
 * @param type $im  リソースID
 * @param type $img_filename 保存先:フルパスのファイル名
 */
function save_img_jpg($im,$img_filename) {
	if (!$im) {
		imagedestroy($im);
	} else {
		imagejpeg($im, $img_filename, 100);
	}
}

/**
 * JPEGファイルの回転
 * @param type $filename
 * @return type
 */
function imagecreatefromjpegexif($filename) {
        $img = imagecreatefromjpeg($filename);
        $exif = exif_read_data($filename);
        if ($img && $exif && isset($exif['Orientation']))
        {
            $ort = $exif['Orientation'];

            if ($ort == 6 || $ort == 5)
                $img = imagerotate($img, 270, null);
            if ($ort == 3 || $ort == 4)
                $img = imagerotate($img, 180, null);
            if ($ort == 8 || $ort == 7)
                $img = imagerotate($img, 90, null);

            if ($ort == 5 || $ort == 4 || $ort == 7)
                imageflip($img, IMG_FLIP_HORIZONTAL);
        }
        return $img;
}

if (is_uploaded_file($_FILES["notice_image"]["tmp_name"])) {
        $userid = date('YmdHis').$_FILES["notice_image"]["name"];

	/*
	 * ガラケーの為の処理
	 */
	$img_store_dir_fp = "././../../notice_images_fp/";
	$resized_img_filename = $img_store_dir_fp . $userid; // 出力ファイル名設定
	$up_filename = $_FILES["notice_image"]["tmp_name"];
	$imgtype = exif_imagetype($up_filename);
	$size = getimagesize($up_filename);

	$width = 200;
	$height = 200;

	$ratio_orig = $size[0]/$size[1];//元画像の比率計算
	/*画像の比率を計算する*/
	if ($width/$height > $ratio_orig) {
		$width = $height*$ratio_orig;
	} else {
		$height = $width/$ratio_orig;
	}
        
	switch ($imgtype) {
		case '1' :
			$im = @imagecreatefromgif($up_filename);
			resize_img_to_jpg($up_filename,$im,$width,$height,$resized_img_filename);
			break;
		case '2' :
//			$im = @imagecreatefromjpeg($up_filename);
            $im = imagecreatefromjpegexif($up_filename);
			resize_img_to_jpg($up_filename,$im,$width,$height,$resized_img_filename);
			break;
		case '3' :
			$im = @imagecreatefrompng($up_filename);
			resize_img_to_jpg($up_filename,$im,$width,$height,$resized_img_filename);
			break;
		default :
			break;
	}
	/*
	 * お知らせ配信サムネイル画像
	 */
	$img_store_dir_sp_middle = "././../../notice_images_m/";
	$resized_img_filename_sp_m = $img_store_dir_sp_middle . $userid; // 出力ファイル名設定
	$up_filename_sp_m = $_FILES["notice_image"]["tmp_name"];
	$imgtype_sp_m = exif_imagetype($up_filename_sp_m);
	$size_sp_m = getimagesize($up_filename_sp_m);

	$width_sp_m = 560;
	$height_sp_m = 560;

	$ratio_orig_sp_m = $size[0]/$size[1];//元画像の比率計算
	/*画像の比率を計算する*/
	if ($width_sp_m/$height_sp_m > $ratio_orig_sp_m) {
		$width_sp_m = $height_sp_m*$ratio_orig_sp_m;
	} else {
		$height_sp_m = $width_sp_m/$ratio_orig_sp_m;
	}
	
	switch ($imgtype) {
		case '1' :
			$im_sp_m = @imagecreatefromgif($up_filename_sp_m);
			resize_img_to_jpg($up_filename_sp_m,$im_sp_m,$width_sp_m,$height_sp_m,$resized_img_filename_sp_m);
			break;
		case '2' :
//			$im = @imagecreatefromjpeg($up_filename);
            $im_sp_m = imagecreatefromjpegexif($up_filename_sp_m);
			resize_img_to_jpg($up_filename_sp_m,$im_sp_m,$width_sp_m,$height_sp_m,$resized_img_filename_sp_m);
			break;
		case '3' :
			$im_sp_m = @imagecreatefrompng($up_filename_sp_m);
			resize_img_to_jpg($up_filename_sp_m,$im_sp_m,$width_sp_m,$height_sp_m,$resized_img_filename_sp_m);
			break;
		default :
			break;
	}

        /*
	 * ガラケー以外の処理
	 */
        $img_store_dir_sp = "./../../notice_images/";
        switch ($imgtype) {
		case '2' :
                        // jpgの場合、方向を調整して保存する
                        $im = imagecreatefromjpegexif($up_filename);
                        save_img_jpg($im, $img_store_dir_sp.$userid);
                        break;
		default :
                        if (move_uploaded_file($_FILES["notice_image"]["tmp_name"], $img_store_dir_sp.$userid)) {
                                chmod($img_store_dir_sp .$userid, 0644);
                        }
			break;
	}

        // レスポンスの設定
        $usersinfo = array(
            "filename" => $userid,  // ファイル名を返却
        );
        print json_encode($usersinfo); // JSON出力

}
?>