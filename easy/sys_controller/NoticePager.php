<?php
//重要なｲﾝｸﾙｰﾄ
require_once('./../../../config.php');
require_once('./../../ad_config.php');

// お知らせ部品を読込み
require_once('./../../common/component/NoticeComponent.php');
$noticeComp = new NoticeComponent();

require_once('./../../common/model/AdminModel.php');
$adminModel = new AdminModel();

$ident_id = $_SESSION['man'][FILE_NAME]['id'];
$admin_id = $_SESSION['man'][FILE_NAME]['admin_id'];
$pre_save = $_SESSION['man'][FILE_NAME]['pre_save'];

// １ページの表示数
$page_max = isset($_POST['page_max']) ? $_POST['page_max'] : '100';
// 表示開始
$start_index = isset($_POST['start']) ? $_POST['start'] : '1';
// サーバ上のindexは0オリジンなので-1する
$start_index = $start_index - 1;

// ページャ条件タイプ saveonly:仮投稿のみ表示
$pager_condition = isset($_POST['pager_condition']) ? $_POST['pager_condition'] : '';
if ($pager_condition == 'saveonly') {
	$show_saveonly = ($pager_condition == 'saveonly') ? '1' : '0';
}


$in_push_type = '';
// 表示する配信タイプを取得
$push_type = isset($_POST['push_type']) ? $_POST['push_type'] : '';
if (count($push_type) > 0) {
	if(in_array('0', $push_type)) {
		// タイプ：お知らせは0,3があるので3を追加する
		$push_type[] = '3';
	}
	$in_push_type = implode(',', $push_type);
}

$rally_date = Util::rally_information_get(ADMIN_ID);
$rally_id = $rally_date['rally_id'];

$db = db_connect();

// 全STAFFのadmin_idを取得
$where = "rally_id = '".$rally_id."' AND status = '1'";
$staff_data = staff_select($db , $where);
while ($staff = mysql_fetch_array($staff_data)){
	$staff_data_list[] = $staff;
}

$admin_type = $noticeComp->get_admin_type($db, $admin_id);
// 支店機能 通常/オーナーはADMIN_ID ,支店権限の場合は支店のadmin_idで検索
$admin_ids = $adminModel->get_all_admin_id_by_own_admin_id($db, $admin_id, $admin_type);
error_log("admin_type:".$admin_type.";");

if (count($admin_ids)) {
	$send_admin_id = implode(",", $admin_ids);
}
error_log("send_admin_id:".$send_admin_id.";");

//配信した支店名の表示
$branch_list = branch_select_by_owner($db, ADMIN_ID);

$where_push_type = "";
if (isset($in_push_type)) {
	$where_push_type = " AND notice_type IN(".$in_push_type.") ";
}
error_log("in_push_type:".$in_push_type.";");

$where_is_saveonly = "";
if ($pager_condition == 'saveonly') {
	// 仮投稿のみ
	$where_is_saveonly = " AND transition = '1' ";
}

// PUSH送信状況を取得
$push_status = $noticeComp->get_push_status($send_admin_id);

$notice_list = array();
$where = "admin_id IN( ".$send_admin_id.") ".$where_push_type.$where_is_saveonly." ORDER BY notice_id DESC ";
$notice_date = notice_select($db , $where);
$notice_total_num = mysql_num_rows($notice_date);
if (!$notice_total_num) {
	$notice_total_num = '0';
}
error_log("notice_total_num:".$notice_total_num);

$index = 0;
while ($notice = mysql_fetch_array($notice_date)){
	if ($index < $start_index){
//		echo "under:".$index."\n";
		$index++;
		continue;
	} else if ($index > ($start_index + $page_max)) {
//		echo "over:".$index."\n";
		$index++;
		break;
	}
	
	list($accept_day_and_hour, $accept_minute, $accept_second) = explode( ":", $notice['acceptance_datetime']);
	$add_date_date = $accept_day_and_hour.":".$accept_minute;
	$add_date_replace = str_replace("-", "/", $add_date_date);

	// ステータスチェック
	// 送信中/送信済/予約
	$status = "送信済";
	if ($notice['transition'] == '1') {
		$status = "仮投稿";
	} else {
		foreach ($push_status as $value) {
			if ($value['notice_id'] == $notice['notice_id']) {
				// 合致した場合ステータスを参照
				if (($value['status'] == '0') || ($value['status'] == '1')) {
					$status = "送信中";
				} else if ($value['status'] == '4') {
					$status = "予約";
				}
			}
		}
	}

	// 対象店舗名
	// 配信した支店名の表示
	$branch_name = "";
	if(!empty($branch_list)){
		// 支店機能なので支店admin_idと照合する
		foreach($branch_list as $branch){
			if($notice['admin_id'] == $branch['child_admin_id']){
				$branch_name = $branch['name'];
				break;
			}
		}
		error_log("branch_name:".$branch_name);
		if(empty($branch_name)) {
			// 支店のadmin_idと一致していないので
			// スタッフのadmin_idと照合する
			foreach($staff_data_list as $result_staff_data){
				if ($notice['admin_id'] == $result_staff_data['admin_id']){
					if($result_staff_data['branch_id'] == 0){
						// スタッフが本部に所属している
						break;
					}
					foreach($branch_list as $branch){
						if($branch['id'] == $result_staff_data['branch_id']){
							$branch_name = $branch['name'];
						}
					}
				}
			}
		}
	}
	if(empty($branch_name)) {
		$branch_name = "本部";
	}
	
	//配信タイプ（0 = お知らせ、1 = チラシ、2 = ブログ、 3 = 個人宛 4=スケジュール配信）を表示する
	if($notice['notice_type'] == 0 || $notice['notice_type'] == 3){
		$notice_type_image = "<img src='./../../sp_images/a03.png' width='15' height='15'>";
	} else if($notice['notice_type'] == 1){
		$notice_type_image = "<img src='./../../sp_images/a02.png' width='15' height='15'>";
	} else if($notice['notice_type'] == 2){
		$notice_type_image = "<img src='./../../sp_images/a01.png' width='15' height='15'>";
	} else if($notice['notice_type'] == 4){
		$notice_type_image = "<img src='./../../sp_images/a04.png' width='15' height='15'>";
	}
// 1レコード分
//	$notice_list[] = $index;
	
	
	if(Util::is_smartphone ()) {
		$notice_list[] = "
			<td align='center'>{$add_date_replace}</td>
			<td align='center'>{$notice_type_image}</td>
			<td>{$notice['notice_title']}</td>
			<td align='center'>{$status}</td>
			<td align='center'><p><a href='?p=notice&check_id={$notice['notice_id']}&pre_save={$pre_save}&show_saveonly={$show_saveonly}'>確認</a></p><p><a href='?p=notice&edit_id={$notice['notice_id']}&pre_save={$pre_save}&show_saveonly={$show_saveonly}'>編集</a></p><a href='?p=notice&dele_id={$notice['notice_id']}&pre_save={$pre_save}&show_saveonly={$show_saveonly}' onClick=\"return confirm('削除しますか?')\">削除</a></td>";
	} else {
		$read_stat = $notice['already_read_num']."/".$notice['app_delivery_num'];
		if ($notice['transition'] == '1') {
			$read_stat = "-";
		}
		$notice_list[] = "
			<td align='center'>{$add_date_replace}</td>
			<td align='center'>{$notice_type_image}</td>
			<td align='center'>{$branch_name}</td>
			<td>{$notice['notice_title']}</td>
			<td align='center'>{$notice['delivery_num']}/{$notice['total_user_count']}</td>
			<td align='center'>{$read_stat}</td>
			<td align='center'>{$status}</td>
			<td align='center'><a href='?p=notice&check_id={$notice['notice_id']}&pre_save={$pre_save}&show_saveonly={$show_saveonly}'>確認</a> ｜ <a href='?p=notice&edit_id={$notice['notice_id']}&pre_save={$pre_save}&show_saveonly={$show_saveonly}'>編集</a> ｜ <a href='?p=notice&dele_id={$notice['notice_id']}&pre_save={$pre_save}&show_saveonly={$show_saveonly}' onClick=\"return confirm('削除しますか?')\">削除</a></td>";
	}

	// ループカウントインクリメント
	$index++;
}
db_close($db);

$usersinfo = array(
	"notice_total_num" => $notice_total_num, 
	"data" =>$notice_list);
print json_encode($usersinfo); // JSON出力

?>