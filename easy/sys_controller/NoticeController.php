<?php

// お知らせ部品を読込み
require_once('./../common/component/NoticeComponent.php');
require_once('./../common/model/AdminModel.php');

/*
 * 支店機能向け お知らせ配信画面表示コントローラー
 */

// ----------------------------------------------------- //
// ブラウザから入力情報を取得
// ----------------------------------------------------- //
$stamp_select = isset($_POST['rally_id']) ? $_POST['rally_id'] : '';
$notice_title = isset($_POST['notice_title']) ? $_POST['notice_title'] : '';
$notice_date = isset($_POST['notice_date']) ? $_POST['notice_date'] : '';
$delivery_set = isset($_POST['delivery_set']) ? $_POST['delivery_set'] : '';
$delivery_num = isset($_POST['delivery_num']) ? $_POST['delivery_num'] : '';
$format = isset($_POST['format']) ? $_POST['format'] : '';
$simple_check = isset($_POST['simple_check']) ? $_POST['simple_check'] : '';
$individual_sex = isset($_POST['individual_sex']) ? $_POST['individual_sex'] : '';
$individual_birthday_start = isset($_POST['individual_birthday_start']) ? $_POST['individual_birthday_start'] : '';
$individual_birthday_end = isset($_POST['individual_birthday_end']) ? $_POST['individual_birthday_end'] : '';
$individual_region = isset($_POST['individual_region']) ? $_POST['individual_region'] : '';
$first_year_start = isset($_POST['first_year_start']) ? $_POST['first_year_start'] : '';
$first_month_start = isset($_POST['first_month_start']) ? $_POST['first_month_start'] : '';
$first_day_start = isset($_POST['first_day_start']) ? $_POST['first_day_start'] : '';
$first_start = isset($_POST['first_start']) ? $_POST['first_start'] : '';
$first_year_end = isset($_POST['first_year_end']) ? $_POST['first_year_end'] : '';
$first_month_end = isset($_POST['first_month_end']) ? $_POST['first_month_end'] : '';
$first_day_end = isset($_POST['first_day_end']) ? $_POST['first_day_end'] : '';
$first_end = isset($_POST['first_end']) ? $_POST['first_end'] : '';
$last_year_start = isset($_POST['last_year_start']) ? $_POST['last_year_start'] : '';
$last_month_start = isset($_POST['last_month_start']) ? $_POST['last_month_start'] : '';
$last_day_start = isset($_POST['last_day_start']) ? $_POST['last_day_start'] : '';
$last_start = isset($_POST['last_start']) ? $_POST['last_start'] : '';
$last_year_end = isset($_POST['last_year_end']) ? $_POST['last_year_end'] : '';
$last_month_end = isset($_POST['last_month_end']) ? $_POST['last_month_end'] : '';
$last_day_end = isset($_POST['last_day_end']) ? $_POST['last_day_end'] : '';
$last_end = isset($_POST['last_end']) ? $_POST['last_end'] : '';
$total_stamp_num = isset($_POST['total_stamp_num']) ? $_POST['total_stamp_num'] : '';
$total_stamp_terms = isset($_POST['total_stamp_terms']) ? $_POST['total_stamp_terms'] : '';

// スケジュール配信のパラメタ受取
$schedule_type = isset($_POST['schedule_type']) ? $_POST['schedule_type'] : '';
$new_user_time_difference = isset($_POST['new_user_time_difference']) ? $_POST['new_user_time_difference'] : '';
$new_user_day_difference = isset($_POST['new_user_day_difference']) ? $_POST['new_user_day_difference'] : '';
$last_stamp_after_day = isset($_POST['last_stamp_after_day']) ? $_POST['last_stamp_after_day'] : '';
$total_stamps = isset($_POST['total_stamps']) ? $_POST['total_stamps'] : '';
$total_stamp_after_day = isset($_POST['total_stamp_after_day']) ? $_POST['total_stamp_after_day'] : '';
$before_day_of_birth = isset($_POST['before_day_of_birth']) ? $_POST['before_day_of_birth'] : '';
$day_of_the_week = isset($_POST['day_of_the_week']) ? $_POST['day_of_the_week'] : '';
$monthly_push_day_of_the_month = isset($_POST['monthly_push_day_of_the_month']) ? $_POST['monthly_push_day_of_the_month'] : '';
$last_month_stamp_num = isset($_POST['last_month_stamp_num']) ? $_POST['last_month_stamp_num'] : '';
$last_month_push_day = isset($_POST['last_month_push_day']) ? $_POST['last_month_push_day'] : '';
// スケジュール配信時間
$push_hour = isset($_POST['push_hour']) ? $_POST['push_hour'] : '';
$push_minute = isset($_POST['push_minute']) ? $_POST['push_minute'] : '';
$schedule_status = isset($_POST['schedule_status']) ? $_POST['schedule_status'] : '';

$edit_schedule_id = isset($_GET['edit_schedule_id']) ? $_GET['edit_schedule_id'] : '';

//お知らせ配信対象外のuser_id
$exclusion_user_id = isset($_POST['exclusion_user_id']) ? $_POST['exclusion_user_id'] : '';
//支店選択
$branch_id = $_POST['branch_list'];

//キーワード検索
$db = db_connect();
$keyword = mysql_real_escape_string($_POST['search_keyword']);
db_close($db);

//チェックページの処理
$check_id = isset($_GET['check_id']) ? $_GET['check_id'] : '';

//編集ページの処理
$edit_id = isset($_GET['edit_id']) ? $_GET['edit_id'] : '';

// $_GET['to_user_id']がある場合、個人宛配信なので、専用表示する
$to_user_id = isset($_GET['to_user_id']) ? $_GET['to_user_id'] : '';

// 仮投稿フラグ
$pre_save = isset($_GET['pre_save']) ? $_GET['pre_save'] : '0';
$_SESSION['man'][FILE_NAME]['pre_save'] = $pre_save;

// 仮投稿の一覧のみを見る
$show_saveonly = isset($_GET['show_saveonly']) ? $_GET['show_saveonly'] : '0';
$_SESSION['man'][FILE_NAME]['show_saveonly'] = $show_saveonly;

// イメージファイル名(チラシの場合のみ使用)
$leaflets_file_name = isset($_POST['leaflets_file_name']) ? $_POST['leaflets_file_name'] : '';

// 送信Adminのadmin_id
$send_admin_id = isset($admin_id) ? $admin_id : '';

// オーナーが自分でないスタッフに切り替えた場合の送信スタップのadmin_id
$staff_admin_id = isset($_POST['staff_admin_id']) ? $_POST['staff_admin_id'] : '';

// スタッフのadmin_idが設定されている場合
if ($staff_admin_id != 0) {
	// 送信者をスタッフのadmin_idに変更する
	$send_admin_id = $staff_admin_id;
}

// ポストしたformがどれかを判断可能
// 'notice':お知らせ
// 'blog':ブログ
// 'leaflets':チラシ
// 'to_person':個人宛
// 'schedule':スケジュール
$delivery_type = isset($_POST['delivery_type']) ? $_POST['delivery_type'] : '';

// 時間指定関連の値の取得
// 時間指定有無
$reserve_check = isset($_POST['reserve_check']) ? $_POST['reserve_check'] : "0";

$reserve_year = isset($_POST['reserve_year']) ? $_POST['reserve_year'] : '';
$reserve_month = isset($_POST['reserve_month']) ? $_POST['reserve_month'] : '';
$reserve_day = isset($_POST['reserve_day']) ? $_POST['reserve_day'] : '';
$reserve_hour = isset($_POST['reserve_hour']) ? $_POST['reserve_hour'] : '';
$reserve_minute = isset($_POST['reserve_minute']) ? $_POST['reserve_minute'] : '';

// 仮投稿チェック有無
$pre_save_check = isset($_POST['pre_save_check']) ? $_POST['pre_save_check'] : "0";

// FB連動チェック有無
$sns_fb_check = isset($_POST['sns_fb_check']) ? $_POST['sns_fb_check'] : "0";
error_log("sns_fb_check:".$sns_fb_check);
// Twitter連動チェック有無
$sns_tw_check = isset($_POST['sns_tw_check']) ? $_POST['sns_tw_check'] : "0";
error_log("sns_tw_check:".$sns_tw_check);


// ----------------------------------------------------- //
// 処理開始
// ----------------------------------------------------- //
// お知らせ部品
$noticeComp = new NoticeComponent();
$adminModel = new AdminModel();

// お知らせ削除
$dele_id = isset($_GET['dele_id']) ? $_GET['dele_id'] : '';
if (!empty($dele_id)) {
	$noticeComp->delete_notice(NULL, $dele_id);
}

// お知らせスケジュール削除
$dele_schedule_id = isset($_GET['dele_schedule_id']) ? $_GET['dele_schedule_id'] : '';
if (!empty($dele_schedule_id)) {
	$noticeComp->delete_notice_schedule(NULL, $dele_schedule_id);
}

$db = db_connect();
// ログインしているアカウントのスコープ範囲内のすべての顧客人数取得
$user_list = $noticeComp->find_all_scope_user($db, $send_admin_id);
$all_customer = count($user_list);


// それぞれの支店や本部アカウントで管理画面にログインした時
// ブログ配信での店員選択で、それぞれの支店や本部に所属している店員のみ表示する。
$rally_id = $noticeComp->get_rally_id($db);
$staff_list = $noticeComp->get_all_staff_list($db, $rally_id, $send_admin_id);
$staff_name = "-";
foreach ($staff_list as $staff) {
	if ($staff['admin_id'] == $send_admin_id) {
		$staff_name = $staff['nickname'];
		break;
	}
}

error_log("staff_name:".$staff_name);

db_close($db);

// 編集処理の場合
if (isset($_POST['edit'])) {
	$noticeComp->edit_notice(NULL, $edit_id, $notice_title, $notice_date);
	// 最初に戻る
	header("Location: ./?p=notice&pre_save=" . $pre_save . "&show_saveonly=".$show_saveonly);
	exit;
}

/*
 * 履歴から確認ページへ遷移
 */
if (!empty($check_id) || !empty($edit_id)) {
	if (!empty($check_id)) {
		$id = $check_id;
	} else {
		$id = $edit_id;
	}
	$db = db_connect();
	$where = "notice_id =" . $id;
	$notice_select_date = notice_select($db, $where);
	$notice_date = mysql_fetch_array($notice_select_date);
	db_close($db);
	if ($notice_date['delivery_set'] == 1) {
		$delivery_set_name = "一斉配信";
		$delivery_set = "all";
	} else if ($notice_date['delivery_set'] == 2) {
		$delivery_set_name = "簡単配信";
		$delivery_set = "simple";
		if ($notice_date['simple_last_month'] == 1) {
			$simple_check[] = "last_month";
		}
		if ($notice_date['simple_this_month'] == 1) {
			$simple_check[] = "this_month";
		}
		if ($notice_date['simple_next_month'] == 1) {
			$simple_check[] = "next_month";
		}
		if ($notice_date['simple_sex'] == 1) {
			$simple_check[] = "man";
			$simple_check[] = "woman";
		}
		if ($notice_date['simple_sex'] == 2) {
			$simple_check[] = "man";
		}
		if ($notice_date['simple_sex'] == 3) {
			$simple_check[] = "woman";
		}
		if ($notice_date['yesterday_come'] == 1) {
			$simple_check[] = "yesterday_come";
		}
		if ($notice_date['month_come'] == 1) {
			$simple_check[] = "month_come";
		}
		if ($notice_date['this_month_come'] == 1) {
			$simple_check[] = "this_month_come";
		}
	} else if ($notice_date['delivery_set'] == 3) {
		$delivery_set_name = "個別配信";
		$delivery_set = "individual";
		switch ($notice_date['individual_sex']) {
			case 1; //全て
				$individual_sex_date = "全て";
				break;
			case 2; //男性
				$individual_sex_date = "男性";
				break;
			case 3;  //女性
				$individual_sex_date = "女性";
				break;
			default;
				break;
		}
		$individual_birthday_start = $notice_date['individual_birthday_start'];
		$individual_birthday_end = $notice_date['individual_birthday_end'];
		$individual_region = $notice_date['individual_region'];
		if (empty($individual_region)) {
			$individual_region = ALL_REGION_LABEL;
		}
		$first_start_array = explode("-", $notice_date['first_start']);
		$first_year_start = $first_start_array[0];
		$first_month_start = $first_start_array[1];
		$first_day_start = $first_start_array[2];
		$first_end_array = explode("-", $notice_date['first_end']);
		$first_year_end = $first_end_array[0];
		$first_month_end = $first_end_array[1];
		$first_day_end = $first_end_array[2];
		$last_start_array = explode("-", $notice_date['last_start']);
		$last_year_start = $last_start_array[0];
		$last_month_start = $last_start_array[1];
		$last_day_start = $last_start_array[2];
		$last_end_array = explode("-", $notice_date['last_end']);
		$last_year_end = $last_end_array[0];
		$last_month_end = $last_end_array[1];
		$last_day_end = $last_end_array[2];
		$total_stamp_num = $notice_date['total_stamp_num'];
		if ($notice_date['total_stamp_terms'] == "1") {
			$total_stamp_terms = "or_more";
		} else if ($notice_date['total_stamp_terms'] == "2") {
			$total_stamp_terms = "downward";
		}
	}

	if ($notice_date['notice_type'] == 1) {
		$delivery_set_name = "チラシ配信";  //個人宛配信
		$delivery_type = "leaflets";
		$leaflets_file_name = $notice_date['thumbnail'];
	} else if ($notice_date['notice_type'] == 2) {
		$staff_admin_id = $notice_date['admin_id'];
		$delivery_set_name = "ブログ配信";  //個人宛配信
		$delivery_type = "blog";
	} else if ($notice_date['notice_type'] == 3) {
		$delivery_set_name = "個人宛配信";  //個人宛配信
		$delivery_type = "to_person";
	} else if ($notice_date['notice_type'] == 4) {
		$delivery_set_name = "スケジュール配信";  //個人宛配信
		$delivery_type = "schedule";
	}

	//************************************ 配信形式 ************************************//
	if ($notice_date['format'] == 1) {
		$format = array("push", "mail");
	} else if ($notice_date['format'] == 2) {
		$format = array("push");
	} else if ($notice_date['format'] == 3) {
		$format = array("mail");
	}

	//************************************ 配信人数 ************************************//
	$delivery_num = $notice_date['delivery_num'];
	
	//************************************ 配信時点のユーザ総数 ************************************//
	$all_customer = $notice_date['total_user_count'];
	
	//************************************ 配信タイトル ************************************//
	$notice_title = $notice_date['notice_title'];

	//************************************ 配信時間指定 ************************************//
	if ($notice_date['acceptance_datetime'] == $notice_date['notice_data']) {
		// 予約指定なし
		$reserve_check = 0;
	} else {
		// 予約指定あり
		$reserve_check = 1;
		// datetime型から値を取り出し
		list($date, $time) = explode(" ", $notice_date['notice_data']);
		list($reserve_year, $reserve_month, $reserve_day) = explode("-", $date);
		list($reserve_hour, $reserve_minute, $second) = explode(":", $time);
	}

	//************************************ 配信状態 ************************************//
	if ($notice_date['transition'] != '1') {
		// 仮投稿なし
		$pre_save_check = 0;
	} else {
		// 仮投稿
		$pre_save_check = 1;
	}

	// SNS連携
	$sns_fb_check = $notice_date['sns_fb_check'];
	$sns_tw_check = $notice_date['sns_tw_check'];

	$db = db_connect();
	$send_admin = $notice_date['admin_id'];
	$admin_type = $noticeComp->get_admin_type($db, $send_admin);
	if ($admin_type == OWNER) {
		// オーナーの場合、branch_idなし
		$staff_name = 'オーナーアカウント';
	} else if ($admin_type == BRANCHES_OWNER) {
		$staff_name = '全店管理アカウント';
	} else if ($admin_type == BRANCH_MANAGER) {
		// 支店の店長の場合、支店のadminを設定
		// admin_idから支店名を取得
		$branch = $adminModel->get_branch_by_admin_id($db, $send_admin);
		$staff_name = $branch['name'].'管理アカウント';
	} else if ($admin_type == ORG_MANAGER) {
		$org = $adminModel->get_organization_by_admin_id($db, $send_admin);
		$staff_name = $org['name'].'組織管理アカウント';
	} else if ($admin_type == STAFF) {
		// スタッフが所属する支店を取得
		// admin_idからスタッフ名を取得する
		$staff = $adminModel->get_staff_by_admin_id($db, $send_admin);
		$staff_name = $staff['nickname'];
	}
	db_close($db);
	
	//************************************ 配信内容 ************************************//
	$notice_date = $notice_date['notice_content'];

	if (Util::is_smartphone()) {
		if (!empty($check_id)) {
			require "./sp/page/notice_check.php";
		} else {
			require "./sp/page/notice_edit.php";
		}
	} else {
		if (!empty($check_id)) {
			require "./pc/page/notice_check.php";
		} else {
			require "./pc/page/notice_edit.php";
		}
	}
	exit;
}

if (isset($_SESSION['asp']['notice']['transition']) && $_SESSION['asp']['notice']['transition'] == 'complete') {
	// セッション情報が入力されていて、送信状態が'complete'の場合
	// ユーザによる中断を無視する
	ignore_user_abort(true);
	// タイムアウトしないようにする
	set_time_limit(0);

	$send_admin = $_SESSION['asp']['notice']['data']['send_admin'];
	$db = db_connect();
	$branch_id = null;
	$admin_type = $noticeComp->get_admin_type($db, $send_admin);
	if (($admin_type == OWNER) || ($admin_type == BRANCHES_OWNER)) {
		// オーナーの場合、branch_idなし
	} else if ($admin_type == BRANCH_MANAGER) {
		// 支店の店長の場合、支店のadminを設定
		// adminから
		$branch_id = $adminModel->get_branch_id_by_admin_id($db, $send_admin);
	} else if ($admin_type == ORG_MANAGER) {
		$org = $adminModel->get_organization_by_admin_id($db, $send_admin);
		$branch_list = $adminModel->get_branch_by_organization_id($db, $org['id']);
		$branch_id_list = array_column($branch_list, 'id');
		$branch_id = implode(",", $branch_id_list);
	} else if ($admin_type == STAFF) {
		// スタッフが所属する支店を取得
		// 支店に所属している場合、
		$branch_id = $noticeComp->get_branch_id_by_related_admin_id($db, $send_admin);
	}
	db_close($db);
	
	$p_admin_id = ADMIN_ID;
	if (!empty($branch_id)) {
		// 支店IDが存在して、且つ、支店権限で実行する場合
		Util::notice_message_v2($p_admin_id, $_SESSION['asp']['notice']['data'], $branch_id);
	} else {
		Util::notice_message_v2($p_admin_id, $_SESSION['asp']['notice']['data'], NULL);
	}
	// 配信先を取得する
	// お知らせ情報取得
	$notice = $_SESSION['asp']['notice']['data'];
	$sns_type = $noticeComp->get_feed_sns_type($notice);
	error_log("sns_type:" . $sns_type);
	if ($sns_type == "-1") {
		unset($_SESSION['asp']['notice']);
		if (Util::is_smartphone()) {
			require "./sp/page/notice_end.php";
		} else {
			require "./pc/page/notice_end.php";
		}
	} else {
		// SNS連携処理を実行
		$_SESSION['asp']['notice']['transition'] = 'feed_sns';
		header('Location: ' . $_SERVER['REQUEST_URI']);
		exit;
	}
} else if (isset($_SESSION['asp']['notice']['transition']) && $_SESSION['asp']['notice']['transition'] == 'save') {
	// 保存(未送信状態)
	unset($_SESSION['asp']['notice']);
	if (Util::is_smartphone()) {
		require "./sp/page/notice_end.php";
	} else {
		require "./pc/page/notice_end.php";
	}
} else if (isset($_SESSION['asp']['notice']['transition']) && $_SESSION['asp']['notice']['transition'] == 'feed_sns') {
	error_log("call feed_sns:");
	// sns処理
	// 未ログインの場合、処理の途中でSNS側のログインへ飛んでcallbackで戻ってくるので、transitionを分けて、何度リダイレクトされても正常動作する作りにする
	// 
	// sns処理の最初にフラグを立てる
	if (!isset($_SESSION['asp']['notice']['sns'])) {
		// snsフィード処理の初期化
		SnsUtility::init_sns_feed();
		$_SESSION['asp']['notice']['sns'] = '1';
	}

	if (SnsUtility::is_facebook_feed_cancel_request()) {
		// Facebookキャンセルリクエストの場合、キャンセルフラグを立てる
		// 以降SnsUtility::send_message_sns()内でfacebook処理が回避される
		SnsUtility::facebook_feed_cancel();
		error_log("is_facebook_feed_cancel_request!!!!!!");
	}

	// お知らせ情報取得
	$notice = $_SESSION['asp']['notice']['data'];

	// 本文内の画像ファイル名を取得
	$img_file_names = $noticeComp->get_img_file_names($notice['notice_content']);

	$title = $notice['notice_title'];
	$body = $notice['notice_content'];

	// 配信先を取得する
	$sns_type = $noticeComp->get_feed_sns_type($notice);

	//
	$like_url = DOMAIN . FILE_NAME . "/public/notice/notice.php?notice_id=" . $notice['notice_id'];

	SnsUtility::send_message_sns($notice['send_admin'], $title, $body, $img_file_names[0], null, $sns_type, $like_url, null);

	$error_title = SnsUtility::get_error_title();
	$error_message = SnsUtility::get_error_message();
	if (!empty($error_message)) {
		$error_title .= "<br>" . $error_message;
	}

	unset($_SESSION['asp']['notice']);
	if (Util::is_smartphone()) {
		require "./sp/page/notice_end.php";
	} else {
		require "./pc/page/notice_end.php";
	}
} else if (isset($_POST['check'])) {
	// 入力フォームから確認画面へ遷移
	// ブログとチラシは配信対象が全てなので、送信数＝顧客数
	if (($delivery_type == 'blog') || ($delivery_type == 'leaflets')) {
		$delivery_num = $all_customer;
	} else if ($delivery_type == 'to_person') {
		$delivery_num = 1;
	}

	switch ($delivery_set) {
		case "all"; //一斉配信
			$delivery_set_name = "一斉配信";
			break;
		case "simple"; //簡単配信
			$delivery_set_name = "簡単配信";
			break;
		case "individual";  //個別配信
			$delivery_set_name = "個別配信";
			break;
		case "blog";  //個別配信
			$delivery_set_name = "ブログ配信";
			break;
		case "leaflets";  //個別配信
			$delivery_set_name = "チラシ配信";
			break;
		case "to_person";  //個人宛配信
			$delivery_set_name = "個人宛配信";
			break;
		case "schedule";  //個人宛配信
			$delivery_set_name = "スケジュール配信";
			break;
		default;
			break;
	}
	switch ($individual_sex) {
		case "all"; //全て
			$individual_sex_date = "全て";
			break;
		case "man"; //男性
			$individual_sex_date = "男性";
			break;
		case "woman";  //女性
			$individual_sex_date = "女性";
			break;
		default;
			break;
	}
	error_log("除外したuser_id : " . $exclusion_user_id);
	//店舗名の表示
	if ($branch_id == 0) {
		$branch_name = "指定なし";
	} else if ($branch_id == -1) {
		$branch_name = "店舗未登録";
	} else {
		$db = db_connect();
		//店舗名の取得
		$branch_name = $noticeComp->get_storename_by_branch_id($db, $branch_id);
		db_close($db);
	}
	
	$db = db_connect();
	// お知らせ送信するadminを設定し
	$send_admin = $send_admin_id;
	$admin_type = $noticeComp->get_admin_type($db, $send_admin);
	if ($admin_type == OWNER) {
		// オーナーの場合、branch_idなし
		$staff_name = 'オーナーアカウント';
	} else if ($admin_type == BRANCHES_OWNER) {
		$staff_name = '全店管理アカウント';
	} else if ($admin_type == BRANCH_MANAGER) {
		// 支店の店長の場合、支店のadminを設定
		// admin_idから支店名を取得
		$branch = $adminModel->get_branch_by_admin_id($db, $send_admin);
		$staff_name = $branch['name'].'管理アカウント';
	} else if ($admin_type == ORG_MANAGER) {
		$org = $adminModel->get_organization_by_admin_id($db, $send_admin);
		$staff_name = $org['name'].'組織管理アカウント';
	} else if ($admin_type == STAFF) {
		// スタッフが所属する支店を取得
		// admin_idからスタッフ名を取得する
		$staff = $adminModel->get_staff_by_admin_id($db, $send_admin);
		$staff_name = $staff['nickname'];
	}
	db_close($db);
	
	error_log("staff sendadmin:".$send_admin);
	error_log("staff admin_type:".$admin_type);
	
	$_SESSION['asp']['notice']['transition'] = 'check';
	if (Util::is_smartphone()) {
		require "./sp/page/notice_check.php";
	} else {
		require "./pc/page/notice_check.php";
	}
} else if (isset($_POST['setting'])) {


	// 確認画面にてお知らせ送信
	if (isset($_SESSION['asp']['notice']['transition']) && $_SESSION['asp']['notice']['transition'] != 'check') {
		header('Location: ' . $_SERVER['REQUEST_URI']);
		exit;
	}
	$now_day = date('Y-m-d H:i:s');  //配信日時

	switch ($delivery_set) {
		case "all"; //一斉配信
			$delivery_set_no = 1;
			break;
		case "simple"; //簡単配信
			$delivery_set_no = 2;
			break;
		case "individual";  //個別配信
			$delivery_set_no = 3;
			break;
		case "blog";  //ブログ配信
			// 条件なし 一斉
			$delivery_set_no = 1;
			break;
		case "leaflets";  //チラシ配信
			// 条件なし 一斉
			$delivery_set_no = 1;
			break;
		case "to_person";  //個別配信
			$delivery_set_no = 5;
			break;
		default;
			break;
	}

	// デフォは両方
	$format_no = 1;
	if (in_array("push", $format) && in_array("mail", $format)) {
		$format_no = 1;
	} else if (in_array("push", $format)) {
		$format_no = 2;
	} else if (in_array("mail", $format)) {
		$format_no = 3;
	}

	// 強制措置
	if ($delivery_type == "leaflets") {
		// チラシはメール配信しないので2に変更
		$format_no = 2;
	}

	$simple_last_month = 0;
	if (in_array("last_month", $simple_check)) {
		$simple_last_month = 1;
	}
	$simple_this_month = 0;
	if (in_array("this_month", $simple_check)) {
		$simple_this_month = 1;
	}
	$simple_next_month = 0;
	if (in_array("next_month", $simple_check)) {
		$simple_next_month = 1;
	}
	$simple_sex = 0;
	if (in_array("man", $simple_check) && in_array("woman", $simple_check)) {
		$simple_sex = 1;
	} else if (in_array("man", $simple_check)) {
		$simple_sex = 2;
	} else if (in_array("woman", $simple_check)) {
		$simple_sex = 3;
	}
	$yesterday_come = 0;
	if (in_array("yesterday_come", $simple_check)) {
		$yesterday_come = 1;
	}
	$month_come = 0;
	if (in_array("month_come", $simple_check)) {
		$month_come = 1;
	}
	$this_month_come = 0;
	if (in_array("this_month_come", $simple_check)) {
		$this_month_come = 1;
	}

	$individual_sex_no = 1;
	switch ($individual_sex) {
		case "all"; //全て
			$individual_sex_no = 1;
			break;
		case "man"; //男性
			$individual_sex_no = 2;
			break;
		case "woman";  //女性
			$individual_sex_no = 3;
			break;
		default;
			break;
	}
	if ($total_stamp_terms == "or_more") {
		$total_stamp_terms_no = 1;
	} else if ($total_stamp_terms == "downward") {
		$total_stamp_terms_no = 2;
	}

	$notice_type = 0;
	if ($delivery_type == "leaflets") {
		// チラシの場合 1
		$notice_type = 1;
	} else if ($delivery_type == "blog") {
		// BLOGの場合 2
		$notice_type = 2;
	} else if ($delivery_type == "to_person") {
		// 個人宛の場合 3
		$notice_type = 3;
	} else if ($delivery_type == "schedule") {
		// スケジュールの場合 4
		$notice_type = 4;
	}

	// お知らせ送信するadminを設定し
	$send_admin = $send_admin_id;
	
	// 予約指定がある場合、送信時間に予約時間を設定する
	$send_datetime = date('Y-m-d H:i:s', strtotime($now_day));  //配信日時
	if ($reserve_check == 1) {
		$send_datetime = $reserve_year . "-" . $reserve_month . "-" . $reserve_day . " " . $reserve_hour . ":" . sprintf("%02d", $reserve_minute) . ":00";
	}

	error_log("reserve_check:" . $reserve_check);
	error_log("send_datetime:" . $send_datetime);

	$db = db_connect();
	$set = "rally_id = '" . $rally_id . "', "
		. "notice_content = '" . mysql_real_escape_string($notice_date) . "', "
		. "admin_id = '" . $send_admin . "', "
		. "notice_data = '" . $send_datetime . "', "
		. "notice_title = '" . mysql_real_escape_string($notice_title) . "', "
		. "delivery_set = '" . $delivery_set_no . "', "
		. "format = '" . $format_no . "', "
		. "simple_last_month = '" . $simple_last_month . "', "
		. "simple_this_month = '" . $simple_this_month . "', "
		. "simple_next_month = '" . $simple_next_month . "', "
		. "simple_sex = '" . $simple_sex . "', "
		. "yesterday_come = '" . $yesterday_come . "', "
		. "month_come = '" . $month_come . "', "
		. "this_month_come = '" . $this_month_come . "', "
		. "individual_sex = '" . $individual_sex_no . "', "
		. "individual_birthday_start = '" . $individual_birthday_start . "', "
		. "individual_birthday_end = '" . $individual_birthday_end . "', "
		. "individual_region = '" . $individual_region . "', "
		. "first_start = '" . $first_start . "', "
		. "first_end = '" . $first_end . "', "
		. "last_start = '" . $last_start . "', "
		. "last_end = '" . $last_end . "', "
		. "total_stamp_num = '" . mysql_real_escape_string($total_stamp_num) . "', "
		. "total_stamp_terms = '" . $total_stamp_terms_no . "', "
		. "delivery_num = '" . $delivery_num . "', "
		. "notice_type = '" . $notice_type . "', "
		. "thumbnail = '" . $leaflets_file_name . "', "
		. "total_user_count = '" . $all_customer . "', "
		. "acceptance_datetime = '" . $now_day . "', "
		. "to_user_id = '" . $to_user_id . "', "
		. "condition_branch_id = '" . $branch_id . "', "
		. "keyword = '" . $keyword . "', "
		. "exclusion_user_id = '" . $exclusion_user_id . "', "
		. "sns_fb_check = '" . $sns_fb_check . "', "
		. "sns_tw_check = '" . $sns_tw_check . "', "
		. "transition = '" . $pre_save_check . "'";

	notice_set_insert($db, $set);
	$notice_id = mysql_insert_id();
	db_close($db);

	if ($pre_save_check == '1') {
		// 仮投稿なので配信処理はしない
		$noticeComp->pre_notice_save($notice_id);
		
		// 仮投稿時に親にPUSH通知する
		$db = db_connect();
		// 親のuser_idを取得する
		$user_id = $noticeComp->get_user_id_by_admin_id($db, $admin_id);
		db_close($db);
		if (!empty($user_id)) {
			$db = db_connect();
			$staff = $adminModel->get_staff_by_admin_id($db, $admin_id);
			db_close($db);
			$message = $staff['nickname']."さんが仮投稿しました。";
			$user_id_list[] = $user_id;
			Util::send_admin_pushes(ADMIN_ID, $message, $user_id_list);
		}
		
	} else {
		// リダイレクト前にセッションに配信内容を設定する
		$noticeComp->pre_notice($notice_id);
	}
	header('Location: ' . $_SERVER['REQUEST_URI']);
} else if (isset($_POST['sendonly'])) {

	// 保存しているものを再送する
	//編集ページの処理
	$notice_id = isset($_POST['notice_id']) ? $_POST['notice_id'] : '';

	// 予約の場合、
	$reserve_condition = '';
	$acceptance_datetime_condition = '';
	if ($reserve_check == 1) {
		$send_datetime = $reserve_year . "-" . $reserve_month . "-" . $reserve_day . " " . $reserve_hour . ":" . sprintf("%02d", $reserve_minute) . ":00";
		$reserve_condition = " notice_data = '" . $send_datetime . "', ";
	} else {
		// 予約がない場合は送信日時を設定する
		$send_datetime = date('Y-m-d H:i:s');  //配信日時
		$reserve_condition = " notice_data = '" . $send_datetime . "', ";
		$acceptance_datetime_condition = " acceptance_datetime = '" . $send_datetime . "', ";
	}

	// お知らせの仮保存ステータスを変更
	$db = db_connect();
	$where = "notice_id = '" . $notice_id . "'";

	// 予約時間とSNS連携は値が更新されている可能性があるので、上書きする
	$set = " transition = '0', "
		. $acceptance_datetime_condition
		. $reserve_condition
		. "sns_fb_check = '" . $sns_fb_check . "', "
		. "sns_tw_check = '" . $sns_tw_check . "'";
	notice_db_edit($db, $set, $where);
	db_close($db);

	// リダイレクト前にセッションに配信内容を設定する
	$noticeComp->pre_notice($notice_id);
	header('Location: ' . $_SERVER['REQUEST_URI']);
	
} else if ($show_saveonly == '1') {
	// 仮投稿の一覧を参照する
	require "./sp/page/notice_save_history.php";
} else {
	// 初期画面表示の場合
	// transitionの初期化
	$_SESSION['asp']['notice']['transition'] = 'input';

	$individual_sex = "all";
	require "./sp/page/notice_form.php";
}
