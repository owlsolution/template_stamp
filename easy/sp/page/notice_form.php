<?php
require './sp/header.php';
?>


<style type="text/css">
.ui-tabs{ border: none; padding: 0px; }
.ui-tabs .ui-widget .ui-widget-content .ui-corner-all{
   border: none; padding: 0px;
}
.ui-widget-header{ background: #ffffff; border: none; }
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active .ui-state-focus {
   border: 1px solid #0092D6; background: #0092D6;
}
.ui-tabs-panel .ui-widget-content .ui-corner-bottom{
   background: #ffffff;
}
.ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited{
   color: #ffffff !important;
}
.ui-state-default a, .ui-state-default a:link, .ui-state-default a:visited{
   color: #c0c0c0;
}
.ui-state-hover a, .ui-state-hover a:hover{
   color: #0092D6;
}

.comment_area {
   background: #ffffff !important;
}

.ui-tabs .ui-tabs-panel {
    padding: 0px !important;
}
</style>

<input type="hidden" id="selected_tab"  value="0">
<script type="text/javascript">
	
	function getUrlVars()
	{
		var vars = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		for(var i = 0; i < hashes.length; i++)
		{
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	}
	
    $(function(){
        // タブ生成
		var selected_index = $('#selected_tab').val();
		var tab_index = getUrlVars()["tab_index"];
		if (tab_index > 0) {
			// スケジュール設定削除の場合、表示するタブはスケジュール配信にする
			selected_index = tab_index;
		}
        $('#tabs').tabs({
//            active: $('#selected_tab').val(),
            active: selected_index,
        } );
        
        $( "#tabs" ).tabs({
            beforeActivate: function( event, ui ) {
                // インデックス番号は次のように取得可能
                $('#selected_tab').val(ui.newTab.index());
            }
        });

        $('#dialog_link, ul#icons li').hover(
            function() { $(this).addClass('ui-state-hover'); },
            function() { $(this).removeClass('ui-state-hover'); }
        );
        // タブ表示
        $('#tabs').css("display","block");

        if($('#selected_tab').val() == 0) {
            // お知らせタブの場合、どの配信タイプを取得
            var value = $("input[name='delivery_set']:checked").val();
            // 表示を調整
            checkradio(value);
        }

    });
</script>

<div class="box">
    <h2> <img src="images/icon1.gif" width="16" height="16"> 配信機能 </h2>
    <div id="tabs" style="display:none;">
        <ul>
            <?php if (empty($pre_save)) {  // スタッフと管理アカウントで表示を変更する?>
            <li><a href="#tabs-1">お知らせ</a></li>
            <li><a href="#tabs-2">ﾌﾞﾛｸﾞ</a></li>
            <?php } else { ?>
            <li><a href="#tabs-4">仮投稿</a></li>
            <?php } ?>
        </ul>
        
        <?php if (empty($pre_save)) {  // 通常と個人宛で表示を変更する?>
        <!-- -------- -->
        <!-- お知らせ  -->
        <!-- -------- -->
        <div id="tabs-1" class="comment_area">
            <form method="POST" action="./?p=notice&pre_save=<?php echo $pre_save;?>" name="form2" enctype="multipart/form-data">
                <div class="container">
                        <ul>
                                参加者へ各種お知らせ配信ができます。
                        </ul>
                        <div class="f_box">
						<div class="container1">
								<h3> お知らせ配信設定 </h3>
								<ul class="clearfix">
									<li>
										<input type="radio" name="delivery_set" value="all"  onclick="removeCheckBoxToUserListPopup();checkradio('all');" checked> 一斉配信
									</li>
									<li style="display: none;">
										<input type="radio" name="delivery_set" value="simple" onclick="removeCheckBoxToUserListPopup();checkradio('simple');"> 簡単配信
									</li>
									<li style="display: none;">
										<input type="radio" name="delivery_set" value="individual" onclick="addCheckBoxToUserListPopup();checkradio('individual');"> 個別配信
									</li>
								</ul>
							</div>

                                <!-- 個別配信 -->
                                <div class="container2" id="individual_menu"  style="display:none">
                                        <h3> 個別配信絞り込み </h3>
                                        <ul class="clearfix">
                                                <li class="clearfix">
                                                        <p class="titel">・性別 <span style="color:red;">※1</span></p>
                                                        <p class="text">
                                                                <input type="radio" name="individual_sex" value="all" <?php if($individual_sex == "all"){ echo "checked"; }?> onclick="individual_check()"> 全て　　　
                                                                <input type="radio" name="individual_sex" value="man" <?php if($individual_sex == "man"){ echo "checked"; }?> onclick="individual_check()"> 男性　　　
                                                                <input type="radio" name="individual_sex" value="woman" <?php if($individual_sex == "woman"){ echo "checked"; }?> onclick="individual_check()"> 女性
                                                        </p>
                                                </li>
                                                <li class="clearfix even">
                                                        <p class="titel">・誕生日 <span style="color:red;">※1</span></p>
                                                        <p class="text" >
                                                                <select name="individual_birthday_start" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=1;$i<=12;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                月 〜
                                                                <select name="individual_birthday_end" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=1;$i<=12;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>" <?php if($i == 12){ echo "selected";}?>><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                月
                                                        </p>
                                                </li>
                                                <li class="clearfix">
                                                        <p class="titel">・地域 <span style="color:red;">※1</span></p>
                                                        <p class="text">
														<select name="individual_region" onChange="individual_check()">
															<option value=""><?php echo ALL_REGION_LABEL;?></option>
															<?php
																$region_group_list = json_decode(REGION_LIST,true);
																foreach ($region_group_list as $group) {
																	// １つのグループを取り出す
																	if(!empty($group['gname'])) {
																		echo '<optgroup label="'.$group['gname'].'">';
																	}

																	// 地域アイテムのループ
																	$region_list = explode(",", $group['regions']);
																	foreach ( $region_list as $region_item) {
																		$elem = ($individual_region == $region_item) ? "selected" : "";
																		echo '<option value="'.$region_item.'" '.$elem.'>'.$region_item.'</option>';
																	}

																	if(!empty($group['gname'])) {
																		echo '</optgroup>';
																	}
																}
															?>
														</select>
                                                        </p>
                                                </li>
                                                <li class="clearfix even">
                                                        <p class="titel">・最終来店日</p>
                                                        <p class="text" align="center">
                                                                <select name="last_year_start" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=2013;$i<=date("Y");$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                年
                                                                <select name="last_month_start" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=1;$i<=12;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                月
                                                                <select name="last_day_start" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=1;$i<=31;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                日<br>
                                                                ↓
                                                                <br>
                                                                <select name="last_year_end" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=2013;$i<=date("Y");$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>" <?php if($i == date("Y")){ echo "selected";}?>><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                年
                                                                <select name="last_month_end" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=1;$i<=12;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>" <?php if($i == date("n")){ echo "selected";}?>><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                月
                                                                <select name="last_day_end" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=1;$i<=31;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>" <?php if($i == date("j")){ echo "selected";}?>><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                日
                                                        </p>
                                                </li>
                                                <li class="clearfix">
                                                        <p class="titel">・累計スタンプ数</p>
                                                        <p class="text">
                                                                <input type="text" name="total_stamp_num" id = "individual_total_stamp_num" size="10" onChange="individual_check()" value="0"> 個
                                                                <select name="total_stamp_terms" onChange="individual_check()">
                                                                        <option value="or_more">以上</option>
                                                                        <option value="downward">以下</option>
                                                                </select>
                                                        </p>
                                                </li>
												<li class="clearfix even">
													<p class="titel">・キーワード検索</p>
													<p class="text">
														<input type='text' id='search_keyword' name='search_keyword' size='26' onChange="individual_check()" value='<?php echo stripslashes($keyword); ?>'>
														<br>※[顧客名][カルテ情報]をキーワード検索します。
													</p>
												</li>
												<?php
												if ($_SESSION["branchId"] == 0) {
												?>
												<li class="clearfix">
													<p class="titel">・店舗一覧</p>
														<p class="text">
															<select name="branch_list" onChange="individual_check()">
																<?php
																//店舗一覧を取得
																$branch_list = $noticeComp->select_branch_list();
																foreach($branch_list as $branch){
																	error_log("branch_id result : ".$branch['branch_id']);
																?>
																	<option value="<?php echo $branch['branch_id'];?>"><?php echo $branch['branch_name'];?></option>
																<?php
																}
																?>
															</select>
														</p>
												</li>
												<?php
												}
												?>
                                        </ul>
                                        <p style="color:red; margin:10px;">※1 参加者がプロフィール登録している場合のみ反映されます。</p>
                                </div>
                                <!-- 簡単配信 -->
                                <div class="container1" id="simple_menu" style="display:none">
                                        <h3> お知らせしたい人を選んで下さい </h3>
                                        <ul class="clearfix">
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox1" value="last_month" onclick="simple_check()"> 先月誕生日の人 <span style="color:red;">※1</span>
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox2" value="this_month" onclick="simple_check()"> 今月誕生日の人 <span style="color:red;">※1</span>
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox3" value="next_month" onclick="simple_check()"> 来月誕生日の人 <span style="color:red;">※1</span>
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox4" value="man" onclick="simple_check()"> 男性 <span style="color:red;">※1</span>
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox5" value="woman" onclick="simple_check()"> 女性 <span style="color:red;">※1</span>
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox6" value="yesterday_come" onclick="simple_check()"> 昨日来店した人
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox7" value="month_come" onclick="simple_check()"> 1ヶ月以上来店してない人
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox8" value="this_month_come" onclick="simple_check()"> 今月来店した人
                                                </li>
                                        </ul>
                                        <p style="color:red; margin:10px;">※1 参加者がプロフィール登録している場合のみ反映されます。</p>
                                </div>
                                <div class="container1" id="format_menu" style="display:none">
                                        <h3> お知らせ形式 </h3>
                                        <ul class="clearfix">
                                                <li>
                                                        <input type="checkbox" name="format[]" id="format_push" onclick="format_check()" value="push" checked> プッシュ通知 <span style="color:red;">※2</span>
                                                </li>
                                                <li>
                                                        <input type="checkbox" name="format[]" id="format_mail" onclick="format_check()" value="mail" checked> メール送信 <span style="color:red;">※3</span>
                                                </li>
                                        </ul>
                                        <p style="color:red; margin:10px;">※2 アプリの方対象<br>※3 主にガラケーの方対象</p>
                                </div>
                                <?php 
                                    // 配信予約を指定するメニューを読み込み
                                    require './sp/page/notice_form_reserve.php';
                                ?>
                                <?php 
                                    // 仮投稿を指定するメニューを読み込み
                                    require './sp/page/notice_form_save.php';
                                ?>

                                <div class="f_digit">
                                        <div class="titel"> お知らせを送る人数 </div>
                                        <div class="digit"> <span id = "search_data" onclick="search_data('notice');"> <?php echo $all_customer;?> </span> <strong>/<?php echo $all_customer?></strong> <input type="hidden" name = "delivery_num" id = "delivery_num" value = "<?php echo $all_customer;?>" ></div>
                                </div>
                        </div>
                </div>
				<p style="color:red; margin:10px;">※絵文字は非対応です。配信が正常に行われない可能性がありますのでご注意ください。<br>顔文字は問題ありません。</p>
                <div class="box" style="">
                        <div class="f_box">
                                <div class="container1">
                                        <h3> タイトル (全角50文字以内) </h3>
                                        <ul class="clearfix">
                                                <li>
                                                        <input name="notice_title" type="text" placeholder="タイトルを入力してください。" value="" class="f_text1">
                                                </li>
                                        </ul>
                                </div>
                                <div class="container1">
                                        <h3> 本文 (全角1000文字以内) </h3>
                                        <ul class="clearfix">
                                                <li>
														<input style="margin-bottom:10px;" onclick="$('#notice_image_up1').click();" type="button" value="ファイルを選択">
														<input type="file" name="notice_image" id="notice_image_up1" style="margin-bottom:0px;font-size: 0px;display:none;" accept="image/*" multiple/><br>
                                                        <textarea class="f_text2" name="notice_date" id="notice_date1" placeholder="配信の内容をここに入力してください。"></textarea>
                                                </li>
                                        </ul>
                                        <div class="btn">
                                                <input name="check" type="submit" value="確 認">
                                        </div>
										<input type="hidden" name="exclusion_user_id" id="exclusion_user_id" value="">
                                        <input type="hidden" name = "delivery_type" value = "notice" >
                                </div>
                        </div>
                </div>
            </form>
        </div>
        <!-- -------- -->
        <!-- ブログ配信 -->
        <!-- -------- -->
        <div id="tabs-2" class="comment_area">
           <form method="POST" action="./?p=notice&pre_save=<?php echo $pre_save;?>" name="form2" enctype="multipart/form-data">
                <div class="container">
                        <ul>
                                参加者へ向けたブログ配信ができます。
                        </ul>
                        <div class="f_box">
							<div class="container1">
									<h3> ブログ配信者指定 </h3>
									<p class="titel">&nbsp;&nbsp;&nbsp;&nbsp;
											<select name="staff_admin_id">
												<option value="0">-</option>
												<?php foreach ($staff_list as $staff) { ?>
												  <option value='<?php echo $staff['admin_id'];?>'><?php echo $staff['nickname'];?></option>
												<?php } ?>
											</select>
							</div>
						</div>
                        <div class="f_box">
                                <?php 
                                    // 配信予約を指定するメニューを読み込み
                                    require './sp/page/notice_form_reserve.php';
                                ?>
                                <?php 
                                    // 仮投稿を指定するメニューを読み込み
                                    require './sp/page/notice_form_save.php';
                                ?>
                        </div>
				<p style="color:red; margin:10px;">※絵文字は非対応です。配信が正常に行われない可能性がありますのでご注意ください。<br>顔文字は問題ありません。</p>
                <div class="box" style="">
                        <div class="f_box">
                                <div class="container1">
                                        <h3> タイトル (全角50文字以内) </h3>
                                        <ul class="clearfix">
                                                <li>
                                                        <input name="notice_title" type="text" placeholder="ブログのタイトルを入力してください。" class="f_text1">
                                                </li>
                                        </ul>
                                </div>
                                <div class="container1">
                                        <h3> 本文 (全角1000文字以内) </h3>
                                        <ul class="clearfix">
                                                <li>
														<input style="margin-bottom:10px;" onclick="$('#notice_image_up2').click();" type="button" value="ファイルを選択">
														<input type="file" name="notice_image" id="notice_image_up2" style="margin-bottom:0px;font-size: 0px;display:none;"><br>
                                                        <textarea class="f_text2" name="notice_date" id="notice_date2" placeholder="配信の内容をここに入力してください。"></textarea>
                                                </li>
                                        </ul>
                                        <div class="btn">
                                                <input name="check" type="submit" value="確 認">
                                        </div>
                                        <input type="hidden" name = "delivery_type" value = "blog" >
                                        <input type="hidden" name="delivery_set" value="blog">
                                </div>
                        </div>
                </div>
            </div>
            </form>
        </div>

        <?php } else { ?>
        <div id="tabs-4" class="comment_area">
        <!-- -------- -->
        <!-- 仮投稿 -->
        <!-- -------- -->
        <form method="POST" action="./?p=notice&pre_save=<?php echo $pre_save;?>" name="form2" enctype="multipart/form-data">
                <div class="container">
                        <ul>
                                ブログ配信の仮投稿ができます。
                                <p style="color:red; margin:10px;">※仮投稿したブログは、管理者が承認することで配信されます。</p>
                        </ul>
                    <div class="f_box">
                                <?php 
                                    // 配信予約を指定するメニューを読み込み
                                    require './sp/page/notice_form_reserve.php';
                                ?>
                    </div>
					<input type="hidden" name="pre_save_check" class="pre_save_check" value="1">
					<p style="color:red; margin:10px;">※絵文字は非対応です。配信が正常に行われない可能性がありますのでご注意ください。<br>顔文字は問題ありません。</p>
                    <div class="box">
                            <div class="f_box">
                                    <div class="container1">
                                            <h3> タイトル (全角50文字以内) </h3>
                                            <ul class="clearfix">
                                                    <li>
                                                            <input name="notice_title" type="text" placeholder="配信のタイトルをここに入力してください。" value="" class="f_text1">
                                                    </li>
                                            </ul>
                                    </div>
                                    <div class="container1">
                                            <h3> 本文 (全角1000文字以内) </h3>
                                            <ul class="clearfix">
                                                    <li>
														<input style="margin-bottom:10px;" onclick="$('#notice_image_up4').click();" type="button" value="ファイルを選択">
														<input type="file" name="notice_image" id="notice_image_up4" style="margin-bottom:0px;font-size: 0px;display:none;"><br>
                                                            <textarea class="f_text2" name="notice_date" id="notice_date4" placeholder="配信の内容をここに入力してください。" ></textarea>
                                                    </li>
                                            </ul>
                                            <div class="btn">
                                                    <input name="check" type="submit" value="確 認">
                                            </div>
                                            <input type="hidden" name = "delivery_type" value = "blog" >
                                            <input type="hidden" name="delivery_set" value="blog">
                                            <input type="hidden" name="format[]" id="format_push" value="push" checked>
                                            <input type="hidden" name="format[]" id="format_mail" value="mail" checked>
                                    </div>
                            </div>
                    </div>
                </div>
            </form>
        <!-- -------- -->
        <!-- 仮投稿(終) -->
        <!-- -------- -->
        </div>
        <?php } ?>
    </div>

</div>

<?php
	require './sp/page/notice_history_pager.php';
?>

<div id="user_list">
        <div class="container">
                <table id="user_list_table" width="100%" border="0" cellpadding="5" cellspacing="0">
                        <tr id="user_list_header" align="center">
								<th width="10%">顧客ID</th>
								<th width="20%">顧客名</th>
								<th width="20%">新規登録日</th>
								<th width="20%">最終スタンプ日</th>
								<th width="10%">スタンプ数</th>
								<th width="10%">累計スタンプ数</th>
                        </tr>
                        <?php
                        $count_user_list = count($user_list);
                        for ($i = 0; $i < $count_user_list; $i++) {
                        ?>
                        <tr class="user_list_data_tr">
                                <td align="center"><?php echo USER_ID.sprintf("%05d", $user_list[$i]['user_id']); ?></td>
                                <td><?php echo empty($user_list[$i]['user_name']) ? "-" : $user_list[$i]['user_name']; ?></td>
								<?php $add_date = str_replace("-", "/", $user_list[$i]['add_date']);?>
								<?php $sign_up_date = explode(" ", $add_date) ;?>
								<td align="center"><?php echo $sign_up_date[0]; ?></td>
								<?php $last_stamp_date = str_replace("-", "/", $user_list[$i]['last_stamp_date']);?>
								<?php $result_last_stamp_date = explode(" ", $last_stamp_date) ;?>
								<td align="center"><?php echo $result_last_stamp_date[0]; ?></td>
                                <td align="center"><?php echo $user_list[$i]['stamp_num'];?></td>
                                <td align="center"><?php echo $user_list[$i]['total_stamp_num'];?></td>
                        </tr>
                        <?php
                        }
                        ?>
                </table>
        </div>
</div>

<script>
	var buttons_val = {};
	var button_name = ['閉じる','対象者更新'];
	var user_list_title = ['お知らせ配信対象一覧' , '詳細絞り込み'];
	var type_num = 0;
	function addCheckBoxToUserListPopup(){
		type_num = 1;
		changeButtonName();
	}
	function removeCheckBoxToUserListPopup(){
		type_num = 0;
		changeButtonName();
	}
	function changeButtonName(){
		buttons_val = {};
		console.log('type_num'+type_num);
		buttons_val[button_name[type_num]] = function(){
			if(type_num === 1){
				var exclusion_user_id = [];
				//var user_id = $('.user_id_checkbox').attr("id");
				var user_list_num = 0;
				$(".user_id_checkbox").each(function(i, elem) {
					if ($(elem).prop('checked')) {
						user_list_num++;
					} else {
						//console.log($(elem).attr("id")); 
						exclusion_user_id.push($(elem).attr("id"));
					}
				});
				$("#search_data").html(user_list_num);
				$("#delivery_num").val(user_list_num);
				console.log("exclusion_user_id"+exclusion_user_id);
				$('#exclusion_user_id').val(exclusion_user_id.join());
			}
			$(this).dialog('close');
			
			console.log($('#exclusion_user_id').val());
		}
		
		$('#user_list').dialog({
			autoOpen: false,
			height: 400,
			width: 600,
			title: user_list_title[type_num],
			closeOnEscape: false,
			modal: true,
			position: [0,0],
			buttons: buttons_val
		});
	}
	$('#user_list').dialog({
		autoOpen: false,
		height: 400,
		width: 880,
		title: 'お知らせ配信対象一覧',
		closeOnEscape: false,
		modal: true,
		position: [0,0],
		buttons: {
			"OK": function(){
			$(this).dialog('close');
			}
		}
	});
	
	$('#allCheck').click(function(){
		var items = $('.user_id_checkbox');
		if($(this).is(':checked')) { //全選択・全解除がcheckedだったら
			$(items).prop('checked', true); //アイテムを全部checkedにする
		} else { //全選択・全解除がcheckedじゃなかったら
			$(items).prop('checked', false); //アイテムを全部checkedはずす
		}
	});	
</script>

<?php
require './sp/footer.php';
?>