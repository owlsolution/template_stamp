<?php
require './sp/header.php';
?>
<div class="box">
	<form method="POST" action="./?p=notice" name="form2" enctype="multipart/form-data">
		<h2> <img src="images/icon1.gif" width="16" height="16"> お知らせ配信機能 </h2>
		<div class="container">
			<ul>
				配信を受付けました。
				<?php if (!empty($error_title)) : ?>
					<p style="color:red;"><?= $error_title;?></p>
				<?php endif;?>
		</div>
	</form>
	<ul class="clearfix" style="text-align:center;">
		<?php if(empty($pre_save) && $_GET['p'] != 'out' && empty($show_saveonly)):?>
			<li> <a href="<?php echo './?p=notice&pre_save=0&rally_id='.$rally_id.'&ident=1&app=1'.'&admin_id='.$admin['admin_id'].'&shop_admin_id='.$shop_admin_id; ?>">お知らせ（ブログ）配信トップに戻る</a> </li>
		<?php elseif($pre_save == '1' && empty($show_saveonly)):?>
			<li> <a href="<?php echo './?p=notice&pre_save=1&rally_id='.$rally_id.'&ident=1&app=1'.'&admin_id='.$admin['admin_id'].'&shop_admin_id='.$shop_admin_id; ?>">ブログの仮登録トップに戻る</a> </li>
		<?php elseif($show_saveonly == '1'):?>
			<li> <a href="<?php echo './?p=notice&show_saveonly=1&rally_id='.$rally_id.'&ident=1&app=1'.'&admin_id='.$admin['admin_id'].'&shop_admin_id='.$shop_admin_id; ?>">仮投稿管理トップに戻る</a> </li>
		<?php endif;?>
	</ul>
</div>
<?php
	// 履歴表示の読み込み
	require './sp/page/notice_history_pager.php';
?>

<?php
require './sp/footer.php';
?>