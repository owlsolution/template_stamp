<!-- 仮投稿チェック -->
<div class="container1" id="save_menu">
		<h3> 仮投稿 </h3>
		<p class="titel">&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="checkbox" name="pre_save_check" class="pre_save_check" value="1">
			チェックすると仮投稿になります<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 12px;">※仮投稿すると内容を保持した状態で保留されます。保留した記事を配信したい場合は配信履歴->確認から配信できます。
		</p>
</div>

<script>
$(function() {
    // 初期設定
    if ($('.pre_save_check').is(':checked')) {
        // 別タブの予約チェックボックスもON
        $('.pre_save_check').attr("checked", "checked");
	} else {
        // チェックを外されたらメニューを無効化
        $('.pre_save_check').removeAttr("checked");
	}

    // checkboxがチェック
    $('.pre_save_check').change(function(){
            if ($(this).is(':checked')) {
                // 別タブの予約チェックボックスもON
                $('.pre_save_check').attr("checked", "checked");
                
            } else {
                // 別タブの予約チェックボックスも OFF
                $('.pre_save_check').removeAttr("checked");
            }
    });
});
</script>
