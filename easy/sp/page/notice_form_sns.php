<div class="container1" id="sns_menu">
	<h3> SNS連動 </h3>
	<div class="sns_box">
	<p class="titel sns_text">&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="checkbox" name="sns_fb_check" class="sns_btn sns_fb_check" <?= empty($sns_fb_check) ? '': 'checked' ?> value="1" disabled="disabled">
		Facebookへ同時に投稿します
	</p>
	<p class="titel sns_text">&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="checkbox" name="sns_tw_check" class="sns_btn sns_tw_check" <?= empty($sns_tw_check) ? '': 'checked' ?> value="1" disabled="disabled">
		twitterへ同時に投稿します
	</p>
	</div>
</div>

<script>
	$(function () {
		
		// 初期設定
		if ($('.sns_fb_check').is(':checked')) {
			// 別タブの予約チェックボックスもON
			$('.sns_fb_check').attr("checked", "checked");
		} else {
			// チェックを外されたらメニューを無効化
			$('.sns_fb_check').removeAttr("checked");
		}
		// checkboxがチェック
		$('.sns_fb_check').change(function () {
			if ($(this).is(':checked')) {
				// 別タブの予約チェックボックスもON
				$('.sns_fb_check').attr("checked", "checked");
			} else {
				// 別タブの予約チェックボックスも OFF
				$('.sns_fb_check').removeAttr("checked");
			}
		});

		// 初期設定
		if ($('.sns_tw_check').is(':checked')) {
			// 別タブの予約チェックボックスもON
			$('.sns_tw_check').attr("checked", "checked");
		} else {
			// チェックを外されたらメニューを無効化
			$('.sns_tw_check').removeAttr("checked");
		}
		// checkboxがチェック
		$('.sns_tw_check').change(function () {
			if ($(this).is(':checked')) {
				// 別タブの予約チェックボックスもON
				$('.sns_tw_check').attr("checked", "checked");

			} else {
				// 別タブの予約チェックボックスも OFF
				$('.sns_tw_check').removeAttr("checked");
			}
		});
		
		$('#form2').submit(function(){
			// 選択させたくないために無効にしているので、送信時には無効を解除
			$('.sns_btn').removeAttr("disabled");
			// submit継続
			return true;
		})
	});
</script>
