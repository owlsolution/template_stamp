<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.5">
<link rel="stylesheet" href="./css/sp_style.css" />
<link rel="stylesheet" href="./css/jquery-ui-1.10.4.custom.css" />
<script type="text/javascript" src="./js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="./js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="./js/jquery.upload-1.0.2.js"></script>
<script type="text/javascript" src="./js/jquery-ui-1.10.4.custom.min.js"></script>
<script type="text/javascript" src="./js/jquery.blockUI.js?ver=6.01"></script>
<script type="text/javascript" src="./js/script.js?ver=6.02"></script>
<title>OWLSTAMAPアプリ管理画面</title>
</head>
<body>
<div id="container_all">
	<div id="header" class="clearfix">
		<h1>
			<img src="images/logo.png" width="100%">
		</h1>
	</div>
