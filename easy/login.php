<?php
//重要なｲﾝｸﾙｰﾄ
require_once('./../../config.php');
require_once('./../ad_config.php');

// お知らせ部品を読込み
require_once('./../common/model/AdminUserModel.php');

// お知らせ部品
$adminUserModel = new AdminUserModel();

// スマホでなければ非サポート表示
//if(!Util::is_smartphone ()) {
//	header('Location:./../sp/unsupport.php');
//	exit;
//}

// 有効期限を30分
$expire = time() + 60 * 30; // 30分有効
//$expire = time() + 30; // 30秒有効
setcookie('stamp_easy_expire', '1', $expire);

$indent_id = empty($_GET['id']) ? '' : $_GET['id'];
if (empty($indent_id)) {
	header('Location:./../sp/unsupport.php');
	exit;
}
$user_id = Util::get_authenticated_user_id();
if (empty($user_id)) {
	header('Location:./../sp/unsupport.php');
	exit;
}
$db = db_connect();
$admin = $adminUserModel->get_admin_id_by_user_id($db, $user_id);
db_close( $db );
if (empty($admin['admin_id'])) {
	header('Location:./../sp/unsupport.php');
	exit;
}


if (isset($_GET['p'])) {
    $_SESSION['man'][FILE_NAME]['p'] = $_GET['p'];
}
if (isset($_GET['rally_id'])) {
    $_SESSION['man'][FILE_NAME]['rally_id'] = $_GET['rally_id'];
}
if (isset($_GET['id'])) {
    $_SESSION['man'][FILE_NAME]['id'] = $_GET['id'];
}
if (!empty($admin['admin_id'])) {
    $_SESSION['man'][FILE_NAME]['admin_id'] = $admin['admin_id'];
    $_SESSION['man'][FILE_NAME]['shop_admin_id'] = $admin['admin_id'];
}

error_log("login_info".print_r($_SESSION['man'][FILE_NAME], true));

if (isset($_SESSION['man'][FILE_NAME])) {
	$p = $_SESSION['man'][FILE_NAME]['p'];
	$rally_id = $_SESSION['man'][FILE_NAME]['rally_id'];
	$ident_id = $_SESSION['man'][FILE_NAME]['id'];
	$admin_id = $_SESSION['man'][FILE_NAME]['admin_id'];
	$shop_admin_id = $_SESSION['man'][FILE_NAME]['shop_admin_id'];
} else {
	echo "ログインできませんでした。管理画面の設定をご確認ください。";
	exit;
}

// phpセッションに各種情報を保存

// rally_idのバリデーションチェック
if (empty($rally_id) || !is_numeric($rally_id)) {
	// 数値以外が含まれている場合、処理しない
	error_log("attack rally_id sql injection:".$rally_id);
	// 店舗情報画面へリダイレクト
	header('Location:./../sp/unsupport.php');
	exit;
}

if(empty($ident_id) || (strlen($ident_id) >= 128)) {
	// idが異常値の場合処理しない
	error_log("attack id sql injection:".$ident_id);
	header('Location:./../sp/unsupport.php');
	exit;
}

$pre_save = isset($_GET['pre_save']) ? "&pre_save=".$_GET['pre_save'] : "";
$show_saveonly = isset($_GET['show_saveonly']) ? "&show_saveonly=".$_GET['show_saveonly'] : "";

header('Location:./?p='.$p.$pre_save.$show_saveonly);

?>
