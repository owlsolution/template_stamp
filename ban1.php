<?php
require_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'config.php');
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'ad_config.php');

$APPLE_STORE_URL = "https://itunes.apple.com/jp/app/gmo%E3%82%B9%E3%83%9E%E3%83%BC%E3%83%88%E6%94%AF%E6%89%95%E3%81%84/id830551218?mt=8";
$GOOGLE_PLAY_URL = "https://play.google.com/store/apps/details?id=com.gmopg.palletgen&hl=ja";

if (Device::is_ios_browser()) {
	header("Location:".$APPLE_STORE_URL);
	exit;
} else if (Device::is_android_browser()) {
	header("Location:".$GOOGLE_PLAY_URL);
	exit;
} else {
	echo "<h1>スマホから閲覧ください。</h1>";
}
