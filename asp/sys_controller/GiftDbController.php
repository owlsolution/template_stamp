<?php
//重要なｲﾝｸﾙｰﾄ
require_once('./../../../config.php');
require_once('./../../ad_config.php');

// 管理アカウント部品
require_once('./../../common/model/AdminModel.php');
$adminModel = new AdminModel();

/**
 * DBアクセス結果の情報を取得する
 * @param type $data_rows
 * @return array
 */
function get_dbdata_array($data_rows) {
    $data_list = array();  // 空配列宣言
	while ($row = mysql_fetch_array($data_rows, MYSQL_ASSOC)){
//		$dateParts = Util::get_rally_of_day($row['add_date2'], $row['rally_id']);
//		// add_dateを現時点での日付に変換
//		$row['add_date'] = $dateParts[0]."-".$dateParts[1]."-".$dateParts[2];
//		
//		$dateParts = Util::get_rally_of_day($row['last_stamp_date'], $row['rally_id']);
//		$row['last_stamp_date'] = $dateParts[0]."-".$dateParts[1]."-".$dateParts[2];
//		
		$data_list[] = $row;
	}
	return $data_list;
}

$rally_date = Util::rally_information_get(ADMIN_ID);
$rally_id = $rally_date['rally_id'];

$db = db_connect();
$branch_ids = null;
if ($_SESSION["branchFlag"] == OWNER) {
	// 全員
} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] == 0)){
	// 全員
} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] != 0)){
	// 単一ブランチ
	$branch_ids = $_SESSION["branchId"];
} else if ($_SESSION["branchFlag"] == BRANCH_MANAGER) {
	// 単一ブランチ
	$branch_ids = $_SESSION["branchId"];
} else if (($_SESSION["branchFlag"] == ORG_MANAGER) && ($_SESSION["branchId"] != 0)){
	// 単一ブランチ
	$branch_ids = $_SESSION["branchId"];
} else if ($_SESSION["branchFlag"] == ORG_MANAGER) {
	// 組織IDから対象のブランチidを取得する
	$branch_list = $adminModel->get_branch_by_organization_id($db, $_SESSION["orgId"]);
	$branch_id_list = array_column($branch_list, 'id');
	$branch_ids = implode(",", $branch_id_list);
} else if ($_SESSION["branchFlag"] == STAFF) {
	// スタッフが所属する支店の支店IDを取得する
	// 現状未対応
}
db_close($db);


// 必須検索条件
$where = " u.user_id = r.user_id AND r.admin_id = ".ADMIN_ID;

// 支店リストを保持している場合、さらに絞り込み条件を付加
if (!empty($branch_ids)) {
	// 対象の支店IDリストがあるものは、支店IDを条件に追加する
	$where .= " AND r.branch_id IN( ".$branch_ids.")";
}

$is_user_list = empty($_POST['is_user_list']) ? "" : $_POST['is_user_list'];
$selection = $_POST['selection'];
if(empty($selection)) {
	exit;
}
if($selection == "all"){
	/*
	 * 一斉配信人数取得
	 */
	$db = db_connect();
	if (empty($is_user_list)) {
			$array_all = user_count($db , $where);
	} else {
		$rally_user_date = all_user_information_select($db , $where);
		$array_all = mysql_num_rows($rally_user_date);
		// ラリーユーザデータを取得
		$user_list = get_dbdata_array($rally_user_date);
	}
	db_close( $db );
} else if($selection == "simple"){
	/*
	 * 簡単配信人数取得
	 */
	$array_all = 0;
	//******************************** 誕生日 ********************************//
	$birth_date_count = 0;
	$birth_date = "";
	$birth_date_where = "";
	if($_POST['simple_terms1'] == "checked"){
		$last_month = date('-m-' , strtotime('first day of previous month'));
		$birth_date .= "birth_date LIKE  '%".$last_month."%'";
		$birth_date_count++;
	}
	if($_POST['simple_terms2'] == "checked"){
		$now_month = date('-m-');
		if($birth_date_count != 0){
			$birth_date .= " OR ";
		}
		$birth_date .= "birth_date LIKE  '%".$now_month."%'";
		$birth_date_count++;
	}
		if($_POST['simple_terms3'] == "checked"){
		$next_month = date('-m-' , strtotime('first day of next month'));
		if($birth_date_count != 0){
			$birth_date .= " OR ";
		}
		$birth_date .= "birth_date LIKE  '%".$next_month."%'";
		$birth_date_count++;
	}
	if($birth_date_count != 0){
		$birth_date_where = " AND u.user_id IN (SELECT user_id FROM user WHERE ".$birth_date.")";
	}
	//******************************** 性別 ********************************//
	$sex_count = 0;
	$sex = "";
	$sex_where = "";
	if($_POST['simple_terms4'] == "checked"){  //男生
		$sex .= "1";
		$sex_count++;
	}
	if($_POST['simple_terms5'] == "checked"){  //女性
		if($sex_count != 0){
			$sex .= ",";
		}
		$sex .= "2";
		$sex_count++;
	}
	if($sex_count == 2){  //どちらでもない
		$sex = "0";
	}
	if($sex_count != 0){
		$sex_where = " AND u.sex IN (".$sex.")";
	}
	//******************************** 最終来店日 ********************************//
	$last_stamp_date_count = 0;
	$last_stamp_date = "";
	$last_stamp_date_where = "";
	if($_POST['simple_terms6'] == "checked"){
//		$yesterday = date('Y-m-d', strtotime('-1 day')); // 昨日の日付
//		$last_stamp_date .= "last_stamp_date = '".$yesterday."'";
		$yesterday = date('Y-m-d H:i:s', strtotime('-1 day')); // 昨日の日付
		$last_start_time = Util::get_rally_start_time_of_day($yesterday, $rally_id);
		$last_end_time = Util::get_rally_end_time_of_day($yesterday, $rally_id);
		$last_stamp_date .= "r.last_stamp_date BETWEEN '".$last_start_time."' AND '".$last_end_time."'";
		$last_stamp_date_count++;
	}
	if($_POST['simple_terms7'] == "checked"){
//		$before_month = date('Y-m-d', strtotime('-1 month')); // 1か月前
		$before_month = date('Y-m-d H:i:s', strtotime('-1 month')); // 1か月前
		// １ヶ月前の日よりも前の日が対象なので
		$before_month = Util::get_rally_start_time_of_day($before_month, $rally_id);
		if($last_stamp_date_count != 0){
			$last_stamp_date .= " OR ";
		}
		$last_stamp_date .= "r.last_stamp_date <= '".$before_month."'";
		$last_stamp_date_count++;
	}
	if($_POST['simple_terms8'] == "checked"){
//		$this_month = date('Y-m-01'); // 1か月前
		$parts_date = Util::get_rally_of_day(date('Y-m-d H:i:s'), $rally_id);
		$this_month = $parts_date[0]."-".$parts_date[1]."-01 00:00:00"; // 今月の１日の初めの時間
		if($last_stamp_date_count != 0){
			$last_stamp_date .= " OR ";
		}
		$last_stamp_date .= "r.last_stamp_date >= '".$this_month."'";
		$last_stamp_date_count++;
	}
	if($last_stamp_date_count != 0){
		$last_stamp_date_where = " AND ".$last_stamp_date."";
	}
	//******************************** お知らせ形式 ********************************//
//	$notice_format_where = "";
//	if(!empty($_POST['format_push'])){
//		$notice_format_where = " AND r.classification = 1";
//	}
//	if(!empty($_POST['format_mail'])){
//		$notice_format_where = " AND r.classification = 2";
//	}
//	if(!empty($_POST['format_push']) && !empty($_POST['format_mail'])){
//		$notice_format_where = "";
//	}
	//******************************** DBアクセス ********************************//
//	if(empty($_POST['format_push']) && empty($_POST['format_mail'])){
//		$array_all = 0;
//	} else 
	if(empty($birth_date_where) && empty($sex_where) && empty($last_stamp_date_where)){
		$array_all = 0;
	} else {
		$db = db_connect();
		
		$where .= " ".$birth_date_where.$sex_where.$last_stamp_date_where;
		if (empty($is_user_list)) {
			$array_all = user_count($db , $where);
		} else {
			$rally_user_date = all_user_information_select($db , $where);
			$array_all = mysql_num_rows($rally_user_date);
			// ラリーユーザデータを取得
			$user_list = get_dbdata_array($rally_user_date);
		}
		
		db_close( $db );
	}
} else if($selection == "individual"){
	/*
	 * 個別配信人数取得
	 */
	//******************************** 性別 ********************************//
	$individual_sex = $_POST['individual_terms1'];
	$sex_count = 0;
	$sex = "";
	$sex_where = "";
	if($individual_sex == "man"){
		$sex .= "1";
		$sex_count++;
	} else if($individual_sex == "woman"){
		if($sex_count != 0){
			$sex .= ",";
		}
		$sex .= "2";
		$sex_count++;
	}
	if($sex_count != 0){
		$sex_where = " AND u.sex IN (".$sex.")";
	}
	//******************************** 誕生日 ********************************//
	$individual_birthday_start = $_POST['individual_terms2'];
	$individual_birthday_end = $_POST['individual_terms3'];
	$birth_date = "";
	$birth_date_where = "";
	for($i=$individual_birthday_start;$i<=$individual_birthday_end;$i++){
		if($i != $individual_birthday_start){
			$birth_date .= " OR ";
		}
		$s = sprintf("%02d", $i);
		$birth_date .= "birth_date LIKE  '%-".$s."-%'";
	}
	if($individual_birthday_start != 1 || $individual_birthday_end !=12){
		$birth_date_where = " AND u.user_id IN (SELECT user_id FROM user WHERE ".$birth_date.")";
	}
	//******************************** 地域 ********************************//
	$individual_region = $_POST['individual_terms4'];
	$region_where = "";
	if(!empty($individual_region)){
		$region_where = " AND u.region = '".$individual_region."'";
	}
	//******************************** 最終来店日 ********************************//
	$last_start = $_POST['individual_terms7'];
	$last_start_time = Util::get_starttime_of_day($last_start, $rally_id);
	$last_end = $_POST['individual_terms8'];
	$last_end_time = Util::get_endtime_of_day($last_end, $rally_id);
	$last_stamp_date_where = "";
	if(!empty($last_start) && !empty($last_end)){
		$last_stamp_date_where = " AND r.last_stamp_date BETWEEN '".$last_start_time."' AND '".$last_end_time."' ";
	}
	$total_stamp_num = $_POST['individual_terms9'];
	$total_stamp_terms = $_POST['individual_terms10'];
	$total_stamp_num_where = "";
	if(!empty($total_stamp_num)){
		if($total_stamp_terms == "or_more"){
			$total_terms = ">=";
		} else if($total_stamp_terms == "downward"){
			$total_terms = "<=";
		}
		$total_stamp_num_where = " AND r.total_stamp_num ".$total_terms." '".$total_stamp_num."' ";
	}
	//******************************** お知らせ形式 ********************************//
//	$notice_format_where = "";
//	if(!empty($_POST['format_push'])){
//		$notice_format_where = " AND r.classification = 1";
//	}
//	if(!empty($_POST['format_mail'])){
//		$notice_format_where = " AND r.classification = 2";
//	}
//	if(!empty($_POST['format_push']) && !empty($_POST['format_mail'])){
//		$notice_format_where = "";
//	}
//	if(empty($_POST['format_push']) && empty($_POST['format_mail'])){
//		$array_all = 0;
//	} else {
		$db = db_connect();
//		//$where = "u.user_id = r.user_id AND r.admin_id = ".ADMIN_ID." ".$sex_where.$birth_date_where.$region_where.$add_date_where.$region_where.$last_stamp_date_where.$total_stamp_num_where.$notice_format_where;
		$where .= " ".$sex_where.$birth_date_where.$add_date_where.$region_where.$last_stamp_date_where.$total_stamp_num_where;
		
		if (empty($is_user_list)) {
			$array_all = user_count($db , $where);
		} else {
			$rally_user_date = all_user_information_select($db , $where);
			$array_all = mysql_num_rows($rally_user_date);

			// ラリーユーザデータを取得
			$user_list = get_dbdata_array($rally_user_date);
		}
		db_close( $db );
	}
//}

$usersinfo = array(
    "user_num" => $array_all, 
    "id" => USER_ID, 
    "data" =>$user_list);
print json_encode($usersinfo); // JSON出力
//print json_encode($array_all); // JSON出力

?>