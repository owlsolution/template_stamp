<?php
/**
 * 予約お問い合わせ画面表示コントローラー
 */

// モデルの読み込み
require_once('./../common/model/AdminModel.php');
$adminModel = new AdminModel();

// 予約と問い合わせのモデルは全店舗共通
require_once('./../common/model/ReserveModel.php');
$reserveModel = new ReserveModel();

require_once('./../common/model/AlertSettingModel.php');
$alertSettingModel = new AlertSettingModel();

/**
 * 削除(ソフトデリート)
 */
if(isset($_GET['dele_id'])){
	$delete_id = $_GET['dele_id'];
	$db = db_connect();
	// ログインしているアカウントのスコープ範囲内かをチェック
	
	// 範囲内であれば削除する
	$reserveModel->delete($db, $delete_id);
	
	error_log("delete_id:".$delete_id);
	
	db_close( $db );
	header("Location: ./?p=reserve");
	return;
}

// 予約IDを設定
$reserve_id = isset($_GET['check_id']) ? $_GET['check_id'] : '';
if (empty($reserve_id)) {
	// エラー
	header("Location: ./?p=reserve");
	return;
}

$db = db_connect();

// rally_id取得
$rally_id = $adminModel->get_rally_id($db, ADMIN_ID);

// 管理画面のスコープとしてのadmin_idを取得
$scope_admin_id = $adminModel->get_admin_limited_manage($db);
if (!$scope_admin_id) {
	echo 'Management scope error. id:'.$admin_id;
	return;
}

// タイプ別に予約や問い合わせのリストが変更される。
//$admin_type = $adminModel->get_admin_type($db, $scope_admin_id);
//if (($admin_type == OWNER)||($admin_type == BRANCHES_OWNER)) {
//	// オーナーアカウント
//	$branch_list = $adminModel->get_branch_by_owner_admin_id($db, $scope_admin_id);
//
//} else if ($admin_type == ORG_MANAGER) {
//	// 組織アカウント
//	$branch_list = $adminModel->get_branch_by_org_admin_id($db, $scope_admin_id);
//	
//} else if ($admin_type == BRANCH_MANAGER) {
//	// 支店アカウント
//	$branch_list = $adminModel->get_branch_by_admin_id($db, $scope_admin_id);
//}

$branch_list = $adminModel->get_branch_by_owner_admin_id($db, ADMIN_ID);

// 予約情報を取得
$reserve = $reserveModel->find_by_rally_id_and_id($db, $rally_id, $reserve_id);

//var_dump($reserve_list);
// 同じユーザからの予約履歴を取得
$history_list = $reserveModel->find_by_rally_id_and_user_id($db, $rally_id, $reserve['user_id'], $reserve['branch_id']);

db_close( $db );



if(Util::is_smartphone ()) {
	require './sp/page/reserve_detail.php';
} else {
	require './pc/page/reserve_detail.php';
}