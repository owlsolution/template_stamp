<?php
require_once(dirname(dirname(dirname(dirname(__FILE__)))).DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'class.image.php');

/**
 * 画像リサイズ
 * @param type $up_filename
 * @param type $im
 * @param type $width
 * @param type $height
 * @param type $resized_img_filename
 */
function resize_img_to_jpg($up_filename,$im,$width,$height,$resized_img_filename) {
	list($org_width,$org_height,$org_type,$org_attr)=getimagesize($up_filename);
	if (! $im) {
		imagedestroy($im);
	} else {
		$resized_img = imagecreatetruecolor($width, $height);
		imagecopyresampled($resized_img, $im, 0, 0, 0, 0, $width, $height, $org_width, $org_height);
		imagejpeg($resized_img, $resized_img_filename, 50);
		imagedestroy($im);
		imagedestroy($resized_img);
	}
}

/**
 * 画像リサイズ
 * @param type $im  リソースID
 * @param type $img_filename 保存先:フルパスのファイル名
 */
function save_img_jpg($im,$img_filename) {
	if (!$im) {
		imagedestroy($im);
	} else {
		imagejpeg($im, $img_filename, 100);
	}
}

/**
 * JPEGファイルの回転
 * @param type $filename
 * @return type
 */
function imagecreatefromjpegexif($filename) {
        $img = imagecreatefromjpeg($filename);
        $exif = exif_read_data($filename);
        if ($img && $exif && isset($exif['Orientation']))
        {
            $ort = $exif['Orientation'];

            if ($ort == 6 || $ort == 5)
                $img = imagerotate($img, 270, null);
            if ($ort == 3 || $ort == 4)
                $img = imagerotate($img, 180, null);
            if ($ort == 8 || $ort == 7)
                $img = imagerotate($img, 90, null);

            if ($ort == 5 || $ort == 4 || $ort == 7)
                imageflip($img, IMG_FLIP_HORIZONTAL);
        }
        return $img;
}


if (is_uploaded_file($_FILES["ic_image"]["tmp_name"])) {
	$filename = date('YmdHis').$_FILES["ic_image"]["name"];
	
	/*
	 * ガラケーの為の処理
	 */
	$img_store_dir_ic = "./../../staff_images/";
	$resized_img_filename = $img_store_dir_ic . $filename; // 出力ファイル名設定
	$up_filename = $_FILES["ic_image"]["tmp_name"];
	$imgtype = exif_imagetype($up_filename);
	$size = getimagesize($up_filename);

	//中間画像の比率算出
	//元画像の横の長さ、縦の長さを取得
	$width = 150;
	$height = 150;
	
	$ratioX = $width/$size[0];
	$ratioY = $height/$size[1];

	// 比率の小さい方を使用して中間画像の幅・高さを算出
	if($ratioX < $ratioY){
		$tmp_img_width = $size[0]*$ratioY;
		$tmp_img_height = $size[1]*$ratioY;
	} else {
		$tmp_img_width = $size[0]*$ratioX;
		$tmp_img_height = $size[1]*$ratioX;
	}
	// 四捨五入
	$tmp_img_width = round($tmp_img_width);
	$tmp_img_height = round($tmp_img_height);

	switch ($imgtype) {
		case '1' :
			$im = @imagecreatefromgif($up_filename);
			resize_img_to_jpg($up_filename,$im,$tmp_img_width,$tmp_img_height,$resized_img_filename);
			break;
		case '2' :
			$im = imagecreatefromjpegexif($up_filename);
			resize_img_to_jpg($up_filename,$im,$tmp_img_width,$tmp_img_height,$resized_img_filename);
			break;
		case '3' :
			$im = @imagecreatefrompng($up_filename);
			resize_img_to_jpg($up_filename,$im,$tmp_img_width,$tmp_img_height,$resized_img_filename);
			break;
		default :
			break;
	}

	// 不要部分のカット
	$thumb = new Image($resized_img_filename);
	// 中央位置になるようにポジションを調整
	$x = ($tmp_img_width - $width)/2;
	$y = ($tmp_img_height - $height)/2;

	// 四捨五入
	$x = round($x);
	$y = round($y);
	$thumb -> width($width);
	$thumb -> height($height);
	$thumb -> crop($x, $y);
	$thumb -> save();
	
	$usersinfo = array(
		"imgtype" => $imgtype,
		"filename" => $filename,  // ファイル名を返却
		"img_store_dir_ic" =>$img_store_dir_ic,
	);
	print json_encode($usersinfo); // JSON出力

} else {
	die("ファイルがないぜよ。");
}
?>