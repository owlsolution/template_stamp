<?php
function get_period_notice($admin_id){
	$db = db_connect();
	$where = "admin_id = ".$admin_id;
	$data = select_period_notice($db , $where);
 	$row = mysql_fetch_array($data);
	db_close( $db );
	return $row;
}

/*
 * 期限日の更新
 */
function update_limit_days($admin_id, $stamp_limit_notice, $coupon_limit_notice, $send_time){
	$db = db_connect();
        $set = "stamp_limit_notice = ".$stamp_limit_notice.", coupon_limit_notice = ".$coupon_limit_notice." , send_time = '".$send_time.":00:00'";
	$where = "admin_id = ".$admin_id;
	$data = update_period_notice($db , $set, $where);
 	$row = mysql_fetch_array($data);
	db_close( $db );
	return $row;
}


/*
 * 変数(初期化など）
 */
$stamp_limit_days = isset($_POST['stamp_limit_days']) ? $_POST['stamp_limit_days'] : '-1';
$coupon_limit_days = isset($_POST['coupon_limit_days']) ? $_POST['coupon_limit_days'] : '-1';
$send_time = isset($_POST['send_time']) ? $_POST['send_time'] : '0';

if(isset($_POST['setting'])){  // 各設定の更新処理

	// 期限日の更新
	update_limit_days(ADMIN_ID, $stamp_limit_days, $coupon_limit_days, $send_time);

	if(Util::is_smartphone ()) {
		require './sp/page/push_setting_end.php';
	} else {
		require './pc/page/push_setting_end.php';
	}
} else if(isset($_POST['check'])){  //確認ページ
	if(Util::is_smartphone ()) {
		require './sp/page/push_setting_check.php';
	} else {
		require './pc/page/push_setting_check.php';
	}
} else {
    // 初期表示
    $period_data = get_period_notice(ADMIN_ID);
    $stamp_limit_days = $period_data['stamp_limit_notice'];
    $coupon_limit_days = $period_data['coupon_limit_notice'];
    $send_times = explode(":", $period_data['send_time']);
    $send_time = $send_times[0];
	
	if(Util::is_smartphone ()) {
		require './sp/page/push_setting_form.php';
	} else {
		require './pc/page/push_setting_form.php';
	}
}
