<?php
	// 月別の対象人数
	$db = db_connect();
	// 月別新規登録者数取得
	$where = "admin_id = '".ADMIN_ID."'";
	$data_list = new_user_count($db , $where);
	while ($row = mysql_fetch_array($data_list)){
		$new_user_list_by_month[] = $row;
	}
	db_close( $db );
	
	// ラリーid取得
	$db = db_connect();
	$rally_date = rally_select($db , "admin_id = ".ADMIN_ID);
	$rally = mysql_fetch_array($rally_date);
	$rally_id = $rally['rally_id'];
	db_close( $db );
	
	$admin_id = ADMIN_ID;
	$admin_list = ADMIN_ID;

	// 支店がある場合、支店のadmin_idを取得
	$db = db_connect();
	$admin_ids = getChildAdminIds($db, ADMIN_ID);
	foreach ($admin_ids as $id) {
		$admin_list .= ",".$id;
	}
	db_close( $db );

	$db = db_connect();
	// rally_idが一致するスタッフは対象
	$where = "rally_id = ".$rally_id." AND status = '1'";
	$staff_data = staff_select($db , $where);
	$staff_list = array();
	while ($staff = mysql_fetch_array($staff_data)){
		$admin_list .= ",".$staff['admin_id'];
	}
	db_close( $db );
	
	// 再来店人数
	$statistics = array();
	$subtotal = 0;
	$unaccess = 0;
	$unaccess_next = 0;
	foreach ($new_user_list_by_month as $value) {
		// 月毎の来店人数を算出
		$month = $value['add_month'];
		
		// 先月の登録者数
		$last_month_num = $subtotal - $unaccess_next;
		$value['last_month_num'] = $last_month_num;
		
		// 当月の総ユーザ数
		$subtotal += $value['count'];
		$value['subtotal'] = $subtotal;
		
		$db = db_connect();
		$user_stamp_data = coming_user_count($db , $rally_id, $month);
		$value['repeat_num'] = 0;
		$user_id = 0;
		$user_repeat_num = 0;

		// 同日のスタンプ処理は１回の来店とみなす
		while ($row = mysql_fetch_array($user_stamp_data)){
			if ($row['user_id'] != $user_id) {
				// ユーザIDが同じでないならuserの初回処理
				if ($row['user_add_month'] == $month) {
					// 同月に登録している場合、初回来店はカウントしない
				} else {
					$user_repeat_num += 1;
				}
			} else {
				// 同日のものはカウントしない
			}
		}
		$value['repeat_num'] = $user_repeat_num;
		
		// 当月以降アクセスしていない人数
		$value['last_access'] = 0;
		$user_data = nonactive_user_num($db , ADMIN_ID, $month);
		while ($row = mysql_fetch_array($user_data)){
			$value['last_access'] += isset($row['count']) ? $row['count'] : 0;
		}
		// アクティブユーザ数
		$value['active_user_num'] = $subtotal - $value['last_access'];
		
		
		// 先月以降アクセスしていない人数
		$value['last_access1'] = 0;
		list($year, $tmp_month) = explode('-', $month);
		
		// 指定月の先月
		$month = date('Y-m', strtotime(date($year.'-'.$tmp_month.'-1') . '-1 month'));
		
		$user_data = nonactive_user_num($db , ADMIN_ID, $month);
		while ($row = mysql_fetch_array($user_data)){
			$value['last_access1'] += isset($row['count']) ? $row['count'] : 0;
		}
		// アクティブユーザ数
		$value['active_user_num1'] = $subtotal - $value['last_access1'];
		
		
		// 指定月の先々月
		$month = date('Y-m', strtotime(date($year.'-'.$tmp_month.'-1') . '-2 month'));
		// 先々月以降アクセスしていない人数
		$value['last_access2'] = 0;
		$user_data = nonactive_user_num($db , ADMIN_ID, $month);
		while ($row = mysql_fetch_array($user_data)){
			$value['last_access2'] += isset($row['count']) ? $row['count'] : 0;
		}
		// アクティブユーザ数
		$value['active_user_num2'] = $subtotal - $value['last_access2'];
		
		
		// 指定月の6か月
		$month = date('Y-m', strtotime(date($year.'-'.$tmp_month.'-1') . '-5 month'));
		// 先々月以降アクセスしていない人数
		$value['last_access5'] = 0;
		$user_data = nonactive_user_num($db , ADMIN_ID, $month);
		while ($row = mysql_fetch_array($user_data)){
			$value['last_access5'] += isset($row['count']) ? $row['count'] : 0;
		}
		// アクティブユーザ数
		$value['active_user_num5'] = $subtotal - $value['last_access5'];
		
		
		// お知らせ配信回数
		$notice_data = notice_count_by_month($db, $admin_list, $month);
		while ($row = mysql_fetch_array($notice_data)){
			if ($row['notice_month'] == $month) {
				$value['notice_num'] = $row['count'];
			}
		}
		
		// プレゼント発行回数
		$present_data = present_count_by_month($db , $admin_list, $month);
		while ($row = mysql_fetch_array($present_data)){
			if ($row['present_month'] == $month) {
				$value['present_num'] = $row['count'];
			}
		}
		
		// リピート率
		$value['repeat_percent'] = round(($value['repeat_num']/$value['active_user_num'])*100 ,1);
		$value['repeat_percent1'] = round(($value['repeat_num']/$value['active_user_num1'])*100 ,1);
		$value['repeat_percent2'] = round(($value['repeat_num']/$value['active_user_num2'])*100 ,1);
		$value['repeat_percent5'] = round(($value['repeat_num']/$value['active_user_num5'])*100 ,1);
		
		$statistics[] = $value;
		db_close( $db );
	}
	$new_user_list_by_month = $statistics;
	
	
	
	require "./pc/page/statistics.php";
