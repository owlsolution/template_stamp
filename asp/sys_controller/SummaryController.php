<?php
/*
 * スタンプ一覧
 */
// 初期化設定
$rally_expire_type = 1;  //スタンプ有効期限タイプ
$goal_expire_type = 1;  //ゴール有効期限タイプ
$disabled = "disabled";
$user_profile_force_check  = 2;
$profile_force = 0;

$rally_id = $_GET['rally_id'];
if(isset($_POST['setting']) || isset($_POST['return'])){
	$rally_name = $_POST['rally_name'];  //スタンプ名
	$hp_url = $_POST['hp_url'];				//ホームページURL
	$category_select = $_POST['category'];	//カテゴリ
	$stamp_day = $_POST['stamp_day'];		//スタンプ有効期限 日
	$stamp_date = $_POST['stamp_date'];		//スタンプ有効期限 日付
	$goal_day = $_POST['goal_day'];			//ゴール有効期限 日
	$goal_date = $_POST['goal_date'];		//ゴール有効期限 日付
	$takeover = $_POST['takeover']; //カード引き継ぎ機能
	$stamp_max = $_POST['stamp_max'];//全スタンプ数
	$smart_setting = $_POST['smart_setting'];  //スマートフォン設定
	$download_url = $_POST['download_url'];  //アプリダウンロードファイル名
	$one_times_a_day = $_POST['one_times_a_day'];  //１日１回付与
	$new_stamp_num = $_POST['new_stamp_num'];  //新規スタンプ数
	$user_profile_force_check = $_POST['user_profile_force_check'];  //ユーザー情報を登録しているかのポップアップを表示非表示のチェック
	$profile_force = $_POST['profile_force'];  //プロフィール要求度  $profile_force == 2 : 初期起動時のみポップアップを表示  == 3 : 初期起動時＋クーポン発行時にポップアップを表示

	/**
	 * プロフィール登録を促すポップアップの表示をいいえの場合
	 * 【プロフィール登録要求度の設定】にチェックが入っていれば、全てOFFにする。
	 * $user_profile_force_check == 1 : はい
	 * $user_profile_force_check == 2 : いいえ
	 */
	$disabled = "";
	if($user_profile_force_check == 2){
		$profile_force = 0;
		$disabled = "disabled";
	}
}
if(isset($_POST['check'])){
	$url = "./?p=summary&rally_id=".$rally_id;
	require "./pc/page/new_stamp_check.php";
} else if(isset($_POST['setting'])){
	//echo $rally_id;
	$db = db_connect();
	$set = "rally_name = '".$rally_name."' , hp_url = '".$hp_url."' , stamp_max = '".$stamp_max."' , category_id = '".$category_select."' , rally_expire_type = '".$rally_expire_type."' , stamp_day = '".$stamp_day."' , stamp_date = '".$stamp_date."' , goal_expire_type = '".$goal_expire_type."' , goal_day = '".$goal_day."' , goal_date = '".$goal_date."' , takeover = '".$takeover."' , smart_setting = '".$smart_setting."' , download_url ='".$download_url."' , one_times_a_day = '".$one_times_a_day."' , sns = '".$sns."' , new_stamp_num = '".$new_stamp_num."' , profile_force = ".$profile_force."" ;
	$where = "rally_id = ".$rally_id ;
	up_rally($db , $set , $where);
	db_close( $db );
	require "./pc/page/setting_end.php";
} else {
	if($rally_id != "" && !isset($_POST['return'])){
		$db = db_connect();
		$where = "rally_id = ".$rally_id ;
		$rally_date = rally_select($db , $where);
		$rally = mysql_fetch_array($rally_date);
		$rally_name = $rally['rally_name'];
		$rally_expire_type = $rally['rally_expire_type'];
		if($rally_expire_type == 1){
			$stamp_day = $rally['stamp_day'];
		}
		$goal_expire_type = $rally['goal_expire_type'];
		if($rally_expire_type == 1){
			$goal_day = $rally['goal_day'];
			$goal_date = "";
		}
		$stamp_max = $rally['stamp_max'];
		$smart_setting = $rally['smart_setting'];
		$one_times_a_day = $rally['one_times_a_day'];
		$new_stamp_num = $rally['new_stamp_num'];
		$profile_force = $rally['profile_force'];  // プロフィール強制度
		$disabled = "";
		if($profile_force == 0){
			$user_profile_force_check = 2;
//			$disabled = "disabled";
		} else {
			$user_profile_force_check = 1;
		}
		
		db_close( $db );
		$url = "./?p=summary&rally_id=".$rally_id;
		require "./pc/page/new_stamp_form.php";
	} else if(isset($_POST['return'])){
		$url = "./?p=summary&rally_id=".$rally_id;
		require "./pc/page/new_stamp_form.php";
	} else {
		require "./pc/page/summary_form.php";
	}
}
?>

