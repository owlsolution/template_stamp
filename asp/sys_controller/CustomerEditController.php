<?php
/**
 * 顧客情報詳細の処理
 */

// GETパラメタの取得
// 今のところ、ここに来た場合,user_idとrally_idが存在チェック済みなので値チェックはしない。
$user_id = $_GET['user_id'];
$rally_id = $_GET['rally_id'];

/**
 * ユーザ情報取得
 */
function get_userinfo($user_id, $rally_id) {
    $db = db_connect();
    $where = "a.user_id = ".$user_id." AND a.rally_id = ".$rally_id." AND b.user_id = ".$user_id;
    $detail_user_date = detail_user_select($db , $where);
    $detail_user = mysql_fetch_array($detail_user_date);
    db_close( $db );
    return $detail_user;
}

/*
 * 顧客累計クーポン数
 */
function get_all_coupon($user_id , $rally_id){
	$db = db_connect();
	$where = "user_id = ".$user_id." AND rally_id = ".$rally_id;
	$get_coupon_date = get_coupon_count($db , $where);
	$get_coupon = mysql_fetch_array($get_coupon_date);
	$all_coupon = $get_coupon['count(*)'];
	db_close( $db );
	return $all_coupon;
}

/*
 * 顧客現在クーポン数
*/
function get_now_coupon($user_id , $rally_id){
	$db = db_connect();
	$where = "user_id = ".$user_id." AND get_coupon_state = 0 AND rally_id = ".$rally_id;
	$get_coupon_date = get_coupon_count($db , $where);
	$get_coupon = mysql_fetch_array($get_coupon_date);
	$now_coupon = $get_coupon['count(*)'];
	db_close( $db );
	return $now_coupon;
}

/*
 * スタンプ履歴の取得
 */
function get_stamp_history($user_id, $rally_id) {
	//スタンプ履歴
	$db = db_connect();
	$where = "user_id = ".$user_id." AND rally_id = ".$rally_id." ORDER BY stamp_history_id DESC";
	$stamp_history_date = stamp_history_select_date($db , $where);
        
        $stamp_history = array();  // 空配列宣言
	while ($row = mysql_fetch_array($stamp_history_date, MYSQL_ASSOC)){
            $stamp_history[] = $row;
	}
	db_close( $db );
        return $stamp_history;
}

/*
 * クーポン取得履歴の取得
 */
function get_got_coupon_history($user_id, $rally_id) {
    $db = db_connect();
    $where = "user_id = ".$user_id." AND rally_id = ".$rally_id."";
    $order = "get_coupon_id DESC";
    $data = get_coupon_select($db ,$where , $order);

    $got_coupon = array();
    while ($row = mysql_fetch_array($data)){
        $got_coupon[] = $row;
    }
    db_close( $db );
    return $got_coupon;
}

/*
 * クーポン消費履歴の取得
 */
function get_consumed_coupon_history($user_id, $rally_id) {
    $db = db_connect();
    $where = "user_id = ".$user_id." AND rally_id = ".$rally_id." AND get_coupon_state = 1";
    $order = "get_coupon_id DESC";
    $data = get_coupon_select($db ,$where , $order);

    $consumed_coupon = array();
    while ($row = mysql_fetch_array($data)){
        $consumed_coupon[] = $row;
    }
    db_close( $db );
    return $consumed_coupon;
}

/*
 * 店舗で利用可能なクーポン名を取得
 */
function get_goal_coupon_name($rally_id) {
    $db = db_connect();
    $where = "rally_id = ".$rally_id."";
    $data = goal_select($db ,$where);

    $goal_coupon = array();
    while ($row = mysql_fetch_array($data)){
        $goal_coupon[] = $row;
    }
    db_close( $db );
    
    return $goal_coupon[0];
}

/*
 * ラリー情報を取得
 */
function get_larry_information($rally_id){
	$db = db_connect();
	$where = "rally_id = ".$rally_id;
	$data = rally_select($db , $where);
	$rally = mysql_fetch_array($data);
	db_close( $db );
	return ($rally);
}

// Ajax処理
if ( isset($_POST['action'] )) {
    require_once('./../../../config.php');
    require_once('./../../ad_config.php');
    
    if ($_POST['action'] == "coupon_delete") {
        
        /**
         *  クーポン削除処理
         */

        $get_coupon_id = $_POST['get_coupon_id'];
        
        $db = db_connect();
        $where = " get_coupon_id = '".$get_coupon_id."';";
        get_coupon_delete($db, $where);
        $ret_array = array(
                'get_coupon_id'	=> $get_coupon_id,
        );
        db_close( $db );
        echo json_encode($ret_array);
        
    } else if ($_POST['action'] == "coupon_add") {
        /**
         *  クーポン追加処理
         */
        
        // セッション情報からラリーID,ユーザIDを取得
        $rally_id = $_SESSION['asp']['customer_edit']['rally_id'];
        $user_id = $_SESSION['asp']['customer_edit']['user_id'];

        // 追加するクーポンのインデックス
        $stamp_index = $_POST['get_coupon_no'];
        
        // クーポン使用終了日の算出
        $rally_data = get_larry_information($rally_id);
        $goal_day = $rally_data['goal_day'];
        $coupon_use_end =  date("Y-m-d H:i:s", strtotime("+".$goal_day." day"));;// クーポン使用終了日

        // $get_coupon_state
        $add_date = date('Y-m-d');  //今日の日付
        $aquisition_date = date('Y-m-d H:i:s');
        
        $goal_coupon = get_goal_coupon_name($rally_id);
        
        $get_coupon_name = $goal_coupon["goal_title_".$stamp_index];
        $stamp_num = $goal_coupon["goal_stamp_num_".$stamp_index];
        $get_coupon_description = $goal_coupon["goal_description_".$stamp_index];
        $get_coupon_img = $goal_coupon["img_name_".$stamp_index];
        $get_coupon_state = "0";
        
        $db = db_connect();
        $into = $rally_id." , ".$user_id." , ".$stamp_num." , '".$get_coupon_name."' , '".$get_coupon_description."' , '".$get_coupon_img."' , ".$get_coupon_state." , '".$aquisition_date."' , '".$add_date."' , '".$aquisition_date."' , '".$coupon_use_end."'";
        coupon_insert($db , $into);
        $get_coupon_id = mysql_insert_id();
        db_close( $db );
        $date_parts = Util::get_rally_of_day($coupon_use_end, $rally_id);
        $ret_array = array(
                'get_coupon_id'	=> $get_coupon_id,
                'coupon_use_end'	=> $date_parts[0]."-".$date_parts[1]."-".$date_parts[2],
                'get_coupon_name'	=> $get_coupon_name,
                'into'	=> $into,
        );
        echo json_encode($ret_array);
        
        
    }
    return;
}


// ユーザ情報取得
$detail_user = get_userinfo($user_id, $rally_id);

// 顧客の累計取得クーポン数取得
$all_coupon = get_all_coupon($user_id , $rally_id);

// 顧客の現在の所持クーポン数取得
$now_coupon = get_now_coupon($user_id , $rally_id);

// スタンプ履歴情報を取得
$stamp_history = get_stamp_history($user_id, $rally_id);

// クーポン取得履歴を取得
$got_coupon_history = get_got_coupon_history($user_id, $rally_id);

// クーポン使用履歴を取得
$consumed_coupon_history = get_consumed_coupon_history($user_id, $rally_id);


if(!empty($_GET['edit'])) {
    // 顧客情報の編集画面を表示
    $_SESSION['asp']['customer_edit']['rally_id'] = $rally_id;
    $_SESSION['asp']['customer_edit']['user_id'] = $user_id;
   
    // 誕生日の初期化
    $birth_year=1980;
    $birth_month=1;
    $birth_day=1;
    
    // 誕生日が初期状態でない場合
    if ($detail_user['birth_date'] != "0000-00-00") {
        // 誕生日を年月日に分割して保持する。
        $birth_array = explode( "-", $detail_user['birth_date']);
        $birth_year = $birth_array[0];
        $birth_month = $birth_array[1];
        $birth_day = $birth_array[2];
    }
    $goal_coupon = get_goal_coupon_name($rally_id);

    // フルブラウザとスマホでビューを切り分ける
    if(Util::is_smartphone ()) {
            require "./sp/page/customer_edit.php";
    } else {
            require "./pc/page/customer_edit.php";
    }

} else if(!empty($_GET['update'])) {    
    // 顧客情報の更新
    
    $user_name = $_POST['customer_name'];
    $region = $_POST['customer_region'];
    $last_stamp_day = $_POST['last_stamp_day'];
    // 男:1 女:2 どちらでもない:0
    $customer_sex = 0;
    if ($_POST['customer_sex'] == "man") {
        $customer_sex = 1;
    } else if ($_POST['customer_sex'] == "woman") {
        $customer_sex = 2;
    }
    
    // 誕生日の設定
    $birth = $_POST['birth_year']."-".$_POST['birth_month']."-".$_POST['birth_day'];
    
    $db = db_connect();
    $set = "user_name = '".mysql_real_escape_string($user_name)."' , sex = '".$customer_sex."' , birth_date = '".$birth."' , region = '".$region."'" ;
    $where = "user_id = ".$user_id ;
    user_update($db , $set , $where);
    db_close( $db );

    // スタンプ数変更時の差分を算出
    if (!ctype_digit($_POST['current_stamp_num'])) {
        echo '<h3>現在のスタンプ数に無効な値が設定されています。<h3>';
        exit();
    }
    $difference_stamp_num = $_POST['current_stamp_num'] - $detail_user['stamp_num'];
    
    // 更新後の累計スタンプ数
    $total_stamp_num = $detail_user['total_stamp_num'] + $difference_stamp_num;

	if ($difference_stamp_num != 0) {
		// スタンプ数に変更があったので
		// スタンプ数の履歴情報インサート
		$db = db_connect();
		$issue_date = date('Y-m-d H:i:s');
		$shop_num = 1;
		$issue_pass = 0;
		$into = $user_id." , ".$rally_id." , '".$issue_pass."' , '".$issue_date."' , ".$difference_stamp_num." , ".$shop_num;
		stamp_history_insert($db , $into);
		db_close( $db );

		// 現在スタンプ数の変更
		$db = db_connect();
		$set = "stamp_num = '".$_POST['current_stamp_num']."' , total_stamp_num = '".$total_stamp_num."'";
		$where = "rally_id = ".$rally_id." AND user_id = ".$user_id;
		rally_user_up($db , $set , $where);
		db_close( $db );
	}
	
	// 最終スタンプ日時を更新
	if (!empty($_POST['last_stamp_day'])) {
		$db = db_connect();
		$set = "last_stamp_date = '".$_POST['last_stamp_day']."'";
		$where = "rally_id = ".$rally_id." AND user_id = ".$user_id;
		rally_user_up($db , $set , $where);
		db_close( $db );
	}
    // 顧客詳細ページへ
    header("Location: ./?p=customer");
    
}

?>

