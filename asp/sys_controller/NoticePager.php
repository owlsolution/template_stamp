<?php
//重要なｲﾝｸﾙｰﾄ
require_once('./../../../config.php');
require_once('./../../ad_config.php');

// お知らせ部品を読込み
require_once('./../../common/component/NoticeComponent.php');
$noticeComp = new NoticeComponent();

// 管理アカウント部品
require_once('./../../common/model/AdminModel.php');
$adminModel = new AdminModel();

// １ページの表示数
$page_max = isset($_POST['page_max']) ? $_POST['page_max'] : '100';
// 表示開始
$start_index = isset($_POST['start']) ? $_POST['start'] : '1';
// サーバ上のindexは0オリジンなので-1する
$start_index = $start_index - 1;

// 表示する配信タイプを取得
$push_type = isset($_POST['push_type']) ? $_POST['push_type'] : '';
if (count($push_type) > 0) {
	if(in_array('0', $push_type)) {
		$push_type[] = '3';
	}
	$in_push_type = implode(',', $push_type);
}

$rally_date = Util::rally_information_get(ADMIN_ID);
$rally_id = $rally_date['rally_id'];


$db = db_connect();
$staff_data_list = $adminModel->get_staff_by_rally_id($db, $rally_id);
// 支店機能 通常/オーナーはADMIN_ID ,支店権限の場合は支店のadmin_idで検索
if ($_SESSION["branchFlag"] == OWNER) {
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, ADMIN_ID, OWNER);
} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] == 0)){
	// 全支店管理アカウントで全支店管理アカウントモードの場合
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, ADMIN_ID, BRANCHES_OWNER);
} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] != 0)){
	// 全支店管理アカウントで全支店アカウントを選択時は選択した支店管理アカウントに限定した機能
	$branch_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	// 選択中の支店管理アカウントの配下のスタッフのadmin_idを取得する
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $branch_admin_id, BRANCH_MANAGER);
} else if ($_SESSION["branchFlag"] == BRANCH_MANAGER) {
	// 支店管理アカウントでログインしている場合
	$branch_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	// 支店管理アカウントにブラ下がるスタッフを取得
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $branch_admin_id, BRANCH_MANAGER);
} else if (($_SESSION["branchFlag"] == ORG_MANAGER) && ($_SESSION["branchId"] != 0)){
	$branch_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	// 選択中の支店管理アカウントの配下のスタッフのadmin_idを取得する
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $branch_admin_id, BRANCH_MANAGER);
} else if ($_SESSION["branchFlag"] == ORG_MANAGER) {
	// 組織管理アカウントなので、組織IDを元に組織にぶら下がる支店のadmin_idを取ってくる
	$org = $adminModel->get_organization_by_id($db, $_SESSION["orgId"]);
	// 組織IDからadmin_idを取得
	// admin_idとタイプを渡して、ぶら下がるadmin_idのリストを返却するメソッドを呼出す
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $org['admin_id'], ORG_MANAGER);
} else if ($_SESSION["branchFlag"] == STAFF) {
	// スタッフのadmin_idを設定
	$staff = $adminModel->get_staff_by_id($db, $_SESSION["staffId"]);
	$admin_id_list[] = $staff['admin_id'];
}

if (count($admin_id_list)) {
	$send_admin_id = implode(",", $admin_id_list);
}

//配信した支店名の表示
$branch_list = branch_select_by_owner($db, ADMIN_ID);

// 対象の全件を取得
//$where = "admin_id IN( ".$send_admin_id.") ORDER BY notice_id DESC ";
//$notice_date = notice_select($db , $where);

$where_push_type = "";
if (isset($in_push_type)) {
	$where_push_type = " AND notice_type IN(".$in_push_type.") ";
}
error_log("pager_send_admin_id:".$send_admin_id.";");

// PUSH送信状況を取得
$push_status = $noticeComp->get_push_status($send_admin_id);
$notice_list = array();
$where = "admin_id IN( ".$send_admin_id.") ".$where_push_type." ORDER BY notice_id DESC ";
//$where = "admin_id IN( ".$send_admin_id.") ORDER BY notice_id DESC "." LIMIT ".$start_index.",".$page_max;
$notice_date = notice_select($db , $where);
$notice_total_num = mysql_num_rows($notice_date);
$index = 0;
while ($notice = mysql_fetch_array($notice_date)){
	if ($index < $start_index){
//		echo "under:".$index."\n";
		$index++;
		continue;
	} else if ($index > ($start_index + $page_max)) {
//		echo "over:".$index."\n";
		$index++;
		break;
	}
	
	list($accept_day_and_hour,$accept_minute, $accept_second) = explode( ":", $notice['acceptance_datetime']);
	$add_date_date = $accept_day_and_hour.":".$accept_minute;
	$add_date_replace = str_replace("-", "/", $add_date_date);

	// ステータスチェック
	// 送信中/送信済/予約
	$status = "送信済";
	if ($notice['transition'] == '1') {
		$status = "仮投稿";
	} else {
		foreach ($push_status as $value) {
			if ($value['notice_id'] == $notice['notice_id']) {
				// 合致した場合ステータスを参照
				if (($value['status'] == '0') || ($value['status'] == '1')) {
					$status = "送信中";
				} else if ($value['status'] == '4') {
					$status = "予約";
				}
			}
		}
	}

	// 対象店舗名
	// 配信した支店名の表示
	$branch_name = "";
	if(!empty($branch_list)){
		// 支店機能なので支店admin_idと照合する
		foreach($branch_list as $branch){
			if($notice['admin_id'] == $branch['child_admin_id']){
				$branch_name = $branch['name'];
				break;
			}
		}
		if(empty($branch_name)) {
			// admin_idと組織アカウントと比較
			$notice_org = $adminModel->get_organization_by_admin_id($db, $notice['admin_id']);
			// マッチしたら組織名を設定
			if (!empty($notice_org['name'])) {
				// 組織情報がある場合、ログインしているのが組織組織管理アカウントなので、組織名を設定
				$branch_name = $notice_org['name'];
			}
		}
		
//		error_log("branch_name:".$branch_name);
		if(empty($branch_name)) {
			// 支店のadmin_idと一致していないので
			// スタッフのadmin_idと照合する
			foreach($staff_data_list as $result_staff_data){
				if ($notice['admin_id'] == $result_staff_data['admin_id']){
					if($result_staff_data['branch_id'] == 0){
						// スタッフが本部に所属している
						break;
					}
					foreach($branch_list as $branch){
						if($branch['id'] == $result_staff_data['branch_id']){
							$branch_name = $branch['name'];
						}
					}
				}
			}
		}
	}
	if(empty($branch_name)) {
		$branch_name = "本部";
	}
	
	//配信タイプ（0 = お知らせ、1 = チラシ、2 = ブログ、 3 = 個人宛 4=スケジュール配信）を表示する
	if($notice['notice_type'] == 0 || $notice['notice_type'] == 3){
		$notice_type_image = "<img src='./../../sp_images/a03.png' width='15' height='15'>";
	} else if($notice['notice_type'] == 1){
		$notice_type_image = "<img src='./../../sp_images/a02.png' width='15' height='15'>";
	} else if($notice['notice_type'] == 2){
		$notice_type_image = "<img src='./../../sp_images/a01.png' width='15' height='15'>";
	} else if($notice['notice_type'] == 4){
		$notice_type_image = "<img src='./../../sp_images/a04.png' width='15' height='15'>";
	} else if($notice['notice_type'] == 5){
		$notice_type_image = "<img src='./../../sp_images/a05.png' width='15' height='15'>";
	}
// 1レコード分
//	$notice_list[] = $index;
	
	if(Util::is_smartphone ()) {
		$notice_list[] = "
			<td align='center'>{$add_date_replace}</td>
			<td align='center'>{$notice_type_image}</td>
			<td>{$notice['notice_title']}</td>
			<td align='center'>{$status}</td>
			<td align='center'><p><a href='?p=notice&check_id={$notice['notice_id']}'>確認</a></p><p><a href='?p=notice&edit_id={$notice['notice_id']}'>編集</a></p><a href='?p=notice&dele_id={$notice['notice_id']}' onClick=\"return confirm('削除しますか?')\">削除</a></td>";
	} else {
		$read_stat = $notice['already_read_num']."/".$notice['app_delivery_num'];
		if ($notice['transition'] == '1') {
			$read_stat = "-";
		}
		$notice_list[] = "
			<td align='center'>{$add_date_replace}</td>
			<td align='center'>{$notice_type_image}</td>
			<td align='center'>{$branch_name}</td>
			<td>{$notice['notice_title']}</td>
			<td align='center'>{$notice['delivery_num']}/{$notice['total_user_count']}</td>
			<td align='center'>{$read_stat}</td>
			<td align='center'>{$status}</td>
			<td align='center'><a href='?p=notice&check_id={$notice['notice_id']}'>確認</a> ｜ <a href='?p=notice&edit_id={$notice['notice_id']}'>編集</a> ｜ <a href='?p=notice&dele_id={$notice['notice_id']}' onClick=\"return confirm('削除しますか?')\">削除</a></td>";
	}

	// ループカウントインクリメント
	$index++;
}
db_close($db);

$usersinfo = array(
    "notice_total_num" => $notice_total_num, 
    "data" =>$notice_list);
print json_encode($usersinfo); // JSON出力

?>