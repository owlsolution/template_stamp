<?php
/**
 * 顧客情報詳細の処理
 */

// GETパラメタの取得
// 今のところ、ここに来た場合,user_idとrally_idが存在チェック済みなので値チェックはしない。
$user_id = $_GET['user_id'];
$rally_id = $_GET['rally_id'];

$stamp_num = isset($_GET['stamp']) ? $_GET['stamp'] : "";

/**
 * スタンプ付与処理
 */
if (!empty($stamp_num)) {
	
	$user_info = get_userinfo($user_id, $rally_id);
	// 現在のスタンプ数を取得
	$current_stamp_num = $user_info['stamp_num'];
	$total_stamp_num = $user_info['total_stamp_num'];
	
	
	// スタンプを付与
	// スタンプ履歴を記録
	$add_date = date('Y-m-d H:i:s');

	// スタンプ数の履歴情報インサート
	$difference_stamp_num = 1;
	$db = db_connect();
	$issue_date = date('YmdHis');
	$shop_num = 1;
	$issue_pass = "manage_stamp";
	$into = $user_id." , ".$rally_id." , '".$issue_pass.$issue_date."' , '".$add_date."' , ".$difference_stamp_num." , ".$shop_num;
	stamp_history_insert($db , $into);
	db_close( $db );
	
	// 現在スタンプ数/最終スタンプ日時の更新
	$db = db_connect();
	$before_stamp_num = $current_stamp_num;
	$current_stamp_num += $stamp_num;
	$total_stamp_num +=  $stamp_num;
	
	$set = "stamp_num = '".$current_stamp_num."' , total_stamp_num = '".$total_stamp_num."' , last_stamp_date = '".$add_date."'";
	$where = "rally_id = ".$rally_id." AND user_id = ".$user_id;
	rally_user_up($db , $set , $where);
	db_close( $db );
	
	if (STAMP_TYPE == 'B') {
		// タイプBの場合
		// クーポン取得数を跨いだらクーポンを自動取得
		
		// トータル数を超えたら現在のクーポン数をリセットする
		// スタンプラリーのMAX数を取得
		$db = db_connect();
		$where = "rally_id = ".$rally_id;
		$rally_date = rally_select($db , $where);
		$rally = mysql_fetch_array($rally_date);
		$stamp_max = $rally['stamp_max'];
		$goal_day = $rally['goal_day'];
		db_close( $db );
		if ($current_stamp_num > $stamp_max) {
			// 最大数を超えたらリセットする
			$reset_stamp_num = $current_stamp_num - $stamp_max;
			$db = db_connect();
			$set = "stamp_num = ".$reset_stamp_num." , last_stamp_date = '".$add_date."'";
			$where = "user_id = ".$user_id." AND rally_id = ".$rally_id." ";
			rally_user_up($db , $set ,$where);
			db_close( $db );
			
		}
		
		// クーポンの自動取得
		$get_coupon_state = 0;
		$db = db_connect();
		$where = "rally_id = ".$rally_id;
		$goal_date = goal_select($db , $where);
		$goal = mysql_fetch_array($goal_date);
		db_close( $db );
		
		// スタンプ自動配布チェックの開始数 (今回付与したスタンプ数分ループする)
		$init_stamp_num = $total_stamp_num - $stamp_num + 1;
		for($i = $init_stamp_num; $i <= $total_stamp_num; $i++){
			// 押下されるスタンプ位置(MAXスタンプ数で割った余った数)
			$cur_stamp_num = ($total_stamp_num%$stamp_max);
			if ($cur_stamp_num == 0 && $total_stamp_num > 0) {
				$cur_stamp_num = $stamp_max;
			}
			
			for($coupon_no = 1; $coupon_no <= 10; $coupon_no++){
				$coupon_stamp_num = $goal['goal_stamp_num_'.$coupon_no];   //クーポンスタンプ数
				if ($coupon_stamp_num == 0) {
					// 未設定なので次の位置へ
					continue;
				}
				
				if($cur_stamp_num == $coupon_stamp_num){
					// クーポンを付与する位置のスタンプを通過しているので、当該のクーポンを発行する。
					$get_coupon_description = $goal['goal_description_'.$coupon_no];   //クーポン内容
					$get_coupon_img = $goal['img_name_'.$coupon_no];   //クーポン画像名
					$get_coupon_name = $goal['goal_title_'.$coupon_no];   //クーポン名

					$coupon_use_end =  date("Y-m-d H:i:s", strtotime("+".$goal_day." day"));// クーポン使用終了日
					$db = db_connect();
					$into = $rally_id." , ".$user_id." , ".$coupon_stamp_num." , '".$get_coupon_name."' , '".$get_coupon_description."' , '".$get_coupon_img."' , ".$get_coupon_state." , '".$add_date."' , '".$add_date."' , '".$add_date."' , '".$coupon_use_end."'";
					coupon_insert($db , $into);
					db_close( $db );
				}
			}
		}
	}
	
	// リダイレクト
	header("Location: ./?p=customer&user_id=".$user_id."&rally_id=".$rally_id);
	exit;
	
}

/**
 * ユーザ情報取得
 */
function get_userinfo($user_id, $rally_id) {
    $db = db_connect();
    $where = "a.user_id = ".$user_id." AND a.rally_id = ".$rally_id." AND b.user_id = ".$user_id;
    $detail_user_date = detail_user_select($db , $where);
    $detail_user = mysql_fetch_array($detail_user_date);
    db_close( $db );
    return $detail_user;
}

/**
 * ユーザのカルテ取得
 */
function get_karte($user_id, $rally_id) {
    $db = db_connect();
    $where = "user_id = ".$user_id." AND rally_id = ".$rally_id;
    $data = user_karte_select($db , $where);
    $detail_user = mysql_fetch_array($data);
    $karte = $detail_user['karte'];
    db_close( $db );
    return $karte;
}

/*
 * 顧客累計クーポン数
 */
function get_all_coupon($user_id , $rally_id){
	$db = db_connect();
	$where = "user_id = ".$user_id." AND rally_id = ".$rally_id;
	$get_coupon_date = get_coupon_count($db , $where);
	$get_coupon = mysql_fetch_array($get_coupon_date);
	$all_coupon = $get_coupon['count(*)'];
	db_close( $db );
	return $all_coupon;
}

/*
 * 顧客現在クーポン数
*/
function get_now_coupon($user_id , $rally_id){
	$db = db_connect();
	$where = "user_id = ".$user_id." AND get_coupon_state = 0 AND rally_id = ".$rally_id;
	$get_coupon_date = get_coupon_count($db , $where);
	$get_coupon = mysql_fetch_array($get_coupon_date);
	$now_coupon = $get_coupon['count(*)'];
	db_close( $db );
	return $now_coupon;
}

/*
 * スタンプ履歴の取得
 */
function get_stamp_history($user_id, $rally_id) {
	//スタンプ履歴
	$db = db_connect();
	$where = "user_id = ".$user_id." AND rally_id = ".$rally_id." ORDER BY stamp_history_id DESC";
	$stamp_history_date = stamp_history_select_date($db , $where);
        
        $stamp_history = array();  // 空配列宣言
	while ($row = mysql_fetch_array($stamp_history_date, MYSQL_ASSOC)){
            $stamp_history[] = $row;
	}
	db_close( $db );
        return $stamp_history;
}

/*
 * クーポン取得履歴の取得
 */
function get_got_coupon_history($user_id, $rally_id) {
    $db = db_connect();
    $where = "user_id = ".$user_id." AND rally_id = ".$rally_id."";
    $order = "get_coupon_id DESC";
    $data = get_coupon_select($db ,$where , $order);

    $got_coupon = array();
    while ($row = mysql_fetch_array($data)){
        $got_coupon[] = $row;
    }
    db_close( $db );
    return $got_coupon;
}

/*
 * クーポン消費履歴の取得
 */
function get_consumed_coupon_history($user_id, $rally_id) {
    $db = db_connect();
    $where = "user_id = ".$user_id." AND rally_id = ".$rally_id." AND get_coupon_state = 1";
    $order = "get_coupon_id DESC";
    $data = get_coupon_select($db ,$where , $order);

    $consumed_coupon = array();
    while ($row = mysql_fetch_array($data)){
        $consumed_coupon[] = $row;
    }
    db_close( $db );
    return $consumed_coupon;
}


/*
 * ラリー情報を取得
 */
function get_larry_information($rally_id){
	$db = db_connect();
	$where = "rally_id = ".$rally_id;
	$data = rally_select($db , $where);
	$rally = mysql_fetch_array($data);
	db_close( $db );
	return ($rally);
}

// Ajax処理
if ( isset($_POST['action'] )) {
    require_once('./../../../config.php');
    require_once('./../../ad_config.php');
    
    if ($_POST['action'] == "save_karte") {

        /**
         *  ユーザカルテの保存処理
         */
        // ユーザIDの取得
        $user_id = $_SESSION['asp']['customer_detail']['user_id'];
        $rally_id = $_SESSION['asp']['customer_detail']['rally_id'];
        // カルテ内容の取得
        $karte_value = $_POST['karte_value'];
        
        $db = db_connect();
        $set = "karte = '".mysql_real_escape_string($karte_value)."'" ;
        $where = "user_id = ".$user_id." AND rally_id = ".$rally_id;
        rally_user_up($db , $set , $where);
        db_close( $db );
        $ret_array = array(
                'user_id'	=> $user_id,
                'karte_value'	=> $karte_value,
        );
        echo json_encode($ret_array);
    }
    return;
}


// ユーザ情報取得
$detail_user = get_userinfo($user_id, $rally_id);

// 顧客の累計取得クーポン数取得
$all_coupon = get_all_coupon($user_id , $rally_id);

// 顧客の現在の所持クーポン数取得
$now_coupon = get_now_coupon($user_id , $rally_id);

// スタンプ履歴情報を取得
$stamp_history = get_stamp_history($user_id, $rally_id);

// クーポン取得履歴を取得
$got_coupon_history = get_got_coupon_history($user_id, $rally_id);

// クーポン使用履歴を取得
$consumed_coupon_history = get_consumed_coupon_history($user_id, $rally_id);

// 顧客詳細情報を表示
$_SESSION['asp']['customer_detail']['rally_id'] = $rally_id;
$_SESSION['asp']['customer_detail']['user_id'] = $user_id;
    
// カルテ情報の取得
$karte = get_karte($user_id, $rally_id);
    
// フルブラウザとスマホでビューを切り分ける
if(Util::is_smartphone ()) {
    require "./sp/page/customer_detail.php";
} else {
    require "./pc/page/customer_detail.php";
}

?>

