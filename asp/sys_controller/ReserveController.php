<?php
/**
 * 予約お問い合わせ画面表示コントローラー
 */

// モデルの読み込み
require_once('./../common/model/AdminModel.php');
$adminModel = new AdminModel();

// 予約と問い合わせのモデルは全店舗共通
require_once('./../common/model/ReserveModel.php');
$reserveModel = new ReserveModel();

require_once('./../common/model/InquiryModel.php');
$inquiryModel = new InquiryModel();

require_once('./../common/model/AlertSettingModel.php');
$alertSettingModel = new AlertSettingModel();



$db = db_connect();

// rally_id取得
$rally_id = $adminModel->get_rally_id($db, ADMIN_ID);

// 管理画面のスコープとしてのadmin_idを取得
$scope_admin_id = $adminModel->get_admin_limited_manage($db);
if (!$scope_admin_id) {
	echo 'Management scope error. id:'.$admin_id;
	return;
}

// 通知設定先を取得
$alert_setting = $alertSettingModel->find_by_rally_id($db, $rally_id, $scope_admin_id);

// タイプ別に予約や問い合わせのリストが変更される。
$admin_type = $adminModel->get_admin_type($db, $scope_admin_id);
if (($admin_type == OWNER)||($admin_type == BRANCHES_OWNER)) {
	// オーナーアカウント
	// オーナー直下の配下の支店リストを取得
	$branch_list = $adminModel->get_branch_by_owner_admin_id($db, $scope_admin_id);
	
	// rally配下の全履歴を取得
	$reserve_list = $reserveModel->find_by_rally_id($db, $rally_id);
	//var_dump($reserve_list);

	// rally配下の全履歴を取得
	$inquiry_list = $inquiryModel->find_by_rally_id($db, $rally_id);
	//var_dump($inquiry_list);
	
} else if ($admin_type == ORG_MANAGER) {
	// 組織アカウント
	
	// 組織配下の支店リストを取得
	$branch_list = $adminModel->get_branch_by_org_admin_id($db, $scope_admin_id);
	if ($branch_list) {
		$branch_ids = array_column($branch_list, 'id');
	}
	// 組織配下の全履歴を取得
	$reserve_list = $reserveModel->find_by_rally_id_and_branch_ids($db, $rally_id, $branch_ids);
	//var_dump($reserve_list);
	
	// 組織配下の全履歴を取得
	$inquiry_list = $inquiryModel->find_by_rally_id($db, $rally_id);
	
	
} else if ($admin_type == BRANCH_MANAGER) {
	// 支店アカウント
	// 
	// 支店IDを取得
	$branch = $adminModel->get_branch_by_admin_id($db, $scope_admin_id);
	$branch_list = [$branch['id']];
	
	// 組織配下の全履歴を取得
	$reserve_list = $reserveModel->find_by_rally_id_and_branch_ids($db, $rally_id, $branch_list);
	//var_dump($reserve_list);

	// 支店アカウント配下の全履歴を取得
	$inquiry_list = $inquiryModel->find_by_rally_id_and_branch_ids($db, $rally_id, $branch_list);
	
	
	
}

db_close( $db );

// 通知先設定の更新処理
if(isset($_POST['setting'])) {
	$db = db_connect();
	$entry_list= [];
	$errors = [];
	for ($index=1; $index < 5; $index++) {
		$account = isset($_POST['alert_setting_account_'.$index]) ? $_POST['alert_setting_account_'.$index] : '';
		if (!empty($account)) {
			if(!$reserveModel->check_val_is_mail($account) && !$reserveModel->check_val_is_user_id($account, USER_ID)) {
				$errors[] = $index.'番目の通知先は設定した形式が誤っています。';
			}
		}
		$setdata = [];
		$setdata['rally_id'] = $rally_id;
		$setdata['admin_id'] = $scope_admin_id;
		$setdata['no'] = $index;
		$setdata['account'] = isset($_POST['alert_setting_account_'.$index]) ? $_POST['alert_setting_account_'.$index] : '';
		$entry_list[] = $setdata;
	}
	
	if (count($errors) <= 0) {
		// １件づつ更新する
		foreach ($entry_list as $key => $setdata) {
			$alertSettingModel->save($db, $setdata);
		}
		db_close( $db );
		header("Location: ./?p=reserve");
		return;
	} else {
		// エラーがある場合、エラー文言を表示して入力値を再設定
		$alert_setting = $entry_list;
	}
	
}


if(Util::is_smartphone ()) {
	require './sp/page/reserve.php';
} else {
	require './pc/page/reserve.php';
}