<?php
/*
 * 過去のプレゼントクーポン表示
*/
function past_gift($admin_id){
	$db = db_connect();
	$where = "admin_id = ".$admin_id." AND first_flag = 1";
	$gift_coupon_date = gift_coupon_select($db , $where);
	while ($row = mysql_fetch_array($gift_coupon_date)){
		$gift_coupon_array[] = $row;
	}
	db_close( $db );
	return $gift_coupon_array;
}

/*
 * 使用済みプレゼントクーポン数
*/
function get_used_present_coupon($coupon_id, $rally_id){
	$db = db_connect();
	$where = "rally_id = ".$rally_id." AND get_coupon_state = 1 AND gift_check = 1 ";
	$data = get_coupon_count($db , $where);
	$get_coupon = mysql_fetch_array($data);
	$user_coupon_count = $get_coupon['count(*)'];
	db_close( $db );
	return $user_coupon_count;
}

//初回プレゼントクーポンカウント
function get_firstcoupon_count($db , $where){
	$sql = "SELECT count(*) as first_couopn from get_coupon WHERE ".$where;
	$result = mysql_query($sql,$db);
	return ($result);
}

/*
 * 変数(初期化など）
 */
$limit_coupon_name = isset($_POST['limit_coupon_name']) ? $_POST['limit_coupon_name'] : '';
$coupon_deadline_day = isset($_POST['coupon_deadline_day']) ? $_POST['coupon_deadline_day'] : '';
$coupon_isuue_flag = isset($_POST['issue_present_coupon_check']) ? $_POST['issue_present_coupon_check'] : 't';

if(isset($_GET['dele_id'])){
	$dele_id = $_GET['dele_id'];
	$db = db_connect();
	$where = "id = ".$dele_id;
	$gift_coupon_date = gift_coupon_delete($db , $where);
	db_close( $db );
}

if(isset($_POST['setting'])){  //クーポン発行
	$coupon_start = "0000-00-00 00:00:00";
	$coupon_end = "0000-00-00 00:00:00";
	$rally_date = Util::rally_information_get(ADMIN_ID);
	$rally_id = $rally_date['rally_id'];
	$coupon_way_date = 1;
	
	if (empty($coupon_deadline_day)) {
		$coupon_deadline_day = 0;
	} else {
		//有効期限を設定する時に全角数字で入力された場合、半角数字に変換する。
		$coupon_deadline_day = mb_convert_kana($coupon_deadline_day, "n");
	}
	
	// プレゼント(gift)クーポンの定義情報テーブルを作成
	$db = db_connect();
	$into = "'".$limit_coupon_name."' , '".$coupon_way_date."' , '".$coupon_start."' , '".$coupon_end."' , ".$coupon_deadline_day." , '".ADMIN_ID."' , '".$rally_id."' , '0' , '0' , '1' ,'".$coupon_isuue_flag."' ";
	first_gift_coupon_insert($db , $into);
	$present_coupon_id = mysql_insert_id();
	db_close($db);

	$gift_coupon_array = past_gift(ADMIN_ID);
	if(Util::is_smartphone ()) {
		require './sp/page/first_gift_end.php';
	} else {
		require './pc/page/first_gift_end.php';
	}
} else if(isset($_POST['check'])){  //確認ページ
	if(Util::is_smartphone ()) {
		require './sp/page/first_gift_check.php';
	} else {
		require './pc/page/first_gift_check.php';
	}
} else {
	$gift_coupon_array = past_gift(ADMIN_ID);
	if(Util::is_smartphone ()) {
		require './sp/page/first_gift_form.php';
	} else {
		require './pc/page/first_gift_form.php';
	}
}
