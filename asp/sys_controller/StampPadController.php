<?php
/*
 * スタンプ台設定（スマートフォン）
 */
if(isset($_POST['check'])){  //*******************************************確認ボタン*******************************************
	$stamp_max = $_POST['stamp_max'];
	for($i=1; $i<=$stamp_max; $i++){
		$img_before[$i] = $_POST['img_before_'.$i];
		$img_after[$i] = $_POST['img_after_'.$i];
	}
	for($i=1; $i<=$stamp_max; $i++){
		$num = mt_rand();
		if (is_uploaded_file($_FILES["before_".$i]["tmp_name"])) {
			if (move_uploaded_file($_FILES["before_".$i]["tmp_name"], "./../stamp_before/" . $i.$num.$_FILES["before_".$i]["name"])) {
				chmod("./../stamp_before/" . $i.$num.$_FILES["before_".$i]["name"], 0644);
				$img_before[$i] = $i.$num.$_FILES["before_".$i]["name"];
			} else {
				echo "ファイルをアップロードできません。";
			}
		}
	}
	for($i=1; $i<=$stamp_max; $i++){
		$num = mt_rand();
		if (is_uploaded_file($_FILES["after_".$i]["tmp_name"])) {
			if (move_uploaded_file($_FILES["after_".$i]["tmp_name"], "./../stamp_after/" . $i.$num.$_FILES["after_".$i]["name"])) {
				chmod("./../stamp_after/" . $i.$num.$_FILES["after_".$i]["name"], 0644);
				$img_after[$i] = $i.$num.$_FILES["after_".$i]["name"];
			} else {
				echo "ファイルをアップロードできません。";
			}
		}
	}
	require "./pc/page/stamp_pad_check.php";
} else if(isset($_POST['setting']) ){  //*******************************************設定ボタン*******************************************
	$stamp_max = $_POST['stamp_max'];  //スタンプ数
	$rally_id = $_POST['rally_id'];  //ラリーID
	$add_date = date('Y-m-d');  //登録日
	$db = db_connect();
	$set = 1;
	$where = "rally_id = ".$rally_id." AND is_stamp = 0";
	up_stamp($db , $set , $where);
	for($i=1; $i<=$stamp_max; $i++){
		$into = $rally_id." , ".$i." , 0 , '".$_POST['img_before_'.$i]."' , '".$_POST['img_after_'.$i]."' , '".$add_date."'";
		new_stamp_insert($db , $into);
	}
	db_close( $db );
	/*
	 * クーポン画像変更した時、rallyテーブルのrally_define_datetimeカラムの日時を更新する。
	 */
	$db = db_connect();
	$now = time();
	$now_datetime = date('Y-m-d H:i:s', $now); // 登録日時
	$set = "rally_define_datetime = '".$now_datetime."'";
	$where = "rally_id = ".$rally_id ;
	up_rally($db , $set , $where);
	db_close( $db );
	require "./pc/page/setting_end.php";
} else if(isset($_POST['return'])){  //*******************************************戻るボタン*******************************************
	require "./pc/page/stamp_pad_form.php";
} else {
	if(isset($_POST['display'])){
		$stamp_select = $_POST['stamp_select'];
	}
	require "./pc/page/stamp_pad_form.php";
}
?>
