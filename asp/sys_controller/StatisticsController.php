<?php
	// 月別の対象人数
	$db = db_connect();
	// 月別新規登録者数取得
	$where = "admin_id = '".ADMIN_ID."'";
	$data_list = new_user_count($db , $where);
	while ($row = mysql_fetch_array($data_list)){
		$new_user_list_by_month[] = $row;
	}
	db_close( $db );
	
	// ラリーid取得
	$db = db_connect();
	$rally_date = rally_select($db , "admin_id = ".ADMIN_ID);
	$rally = mysql_fetch_array($rally_date);
	$rally_id = $rally['rally_id'];
	db_close( $db );
	
	$admin_id = ADMIN_ID;
	$admin_list = ADMIN_ID;

	// 支店がある場合、支店のadmin_idを取得
	$db = db_connect();
	$admin_ids = getChildAdminIds($db, ADMIN_ID);
	foreach ($admin_ids as $id) {
		$admin_list .= ",".$id;
	}
	db_close( $db );

	$db = db_connect();
	// rally_idが一致するスタッフは対象
	$where = "rally_id = ".$rally_id." AND status = '1'";
	$staff_data = staff_select($db , $where);
	$staff_list = array();
	while ($staff = mysql_fetch_array($staff_data)){
		$admin_list .= ",".$staff['admin_id'];
	}
	db_close( $db );
	
	// 再来店人数
	$statistics = array();
	$subtotal = 0;
	$unaccess = 0;
	$unaccess_next = 0;
	$last_month = 0;
	$last_2month = 0;
	$last_3month = 0;
	$current_month = 0;
	foreach ($new_user_list_by_month as $value) {
		
		// 3か月前の登録人数
		$last_3month = $last_2month;
		// 2か月前の登録人数
		$last_2month = $last_month;
		// 1か月前の登録人数
		$last_month = $current_month;
		// 
		$current_month = $value['count'];
		
		// 先月の登録者数
		$value['target_num1'] = $last_month;
		// 先月と先々月の登録者数(先月の総数から先月分を引く)
		$value['target_num2'] = $value['target_num1'] + $last_2month;
		// 先月と先々月の3か月前の登録者数
		$value['target_num3'] = $value['target_num2'] + $last_3month;
		
		// 月毎の来店人数を算出
		$month = $value['add_month'];
		
		// 当月の総ユーザ数
		$subtotal += $value['count'];
		$value['subtotal'] = $subtotal;
		
		$db = db_connect();
		
		// 指定した期間の間に新規登録したユーザで$monthに指定した月にスタンプ付与したユーザを洗い出す
		list($year, $tmp_month) = explode('-', $month);

		// 指定月の先月
		$start_month = date('Y-m', strtotime(date($year.'-'.$tmp_month.'-1') . '-1 month'));
		$end_month = date('Y-m', strtotime(date($year.'-'.$tmp_month.'-1') . '-1 month'));
		
		$user_stamp_data = get_coming_user($db , $rally_id, $month ,$start_month , $end_month);
		$value['repeat_num1'] = 0;

		// 同日のスタンプ処理は１回の来店とみなす
		$user_array1 = array();
		$user_id = 0;
		while ($row = mysql_fetch_array($user_stamp_data)){
			if ($row['user_id'] != $user_id) {
				// ユーザIDが同じでないならuserの初回処理
				$user_id = $row['user_id'];
				$user_array1[] = $user_id;
				if ($row['user_add_month'] == $month) {
					// 同月に登録している場合、初回来店はカウントしない
				} else {
					$value['repeat_num1'] += 1;
				}
			}else {
				// 今月、別日で２回目の来店は無条件にカウントアップ
				$value['repeat_num1'] += 1;
			}
		}
		
		//
		// 指定月の先月
		$start_month = date('Y-m', strtotime(date($year.'-'.$tmp_month.'-1') . '-2 month'));
		
		$user_stamp_data = get_coming_user($db , $rally_id, $month ,$start_month , $end_month);
		$value['repeat_num2'] = 0;

		// 同日のスタンプ処理は１回の来店とみなす
		$user_array2 = array();
		$user_id = 0;
		while ($row = mysql_fetch_array($user_stamp_data)){
			if ($row['user_id'] != $user_id) {
				// ユーザIDが同じでないならuserの初回処理
				$user_id = $row['user_id'];
				$user_array2[] = $user_id;
				if ($row['user_add_month'] == $month) {
					// 同月に登録している場合、初回来店はカウントしない
				} else {
					$value['repeat_num2'] += 1;
				}
			}else {
				// 今月、別日で２回目の来店は無条件にカウントアップ
				$value['repeat_num2'] += 1;
			}
		}
		
		
		//
		// 先月、先々月、3か月前から
		$start_month = date('Y-m', strtotime(date($year.'-'.$tmp_month.'-1') . '-3 month'));
		
		$user_stamp_data = get_coming_user($db , $rally_id, $month ,$start_month , $end_month);
		$value['repeat_num3'] = 0;
		
		// 同日のスタンプ処理は１回の来店とみなす
		$user_array3 = array();
		$user_id = 0;
		while ($row = mysql_fetch_array($user_stamp_data)){
			if ($row['user_id'] != $user_id) {
				// ユーザIDが同じでないならuserの初回処理
				$user_id = $row['user_id'];
				$user_array3[] = $user_id;
				if ($row['user_add_month'] == $month) {
					// 同月に登録している場合、初回来店はカウントしない
				} else {
					$value['repeat_num3'] += 1;
				}
			}else {
				// 今月、別日で２回目の来店は無条件にカウントアップ
				$value['repeat_num3'] += 1;
			}
		}
		
		
		// お知らせ配信回数
		$user_array = null;
		$notice_data = notice_count_by_month($db, $admin_list, $month, $user_array);
		$value['notice_num'] = 0;
		$value['read_count'] = 0;
		$value['notice_total_count'] = 0;
		while ($row = mysql_fetch_array($notice_data)){
			if ($row['notice_month'] == $month) {
				$value['notice_num']++;
				$value['notice_total_count'] += $row['count'];
				$value['read_count'] += $row['read_count'];
			}
		}
		
		// お知らせ配信回数
		$notice_data = notice_count_by_month($db, $admin_list, $month, $user_array1);
		$value['notice_num1'] = 0;
		$value['notice_total_count1'] = 0;
		$value['read_count1'] = 0;
		while ($row = mysql_fetch_array($notice_data)){
			if ($row['notice_month'] == $month) {
				$value['notice_num1']++;
				$value['notice_total_count1'] += $row['count'];
				$value['read_count1'] += $row['read_count'];
			}
		}
		
		// お知らせ配信回数
		$notice_data = notice_count_by_month($db, $admin_list, $month, $user_array2);
		$value['notice_num2'] = 0;
		$value['notice_total_count2'] = 0;
		$value['read_count2'] = 0;
		while ($row = mysql_fetch_array($notice_data)){
			if ($row['notice_month'] == $month) {
				$value['notice_num2']++;
				$value['notice_total_count2'] += $row['count'];
				$value['read_count2'] += $row['read_count'];
			}
		}
		
		// お知らせ配信回数
		$notice_data = notice_count_by_month($db, $admin_list, $month, $user_array3);
		$value['notice_num3'] = 0;
		$value['notice_total_count3'] = 0;
		$value['read_count3'] = 0;
		while ($row = mysql_fetch_array($notice_data)){
			if ($row['notice_month'] == $month) {
				$value['notice_num3']++;
				$value['notice_total_count3'] += $row['count'];
				$value['read_count3'] += $row['read_count'];
			}
		}
		
		// プレゼント発行回数
		$present_data = present_count_by_month($db , $admin_list, $month);
		while ($row = mysql_fetch_array($present_data)){
			if ($row['present_month'] == $month) {
				$value['present_num'] = $row['count'];
			}
		}
		
		// リピート率
		$value['repeat_percent1'] = round(($value['repeat_num1']/$value['target_num1'])*100 ,1);
		$value['repeat_percent2'] = round(($value['repeat_num2']/$value['target_num2'])*100 ,1);
		$value['repeat_percent3'] = round(($value['repeat_num3']/$value['target_num3'])*100 ,1);
		
		$statistics[] = $value;
		db_close( $db );
	}
	$new_user_list_by_month = $statistics;
	
	
	
	require "./pc/page/statistics.php";
