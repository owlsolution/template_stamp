<?php

//店舗画像削除チェック
$img_sp_1_delete = $_POST['img_sp_1_delete'];
$img_sp_2_delete = $_POST['img_sp_2_delete'];
$img_sp_3_delete = $_POST['img_sp_3_delete'];
$img_sp_4_delete = $_POST['img_sp_4_delete'];
$img_sp_5_delete = $_POST['img_sp_5_delete'];

/*
 * 店舗画像設定
 */
if(isset($_POST['check'])){
	for($i=1; $i<=5; $i++){
		$img_after[$i] = $_POST["img_sp_".$i];
		$num = mt_rand();
		if (is_uploaded_file($_FILES["img_sp_".$i]["tmp_name"])) {
			if (move_uploaded_file($_FILES["img_sp_".$i]["tmp_name"], "./../store_images/" . $i.$num.$_FILES["img_sp_".$i]["name"])) {
				chmod("./../store_images/" . $i.$num.$_FILES["img_sp_".$i]["name"], 0644);
				$img_after[$i] = $i.$num.$_FILES["img_sp_".$i]["name"];
			} else {
				echo "ファイルをアップロードできません。";
			}
		}
	}
	if(isset($_POST['pattern'])){
		$store_name = $_POST['store_name'];
		$store_id = $_POST['store_id'];
		$_POST['store_select'] = $_POST['store_no'];
		$pattern = $_POST['pattern'];
	} else {
		$db = db_connect();
		$where = "admin_id = ".ADMIN_ID." AND store_no = ".$_POST['store_select'];
		$order = "store_no ASC";
		$store_date = store_select($db , $where ,$order);
		$store = mysql_fetch_array($store_date);
		$store_name = $store['store_name'];
		$store_id = $store['store_id'];
	}
	require "./pc/page/store_img_check.php";
} else if(isset($_POST['setting'])){  //登録ボタンの処理
	if($_POST['pattern'] == ""){  //新規追加の場合
		$rally_id = 0;  //ラリーID
		$admin_id = ADMIN_ID;  //adminID
//		$store_id = $_POST['store_id'];  //店舗ID
		$store_id = 0;  //店舗ID
//		$store_no = $_POST['store_no'];  //店舗ナンバー
		$store_no = 0;  //店舗ナンバー
//		$store_name = $_POST['store_name'];//店舗名
		$store_name = "";
		$img_sp_1 = $_POST['img_sp_1'];  //画像１
		$img_sp_2 = $_POST['img_sp_2'];  //画像２
		$img_sp_3 = $_POST['img_sp_3'];  //画像３
		$img_sp_4 = $_POST['img_sp_4'];  //画像４
		$img_sp_5 = $_POST['img_sp_5'];  //画像５
		$add_date = date('Y-m-d');  //作成日
		$db = db_connect();
		$into = $rally_id." , ".$admin_id." , ".$store_id." , ".$store_no." , '".$store_name."' , '".$img_sp_1."' , '".$img_sp_2."' , '".$img_sp_3."' , '".$img_sp_4."' , '".$img_sp_5."' , '".$add_date."'";
		store_img_insert($db , $into);
		db_close($db);
		/*
		 * クーポン画像変更した時、rallyテーブルのrally_define_datetimeカラムの日時を更新する。
		 */
		$db = db_connect();
		$now = time();
		$now_datetime = date('Y-m-d H:i:s', $now); // 登録日時
		$set = "rally_define_datetime = '".$now_datetime."'";
		$where = "admin_id = ".ADMIN_ID;
		up_rally($db , $set , $where);
		db_close( $db );
		require "./pc/page/new_setting_end.php";
	} else {  //編集の場合
		$rally_id = $_POST['rally_id'];  //ラリーID
		$admin_id = ADMIN_ID;  //adminID
		//$store_id = $_POST['store_id'];  //店舗ID
		$store_id = 0;  //店舗ID
//		$store_no = $_POST['store_no'];  //店舗ナンバー
		$store_no = 0;  //店舗ナンバー
//		$store_name = $_POST['store_name'];//店舗名
		$store_name = "";//店舗名
		$img_sp_1 = $_POST['img_sp_1'];  //画像１
		$img_sp_2 = $_POST['img_sp_2'];  //画像２
		$img_sp_3 = $_POST['img_sp_3'];  //画像３
		$img_sp_4 = $_POST['img_sp_4'];  //画像４
		$img_sp_5 = $_POST['img_sp_5'];  //画像５
		//画像削除
		if(!empty($img_sp_1_delete)){
			$img_sp_1 = "";
		}
		if (!empty($img_sp_2_delete)){
			$img_sp_2 = "";
		}
		if (!empty($img_sp_3_delete)){
			$img_sp_3 = "";
		}
		if (!empty($img_sp_4_delete)){
			$img_sp_4 = "";
		}
		if (!empty($img_sp_5_delete)){
			$img_sp_5 = "";
		}
		$db = db_connect();
		$set = "img_sp_1 = '".$img_sp_1."' , img_sp_2 = '".$img_sp_2."' , img_sp_3 = '".$img_sp_3."' , img_sp_4 = '".$img_sp_4."' , img_sp_5 = '".$img_sp_5."'";
		$where = "admin_id = ".$admin_id." AND store_no = ".$store_no." AND rally_id = 0";
		store_img_up($db , $set , $where);
		db_close($db);
		/*
		 * クーポン画像変更した時、rallyテーブルのrally_define_datetimeカラムの日時を更新する。
		 */
		$db = db_connect();
		$now = time();
		$now_datetime = date('Y-m-d H:i:s', $now); // 登録日時
		$set = "rally_define_datetime = '".$now_datetime."'";
		$where = "admin_id = ".ADMIN_ID;
		up_rally($db , $set , $where);
		db_close( $db );
		require "./pc/page/setting_end.php";
	}
} else if(isset($_GET['edit'])){
	$store_img_id = $_GET['edit'];
	require "./pc/page/store_img_edit.php";
} else {
if(isset($_POST['display'])){
	$stamp_select = $_POST['stamp_select'];
}
	require "./pc/page/store_img_form.php";
}
