<?php
/*
 * 支店機能向けプレゼントクーポン表示
*/

require_once('./../common/model/AdminModel.php');
$adminModel = new AdminModel();

/**
 * DBアクセス結果の情報を取得する
 * @param type $data_rows
 * @return array
 */
function get_dbdata_array($data_rows) {
        $data_list = array();  // 空配列宣言
        while ($row = mysql_fetch_array($data_rows, MYSQL_ASSOC)){
				$dateParts = Util::get_rally_of_day($row['add_date2'], $row['rally_id']);
				// add_dateを現時点での日付に変換
				$row['add_date'] = $dateParts[0]."-".$dateParts[1]."-".$dateParts[2];
                $data_list[] = $row;
        }
        return $data_list;
}

function postJson($url, $post) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
}

/**
 * PUSHのキャンセル要求
 * @param type $gift_id 
 */
function cancel_push ($gift_id) {
        $post = array(
                "mode"=>"gift",
                "notice_id" => $gift_id,
        );
        
        // JSONを文字列に変換
        $post = json_encode($post);
        $url = PUSH_SERVER."?p=cancel";
        $response = postJson($url, $post);
        
        return json_decode($response, true);
}

/*
 * 過去のプレゼントクーポン表示
 * admin_idはカンマ区切りで複数指定可能
*/
function past_gift($admin_id){
	$db = db_connect();
	
	$where = "AND gift_c.admin_id IN (".$admin_id.") ";
	$gift_coupon_date = gift_coupon_select_with_summary($db , $where);
	while ($row = mysql_fetch_array($gift_coupon_date)){
		$gift_coupon_array[] = $row;
	}
	db_close( $db );
	return $gift_coupon_array;
}

/*
 * 使用済みプレゼントクーポン数
*/
function get_used_present_coupon($coupon_id, $rally_id){
	$db = db_connect();
	$where = "rally_id = ".$rally_id." AND get_coupon_state = 1 AND gift_check = 1 ";
	$data = get_coupon_count($db , $where);
	$get_coupon = mysql_fetch_array($data);
	$user_coupon_count = $get_coupon['count(*)'];
	db_close( $db );
	return $user_coupon_count;
}

/**
 * プレゼントクーポン発行時にPUSH配信するかどうか判断
 * @param type $admin_id
 * @return type
 */
function isPresentPush($admin_id) {
	$db = db_connect();
	$where = "admin_id = ".$admin_id." ";
	$data = admin_select_date($db , $where);
	$row = mysql_fetch_array($data);
	$present_push = $row['present_push'];
	db_close( $db );
	return $present_push;
}

/*
 * 変数(初期化など）
 */
$limit_coupon_name = isset($_POST['limit_coupon_name']) ? $_POST['limit_coupon_name'] : '';
$coupon_way = isset($_POST['coupon_way']) ? $_POST['coupon_way'] : '';
$coupon_year_start = isset($_POST['coupon_year_start']) ? $_POST['coupon_year_start'] : '';
$coupon_month_start = isset($_POST['coupon_month_start']) ? $_POST['coupon_month_start'] : '';
$coupon_day_start = isset($_POST['coupon_day_start']) ? $_POST['coupon_day_start'] : '';
$coupon_hour_start = isset($_POST['coupon_hour_start']) ? $_POST['coupon_hour_start'] : '';
$coupon_minute_start = isset($_POST['coupon_minute_start']) ? $_POST['coupon_minute_start'] : '';
$coupon_year_end = isset($_POST['coupon_year_end']) ? $_POST['coupon_year_end'] : '';
$coupon_month_end = isset($_POST['coupon_month_end']) ? $_POST['coupon_month_end'] : '';
$coupon_day_end = isset($_POST['coupon_day_end']) ? $_POST['coupon_day_end'] : '';
$coupon_hour_end = isset($_POST['coupon_hour_end']) ? $_POST['coupon_hour_end'] : '';

// ブラウザから入力情報を取得
$stamp_select = isset($_POST['rally_id']) ? $_POST['rally_id'] : '';
$notice_title = isset($_POST['notice_title']) ? $_POST['notice_title'] : '';
$notice_date = isset($_POST['notice_date']) ? $_POST['notice_date'] : '';
$delivery_set = isset($_POST['delivery_set']) ? $_POST['delivery_set'] : '';
$delivery_num = isset($_POST['delivery_num']) ? $_POST['delivery_num'] : '';
$simple_check = isset($_POST['simple_check']) ? $_POST['simple_check'] : '';
$individual_sex = isset($_POST['individual_sex']) ? $_POST['individual_sex'] : '';
$individual_birthday_start = isset($_POST['individual_birthday_start']) ? $_POST['individual_birthday_start'] : '';
$individual_birthday_end = isset($_POST['individual_birthday_end']) ? $_POST['individual_birthday_end'] : '';
$individual_region = isset($_POST['individual_region']) ? $_POST['individual_region'] : '';
$first_year_start = isset($_POST['first_year_start']) ? $_POST['first_year_start'] : '';
$first_month_start = isset($_POST['first_month_start']) ? $_POST['first_month_start'] : '';
$first_day_start = isset($_POST['first_day_start']) ? $_POST['first_day_start'] : '';
$first_start = isset($_POST['first_start']) ? $_POST['first_start'] : '';
$first_year_end = isset($_POST['first_year_end']) ? $_POST['first_year_end'] : '';
$first_month_end = isset($_POST['first_month_end']) ? $_POST['first_month_end'] : '';
$first_day_end = isset($_POST['first_day_end']) ? $_POST['first_day_end'] : '';
$first_end = isset($_POST['first_end']) ? $_POST['first_end'] : '';
$last_year_start = isset($_POST['last_year_start']) ? $_POST['last_year_start'] : '';
$last_month_start = isset($_POST['last_month_start']) ? $_POST['last_month_start'] : '';
$last_day_start = isset($_POST['last_day_start']) ? $_POST['last_day_start'] : '';
$last_start = isset($_POST['last_start']) ? $_POST['last_start'] : '';
$last_year_end = isset($_POST['last_year_end']) ? $_POST['last_year_end'] : '';
$last_month_end = isset($_POST['last_month_end']) ? $_POST['last_month_end'] : '';
$last_day_end = isset($_POST['last_day_end']) ? $_POST['last_day_end'] : '';
$last_end = isset($_POST['last_end']) ? $_POST['last_end'] : '';
$total_stamp_num = isset($_POST['total_stamp_num']) ? $_POST['total_stamp_num'] : '';
$total_stamp_terms = isset($_POST['total_stamp_terms']) ? $_POST['total_stamp_terms'] : '';
$rally_id = $_GET['rally_id'];
//プレゼントクーポン画像名
$present_img = $_POST['present_up_img'];
//プレゼントクーポン画像設定で設定していない場合、クーポン設定で設定しているクーポン画像名を渡す
if(isset($present_img)){
	$present_up_img = $_POST['present_up_img'];
} else {
	$db = db_connect();
	$where = "admin_id = ".ADMIN_ID;
	$rally_date = rally_select($db , $where);
	$rally = mysql_fetch_array($rally_date);
	$where = "rally_id = ".$rally['rally_id'];
	$goal_date = goal_select($db , $where);
	$goal = mysql_fetch_array($goal_date);
	db_close( $db );
	$present_up_img = $goal['present_img_name'];
}
$coupon_isuue_flag = $_POST['issue_present_coupon_check'];
//プッシュ通知ON OFF
$push_check = isset($_POST['push_check']) ? $_POST['push_check'] : '';
//チェックページの処理
$check_id = isset($_GET['check_id']) ? $_GET['check_id'] : '';
// $_GET['to_user_id']がある場合、個人宛にプレゼントクーポンを配布するので、専用表示する
$to_user_id = isset($_GET['to_user_id']) ? $_GET['to_user_id'] : '';
// 支店権限の状態から検索条件で必要なadmin_idを取得する

$db = db_connect();
// 支店機能 通常/オーナーはADMIN_ID ,支店権限の場合は支店のadmin_idで検索
if ($_SESSION["branchFlag"] == OWNER) {
	$present_admin_id = ADMIN_ID;
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, ADMIN_ID, OWNER);
} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] == 0)){
	$present_admin_id = ADMIN_ID;
	// 全支店管理アカウントで全支店管理アカウントモードの場合
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, ADMIN_ID, BRANCHES_OWNER);
} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] != 0)){
	// 全支店管理アカウントで全支店アカウントを選択時は選択した支店管理アカウントに限定した機能
	$present_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	// 選択中の支店管理アカウントの配下のスタッフのadmin_idを取得する
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $present_admin_id, BRANCH_MANAGER);
} else if ($_SESSION["branchFlag"] == BRANCH_MANAGER) {
	// 支店管理アカウントでログインしている場合
	$present_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	// 支店管理アカウントにブラ下がるスタッフを取得
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $present_admin_id, BRANCHES_OWNER);
} else if (($_SESSION["branchFlag"] == ORG_MANAGER) && ($_SESSION["branchId"] != 0)){
	$branch_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	$present_admin_id = $branch_admin_id;
	// 選択中の支店管理アカウントの配下のスタッフのadmin_idを取得する
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $branch_admin_id, BRANCH_MANAGER);
} else if ($_SESSION["branchFlag"] == ORG_MANAGER) {
	// 組織管理アカウントなので、組織IDを元に組織にぶら下がる支店のadmin_idを取ってくる
	$org = $adminModel->get_organization_by_id($db, $_SESSION["orgId"]);
	// 組織IDからadmin_idを取得
	$present_admin_id = $org['admin_id'];
	// admin_idとタイプを渡して、ぶら下がるadmin_idのリストを返却するメソッドを呼出す
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $org['admin_id'], ORG_MANAGER);
} else if ($_SESSION["branchFlag"] == STAFF) {
	// スタッフのadmin_idを設定
	$staff = $adminModel->get_staff_by_id($db, $_SESSION["staffId"]);
	$present_admin_id = $staff['admin_id'];
	$admin_id_list[] = $staff['admin_id'];
}
$present_admin_ids = implode(',', $admin_id_list);
db_close( $db );


//$present_admin_id = ADMIN_ID;
// ポストしたformがどれかを判断可能
// 'gift':プレゼントクーポン発行
// 'to_person':個人宛
$delivery_type = isset($_POST['delivery_type']) ? $_POST['delivery_type'] : '';
//$db = db_connect();
//if ($_SESSION["branchFlag"] == 2) {
//    // 支店でログインしている場合
//    $present_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
//} else if ($_SESSION["branchFlag"] == 1) {
//    // オーナーの場合
//    if ($_SESSION["branchId"] != 0) {
//        $present_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
//    }
//    else {
//        // 支店で送信したプレゼントクーポンも対象に入れる
//        $branch_ids = getChildAdminIds($db, ADMIN_ID);
//        foreach ($branch_ids as $value) {
//            $present_admin_ids .= ",".$value;
//        }
//    }
//}
//db_close( $db );

// 発行対象の人数取得
$db = db_connect();

$branch_ids = null;
if ($_SESSION["branchFlag"] == OWNER) {
	// 全員
} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] == 0)){
	// 全員
} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] != 0)){
	// 単一ブランチ
	$branch_ids = $_SESSION["branchId"];
} else if ($_SESSION["branchFlag"] == BRANCH_MANAGER) {
	// 単一ブランチ
	$branch_ids = $_SESSION["branchId"];
} else if (($_SESSION["branchFlag"] == ORG_MANAGER) && ($_SESSION["branchId"] != 0)){
	// 単一ブランチ
	$branch_ids = $_SESSION["branchId"];
} else if ($_SESSION["branchFlag"] == ORG_MANAGER) {
	// 組織IDから対象のブランチidを取得する
	$branch_list = $adminModel->get_branch_by_organization_id($db, $_SESSION["orgId"]);
	$branch_id_list = array_column($branch_list, 'id');
	$branch_ids = implode(",", $branch_id_list);
} else if ($_SESSION["branchFlag"] == STAFF) {
	// スタッフが所属する支店の支店IDを取得する
	// 現状未対応
}

// 支店IDを保持している場合は支店内のユーザが対象になる
$where = " u.user_id = r.user_id AND r.admin_id = ".ADMIN_ID;
if (!empty($branch_ids)) {
	// 対象の支店IDリストがあるものは、支店IDを条件に追加する
	$where .= " AND r.branch_id IN( ".$branch_ids.")";
}

$rally_user_date = all_user_information_select($db , $where);
$rally_user_rows = mysql_num_rows($rally_user_date);
$all_customer = $rally_user_rows;

// ラリーユーザデータを取得
$user_list = get_dbdata_array($rally_user_date);
db_close( $db );

// -------------------------
// 現在店舗が発行しているプレゼントクーポンの総数を取得
// -------------------------
// 支店で送信したプレゼントクーポンも対象に入れる
//$all_ids = ADMIN_ID;
//$db = db_connect();
//$all_branch_ids = getChildAdminIds($db, ADMIN_ID);
//foreach ($all_branch_ids as $value) {
//    $all_ids .= ",".$value;
//}
//db_close( $db );
$gift_coupon_array = past_gift($present_admin_ids);
$all_gift_coupon_num = count($gift_coupon_array);
// -------------------------

//if (empty($present_admin_ids)) {
//    $present_admin_ids = $present_admin_id;
//} else {
//    // オーナーの場合、複数の支店が取得されるので、表示対象に入れる
//    $present_admin_ids = $present_admin_id.$present_admin_ids;
//}

if(isset($_GET['dele_id'])){
	$dele_id = $_GET['dele_id'];
	$rally_date = Util::rally_information_get(ADMIN_ID);

	// 本日の最初の時間を取得
	$date_start_time = Util::get_rally_start_time_of_day(date("Y-m-d H:i:s"), $rally_date['rally_id']);
	$db = db_connect();
	//削除対象のクーポンの内、期限が切れていないクーポンを削除する
	$where_get_coupon = "coupon_id = '".$dele_id."' AND coupon_use_end >= '".$date_start_time."' AND gift_check = '1'";
	$user_gift_coupon_delete = get_coupon_update_to_deletion_state($db, $where_get_coupon);
	//gift_couponテーブル上のクーポンを削除する
	$where_gift_coupon = "id = ".$dele_id;
	$gift_coupon_date = gift_coupon_delete($db , $where_gift_coupon);
	db_close( $db );
	
	// PUSHサーバ上の未処理のPUSHはキャンセル要求する
	cancel_push($dele_id);
}

if(!empty($check_id)){
	$db = db_connect();
	$where = "id = ".$check_id;
	$gift_coupon_date = gift_coupon_select($db , $where);
	$gift_row = mysql_fetch_array($gift_coupon_date);
	db_close( $db );
	if($gift_row['delivery_set'] == 1){
		$delivery_set_name = "一斉配信";
		$delivery_set = "";
	} else if($gift_row['delivery_set'] == 2){
		$delivery_set_name = "簡単配信";
		$delivery_set = "simple";
		if($gift_row['simple_last_month'] == 1){
			$simple_check[] = "last_month";
		}
		if($gift_row['simple_this_month'] == 1){
			$simple_check[] = "this_month";
		}
		if($gift_row['simple_next_month'] == 1){
			$simple_check[] = "next_month";
		}
		if($gift_row['simple_sex'] == 1){
			$simple_check[] = "man";
			$simple_check[] = "woman";
		}
		if($gift_row['simple_sex'] == 2){
			$simple_check[] = "man";
		}
		if($gift_row['simple_sex'] == 3){
			$simple_check[] = "woman";
		}
		if($gift_row['yesterday_come'] == 1){
			$simple_check[] = "yesterday_come";
		}
		if($gift_row['month_come'] == 1){
			$simple_check[] = "month_come";
		}
		if($gift_row['this_month_come'] == 1){
			$simple_check[] = "this_month_come";
		}
	} else if($gift_row['delivery_set'] == 3){
		$delivery_set_name = "個別配信";
		$delivery_set = "individual";
		switch ($gift_row['individual_sex']) {
			case 1;//全て
				$individual_sex_date = "全て";
				break;
			case 2; //男性
				$individual_sex_date = "男性";
				break;
			case 3;  //女性
				$individual_sex_date = "女性";
				break;
			default;
				break;
		}
		$individual_birthday_start = $gift_row['individual_birthday_start'];
		$individual_birthday_end = $gift_row['individual_birthday_end'];
		$individual_region = $gift_row['individual_region'];
		if(empty($individual_region)){
			$individual_region = "全ての都道府県";
		}
		//最終来店日
		$last_start_array = explode("-", $gift_row['last_start']);
		$last_year_start = $last_start_array[0];
		$last_month_start = $last_start_array[1];
		$last_day_start = $last_start_array[2];
		$last_end_array = explode("-", $gift_row['last_end']);
		$last_year_end = $last_end_array[0];
		$last_month_end = $last_end_array[1];
		$last_day_end = $last_end_array[2];
		$total_stamp_num = $gift_row['total_stamp_num'];
		if($gift_row['total_stamp_terms'] == "1"){
			$total_stamp_terms = "or_more";
		} else if($gift_row['total_stamp_terms'] == "2"){
			$total_stamp_terms = "downward";
		}
		//プッシュ通知チェック
		$push_check = $gift_row['push_check'];
		
	} else if($gift_row['delivery_set'] == 5){
        $delivery_set_name = "個人宛配信";  //個人宛配信
        $delivery_set = "to_person";
	}
	//プレゼントクーポン：有効期限設定
    $coupon_start = explode(" ", $gift_row['coupon_start']);
    $coupon_start_date = explode("-" , $coupon_start[0]);
    $coupon_year_start = $coupon_start_date[0];
    $coupon_month_start = $coupon_start_date[1];
    $coupon_day_start = $coupon_start_date[2];
    $coupon_start_time = explode(":" , $coupon_start[1]);
    $coupon_hour_start = $coupon_start_time[0];
    $coupon_minute_start = $coupon_start_time[1];
    $coupon_end = explode(" ", $gift_row['coupon_end']);
    $coupon_end_date = explode("-", $coupon_end[0]);
    $coupon_year_end = $coupon_end_date[0];
    $coupon_month_end = $coupon_end_date[1];
    $coupon_day_end = $coupon_end_date[2];
    $coupon_end_time = explode(":", $coupon_end[1]);
    $coupon_hour_end = $coupon_end_time[0];
	//配信人数
	$delivery_num = $gift_row['delivery_num'];
	//プレゼントクーポン名
	$limit_coupon_name = $gift_row['coupon_name'];
//	$gift_coupon_array = past_gift($present_admin_ids);
	$coupon_isuue_flag = $gift_row['disp_type'];
	$present_up_img = $gift_row['coupon_image_name'];

	if(Util::is_smartphone ()) {
		require './sp/page/gift_check.php';
	} else {
		require './pc/page/gift_check.php';
	}
	exit;
}
if (isset($_SESSION['asp']['gift']['transition']) && $_SESSION['asp']['gift']['transition'] == 'complete') {
    // セッション情報が入力されていて、送信状態が'complete'の場合
    // ユーザによる中断を無視する
    ignore_user_abort(true);
    // タイムアウトしないようにする
    set_time_limit(0);

    $p_admin_id = $_SESSION['userName'];
    $subject = $_SESSION['asp']['gift']['data']['subject'];
    $db = db_connect();
    $where = "admin_id = ".$p_admin_id;
    $rally_select_date = rally_select($db , $where);
    $rally_select = mysql_fetch_array($rally_select_date);
    $rally_name = $rally_select['rally_name'];
    db_close($db);

//    // 支店IDが存在して、且つ、支店権限で実行する場合
//    if ((isset($_SESSION["branchId"]) && $_SESSION["branchId"] != 0)) {
//        // 支店IDが存在して、且つ、支店権限で実行する場合
//			Util::gift_message($p_admin_id, $_SESSION['asp']['gift']['data'],  $_SERVER['REQUEST_URI'] ,$rally_name ,$present_admin_id,$_SESSION["branchId"]);
//    } else {
//			Util::gift_message($p_admin_id, $_SESSION['asp']['gift']['data'],  $_SERVER['REQUEST_URI'] ,$rally_name ,$present_admin_id);
//    }
	
	if (empty($branch_ids)) {
		// 支店IDを保持している場合、対象を支店IDで絞り込む必要がある
		Util::gift_message($p_admin_id, $_SESSION['asp']['gift']['data'], $_SERVER['REQUEST_URI'] , $rally_name , $present_admin_id);
	} else {
		Util::gift_message($p_admin_id, $_SESSION['asp']['gift']['data'], $_SERVER['REQUEST_URI'] , $rally_name , $present_admin_id, $branch_ids);
	}

	$gift_coupon_array = past_gift($present_admin_ids);
	
    unset($_SESSION['asp']['gift']);
    if(Util::is_smartphone ()) {
        require "./sp/page/gift_end.php";
    } else {
        require "./pc/page/gift_end.php";
    }
}else if(isset($_POST['setting'])){  //クーポン発行
    // 確認画面にてプレゼントクーポンを送信
    if (isset($_SESSION['asp']['gift']['transition']) && $_SESSION['asp']['gift']['transition'] != 'check') {
        header('Location: '.$_SERVER['REQUEST_URI']);
        exit;
    }
        /**
         *
         * $limit_coupon_name          プレゼントクーポン名
         * ADMIN_ID                    adminID
         * $delivery_set               プレゼントクーポン配信設定 (all:一斉配信 , simple:簡単配信 , individual:個別配信 , to_person:個人宛配信)
         * $simple_check               簡単配信条件 ([配列] last_month:先月誕生日の人 , this_month:今月誕生日の人 , next_month:来月誕生日の人 , man:男性 , woman:女性 , yesterday_come:昨日来店した人 , month_come:1ヶ月以上来店してないの人 , this_month_come:今月以上来店してないの人)
         * $individual_sex             性別
         * $individual_birthday_start  誕生日月 (始)
         * $individual_birthday_end    誕生日月 (終)
         * $individual_region          地域
         * $first_start                初回登録日 (始)
         * $first_end                  初回登録日 (終)
         * $last_start                 最終来店日 (始)
         * $last_end                   最終来店日 (終)
         * $total_stamp_num            累計スタンプ数
         * $total_stamp_terms          累計スタンプ数条件 (or_more:以上 , downward:以下)
         * push_check                  プッシュ通知をするかのチェック
         * $coupon_start               プレゼントクーポンの使用：開始日時
         * $coupon_end                 プレゼントクーポンの使用：期限日時
         * $gift_coupon_id             プレゼントクーポンID
         * $rally_id                   ラリーID
         *
         * @var unknown_type
         */
		error_log("発行ボタン present_img_name : ".$present_img_name);
		error_log("発行ボタン coupon_isuue_flag : ".$coupon_isuue_flag);
        switch ($delivery_set) {
            case "all";//一斉配信
                $delivery_set_no = 1;
                break;
            case "simple"; //簡単配信
                $delivery_set_no = 2;
                break;
            case "individual";  //個別配信
                $delivery_set_no = 3;
                break;
            case "to_person";  //個人宛配信
                $delivery_set_no = 5;
                break;
            default;
                break;
        }

        $simple_last_month = 0;
        if(in_array("last_month", $simple_check)){
            $simple_last_month = 1;
        }
        $simple_this_month = 0;
        if(in_array("this_month", $simple_check)){
            $simple_this_month = 1;
        }
        $simple_next_month = 0;
        if(in_array("next_month", $simple_check)){
            $simple_next_month = 1;
        }
        $simple_sex = 0;
        if(in_array("man", $simple_check) && in_array("woman", $simple_check)){
            $simple_sex = 1;
        } else if(in_array("man", $simple_check)){
            $simple_sex = 2;
        } else if(in_array("woman", $simple_check)){
            $simple_sex = 3;
        }
        $yesterday_come = 0;
        if(in_array("yesterday_come", $simple_check)){
            $yesterday_come = 1;
        }
        $month_come = 0;
        if(in_array("month_come", $simple_check)){
            $month_come = 1;
        }
        $this_month_come = 0;
        if(in_array("this_month_come", $simple_check)){
            $this_month_come = 1;
        }

        $individual_sex_no=1;
        switch ($individual_sex) {
            case "all";//全て
                $individual_sex_no = 1;
                break;
            case "man"; //男性
                $individual_sex_no = 2;
                break;
            case "woman";  //女性
                $individual_sex_no = 3;
                break;
            default;
                break;
        }
        if($total_stamp_terms == "or_more"){
            $total_stamp_terms_no = 1;
        } else if($total_stamp_terms == "downward"){
            $total_stamp_terms_no = 2;
        }
	
	$coupon_start = $coupon_year_start."-".$coupon_month_start."-".$coupon_day_start." ".$coupon_hour_start.":".$coupon_minute_start.":00";
	$coupon_end = $coupon_year_end."-".$coupon_month_end."-".$coupon_day_end." ".$coupon_hour_end.":"."00:00";
	$rally_date = Util::rally_information_get(ADMIN_ID);
	$rally_id = $rally_date['rally_id'];
	//ユーザーにクーポン配布
	if($coupon_way == "period"){
		$coupon_way_date = 0;
	}
    //プッシュ通知チェック
    if (empty($push_check)) {
        $push_check = 0;
    }else{
		$push_check = 1;
	}
//	$cul_num = 27;
	// プレゼント(gift)クーポンの定義情報テーブルを作成
	$db = db_connect();
//	$into = "'".mysql_real_escape_string($limit_coupon_name)."' , '".$coupon_way_date."' , '".$coupon_start."' , '".$coupon_end."' , 0 , '".$present_admin_id."' , '".$rally_id."' , '0' , '0' , '".$push_check."' , '".$delivery_set_no."' , '".$simple_last_month."' , '".$simple_this_month."' , '".$simple_next_month."' , '".$simple_sex."' , '".$yesterday_come."' , '".$month_come."' , '".$this_month_come."' , '".$individual_sex_no."' , '".$individual_birthday_start."' , '".$individual_birthday_end."' , '".$individual_region."' , '".$last_start."' , '".$last_end."' , '".mysql_real_escape_string($total_stamp_num)."' , '".$total_stamp_terms_no."' , '".$delivery_num."'";
//	gift_coupon_insert($db , $into , $cul_num);
	$set = "coupon_name = '".mysql_real_escape_string($limit_coupon_name)."', coupon_way = '".$coupon_way_date."', coupon_start = '".$coupon_start."', coupon_end = '".$coupon_end."', coupon_deadline_day = '0', admin_id = '".$present_admin_id."', rally_id = '".$rally_id."', issue_num = '0', use_num = '0', push_check = '".$push_check."', delivery_set = '".$delivery_set_no."', simple_last_month = '".$simple_last_month."', simple_this_month = '".$simple_this_month."', simple_next_month = '".$simple_next_month."', simple_sex = '".$simple_sex."', yesterday_come = '".$yesterday_come."', month_come = '".$month_come."', this_month_come = '".$this_month_come."', individual_sex = '".$individual_sex_no."', individual_birthday_start = '".$individual_birthday_start."', individual_birthday_end = '".$individual_birthday_end."', individual_region = '".$individual_region."', last_start = '".$last_start."', last_end = '".$last_end."', total_stamp_num = '".mysql_real_escape_string($total_stamp_num)."', total_stamp_terms = '".$total_stamp_terms_no."', delivery_num = '".$delivery_num."', disp_type = '".$coupon_isuue_flag."' , coupon_image_name = '".$present_up_img."'";
	gift_coupon_insert_set($db , $set);
	
	$gift_coupon_id = mysql_insert_id();
	db_close($db);
	
    // プレゼントクーポンを送信するadminを設定し
    // 送信時点での送信可能な総ユーザ数を取得する。
//    $send_admin = 0;
//    if ($_SESSION["branchId"] == 0) {
//        // 支店でない場合、オーナーのADMINを設定
//        $send_admin = ADMIN_ID;
//        $db = db_connect();
//        // 支店でない場合、admin_idをキーにadminに属する総ユーザ数を取得する。
//        $count_users = get_rally_user_count_by_admin_id($db , $send_admin);
//        db_close( $db );
//    } else {
//        // 支店の店長の場合、支店のadminを設定
//        $db = db_connect();
//        $child_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
//        $send_admin = $child_admin_id;
//        // 支店の場合、branch_idをキーに支店の総ユーザ数を取得する
//        $count_users = get_rally_user_count_by_branch_id($db , $_SESSION["branchId"]);
//        db_close( $db );
//    }

	$db = db_connect();
	if (empty($branch_ids)) {
		// 支店IDで絞り込む必要がないので、
		// 支店でない場合、admin_idをキーにadminに属する総ユーザ数を取得する。
		$count_users = get_rally_user_count_by_admin_id($db , $present_admin_id);
	} else {
		// 支店IDで絞り込む必要がある
		$count_users = get_rally_user_count_by_branch_id($db , $branch_ids);
	}
	db_close( $db );
	
    $db = db_connect();
    $where = "admin_id = '".$present_admin_id."' AND coupon_name = '".mysql_real_escape_string($limit_coupon_name)."'";
    $result = gift_coupon_select($db , $where);
    $gift_row = mysql_fetch_array($result);
    $gift_id = $gift_row['id'];
    db_close( $db );

    $_SESSION['asp']['gift']['transition'] = 'complete';
    $_SESSION['asp']['gift']['data'] = array(
        'gift_id' => $gift_id,
        'gift_coupon_name' => $limit_coupon_name,
        'delivery_set' => $delivery_set_no,
        'push_check' => $push_check,
        'simple_last_month' => $simple_last_month,
        'simple_this_month' => $simple_this_month ,
        'simple_next_month' => $simple_next_month ,
        'simple_sex' => $simple_sex ,
        'month_come' => $month_come ,
        'this_month_come' => $this_month_come ,
        'yesterday_come' => $yesterday_come ,
        'individual_sex' => $individual_sex_no ,
        'individual_birthday_start' => $individual_birthday_start ,
        'individual_birthday_end' => $individual_birthday_end ,
        'individual_region' => $individual_region ,
        'last_start' => $last_start ,
        'last_end' => $last_end ,
        'total_stamp_num' => $total_stamp_num ,
        'total_stamp_terms' => $total_stamp_terms ,
		'coupon_start' => $coupon_start ,
		'coupon_end' => $coupon_end ,
		'gift_coupon_id' => $gift_coupon_id,
		'rally_id' => $rally_id,
		'to_user_id' => $to_user_id,
		'coupon_image_name' => $present_up_img,
		'coupon_isuue_flag' => $coupon_isuue_flag
        );
        header('Location: '.$_SERVER['REQUEST_URI']);

} else if(isset($_POST['check'])){  //確認ページ
	if ($delivery_type == 'to_person') {
        $delivery_num = 1;
    }
	$num = mt_rand();
	//プレゼントクーポン画像の保存チェック
	if (is_uploaded_file($_FILES["present_up_img"]["tmp_name"])) {
		if (move_uploaded_file($_FILES["present_up_img"]["tmp_name"], "./../img_coupon/" .$num.$_FILES["present_up_img"]["name"])) {
			chmod("./../img_coupon/" .$num.$_FILES["present_up_img"]["name"], 0644);
			$present_up_img = $num.$_FILES["present_up_img"]["name"];
		} else {
			echo "プレゼント画像ファイルをアップロードできません";
		}
	}
    // 入力フォームから確認画面へ遷移
    switch ($delivery_set) {
        case "all";//一斉配信
            $delivery_set_name = "一斉配信";
            break;
        case "simple"; //簡単配信
            $delivery_set_name = "簡単配信";
            break;
        case "individual";  //個別配信
            $delivery_set_name = "個別配信";
            break;
        case "to_person";  //個人宛配信
            $delivery_set_name = "個人宛配信";
            break;
        default;
            break;
    }
    switch ($individual_sex) {
        case "all";//全て
            $individual_sex_date = "全て";
            break;
        case "man"; //男性
            $individual_sex_date = "男性";
            break;
        case "woman";  //女性
            $individual_sex_date = "女性";
            break;
        default;
            break;
    }
//	$gift_coupon_array = past_gift($present_admin_ids);
    $_SESSION['asp']['gift']['transition'] = 'check';
	if(Util::is_smartphone ()) {
		require './sp/page/gift_check.php';
	} else {
		require './pc/page/gift_check.php';
	}
} else {
	// 初期画面表示の場合
    
    // transitionの初期化
    $_SESSION['asp']['gift']['transition'] = 'input';
    
    $individual_sex = "all";
	
//	$gift_coupon_array = past_gift($present_admin_ids);
	
	if(Util::is_smartphone ()) {
		require './sp/page/gift.php';
	} else {
		require './pc/page/gift.php';
	}
}
?>