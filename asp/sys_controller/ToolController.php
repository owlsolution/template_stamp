<?php
$rally_date = Util::rally_information_get(ADMIN_ID);
$rally_id = $rally_date['rally_id'];

$qr_code_data = "http://".HOST_NAME."/stamp/".FILE_NAME."/?rally_id={$rally_id}&store=1";

// 資料準備中フラグ true:準備中と表示
$ai_flag_off = true;

// 資料リンクの表示/非表示フラグ true:表示 false:非表示
$A2_1 = false;
$A4_1 = true;
$A4_2 = true;
$A6_1 = true;
$A6_2 = true;

if(Util::is_smartphone ()) {
	require './sp/page/tool.php';
} else {
	require './pc/page/tool.php';
}
