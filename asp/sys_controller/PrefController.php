<?php

//ラリーID
$db = db_connect();
$where = "admin_id = ".ADMIN_ID;
$rally_date = rally_select($db , $where);
$rally = mysql_fetch_array($rally_date);
$rally_id = $rally['rally_id'];
db_close( $db );

$stamp_day = $_POST['stamp_day'];  //スタンプ有効期限 日
$goal_day = $_POST['goal_day'];  //ゴール有効期限 日
$one_times_a_day = $_POST['one_times_a_day'];  //１日１回付与
$user_profile_force_check = $_POST['user_profile_force_check'];  //ユーザー情報を登録しているかのポップアップを表示非表示のチェック
$new_stamp_num = $_POST['new_stamp_num'];  //新規スタンプ数
$profile_force = $_POST['profile_force'];  //プロフィール要求度  $profile_force == 2 : 初期起動時のみポップアップを表示  == 3 : 初期起動時＋クーポン発行時にポップアップを表示



// SNS連携情報のクリア

error_log("['sns']['clear']:".$_SESSION['sns']['clear']);
if (isset($_POST['sns_clear_exec']) || isset($_SESSION['sns']['clear'])) {
	$sns_clear = isset($_POST['sns_clear']) ? $_POST['sns_clear'] : '0';
	
error_log("sns_clear:".$sns_clear);
	if (isset($_SESSION['sns']['clear'])) {
		$sns_clear = $_SESSION['sns']['clear'];
	}
error_log("sns_clear:".$sns_clear);
	if (!empty($sns_clear)) {
		$_SESSION['sns']['clear'] = $sns_clear;
		
		if ($sns_clear != 1) {
			// Twitter clear
			error_log("Twitter clear start.".ADMIN_ID);
			
			// Twitter関連のセッションクリア
			// admin_idと連携しているtokenクリア
			$db = db_connect();
			$admin_auth = get_one_admin_auth_by_admin_id($db, ADMIN_ID, 'twitter');
			db_close($db);
			
			if (!empty($admin_auth)) {
				error_log("not empty:".$admin_auth['admin_auth_id']);
				// 削除
				$db = db_connect();
				delete_admin_auth($db, $admin_auth['admin_auth_id']);
				db_close($db);
			}
		}
		if ($sns_clear != 2) {
			// Facebook clear
			error_log("Facebook clear start.");
			// Facebook関連セッションクリアとコマンド実行
			$access_token = SnsUtility::get_facebook_access_token();
			// アクセストークンが取得出来ない場合はFacebookログイン
			if ((empty($access_token)) && ($_SESSION['sns']['facebook']['login'] == 'on')){
				//　ログインからのコールバックのはずが、トークンが取れない場合処理中断
				unset($_SESSION['sns']['facebook']['login']);
			} else if (empty($access_token)) {
				$login_url = SnsUtility::get_facebook_login_url();
				echo '<a href="'.$login_url.'">セッションクリア[Facebookへ移動します]</a>';
				header('Location: ' . $login_url);
				// 初回
				$_SESSION['sns']['facebook']['login'] = 'on';
				exit;
			} else {
				$logout_url = SnsUtility::get_facebook_logout_url((empty($_SERVER['HTTPS']) ? 'http://' : 'https://') . $_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI']);
				header('Location: ' . $logout_url);
				unset($_SESSION['sns']);
				error_log("Facebook logout.");
				exit;
			}
		}
		unset($_SESSION['sns']);
		header('Location: ' . DOMAIN . FILE_NAME . "/asp/?p=pref");
		exit();
	}
}


$disabled = "";
$change_param_reslt = "";  //変更ボタンが押され、変更できたら、このテキストを表示する。
/**
 * プロフィール登録を促すポップアップの表示をいいえの場合
 * 【プロフィール登録要求度の設定】にチェックが入っていれば、全てOFFにする。
 * $user_profile_force_check == 1 : はい
 * $user_profile_force_check == 2 : いいえ
 */
if($user_profile_force_check == 2){
	$profile_force = 0;
	$disabled = "disabled";
}


//変更ボタンが押された時
if(isset($_POST['change_param'])){
	$db = db_connect();
	//有効期限設定でスタンプとクーポンの設定した時、全角で設定した場合、半角に変換する
	$stamp_day = mb_convert_kana($stamp_day, "n");
	$goal_day = mb_convert_kana($goal_day, "n");
	$set = "stamp_day = '".$stamp_day."' , goal_day = '".$goal_day."' , one_times_a_day = '".$one_times_a_day."' , sns = '".$user_profile_force_check."' , new_stamp_num = '".$new_stamp_num."' , profile_force = ".$profile_force."" ;
	$where = "rally_id = ".$rally_id ;
	up_rally($db , $set , $where);
	db_close( $db );
	$change_param_reslt = "変更しました。";
//初期値の表示
}else{
	$db = db_connect();
	$where = "rally_id = ".$rally_id;
	$rally_date = rally_select($db , $where);
	$rally = mysql_fetch_array($rally_date);
	$stamp_day = $rally['stamp_day'];
	$goal_day = $rally['goal_day'];
	$one_times_a_day = $rally['one_times_a_day'];
	//ユーザー情報を登録しているかのポップアップを表示非表示のチェック
//	$user_profile_force_check = $rally['sns'];
	$profile_force = $rally['profile_force'];  // プロフィール強制度
	$disabled = "";
	if($profile_force == 0){
			$user_profile_force_check = 2;
	} else {
			$user_profile_force_check = 1;
	}
	$new_stamp_num = $rally['new_stamp_num'];
	$profile_force = $rally['profile_force'];  // プロフィール強制度
	db_close( $db );
}
if(Util::is_smartphone ()) {
	require './sp/page/pref_set_up.php';
} else {
	require './pc/page/pref_set_up.php';
}
