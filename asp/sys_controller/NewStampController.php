<?php
/*
 * 新規スタンプ作成
 */

// 初期化設定
$rally_expire_type = 1;  //スタンプ有効期限タイプ
$goal_expire_type = 1;  //ゴール有効期限タイプ
$disabled = "disabled";
$user_profile_force_check  = 2;
$profile_force = 0;


if(isset($_POST['setting']) || isset($_POST['return'])){
	$rally_name = $_POST['rally_name'];  //スタンプ名
	$hp_url = $_POST['hp_url'];				//ホームページURL
	$category_select = $_POST['category'];	//カテゴリ
	$stamp_day = $_POST['stamp_day'];		//スタンプ有効期限 日
	$stamp_date = $_POST['stamp_date'];		//スタンプ有効期限 日付
	$goal_day = $_POST['goal_day'];			//ゴール有効期限 日
	$goal_date = $_POST['goal_date'];		//ゴール有効期限 日付
	$takeover = $_POST['takeover']; //カード引き継ぎ機能
	$stamp_max = $_POST['stamp_max'];//全スタンプ数
	$smart_setting = $_POST['smart_setting'];  //スマートフォン設定
	$download_url = $_POST['download_url'];  //アプリダウンロードファイル名
	$one_times_a_day = $_POST['one_times_a_day'];  //１日１回付与
	$new_stamp_num = $_POST['new_stamp_num'];  //新規スタンプ数
	$user_profile_force_check = $_POST['user_profile_force_check'];  //ユーザー情報を登録しているかのポップアップを表示非表示のチェック
	$profile_force = $_POST['profile_force'];  //プロフィール要求度  $profile_force == 2 : 初期起動時のみポップアップを表示  == 3 : 初期起動時＋クーポン発行時にポップアップを表示

	/**
	 * プロフィール登録を促すポップアップの表示をいいえの場合
	 * 【プロフィール登録要求度の設定】にチェックが入っていれば、全てOFFにする。
	 * $user_profile_force_check == 1 : はい
	 * $user_profile_force_check == 2 : いいえ
	 */
	$disabled = "";
	if($user_profile_force_check == 2){
		$profile_force = 0;
		$disabled = "disabled";
	}

}
if(isset($_POST['check'])){  //確認ボタン
	$url = "./?p=new_stamp";
	require "./pc/page/new_stamp_check.php";
} else if(isset($_POST['setting'])){  //設定ボタン
	
	$add_date = date('Y-m-d');
	$db = db_connect();
	$into = ADMIN_ID." , '".$rally_name."' , '".$hp_url."' , '".$stamp_max."' , '".$category_select."' , '".$rally_expire_type."' , '".$stamp_day."' , '".$stamp_date."' , '".$goal_expire_type."' , '".$goal_day."' , '".$goal_date."' , '".$takeover."' , '".$smart_setting."' , '".$add_date."' , '".$download_url."' , '".$one_times_a_day."' , '1' , '".$new_stamp_num."' , ".$profile_force."";
	new_rally_insert($db , $into);
	$where = "rally_name = '".$rally_name."' AND add_date ='".$add_date."'";
	$new_stamp_select_date = new_stamp_select($db , $where);
	$new_stamp_select = mysql_fetch_array($new_stamp_select_date);
	$rally_id = $new_stamp_select['rally_id'];
	$into = $rally_id." , '".$add_date."'";
	rally_page_insert($db , $into);
	goal_page_insert($db , $into);
	
	// 定時処理の初期化
	$into = ADMIN_ID." , '".$rally_id."' , '12:00:00' , -1 , -1 ";
	insert_period_notice($db , $into);
	db_close( $db );
	require "./pc/page/new_setting_end.php";
} else if(isset($_POST['return'])){  //戻るボタン
	$url = "./?p=new_stamp";
	require "./pc/page/new_stamp_form.php";
} else {
	$url = "./?p=new_stamp";
	require "./pc/page/new_stamp_form.php";
}
?>