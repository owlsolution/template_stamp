<?php
require_once('./../common/model/RallyUserModel.php');
$rallyUserModel = new RallyUserModel();

require_once('./../common/model/AdminModel.php');
$adminModel = new AdminModel();

/**
 * ランダム文字列生成 (英数字)
 * $length: 生成する文字数
 */
function makeRandStr($length = 8) {
	static $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJLKMNOPQRSTUVWXYZ0123456789';
	$str = '';
	for ($i = 0; $i < $length; ++$i) {
		$str .= $chars[mt_rand(0, 61)];
	}
	return $str;
}

function makeRandInt($length = 7) {
	$str = '';
	for ($i = 0; $i < $length; ++$i) {
		$str .= mt_rand(0, 9);
	}
	return $str;
}

/**
 * adminに連携するuser_idを取得する
 * @param type $admin_id
 * @return type
 */
function getUserIdRelatedAdminId($db, $admin_id) {
	$where = "admin_id = '".$admin_id."' LIMIT 0,1";
	$data = admin_user_select($db , $where);
	$admin = mysql_fetch_array($data);
	if(empty($admin)) {
		return false;
	}
	return $admin['user_id'];
}

/**
 * 指定した管理アカウントのリストを取得する
 * @param type $admin_id
 * @return type
 */
function get_admin_account_list($db, $in_admin_ids, $branch_list) {
	$admin_list = null;
	$adminModel = new AdminModel();
	$where = ' admin_id IN(' . implode(',', $in_admin_ids) . ') ORDER BY field(branch_flag,0,1,4,2,3)';
	$admin_data = admin_select_date($db, $where);
	while ($admin_account = mysql_fetch_array($admin_data)) {
		$staff_user_id = getUserIdRelatedAdminId($db, $admin_account['admin_id']);
		$admin_account['user_id'] = $staff_user_id;

		// 変更対象の管理アカウントのロールタイプを取得
		$admin_type = $adminModel->get_admin_type($db, $admin_account['admin_id']);
		$account_name = 'アカウント';
		if ($admin_type == OWNER) {
			// オーナーアカウント
			$account_name = 'オーナーアカウント';
		} else if ($admin_type == BRANCHES_OWNER) {
			// 統括アカウント
			$account_name = '全店管理アカウント';
		} else if ($admin_type == BRANCH_MANAGER) {
			// 支店アカウント
			$account_name = '管理アカウント';
		} else if ($admin_type == ORG_MANAGER) {
			$account_name = '組織管理アカウント';
			$org = $adminModel->get_organization_by_admin_id($db, $admin_account['admin_id']);
		}
		$admin_account['name'] = $account_name;
		
		// 組織名取得
		if (isset($org)) {
			$admin_account['branch_name'] = $org['name'];
			unset($org);
		}
		
		// 支店名取得
		$branch_name  = "";
		foreach ($branch_list as $branch) {
			if ($admin_account['admin_id'] == $branch['child_admin_id']) {
				$branch_name = $branch['name'];
				break;
			}
		}
		if(!empty($branch_name)){
			$admin_account['branch_name'] = $branch_name;
		}
		$admin_list[] = $admin_account;
	}
	return $admin_list;
}

/**
 * 指定した管理アカウントのリストを取得する
 * @param type $admin_id
 * @return type
 */
function get_staff_list($db, $rally_id, $branch_list) {
	// スタッフ一覧情報を取得する
	$staff_list = null;
	
	$adminModel = new AdminModel();
	
	$staffs = $adminModel->get_staff_by_rally_id($db, $rally_id);
	// 全スタッフが全て取れているので、ログイン権限毎にブログ配信で選択リストに表示されるスタッフを絞り込む
	if ($_SESSION["branchFlag"] == OWNER) {
		// 全員
		$staff_list = $staffs;
	} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] == 0)){
		// オーナー直下のスタッフ
		$staff_list = $adminModel->filter_staff_by_under_owner($staffs);
	} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] != 0)){
		// 単一ブランチ
		$branch_ids = $_SESSION["branchId"];
		$staff_list = $adminModel->filter_staff_by_branch_id($staffs, $branch_ids);
	} else if ($_SESSION["branchFlag"] == BRANCH_MANAGER) {
		// 単一ブランチ
		$branch_ids = $_SESSION["branchId"];
		$staff_list = $adminModel->filter_staff_by_branch_id($staffs, $branch_ids);
	} else if (($_SESSION["branchFlag"] == ORG_MANAGER) && ($_SESSION["branchId"] != 0)){
		// 単一ブランチ
		$branch_ids = $_SESSION["branchId"];
		$staff_list = $adminModel->filter_staff_by_branch_id($staffs, $branch_ids);
	} else if ($_SESSION["branchFlag"] == ORG_MANAGER) {
		// 組織IDから組織直下取得
		$staff_list = $adminModel->filter_staff_by_organization_id($staffs, $_SESSION["orgId"]);
		// 組織配下の支店に属するスタッフを取得
		$branch_list = $adminModel->get_branch_by_organization_id($db, $_SESSION["orgId"]);
		$branch_id_list = array_column($branch_list, 'id');
		$staff_of_under_branch_list = $adminModel->filter_staff_by_branch_id($staffs, $branch_id_list);
		
		// スタッフの結合
		$staff_list = array_merge($staff_list, $staff_of_under_branch_list);
	} else if ($_SESSION["branchFlag"] == STAFF) {
		// スタッフが所属する支店の支店IDを取得する
		// 現状未対応
	}
	
	foreach ($staff_list as &$staff) {
		$branch_name = "-";
		foreach ($branch_list as $branch) {
			if ($staff['branch_id'] == $branch['id']) {
				$branch_name = $branch['name'];
				break;
			}
		}
		$staff['branch_name'] = $branch_name;
		// 紐付きのある顧客がいる場合はuser_idを取得する
		$staff_user_id = getUserIdRelatedAdminId($db, $staff['admin_id']);
		$staff['user_id'] = $staff_user_id;
	}
	return $staff_list;
}

/**
 * 現在ログインしている管理アカウントのモードを判定して、振舞うべきadmin_idを返す
 * @return type
 */
function judge_current_admin_id() {
	// 管理画面へのログインアカウントからアカウント一覧に表示するadminを判断する
	$current_admin_id = 0;
	if ($_SESSION["branchFlag"] == 0) {
		// １店舗のみ（支店機能なし）のオーナー
		// １件のみ
		$current_admin_id = ADMIN_ID;
	} else if ($_SESSION["branchFlag"] == 1) {
		// 支店機能のオーナー
		$current_admin_id = ADMIN_ID;
		if ($_SESSION["branchId"] != 0) {
			// オーナーが支店モード機能で操作しているので支店権限で発行する
			$db = db_connect();
			$current_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
			db_close( $db );
		}
	} else if ($_SESSION["branchFlag"] == 2) {
		$db = db_connect();
		$current_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);;
		db_close( $db );
	} else if ($_SESSION["branchFlag"] == 3) {
		// スタッフの場合、
		// 現状ありえない
	} else {
		// 現状ありえない
	}
	
	if (empty($current_admin_id)) {
		return false;
	}
	return $current_admin_id;
}


$delete_id = isset($_GET['delete']) ? $_GET['delete'] : '';
$edit_id = isset($_GET['edit']) ? $_GET['edit'] : '';
$admin_account_id = isset($_GET['admin_edit']) ? $_GET['admin_edit'] : '';


// 支店情報を取得
$db = db_connect();
$branch_list = branch_select_by_owner($db, ADMIN_ID);
db_close($db);

// rally_idを取得
$db = db_connect();
$rally_select_date = rally_select($db , "admin_id = ".ADMIN_ID);
$rally_select = mysql_fetch_array($rally_select_date);
$rally_id = $rally_select['rally_id'];
db_close($db);

// スタッフ削除
if(!empty($delete_id)) {
	$db = db_connect();
	$where = "admin_id=".$delete_id;
	// スタッフテーブルの更新
	$set = "status = '2'";
	staff_update($db , $set, $where);
	// adminテーブルの更新 
	// 今は不要
	// admin_user削除
	$where = "admin_id=".$delete_id;
	admin_user_delete($db, $where);
	db_close($db);
	// admin削除
//	management_admin_delete($db, $where);
	
	header('Location: ./?p=staff');
	exit();
}
else if(!empty($edit_id)) {
	$is_disp_entry_user_id = true;
	// スタッフ編集時
	$db = db_connect();
	$where = "admin_id=".$edit_id;
	$staff_data = staff_select($db , $where);
	$staff = mysql_fetch_array($staff_data);
	// 編集対象のレコード情報を変数に保持
	$admin_id = $staff['admin_id'];
	$nickname = $staff['nickname'];
	$rally_id = $staff['rally_id'];
	$branch_id = $staff['branch_id'];
	$img_path = $staff['img_path'];
	//支店IDを取得
	$child_admin_id = getChildAdminIdByBranchId($db, $branch_id);
	foreach ($branch_list as $branch){
		if($branch_id == 0){
			$branch_name = "-";
		} else {
			if($child_admin_id == $branch['child_admin_id']) {
				$branch_name = $branch['name'];
			}
		}
	}
	$selected_branch_name = $branch_name;
	// アプリユーザ連携するuser_idを取得する
	$user_id = getUserIdRelatedAdminId($db, $admin_id);
	db_close($db);
	
} 
else if(!empty($admin_account_id)) {
	
	$db = db_connect();
	// 変更対象の管理アカウントのロールタイプを取得
	$admin_type = $adminModel->get_admin_type($db, $admin_account_id);
	$account_name = 'アカウント';
	if ($admin_type == OWNER) {
		// オーナーアカウント
		$account_name = 'オーナーアカウント';
	} else if ($admin_type == BRANCHES_OWNER) {
		// 統括アカウント
		$account_name = '全店管理アカウント';
	} else if ($admin_type == ORG_MANAGER) {
		// 組織管理アカウント
		$account_name = '組織管理アカウント';
	} else if ($admin_type == BRANCH_MANAGER) {
		// 支店アカウント
		$account_name = '支店管理アカウント';
	}
	
	// アプリユーザ連携するuser_idを取得する
	$account_user_id = getUserIdRelatedAdminId($db, $admin_account_id);
	
	// サムネイル画像を取得
	$admin = $adminModel->get_admin_by_admin_id($db, $admin_account_id);
	$admin_img_path = '';
	if(isset($admin['img_path'])) {
		$admin_img_path = $admin['img_path'];
	}
	
	db_close($db);
	
} 
else if(isset($_POST['update'])) {
	$edit_id = isset($_POST['edit_id']) ? $_POST['edit_id'] : '0';
	$entry_user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
	if (isset($entry_user_id)) {
		// トリム
		$entry_user_id = trim($entry_user_id);
		$entry_user_id = str_replace(USER_ID, '', $entry_user_id);
	}
	$user_id = $entry_user_id;
	
	// データ追加/更新時
	$staff_name = isset($_POST['staff_name']) ? $_POST['staff_name'] : "名無し";
	$branch_id = isset($_POST['branch_id']) ? $_POST['branch_id'] : "0";
	$filename = isset($_POST['ic_image_filename']) ? $_POST['ic_image_filename'] : "";

	// スタッフテーブルの更新
	$db = db_connect();
	$where = "admin_id=".$edit_id;
	$set = "nickname = '".Util::sanitize_sql($staff_name)."', img_path = '".$filename."', branch_id = '".$branch_id."'";
	staff_update($db , $set, $where);
	// adminテーブルの更新 
	// 今は不要
	db_close($db);
	
	if (empty($user_id)) {
		// 空なのでリセットする
		$db = db_connect();
		$where = "admin_id=".$edit_id;
		$set = "admin_id='".$edit_id."' , user_id = NULL, updated = now()";
		if(!admin_user_update($db, $set, $where)) {
			$error_no = mysql_errno();
			if ($error_no == '1062') {
				$is_disp_entry_user_id = true;
				// 重複エラー
				$error_message_admin = '連携情報の更新に失敗しました。detail:'.mysql_error();
				error_log($error_message_admin.'Staff admin_user_update Invalid update query: ' . mysql_error());
			}
		}
		db_close($db);
	} else {
		$db = db_connect();
		$r_user_info = $rallyUserModel->get_userinfo_by_user_id_and_rally_id($db, $user_id, $rally_id);
		db_close($db);
		if(!empty($r_user_info['user_id'])) {
			// AdminUser連携の更新
			$db = db_connect();
			$where = "admin_id=".$edit_id;
			if(empty($user_id)) {
				$q_user_id = 'null';
			} else {
				$q_user_id = $user_id;
			}
			$set = "admin_id='".$edit_id."' , user_id = ".$q_user_id.", updated = now()";
			if(!admin_user_update($db, $set, $where)) {
				$error_no = mysql_errno();
				if ($error_no == '1062') {
					$is_disp_entry_user_id = true;
					// 重複エラー
					$error_message = '指定した顧客IDは既に別のアカウントと連携しています。';
				} else {
					die('Invalid update query: ' . mysql_error());
				}
			}
			if (mysql_affected_rows() == 0) {
				// 存在しないのでインサートする
				if(!admin_user_insert($db , $set) ) {
					$error_message = '指定した顧客IDは既に別のアカウントと連携しています。';
					error_log($error_message.'Staff admin_user_insert Invalid update query: ' . mysql_error());
				}
			}
			db_close($db);

			error_log("send_admin_pushes user_id:".$r_user_info['user_id']);
			// スタッフと顧客IDを連携した際にユーザへPUSH通知する
			$message = "管理アカウントと連携されました。";
			$user_id_list[] = $r_user_info['user_id'];
			Util::send_admin_pushes(ADMIN_ID, $message, $user_id_list);

		} else {
			// 実在しない場合、処理を回避してエラーメッセージ表示
			$error_message = "設定に失敗しました。[指定した顧客IDは存在しません。]";
		}
	}
	
	if (empty($error_message)) {
		$user_id = null;
		$edit_id = null;
	}else {
		$db = db_connect();
		$where = "admin_id=".$edit_id;
		$staff_data = staff_select($db , $where);
		$staff = mysql_fetch_array($staff_data);
		// 編集対象のレコード情報を変数に保持
		$admin_id = $staff['admin_id'];
		$nickname = $staff['nickname'];
		$rally_id = $staff['rally_id'];
		$branch_id = $staff['branch_id'];
		$img_path = $staff['img_path'];
		//支店IDを取得
		$child_admin_id = getChildAdminIdByBranchId($db, $branch_id);
		foreach ($branch_list as $branch){
			if($branch_id == 0){
				$branch_name = "-";
			} else {
				if($child_admin_id == $branch['child_admin_id']) {
					$branch_name = $branch['name'];
				}
			}
		}
		db_close($db);
	}
	
} else if(isset($_POST['adminupdate'])) {
	$edit_id = isset($_POST['admin_edit_id']) ? $_POST['admin_edit_id'] : '0';
	$entry_user_id = isset($_POST['account_user_id']) ? $_POST['account_user_id'] : '';
	
	// サムネイル保存
	$filename = isset($_POST['admin_ic_image_filename']) ? $_POST['admin_ic_image_filename'] : "";
	if (isset($edit_id)) {
		$db = db_connect();
		$set = "admin_id='".$edit_id."' , img_path = '".$filename."'";
		$where = "admin_id=".$edit_id;
		$adminModel->update_admin($db, $set, $where);
		db_close($db);
	}
	
	if (isset($entry_user_id)) {
		// トリム
		$entry_user_id = trim($entry_user_id);
		$entry_user_id = str_replace(USER_ID, '', $entry_user_id);
	}
	$account_user_id = $entry_user_id;
	
	if (empty($account_user_id)) {
		// 空なのでリセットする
		$db = db_connect();
		$where = "admin_id=".$edit_id;
		$set = "admin_id='".$edit_id."' , user_id = NULL, updated = now()";
		if(!admin_user_update($db, $set, $where)) {
			$error_no = mysql_errno();
			if ($error_no == '1062') {
				$is_disp_entry_user_id = true;
				// 重複エラー
				$error_message_admin = '連携情報の更新に失敗しました。detail:'.mysql_error();
				error_log($error_message_admin.'Staff admin_user_update Invalid update query: ' . mysql_error());
			}
		}
		db_close($db);
	} else {
		// user_idは実在するかをチェック
		$db = db_connect();
		$r_user_info = $rallyUserModel->get_userinfo_by_user_id_and_rally_id($db, $account_user_id, $rally_id);
		db_close($db);
		if(!empty($r_user_info['user_id'])) {
			// 実在する場合、処理続行
			// AdminUser連携の更新
			$db = db_connect();
			$where = "admin_id=".$edit_id;
			if(empty($account_user_id)) {
				$q_user_id = 'null';
			} else {
				$q_user_id = $account_user_id;
			}
			$set = "admin_id='".$edit_id."' , user_id = ".$q_user_id.", updated = now()";
			if(!admin_user_update($db, $set, $where)) {
				$error_no = mysql_errno();
				if ($error_no == '1062') {
					$is_disp_entry_user_id = true;
					// 重複エラー
					$error_message_admin = '連携情報の更新に失敗しました。detail:'.mysql_error();
					error_log($error_message_admin.'Staff admin_user_update Invalid update query: ' . mysql_error());
				}
			}
			if (mysql_affected_rows() == 0) {
				// 存在しないのでインサートする
				if(!admin_user_insert($db , $set) ) {
					if ($error_no == '1062') {
						$is_disp_entry_user_id = true;
						// 重複エラー
						$error_message_admin = '指定した顧客IDは既に別のアカウントと連携しています。';
					} else {
						$error_message_admin = '連携情報の設定に失敗しました。detail:'.mysql_error();
						error_log($error_message_admin.'Staff admin_user_insert Invalid update query: ' . mysql_error());
					}
				}
			}
			db_close($db);
		} else {
			// 実在しない場合、処理を回避してエラーメッセージ表示
			$error_message_admin = "設定に失敗しました。[指定した顧客IDは存在しません。]";
		}
	}
	
	if (empty($error_message_admin)) {
		$account_user_id = null;
		$edit_id = null;
	}else {
		$db = db_connect();
		// 変更対象の管理アカウントのロールタイプを取得
		$admin_type = $adminModel->get_admin_type($db, $admin_account_id);
		$account_name = 'アカウント';
		if ($admin_type == OWNER) {
			// オーナーアカウント
			$account_name = 'オーナーアカウント';
		} else if ($admin_type == BRANCHES_OWNER) {
			// 統括アカウント
			$account_name = '全支店管理アカウント';
		} else if ($admin_type == BRANCH_MANAGER) {
			// 支店アカウント
			$account_name = '支店管理アカウント';
		} else if ($admin_type == ORG_MANAGER) {
			// 支店アカウント
			$account_name = '組織管理アカウント';
		}
		db_close($db);
	}
	
} else if(isset($_POST['setting'])) {
	// データ追加
	$staff_name = isset($_POST['staff_name']) ? $_POST['staff_name'] : "";
	$branch_id = isset($_POST['branch_id']) ? $_POST['branch_id'] : "0";
	$filename = isset($_POST['ic_image_filename']) ? $_POST['ic_image_filename'] : "";

	// 組織アカウントでログイン時、組織IDを取得する
	$organization_id = 0;
	if (($branch_id == "0") && ($_SESSION["branchFlag"] == ORG_MANAGER)) {
		// 組織ID
		$organization_id = $_SESSION["orgId"];
	}
	
	$db = db_connect();
	// adminテーブルへのインサート
	// オーナーの情報をベースにするので、オーナー情報を取得
	$where = "admin_id=".ADMIN_ID;
	$rows = admin_select_date($db, $where);
	$admin = mysql_fetch_array($rows);
	// adminテーブルにインサート
	$set = "admin_name='".$admin['admin_name']."_staff".makeRandInt()."' , login_pass='".makeRandStr(8)."' , file_name='".$admin['file_name']."' , branch_flag ='3' ";
	$id = admin_insert($db, $set);
	db_close($db);
	
	// スタッフテーブルへのインサート
	if ($id != false) {
		$db = db_connect();
		$where = "id=".$id;
		$rows = admin_select_date($db, $where);
		$admin = mysql_fetch_array($rows);
		$admin_id = $admin['admin_id'];
		$staff_name = Util::removeEmoji($staff_name);
		$set = "admin_id = '".$admin_id."', nickname = '".Util::sanitize_sql($staff_name)."', img_path = '".$filename."', branch_id = '".$branch_id."', rally_id = '".$rally_id."' , status = '1', organization_id ='".$organization_id."'";
		staff_insert($db , $set);
		db_close($db);
	}
}

// rallyに紐づくadmin_idを取得する
//$current_admin_id = judge_current_admin_id();
$db = db_connect();

$current_admin_id = "";
// 振替時の一時的な権限モード
$tmp_admin_type = $_SESSION["branchFlag"];
if ($_SESSION["branchFlag"] == OWNER) {
	// 全員
	$current_admin_id = ADMIN_ID;
} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] == 0)){
	// 全員
	$current_admin_id = ADMIN_ID;
} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] != 0)){
	// 単一ブランチ
	$current_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	// オーナーだが、支店管理アカウント権限モードへ振り返る
	$tmp_admin_type = BRANCH_MANAGER;
} else if ($_SESSION["branchFlag"] == BRANCH_MANAGER) {
	// 単一ブランチ
	$current_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
} else if (($_SESSION["branchFlag"] == ORG_MANAGER) && ($_SESSION["branchId"] != 0)){
	// 単一ブランチ
	$current_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	// 組織オーナーだが、支店管理アカウント権限モードへ振り返る
	$tmp_admin_type = BRANCH_MANAGER;
} else if ($_SESSION["branchFlag"] == ORG_MANAGER) {
	$org = $adminModel->get_organization_by_id($db, $_SESSION["orgId"]);
	// 組織IDからadmin_idを取得
	$current_admin_id = $org['admin_id'];
} else if ($_SESSION["branchFlag"] == STAFF) {
	// スタッフが所属する支店の支店IDを取得する
	// スタッフのadmin_idを設定
	$staff = $adminModel->get_staff_by_id($db, $_SESSION["staffId"]);
	$current_admin_id = $staff['admin_id'];
}

$in_admin_ids = $adminModel->get_admin_id_exclude_staff_by_own_admin_id($db, $current_admin_id, $tmp_admin_type);
//$in_admin_ids = '';
//if ($admin_type == OWNER) {
//    // 自前のidのみ
//    $in_admin_ids[] = $current_admin_id;
//} else if ($admin_type == BRANCHES_OWNER) {
//    // 自分とその配下の支店のadmin_id
//    $in_admin_ids[] = $current_admin_id;
//	$branch_list = branch_select_by_owner($db, $current_admin_id);
//	foreach($branch_list as $branch){
//        $in_admin_ids[] = $branch['child_admin_id'];
//	}
//} else if ($admin_type == BRANCH_MANAGER) {
//    // 自分だけ
//    $in_admin_ids[] = $current_admin_id;
//}

db_close($db);

// 支店名を取得する
$branch_name = "";
foreach ($branch_list as $branch) {
	if ($admin_account_id == $branch['child_admin_id']) {
		$branch_name = $branch['name'];
		break;
	}
}
// 支店アカウントの場合支店名を名前に追加
$account_name = (empty($branch_name) ? '' : '['.$branch_name.']').$account_name;

// 関連する(編集対象の)adminのリスト情報を取得する
$db = db_connect();
$admin_list = get_admin_account_list($db, $in_admin_ids, $branch_list);
db_close($db);

// スタッフ一覧情報を取得する
$db = db_connect();
$staff_list = get_staff_list($db, $rally_id, $branch_list);
db_close($db);

if(Util::is_smartphone ()) {
	require './sp/page/staff.php';
} else {
	require './pc/page/staff.php';
}
