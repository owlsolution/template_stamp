<?php
if(isset($_POST['input_check'])){
	$people_name = $_POST['people_name'];
	$mail_address = $_POST['mail_address'];
	$content = $_POST['content'];
	if($people_name == ""){
		$error['people_name'] = "ng";
	}
	if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $mail_address)) {
		$error['mail_address'] = "ng";
	}
	if(Util::is_smartphone ()) {
		require './sp/page/inquiry_check.php';
	} else {
		require './pc/page/inquiry_check.php';
	}
} else if(isset($_POST['transmission'])){
	$people_name = $_POST['people_name'];
	$mail_address = $_POST['mail_address'];
	$content = $_POST['content'];
	mb_language("japanese");

	$previous = SUPPORT_EMAIL;//宛先
	$mail_title = "お問い合わせがありました。";//題名
	$message  = "スタンプカードアプリ名 : ".FILE_NAME."\n"; //メーセージ
	$message .= "お名前 : ".$people_name."\n";
	$message .= "メールアドレス : ".$mail_address."\n";
	$message .= "お問い合わせ内容 : ".$content."\n";
	$message .= "送信日時 : ".date( "Y/m/d (D) H:i:s", time() )."\n";
	$sender = $mail_address; //差出人
	$header = "From:".mb_encode_mimeheader($encSender, "ISO-2022-JP")."<".$sender.">";
	mb_send_mail($previous,$mail_title,$message,$header);
	
	$previous = $mail_address;//宛先
	$mail_title = "お問い合わせを受付ました。";//題名
	$message  = "================================================================\n"; //メーセージ
	$message .= "お問い合わせ内容の確認\n"; //メーセージ
	$message .= "================================================================\n";
	$message .= "この度はアウルソリューションよりご質問・お問い合わせいただき、誠にありがとうございます。\n\n";
	$message .= "下記の通りお申し込みを受け付けました。\n\n";
	$message .= "****************************************\n\n";
	$message .= "お名前 : ".$people_name."\n";
	$message .= "メールアドレス : ".$mail_address."\n";
	$message .= "お問い合わせ内容 : ".$content."\n\n";
	$message .= "****************************************\n\n";
	$message .= "担当より内容を確認後、返信させて頂きますので、今しばらくお待ちください。\n\n";
	$message .= "尚、お急ぎでしたらお電話にてご連絡お願い致します。\n\n";
	$message .= "================================================================\n\n";
	$message .= "アウルソリューション\n";
	$message .= "http://".HOST_NAME."\n";
	$message .= "TEL 092-980-2138\n";
	$message .= "MAIL ".NOTICE_MAIL_FROM_ADDRESS."\n\n";
	$message .= "================================================================\n\n";
	$sender = SUPPORT_EMAIL; //差出人
	$header = "From:".mb_encode_mimeheader($encSender, "ISO-2022-JP")."<".$sender.">";
	Util::send_mail($previous,$mail_title,$message,$header);
	if(Util::is_smartphone ()) {
		require './sp/page/inquiry_end.php';
	} else {
		require './pc/page/inquiry_end.php';
	}
} else {
	if(isset($_POST['return'])){
		$people_name = $_POST['people_name'];
		$mail_address = $_POST['mail_address'];
		$content = $_POST['content'];
	} else {
		$store_name = "";
		$people_name = "";
		$mail_address = "";
		$content = "";
	}
	if(Util::is_smartphone ()) {
		require './sp/page/inquiry.php';
	} else {
		require './pc/page/inquiry.php';
	}
}
