<?php
// お知らせ部品を読込み
require_once('./../common/component/NoticeComponent.php');
require_once('./../common/model/AdminModel.php');
require_once('./../common/model/ReserveModel.php');
require_once('./../common/model/InquiryModel.php');

/*
 * 支店機能向け お知らせ配信画面表示コントローラー
 */

// ブラウザから入力情報を取得
$stamp_select = isset($_POST['rally_id']) ? $_POST['rally_id'] : '';
$notice_title = isset($_POST['notice_title']) ? $_POST['notice_title'] : '';
$notice_date = isset($_POST['notice_date']) ? $_POST['notice_date'] : '';
$delivery_set = isset($_POST['delivery_set']) ? $_POST['delivery_set'] : '';
$delivery_num = isset($_POST['delivery_num']) ? $_POST['delivery_num'] : '';
$format = isset($_POST['format']) ? $_POST['format'] : '';
$simple_check = isset($_POST['simple_check']) ? $_POST['simple_check'] : '';
$individual_sex = isset($_POST['individual_sex']) ? $_POST['individual_sex'] : '';
$individual_birthday_start = isset($_POST['individual_birthday_start']) ? $_POST['individual_birthday_start'] : '';
$individual_birthday_end = isset($_POST['individual_birthday_end']) ? $_POST['individual_birthday_end'] : '';
$individual_region = isset($_POST['individual_region']) ? $_POST['individual_region'] : '';
$first_year_start = isset($_POST['first_year_start']) ? $_POST['first_year_start'] : '';
$first_month_start = isset($_POST['first_month_start']) ? $_POST['first_month_start'] : '';
$first_day_start = isset($_POST['first_day_start']) ? $_POST['first_day_start'] : '';
$first_start = isset($_POST['first_start']) ? $_POST['first_start'] : '';
$first_year_end = isset($_POST['first_year_end']) ? $_POST['first_year_end'] : '';
$first_month_end = isset($_POST['first_month_end']) ? $_POST['first_month_end'] : '';
$first_day_end = isset($_POST['first_day_end']) ? $_POST['first_day_end'] : '';
$first_end = isset($_POST['first_end']) ? $_POST['first_end'] : '';
$last_year_start = isset($_POST['last_year_start']) ? $_POST['last_year_start'] : '';
$last_month_start = isset($_POST['last_month_start']) ? $_POST['last_month_start'] : '';
$last_day_start = isset($_POST['last_day_start']) ? $_POST['last_day_start'] : '';
$last_start = isset($_POST['last_start']) ? $_POST['last_start'] : '';
$last_year_end = isset($_POST['last_year_end']) ? $_POST['last_year_end'] : '';
$last_month_end = isset($_POST['last_month_end']) ? $_POST['last_month_end'] : '';
$last_day_end = isset($_POST['last_day_end']) ? $_POST['last_day_end'] : '';
$last_end = isset($_POST['last_end']) ? $_POST['last_end'] : '';
$total_stamp_num = isset($_POST['total_stamp_num']) ? $_POST['total_stamp_num'] : '';
$total_stamp_terms = isset($_POST['total_stamp_terms']) ? $_POST['total_stamp_terms'] : '';

// スケジュール配信のパラメタ受取
// new_user_push_1
// new_user_push_2
// last_stamp_push_1
// last_stamp_push_2
// birthday_push_1
// birthday_push_2
// weekly_push
// monthly_push
// last_month_stamp_push
$schedule_type = isset($_POST['schedule_type']) ? $_POST['schedule_type'] : '';
$new_user_time_difference = isset($_POST['new_user_time_difference']) ? $_POST['new_user_time_difference'] : '';
$new_user_day_difference = isset($_POST['new_user_day_difference']) ? $_POST['new_user_day_difference'] : '';
$last_stamp_after_day = isset($_POST['last_stamp_after_day']) ? $_POST['last_stamp_after_day'] : '';
$total_stamps = isset($_POST['total_stamps']) ? $_POST['total_stamps'] : '';
$total_stamp_after_day = isset($_POST['total_stamp_after_day']) ? $_POST['total_stamp_after_day'] : '';
$before_day_of_birth = isset($_POST['before_day_of_birth']) ? $_POST['before_day_of_birth'] : '';
$day_of_the_week = isset($_POST['day_of_the_week']) ? $_POST['day_of_the_week'] : '';
$monthly_push_day_of_the_month = isset($_POST['monthly_push_day_of_the_month']) ? $_POST['monthly_push_day_of_the_month'] : '';
$last_month_stamp_num = isset($_POST['last_month_stamp_num']) ? $_POST['last_month_stamp_num'] : '';
$last_month_push_day = isset($_POST['last_month_push_day']) ? $_POST['last_month_push_day'] : '';
// スケジュール配信時間
$push_hour = isset($_POST['push_hour']) ? $_POST['push_hour'] : '';
$push_minute = isset($_POST['push_minute']) ? $_POST['push_minute'] : '';
$schedule_status = isset($_POST['schedule_status']) ? $_POST['schedule_status'] : '';

$edit_schedule_id = isset($_GET['edit_schedule_id']) ? $_GET['edit_schedule_id'] : '';

//お知らせ配信対象外のuser_id
$exclusion_user_id = isset($_POST['exclusion_user_id']) ? $_POST['exclusion_user_id'] : '';
// 支店選択
$branch_id = $_POST['branch_list'];
//キーワード検索
$db = db_connect();
$keyword = mysql_real_escape_string($_POST['search_keyword']);
db_close( $db );
//チェックページの処理
$check_id = isset($_GET['check_id']) ? $_GET['check_id'] : '';

//編集ページの処理
$edit_id = isset($_GET['edit_id']) ? $_GET['edit_id'] : '';

// $_GET['to_user_id']がある場合、個人宛配信なので、専用表示する
$to_user_id = isset($_GET['to_user_id']) ? $_GET['to_user_id'] : '';

// イメージファイル名(チラシの場合のみ使用)
$leaflets_file_name = isset($_POST['leaflets_file_name']) ? $_POST['leaflets_file_name'] : '';

// 送信スタップのadmin_id
$staff_admin_id = isset($_POST['staff_admin_id']) ? $_POST['staff_admin_id'] : '';

// ポストしたformがどれかを判断可能
// 'notice':お知らせ
// 'blog':ブログ
// 'leaflets':チラシ
// 'to_person':個人宛
// 'schedule':スケジュール
$delivery_type = isset($_POST['delivery_type']) ? $_POST['delivery_type'] : '';

// 時間指定関連の値の取得
// 時間指定有無
$reserve_check = isset($_POST['reserve_check']) ? $_POST['reserve_check'] : "0";

$reserve_year = isset($_POST['reserve_year']) ? $_POST['reserve_year'] : '';
$reserve_month = isset($_POST['reserve_month']) ? $_POST['reserve_month'] : '';
$reserve_day = isset($_POST['reserve_day']) ? $_POST['reserve_day'] : '';
$reserve_hour = isset($_POST['reserve_hour']) ? $_POST['reserve_hour'] : '';
$reserve_minute = isset($_POST['reserve_minute']) ? $_POST['reserve_minute'] : '';

// 仮投稿チェック有無
$pre_save_check = isset($_POST['pre_save_check']) ? $_POST['pre_save_check'] : "0";

// FB連動チェック有無
$sns_fb_check = isset($_POST['sns_fb_check']) ? $_POST['sns_fb_check'] : "0";
error_log("sns_fb_check:".$sns_fb_check);
// Twitter連動チェック有無
$sns_tw_check = isset($_POST['sns_tw_check']) ? $_POST['sns_tw_check'] : "0";
error_log("sns_tw_check:".$sns_tw_check);


// ----------------------------------------------------- //
// 処理開始
// ----------------------------------------------------- //
// お知らせ部品
$noticeComp = new NoticeComponent();
$adminModel = new AdminModel();

// お知らせ削除
$dele_id = isset($_GET['dele_id']) ? $_GET['dele_id'] : '';
if (!empty($dele_id)) {
	$noticeComp->delete_notice(NULL, $dele_id);
}

// お知らせスケジュール削除
$dele_schedule_id = isset($_GET['dele_schedule_id']) ? $_GET['dele_schedule_id'] : '';
if (!empty($dele_schedule_id)) {
	$noticeComp->delete_notice_schedule(NULL, $dele_schedule_id);
}

//すべての人数取得
$db = db_connect();
$branch_ids = null;
if ($_SESSION["branchFlag"] == OWNER) {
	// 全員
	$send_admin = ADMIN_ID;
	$branch_ids = '0';
} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] == 0)){
	// 全員
	$send_admin = ADMIN_ID;
	$branch_ids = '0';
} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] != 0)){
	// 単一ブランチ
	$send_admin = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	$branch_ids = $_SESSION["branchId"];
} else if ($_SESSION["branchFlag"] == BRANCH_MANAGER) {
	// 単一ブランチ
	$send_admin = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	$branch_ids = $_SESSION["branchId"];
} else if (($_SESSION["branchFlag"] == ORG_MANAGER) && ($_SESSION["branchId"] != 0)){
	// 単一ブランチ
	$send_admin = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	$branch_ids = $_SESSION["branchId"];
} else if ($_SESSION["branchFlag"] == ORG_MANAGER) {
	$org = $adminModel->get_organization_by_id($db, $_SESSION["orgId"]);
	// 組織IDからadmin_idを取得
	$send_admin = $org['admin_id'];
	
	// 組織IDから対象のブランチidを取得する
	$branch_list = $adminModel->get_branch_by_organization_id($db, $_SESSION["orgId"]);
	$branch_id_list = array_column($branch_list, 'id');
	$branch_ids = implode(",", $branch_id_list);
} else if ($_SESSION["branchFlag"] == STAFF) {
	// スタッフが所属する支店の支店IDを取得する
	// スタッフのadmin_idを設定
	$staff = $adminModel->get_staff_by_id($db, $_SESSION["staffId"]);
	$send_admin = $staff['admin_id'];
	$branch_ids = $staff['branch_id'];
}

//echo "send_admin:".$send_admin."<BR>";

// 必須検索条件
$where = " u.user_id = r.user_id AND r.admin_id = ".ADMIN_ID;
// 支店リストを保持している場合、さらに絞り込み条件を付加
if (!empty($branch_ids)) {
	// 対象の支店IDリストがあるものは、支店IDを条件に追加する
	$where .= " AND r.branch_id IN( ".$branch_ids.")";
}
//
//// 支店IDを保持している場合は支店内のユーザが対象になる
//if ($_SESSION["branchId"] == 0) {
//        $where = " u.user_id = r.user_id AND r.admin_id = ".ADMIN_ID;
//} else {
//        $where = " u.user_id = r.user_id AND r.admin_id = ".ADMIN_ID." AND r.branch_id = ".$_SESSION["branchId"];
//}
$rally_user_date = all_user_information_select($db , $where);
$rally_user_rows = mysql_num_rows($rally_user_date);
$all_customer = $rally_user_rows;

// ラリーユーザデータを取得
$user_list = $adminModel->get_dbdata_array($rally_user_date);
db_close( $db );


// それぞれの支店や本部アカウントで管理画面にログインした時
// ブログ配信での店員選択で、それぞれの支店や本部に所属している店員のみ表示する。
$db = db_connect();
$rally_date = rally_select($db , "admin_id = ".ADMIN_ID);
$rally = mysql_fetch_array($rally_date);
$rally_id = $rally['rally_id'];
//$where = "rally_id = ".$rally['rally_id']." AND status = '1'";
if(empty($branch_ids)){
	$branch_ids = '0';
}
// ラリーIDで全スタッフ取得
$staffs = $adminModel->get_staff_by_rally_id($db, $rally_id);
// 全スタッフが全て取れているので、ログイン権限毎にブログ配信で選択リストに表示されるスタッフを絞り込む
if ($_SESSION["branchFlag"] == OWNER) {
	// 全員
	$staff_list = $staffs;
} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] == 0)){
	// オーナー直下のスタッフ
	$staff_list = $adminModel->filter_staff_by_under_owner($staffs);
} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] != 0)){
	// 単一ブランチ
	$branch_ids = $_SESSION["branchId"];
	$staff_list = $adminModel->filter_staff_by_branch_id($staffs, $branch_ids);
} else if ($_SESSION["branchFlag"] == BRANCH_MANAGER) {
	// 単一ブランチ
	$branch_ids = $_SESSION["branchId"];
	$staff_list = $adminModel->filter_staff_by_branch_id($staffs, $branch_ids);
} else if (($_SESSION["branchFlag"] == ORG_MANAGER) && ($_SESSION["branchId"] != 0)){
	// 単一ブランチ
	$branch_ids = $_SESSION["branchId"];
	$staff_list = $adminModel->filter_staff_by_branch_id($staffs, $branch_ids);
} else if ($_SESSION["branchFlag"] == ORG_MANAGER) {
	// 組織IDから組織直下取得
	$staff_list = $adminModel->filter_staff_by_organization_id($staffs, $_SESSION["orgId"]);
} else if ($_SESSION["branchFlag"] == STAFF) {
	// スタッフが所属する支店の支店IDを取得する
	// 現状未対応
}

$staff_name = "-";
foreach ($staff_list as $staff) {
	if ($staff['admin_id'] == $staff_admin_id) {
		$staff_name = $staff['nickname'];
		break;
	}
}
db_close( $db );


// 編集処理の場合
if(isset($_POST['edit'])){
	
	$db = db_connect();
	$where = "notice_id = '".$edit_id."'";
	$set = "notice_title = '".$notice_title."' , notice_content = '".$notice_date."'";
	notice_db_edit($db , $set ,$where);
	db_close( $db );
	
	// 最初に戻る
	header("Location: ./?p=notice");
	exit;
}

// スケジュール配信の編集
if(isset($_POST['schedule_edit'])){
	
	$db = db_connect();
	$where = "id = '".$edit_schedule_id."'";
	$set = "title = '".$notice_title."' , content = '".$notice_date."' , status = '".$schedule_status."' ";
	error_log("set::".$set);
	notice_schedule_update($db , $set ,$where);
	db_close( $db );
	
	// 最初に戻る
	header("Location: ./?p=notice&tab_index=3");
	exit;
}

/*
 * 履歴から確認ページへ遷移
 */
if (!empty($check_id) || !empty($edit_id)) {
	if (!empty($check_id)) {
		$id = $check_id;
	} else {
		$id = $edit_id;
	}
	$db = db_connect();
	$where = "notice_id =" . $id;
	$notice_select_date = notice_select($db, $where);
	$notice_date = mysql_fetch_array($notice_select_date);
	db_close($db);
	if ($notice_date['delivery_set'] == 1) {
		$delivery_set_name = "一斉配信";
		$delivery_set = "all";
	} else if ($notice_date['delivery_set'] == 2) {
		$delivery_set_name = "簡単配信";
		$delivery_set = "simple";
		if ($notice_date['simple_last_month'] == 1) {
			$simple_check[] = "last_month";
		}
		if ($notice_date['simple_this_month'] == 1) {
			$simple_check[] = "this_month";
		}
		if ($notice_date['simple_next_month'] == 1) {
			$simple_check[] = "next_month";
		}
		if ($notice_date['simple_sex'] == 1) {
			$simple_check[] = "man";
			$simple_check[] = "woman";
		}
		if ($notice_date['simple_sex'] == 2) {
			$simple_check[] = "man";
		}
		if ($notice_date['simple_sex'] == 3) {
			$simple_check[] = "woman";
		}
		if ($notice_date['yesterday_come'] == 1) {
			$simple_check[] = "yesterday_come";
		}
		if ($notice_date['month_come'] == 1) {
			$simple_check[] = "month_come";
		}
		if ($notice_date['this_month_come'] == 1) {
			$simple_check[] = "this_month_come";
		}
	} else if ($notice_date['delivery_set'] == 3) {
		$delivery_set_name = "個別配信";
		$delivery_set = "individual";
		switch ($notice_date['individual_sex']) {
			case 1; //全て
				$individual_sex_date = "全て";
				break;
			case 2; //男性
				$individual_sex_date = "男性";
				break;
			case 3;  //女性
				$individual_sex_date = "女性";
				break;
			default;
				break;
		}
		$individual_birthday_start = $notice_date['individual_birthday_start'];
		$individual_birthday_end = $notice_date['individual_birthday_end'];
		$individual_region = $notice_date['individual_region'];
		if (empty($individual_region)) {
			$individual_region = ALL_REGION_LABEL;
		}
		$first_start_array = explode("-", $notice_date['first_start']);
		$first_year_start = $first_start_array[0];
		$first_month_start = $first_start_array[1];
		$first_day_start = $first_start_array[2];
		$first_end_array = explode("-", $notice_date['first_end']);
		$first_year_end = $first_end_array[0];
		$first_month_end = $first_end_array[1];
		$first_day_end = $first_end_array[2];
		$last_start_array = explode("-", $notice_date['last_start']);
		$last_year_start = $last_start_array[0];
		$last_month_start = $last_start_array[1];
		$last_day_start = $last_start_array[2];
		$last_end_array = explode("-", $notice_date['last_end']);
		$last_year_end = $last_end_array[0];
		$last_month_end = $last_end_array[1];
		$last_day_end = $last_end_array[2];
		$total_stamp_num = $notice_date['total_stamp_num'];
		if ($notice_date['total_stamp_terms'] == "1") {
			$total_stamp_terms = "or_more";
		} else if ($notice_date['total_stamp_terms'] == "2") {
			$total_stamp_terms = "downward";
		}
	}
	
	$notice_type = $notice_date['notice_type'];
	if ($notice_date['notice_type'] == 1) {
		$delivery_set_name = "チラシ配信";  //個人宛配信
		$delivery_type = "leaflets";
		$leaflets_file_name = $notice_date['thumbnail'];
	} else if ($notice_date['notice_type'] == 2) {
		$staff_admin_id = $notice_date['admin_id'];
		$delivery_set_name = "ブログ配信";  //個人宛配信
		$delivery_type = "blog";
	} else if ($notice_date['notice_type'] == 3) {
		$delivery_set_name = "個人宛配信";  //個人宛配信
		$delivery_type = "to_person";
	} else if ($notice_date['notice_type'] == 4) {
		$delivery_set_name = "スケジュール配信";  //個人宛配信
		$delivery_type = "schedule";
	} else if ($notice_date['notice_type'] == 5) {
		$delivery_set_name = "予約通知";  // アプリ予約通知
		$delivery_type = "reserve";
	}

	//************************************ 配信形式 ************************************//
	if ($notice_date['format'] == 1) {
		$format = array("push", "mail");
	} else if ($notice_date['format'] == 2) {
		$format = array("push");
	} else if ($notice_date['format'] == 3) {
		$format = array("mail");
	}

	//************************************ 配信人数 ************************************//
	$delivery_num = $notice_date['delivery_num'];

	//************************************ 配信時点のユーザ総数 ************************************//
	$all_customer = $notice_date['total_user_count'];
	
	//************************************ 配信タイトル ************************************//
	$notice_title = $notice_date['notice_title'];

	//************************************ 配信時間指定 ************************************//
	if ($notice_date['acceptance_datetime'] == $notice_date['notice_data']) {
		// 予約指定なし
		$reserve_check = 0;
	} else {
		// 予約指定あり
		$reserve_check = 1;
		// datetime型から値を取り出し
		list($date, $time) = explode(" ", $notice_date['notice_data']);
		list($reserve_year, $reserve_month, $reserve_day) = explode("-", $date);
		list($reserve_hour, $reserve_minute, $second) = explode(":", $time);
	}

	//************************************ 配信状態 ************************************//
	if ($notice_date['transition'] != '1') {
		// 仮投稿
		$pre_save_check = 0;
	} else {
		// 仮投稿なし
		$pre_save_check = 1;
	}

	// SNS連携
	$sns_fb_check = $notice_date['sns_fb_check'];
	$sns_tw_check = $notice_date['sns_tw_check'];

	$db = db_connect();
	$send_admin = $notice_date['admin_id'];
	$admin_type = $noticeComp->get_admin_type($db, $send_admin);
	if ($admin_type == OWNER) {
		// オーナーの場合、branch_idなし
		$staff_name = 'オーナーアカウント';
	} else if ($admin_type == BRANCHES_OWNER) {
		$staff_name = '全店管理アカウント';
	} else if ($admin_type == BRANCH_MANAGER) {
		// 支店の店長の場合、支店のadminを設定
		// admin_idから支店名を取得
		$branch = $adminModel->get_branch_by_admin_id($db, $send_admin);
		$staff_name = $branch['name'].'管理アカウント';
	} else if ($admin_type == STAFF) {
		// スタッフが所属する支店を取得
		// admin_idからスタッフ名を取得する
		$staff = $adminModel->get_staff_by_admin_id($db, $send_admin);
		$staff_name = $staff['nickname'];
	}
	db_close($db);
	
	//************************************ 配信内容 ************************************//
	$notice_date = $notice_date['notice_content'];

	if (Util::is_smartphone()) {
		if (!empty($check_id)) {
			require "./sp/page/notice_check.php";
		} else {
			require "./sp/page/notice_edit.php";
		}
	} else {
		if (!empty($check_id)) {
			require "./pc/page/notice_check.php";
		} else {
			require "./pc/page/notice_edit.php";
		}
	}
	exit;
}

/*
 * スケジュール配信一覧から編集ページへ遷移
 */
if(!empty($edit_schedule_id)){
	
		if (!empty($edit_schedule_id)) {
			$id = $edit_schedule_id;
		}
		$db = db_connect();
		// 対象の配信スケジュールを取得する。
		$where = "id = '".$id."'";
		$notice_schedule_data = notice_schedule_select($db , $where);
		$schedule = mysql_fetch_array($notice_schedule_data);

		$schedule_type = $schedule['schedule_type'];
		$new_user_time_difference = $schedule['new_user_time_difference'];
		$new_user_day_difference = $schedule['new_user_day_difference'];
		$last_stamp_after_day = $schedule['last_stamp_after_day'];
		$total_stamps = $schedule['total_stamps'];
		$total_stamp_after_day = $schedule['total_stamp_after_day'];
		$before_day_of_birth = $schedule['before_day_of_birth'];
		$day_of_the_week = $schedule['day_of_the_week'];
		$monthly_push_day_of_the_month = $schedule['monthly_push_day_of_the_month'];
		if ($monthly_push_day_of_the_month == 'last_day') {
				$monthly_push_day_of_the_month = '月末';
		}
		
		$last_month_stamp_num = $schedule['last_month_stamp_num'];
		$last_month_push_day = $schedule['last_month_push_day'];
		if ($last_month_push_day == 'last_day') {
				$last_month_push_day = '月末';
		}
		$push_hour = $schedule['push_hour'];
		$push_minute = $schedule['push_minute'];
		$schedule_status = $schedule['status'];
		
		$schedule_title = $schedule['title'];
		$schedule_content = $schedule['content'];
		// PUSH or Mail 配信形式設定
		$format_no = $schedule['format'];
		
		db_close($db);
		
		
		$delivery_set_name = "スケジュール配信";
		$schedule_type_name = '';
		$schedule_type_comment = '';
		
		if(!empty($schedule_type)) {
			// スケジュールタイプ毎に対比
			$typeinfo = $noticeComp->schedule_list[$schedule_type];
			$schedule_type_name = $typeinfo['name'];

			if ($schedule_type == 'new_user_push_1') {
				if ($new_user_time_difference >= 180) {
					$display_time = ($new_user_time_difference/60)."時間";
				}else {
					$display_time = $new_user_time_difference."分";
				}
				$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type, $display_time);
			} else if ($schedule_type == 'new_user_push_2') {
				$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type, $new_user_day_difference);
			} else if ($schedule_type == 'last_stamp_push_1') {
				$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type, $last_stamp_after_day);
			} else if ($schedule_type == 'last_stamp_push_2') {
				$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type, $total_stamps, $total_stamp_after_day);
			} else if ($schedule_type == 'birthday_push_1') {
				$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type, $before_day_of_birth);
			} else if ($schedule_type == 'birthday_push_2') {
				$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type);
			} else if ($schedule_type == 'weekly_push') {
				$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type, $noticeComp->weekly[$day_of_the_week]);
			} else if ($schedule_type == 'monthly_push') {
				$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type, $monthly_push_day_of_the_month);
			} else if ($schedule_type == 'last_month_stamp_push') {
				$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type, $last_month_stamp_num, $last_month_push_day);
			}
		}

		
		if(Util::is_smartphone ()) {
			require "./sp/page/notice_schedule_edit.php";
		} else {
			require "./pc/page/notice_schedule_edit.php";
		}
		exit;
}

if (isset($_SESSION['asp']['notice']['transition']) && $_SESSION['asp']['notice']['transition'] == 'complete') {
	// セッション情報が入力されていて、送信状態が'complete'の場合
	// ユーザによる中断を無視する
	ignore_user_abort(true);
	// タイムアウトしないようにする
	set_time_limit(0);

	$p_admin_id = ADMIN_ID;
	if (empty($branch_ids)) {
		// 支店IDが存在して、且つ、支店権限で実行する場合
		$branch_ids = NULL;
	}
	Util::notice_message_v2($p_admin_id, $_SESSION['asp']['notice']['data'], $branch_ids);

	// 予約や問合せからの返信の場合,送信が完了したら起点の予約/問合せページへ遷移
	$reserve_id = isset($_REQUEST['reserve_id']) ? $_REQUEST['reserve_id'] : '';
	$inquiry_id = isset($_REQUEST['inquiry_id']) ? $_REQUEST['inquiry_id'] : '';
	
	// 配信先を取得する
	// お知らせ情報取得
	$notice = $_SESSION['asp']['notice']['data'];
	$sns_type = $noticeComp->get_feed_sns_type($notice);
	error_log("sns_type:" . $sns_type);
	if ($sns_type == "-1") {
		unset($_SESSION['asp']['notice']);
		if (Util::is_smartphone()) {
			require "./sp/page/notice_end.php";
		} else {
			require "./pc/page/notice_end.php";
		}
		
	} else {
		// SNS連携処理を実行
		$_SESSION['asp']['notice']['transition'] = 'feed_sns';
		header('Location: ' . $_SERVER['REQUEST_URI']);
		exit;
	}
} else if (isset($_SESSION['asp']['notice']['transition']) && $_SESSION['asp']['notice']['transition'] == 'save') {
	// 保存(未送信状態)
	unset($_SESSION['asp']['notice']);
	if (Util::is_smartphone()) {
		require "./sp/page/notice_end.php";
	} else {
		require "./pc/page/notice_end.php";
	}
} else if (isset($_SESSION['asp']['notice']['transition']) && $_SESSION['asp']['notice']['transition'] == 'feed_sns') {
	error_log("call feed_sns:");
	// sns処理
	// 未ログインの場合、処理の途中でSNS側のログインへ飛んでcallbackで戻ってくるので、transitionを分けて、何度リダイレクトされても正常動作する作りにする
	// 
	// sns処理の最初にフラグを立てる
	if (!isset($_SESSION['asp']['notice']['sns'])) {
		// snsフィード処理の初期化
		SnsUtility::init_sns_feed();
		$_SESSION['asp']['notice']['sns'] = '1';
	}

	if (SnsUtility::is_facebook_feed_cancel_request()) {
		// Facebookキャンセルリクエストの場合、キャンセルフラグを立てる
		// 以降SnsUtility::send_message_sns()内でfacebook処理が回避される
		SnsUtility::facebook_feed_cancel();
		error_log("is_facebook_feed_cancel_request!!!!!!");
	}

	// お知らせ情報取得
	$notice = $_SESSION['asp']['notice']['data'];

	// 本文内の画像ファイル名を取得
	$img_file_names = $noticeComp->get_img_file_names($notice['notice_content']);

	$title = $notice['notice_title'];
	$body = $notice['notice_content'];

	// 配信先を取得する
	$sns_type = $noticeComp->get_feed_sns_type($notice);

	//
	$like_url = DOMAIN . FILE_NAME . "/public/notice/notice.php?notice_id=" . $notice['notice_id'];

	SnsUtility::send_message_sns($notice['send_admin'], $title, $body, $img_file_names[0], null, $sns_type, $like_url, null);

	$error_title = SnsUtility::get_error_title();
	$error_message = SnsUtility::get_error_message();
	if (!empty($error_message)) {
		$error_title .= "<br>" . $error_message;
	}

	unset($_SESSION['asp']['notice']);
	if (Util::is_smartphone()) {
		require "./sp/page/notice_end.php";
	} else {
		require "./pc/page/notice_end.php";
	}
} else if (isset($_POST['check'])) {
	// 入力フォームから確認画面へ遷移
	// ブログとチラシは配信対象が全てなので、送信数＝顧客数
	if (($delivery_type == 'blog') || ($delivery_type == 'leaflets')) {
		$delivery_num = $all_customer;
	} else if ($delivery_type == 'to_person') {
		$delivery_num = 1;
	}

	switch ($delivery_set) {
		case "all"; //一斉配信
			$delivery_set_name = "一斉配信";
			break;
		case "simple"; //簡単配信
			$delivery_set_name = "簡単配信";
			break;
		case "individual";  //個別配信
			$delivery_set_name = "個別配信";
			break;
		case "blog";  //個別配信
			$delivery_set_name = "ブログ配信";
			break;
		case "leaflets";  //個別配信
			$delivery_set_name = "チラシ配信";
			break;
		case "to_person";  //個人宛配信
			$delivery_set_name = "個人宛配信";
			break;
		case "schedule";  //個人宛配信
			$delivery_set_name = "スケジュール配信";
			break;
		default;
			break;
	}
	switch ($individual_sex) {
		case "all"; //全て
			$individual_sex_date = "全て";
			break;
		case "man"; //男性
			$individual_sex_date = "男性";
			break;
		case "woman";  //女性
			$individual_sex_date = "女性";
			break;
		default;
			break;
	}
	error_log("除外したuser_id : " . $exclusion_user_id);
	//店舗名の表示
	if ($branch_id == 0) {
		$branch_name = "指定なし";
	} else if ($branch_id == -1) {
		$branch_name = "店舗未登録";
	} else {
		$db = db_connect();
		//店舗名の取得
		$branch_name = $noticeComp->get_storename_by_branch_id($db, $branch_id);
		db_close($db);
	}
	error_log("NoticeController check keyword : " . $keyword);
	error_log("NoticeController check branch_id : " . $branch_id);

	$_SESSION['asp']['notice']['transition'] = 'check';
	if (Util::is_smartphone()) {
		require "./sp/page/notice_check.php";
	} else {
		require "./pc/page/notice_check.php";
	}
} else if (isset($_POST['setting'])) {
	
	$addpath = '';
	// 予約や問合せからの返信
	$reserve_id = isset($_REQUEST['reserve_id']) ? $_REQUEST['reserve_id'] : '';
	if (!empty($reserve_id)) {
		$reserveModel = new ReserveModel();
		$db = db_connect();
		$reserveModel->change_status($db, $reserve_id, ReserveModel::$RESERVE_STATUS_REPLY);
		db_close($db);
		
		// 個人宛の予約返信にタイプを変更
		$delivery_type = "reserve";
		$addpath .= '&reserve_id='.$reserve_id;
	}
	$inquiry_id = isset($_REQUEST['inquiry_id']) ? $_REQUEST['inquiry_id'] : '';
	if (!empty($inquiry_id)) {
		$inquiryModel = new InquiryModel();
		$db = db_connect();
		$inquiryModel->change_status($db, $inquiry_id, InquiryModel::$INQUIRY_STATUS_REPLY);
		db_close($db);
		
		// 個人宛の予約返信にタイプを変更
		$delivery_type = "reserve";
		$addpath .= '&inquiry_id='.$inquiry_id;
	}

	error_log("NoticeController setting keyword : " . $keyword);
	error_log("NoticeController setting branch_id : " . $branch_id);

	// 確認画面にてお知らせ送信
	if (isset($_SESSION['asp']['notice']['transition']) && $_SESSION['asp']['notice']['transition'] != 'check') {
		header('Location: ' . $_SERVER['REQUEST_URI']);
		exit;
	}
	$now_day = date('Y-m-d H:i:s');  //配信日時

	switch ($delivery_set) {
		case "all"; //一斉配信
			$delivery_set_no = 1;
			break;
		case "simple"; //簡単配信
			$delivery_set_no = 2;
			break;
		case "individual";  //個別配信
			$delivery_set_no = 3;
			break;
		case "blog";  //ブログ配信
			// 条件なし 一斉
			$delivery_set_no = 1;
			break;
		case "leaflets";  //チラシ配信
			// 条件なし 一斉
			$delivery_set_no = 1;
			break;
		case "to_person";  //個別配信
			$delivery_set_no = 5;
			break;
		default;
			break;
	}

	// デフォは両方
	$format_no = 1;
	if (in_array("push", $format) && in_array("mail", $format)) {
		$format_no = 1;
	} else if (in_array("push", $format)) {
		$format_no = 2;
	} else if (in_array("mail", $format)) {
		$format_no = 3;
	}

	// 強制措置
	if ($delivery_type == "leaflets") {
		// チラシはメール配信しないので2に変更
		$format_no = 2;
	}

	$simple_last_month = 0;
	if (in_array("last_month", $simple_check)) {
		$simple_last_month = 1;
	}
	$simple_this_month = 0;
	if (in_array("this_month", $simple_check)) {
		$simple_this_month = 1;
	}
	$simple_next_month = 0;
	if (in_array("next_month", $simple_check)) {
		$simple_next_month = 1;
	}
	$simple_sex = 0;
	if (in_array("man", $simple_check) && in_array("woman", $simple_check)) {
		$simple_sex = 1;
	} else if (in_array("man", $simple_check)) {
		$simple_sex = 2;
	} else if (in_array("woman", $simple_check)) {
		$simple_sex = 3;
	}
	$yesterday_come = 0;
	if (in_array("yesterday_come", $simple_check)) {
		$yesterday_come = 1;
	}
	$month_come = 0;
	if (in_array("month_come", $simple_check)) {
		$month_come = 1;
	}
	$this_month_come = 0;
	if (in_array("this_month_come", $simple_check)) {
		$this_month_come = 1;
	}

	$individual_sex_no = 1;
	switch ($individual_sex) {
		case "all"; //全て
			$individual_sex_no = 1;
			break;
		case "man"; //男性
			$individual_sex_no = 2;
			break;
		case "woman";  //女性
			$individual_sex_no = 3;
			break;
		default;
			break;
	}
	if ($total_stamp_terms == "or_more") {
		$total_stamp_terms_no = 1;
	} else if ($total_stamp_terms == "downward") {
		$total_stamp_terms_no = 2;
	}

	$notice_type = 0;
	if ($delivery_type == "leaflets") {
		// チラシの場合 1
		$notice_type = 1;
	} else if ($delivery_type == "blog") {
		// BLOGの場合 2
		$notice_type = 2;
	} else if ($delivery_type == "to_person") {
		// 個人宛の場合 3
		$notice_type = 3;
	} else if ($delivery_type == "schedule") {
		// スケジュールの場合 4
		$notice_type = 4;
	} else if ($delivery_type == "reserve") {
		// スケジュールの場合 5
		$notice_type = 5;
	}

	// お知らせ送信するadminを設定し
	// 送信時点での送信可能な総ユーザ数を取得する。
//	$send_admin = 0;
//	if ($_SESSION["branchId"] == 0) {
//		// 支店でない場合、オーナーのADMINを設定
//		$send_admin = ADMIN_ID;
//		$db = db_connect();
//		// 支店でない場合、admin_idをキーにadminに属する総ユーザ数を取得する。
//		$count_users = get_rally_user_count_by_admin_id($db, $send_admin);
//		db_close($db);
//	} else {
//		// 支店の店長の場合、支店のadminを設定
//		$db = db_connect();
//		$child_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
//		$send_admin = $child_admin_id;
//		// 支店の場合、branch_idをキーに支店の総ユーザ数を取得する
//		$count_users = get_rally_user_count_by_branch_id($db, $_SESSION["branchId"]);
//		db_close($db);
//	}

	// スタッフのadmin_idが設定されている場合
	if ($staff_admin_id != 0) {
		// 送信者をスタッフのadmin_idに変更する
		$send_admin = $staff_admin_id;
	}
	// 予約指定がある場合、送信時間に予約時間を設定する
	$send_datetime = date('Y-m-d H:i:s', strtotime($now_day));  //配信日時
	if ($reserve_check == 1) {
		$send_datetime = $reserve_year . "-" . $reserve_month . "-" . $reserve_day . " " . $reserve_hour . ":" . sprintf("%02d", $reserve_minute) . ":00";
	}

//	error_log("reserve_check:" . $reserve_check);
//	error_log("send_datetime:" . $send_datetime);

	$db = db_connect();
	$rally_date = rally_select($db, "admin_id = " . ADMIN_ID . " LIMIT 0,1");
	$rally = mysql_fetch_array($rally_date);
	$rally_id = $rally['rally_id'];
	db_close($db);

	$db = db_connect();
//        $into = "'".$rally_id."' , '".mysql_real_escape_string($notice_date)."' , '".$send_admin."' , '".$send_datetime."' , '".mysql_real_escape_string($notice_title)."' , '".$delivery_set_no."' , '".$format_no."' , '".$simple_last_month."' , '".$simple_this_month."' , '".$simple_next_month."' , '".$simple_sex."' , '".$yesterday_come."' , '".$month_come."' , '".$this_month_come."' , '".$individual_sex_no."' , '".$individual_birthday_start."' , '".$individual_birthday_end."' , '".$individual_region."' , '".$first_start."' , '".$first_end."' , '".$last_start."' , '".$last_end."' , '".mysql_real_escape_string($total_stamp_num)."' , '".$total_stamp_terms_no."' , '".$delivery_num."', '".$notice_type."' , '".$leaflets_file_name."' , '".$count_users."' , '".$now_day."'";
//        $col_num = 29;
//        notice_insert($db , $into, $col_num);
	$set = "rally_id = '" . $rally_id . "', "
		. "notice_content = '" . mysql_real_escape_string($notice_date) . "', "
		. "admin_id = '" . $send_admin . "', "
		. "notice_data = '" . $send_datetime . "', "
		. "notice_title = '" . mysql_real_escape_string($notice_title) . "', "
		. "delivery_set = '" . $delivery_set_no . "', "
		. "format = '" . $format_no . "', "
		. "simple_last_month = '" . $simple_last_month . "', "
		. "simple_this_month = '" . $simple_this_month . "', "
		. "simple_next_month = '" . $simple_next_month . "', "
		. "simple_sex = '" . $simple_sex . "', "
		. "yesterday_come = '" . $yesterday_come . "', "
		. "month_come = '" . $month_come . "', "
		. "this_month_come = '" . $this_month_come . "', "
		. "individual_sex = '" . $individual_sex_no . "', "
		. "individual_birthday_start = '" . $individual_birthday_start . "', "
		. "individual_birthday_end = '" . $individual_birthday_end . "', "
		. "individual_region = '" . $individual_region . "', "
		. "first_start = '" . $first_start . "', "
		. "first_end = '" . $first_end . "', "
		. "last_start = '" . $last_start . "', "
		. "last_end = '" . $last_end . "', "
		. "total_stamp_num = '" . mysql_real_escape_string($total_stamp_num) . "', "
		. "total_stamp_terms = '" . $total_stamp_terms_no . "', "
		. "delivery_num = '" . $delivery_num . "', "
		. "notice_type = '" . $notice_type . "', "
		. "thumbnail = '" . $leaflets_file_name . "', "
		. "total_user_count = '" . $all_customer . "', "
		. "acceptance_datetime = '" . $now_day . "', "
		. "to_user_id = '" . $to_user_id . "', "
		. "condition_branch_id = '" . $branch_id . "', "
		. "keyword = '" . $keyword . "', "
		. "exclusion_user_id = '" . $exclusion_user_id . "', "
		. "sns_fb_check = '" . $sns_fb_check . "', "
		. "sns_tw_check = '" . $sns_tw_check . "', "
		. "reserve_id = '" . (empty($reserve_id) ? '0' : $reserve_id) . "', "
		. "inquiry_id = '" . (empty($inquiry_id) ? '0' : $inquiry_id) . "', "
		. "transition = '" . $pre_save_check . "'";

	notice_set_insert($db, $set);
	$notice_id = mysql_insert_id();
	db_close($db);

	if ($pre_save_check == '1') {
		// 仮投稿なので配信処理はしない
		$noticeComp->pre_notice_save($notice_id);
	} else {
		// リダイレクト前にセッションに配信内容を設定する
		$noticeComp->pre_notice($notice_id);
	}
	
	header('Location: ' . $_SERVER['REQUEST_URI'].$addpath);
} else if (isset($_POST['schedule_check'])) {
	// スケジュール配信処理from→確認画面
	// 確認画面は固有のものを使用する
	// PUSH or Mail 配信形式設定
	$format_no = 0;
	if (in_array("push", $format) && in_array("mail", $format)) {
		$format_no = 1;
	} else if (in_array("push", $format)) {
		$format_no = 2;
	} else if (in_array("mail", $format)) {
		$format_no = 3;
	}

	$delivery_set_name = "スケジュール配信";
	$schedule_type_name = '';
	$schedule_type_comment = '';

	if (!empty($schedule_type)) {
		// スケジュールタイプ毎に対比
		$typeinfo = $noticeComp->schedule_list[$schedule_type];
		$schedule_type_name = $typeinfo['name'];

		if ($schedule_type == 'new_user_push_1') {
			if ($new_user_time_difference >= 180) {
				$display_time = ($new_user_time_difference / 60) . "時間";
			} else {
				$display_time = $new_user_time_difference . "分";
			}
			$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type, $display_time);
		} else if ($schedule_type == 'new_user_push_2') {
			$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type, $new_user_day_difference);
		} else if ($schedule_type == 'last_stamp_push_1') {
			$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type, $last_stamp_after_day);
		} else if ($schedule_type == 'last_stamp_push_2') {
			$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type, $total_stamps, $total_stamp_after_day);
		} else if ($schedule_type == 'birthday_push_1') {
			$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type, $before_day_of_birth);
		} else if ($schedule_type == 'birthday_push_2') {
			$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type);
		} else if ($schedule_type == 'weekly_push') {
			$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type, $noticeComp->weekly[$day_of_the_week]);
		} else if ($schedule_type == 'monthly_push') {
			$push_day_display = $monthly_push_day_of_the_month;
			if ($monthly_push_day_of_the_month == 'last_day') {
				$push_day_display = '月末';
			}
			$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type, $push_day_display);
		} else if ($schedule_type == 'last_month_stamp_push') {
			$push_day_display = $last_month_push_day;
			if ($last_month_push_day == 'last_day') {
				$push_day_display = '月末';
			}
			$schedule_type_comment = $noticeComp->convertScheduleCommentTag($noticeComp->schedule_list, $schedule_type, $last_month_stamp_num, $push_day_display);
		}
	}

	if (Util::is_smartphone()) {
		require "./sp/page/notice_schedule_check.php";
	} else {
		require "./pc/page/notice_schedule_check.php";
	}
} else if (isset($_POST['schedule_setting'])) {

	// PUSH or Mail 配信形式設定
	$format_no = 0;
	if (in_array("push", $format) && in_array("mail", $format)) {
		$format_no = 1;
	} else if (in_array("push", $format)) {
		$format_no = 2;
	} else if (in_array("mail", $format)) {
		$format_no = 3;
	}

	$db = db_connect();
	// お知らせ送信するadminを設定する
	$send_admin = 0;
	if ($_SESSION["branchFlag"] == OWNER) {
		// １店舗のみ（支店機能なし）のオーナー
		$send_admin = ADMIN_ID;
	} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] == 0)){
		// 支店機能のオーナー
		$send_admin = ADMIN_ID;
	} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] != 0)){
		// オーナーが支店モード機能で操作しているので支店権限で発行する
		$send_admin = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	} else if ($_SESSION["branchFlag"] == BRANCH_MANAGER) {
		// 支店の店長
		$send_admin = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	} else if (($_SESSION["branchFlag"] == ORG_MANAGER) && ($_SESSION["branchId"] != 0)){
		// 組織オーナーが支店モード機能で操作しているので支店権限で発行する
		$send_admin = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	} else if ($_SESSION["branchFlag"] == ORG_MANAGER) {
		// 組織管理アカウントなので、組織IDを元に組織にぶら下がる支店のadmin_idを取ってくる
		$org = $adminModel->get_organization_by_id($db, $_SESSION["orgId"]);
		$send_admin = $org['admin_id'];
	} else if ($_SESSION["branchFlag"] == STAFF) {
		// スタッフの場合、
		$staff = $adminModel->get_staff_by_id($db, $_SESSION["staffId"]);
		$send_admin = $staff['admin_id'];
	} else {
		// 現状ありえない
	}
	db_close($db);

	$db = db_connect();
	$rally_date = rally_select($db, "admin_id = " . ADMIN_ID . " LIMIT 0,1");
	$rally = mysql_fetch_array($rally_date);
	$rally_id = $rally['rally_id'];

	$p_admin_id = ADMIN_ID;

	$record = "rally_id='" . $rally_id . "', admin_id='" . $send_admin . "', p_admin_id = '" . $p_admin_id . "', title='" . mysql_real_escape_string($notice_title) . "', content='" . mysql_real_escape_string($notice_date) . "', format='" . $format_no . "', schedule_type='" . $schedule_type . "', new_user_time_difference='" . $new_user_time_difference . "', new_user_day_difference='" . $new_user_day_difference . "', last_stamp_after_day='" . $last_stamp_after_day . "', total_stamps='" . $total_stamps . "', total_stamp_after_day='" . $total_stamp_after_day . "', before_day_of_birth = '" . $before_day_of_birth . "', day_of_the_week='" . $day_of_the_week . "', monthly_push_day_of_the_month='" . $monthly_push_day_of_the_month . "', last_month_stamp_num='" . $last_month_stamp_num . "', last_month_push_day='" . $last_month_push_day . "', push_hour='" . $push_hour . "', push_minute='" . $push_minute . "' ";
	error_log("record:" . $record);

	// 配信スケジュールを登録
	notice_schedule_insert($db, $record);
	db_close($db);

	unset($_SESSION['asp']['notice']);
	if (Util::is_smartphone()) {
		require "./sp/page/notice_schedule_end.php";
	} else {
		require "./pc/page/notice_schedule_end.php";
	}
} else if (isset($_POST['sendonly'])) {

	// 保存しているものを再送する
	//編集ページの処理
	$notice_id = isset($_POST['notice_id']) ? $_POST['notice_id'] : '';

	// 予約の場合、
	$reserve_condition = '';
	$acceptance_datetime_condition = '';
	if ($reserve_check == 1) {
		$send_datetime = $reserve_year . "-" . $reserve_month . "-" . $reserve_day . " " . $reserve_hour . ":" . sprintf("%02d", $reserve_minute) . ":00";
		$reserve_condition = " notice_data = '" . $send_datetime . "', ";
	} else {
		// 予約がない場合は送信日時を設定する
		$send_datetime = date('Y-m-d H:i:s');  //配信日時
		$reserve_condition = " notice_data = '" . $send_datetime . "', ";
		$acceptance_datetime_condition = " acceptance_datetime = '" . $send_datetime . "', ";
	}

	// お知らせの仮保存ステータスを変更
	$db = db_connect();
	$where = "notice_id = '" . $notice_id . "'";

	// 予約時間とSNS連携は値が更新されている可能性があるので、上書きする
	$set = " transition = '0', "
		. $acceptance_datetime_condition
		. $reserve_condition
		. "sns_fb_check = '" . $sns_fb_check . "', "
		. "sns_tw_check = '" . $sns_tw_check . "'";
	notice_db_edit($db, $set, $where);
	db_close($db);

	// リダイレクト前にセッションに配信内容を設定する
	$noticeComp->pre_notice($notice_id);
	header('Location: ' . $_SERVER['REQUEST_URI']);
} else {
	// 初期画面表示の場合
	// transitionの初期化
	$_SESSION['asp']['notice']['transition'] = 'input';

	$individual_sex = "all";
	if (Util::is_smartphone()) {
		require "./sp/page/notice_form.php";
	} else {
		require "./pc/page/notice_form.php";
	}
}
