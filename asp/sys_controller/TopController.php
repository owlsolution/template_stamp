<?php
/**
 * 支店機能対応のトップ画面表示コントローラー
 */

require_once('./../common/model/AdminModel.php');
$adminModel = new AdminModel();

$db = db_connect();
$new_day = date('Y-m-d');
$where = "display_date <= '".$new_day."' ORDER BY display_date DESC";
$management_notice_date = management_notice_select($db , $where);
while ($row = mysql_fetch_array($management_notice_date)){
	$management_notice_array[] = $row;
}
db_close( $db );

/* --------------------------------------*/
/* 関連admin_idで発行された最新のお知らせを取得する */
/* --------------------------------------*/
$db = db_connect();
// 支店機能 通常/オーナーはADMIN_ID ,支店権限の場合は支店のadmin_idで検索
if ($_SESSION["branchFlag"] == OWNER) {
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, ADMIN_ID, OWNER);
} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] == 0)){
	// 全支店管理アカウントで全支店管理アカウントモードの場合
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, ADMIN_ID, BRANCHES_OWNER);
} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] != 0)){
	// 全支店管理アカウントで全支店アカウントを選択時は選択した支店管理アカウントに限定した機能
	$branch_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	// 選択中の支店管理アカウントの配下のスタッフのadmin_idを取得する
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $branch_admin_id, BRANCH_MANAGER);
} else if ($_SESSION["branchFlag"] == BRANCH_MANAGER) {
	// 支店管理アカウントでログインしている場合
	$branch_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	// 支店管理アカウントにブラ下がるスタッフを取得
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $branch_admin_id, BRANCHES_OWNER);
} else if (($_SESSION["branchFlag"] == ORG_MANAGER) && ($_SESSION["branchId"] != 0)){
	// 全支店管理アカウントで全支店アカウントを選択時は選択した支店管理アカウントに限定した機能
	$branch_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
	// 選択中の支店管理アカウントの配下のスタッフのadmin_idを取得する
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $branch_admin_id, BRANCH_MANAGER);
} else if ($_SESSION["branchFlag"] == ORG_MANAGER) {
	// 組織管理アカウントなので、組織IDを元に組織にぶら下がる支店のadmin_idを取ってくる
	$org = $adminModel->get_organization_by_id($db, $_SESSION["orgId"]);
	// 組織IDからadmin_idを取得
	// admin_idとタイプを渡して、ぶら下がるadmin_idのリストを返却するメソッドを呼出す
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $org['admin_id'], ORG_MANAGER);
} else if ($_SESSION["branchFlag"] == STAFF) {
	// スタッフのadmin_idを設定
	$staff = $adminModel->get_staff_by_id($db, $_SESSION["staffId"]);
	$admin_id_list[] = $staff['admin_id'];
}
$send_admin_id = implode(',', $admin_id_list);

// 表示対象のお知らせを取得
$where = "admin_id IN (".$send_admin_id.") ORDER BY notice_id DESC LIMIT 0 , 1";
$notice_date_array = notice_select($db , $where);
while ($notice = mysql_fetch_array($notice_date_array)){
	$notice_array[] = $notice;
}
db_close( $db );

if(Util::is_smartphone ()) {
	require './sp/page/top.php';
} else {
	require './pc/page/top.php';
}