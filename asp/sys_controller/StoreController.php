<?php
// 支店情報を取得
$db = db_connect();
$branch_list = branch_select_by_owner($db, ADMIN_ID);
db_close($db);

if(!empty($_POST['store_name'])){
	$admin_id = ADMIN_ID;
	if (isset($_POST['branch']) && $_POST['branch'] != 0) {
		$admin_id = $_POST['branch'];
	}
	//店舗名
	$store_name = $_POST['store_name'];
	//住所
	$store_address = $_POST['store_address'];
	//店休日
	$store_rest = $_POST['store_rest'];
	//紹介文
	$store_prelusion = $_POST['store_prelusion'];
	//ホームページURL
	$hp_url = $_POST['hp_url'];
	//マップURL
	$map_url = $_POST['map_url'];
	//営業時間
	$business_hours = $_POST['business_hours'];
	//電話番号
	$phone_number = $_POST['phone_number'];
	//店舗情報デザイン HTML
	$store_info_design = $_POST['store_info_design'];
	//店舗情報追加日付
	$add_date = date('Y-m-d');
	
	// SQLでの特殊文字はエスケープする
	$db = db_connect();
	$store_name = mysql_real_escape_string($store_name);
	$store_address = mysql_real_escape_string($store_address);
	$store_prelusion = mysql_real_escape_string($store_prelusion);
	$hp_url = mysql_real_escape_string($hp_url);
	$map_url = mysql_real_escape_string($map_url);
	$business_hours = mysql_real_escape_string($business_hours);
	$phone_number = mysql_real_escape_string($phone_number);
	$store_info_design = mysql_real_escape_string($store_info_design);
	db_close( $db );

}
$id = $_POST['edit_id'];
$edit_id = isset($_GET['edit_id']) ? $_GET['edit_id'] : '';
$delete_id = isset($_GET['delete_id']) ? $_GET['delete_id'] : '';

if(isset($_POST['setting'])){
	//新しい電場番号、営業時間、アプリの店舗情報に表示するデザインを追加
	$into = "'".$admin_id."' , '".$store_name."' , '".$store_address."' , '".$business_start."' , '".$business_end."' , '".$tel."' , '".$store_rest."' , '".$add_date."' , '".$store_prelusion."' , '".$hp_url."' , '".$map_url."' , '".$business_hours."' , '".$phone_number."' , '".$store_info_design."'";
	$db = db_connect();
	shop_insert($db , $into);
	db_close( $db );
} else if(!empty($_POST['update'])){
	$set = "store_name = '".$store_name."' , store_address = '".$store_address."' , store_rest = '".$store_rest."' , store_prelusion = '".$store_prelusion."' , hp_url = '".$hp_url."' , map_url = '".$map_url."' , business_hours = '".$business_hours."' , phone_number = '".$phone_number."' , store_info_design = '".$store_info_design."'";
	$where = "id = ".$id;
	$db = db_connect();
	shop_update($db , $set , $where);
	db_close( $db );
} else if (!empty ($delete_id)){
	$where = "id = '".$delete_id."'";
	$db = db_connect();
	shop_delete($db , $where);
	db_close( $db );
}

// 店舗情報の取得をする
// まず、表示する店舗のadmin_idを洗い出す
$admin_ids = ADMIN_ID;
foreach ($branch_list as $branch) {
    $admin_ids .= ",".$branch['child_admin_id'];
}

// admin_idをキーに店舗一覧を取得する
$where = "admin_id IN(".$admin_ids.")";
$db = db_connect();
$shop_date = shop_select($db , $where);
db_close( $db );
require "./pc/page/store_form.php";