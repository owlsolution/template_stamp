<?php
/*
 * クーポンの設定
 */
if(isset($_POST['check'])){  //確認ボタン
	$num = mt_rand();
	/*
	 * クーポン
	 */
	for($i=1; $i<=10; $i++){
		$img_name[$i] = $_POST['img_name_'.$i];
		if (is_uploaded_file($_FILES["up_img_".$i]["tmp_name"])) {
			if (move_uploaded_file($_FILES["up_img_".$i]["tmp_name"], "./../img_coupon/" . $i.$num.$_FILES["up_img_".$i]["name"])) {
				chmod("./../img_coupon/" . $i.$num.$_FILES["up_img_".$i]["name"], 0644);
				$img_name[$i] = $i.$num.$_FILES["up_img_".$i]["name"];
			} else {
				echo "ファイルをアップロードできません。";
			}
		}
	}
	/*
	 * プレゼントクーポン画像
	 */
	$present_img_name = $_POST['present_img_name'];
	if (is_uploaded_file($_FILES["present_up_img"]["tmp_name"])) {
		if (move_uploaded_file($_FILES["present_up_img"]["tmp_name"], "./../img_coupon/" . "pre".$num.$_FILES["present_up_img"]["name"])) {
			chmod("./../img_coupon/" . "pre".$num.$_FILES["present_up_img"]["name"], 0644);
			$present_img_name = "pre".$num.$_FILES["present_up_img"]["name"];
		} else {
			echo "プレゼント画像ファイルをアップロードできません";
		}
	}
	/*
	 * 初回プレゼントクーポン画像
	 */
	$first_present_img_name = $_POST['first_present_img_name'];
	if (is_uploaded_file($_FILES["first_present_up_img"]["tmp_name"])) {
		if (move_uploaded_file($_FILES["first_present_up_img"]["tmp_name"], "./../img_coupon/" . "fpre".$num.$_FILES["first_present_up_img"]["name"])) {
			chmod("./../img_coupon/" . "fpre".$num.$_FILES["first_present_up_img"]["name"], 0644);
			$first_present_img_name = "fpre".$num.$_FILES["first_present_up_img"]["name"];
		} else {
			echo "初期プレゼント画像ファイルをアップロードできません";
		}
	}
	require "./pc/page/goal_page_check.php";
} else if(isset($_POST['setting']) ){  //設定ボタン
	$rally_id = $_POST['rally_id'];  //ラリーID
	$add_date = date('Y-m-d');  //登録日
	for($i = 1; $i <= 10 ; $i++){
		if($_POST['goal_stamp_num_'.$i] == ""){
			$_POST['goal_stamp_num_'.$i] = 0;
		}
	}
	if(!isset($_POST['present_img_name'])){
		$_POST['present_img_name'] = "gift.png";
	}
	if(!isset($_POST['first_present_img_name'])){
		$_POST['first_present_img_name'] = "gift.png";
	}
	$db = db_connect();
	//$set = "goal_title_1='".$_POST['goal_title_1']."',goal_stamp_num_1='".$_POST['goal_stamp_num_1']."',goal_description_1='".$_POST['goal_description_1']."',goal_title_2='".$_POST['goal_title_2']."',goal_stamp_num_2='".$_POST['goal_stamp_num_2']."',goal_description_2='".$_POST['goal_description_2']."',goal_title_3='".$_POST['goal_title_3']."',goal_stamp_num_3='".$_POST['goal_stamp_num_3']."',goal_description_3='".$_POST['goal_description_3']."',goal_title_4='".$_POST['goal_title_4']."',goal_stamp_num_4='".$_POST['goal_stamp_num_4']."',goal_description_4='".$_POST['goal_description_4']."',goal_title_5='".$_POST['goal_title_5']."',goal_stamp_num_5='".$_POST['goal_stamp_num_5']."',goal_description_5='".$_POST['goal_description_5']."',goal_title_6='".$_POST['goal_title_6']."',goal_stamp_num_6='".$_POST['goal_stamp_num_6']."',goal_description_6='".$_POST['goal_description_6']."',goal_title_7='".$_POST['goal_title_7']."',goal_stamp_num_7='".$_POST['goal_stamp_num_7']."',goal_description_7='".$_POST['goal_description_7']."',goal_title_8='".$_POST['goal_title_8']."',goal_stamp_num_8='".$_POST['goal_stamp_num_8']."',goal_description_8='".$_POST['goal_description_8']."',goal_title_9='".$_POST['goal_title_9']."',goal_stamp_num_9='".$_POST['goal_stamp_num_9']."',goal_description_9='".$_POST['goal_description_9']."',goal_title_10='".$_POST['goal_title_10']."',goal_stamp_num_10='".$_POST['goal_stamp_num_10']."',goal_description_10='".$_POST['goal_description_10']."',add_date='".$add_date."' , img_name_1 ='".$_POST['img_name_1']."' , img_name_2 ='".$_POST['img_name_2']."' , img_name_3 ='".$_POST['img_name_3']."' , img_name_4 ='".$_POST['img_name_4']."' , img_name_5 ='".$_POST['img_name_5']."' , img_name_6 ='".$_POST['img_name_6']."' , img_name_7 ='".$_POST['img_name_7']."' , img_name_8 ='".$_POST['img_name_8']."' , img_name_9 ='".$_POST['img_name_9']."' , img_name_10 ='".$_POST['img_name_10']."'";
	$set = "goal_title_1='".$_POST['goal_title_1']."',goal_stamp_num_1='".$_POST['goal_stamp_num_1']."',goal_description_1='".$_POST['goal_description_1']."',goal_title_2='".$_POST['goal_title_2']."',goal_stamp_num_2='".$_POST['goal_stamp_num_2']."',goal_description_2='".$_POST['goal_description_2']."',goal_title_3='".$_POST['goal_title_3']."',goal_stamp_num_3='".$_POST['goal_stamp_num_3']."',goal_description_3='".$_POST['goal_description_3']."',goal_title_4='".$_POST['goal_title_4']."',goal_stamp_num_4='".$_POST['goal_stamp_num_4']."',goal_description_4='".$_POST['goal_description_4']."',goal_title_5='".$_POST['goal_title_5']."',goal_stamp_num_5='".$_POST['goal_stamp_num_5']."',goal_description_5='".$_POST['goal_description_5']."',goal_title_6='".$_POST['goal_title_6']."',goal_stamp_num_6='".$_POST['goal_stamp_num_6']."',goal_description_6='".$_POST['goal_description_6']."',goal_title_7='".$_POST['goal_title_7']."',goal_stamp_num_7='".$_POST['goal_stamp_num_7']."',goal_description_7='".$_POST['goal_description_7']."',goal_title_8='".$_POST['goal_title_8']."',goal_stamp_num_8='".$_POST['goal_stamp_num_8']."',goal_description_8='".$_POST['goal_description_8']."',goal_title_9='".$_POST['goal_title_9']."',goal_stamp_num_9='".$_POST['goal_stamp_num_9']."',goal_description_9='".$_POST['goal_description_9']."',goal_title_10='".$_POST['goal_title_10']."',goal_stamp_num_10='".$_POST['goal_stamp_num_10']."',goal_description_10='".$_POST['goal_description_10']."',add_date='".$add_date."' , img_name_1 ='".$_POST['img_name_1']."' , img_name_2 ='".$_POST['img_name_2']."' , img_name_3 ='".$_POST['img_name_3']."' , img_name_4 ='".$_POST['img_name_4']."' , img_name_5 ='".$_POST['img_name_5']."' , img_name_6 ='".$_POST['img_name_6']."' , img_name_7 ='".$_POST['img_name_7']."' , img_name_8 ='".$_POST['img_name_8']."' , img_name_9 ='".$_POST['img_name_9']."' , img_name_10 ='".$_POST['img_name_10']."' , present_img_name = '".$_POST['present_img_name']."' , first_present_img_name = '".$_POST['first_present_img_name']."'";
	$where = "rally_id = ".$rally_id."";
	up_goal($db , $set , $where);
	db_close( $db );
	
	/*
	 * クーポン画像変更した時、rallyテーブルのrally_define_datetimeカラムの日時を更新する。
	 */
	$db = db_connect();
	$now = time();
	$now_datetime = date('Y-m-d H:i:s', $now); // 登録日時
	$set = "rally_define_datetime = '".$now_datetime."'";
	$where = "rally_id = ".$rally_id ;
	up_rally($db , $set , $where);
	db_close( $db );
	
	require "./pc/page/setting_end.php";
} else if(isset($_POST['return'])){  //戻るボタン
	$stamp_select = $_POST['rally_id'];
	require "./pc/page/goal_page_form.php";
} else {
	if(isset($_POST['display'])){
		$stamp_select = $_POST['stamp_select'];
	}
	require "./pc/page/goal_page_form.php";
}
?>
