<?php
/**
 * 支店機能向け 顧客情報一覧の処理
 */
require_once('./../common/model/AdminModel.php');
$adminModel = new AdminModel();

/**
 * アクセス時間を基にユーザの色を取得する
 * @param type $last_date
 * @return string
 */
function getUserColor($last_date) {
	// 顧客一覧を月別に色分けをする。
	// ここで設定した日付とユーザーの更新日付を比較する。
	$login_date_compare_ptn4 = date("Y-m-d",strtotime("-12 month"));
	$login_date_compare_ptn3 = date("Y-m-d",strtotime("-6 month"));
	$login_date_compare_ptn2 = date("Y-m-d",strtotime("-3 month"));
	$login_date_compare_ptn1 = date("Y-m-d",strtotime("-1 month"));

	// パターンに対応した色を定義
	$color_type_def = "#FFFFFF";
	$color_type_ptn1 = "#f8f8f8";
	$color_type_ptn2 = "#e4e4e4";
	$color_type_ptn3 = "#cccccc";
	$color_type_ptn4 = "#999999";

	// 日付の比較をして<tr>タグの背景色を変える
	if($last_date < $login_date_compare_ptn4){
		$color_type = $color_type_ptn4;
	}else if ($last_date < $login_date_compare_ptn3){
		$color_type = $color_type_ptn3;
	}else if ($last_date < $login_date_compare_ptn2){
		$color_type = $color_type_ptn2;
	}else if($last_date < $login_date_compare_ptn1){
		$color_type = $color_type_ptn1;
	}else {
		$color_type = $color_type_def;
	}
	return $color_type;
}

/**
 * @param type $last_login_date 最終ログイン日
 * @param type $now_date 現在時刻
 * @return type 最終ログイン日と現在時刻の差分
 */
function day_diff($last_login_date , $now_date){
	$login_date = strtotime($last_login_date);
	$now_dates = strtotime($now_date);
	$second_diff = abs($now_dates - $login_date);
	$day_diff = $second_diff / ( 60 * 60 * 24 );
	return $day_diff;
	
}

/*
 * 顧客累計クーポン数
 */
function all_coupon($db, $user_id , $rally_id){
//	$db = db_connect();
	$where = "user_id = ".$user_id." AND rally_id = ".$rally_id;
	$get_coupon_date = get_coupon_count($db , $where);
	$get_coupon = mysql_fetch_array($get_coupon_date);
	$all_coupon = $get_coupon['count(*)'];
//	db_close( $db );
	return $all_coupon;
}

/*
 * 顧客累計クーポン数
 */
function sum_all_coupon($db, $user_id , $rally_id){
//	$db = db_connect();
	$where = "user_id IN( ".$user_id.") AND rally_id = ".$rally_id;
	$get_coupon_date = get_coupon_count($db , $where);
	$get_coupon = mysql_fetch_array($get_coupon_date);
	$all_coupon = $get_coupon['count(*)'];
//	db_close( $db );
	return $all_coupon;
}

/*
 * 顧客現在クーポン数
*/
function now_coupon($db, $user_id , $rally_id){
//	$db = db_connect();
	$where = "user_id = ".$user_id." AND get_coupon_state = 0 AND rally_id = ".$rally_id;
	$get_coupon_date = get_coupon_count($db , $where);
	$get_coupon = mysql_fetch_array($get_coupon_date);
	$now_coupon = $get_coupon['count(*)'];
//	db_close( $db );
	return $now_coupon;
}

/*
 * 顧客現在クーポン数
*/
function sum_now_coupon($db, $user_id , $rally_id){
//	$db = db_connect();
	$where = "user_id IN( ".$user_id.") AND get_coupon_state = 0 AND rally_id = ".$rally_id;
	$get_coupon_date = get_coupon_count($db , $where);
	$get_coupon = mysql_fetch_array($get_coupon_date);
	$now_coupon = $get_coupon['count(*)'];
//	db_close( $db );
	return $now_coupon;
}

function rally_name_get($admin_id){
	$db = db_connect();
	$where = "admin_id = ".$admin_id;
	$rally_date = rally_select($db , $where);
	$rally = mysql_fetch_array($rally_date);
	$rally_name = $rally['rally_name'];
	db_close( $db );
	return $rally_name;
}

/*
* @return string $pagingString:ページネーションリンク文字列
*/
function paging($page, $total_page , $sort_num , $change_order , $per_page){
	
	$nextPage = $page + 1;
	$backPage = $page - 1;
	if(empty($start)){
		$start = 1;
	}
	$masterMyhomePaginationLinkNumber = 10;
	$maxLinkView = "";
	if(empty($maxLinkView)){
		$maxLinkView = $masterMyhomePaginationLinkNumber;
	}
	$pagingString = '';
	//一番最初に移動
	$pagingString .= '<span id="paging_first_link">';
	$pagingString .= '<a href="./?p=customer&page=1&sort_num='.$sort_num.'&change_order='.$change_order.'&search_user_num='.$per_page.'&search_user_num='.$per_page.'" style="font-size: 15px;">First&nbsp;&nbsp;&nbsp;</a>';
	$pagingString .= '</span>';
	// 一個前に戻る
	if($backPage > 0){
		$pagingString .= '<span id="paging_back_link"><a href="./?p=customer&page='.$backPage.'&sort_num='.$sort_num.'&change_order='.$change_order.'&search_user_num='.$per_page.'" style="font-size: 24px;">&lt;&nbsp;</a></span>';
	}
	
	// ページ番号表示を動的に変更させる
	// 現在ページより左に表示すべきページ数
	$left_page = floor(($masterMyhomePaginationLinkNumber-1)/2) + 1;
	// 現在ページより右に表示すべきページ数
	$right_page = floor(($masterMyhomePaginationLinkNumber-1)/2);
	
	// リンク番号の開始ページの算出
	if(($page - $left_page) > 0 ){
		// 左に余力あるのでそのままstartはそのまま設定
		$start = $page - $left_page;
	} else {
		// 左に余力が無いのでそのままstartは初期値を設定
		$start = 1;
	}
	
	// リンク番号の終了ページの算出
	if(($page + $right_page) < $total_page ){
		// 左に余力あるのでそのまま$maxLinkViewそのまま設定
		$maxLinkView = $page + $right_page;
	} else {
		// 右に余力が無いのでそのまま$maxLinkViewは$total_pageを設定
		$maxLinkView = $total_page;
	}
	/**
	 * 算出した開始ページから終了ページまでのループ
	 */
	for($i = $start; $i <= $maxLinkView; $i++){
		$pagingString .= ' ';
		// 現在ページに色付け
		if($i == $page){
			$pageActiveLinkColor = 'color: #ff0000';
		} else {
			$pageActiveLinkColor = '';
		}
		
		$pagingString .= '<span class="paging_links"><a href="./?p=customer&page='.$i.'&sort_num='.$sort_num.'&change_order='.$change_order.'&search_user_num='.$per_page.'" style="font-size: 24px; '.$pageActiveLinkColor.'">'.$i.'</a></span>';
		$pagingString .= ' ';
	}
	// 一個次に進む
	if($nextPage <= $total_page){
		$pagingString .= '<span id="paging_next_link"><a href="./?p=customer&page='.$nextPage.'&sort_num='.$sort_num.'&change_order='.$change_order.'&search_user_num='.$per_page.'" style="font-size: 24px;">&nbsp;&gt;</a></span>';
	}
	//一番最後のページに移動
	$pagingString .= '<span id="paging_last_link">';
	$pagingString .= '<a href="./?p=customer&page='.$total_page.'&sort_num='.$sort_num.'&change_order='.$change_order.'&search_user_num='.$per_page.'&search_user_num='.$per_page.'" style="font-size: 15px;">&nbsp;&nbsp;&nbsp;Last</a>';
	$pagingString .= '</span>';
	$pagingString .= '<br class="clear" />';
	return $pagingString;
}


//$_GETを取得
$user_id = $_GET['user_id'];
$rally_id = $_GET['rally_id'];
$delete_id = $_GET['delete'];

// $rally_idは値の有無で挙動制御に使われているので、別名でrally_idを取得する
$rally_date = Util::rally_information_get(ADMIN_ID);
$s_rally_id = $rally_date['rally_id'];

if(!empty($delete_id)) {  
	//顧客を削除する
	$where = "user_id = ".$delete_id." AND rally_id = ".$rally_id;
	$db = db_connect();
	rally_user_delete($db , $where);
	// プッシュ情報削除
	db_close( $db );
} 

if(!empty($user_id) && !empty($rally_id)){
	/**
	* 顧客の個別機能へ遷移
	*/
	if(!empty($_GET['edit']) || !empty($_GET['update'])) {
		// 顧客情報編集処理へ遷移
		require "./sys_controller/CustomerEditController.php";
	} else {
		// 顧客情報の詳細表示へ遷移
		require "./sys_controller/CustomerDetailController.php";
	}
} else {
	$now_year = date('Y');
	$now_month = date('m');
	$now_day = date('d');

	// ソート項目
	$get_sort_num = (!empty($_GET['sort_num']))? $_GET['sort_num'] : "user_id";
	
	$db = db_connect();
	$sort_num = mysql_real_escape_string($get_sort_num);
	// 昇順・降順
	$get_change_order = (!empty($_GET['change_order']))? $_GET['change_order'] : "ASC";
	$change_order = mysql_real_escape_string($get_change_order);
	db_close( $db );
	
	//現在のページ数
	$page = (!empty($_GET['page']))? $_GET['page'] : 1;
	//1ページに表示させる件数
	//デフォルトは100人
	$per_page = (!empty($_GET['search_user_num']))? $_GET['search_user_num'] : 100;

	//POST
	if(isset($_POST['search'])){
		/**
		 * 検索ボタンを押された時の処理
		 */

		//日付検索　新規登録 or 最終スタンプ日
		$conditions_select = (!empty($_POST['conditions_select']))? $_POST['conditions_select'] : 0;
		//開始年月日
		$years_select = (!empty($_POST['years_select']))? $_POST['years_select'] : 2013;
		$month_select = (!empty($_POST['month_select']))? $_POST['month_select'] : 1;
		$day_select = (!empty($_POST['day_select']))? $_POST['day_select'] : 1;
		//終了年月日
		$end_years_select = (!empty($_POST['end_years_select']))? $_POST['end_years_select'] : $now_year;
		$end_month_select = (!empty($_POST['end_month_select']))? $_POST['end_month_select'] : $now_month;
		$end_day_select = (!empty($_POST['end_day_select']))? $_POST['end_day_select'] : $now_day;

		// 顧客IDで検索
		$search_user_id = (!empty($_POST['search_user_id'])) ? $_POST['search_user_id'] : "";

		// 顧客名・カルテでキーワード検索
		$search_keyword = (!empty($_POST['search_keyword']))? $_POST['search_keyword'] : $_GET['search_keyword'];
		$search_keyword = urldecode($search_keyword);
		$db = db_connect();
		$keyword = mysql_real_escape_string($search_keyword);
		db_close( $db );
		
		
		//SESSIONの設定
		$_SESSION['customer']['years_select'] = $years_select;
		$_SESSION['customer']['month_select'] = $month_select;
		$_SESSION['customer']['day_select'] = $day_select;
		$_SESSION['customer']['end_years_select'] = $end_years_select;
		$_SESSION['customer']['end_month_select'] = $end_month_select;
		$_SESSION['customer']['end_day_select'] = $end_day_select;
		$_SESSION['customer']['conditions_select'] = $conditions_select;
		$_SESSION['customer']['start_time'] = Util::get_starttime_of_day($years_select."-".$month_select."-".$day_select, $s_rally_id);
		$_SESSION['customer']['end_time'] = Util::get_starttime_of_day($end_years_select."-".$end_month_select."-".$end_day_select, $s_rally_id);
		$_SESSION['customer']['search_user_id'] = $search_user_id;
		$_SESSION['customer']['keyword'] = $keyword;

	} else if(isset($_POST['reset'])){  //
		/**
		 * リセットボタンを押された時の処理
		 */
		$conditions_select = 0;
		$end_years_select = $now_year;
		$end_month_select = $now_month;
		$end_day_select = $now_day;
		$years_select = 2013;
		$month_select = 1;
		$day_select = 1;
		$keyword = "";
		$search_user_id = "";
		//SESSIONの解放
		unset($_SESSION['customer']);
	} else {
		if(isset($_SESSION['customer'])){
			$years_select = $_SESSION['customer']['years_select'];
			$month_select = $_SESSION['customer']['month_select'];
			$day_select = $_SESSION['customer']['day_select'];
			$end_years_select = $_SESSION['customer']['end_years_select'];
			$end_month_select = $_SESSION['customer']['end_month_select'];
			$end_day_select = $_SESSION['customer']['end_day_select'];
			$conditions_select = $_SESSION['customer']['conditions_select'];
			$keyword = $_SESSION['customer']['keyword'];
			$search_user_id = $_SESSION['customer']['search_user_id'];
		} else {
			$conditions_select = 0;
			$end_years_select = $now_year;
			$end_month_select = $now_month;
			$end_day_select = $now_day;
			$years_select = 2013;
			$month_select = 1;
			$day_select = 1;
			$search_user_id = "";
		}
	}
	
	/****************************************/
	/* 顧客情報一覧の検索条件のWHERE句生成
	/****************************************/
	// 検索条件を付加しての一覧表示
	// 必須条件
	$db = db_connect();
	$branch_ids = null;
	if ($_SESSION["branchFlag"] == OWNER) {
		// 全員
	} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] == 0)){
		// 全員
	} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] != 0)){
		// 単一ブランチ
		$branch_ids = $_SESSION["branchId"];
	} else if ($_SESSION["branchFlag"] == BRANCH_MANAGER) {
		// 単一ブランチ
		$branch_ids = $_SESSION["branchId"];
	} else if (($_SESSION["branchFlag"] == ORG_MANAGER) && ($_SESSION["branchId"] != 0)){
		$branch_ids = $_SESSION["branchId"];
	} else if ($_SESSION["branchFlag"] == ORG_MANAGER) {
		// 組織IDから対象のブランチidを取得する
		$branch_list = $adminModel->get_branch_by_organization_id($db, $_SESSION["orgId"]);
		$branch_id_list = array_column($branch_list, 'id');
		$branch_ids = implode(",", $branch_id_list);
	} else if ($_SESSION["branchFlag"] == STAFF) {
		// スタッフが所属する支店の支店IDを取得する
		// 現状未対応
	}
	
	$where = "AND r.admin_id = ".ADMIN_ID;
	// 支店IDを保持している場合、支店権限参照なので、支店での絞り込み条件を付加
	if (!empty($branch_ids)) {
		$where .= " AND r.branch_id IN (".$branch_ids.")";
	}
	
	//日付検索
	$date_conditions = "";
	$user_id_condition = "";
	$keyword_condition = "";
	if(isset($_SESSION['customer'])) {
		if($_SESSION['customer']['conditions_select'] == 0){
			$date_conditions .= " AND r.add_date BETWEEN '".$_SESSION['customer']['start_time']."' AND '".$_SESSION['customer']['end_time']."'";
		} else {
			$date_conditions .= " AND r.last_stamp_date BETWEEN '".$_SESSION['customer']['start_time']."' AND '".$_SESSION['customer']['end_time']."'";
		}
		
		// 顧客ID検索
		// 不要なものを除去
		$entry_user_id = trim($search_user_id);
		$entry_user_id = str_replace(USER_ID, '', $entry_user_id);
		if (!empty($entry_user_id)) {
			$user_id_condition = " AND r.user_id = '".$entry_user_id."'";
		}
		
		//キーワード検索
		if (!empty($_SESSION['customer']['keyword']) || $_SESSION['customer']['keyword'] == "0") {
			$keyword_condition = " AND (u.user_name LIKE '%".$_SESSION['customer']['keyword']."%' OR r.karte LIKE '%".$_SESSION['customer']['keyword']."%')";
		}
	}
	//WHERE句の完成
	$where .= $date_conditions.$user_id_condition.$keyword_condition;
	/****************************************/
	/* 顧客情報一覧の検索条件のORDER BY句生成
	/****************************************/
	$orderby = " ORDER BY ".$sort_num." ".$change_order;
	/****************************************/
	/* 顧客情報一覧の検索条件のLIMIT句生成
	/****************************************/
	// 現在のページ数 x ページ毎の表示件数
	$limit = " LIMIT ".(($page - 1) * $per_page).",".$per_page;
	
	$record_index = ($page - 1) * $per_page;
	
//	echo 'record_index:'.$record_index;
	
	// 検索結果全件数の取得
//	$rally_user_date_all = rally_user_and_userinfo_select($db , $where);
//	$total_count = mysql_num_rows($rally_user_date_all);
	$rally_user_date_all = get_customer_data_simple($db , $s_rally_id , $where);
	$total_count = mysql_num_rows($rally_user_date_all);
	while ($row = mysql_fetch_array($rally_user_date_all)){
		$larry_user_all_array[] = $row;
	}
	
	if ($sort_num == 'all_co' || $sort_num == 'now_co') {
		// テーブルに存在しないカラむのページネーション処理
		$length = count($larry_user_all_array);
		for ($i = 0; $i < $length;$i++) {
			$row = $larry_user_all_array[$i];
			$larry_user_all_array[$i]['all_co'] = all_coupon($db, $row['user_id'] , $row['rally_id']);
			$larry_user_all_array[$i]['now_co'] = now_coupon($db, $row['user_id'] , $row['rally_id']);
		}
		foreach ($larry_user_all_array as $key => $value){
			$key_ids[$key] = $value[$sort_num];
		}
		
		if ($change_order == 'ASC') {
			array_multisort ( $key_ids , SORT_ASC , $larry_user_all_array);
		}else {
			array_multisort ( $key_ids , SORT_DESC , $larry_user_all_array);
		}
		
		// ページ
		$startpos = ($page - 1) * $per_page;
		$endpos = $startpos + $per_page;
		for ($i = $startpos; $i < $endpos; $i++ ){
			if ($i >= $total_count) {
				break;
			}
			$larry_user_array[] = $larry_user_all_array[$i];
		}
		
	} else {
		// SQL結果そのままを利用可能
		$rally_user_date = get_customer_data_simple($db , $s_rally_id , $where.$orderby.$limit);
		while ($row = mysql_fetch_array($rally_user_date)){
			$larry_user_array[] = $row;
		}
	}
	
	db_close( $db );
	
	// 必要になるページ数
	$total_page = ceil($total_count/$per_page);
	
	
	// 上記条件にスタンプ,クーポンの各累計値を算出する
	$sum_total_stamp_num = 0;
	$sum_stamp_num = 0;
	$sum_total_coupon_num = 0;
	$sum_coupon_num = 0;
	foreach( $larry_user_all_array as $key => $value) {
		$sum_total_stamp_num += $value['total_stamp_num'];
		$sum_stamp_num += $value['stamp_num'];
//		$sum_total_coupon_num += $value['all_co'];
//		$sum_coupon_num += $value['now_co'];
		$user_ids[] = $value['user_id'];
	}
	
	
//	$userlist = array_column($larry_user_all_array, 'user_id');
	$user_ids = implode(",",$user_ids);
	$db = db_connect();
	$sum_total_coupon_num = sum_all_coupon($db, $user_ids, $s_rally_id);
	$sum_coupon_num = sum_now_coupon($db, $user_ids, $s_rally_id);
	db_close( $db );
	
	if(Util::is_smartphone ()) {
		require "./sp/page/customer.php";
	} else {
		require "./pc/page/customer.php";
	}
}
?>

