<?php
/**
 * 支店機能対応時のページ振り分け処理
 */
// APIの入口で支店機能に必要なセッション情報の設定/取得を行う

// ヘッダのリスト選択時のクエリ
if(isset($_POST['branch'])) {
    // セッションに保持
    $_SESSION["branchId"] = $_POST['branch'];
    
    // 切替発生時のセッションクリア
    // お知らせ配信入力途中でもデータクリアして最初から。
    unset($_SESSION['asp']['notice']);
    // 顧客一覧検索の条件もクリアする
    unset($_SESSION['search_condition']);

    // ブラウザの戻るボタンでのフォーム再送要求の回避のためリダイレクトする
    header('Location: ./?p='.$_GET['p']);

}

$db = db_connect();
if (($_SESSION["branchFlag"] == OWNER) 
	|| ($_SESSION["branchFlag"] == BRANCHES_OWNER)
	){
	// 何もしない
} else if ($_SESSION["branchFlag"] == BRANCH_MANAGER) {
	$branch_info = $adminModel->get_branch_by_id($db, $_SESSION["branchId"]);
	$org = $adminModel->get_organization_by_id($db, $branch_info["organization_id"]);
} else if ($_SESSION["branchFlag"] == ORG_MANAGER) {
	$org = $adminModel->get_organization_by_id($db, $_SESSION["orgId"]);
} else if ($_SESSION["branchFlag"] == STAFF) {
	$staff = $adminModel->get_staff_by_id($db, $_SESSION["staffId"]);
}
db_close($db);

$p = "";
if(isset($_GET['p'])){
	$p = $_GET['p'];
} else {
	$p = "top";
}
switch ($p) {
	/**********    管理メニュー   ***********/
	case "top";//TOPページ
		$controller = "TopController.php";
		break;
	case "notice";  //お知らせ配信
		$controller = "NoticeController.php";
		break;
	case "sns";  //SNS配信
		$controller = "SnsController.php";
		break;
	case "customer";  //顧客情報
		$controller = "CustomerController.php";
		break;
	case "qr";  //各種QRコード発行
		$controller = "QrController.php";
		break;
	case "gift";  //プレゼントクーポン発行
		$controller = "GiftController.php";
		break;
	case "tool";  //ツール
		$controller = "ToolController.php";
		break;
	case "inquiry";  //お問い合わせ
		$controller = "InquiryController.php";
		break;
	case "pref";  //設定管理
		$controller = "PrefController.php";
		break;
	case "mail_delivery";
		$controller = "MailDeliveryController.php";
		break;
	case "reserve";  //予約/問合せ
		$controller = "ReserveController.php";
		break;
	case "reserve_detail";  //予約詳細
		$controller = "ReserveDetailController.php";
		break;
	case "inquiry_detail";  //問合わせ詳細
		$controller = "InquiryDetailController.php";
		break;
	/**********   管理者   ***********/
	case "summary";    //スタンプ一覧
		$controller = "SummaryController.php";
		break;
	case "new_stamp";  //新規スタンプ作成
		$controller = "NewStampController.php";
		break;
	case "stamp_pad";  //スタンプ台作成
		$controller = "StampPadController.php";
		break;
	case "stamp_pad_future";  //スタンプ台作成(フィーチャーフォン)
		$controller = "StampPadFutureController.php";
		break;
	case "stamp_page";  //ページ設定
		$controller = "StampPageController.php";
		break;
	case "goal_page";  //クーポン設定
		$controller = "GoalPageController.php";
		break;
	case "store_img";  //店舗の設定
		$controller = "StoreImgController.php";
		break;
	case "img_up";  //画像アップ設定
		$controller = "ImgUpController.php";
		break;
	case "questionnaire";  //アンケート設定
		$controller = "QuestionnaireController.php";
		break;
	case "original_tag";  //独自タグ一覧
		$controller = "OriginalTagController.php";
		break;
	case "shop";  //店舗の設定
		$controller = "StoreController.php";
		break;
	case "branch";  // 支店設定
		$controller = "BranchController.php";
		break;
	case "push_set";  //プッシュ配信設定
		$controller = "PushController.php";
		break;
	case "first_present";  //初回プレゼントクーポン設定
		$controller = "FirstGiftController.php";
		break;
	case "staff";    //店員設定
		$controller = "StaffController.php";
		break;
	default;//ログイン画面
	break;
}
require './sys_controller/'.$controller;
?>
