<?php
//重要なｲﾝｸﾙｰﾄ
require_once('./../../config.php');
require_once('./../ad_config.php');

require_once('./../common/model/AdminModel.php');
$adminModel = new AdminModel();

//ユーザーエージェント
$ua = $_SERVER["HTTP_USER_AGENT"];
if($_GET['p'] == "out"){
	$user = $_SESSION["userName"];
	user_out($user);
	header('Location:./');
	exit;
}

// ログイン画面でログインボタンが押下されているので
// 入り直す
if(isset($_POST["login"])) {
        // ログインするのでセッション情報をクリア
        $_SESSION = array();
	if(Util::is_smartphone ()) {
 		require "./sp/login.php";
 	} else {
 		require "./pc/login.php";
 	}
 	exit;
}

/*
 * $_SESSION["userName"]を持っているか識別
 * ログインチェック
 */
if(isset($_SESSION["userName"]) &&
        ($_SESSION["userName"] == ADMIN_ID || $_SESSION["userName"] == 0 )){
        // 支店機能対応時のページ振り分け処理
        require "./main.php";
} else {
        // ログインするのでセッション情報をクリア
        $_SESSION = array();
	if(Util::is_smartphone ()) {
 		require "./sp/login.php";
 	} else {
 		require "./pc/login.php";
 	}
 	exit;
}
?>