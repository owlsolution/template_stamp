<?php
require './pc/header.php';
?>
<div style="background:#FFF; padding:10px;">
<ul class="form">
	<form method="POST" action="<?php echo $url;?>" name="form2">
		<div class="f_box">
			<div class="container1">
				<h2> <img src="images/icon10.gif" width="16" height="16"> プッシュ通知時間設定 </h2>
				<br>
				<h3> スタンプ／クーポン期限のプッシュ通知時間設定 </h3>
				<ul>
					<select name="send_time" id="send_time">
						<?php
						for($s=0;$s<24;$s++){
						?>
						<option value="<?php echo $s;?>" <?php if($s == $send_time){?> selected <?php }?>><?php echo $s;?></option>
						<?php
						}
						?>
					</select>時
				</ul><br />
				<h3> 期限切れ事前通知設定 </h3>
				<ul>
							・スタンプ期限
					<select name="stamp_limit_days" id="stamp_limit_days">
						<?php
						for($s=-1;$s<=60;$s++){
						?>
						<option value="<?php echo $s;?>" <?php if($s == $stamp_limit_days){?> selected <?php }?>><?php echo $s;?></option>
						<?php
						}
						?>
					</select>日前にプッシュ通知でお知らせする。
								<br>
								<p style="color:red; font-size: 12px;">※「-1」はプッシュ通知をしない設定となっております。 </p>
				</ul><br />
				<ul>
							<br>
							・クーポン期限
					<select name="coupon_limit_days" id="coupon_limit_days">
						<?php
						for($s=-1;$s<=60;$s++){
						?>
						<option value="<?php echo $s;?>" <?php if($s == $coupon_limit_days){?> selected <?php }?>><?php echo $s;?></option>
						<?php
						}
						?>
					</select>日前にプッシュ通知でお知らせする。
								<br>
								<p style="color:red; font-size: 12px;">※「-1」はプッシュ通知をしない設定となっております。 </p>
				</ul><br />

						<br />
				<div align="center">
					<li class="btm_body">
						<div class="btn">
							<input name="check" type="submit" class="btm" value="確 認" /><input name="return" type="submit" class="btm" value="戻 る" onclick="javascript:window.history.back(-1);return false;"/>
						</div>
					</li>
				</div>
				<br>
			</div>
		</div>
	</form>
</ul>
</div>
<?php
require './pc/footer.php';
?>