<?php
require './pc/header.php';
?>
<div class="box">
	<form method="POST" action="./?p=notice" name="form2" enctype="multipart/form-data">
		<h2> <img src="images/icon1.gif" width="16" height="16"> お知らせ配信機能 </h2>
		<div class="container">
			<ul>
				配信を受付けました。
				<br>
				<br>
				
				<?php if (!empty($error_title)) : ?>
					<p style="color:red;"><?= $error_title;?></p>
				<?php endif;?>
				<br>
				<?php if (!empty($reserve_id)) : ?>
					<a href="./?p=reserve">受付へ戻る</a><br>
					<a href="./?p=reserve_detail&check_id=<?= $reserve_id; ?>">返信元の予約へ戻る</a>
				<?php endif;?>
				<?php if (!empty($inquiry_id)) : ?>
					<a href="./?p=reserve">受付へ戻る</a><br>
					<a href="./?p=inquiry_detail&check_id=<?= $inquiry_id; ?>">返信元の問合せへ戻る</a>
				<?php endif;?>
		</div>
	</form>
</div>
<?php
// 履歴表示の読み込み
if($to_user_id != ''){
    require './pc/page/notice_history_to_person.php';
} else {
    require './pc/page/notice_history_pager.php';
}
require './pc/footer.php';
?>