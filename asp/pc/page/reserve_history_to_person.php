<div class="box">
		<h2> <img src="images/icon1.gif" width="16" height="16"> 予約履歴 </h2>
		<div class="container">
			<table id="rsv_list_table" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr align="center">
									<th width="10%">予約No</th>
									<th width="10%">顧客ID</th>
									<th width="10%">氏名</th>
									<th width="10%">状態</th>
									<th width="10%">受付先</th>
									<th width="10%"></th>
							</tr>
						<?php foreach ($history_list as $key => $rsv): ?>
							<tr>
							<td align="center"><?= 'RSV'.sprintf("%05d", $rsv['reserve_id']); ?></td>
								<td align="center"><?= USER_ID.sprintf("%05d", $rsv['user_id']); ?></td>
								<td align="center"><?= $rsv['name']; ?></td>
								<td align="center"><?= ReserveModel::$STATUS_TEXTS[$rsv['status']]; ?></td>
								<?php $branch = $adminModel->get_target_branch($rsv['branch_id'], $branch_list); ?>
								<td align="center"><?= isset($branch['name']) ? $branch['name'] : '本部' ?></td>
								<td align="center"><a href="?p=reserve_detail&check_id=<?= $rsv['reserve_id'];?>">確認</a> ｜ <a href="?p=reserve_detail&dele_id=<?= $rsv['reserve_id'];?>" onClick="return confirm('削除しますか?')">削除</a></td>
							</tr>
						<?php endforeach; ?>
			</table>
		</div>
</div>
