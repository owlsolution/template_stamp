<?php
require './pc/header.php';
?>
<div class="box news_box">
	<div class="text_box">
		<ul>
			<?php
			foreach ($management_notice_array as $val){
				$display_date = str_replace("-", "/", $val['display_date']);
			?>
			<li class="clearfix">
				<p class="time"><?php echo $display_date;?></p>
				<p class="text"><?php echo nl2br($val['notice_content']);?></p>
			</li>
			<?php
			}
			?>
		</ul>
	</div>
</div>
<div class="clearfix">
	<?php
	foreach ($notice_array as $val){
	?>
	<div id="nwes_digit" class="box digit_box">
		<div class="nwes_digit_l">
			<img src="images/title1.png" width="130" height="19">
			<div class="digit digit1">
			<?php echo $val['already_read_num']?><strong>/<?php echo $val['app_delivery_num']?></strong>
			</div>
		</div>
		<div class="nwes_digit_r">
			<ul>
				<li class="clearfix" style="padding-top:0px;">
					<p class="time">配信日</p>
					<?php
					$add_date_str = explode( ":", $val['notice_data']);
					$add_date_date = $add_date_str[0].":".$add_date_str[1];
					$add_date_replace = str_replace("-", "/", $add_date_date);
					?>
					<p class="text"><?php echo $add_date_replace;?></p>
				</li>
				<li class="clearfix" style="border:0px;">
					<p class="time">タイトル</p>
					<p class="text"><?php echo $val['notice_title']?></p>
				</li>
			</ul>
		</div>
	</div>
	<?php
	}
	?>
	
<!--	<div id="sns_digit" class="box center digit_box">
		<img src="images/title2.png" width="141" height="19">
		<div class="digit digit2">
			<?php echo $number_people;?>
		</div>
	</div>
	<div id="sns_digit" class="box digit_box">
		<img src="images/title3.png"  height="19">
		<div class="digit digit3">
			<?php echo $twitter_number_people;?>
		</div>
	</div>-->
</div>
<?php
require './pc/footer.php';
?>