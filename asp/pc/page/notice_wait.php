<?php
require './pc/header.php';
?>
<div class="box">
	<form method="POST" action="./?p=notice" name="form2" enctype="multipart/form-data">
		<h2> <img src="images/icon1.gif" width="16" height="16"> お知らせ配信機能 </h2>
		<div class="container">
			<ul>
				処理中です。しばらくお待ちください。
				<?php if (!empty($error_title)) : ?>
					<p style="color:red;"><?= $error_title;?></p>
				<?php endif;?>
		</div>
	</form>
</div>
<?php
// 履歴表示の読み込み
if($to_user_id != ''){
    require './pc/page/notice_history_to_person.php';
} else {
    require './pc/page/notice_history_pager.php';
}
require './pc/footer.php';
?>