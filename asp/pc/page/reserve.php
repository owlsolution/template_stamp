<?php
require './pc/header.php';
?>


<style type="text/css">
.ui-tabs{ border: none; padding: 0px; }
.ui-tabs .ui-widget .ui-widget-content .ui-corner-all{
   border: none; padding: 0px;
}
.ui-widget-header{ background: #ffffff; border: none; }
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active .ui-state-focus {
   border: 1px solid #0092D6; background: #0092D6;
}
.ui-tabs-panel .ui-widget-content .ui-corner-bottom{
   background: #ffffff;
}
.ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited{
   color: #ffffff !important;
}
.ui-state-default a, .ui-state-default a:link, .ui-state-default a:visited{
   color: #c0c0c0;
}
.ui-state-hover a, .ui-state-hover a:hover{
   color: #0092D6;
}
.comment_area {
   background: #ffffff  !important;
}

#alertSettingBlock {
	margin: 10px 0 0 10px;
	display: none;
	position: relative;
	top:0px;
}
#alertSettingBlock input[type="text"]{
  margin: 5px 10px;
  width: 90%;
  font-size: 1.0em;
  box-sizing: border-box;
  padding: 0.6em 1.5em;
  border: #808080 solid 1px;
  border-radius: 0.33em;
  color: #333333;
}

#open_alert_setting_block {
	margin: 10px 20px;
	text-align:center;
	width:180px;
	background:#0092D6;
	padding:5px 20px;
	border:0px;
	border-radius: 0px;        /* CSS3草案 */
	-webkit-border-radius: 0px;    /* Safari,Google Chrome用 */
	-moz-border-radius: 0px;   /* Firefox用 */
	font-weight:bold;
	color:#FFF;
}

</style>

<input type="hidden" id="selected_tab"  value="0">
<script type="text/javascript">
	
	function getUrlVars()
	{
		var vars = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		for(var i = 0; i < hashes.length; i++)
		{
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	}
	
	$(function(){
		// タブ生成
		var selected_index = $('#selected_tab').val();
		var tab_index = getUrlVars()["tab_index"];
		if (tab_index > 0) {
			// スケジュール設定削除の場合、表示するタブはスケジュール配信にする
			selected_index = tab_index;
		}
		$('#tabs').tabs({
//            active: $('#selected_tab').val(),
            active: selected_index,
        } );
        
        $( "#tabs" ).tabs({
            beforeActivate: function( event, ui ) {
                // インデックス番号は次のように取得可能
                $('#selected_tab').val(ui.newTab.index());
            }
        });

        $('#dialog_link, ul#icons li').hover(
            function() { $(this).addClass('ui-state-hover'); },
            function() { $(this).removeClass('ui-state-hover'); }
        );
        // タブ表示
        $('#tabs').css("display","block");

        if($('#selected_tab').val() == 0) {
            // お知らせタブの場合、どの配信タイプを取得
            var value = $("input[name='delivery_set']:checked").val();
            // 表示を調整
            checkradio(value);
        }
		
		// 
		$("#open_alert_setting_block").click(function(){
			$("#alertSettingBlock").fadeIn(500).animate({
				'top': '0px'
			},{
			  duration: 200,
			  queue: false
			});
		});

    });
</script>

<div class="box">
    <h2> <img src="images/icon11.gif" width="16" height="16"> 受付 </h2>
		<ul>
			受付られた来店予約や問合せを確認できます。
		</ul>
	<br>
	
	<a id="open_alert_setting_block">通知先設定を表示</a>
	<p style="color:red; margin:10px 20px;">※通知先設定:予約/問合せが受付られた際に設定した先へその旨を通知します。</p>
	<?php foreach ($errors as $error) : ?>
		<p style="color:red; margin:20px;">エラー : <?= $error ?></p>
	<?php endforeach; ?>
	<div id="alertSettingBlock">
		<form method="post" action="./?p=reserve" name="form2" >
			<div >
				<?php for ($index=0; $index < 4; $index++) : ?>
				<input placeholder="メールアドレス(xxx@xxx.jp)もしくは顧客ID(例:<?= USER_ID.'0000000' ?>)を設定できます" type="text" name="alert_setting_account_<?= $index+1 ?>" id = "alert_setting_account_<?= $index+1 ?>" size="30" value="<?php echo (isset($alert_setting[$index]) && $alert_setting[$index]['no'] == $index+1) ? $alert_setting[$index]['account'] : '' ?>">
				<br>
				<?php endfor; ?>

				<br>
				<div class="btn">
					<input id="setting" name="setting" type="submit" value="設 定" />
				</div>
				<br>
				<br>
			</div>
		</form>
	</div>
	
    <div id="tabs" style="display:none;">
        <ul>
            <li><a href="#tabs-1">来店予約</a></li>
            <li><a href="#tabs-2">お問合せ</a></li>
        </ul>
        
        <div id="tabs-1" class="comment_area">
				<div id="rsv_pager">
					<table id="rsv_list_table" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr align="center">
									<th width="10%">予約No</th>
									<th width="10%">受付日</th>
									<th width="10%">氏名</th>
									<th width="10%">状態</th>
									<th width="10%"></th>
							</tr>
						<?php foreach ($reserve_list as $key => $rsv): ?>
							<tr>
							<td align="center"><?= 'RSV'.sprintf("%05d", $rsv['reserve_id']); ?></td>
								<td align="center"><?= $rsv['create_date']; ?></td>
								<td align="center"><?= $rsv['name']; ?></td>
								<td align="center"><?= ReserveModel::$STATUS_TEXTS[$rsv['status']]; ?></td>
								<td align="center"><a href="?p=reserve_detail&check_id=<?= $rsv['reserve_id'];?>">確認</a> ｜ <a href="?p=reserve_detail&dele_id=<?= $rsv['reserve_id'];?>" onClick="return confirm('削除しますか?')">削除</a></td>
							</tr>
						<?php endforeach; ?>
					</table>
				</div>
        </div>
        <div id="tabs-2" class="comment_area">
				<div id="rsv_pager">
					<table id="rsv_list_table" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr align="center">
									<th width="10%">予約No</th>
									<th width="10%">受付日</th>
									<th width="10%">氏名</th>
									<th width="10%">状態</th>
									<th width="10%"></th>
							</tr>
						<?php foreach ($inquiry_list as $key => $inq): ?>
							<tr>
							<td align="center"><?= 'INQ'.sprintf("%05d", $inq['inquiry_id']); ?></td>
								<td align="center"><?= $inq['create_date']; ?></td>
								<td align="center"><?= $inq['name']; ?></td>
								<td align="center"><?= InquiryModel::$STATUS_TEXTS[$inq['status']]; ?></td>
								<?php $branch = $adminModel->get_target_branch($inq['branch_id'], $branch_list); ?>
								<td align="center"><a href="?p=inquiry_detail&check_id=<?= $inq['inquiry_id'];?>">確認</a> ｜ <a href="?p=inquiry_detail&dele_id=<?= $inq['inquiry_id'];?>" onClick="return confirm('削除しますか?')">削除</a></td>
							</tr>
						<?php endforeach; ?>
					</table>
				</div>
        </div>
		
    </div>
</div>


<?php
// 履歴表示の読み込み
//    require './pc/page/notice_history_pager.php';
?>

<?php
require './pc/footer.php';
?>