<?php
require './pc/header.php';
?>
<ul class="form" style="background:#FFF; padding:10px;">
	<li>■ CSS</li>
	<li>
		<table width="80%"  border="0" cellpadding="5" cellspacing="1" bgcolor="#431407">
			<tr align="center" bgcolor="#ffb202">
				<td width="30%" style="padding: 2px;"><font style="color:#FFFFFF;font-size:12px;">クラス</font></td>
				<td width="70%" style="padding: 2px;"><font style="color:#FFFFFF;font-size:12px;">内容</font></td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">stamp_box_on</td>
				<td>クーポン一覧（発行）ページの全クーポン表示させ取得出来るクーポンのみボタンを表示させる時の取得出来る時のDIVのクラス名</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">stamp_box_off</td>
				<td>クーポン一覧（発行）ページの全クーポン表示させ取得出来るクーポンのみボタンを表示させる時の取得出来ない時のDIVのクラス名</td>
			</tr>
		</table><br />
	</li>
	<li>■ スタンプページ</li>
	<li>
		<table width="80%"  border="0" cellpadding="5" cellspacing="1" bgcolor="#431407">
			<tr align="center" bgcolor="#ffb202">
				<td width="30%" style="padding: 2px;"><font style="color:#FFFFFF;font-size:12px;">タグ</font></td>
				<td width="70%" style="padding: 2px;"><font style="color:#FFFFFF;font-size:12px;">内容</font></td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">&lt;#notice_loop#&gt<br>〜<br>&lt;/#notice_loop#&gt;</div></td>
				<td style="padding: 2px;">お知らせの内容をループ</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#notice#</td>
				<td style="padding: 2px;">お知らせ内容を表示させる</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#stamp_num#</td>
				<td style="padding: 2px;">ユーザーの現在のスタンプ数を表示させる</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#stamp◯#</td>
				<td style="padding: 2px;">スタンプ画像URLを表示させる<br>(◯に半角でスタンプナンバーを記入する)</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">&lt;#application_display#&gt;<br>〜<br>&lt;/#application_display#&gt;</td>
				<td style="padding: 2px;">クーポンページへのリンクの記述</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#coupon#</td>
				<td style="padding: 2px;">取得クーポンページへのリンクのURLを表示させる</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#coupon_title_◯#</td>
				<td style="padding: 2px;">クーポンタイトルを表示させる（◯ は 半角 1 〜 10）</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#coupon_stamp_num_◯#</td>
				<td style="padding: 2px;">クーポンスタンプ数を表示させる（◯ は 半角 1 〜 10）</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#coupon_image_url_◯#</td>
				<td style="padding: 2px;">クーポン画像を表示させる（◯ は 半角 1 〜 10）</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#coupon_content_◯#</td>
				<td style="padding: 2px;">クーポン内容を表示させる（◯ は 半角 1 〜 10）</td>
			</tr>
		</table><br />
	</li>
	<li>■ クーポン一覧（発行）ページ</li>
	<li>
		<table width="80%"  border="0" cellpadding="5" cellspacing="1" bgcolor="#431407">
			<tr align="center" bgcolor="#ffb202">
				<td width="30%" style="padding: 2px;"><font style="color:#FFFFFF;font-size:12px;">タグ</font></td>
				<td width="70%" style="padding: 2px;"><font style="color:#FFFFFF;font-size:12px;">内容</font></td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">&lt;#coupon_display_A#&gt;<br>〜<br>&lt;/#coupon_display_A#&gt;</div></td>
				<td style="padding: 2px;">取得出来るクーポンのみ表示させるループ表示</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">&lt;#coupon_display_B#&gt;<br>〜<br>&lt;/#coupon_display_B#&gt;</div></td>
				<td style="padding: 2px;">全クーポン表示させ取得出来るクーポンのみボタン表示させるループ表示</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">&lt;#application_link#&gt;<br>〜<br>&lt;/#application_link#&gt;</div></td>
				<td style="padding: 2px;">取得するリンクの記述（&lt;#coupon_display_B#&gt;のみ使用可能）</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#stamp_num#</div></td>
				<td style="padding: 2px;">クーポンのスタンプ数を表示させる</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#image_url#</div></td>
				<td style="padding: 2px;">クーポンのイメージのURLを表示させる</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#coupon_title#</div></td>
				<td style="padding: 2px;">クーポンのタイトルを表示させる</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#coupon_content#</div></td>
				<td style="padding: 2px;">クーポンの内容を表示させる</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#application#</div></td>
				<td style="padding: 2px;">応募リンクのURLを表示させる。</td>
			</tr>
		</table><br />
	</li>
	<li>■ 取得済みクーポンページ</li>
	<li>
		<table width="80%"  border="0" cellpadding="5" cellspacing="1" bgcolor="#431407">
			<tr align="center" bgcolor="#ffb202">
				<td width="30%" style="padding: 2px;"><font style="color:#FFFFFF;font-size:12px;">タグ</font></td>
				<td width="70%" style="padding: 2px;"><font style="color:#FFFFFF;font-size:12px;">内容</font></td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">&lt;#acquisition_coupon#&gt; 〜 &lt;/#acquisition_coupon#&gt;</div></td>
				<td style="padding: 2px;">取得済みクーポンループ表示</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#acquisition_date#</div></td>
				<td style="padding: 2px;">クーポン取得日</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#acquisition_stamp_num#</div></td>
				<td style="padding: 2px;">スタンプ数</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#acquisition_image_url#</div></td>
				<td style="padding: 2px;">クーポン画像URL</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#acquisition_coupon_title#</div></td>
				<td style="padding: 2px;">クーポンタイトル</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#acquisition_coupon_content#</div></td>
				<td style="padding: 2px;">クーポン内容</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#acquisition_coupon_use_end#</div></td>
				<td style="padding: 2px;">クーポン有効期限</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#acquisition_use_url#</div></td>
				<td style="padding: 2px;">使用する</td>
			</tr>
		</table><br />
	</li>
	<li>■ 店舗設定の店舗情報デザイン設定</li>
	<li>
		<table width="80%"  border="0" cellpadding="5" cellspacing="1" bgcolor="#431407">
			<tr align="center" bgcolor="#ffb202">
				<td width="30%" style="padding: 2px;"><font style="color:#FFFFFF;font-size:12px;">タグ</font></td>
				<td width="70%" style="padding: 2px;"><font style="color:#FFFFFF;font-size:12px;">内容</font></td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#store_name#</div></td>
				<td style="padding: 2px;">店舗名</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#map_url#</div></td>
				<td style="padding: 2px;">地図URL</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#business_hours#</div></td>
				<td style="padding: 2px;">営業時間</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#tel#</div></td>
				<td style="padding: 2px;">電話番号</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#store_rest#</div></td>
				<td style="padding: 2px;">定休日</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#hp_url#</div></td>
				<td style="padding: 2px;">ホームページURL</td>
			</tr>
			<tr bgcolor="#ffffff">
				<td align="center" style="padding: 2px;">#store_prelusion#</div></td>
				<td style="padding: 2px;">店舗紹介文</td>
			</tr>
		</table><br />
	</li>
</ul>
<?php
require './pc/footer.php';
?>