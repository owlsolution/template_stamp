<?php
require './pc/header.php';
?>
<div class="box">
		<h2> <img src="images/icon1.gif" width="16" height="16"> 統計情報 </h2>
		<div class="container">
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
						<tr align="center">
								<th width="10%">年/月</th>
								<th width="10%">総人数</th>
								<th width="5%">アクティブ人数1</th>
								<th width="5%">アクティブ人数2</th>
								<th width="5%">アクティブ人数3</th>
								<th width="5%">アクティブ人数4</th>
								<th width="8%">非アクティブ1<br>(1ヶ月アクセスがない)人数</th>
								<th width="8%">非アクティブ2<br>(2ヶ月アクセスがない)人数</th>
								<th width="8%">非アクティブ3<br>(3ヶ月アクセスがない)人数</th>
								<th width="8%">非アクティブ6<br>(半年アクセスがない)人数</th>
								<th width="5%">再来店人数(のべ)</th>
								<th width="10%">リピート率1<br>再来店人数/アクティブ人数1*100</th>
								<th width="10%">リピート率2<br>再来店人数/アクティブ人数2*100</th>
								<th width="10%">リピート率3<br>再来店人数/アクティブ人数3*100</th>
								<th width="10%">リピート率4<br>再来店人数/アクティブ人数4*100</th>
								<th width="10%">お知らせ配信数</th>
								<th width="10%">プレゼント発行数</th>
						</tr>
						<?php
						foreach ($new_user_list_by_month as $value) {
						?>
						<tr>
								<?php
									$target_month = $value['add_month'];
									$target_num = $value['active_user_num'];
									$target_num1 = $value['active_user_num1'];
									$target_num2 = $value['active_user_num2'];
									$target_num5 = $value['active_user_num5'];
									$last_access = $value['last_access'];
									$last_access1 = $value['last_access1'];
									$last_access2 = $value['last_access2'];
									$last_access5 = $value['last_access5'];
									$repeat_num = $value['repeat_num'];
									$repeat_percent = $value['repeat_percent'];
									$repeat_percent1 = $value['repeat_percent1'];
									$repeat_percent2 = $value['repeat_percent2'];
									$repeat_percent5 = $value['repeat_percent5'];
									$notice_num = isset($value['notice_num']) ? $value['notice_num'] : "0";
									$present_num = isset($value['present_num']) ? $value['present_num'] : "0";
								
								?>
								<td align="center"><?php echo $target_month; ?></td>
								<td align="center"><?php echo $value['subtotal']; ?></td>
 								<td align="center"><?php echo $target_num; ?></td>
 								<td align="center"><?php echo $target_num1; ?></td>
 								<td align="center"><?php echo $target_num2; ?></td>
 								<td align="center"><?php echo $target_num5; ?></td>
 								<td align="center"><?php echo $last_access; ?></td>
 								<td align="center"><?php echo $last_access1; ?></td>
 								<td align="center"><?php echo $last_access2; ?></td>
 								<td align="center"><?php echo $last_access5; ?></td>
								<td align="center"><?php echo $repeat_num; ?></td>
								<td align="center"><?php echo $repeat_percent; ?>%</td>
								<td align="center"><?php echo $repeat_percent1; ?>%</td>
								<td align="center"><?php echo $repeat_percent2; ?>%</td>
								<td align="center"><?php echo $repeat_percent5; ?>%</td>
								<td align="center"><?php echo $notice_num; ?></td>
								<td align="center"><?php echo $present_num; ?></td>
						</tr>
						<?php
						}
						?>
				</table>
		</div>
</div>

<?php
require './pc/footer.php';
?>