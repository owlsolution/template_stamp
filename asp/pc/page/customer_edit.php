<?php
// 顧客情報詳細表示
require './pc/header.php';
?>
<div class="box">
	<h2> <img src="images/icon7.gif" width="16" height="16"> 顧客情報編集 </h2>
        <br>
        <div class="f_box">
	<form id='updateForm' method="POST" action="./?p=customer&user_id=<?php echo $user_id?>&rally_id=<?php echo $rally_id;?>&update=1" name="updateForm">
            <div class="container2" id="individual_menu">
                    <h3> 顧客情報 </h3>
                    <ul class="clearfix">
                            <li class="clearfix">
                                    <p class="titel">・名前 </p>
                                    <p class="text">
                                            <input type="text" name="customer_name" id = "individual_total_stamp_num" size="10" value="<?php echo $detail_user['user_name']?>">様
                                    </p>
                            </li>			
                            <li class="clearfix even">
                                    <p class="titel">・性別</p>
                                    <p class="text">
                                            <input type="radio" name="customer_sex" value="man" <?php if($detail_user['sex'] == 1){ echo "checked"; }?> > 男性　　　
                                            <input type="radio" name="customer_sex" value="woman" <?php if($detail_user['sex'] == 2){ echo "checked"; }?> > 女性　　　
                                            <input type="radio" name="customer_sex" value="all" <?php if($detail_user['sex'] == 0){ echo "checked"; }?> > どちらでもない
                                    </p>
                            </li>
                            <li class="clearfix">
                                    <p class="titel">・誕生日</p>
                                    <p class="text">
                                            <select name="birth_year">
                                                    <?php
                                                    for($i=1930;$i<=date("Y");$i++){
                                                    ?>
                                                    <option value="<?php echo $i;?>" <?php if($i == $birth_year){ echo "selected";}?>><?php echo $i;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                            </select>
                                            年
                                            <select name="birth_month">
                                                    <?php
                                                    for($i=1;$i<=12;$i++){
                                                    ?>
                                                    <option value="<?php echo $i;?>" <?php if($i == $birth_month){ echo "selected";}?>><?php echo $i;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                            </select>
                                            月
                                            <select name="birth_day">
                                                    <?php
                                                    for($i=1;$i<=31;$i++){
                                                    ?>
                                                    <option value="<?php echo $i;?>" <?php if($i == $birth_day){ echo "selected";}?>><?php echo $i;?></option>
                                                    <?php
                                                    }
                                                    ?>
                                            </select>
                                            日
                                    </p>
                            </li>
                            <li class="clearfix even">
                                    <p class="titel">・地域</p>
                                    <p class="text">
										<select name="customer_region">
											<option value=""><?php echo ALL_REGION_LABEL;?></option>
											<?php
												$region_group_list = json_decode(REGION_LIST,true);
												foreach ($region_group_list as $group) {
													// １つのグループを取り出す
													if(!empty($group['gname'])) {
														echo '<optgroup label="'.$group['gname'].'">';
													}

													// 地域アイテムのループ
													$region_list = explode(",", $group['regions']);
													foreach ( $region_list as $region_item) {
														$elem = ($individual_region == $region_item) ? "selected" : "";
														echo '<option value="'.$region_item.'" '.$elem.'>'.$region_item.'</option>';
													}

													if(!empty($group['gname'])) {
														echo '</optgroup>';
													}
												}
											?>
										</select>
                                    </p>
                            </li>
                            <li class="clearfix">
                                    <p class="titel">・現在のｽﾀﾝﾌﾟ数 </p>
                                    <p class="text">
                                        <input type='text' id='current_stamp_num' name='current_stamp_num' size='4' value="<?php echo $detail_user['stamp_num']?>">
                                    </p>
                            </li>
                            <li class="clearfix even">
                                    <p class="titel">・最終スタンプ日 </p>
                                    <p class="text">
                                        <input type='text' id='last_stamp_day' name='last_stamp_day' size='24' value="<?php  $today = explode(" ",$detail_user['last_stamp_date']); echo str_replace("-","/",$today[0]);?>">
                                        　<button id="set_today" type="button" >⇦現在日を自動入力　</button>
                                    </p>
                            </li>
							<br>
                    </ul>
                    <div class="btn">
			<input name="submit" type="submit" value="更 新">
		    </div>

            </div>
        </form>
        <br>
            <div class="container2" id="individual_menu">
                    <h3> 顧客ｸｰﾎﾟﾝ情報 </h3>
                    <ul class="clearfix">
                            <li class="clearfix">
                                    <p class="titel">・ｸｰﾎﾟﾝ付与</p>
                                    <p class="text">
                                        <?php 
                                        
                                        for ($i = 1; $i <= 10; $i++) {
                                            echo empty($goal_coupon['goal_title_'.$i]) ? "" : "<input type='radio' name='add_coupon_group' value='".$i."' >".$goal_coupon['goal_title_'.$i]."</input></br>";
                                        }
                                        ?>
                                        <br>
                                        <button id="coupon_add">　付 与　</button>
                                    </p>
                            </li>
                            <li class="clearfix">
                                <p class="titel">・ｸｰﾎﾟﾝ発行履歴 </p>
                                <table id="get_coupon_tbl" width="100%"  border="0" cellpadding="0" cellspacing="0" >
                                    <tr align="center" >
                                        <th width="50%" bgcolor="#ffb202">保持ｸｰﾎﾟﾝ名</th>
                                        <th width="20%" bgcolor="#ffb202">使用状況</th>
                                        <th width="20%" bgcolor="#ffb202">使用期限</th>
                                        <th width="10%" bgcolor="#ffb202">操作</th>
                                    </tr>
                                    <?php
                                        // クーポン取得履歴
                                        $count_hist = count($got_coupon_history);
                                        for ($i = 0; $i < $count_hist; $i++) {
                                            $date_parts = Util::get_rally_of_day($got_coupon_history[$i]['coupon_use_end'], $rally_id);
                                    ?>
                                    
                                    <tr id="<?php echo $got_coupon_history[$i]['get_coupon_id'];?>" align="center" >
                                    <td align="left" valign="top">
                                            <?php echo $got_coupon_history[$i]['get_coupon_name'];?>
                                    </td>
                                    <td align="center" valign="top">
                                            <?php 
											// 状態：未使用/使用済み/期限切れ
											if ($got_coupon_history[$i]['get_coupon_state'] == "1") {
												echo "使用済み";
											} else if (strtotime(date('Y/m/d')) > strtotime($got_coupon_history[$i]['coupon_use_end'])) {
												echo "期限切れ";
											} else {
												echo "未使用";
											}
											?>
                                    </td>
                                    <td align="center" valign="top">
                                            <?php echo $date_parts[0]."/".$date_parts[1]."/".$date_parts[2];?>
                                    </td>
                                    <td align="center" valign="top">
                                        <a class="coupon_delete">削除</a>
                                    </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </table>
                            </li>			
                    </ul>
            </div>
        </div>
        
</div>

<script type="text/javascript">
$(document).ready(function() {
        // Formに対するバリデーション
        $("#updateForm").validate({
            rules : {
                current_stamp_num: {
                    required: true,
                    min: 0
                 }
            },
            messages: {
               current_stamp_num: {
                 required: " 必須項目です。数値を入力してください。",
                 min: " 0以上の数字を入力してください。"
               }
            }
          });
});

function initCouponDelete() {
    //クーポン削除をクリックしたときの処理
    $('.coupon_delete').click(function() {
                
        if(!confirm("本当に削除しますか？")) {
            return;
        }
        
        $.ajax({
            type: "POST",        
            url: "./sys_controller/CustomerEditController.php", 
            data: {
                action : "coupon_delete",
                    get_coupon_id : $(this).parent().parent("tr").attr("id")
            }, 
            success: function(data) {
                if (data.length > 0) {
                    // 削除 coupon_idが合致する行を削除
                    var jsonpData = JSON.parse(data);
                    var coupon = $("#"+jsonpData.get_coupon_id);
                    if (jsonpData.get_coupon_id === coupon.attr("id")) {
                        coupon.remove();
                    }
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log("XMLHttpRequest : " + XMLHttpRequest.status);
                console.log("textStatus : " + textStatus);
                console.log("errorThrown : " + errorThrown.message);
            }
        });
    });

}


$ (function(){
    
        // クーポン削除要素の初期化
        initCouponDelete();
        
	//クーポン付与をクリックしたときの処理
	$('#coupon_add').click(function() {
                
                if(!confirm("選択したクーポンを付与しますか？")) {
                    return;
                }
                $.ajax({
                    type: "POST",        
                    url: "./sys_controller/CustomerEditController.php", 
                    data: {
                        action : "coupon_add",
                        get_coupon_no : $("input:radio[name='add_coupon_group']:checked").val()
                    }, 
                    success: function(data) {
                        if (data.length > 0) {
                            var jsonpData = JSON.parse(data);
                            
                            // クーポン名要素生成
                            var $coupon_name = $("<td/>").attr("align","left").attr("valign","top").append(jsonpData.get_coupon_name);
                            // クーポン使用状況
                            var $coupon_state = $("<td/>").attr("align","center").attr("valign","top").append("未使用");
                            // クーポン期限要素生成
                            var $coupon_date = $("<td/>").attr("align","center").attr("valign","top").append(jsonpData.coupon_use_end.replace(/-/g, "/"));
                            // クーポン削除要素生成
                            var $coupon_delete = $("<td/>").attr("align","center").attr("valign","top").append('<a class="coupon_delete" >削除</a>');
                            // クーポン行を生成
                            var $coupon_item = $("<tr/>").attr("align","center").attr("id", jsonpData.get_coupon_id)
                                    .append($coupon_name)
                                    .append($coupon_state)
                                    .append($coupon_date)
                                    .append($coupon_delete);
                            
                            // クーポン一覧へ追加する
                            $("#get_coupon_tbl").append($coupon_item);

                            // 追加したクーポンに削除の取りがが未設定なので初期化する
                            initCouponDelete();

                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        console.log("XMLHttpRequest : " + XMLHttpRequest.status);
                        console.log("textStatus : " + textStatus);
                        console.log("errorThrown : " + errorThrown.message);
                    }
                });
	});
	
	$('#set_today').click(function() {
		var now = new Date();
		var today = now.getFullYear() + "/" + ( "0"+( now.getMonth()+1 ) ).slice(-2) +  "/"  + ( "0"+now.getDate() ).slice(-2);
		$('#last_stamp_day').val(today);
	});


});
</script>


<?php
require './pc/footer.php';
?>