<div class="box">
	<h2> <img src="images/icon3.gif" width="16" height="16"> 発行履歴 </h2>
	<div class="container">
		<table width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr align="center">
				<th width="40%">クーポン名</th>
				<th width="10%">発行数</th>
				<th width="10%">利用数</th>
				<th width="10%">クーポン開始日時</th>
				<th width="10%">クーポン終了日時</th>
				<th width="15%"></th>
			</tr>
			<?php
			foreach ($gift_coupon_array as $value){
			?>
			<tr>
				<!--<td align="center"><?php //echo $value['coupon_start'];?></td>-->
				<td><?php echo $value['coupon_name'];?></td>
				<td align="center"><?php echo $value['gifted_num'];?></td>
				<td align="center"><?php echo $value['used_num'];?>/<?php echo $value['gifted_num'];?></td>
				<td align="center"><?php echo $value['coupon_start'];?></td>
				<td align="center"><?php echo $value['coupon_end'];?></td>
				<td align="center"><a href="?p=gift&check_id=<?php echo $value['id'];?>">確認</a> ｜ <?php /*<a href="?p=gift&stop_id=<?php echo $value['id'];?>">停止</a> ｜*/?> <a href="?p=gift&dele_id=<?php echo $value['id'];?>" onClick="return confirm('削除しますか?')">削除</a></td>
			</tr>
			<?php
			}
			?>
		</table>
	</div>
</div>