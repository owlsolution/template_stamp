<?php
require './pc/header.php';
?>
<div style="background:#FFF; padding:10px;">
<ul class="form">
	<form method="POST" action="<?php echo $url;?>" name="form2">
		<div class="f_box">
			<div class="container1">
				<h2> <img src="images/icon10.gif" width="16" height="16"> プッシュ通知時間設定 </h2>
				<br>
				<h3> スタンプ/クーポン期限のプッシュ通知時間設定 </h3>
				<ul>
					<?php echo $send_time; ?>時
					<input name="send_time" type="hidden" value="<?php echo $send_time;?>"/>
				</ul>
				<br>
				<h3> 期限切れ事前通知設定 </h3>
				<ul>
							・スタンプ期限<?php echo $stamp_limit_days; ?>日前<br>
								<input name="stamp_limit_days" type="hidden" value="<?php echo $stamp_limit_days;?>"/>
								<p style="color:red; font-size: 12px;">※「-1」はプッシュ通知をしない設定となっております。 </p>
				</ul><br />
				<ul>
							<br>
							・クーポン期限<?php echo $coupon_limit_days; ?>日前<br>
								<input name="coupon_limit_days" type="hidden" value="<?php echo $coupon_limit_days;?>"/>
								<p style="color:red; font-size: 12px;">※「-1」はプッシュ通知をしない設定となっております。 </p>
				</ul><br />

						<br />
				<div align="center">
					<li class="btm_body">
						<div class="btn">
							<input name="setting" type="submit" class="btm" value="設 定" /><input name="return" type="submit" class="btm" value="戻 る" />
						</div>
					</li>
				</div>
				<br>
			</div>
		</div>
	</form>
</ul>
</div>
<?php
require './pc/footer.php';
?>