<?php
require './pc/header.php';
?>
<div class="box">
    <form id="form2" method="POST" action="./?p=notice<?php echo $to_user_id != '' ? "&to_user_id=".$to_user_id : ''; ?>" name="form2" enctype="multipart/form-data">
		<input type="hidden" name="exclusion_user_id" id="exclusion_user_id" value="<?php echo $exclusion_user_id;?>">
		<h2> <img src="images/icon1.gif" width="16" height="16"> 配信機能 </h2>
		<div class="container">
			<ul>
				ここで、参加者へ各種お知らせ配信ができます。
				<p style="color: #FF0000">送信内容は下記の通りで宜しいですか？<br />宜しければ送信ボタンを押してください。</p>
			</ul>
			<div class="f_box">
				<div class="container1">
					<h3> 配信設定 </h3>
					<ul class="clearfix">
						<li>
							<?php echo $delivery_set_name;?><input type="hidden" name = "delivery_set" value = "<?php echo $delivery_set;?>" >
						</li>
					</ul>
				</div>
				
				<div class="container2" id="individual_menu">
					<h3> スケジュール設定内容 </h3>
					<ul class="clearfix">
						<li class="clearfix">
							<p class="titel">・設定タイプ</p>
							<p class="text"><?php echo $schedule_type_name;?></p>
							<input type="hidden" name = "schedule_type" value = "<?php echo $schedule_type ; ?>" >
						</li>
						<li class="clearfix even">
							<p class="titel">・設定内容</p>
							<p class="text"><?php echo $schedule_type_comment;?></p>
							<input type="hidden" name = "new_user_time_difference" value = "<?php echo $new_user_time_difference ; ?>" >
							<input type="hidden" name = "new_user_day_difference" value = "<?php echo $new_user_day_difference ; ?>" >
							<input type="hidden" name = "last_stamp_after_day" value = "<?php echo $last_stamp_after_day ; ?>" >
							<input type="hidden" name = "total_stamps" value = "<?php echo $total_stamps ; ?>" >
							<input type="hidden" name = "total_stamp_after_day" value = "<?php echo $total_stamp_after_day ; ?>" >
							<input type="hidden" name = "before_day_of_birth" value = "<?php echo $before_day_of_birth ; ?>" >
							<input type="hidden" name = "day_of_the_week" value = "<?php echo $day_of_the_week ; ?>" >
							<input type="hidden" name = "monthly_push_day_of_the_month" value = "<?php echo $monthly_push_day_of_the_month ; ?>" >
							<input type="hidden" name = "last_month_stamp_num" value = "<?php echo $last_month_stamp_num ; ?>" >
							<input type="hidden" name = "last_month_push_day" value = "<?php echo $last_month_push_day ; ?>" >
						</li>
					</ul>
				</div>

				<div class="container1">
					<h3> お知らせ形式 </h3>
					<ul class="clearfix">
						<?php
						if($format_no == '1'){
						?>
						<li>
							 プッシュ通知/メール送信
						</li>
						<?php
						} else if($format_no == '2'){
						?>
						<li>
							 プッシュ通知のみ
						</li>
						<?php
						} else if($format_no == '3'){
						?>
						<li>
							 メール送信のみ
						</li>
						<?php
						} else {
						?>
						<li>
							 形式未選択
						</li>
						<?php
						}
						?>
					</ul>
					<?php
					foreach ($format as $val) {
					?>
					<input type="hidden" name = "format[]" value = "<?php echo $val?>" >
					<?php
					}
					?>
				</div>
				
				<div class="container1">
					<h3> 配信時間設定 </h3>
						<p class="time_menu">
							&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $push_hour; ?>
							<input type="hidden" name="push_hour" value="<?php echo $push_hour; ?>">
							時 <?php echo $push_minute; ?>
							<input type="hidden" name="push_minute" value="<?php echo $push_minute; ?>">
							分
							</p>
						<p style="color:red; margin:10px;">※[新規入会後配信]の場合、配信時間指定は無視されます。</p>
					<br>
				</div>
			</div>
		</div>
		<div class="box" style="margin:0 150px;">
			<div class="f_box">
				<div class="container1">
					<h3> タイトル (全角50文字以内) </h3>
					<ul class="clearfix">
						<li>
							<?php echo htmlspecialchars($notice_title);?> <input name="notice_title" type="hidden" value="<?php echo htmlspecialchars($notice_title);?>">
						</li>
					</ul>
				</div>
				<div class="container1">
					<h3> 本文 </h3>
					<ul class="clearfix">
						<li>
							<?php 
							// 入力フォームに絵文字が入っていたらremoveEmoji関数で削除する
							$notice_date = Util::removeEmoji($notice_date);
							if ($delivery_type != "leaflets") {
								$notice_str = $notice_date;
								$img_src_start = '<img src="./../notice_images/';
								$img_src_end = '" width="100%">';
								$notice_str = str_replace("<img #", $img_src_start, $notice_str);
								$notice_str = str_replace("#>", $img_src_end, $notice_str);
								echo nl2br($notice_str);
							?>
								<input name="notice_date" type="hidden" value="<?php echo htmlspecialchars($notice_date);?>">
							<?php 
								}
							?>
						</li>
					</ul>
					<?php
					if(empty($check_id)){
					?>
					<div class="btn">
						<input id="setting" name="schedule_setting" type="submit" value="送信"> <input id="return"　name="return" type="submit" value="キャンセル" />
						<input type="hidden" name = "delivery_type" value = "<?php echo $delivery_type ?>" >
					</div>
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</form>
</div>
<?php
// 確認のリンクを押下して遷移してきた場合のみ表示
if(!empty($check_id)){
    // 履歴表示の読み込み
    if($to_user_id != ''){
        require './pc/page/notice_history_to_person.php';
    } else {
        require './pc/page/notice_schedule_list.php';
    }
}
?>

<?php
require './pc/footer.php';
?>

<script>
    // キャンセルボタンが押された場合
    $('#return').click(function(event){
        window.history.back();
        event.preventDefault();
    });

</script>
