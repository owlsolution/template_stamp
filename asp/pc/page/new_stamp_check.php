<?php
require './pc/header.php';
?>
<ul class="form">
	<form method="POST" action="<?php echo $url;?>" name="form2">
		<li>●スタンプ名</li>
		<li>  <?php echo $_POST['rally_name'];?><input name="rally_name" type="hidden" value="<?php echo $_POST['rally_name'];?>"/></li>
		<li>●スタンプ有効期限</li>
		<?php
			$rally_expire_type_text = "ユーザーがスタンプ作成してから　".$_POST['stamp_day']."日";
		?>
		<li>  <?php echo $rally_expire_type_text;?> <input name="rally_expire_type" type="hidden" value="<?php echo $_POST['rally_expire_type'];?>" /><input name="stamp_day" type="hidden" value="<?php echo $_POST['stamp_day'];?>" /><input name="stamp_date" type="hidden" value="<?php echo $_POST['stamp_date'];?>" />
		</li>
		<li>●クーポン有効期限</li>
		<?php
		$goal_expire_type_text = "ユーザーがゴールしてから　".$_POST['goal_day']."日";
		?>
		<li>  <?php echo $goal_expire_type_text;?> <input name="goal_expire_type" type="hidden" value="<?php echo $_POST['goal_expire_type'];?>" /><input name="goal_day" type="hidden" value="<?php echo $_POST['goal_day'];?>" /><input name="goal_date" type="hidden" value="<?php echo $_POST['goal_date'];?>" />
		<li>●全スタンプ数</li>
		<li>  <?php echo $_POST['stamp_max']; ?><input name="stamp_max" type="hidden" value="<?php echo $_POST['stamp_max'];?>" /> 個</li>
		<li>●スタンプ付与制限（１日１回のみ）</li>
		<li><?php if($_POST['one_times_a_day'] == 1){ echo "ON"; } else { echo "OFF";}?><input name="one_times_a_day" type="hidden" value="<?php echo $_POST['one_times_a_day'];?>" /></li>
		<li>●新規スタンプ数</li>
		<li><?php echo $_POST['new_stamp_num']?>個<input name="new_stamp_num" type="hidden" value="<?php echo $_POST['new_stamp_num'];?>" /></li>
		<li>●ユーザがプロフィールを登録しているかチェック</li>
		<li>
			<?php 
				$profile_force_text = "プロフィール登録チェックをしない。";
				if ($_POST['user_profile_force_check'] == 1) {
					if ($_POST['profile_force'] == 2) {
						$profile_force_text = "アプリ起動時にプロフィール入力をポップアップで促す。";
					}else if ($_POST['profile_force'] == 3) {
						$profile_force_text = "クーポンを取得する際に、プロフィール入力をポップアップで促す。(未入力の場合クーポン取得不可)";
					}else if ($_POST['profile_force'] == 5) {
						$profile_force_text = "アプリ起動時とユーザーがクーポンを取得する際に、プロフィール入力をポップアップで促す。";
					}
				}
				echo $profile_force_text;
			?>
			<input type="hidden" name="user_profile_force_check" value="<?php echo $_POST['user_profile_force_check']; ?>" >
			<input type="hidden" class="profile_force" name="profile_force" value="<?php echo $_POST['profile_force']; ?>" ><br>
		</li><br />
		<div align="center">
			<li class="btm_body">
				<input name="setting" type="submit" class="btm" value="設定" /><input name="return" type="submit" class="btm" value="戻る" />
			</li>
		</div>
	</form>
</ul>
<?php
require './pc/footer.php';
?>