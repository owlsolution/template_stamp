<?php
require './pc/header.php';
?>
<div class="box">
    <form id="form2" method="POST" action="./?p=notice<?php echo $to_user_id != '' ? "&to_user_id=".$to_user_id : ''; ?><?php echo empty($edit_id) ? '' : "&edit_id=".$edit_id; ?>" name="form2" enctype="multipart/form-data">
		<input type="hidden" name="exclusion_user_id" id="exclusion_user_id" value="<?php echo $exclusion_user_id;?>">
		<h2> <img src="images/icon1.gif" width="16" height="16"> 配信機能 </h2>
		<div class="container">
			<ul>
				お知らせ配信の編集ができます。
				<?php
				if(empty($check_id)){
				?>
				<?php
				}
				?>
			</ul>
			<div class="f_box">
				<?php if ($delivery_set == 'blog') { ?>
				<div class="container1">
					<h3> 配信者設定 </h3>
					<ul class="clearfix">
						<li>
							<?php echo $staff_name;?><input type="hidden" name = "staff_admin_id" value = "<?php echo $staff_admin_id;?>" >
						</li>
					</ul>
				</div>
				<?php } ?>

				<div class="container1">
					<h3> 配信設定 </h3>
					<ul class="clearfix">
						<li>
							<?php echo $delivery_set_name;?><input type="hidden" name = "delivery_set" value = "<?php echo $delivery_set;?>" >
						</li>
					</ul>
				</div>
				<?php
				if($delivery_set == "individual"){
				?>
				<div class="container2" id="individual_menu">
					<h3> 個別配信絞り込み </h3>
					<ul class="clearfix">
						<li class="clearfix">
							<p class="titel">・性別</p>
							<p class="text">
								<?php echo $individual_sex_date;?><input type="hidden" name = "individual_sex" value = "<?php echo $individual_sex;?>" >
							</p>
						</li>
						<li class="clearfix even">
							<p class="titel">・誕生日</p>
							<p class="text"><?php echo $individual_birthday_start ; ?> 月　〜　<?php echo $individual_birthday_end ; ?> 月</p>
							<input type="hidden" name = "individual_birthday_start" value = "<?php echo $individual_birthday_start ; ?>" >
							<input type="hidden" name = "individual_birthday_end" value = "<?php echo $individual_birthday_end ; ?>" >
						</li>
						<li class="clearfix">
							<p class="titel">・地域</p>
							<?php
							if(empty($individual_region)){
								$individual_string = "全ての都道府県";
							} else {
								$individual_string = $individual_region;
							}
							?>
							<p class="text"> <?php echo $individual_string;?> </p>
							<input type="hidden" name = "individual_region" value = "<?php echo $individual_region;?>" >
						</li>
						<li class="clearfix even">
							<p class="titel">・最終来店日</p>
							<p class="text"> <?php echo $last_year_start;?> 年 <?php echo $last_month_start; ?> 月 <?php echo $last_day_start;?> 日　〜　 <?php echo $last_year_end;?> 年 <?php echo $last_month_end; ?> 月 <?php echo $last_day_end;?> 日 </p>
							<input type="hidden" name = "last_start" value = "<?php echo $last_year_start;?>-<?php echo $last_month_start; ?>-<?php echo $last_day_start;?>" >
							<input type="hidden" name = "last_end" value = "<?php echo $last_year_end;?>-<?php echo $last_month_end; ?>-<?php echo $last_day_end;?>" >
						</li>
						<li class="clearfix">
							<p class="titel">・累計スタンプ数</p>
							<p class="text"><?php echo $total_stamp_num ; ?>個　<?php if($total_stamp_terms == "or_more"){ echo "以上";} else if($total_stamp_terms == "downward"){ echo "以下";}?></p>
							<input type="hidden" name = "total_stamp_num" value = "<?php echo $total_stamp_num;?>" >
							<input type="hidden" name = "total_stamp_terms" value = "<?php echo $total_stamp_terms;?>" >
						</li>
						<li class="clearfix even">
							<p class="titel">・キーワード検索</p>
							<p class="text"> 
								<?php 
								if(empty($keyword)){
									$keyword_result = "キーワードなし";
								} else {
									$keyword_result = $keyword;
								}
								echo $keyword_result;?>
							</p>
							<input type="hidden" name = "search_keyword" value = "<?php echo $keyword;?>" >
						</li>
						<li class="clearfix">
							<p class="titel">・店舗一覧</p>
							<p class="text"><?php echo $branch_name;?></p>
							<input type="hidden" name = "branch_list" value = "<?php echo $branch_id;?>" >
						</li>
					</ul>
				</div>
				<?php
				}
				if($delivery_set == "simple"){
				?>
				<!-- 簡単配信 -->
				<div class="container1" id="simple_menu">
					<h3> お知らせしたい人 </h3>
					<ul class="clearfix">
						<?php
						if(in_array("last_month", $simple_check)){
						?>
						<li>
							 先月誕生日の人
						</li>
						<?php
						}
						if(in_array("this_month", $simple_check)){
						?>
						<li>
							 今月誕生日の人
						</li>
						<?php
						}
						if(in_array("next_month", $simple_check)){
						?>
						<li>
							 来月誕生日の人
						</li>
						<?php
						}
						if(in_array("man", $simple_check)){
						?>
						<li>
							男性
						</li>
						<?php
						}
						if(in_array("woman", $simple_check)){
						?>
						<li>
							 女性
						</li>
						<?php
						}
						if(in_array("yesterday_come", $simple_check)){
						?>
						<li>
							 昨日来店した人
						</li>
						<?php
						}
						if(in_array("month_come", $simple_check)){
						?>
						<li>
							 1ヶ月以上来店してないの人
						</li>
						<?php
						}
						if(in_array("this_month_come", $simple_check)){
						?>
						<li>
							 今月来店した人
						</li>
						<?php
						}
						?>
					</ul>
					<?php
					foreach ($simple_check as $val) {
					?>
					<input type="hidden" name = "simple_check[]" value = "<?php echo $val;?>" >
					<?php
					}
					?>

				</div>
				<?php
				}
				if($delivery_set != "individual" && $delivery_set != "simple"){
					foreach ($format as $val) {
					?>
					<input type="hidden" name = "format[]" value = "<?php echo $val?>" >
					<?php
					}
				} else {
				?>
				<div class="container1">
					<h3> お知らせ形式 </h3>
					<ul class="clearfix">
						<?php
						if(in_array("push", $format)){
						?>
						<li>
							 プッシュ通知
						</li>
						<?php
						}
						if(in_array("mail", $format)){
						?>
						<li>
							 メール送信
						</li>
						<?php
						}
						?>
					</ul>
					<?php
					foreach ($format as $val) {
					?>
					<input type="hidden" name = "format[]" value = "<?php echo $val?>" >
					<?php
					}
					?>
				</div>
				<?php
				}
				?>

                                <div class="container1">
                                        <h3> 配信時間指定 </h3>
                                        <p class="titel">&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="reserve_check" value="<?php echo $reserve_check == 1 ? '1' : '0'?>"><?php echo $reserve_check == 1 ? '配信指定あり' : '配信指定なし'?></p>
                                        <br>
                                        <?php if($reserve_check == 1) { ?>
                                        <p class="time_menu">
                                                &nbsp;&nbsp;&nbsp;&nbsp;指定時間&nbsp;:&nbsp;<?php echo $reserve_year; ?>
                                                <input type="hidden" name="reserve_year" value="<?php echo $reserve_year; ?>">
                                                年<?php echo $reserve_month; ?>
                                                <input type="hidden" name="reserve_month" value="<?php echo $reserve_month; ?>">
                                                月<?php echo $reserve_day; ?>
                                                <input type="hidden" name="reserve_day" value="<?php echo $reserve_day; ?>">
                                                日 <?php echo $reserve_hour; ?>
                                                <input type="hidden" name="reserve_hour" value="<?php echo $reserve_hour; ?>">
                                                時 <?php echo $reserve_minute; ?>
                                                <input type="hidden" name="reserve_minute" value="<?php echo $reserve_minute; ?>">
                                                分
                                        </p>
                                        <?php } // if($reserve_check) ?>
                                        <br>
                                </div>

				<div class="f_digit">
					<div class="titel"> お知らせを送る人数 </div>
					<div class="digit"><span id = "search_data"> <?php echo $delivery_num ;?> </span><strong>/<?php echo $all_customer?></strong> <input type="hidden" name = "delivery_num" id = "delivery_num" value = "<?php echo $delivery_num ;?>" > </div>
				</div>
			</div>
		</div>
		<div class="box" style="margin:0 150px;">
			<div class="f_box">
				<div class="container1">
					<h3> タイトル (全角50文字以内) </h3>
					<ul class="clearfix">
						<li>
							<input name="notice_title" type="input" class="f_text1" value="<?php echo htmlspecialchars($notice_title);?>">
						</li>
					</ul>
				</div>
				<div class="container1">
					<h3> 本文 </h3>
					<ul class="clearfix">
						<li>
                                                    <?php 
                                                        // 入力フォームに絵文字が入っていたらremoveEmoji関数で削除する
                                                        $notice_date = Util::removeEmoji($notice_date);
                                                       if ($delivery_type != "leaflets") {
                                                    ?>
                                                            <textarea class="f_text2" name="notice_date" id="notice_date1" placeholder="配信の内容をここに入力してください。"><?php echo htmlspecialchars($notice_date);?></textarea>
                                                    <?php 
                                                        } else {    // チラシの場合
                                                    ?>
                                                            <?php
                                                            echo '<img src="./../notice_images/'.$leaflets_file_name.'" width="100%">';
                                                            ?>
                                                            <input name="notice_date" type="hidden" value="<?php echo htmlspecialchars($notice_str);?>">
                                                            <input id="leaflets_file_name" name="leaflets_file_name" type="hidden" value="<?php echo $leaflets_file_name ?>">
															<p style="color: #FF0000">※チラシ画像の変更はできません。</p>
                                                    <?php 
                                                        }
                                                    ?>
						</li>
					</ul>
					<?php
					if(empty($check_id) && empty($edit_id)){
					?>
					<div class="btn">
						<input id="setting" name="setting" type="submit" value="送信"> <input id="return"　name="return" type="submit" value="キャンセル" />
                                                <input type="hidden" name = "delivery_type" value = "<?php echo $delivery_type ?>" >
					</div>
					<?php
					}
					?>
					<?php
					if($edit_id){
					?>
					<div class="btn">
						<input id="edit" name="edit" type="submit" value="更新"> <input id="return"　name="return" type="submit" value="戻る" />
					</div>
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</form>
</div>
<?php
// 確認のリンクを押下して遷移してきた場合のみ表示
if(!empty($check_id)){
    // 履歴表示の読み込み
    if($to_user_id != ''){
        require './pc/page/notice_history_to_person.php';
    } else {
        require './pc/page/notice_history_pager.php';
    }
}
?>

<?php
require './pc/footer.php';
?>

<script>
    // キャンセルボタンが押された場合
    $('#return').click(function(event){
        window.history.back();
        event.preventDefault();
    });

</script>
