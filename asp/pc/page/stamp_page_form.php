<?php
require './pc/header.php';
?>
<ul class="form" style="background:#FFF; padding:10px;">
	<form method="POST" action="./?p=stamp_page" name="form2">
		<div align="right">
			<li class="btm_body">
				<select name="stamp_select" id="select">
					<option value="0" <?php if($stamp_select == "" || $stamp_select == 0){ echo "selected"; }?>>選択してください</option>
					<?php
					$db = db_connect();
					$where = "admin_id = ".ADMIN_ID;
					$rally_date = rally_select($db , $where);
					while ($rally = mysql_fetch_array($rally_date)){
					?>
					<option value="<?php echo $rally['rally_id'];?>" <?php if($stamp_select == $rally['rally_id']){ echo "selected";  $stamp_max = $rally['stamp_max']; }?>><?php echo $rally['rally_name']?></option>
					<?php
					}
					db_close( $db );
					?>
				</select>
				<input name="display" type="submit" class="btm" value="表示" />
			</li>
		</div>
		<li></li>
		<?php
		if($stamp_select != "" && $stamp_select != 0){
			if(isset($stamp_select)){
				$db = db_connect();
				$where = "rally_id = ".$stamp_select;
				$stamp_page_date = stamp_page_select($db , $where);
				$stamp_page = mysql_fetch_array($stamp_page_date);
			}
		?>
		<li>■ 共通CSS</li>
		<li><textarea name="page_css" rows="10" cols="100"><?php echo $stamp_page['page_css'];?></textarea></li>
		<li><br />
<br />
<hr><br />
<br />
</li>
		<li>■ ヘッダー</li>
		<li> フィーチャーフォン </li>
		<li><textarea name="future_header_page" rows="10" cols="100"><?php echo $stamp_page['future_header_page'];?></textarea></li>
		<li><br />
<br />
<hr><br />
<br />
</li>
		<li>■ フッター</li>
		<li> フィーチャーフォン </li>
		<li>
			<font style = "color:#009900; font-size:12px;">
				退会URL：#withdrawal#<br>
				利用規約URL：#terms#<br>
				プライバシーポリシーURL：#privacy_policy#<br>
				お店情報URL：#shop_information#<br>
				ヘルプURL：#help#<br>
				設定URL：#setting#<br>
				スタンプカード手帳URL：#summary#<br>
				スタンプカードURL：#home#<br>
			</font>
		</li>
		<li><textarea name="future_footer_page" rows="10" cols="100"><?php echo $stamp_page['future_footer_page'];?></textarea></li>
		<li><br />
<br />
<hr><br />
<br />
</li>
		<li>■ スタンプページ</li>
		<li> タイトル </li>
		<li><input name="title_stamp_page" type="text" value="<?php echo $stamp_page['title_stamp_page'];?>" size="60" /></li>
		<li> フィーチャーフォン </li>
		<li>
			<font style = "color:#009900; font-size:12px;">
				お知らせループ：&lt;#notice_loop#&gt 〜 &lt;/#notice_loop#&gt;<br>
				お知らせ内容：#notice#<br>
				現在のスタンプ数：#stamp_num#<br>
				スタンプ画像URL:#stamp◯#<br>
				クーポン発行：&lt;#application_display#&gt; 〜 &lt;/#application_display#&gt;<br>
				クーポン発行ページURL：#application#<br>
				クーポン一覧URL：#coupon#<br>
				クーポンタイトル：#coupon_title_◯#<br>
				クーポンスタンプ数：#coupon_stamp_num_◯#<br>
				クーポン画像URL：#coupon_image_url_◯#<br>
				クーポン内容：#coupon_content_◯#<br>
				設定URL：#setting#<br>
				クーポン発行の促し：#warning#<br>
				追加されたスタンプ分： #stamp_up_num#<br>
				クーポンまでのお知らせ： #coupon_notice#<br>
				ユーザー名（ID）：  #user_name#<br>
			</font>
		</li>
		<li><textarea name="future_stamp_page" rows="10" cols="100"><?php echo $stamp_page['future_stamp_page'];?></textarea></li>
		<li><br />
<br />
<hr><br />
<br />
</li>
		<li>■ クーポン一覧（発行）ページ</li>
		<li> タイトル </li>
		<li><input name="title_coupon_page" type="text" value="<?php echo $stamp_page['title_coupon_page'];?>" size="60" /></li>
		<li> フィーチャーフォン </li>
		<li>
			<font style = "color:#009900; font-size:12px;">
				取得出来るクーポンのみ表示タイプ：&lt;#coupon_display_A#&gt; 〜 &lt;/#coupon_display_A#&gt;<br>
				全クーポン表示タイプ：&lt;#coupon_display_B#&gt; 〜 &lt;/#coupon_display_B#&gt;<br>
				取得するリンクの記述：&lt;#application_link#&gt; 〜 &lt;/#application_link#&gt;<br>
				クーポンのスタンプ数：#stamp_num#<br>
				クーポンタイトル：#coupon_title#<br>
				クーポン画像URL：#image_url#<br>
				クーポン内容：#coupon_content#<br>
				取得URL：#application#
			</font>
		</li>
		<li><textarea name="future_coupon_page" rows="10" cols="100"><?php echo $stamp_page['future_coupon_page'];?></textarea></li>
		<li><br />
<br />
<hr><br />
<br />
</li>
		<li>■ 取得済みクーポンページ</li>
		<li> タイトル </li>
		<li><input name="title_acq_coupon_page" type="text" value="<?php echo $stamp_page['title_acq_coupon_page'];?>" size="60" /></li>
		<li> フィーチャーフォン </li>
		<li>
			<font style = "color:#009900; font-size:12px;">
				取得済みクーポンループ：&lt;#acquisition_coupon#&gt; 〜 &lt;/#acquisition_coupon#&gt;<br>
				取得日：#acquisition_date#<br>
				スタンプ数：#acquisition_stamp_num#<br>
				クーポン画像URL：#acquisition_image_url#<br>
				クーポンタイトル：#acquisition_coupon_title#<br>
				クーポン内容：#acquisition_coupon_content#<br>
				使用する：#acquisition_use_url#</font>
		</li>
		<li><textarea name="future_acq_coupon_page" rows="10" cols="100"><?php echo $stamp_page['future_acq_coupon_page'];?></textarea></li>
		<li><br />
<br />
<hr><br />
<br />
</li>
		<li>■ 有効期限切れページ</li>
		<li> タイトル </li>
		<li><input name="title_expired_page" type="text" value="<?php echo $stamp_page['title_expired_page'];?>" size="60" /></li>
		<li> フィーチャーフォン </li>
		<li><textarea name="future_expired_page" rows="10" cols="100"><?php echo $stamp_page['future_expired_page'];?></textarea></li>
		<li><br />
<br />
<hr><br />
<br />
</li>
		<li>■ お知らせページ</li>
		<li> タイトル </li>
		<li><input name="title_notice_page" type="text" value="<?php echo $stamp_page['title_notice_page'];?>" size="60" /></li>
		<li> フィーチャーフォン </li>
		<li>
			<font style = "color:#009900; font-size:12px;">
				お知らせループ：&lt;#notice_loop#&gt 〜 &lt;/#notice_loop#&gt;<br>
				お知らせ日：#notice_data#<br>
				お知らせタイトル：#notice_title#<br>
				お知らせ内容：#notice_content#</font>
		</li>
		<li><textarea name="future_notice_page" rows="10" cols="100"><?php echo $stamp_page['future_notice_page'];?></textarea></li>
		<li>
		<li> アプリ専用 </li>
		<li>
			<font style = "color:#009900; font-size:12px;">
				お知らせ日：#notice_data#<br>
				お知らせタイトル：#notice_title#<br>
				お知らせ内容：#notice_content#</font>
		</li>
		<li><textarea name="smart_notice_app_page" rows="10" cols="100"><?php echo $stamp_page['smart_notice_app_page'];?></textarea></li>
		<li><br />
		<br />
		<hr><br />
		<br />
		</li>
		<li>■ 利用規約</li>
		<li> フィーチャーフォン </li>
		<li><input name="use_page_date" type="text" value="<?php echo $stamp_page['use_page_date'];?>" size="60" /></li>
		<li><br />
			
		<li>■ アプリからの仮投稿ログインボタン用CSS</li>
		<li><textarea name="easy_login_css" rows="10" cols="100"><?php echo $stamp_page['easy_login_css'];?></textarea></li>
		<li><br />
			
<br />
<hr><br />
<br />
</li>
		<input name="rally_id" type="hidden" value="<?php echo $stamp_select;?>" />
		<div align="center">
			<li class="btm_body">
			<br /><br />
				<li class="btm_body"><input name="setting" type="submit" class="btm" value=" 変 更 " /></li>
			</li>
		</div>
		<?php
		}
		?>
	</form>
</ul>
<?php
require './pc/footer.php';
?>