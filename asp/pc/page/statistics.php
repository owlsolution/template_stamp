<?php
require './pc/header.php';
?>
<div class="box">
		<h2> <img src="images/icon1.gif" width="16" height="16"> 統計情報 </h2>
		<div class="container">
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
						<tr align="center">
								<th width="10%">年/月</th>
								<th width="10%">総人数</th>
								<th width="5%">新規登録人数1(先月)</th>
								<th width="8%">再来店人数(のべ)1<br>(先月新規登録した人で来店した)人数</th>
								<th width="10%">リピート率1<br>再来店人数/新規登録人数1*100</th>
								<th width="10%">お知らせ配信数1</th>
								<th width="5%">新規登録人数2(先月/先々月)</th>
								<th width="8%">再来店人数(のべ)2<br>(先月/先々月に新規登録した人で来店した)人数</th>
								<th width="10%">リピート率2<br>再来店人数/新規登録人数2*100</th>
								<th width="10%">お知らせ配信数2</th>
								<th width="5%">新規登録人数3(先月/先々月/３ヶ月前)</th>
								<th width="8%">再来店人数(のべ)3<br>(先月/先々月/3ヶ月前に登録した人で来店した)人数</th>
								<th width="10%">リピート率3<br>再来店人数/新規登録人数3*100</th>
								<th width="10%">お知らせ配信数3</th>
								<th width="10%">総お知らせ配信数</th>
								<th width="10%">プレゼント発行数</th>
						</tr>
						<?php
						foreach ($new_user_list_by_month as $value) {
						?>
						<tr>
								<?php
									$target_month = $value['add_month'];
									$total_num = $value['subtotal'];
									$target_num1 = $value['target_num1'];
									$target_num2 = $value['target_num2'];
									$target_num3 = $value['target_num3'];
									$repeat_num1 = $value['repeat_num1'];
									$repeat_num2 = $value['repeat_num2'];
									$repeat_num3 = $value['repeat_num3'];
									$repeat_percent1 = $value['repeat_percent1'];
									$repeat_percent2 = $value['repeat_percent2'];
									$repeat_percent3 = $value['repeat_percent3'];
									$notice_num = isset($value['notice_num']) ? $value['notice_num'] : "0";
									$notice_num1 = isset($value['notice_num1']) ? $value['notice_num1'] : "0";
									$notice_num2 = isset($value['notice_num2']) ? $value['notice_num2'] : "0";
									$notice_num3 = isset($value['notice_num3']) ? $value['notice_num3'] : "0";
									$present_num = isset($value['present_num']) ? $value['present_num'] : "0";
								
								?>
								<td align="center"><?php echo $target_month; ?></td>
								<td align="center"><?php echo $total_num; ?></td>
 								<td align="center" bgcolor="#F0F7FF"><?php echo $target_num1; ?></td>
 								<td align="center" bgcolor="#F0F7FF"><?php echo $repeat_num1; ?></td>
								<td align="center" bgcolor="#F0F7FF"><?php echo $repeat_percent1; ?>%</td>
								<td align="center" bgcolor="#F0F7FF"><?php echo $notice_num1; ?> 回 <br><?php echo ceil($value['read_count1']/$value['notice_total_count1']*100) ?>%(<?php echo $value['read_count1']; ?>/<?php echo $value['notice_total_count1']; ?>) </td>
 								<td align="center"><?php echo $target_num2; ?></td>
 								<td align="center"><?php echo $repeat_num2; ?></td>
								<td align="center"><?php echo $repeat_percent2; ?>%</td>
								<td align="center"><?php echo $notice_num2; ?> 回 <br><?php echo ceil($value['read_count2']/$value['notice_total_count2']*100) ?>%(<?php echo $value['read_count2']; ?>/<?php echo $value['notice_total_count2']; ?>) </td>
 								<td align="center" bgcolor="#F0F7FF"><?php echo $target_num3; ?></td>
 								<td align="center" bgcolor="#F0F7FF"><?php echo $repeat_num3; ?></td>
								<td align="center" bgcolor="#F0F7FF"><?php echo $repeat_percent3; ?>%</td>
								<td align="center" bgcolor="#F0F7FF"><?php echo $notice_num3; ?> 回 <br><?php echo ceil($value['read_count3']/$value['notice_total_count3']*100) ?>%(<?php echo $value['read_count3']; ?>/<?php echo $value['notice_total_count3']; ?>) </td>
								<td align="center"><?php echo $notice_num; ?> 回 <br><?php echo ceil($value['read_count']/$value['notice_total_count']*100) ?>%(<?php echo $value['read_count']; ?>/<?php echo $value['notice_total_count']; ?>) </td>
								<td align="center"><?php echo $present_num; ?></td>
						</tr>
						<?php
						}
						?>
				</table>
		</div>
</div>

<?php
require './pc/footer.php';
?>