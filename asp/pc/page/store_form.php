<?php
require './pc/header.php';
?>
<ul class="form" style="background:#FFF; padding:10px;">
	<form method="POST" action="./?p=shop" name="form2" >
		<li>■ 店舗情報追加</li>
		<li>
		<?php
		$display_branch_select = "";
		if(!empty($edit_id)){
			$store_name = "";
			$store_address = "";
			$tel_value = "";
			$phone_number = "";
			$store_rest = "";
			$store_prelusion = "";
			$hp_url = "";
			$map_url = "";
			$business_start = "";
			$business_end = "";
			$business_hours = "";
			$store_info_design = "";
			//編集画面の時、支店選択を非表示にする。
			$display_branch_select = 'style="display:none"';
			
			$db = db_connect();
			$where = " id = ".$edit_id;
			$shop_date = shop_select($db , $where);
			db_close( $db );
			foreach( $shop_date as $key => $shop_info) {
				$store_name = $shop_info['store_name'];
				$store_address = $shop_info['store_address'];
				$phone_number = $shop_info['phone_number'];
				$store_rest = $shop_info['store_rest'];
				$store_prelusion = $shop_info['store_prelusion'];
				$hp_url = $shop_info['hp_url'];
				$map_url = $shop_info['map_url'];
				$business_hours = $shop_info['business_hours'];
				$store_info_design = $shop_info['store_info_design'];
			}
		}
		?>
			<table width="65%"  border="0" cellpadding="5" cellspacing="1">
				<tr <?php echo $display_branch_select;?>>
					<td>支店選択：</td>
						<td>
							<select name="branch">
								<option value="0">全て</option>
								<?php foreach ($branch_list as $branch) { ?>
									<option value='<?php echo $branch['child_admin_id'];?>'><?php echo $branch['name'];?></option>
								<?php } ?>
							</select>
							<br>※ 支店機能を使用する場合、事前に支店情報を入力しておく。
							<br>　未使用の場合「全て」のままでOK.
						</td>
				</tr>
				<tr>
					<td>表示店舗名（支店）：</td>
					<td><input name="store_name" type="text" value="<?php echo $store_name;?>" id="store_name" size="50"/></td>
				</tr>
				<tr>
					<td>住所：</td>
					<td><textarea name="store_address" id="store_address"><?php echo $store_address;?></textarea></td>
				</tr>
				<tr>
					<td>営業時間：</td>
					<td><textarea name="business_hours" ><?php echo $business_hours;?></textarea></td>
				</tr>
				<tr>
					<td>TEL：</td>
					<td><input type="text" name="phone_number" value="<?php echo $phone_number;?>" class="input_box" size="16"></td>
				</tr>
				<tr>
					<td>定休日：</td>
					<td><textarea name="store_rest" class="input_box" size="50"><?php echo $store_rest;?></textarea></td>
				</tr>
				<tr>
					<td>ホームページ URL：</td>
					<td><input type="text" name="hp_url" value="<?php echo $hp_url;?>" class="input_box" size="50"></td>
				</tr>
				<tr>
					<td>マップ URL：</td>
					<td><input type="text" name="map_url" value="<?php echo $map_url;?>" class="input_box" size="50"></td>
				</tr>
				<tr>
					<td>紹介文：</td>
					<td><textarea name="store_prelusion" rows="6"  cols="60"><?php echo $store_prelusion;?></textarea></td>
				</tr>
				<tr>
					<td>店舗情報デザイン設定：</td>
					<td><textarea id="store_info_design" name="store_info_design" rows="6"  cols="60"><?php echo $store_info_design;?></textarea></td>
				</tr>
			</table><br />
		</li>
		<input name="edit_id" type="hidden" id="edit_id" value="<?php echo $edit_id ?>" />
		<div align="center">
		<li class="btm_body">
		<?php
		if(empty($edit_id)){
		?>
			<input name="setting" type="submit" class="btm" value="　追　加　" />
		<?php
		}else{
		?>
			<input name="update" type="submit" class="btm" value="　更　新　" />
		<?php 
		}
		?>
		<input name="check" type="submit" class="btm" value="　確　認　" />
		</li>
		</div>
		<br>
		<br>
	</form>
	<form method="POST" action="./?p=shop" name="form1" >
		<li>■ 店舗登録情報</li>
		<?php
		$shop_count = 1;
		foreach( $shop_date as $key => $value) {
			if($value['phone_number'] == ""){
				$store_tel_number = $value['tel'];
			} else {
				$store_tel_number = $value['phone_number'];
			}
			if($value['business_hours'] == ""){
				$store_business_hours = $value['business_start']."~".$value['business_end'];
			} else {
				$store_business_hours = $value['business_hours'];
			}
		?>
		<li>
			<table width="100%"  border="0" cellpadding="3" cellspacing="1" bgcolor="#431407">
				<tr align="center" bgcolor="#ffb202">
					<td width="35%" colspan="2"><div align="center"><font style="color:#FFFFFF;font-size:13px;">店舗名（支店）</font></div></td>
					<td width="30%" colspan="2"><div align="center"><font style="color:#FFFFFF;font-size:13px;">TEL</font></div></td>
					<td width="30%" colspan="2"><div align="center"><font style="color:#FFFFFF;font-size:13px;">営業時間</font></div></td>
					<td width="5%" bgcolor="#ffffff" rowspan="12"><a href="?p=shop&edit_id=<?php echo $value['id']?>">編集</a><br><br><a href="./?p=shop&delete_id=<?php echo $value['id']?>" onClick="return confirm('このアカウントを削除されますと、過去に配信したブログ記事が削除されますが宜しいでしょうか？')">削除 </a><br><br><a href="../public/shop/index.php?admin_id=<?php echo $value['admin_id']?>" target="_blank">確認</a></td>
				</tr>
				<tr align="center" bgcolor="#ffffff">
					<td colspan="2"><?php echo $value['store_name']?></td>
					<td colspan="2"><?php echo $store_tel_number?></td>
					<td colspan="2"><?php echo $store_business_hours?></td>
				</tr>
				<tr align="center" bgcolor="#ffb202">
					<td colspan="4"><div align="center"><font style="color:#FFFFFF;font-size:13px;">住所</font></div></td>
					<td colspan="2"><div align="center"><font style="color:#FFFFFF;font-size:13px;">定休日</font></div></td>
				</tr>
				<tr align="center" bgcolor="#ffffff">
					<td colspan="4"><?php echo $value['store_address']?></td>
					<td colspan="2"><?php echo $value['store_rest']?></td>
				</tr>
				<tr align="center" bgcolor="#ffb202">
					<td colspan="3"><div align="center"><font style="color:#FFFFFF;font-size:13px;">ホームページ URL</font></div></td>
					<td colspan="3"><div align="center"><font style="color:#FFFFFF;font-size:13px;">マップ URL</font></div></td>
				</tr>
				<tr align="center" bgcolor="#ffffff">
					<td colspan="3"><?php echo $value['hp_url']?></td>
					<td colspan="3"><?php echo $value['map_url']?></td>
				</tr>
				<tr align="center" bgcolor="#ffb202">
					<td colspan="6"><div align="center"><font style="color:#FFFFFF;font-size:13px;">紹介文</font></div></td>
				</tr>
				<tr align="center" bgcolor="#ffffff">
					<td colspan="6"><?php echo nl2br(htmlspecialchars($value['store_prelusion']));?></td>
				</tr>
				<tr align="center" bgcolor="#ffb202">
					<td colspan="6"><div align="center"><font style="color:#FFFFFF;font-size:13px;">店舗情報デザイン</font></div></td>
				</tr>
				<tr align="center" bgcolor="#ffffff">
					<td colspan="6"><div align="left"><?php echo nl2br(htmlspecialchars($value['store_info_design']));?></div></td>
				</tr>
			</table>
			<input type="hidden" name="id_<?php echo $shop_count;?>" value="<?php echo $value['id']?>">
		</li>
		<?php
			$shop_count++;
		}
		?>
		<input type="hidden" name="shop_count" value="<?php echo $shop_count;?>">
	</form>
</ul>
<?php
require './pc/footer.php';
?>