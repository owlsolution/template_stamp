<?php
$now_day=date("Y-m-d");
require './pc/header.php';
?>
<div style="background:#FFF; padding:10px;">
<ul class="form">
	<li>
    <p style="padding:5px;">
    開催したスランプラリーの一覧
    </p>
		<table width="100%"  border="0" cellpadding="5" cellspacing="0">
			<tr align="center">
				<th width="20%"><div align="center">スタンプ名称</div></th>
				<th width="15%"><div align="center">開始日</div></th>
				<th width="15%"><div align="center">終了日</div></th>
				<th width="15%"><div align="center">タイプ</div></th>
				<th width="20%"><div align="center">総参加人数</div></th>
				<th width="15%"><div align="center"></div></th>
			</tr>
			<?php
			$db = db_connect();
			$where = "admin_id = ".ADMIN_ID;
			$rally_date = rally_select($db , $where);
			while ($rally = mysql_fetch_array($rally_date)){
			?>
			<tr align="center" bgcolor="#ffffff">
				<td><?php echo $rally['rally_name']?></td>
				<td><?php echo $rally['add_date']?></td>
				<?php
				if($rally['rally_expire_type'] == 1){
					$stamp_day = $rally['stamp_day'];
					$add_date = explode("-",$rally['add_date']);
					$end_date = date("Y-m-d", mktime(0, 0, 0, $add_date[1], $add_date[2]+$stamp_day, $add_date[0]));
					if($end_date > $now_day){
						$end_date_content = "実施中";
					} else {
						$end_date_content = $end_date;
					}
				} else if($rally['rally_expire_type'] == 2){
					$end_date = $rally['stamp_date'];
					if($end_date > $now_day){
						$end_date_content = "実施中";
					} else {
						$end_date_content = $end_date;
					}
				}
				?>
				<td><?php echo $end_date_content;?></td>
				<td>店舗型</td>
				<?php
				$rally_id = $rally['rally_id'];
				$where = "admin_id = ".ADMIN_ID." AND rally_id = ".$rally_id ;
				$rally_user_count_date = rally_user_count_select($db , $where);
				$rally_user_count = mysql_fetch_array($rally_user_count_date);
				?>
				<td><?php echo $rally_user_count['count(*)'];?>人</td>
				<td><a href="?p=summary&rally_id=<?php echo $rally_id;?>">編集</a></td>
			</tr>
			<?php
			}
			db_close( $db );
			?>
		</table>
	</li>
</ul>
</div>
<?php
require './pc/footer.php';
?>