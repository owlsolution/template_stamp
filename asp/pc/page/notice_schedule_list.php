<div class="">
		<h2> <img src="images/icon1.gif" width="16" height="16"> スケジュール配信一覧 </h2>
		<div class="container">
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr align="center">
							<th width="15%">設定日時</th>
							<th width="15%">設定タイプ</th>
							<th width="10%">対象店舗名</th>
							<th width="35%">タイトル</th>
							<th width="10%">状態</th>
							<th width="15%"></th>
					</tr>
					<?php
						$db = db_connect();
						// 支店機能 通常/オーナーはADMIN_ID ,支店権限の場合は支店のadmin_idで検索
						if ($_SESSION["branchFlag"] == OWNER) {
							$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, ADMIN_ID, OWNER);
						} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] == 0)){
							// 全支店管理アカウントで全支店管理アカウントモードの場合
							$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, ADMIN_ID, BRANCHES_OWNER);
						} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] != 0)){
							// 全支店管理アカウントで全支店アカウントを選択時は選択した支店管理アカウントに限定した機能
							$branch_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
							// 選択中の支店管理アカウントの配下のスタッフのadmin_idを取得する
							$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $branch_admin_id, BRANCH_MANAGER);
						} else if ($_SESSION["branchFlag"] == BRANCH_MANAGER) {
							// 支店管理アカウントでログインしている場合
							$branch_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
							// 支店管理アカウントにブラ下がるスタッフを取得
							$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $branch_admin_id, BRANCH_MANAGER);
						} else if (($_SESSION["branchFlag"] == ORG_MANAGER) && ($_SESSION["branchId"] != 0)){
							$branch_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
							// 選択中の支店管理アカウントの配下のスタッフのadmin_idを取得する
							$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $branch_admin_id, BRANCH_MANAGER);
						} else if ($_SESSION["branchFlag"] == ORG_MANAGER) {
							// 組織管理アカウントなので、組織IDを元に組織にぶら下がる支店のadmin_idを取ってくる
							$org = $adminModel->get_organization_by_id($db, $_SESSION["orgId"]);
							// 組織IDからadmin_idを取得
							// admin_idとタイプを渡して、ぶら下がるadmin_idのリストを返却するメソッドを呼出す
							$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $org['admin_id'], ORG_MANAGER);
						} else if ($_SESSION["branchFlag"] == STAFF) {
							// スタッフのadmin_idを設定
							$staff = $adminModel->get_staff_by_id($db, $_SESSION["staffId"]);
							$admin_id_list[] = $staff['admin_id'];
						}
						$admin_id_list = array_filter($admin_id_list, "strlen");
						$send_admin_id = implode(',', $admin_id_list);
						
						//配信した支店名の表示
						$branch_list = branch_select_by_owner($db, ADMIN_ID);

						// 全スタッフを取得
						$rally_id = $adminModel->get_rally_id($db, ADMIN_ID);
						$staff_data_list = $adminModel->get_staff_by_rally_id($db, $rally_id);

						// 対象の配信スケジュールのリストを取得する。
						$where = "rally_id = '".$rally['rally_id']."' AND admin_id IN( ".$send_admin_id.") ORDER BY id ASC";
//						error_log("notice_schedule_select:".$where);
						$notice_schedule_data = notice_schedule_select($db , $where);
						while ($schedule = mysql_fetch_array($notice_schedule_data)){
						?>
						<tr>
							<?php
								// 1行分のレンダリング
								// 設定日時
								$add_date_str = explode( ":", $schedule['modified']);
								$add_date_date = $add_date_str[0].":".$add_date_str[1];
								$add_date = str_replace("-", "/", $add_date_date);
								
								// 設定したスケジュールタイプ
								$schedule_type = $schedule['schedule_type'];
								$typeinfo = $noticeComp->schedule_list[$schedule_type];
								$schedule_type_name = $typeinfo['name'];
								
								// 設定内容

								// 対象店舗名
								// 配信した支店名の表示
								$branch_name = "";
								if(!empty($branch_list)){
									// 支店機能なので支店admin_idと照合する
									foreach($branch_list as $branch){
										if($schedule['admin_id'] == $branch['child_admin_id']){
											$branch_name = $branch['name'];
											break;
										}
									}
									
									if(empty($branch_name)) {
										// 支店のadmin_idと一致していないので
										// スタッフのadmin_idと照合する
										foreach($staff_data_list as $result_staff_data){
											if ($schedule['admin_id'] == $result_staff_data['admin_id']){
												if($result_staff_data['branch_id'] == 0){
													// スタッフが本部に所属している
													break;
												}
												foreach($branch_list as $branch){
													if($branch['id'] == $result_staff_data['branch_id']){
														$branch_name = $branch['name'];
													}
												}
											}
										}
									}
								}
								if(empty($branch_name)) {
									$branch_name = "本部";
									if (isset($org)) {
										// 組織情報がある場合、ログインしているのが組織組織管理アカウントなので、組織名を設定
										$branch_name = $org['name'];
									}
								}
								
								// タイトル
								 
								// 状態 [実施中/停止]
								$status = "実施中";
								if($schedule['status'] == '1') {
									$status = "停止中";
								}
								 
								?>
								<td align="center"><?php echo $add_date; ?></td>
 								<td align="center"><?php echo $schedule_type_name; ?></td>
								<td align="center"><?php echo $branch_name; ?></td>
								<td><?php echo $schedule['title']; ?></td>
								<td align="center"><?php echo $status;?></td>
								<td align="center"><a style="color:#1122CC;" href="?p=notice&tab_index=3&edit_schedule_id=<?php echo $schedule['id'];?>">編集</a> ｜ <a style="color:#1122CC;" href="?p=notice&tab_index=3&dele_schedule_id=<?php echo $schedule['id'];?>" onClick="return confirm('削除しますか?')">削除</a></td>
						</tr>
						<?php
						}
						db_close($db);
						?>
				</table>
		</div>
</div>
