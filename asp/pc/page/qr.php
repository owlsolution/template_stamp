<?php
require './pc/header.php';
?>
<div class="box">
	<h2> <img src="images/icon4.gif" width="16" height="16"> 各種QRコード発行 </h2>
	<div class="container">
		<ul>
			ここで、QRコードを発行できます。
		</ul>
		<div class="f_box">
			<form method="POST" action="./?p=qr" name="form2" target="_blank">
				<div class="container1">
					<h3> QRコードの内容 </h3>
					<ul class="clearfix">
						<p style="padding-left:10px; margin-bottom:10px;">　
							<select name="qr_date">
									<option value="new">新規用QRコード</option>
									<option value="stamp_1">スタンプ1個用QRコード</option>
									<option value="stamp_2">スタンプ2個用QRコード</option>
									<option value="stamp_3">スタンプ3個用QRコード</option>
									<option value="stamp_4">スタンプ4個用QRコード</option>
									<option value="stamp_5">スタンプ5個用QRコード</option>
									<option value="stamp_6">スタンプ6個用QRコード</option>
									<option value="stamp_7">スタンプ7個用QRコード</option>
									<option value="stamp_8">スタンプ8個用QRコード</option>
									<option value="stamp_9">スタンプ9個用QRコード</option>
									<option value="stamp_10">スタンプ10個用QRコード</option>
							</select>
						</p>
					</ul>
				</div>
				<div class="container1">
					<h3> QRコードの数 </h3>
					<ul class="clearfix">
						<li style="text-align:center;">
							<p style="margin-bottom:5px;"><input type="radio" name="size_qr" value="extra_large" checked> 特大</p>
							<img src="images/qr1.gif" width="140px">
						</li>
						<li style="text-align:center;">
							<p style="margin-bottom:5px;"><input type="radio" name="size_qr" value="large"> 大</p>
							<img src="images/qr2.gif" width="140px">
						</li>
						<li style="text-align:center;">
							<p style="margin-bottom:5px;"><input type="radio" name="size_qr" value="medium"> 中</p>
							<img src="images/qr4.gif" width="140px">
						</li>
						<li style="text-align:center;">
							<p style="margin-bottom:5px;"><input type="radio" name="size_qr" value="small"> 小</p>
							<img src="images/qr3.gif" width="140px">
						</li>
					</ul>
				</div>
				<div class="btn">
					<input type="submit" name="pdf_dl" value="PDF DL">
					<?php
					//<input type="submit" value="ブラウザ">
					?>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
require './pc/footer.php';
?>