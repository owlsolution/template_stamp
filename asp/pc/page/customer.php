<?php
require './pc/header.php';
$now_year = date('Y');
//現在日
$now_date = date('Y-m-d');
$rally_name_date = "全スタンプ";
?>
<div class="box">
	<h2> <img src="images/icon7.gif" width="16" height="16"> 顧客管理一覧 </h2>
	<div class="container">
		<ul>
			スタンプラリー参加者の情報が見れます。
		</ul>
		<div class="f_box">
			<form method="POST" action="./?p=customer" name="form2">
				<div class="container2">
					<h3> 絞り込み検索 </h3>
					<ul class="clearfix">
						<li class="clearfix">
							<p class="titel">・対象スタンプ</p>
							<p class="text"><?php echo rally_name_get(ADMIN_ID);?></p>
						</li>
						<li class="clearfix even">
							<p class="titel">・日付</p>
							<p class="text">
								<select name="conditions_select" id="select">
									<option value="0" <?php if(0 == $conditions_select){ echo "selected"; }?>>新規登録日</option>
									<option value="1" <?php if(1 == $conditions_select){ echo "selected"; }?>>最終ｽﾀﾝﾌﾟ日</option>
								</select>
								<select name="years_select" id="select">
								<?php
								for($i = 2013 ; $i <= $now_year ; $i++){
								?>
									<option value="<?php echo $i;?>" <?php if($i == $years_select){ echo "selected"; }?>><?php echo $i;?></option>
								<?php
								}
								?>
								</select>
								年　
								<select name="month_select" id="select">
								<?php
								for($i = 1; $i <= 12 ; $i++){
								?>
									<option value="<?php echo $i;?>" <?php if($i == $month_select){ echo "selected"; }?>><?php echo $i;?></option>
								<?php
								}
								?>
								</select>
								月　
								<select name="day_select" id="select">
								<?php
								for($i = 1; $i <= 31 ; $i++){
								?>
									<option value="<?php echo $i;?>" <?php if($i == $day_select){ echo "selected"; }?>><?php echo $i;?></option>
								<?php
								}
								?>
								</select>
								日　〜　
								<select name="end_years_select" id="select">
								<?php
								for($i = 2013; $i <= $now_year; $i++){
								?>
									<option value="<?php echo $i;?>" <?php if($i == $end_years_select){ echo "selected"; }?>><?php echo $i;?></option>
								<?php
								}
								?>
								</select>
								年　
								<select name="end_month_select" id="select">
								<?php
								for($i = 1; $i <= 12 ; $i++){
								?>
									<option value="<?php echo $i;?>" <?php if($i == $end_month_select){ echo "selected"; }?>><?php echo $i;?></option>
								<?php
								}
								?>
								</select>
								月　
								<select name="end_day_select" id="select">
								<?php
								for($i = 1; $i <= 31 ; $i++){
								?>
									<option value="<?php echo $i;?>" <?php if($i == $end_day_select){ echo "selected"; }?>><?php echo $i;?></option>
								<?php
								}
								?>
								</select>
								日　
							</p>
						</li>
						<li class="clearfix">
							<p class="titel">・顧客ID検索 </p>
							<input type='text' id='search_user_id' name='search_user_id' size='18' value='<?php echo stripslashes($search_user_id); ?>'>
							※[顧客ID]で検索します。
						</li>
						<li class="clearfix even">
							<p class="titel">・キーワード検索 </p>
							<input type='text' id='search_keyword' name='search_keyword' size='36' value='<?php echo stripslashes($keyword); ?>'>
							※[顧客名][カルテ情報]をキーワード検索します。
						</li>
					</ul>
				</div>
				<div class="btn">
					<input name="search" type="submit" class="btm" value="検索" />　　<input name="reset" type="submit" class="btm" value="リセット" />
				</div>
			</form>
		</div>
	</div>
</div>
<div class="box">
	<h2> <img src="images/icon7.gif" width="16" height="16"> 検索結果 <?php if(empty($_SESSION['search_condition'])){ echo '(全表示)';}?></h2>
	<div class="container">
		<!--ページネーション-->
		<form method="GET" action="./?p=customer" name="user_sort">
			・ページ最大表示行数：
			<select name="search_user_num" id="search_user_num">
				<option id="search_num" value="100" <?php if(100 == $per_page){ echo "selected"; }?>>100</option>
				<option id="search_num" value="500" <?php if(500 == $per_page){ echo "selected"; }?>>500</option>
				<option id="search_num" value="1000"<?php if(1000 == $per_page){ echo "selected"; }?>>1000</option>
			</select>
		<center>
			<?php
				echo paging($page, $total_page , $sort_num , $change_order , $per_page);
			?>
			<p class="page_message">　全<strong><?php echo $total_count;?></strong>件中 <strong><?php echo $record_index + 1;?>~<?php echo $record_index + count($larry_user_array);?></strong>件目を表示</p>
		</center>
		<div class="box">
			<table id="user_list_table" width="100%" border="0" bordercolor="#ff0000" cellpadding="5" cellspacing="0" >
				<thead>
					<tr align="center">
						<th width="4%">No</th>
						<th width="8%">顧客ID<br><a href="./?p=customer&sort_num=user_id&change_order=DESC&search_user_num=<?php echo $per_page;?>" name="DESC" id="user_id" class="sort">▼</a><a href="./?p=customer&sort_num=user_id&change_order=ASC&search_user_num=<?php echo $per_page;?>" name="ASC" id="user_id" class="sort">▲</a></th>
						<th width="11%">顧客名<br><a href="./?p=customer&sort_num=user_name&change_order=DESC&search_user_num=<?php echo $per_page;?>" name="DESC" id="user_name" class="sort">▼</a><a href="./?p=customer&sort_num=user_name&change_order=ASC&search_user_num=<?php echo $per_page;?>" name="ASC" id="user_name" class="sort">▲</a></th>
						<th width="12%">新規登録日<br><a href="./?p=customer&sort_num=add_date2&change_order=DESC&search_user_num=<?php echo $per_page;?>" name="DESC" id="add_date2" class="sort">▼</a><a href="./?p=customer&sort_num=add_date2&change_order=ASC&search_user_num=<?php echo $per_page;?>" name="ASC" id="add_date2" class="sort">▲</a></th>
						<th width="12%">最終ｽﾀﾝﾌﾟ日<br><a href="./?p=customer&sort_num=last_stamp_date&change_order=DESC&search_user_num=<?php echo $per_page;?>" name="DESC" id="last_stamp_date" class="sort">▼</a><a href="./?p=customer&sort_num=last_stamp_date&change_order=ASC&search_user_num=<?php echo $per_page;?>" name="ASC" id="last_stamp_date" class="sort">▲</a></th>
						<th width="10%">累計ｽﾀﾝﾌﾟ数<br><a href="./?p=customer&sort_num=total_stamp_num&change_order=DESC&search_user_num=<?php echo $per_page;?>" name="DESC" id="total_stamp_num" class="sort">▼</a><a href="./?p=customer&sort_num=total_stamp_num&change_order=ASC&search_user_num=<?php echo $per_page;?>" name="ASC" id="total_stamp_num" class="sort">▲</a></th>
						<th width="10%">現在ｽﾀﾝﾌﾟ数<br><a href="./?p=customer&sort_num=stamp_num&change_order=DESC&search_user_num=<?php echo $per_page;?>" name="DESC" id="stamp_num" class="sort">▼</a><a href="./?p=customer&sort_num=stamp_num&change_order=ASC&search_user_num=<?php echo $per_page;?>" name="ASC" id="stamp_num" class="sort">▲</a></th>
						<th width="10%">累計クーポン数<br><a href="./?p=customer&sort_num=all_co&change_order=DESC&search_user_num=<?php echo $per_page;?>" name="DESC" id="stamp_num" class="sort">▼</a><a href="./?p=customer&sort_num=all_co&change_order=ASC&search_user_num=<?php echo $per_page;?>" name="ASC" id="stamp_num" class="sort">▲</a></th>
						<th width="10%">現在クーポン数<br><a href="./?p=customer&sort_num=now_co&change_order=DESC&search_user_num=<?php echo $per_page;?>" name="DESC" id="stamp_num" class="sort">▼</a><a href="./?p=customer&sort_num=now_co&change_order=ASC&search_user_num=<?php echo $per_page;?>" name="ASC" id="stamp_num" class="sort">▲</a></th>
						<th width="12%">詳細情報<br></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$index = 1;
					foreach( $larry_user_array as $key => $value ) {
						// 新規登録日を表示する。
						$add_date = Util::get_rally_of_day($value['add_date2'] , $value['rally_id']);
						// 最終来店日を表示する。
						$last_stamp_date = Util::get_rally_of_day($value['last_stamp_date'] , $value['rally_id']);
						$last_login_date = Util::get_rally_of_day($value['last_login_date'] , $value['rally_id']);

						/**
						 *  日付の比較をして<tr>タグの背景色を変える   $color_type
						 * ユーザーの最終ログイン日から現在日までの差分を取得する。   $day_diff
						 */
						//$value['last_login_date']が空の場合、背景色を白(#FFFFFF)に変更、現在日に変換する。
						if($last_login_date[0]."-".$last_login_date[1]."-".$last_login_date[2] == "-0001-11") {
							$color_type = getUserColor($now_date);
							$day_diff = day_diff($now_date , $now_date);
						} else {
							$color_type = getUserColor($last_login_date[0]."-".$last_login_date[1]."-".$last_login_date[2]);
							$day_diff = day_diff($last_login_date[0]."-".$last_login_date[1]."-".$last_login_date[2] , $now_date);
						}

						/**
						 * 吹き出しにユーザーネームを表示する。
						 * 無名の場合はユーザーIDを表示する。
						 */
						$user_name = "";
						if(!empty($value['user_name'])){
							$user_name = $value['user_name'];
						} else {
							$user_name = USER_ID.sprintf("%05d", $value['user_id']);
						}
					?>
					<tr align="center" bgcolor="<?php echo $color_type;?>" class="<?php echo $key;?>">
						<td><?php echo ($record_index + $index);$index++;?></td>
						<td><?php echo USER_ID.sprintf("%05d", $value['user_id']);?><div class="user_<?php echo $key;?>" id="arrow_box" ><?php echo $user_name."様は".$day_diff."日間ログインしていません。";?></div></td>
						<td><?php echo empty($value['user_name']) ? "-" : $value['user_name'];?></div></td>
						<td><?php echo $add_date[0]."/".$add_date[1]."/".$add_date[2];?></span></td>
						<?php
						//最終スタンプ日
						$final_stamp_date = "";
						if($value['last_stamp_date'] == "0000-00-00 00:00:00"){
							$final_stamp_date = "-";
						} else {
							$final_stamp_date = $last_stamp_date[0]."/".$last_stamp_date[1]."/".$last_stamp_date[2];
						}
						?>
						<td><?php echo $final_stamp_date;?></td>
						<td><?php echo $value['total_stamp_num'];?></td>
						<td><?php echo $value['stamp_num'];?></td>
							<?php 	$db = db_connect(); ?>
						<td><?php echo all_coupon($db, $value['user_id'] , $value['rally_id']);?></td>
						<td><?php echo now_coupon($db, $value['user_id'] , $value['rally_id']); ?></td>
							<?php db_close($db); ?>
						<td bgcolor="FFFFFF"><a href="?p=customer&user_id=<?php echo $value['user_id']?>&rally_id=<?php echo $value['rally_id'];?>">詳細</a> | <a href="./?p=customer&rally_id=<?php echo $value['rally_id'];?>&delete=<?php echo $value['user_id'];?>&sort_num=<?php echo $sort_num;?>&change_order=<?php echo $change_order;?>&search_user_num=<?php echo $per_page;?>&page=<?php echo $page;?>" onClick="return confirm('削除しますか?')">削除</a> | <a href="?p=customer&user_id=<?php echo $value['user_id']?>&rally_id=<?php echo $value['rally_id'];?>&edit=1">編集<a></td>
					</tr>
					<script>
						/**
						 * マウスカーソルを顧客一覧に合わせると
						 * その人が何日ログインしていないか表示をさせる。
						 */
						$(function(){
							var key = "<?php echo $key;?>";
							var value = $('#user_list_table .user_'+key);
							$('#user_list_table .'+key).hover(
							// hover
							function(){
								value.show();
							},
							// hover_out
							function(){
								value.hide();
							});
							
						})
					</script>
					<?php
					}
					?>
				</tbody>
				<?php if(count($larry_user_array) > 0) { ?>
					<tr class="tablefooter" align="center">
							<td class="tablefooter_head"></td>
							<td class="tablefooter_head"></td>
							<td class="tablefooter_head"></td>
							<td class="tablefooter_head"></td>
							<td class="tablefooter_head">合計</td>
							<td class="tablefooter_value"><?php echo $sum_total_stamp_num;?></td>
							<td class="tablefooter_value"><?php echo $sum_stamp_num;?></td>
							<td class="tablefooter_value"><?php echo $sum_total_coupon_num;?></td>
							<td class="tablefooter_value"><?php echo $sum_coupon_num; ?></td>
							<td>-</td>
					</tr>
				<?php
				}
				?>
			</table>
			
		</div>
	</form>
		<!--ページネーション-->
		<center>
			<?php
			echo paging($page, $total_page , $sort_num , $change_order , $per_page);
				?>
		</center>
	</div>
</div>
<script>
$(function($) {
  $('#search_user_num').change(function() {
	var search_num = $(this).val();
	var get_search_num = "./?p=customer&search_user_num="+search_num;
	location.href = location.href.split('?')[0]+get_search_num;
  });
});
</script>
<?php
require './pc/footer.php';
?>