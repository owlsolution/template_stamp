<?php
require './pc/header.php';
?>
<div style="background:#FFF; padding:10px;">
<ul class="form">
	<form method="POST" action="<?php echo $url;?>" name="form2">
		<li>●スタンプ名</li>
		<li><input name="rally_name" type="text" value="<?php echo $rally_name;?>" id="rally_name" size="30" /></li><br />
		<li>●スタンプ有効期限</li>
		<li>
			　ユーザーがスタンプを付与されてから <input name="stamp_day" type="text" value="<?php echo $stamp_day;?>" id="stamp_day" size="5" /> 日間　
		</li><br />
		<li>●クーポン有効期限</li>
		<li>
			　ユーザーがクーポンを取得してから <input name="goal_day" type="text" value="<?php echo $goal_day;?>" id="goal_day" size="5" /> 日間　
		</li><br />
		<li>●全スタンプ数</li>
		<li><input name="stamp_max" type="text" value="<?php echo $stamp_max;?>" id="stamp_max" size="10" /> 個</li>
		<li>●スタンプ付与制限（１日１回のみ）</li>
		<li>
			<input type="radio" name="one_times_a_day" value="1" <?php if($one_times_a_day == 1 || $one_times_a_day == ""){?> checked <?php }?>> ON　
			<input type="radio" name="one_times_a_day" value="2" <?php if($one_times_a_day == 2){?> checked <?php }?>> OFF
		</li><br />
		<li>●新規スタンプ数</li>
		<li>
			<select name="new_stamp_num" id="new_stamp_num">
				<?php
				for($s=0;$s<6;$s++){
				?>
				<option value="<?php echo $s;?>" <?php if($s == $new_stamp_num){?> selected <?php }?>><?php echo $s;?></option>
				<?php
				}
				?>
			</select>個
		</li><br />
		<li>●ユーザがプロフィールを登録しているかチェックをする</li>
		<li>
			<input type="radio" name="user_profile_force_check" value="1" <?php if($user_profile_force_check == 1 || $user_profile_force_check == ""){?> checked <?php }?>onClick="profile_force_flag('1')" > はい　
			<input type="radio" name="user_profile_force_check" value="2" <?php if($user_profile_force_check == 2){?> checked <?php }?> onClick="profile_force_flag('2')"> いいえ
		</li>
		<br />
		<li class="profile_force_area">・プロフィール登録要求度の設定</li>
		<li class="profile_force_area">
			【・ユーザがプロフィールを登録しているかチェックをする】設定が「はい」の場合、以下の設定が可能です。<br>
			<ul class="profile_force_area" style="font-size: 12px;">*プロフィール未入力の場合</ul>
			<input type="radio" class="profile_force" name="profile_force" value="2" <?php if($profile_force == 2){?> checked <?php }?> <?php echo $disabled;?>> アプリ起動時にプロフィール入力をポップアップで促す。<br>
			<input type="radio" class="profile_force" name="profile_force" value="3" <?php if($profile_force == 3){?> checked <?php }?> <?php echo $disabled;?>> クーポンを取得する際に、プロフィール入力をポップアップで促す。(未入力の場合クーポン取得不可)<br>
			<input type="radio" class="profile_force" name="profile_force" value="5" <?php if($profile_force == 5){?> checked <?php }?> <?php echo $disabled;?>> アプリ起動時とユーザーがクーポンを取得する際に、プロフィール入力をポップアップで促す。<br>
		</li><br />

		<br />
		<div align="center">
			<li class="btm_body">
				<input name="check" type="submit" class="btm" value="確認" />
			</li>
		</div>
	</form>
</ul>
</div>

<script>
	$(function(){
		profile_force_flag();
	});
	function profile_force_flag(flag) {
		radioCheck = $("input[name='user_profile_force_check']:checked").val();
		if(typeof flag == "undefined" && radioCheck == 2){
			$('.profile_force').attr("disabled", "disabled");
			$('.profile_force').removeAttr("checked", "checked");
			$('.profile_force_area').css("color", "silver");
		}else{
			if(flag == "1"){
				$('.profile_force').removeAttr("disabled", "disabled");
				$('.profile_force_area').css("color", "");
			}else if (flag == "2"){
				$('.profile_force').attr("disabled", "disabled");
				$('.profile_force').removeAttr("checked", "checked");
				$('.profile_force_area').css("color", "silver");
			}
		}
	}
</script>

<?php
require './pc/footer.php';
?>