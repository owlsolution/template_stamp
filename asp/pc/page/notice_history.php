<div class="box">
        <h2> <img src="images/icon1.gif" width="16" height="16"> 配信履歴 </h2>
        <div class="container">
 				<table width="25%" border="0" cellpadding="5" cellspacing="0">
					<tr align="center">
						<th width="15%">お知らせ配信</th>
						<th width="15%">ブログ配信</th>
						<th width="15%">チラシ配信</th>
						<th width="15%">ｽｹｼﾞｭｰﾙ<br>配信</th>
					</tr>
					<tr>
						<td align="center"><img src='./../../sp_images/a03.png' width='15' height='15'></td>
						<td align="center"><img src='./../../sp_images/a01.png' width='15' height='15'></td>
						<td align="center"><img src='./../../sp_images/a02.png' width='15' height='15'></td>
						<td align="center"><img src='./../../sp_images/a04.png' width='15' height='15'></td>
					</tr>
				</table>
				<br>
                <table width="100%" border="0" cellpadding="5" cellspacing="0">
                        <tr align="center">
                                <th width="12%">配信受付日</th>
								<th width="4%">配信タイプ</th>
								<th width="12%">配信元店舗名</th>
                                <th width="34%">タイトル</th>
                                <th width="9%">配信数</th>
                                <th width="9%">既読数</th>
                                <th width="6%">状態</th>
                                <th width="18%"></th>
                        </tr>
                        <?php
                        $db = db_connect();
                        $send_admin_id = ADMIN_ID;
                        if ($_SESSION["branchFlag"] == 2) {
                            // 支店でログインしている場合
                            $send_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
                        } else if ($_SESSION["branchFlag"] == 1) {
                            // オーナーの場合
                            if ($_SESSION["branchId"] != 0) {
                                $send_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
                            }
                            else {
                                // 支店で送信したお知らせも対象に入れる
                                $branch_ids = getChildAdminIds($db, ADMIN_ID);
                                foreach ($branch_ids as $value) {
                                    $send_admin_id .= ",".$value;
                                }
                            }
                        }
						//配信した支店名の表示
						$branch_list = branch_select_by_owner($db, ADMIN_ID);

						// 店員さんのadmin_idを追加
						$rally_date = rally_select($db , "admin_id = ".ADMIN_ID);
						$rally = mysql_fetch_array($rally_date);
						$where = "rally_id = '".$rally['rally_id']."' AND status = '1'";
						$staff_data = staff_select($db , $where);
						while ($staff = mysql_fetch_array($staff_data)){
							if($_SESSION["branchId"] == $staff['branch_id'] || $_SESSION["branchId"] == 0){
								$send_admin_id .= ",".$staff['admin_id'];
							}
							$staff_data_list[] = $staff;
						}

                        // PUSH送信状況を取得
                        $push_status = $noticeComp->get_push_status($send_admin_id);
                        
                        $where = "admin_id IN( ".$send_admin_id.") ORDER BY notice_id DESC";
                        $notice_date = notice_select($db , $where);
                        while ($notice = mysql_fetch_array($notice_date)){
                        ?>
                        <tr>
                                <?php
                                $where = "rally_id = ".$notice['rally_id'];
                                $rally_date = rally_select($db , $where);
                                $rally = mysql_fetch_array($rally_date);
                                $add_date_str = explode( ":", $notice['acceptance_datetime']);
                                $add_date_date = $add_date_str[0].":".$add_date_str[1];
                                $add_date_replace = str_replace("-", "/", $add_date_date);
                                
                                // ステータスチェック
                                // 送信中/送信済/予約
                                $status = "送信済";
                                foreach ($push_status as $value) {
                                    if ($value['notice_id'] == $notice['notice_id']) {
                                        // 合致した場合ステータスを参照
                                        if (($value['status'] == '0') || ($value['status'] == '1')) {
                                            $status = "送信中";
                                        } else if ($value['status'] == '4') {
                                            $status = "予約";
                                        }
                                    }
                                }
								//配信した支店名の表示
								if(empty($branch_list)){
									$branch_name = "本部";
								} else {
									foreach($branch_list as $branch){
										if ($notice['admin_id'] == $branch['parent_admin_id']){
											$branch_name = "本部";
										} else if($notice['admin_id'] == $branch['child_admin_id']){
											$branch_name = $branch['name'];
										}
									}
								}
								foreach($staff_data_list as $result_staff_data){
									if ($notice['admin_id'] == $result_staff_data['admin_id']){
										foreach($branch_list as $branch){
											if($branch['id'] == $result_staff_data['branch_id']){
												$branch_name = $branch['name'];
											} else if($result_staff_data['branch_id'] == 0){
												$branch_name = "本部";
											}
										}
									}
								}
								//配信タイプ（0 = お知らせ、1 = チラシ、2 = ブログ、 3 = 個人宛）を表示する
								 if($notice['notice_type'] == 0 || $notice['notice_type'] == 3){
								 	$notice_type_image = "<img src='./../../sp_images/a03.png' width='15' height='15'>";
								 } else if($notice['notice_type'] == 1){
								 	$notice_type_image = "<img src='./../../sp_images/a02.png' width='15' height='15'>";
								 } else if($notice['notice_type'] == 2){
								 	$notice_type_image = "<img src='./../../sp_images/a01.png' width='15' height='15'>";
								 }
                                ?>
                                <td align="center"><?php echo $add_date_replace; ?></td>
 								<td align="center"><?php echo $notice_type_image; ?></td>
								<td align="center"><?php echo $branch_name; ?></td>
                                <td><?php echo $notice['notice_title']; ?></td>
                                <td align="center"><?php echo $notice['delivery_num'];?>/<?php echo $notice['total_user_count'];?></td>
                                <td align="center"><?php echo $notice['already_read_num'];?>/<?php echo $notice['app_delivery_num'];?></td>
                                <td align="center"><?php echo $status;?></td>
                                <td align="center"><a href="?p=notice&check_id=<?php echo $notice['notice_id'];?>">確認</a> ｜ <a href="?p=notice&edit_id=<?php echo $notice['notice_id'];?>">編集</a> ｜ <a href="?p=notice&dele_id=<?php echo $notice['notice_id'];?>" onClick="return confirm('削除しますか?')">削除</a></td>
                        </tr>
                        <?php
                        }
                        db_close($db);
                        ?>
                </table>
        </div>
</div>
