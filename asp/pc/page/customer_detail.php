<?php
// 顧客情報詳細表示
require './pc/header.php';
?>

<!-- include libries(jQuery, bootstrap, fontawesome) -->
<link href="./css/bootstrap.min.css" rel="stylesheet"> 
<script src="./js/bootstrap.min.js"></script> 
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">

<!-- include summernote css/js-->
<link rel="stylesheet" type="text/css" href="./js/dist/summernote.css" />
<script src="./js/dist/summernote.min.js"></script>

<!-- jQuery UI -->
<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/start/jquery-ui.css" rel="stylesheet">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<div class="box">    
	<h2> <img src="images/icon7.gif" width="16" height="16"> 顧客情報 </h2>
	<ul style="float: right;">
		<span style="margin-top: -10px;"><button class="btn" name="to_gift_btn" value="to_gift_btn" onclick="location.href='./?p=gift&to_user_id=<?php echo $user_id; ?>'"> ﾌﾟﾚｾﾞﾝﾄｸｰﾎﾟﾝ発行 </button></span>&nbsp;&nbsp;
		<span style="margin-top: -10px;"><button class="btn" name="to_notice_btn" value="to_notice_btn" onclick="location.href='./?p=notice&to_user_id=<?php echo $user_id; ?>'"> 個人宛にお知らせ配信 </button></span>
		<span style="margin-top: -10px;"><button id="stamp_btn" class="btn" name="stamp_btn" value="stamp_btn" > 1ｽﾀﾝﾌﾟ付与 </button></span>
	</ul>
	<div id="show_dialog"></div>
	<div class="container">
		
		<div class="clearfix" style="margin-bottom:10px;">
			
			<img src="images/person.gif" width="150" height="180" align="left"/>
			
			<div style="margin-left:190px;">
                            <p style="margin-bottom:10px; border-bottom:#CCC 1px dotted; padding-bottom:5px;">顧客ID/ <font style="color:#06F;font-size:13px;"><?php echo USER_ID.sprintf("%05d", $user_id);?></font></p>
							<p style="font-size:18px; font-weight:bold; margin-bottom:10px;">
					<?php echo $detail_user['user_name'];?><strong style="font-size:14px; color:#999;">様</strong>
				</p>
				<table width="100%"  border="0" cellpadding="0" cellspacing="0">
					<tr align="center">
						<th width="15%">性別</th>
						<td width="10%" align="center" valign="middle"><?php if($detail_user['sex'] == 1){ echo "男";} else if($detail_user['sex'] == 2){ echo "女";}?></td>
						<th width="15%">生年月日</th>
						<?php $birth_date = str_replace("-", "/", $detail_user['birth_date']);?>
						<td width="10%" align="center" valign="middle"><?php if($detail_user['birth_date'] != "" && $detail_user['birth_date'] != "0000-00-00"){ echo $birth_date;}?></td>
						<th width="15%">地域</th>
						<td width="10%" align="center" valign="middle"><?php echo $detail_user['region'];?></td>
					</tr>
					<tr align="center" bgcolor="#ffffff">
						<th  >新規登録日</th>
						<?php $add_date = Util::get_rally_of_day($detail_user['add_date2'] , $detail_user['rally_id']);?>
						<td  align="center" valign="middle"><?php echo $add_date[0]."/".$add_date[1]."/".$add_date[2];?></td>
						<th>累計ｽﾀﾝﾌﾟ数</th>
						<td  align="center" valign="middle"><?php echo $detail_user['total_stamp_num'];?></td>
						<th>現在ｽﾀﾝﾌﾟ数</th>
						<td  align="center" valign="middle"><?php echo $detail_user['stamp_num'];?></td>
					</tr>
					<tr align="center" bgcolor="#ffffff">
						<th>最終ｽﾀﾝﾌﾟ日</th>
						<?php $last_stamp_date = Util::get_rally_of_day($detail_user['last_stamp_date'] , $detail_user['rally_id']);?>
						<td  align="center" valign="middle"><?php echo $last_stamp_date[0]."/".$last_stamp_date[1]."/".$last_stamp_date[2];?></td>
						<th>累計取得クーポン数</th>
						<td  align="center" valign="middle"><?php echo $all_coupon;?></td>
						<th>現在取得クーポン数</th>
						<td  align="center" valign="middle"><?php echo $now_coupon;?></td>
					</tr>
				</table>
			</div>
		</div>

        <div class="karte_container" style="margin-bottom:20px;">
            <textarea id="summernote" name="content" rows="18">
                <?php echo $karte; ?>
            </textarea>
	</div>
            
		<table width="100%"  border="0" cellpadding="0" cellspacing="0" >
			<tr align="center" >
				<th width="33%" bgcolor="#ffb202">スタンプ履歴</th>
				<th width="33%" bgcolor="#ffb202">クーポン発行履歴</th>
				<th width="33%" bgcolor="#ffb202">クーポン使用履歴</th>
			</tr>
			<tr align="center">
				<td height="500"  align="left" valign="top">
					<ul class="log_box">
					<?php
					//スタンプ履歴
                                        
                                        // スタンプ履歴数を取得
					$count_hist = count($stamp_history);
                                        for ($i = 0; $i < $count_hist; $i++) {
						$add_date_str = explode( ":", $stamp_history[$i]['add_date']);
						$add_date_date = $add_date_str[0].":".$add_date_str[1];
						$add_date_date = str_replace("-", "/", $add_date_date);
					?>
						<li><b><?php echo $add_date_date;?></b>　スタンプ <?php echo abs($stamp_history[$i]['stamp_num']);?> 個
                                                    <?php echo $stamp_history[$i]['stamp_num'] >= 0 ? "付与" : "無効";?>
                                                </li>
					<?php
					}
					?>
					</ul>
				</td>
				<td width="400" align="left" valign="top">
					<ul class="log_box">
					<?php
					// クーポン取得履歴
                                        $count_hist = count($got_coupon_history);
                                        for ($i = 0; $i < $count_hist; $i++) {
						$acquisition_date_str = explode( ":", $got_coupon_history[$i]['acquisition_date']);
						$acquisition_date_date = $acquisition_date_str[0].":".$acquisition_date_str[1];
						$acquisition_date_date = str_replace("-", "/", $acquisition_date_date);
					?>
						<li><b><?php echo $acquisition_date_date;?></b>　「<?php echo $got_coupon_history[$i]['get_coupon_name'];?>」クーポン取得</li>
					<?php
					}
					?>
					</ul>
				</td>
				<td width="400"  align="left" valign="top">
					<ul class="log_box">
					<?php
					// クーポン使用履歴
                                        $count_hist = count($consumed_coupon_history);
                                        for ($i = 0; $i < $count_hist; $i++) {
						$use_date_str = explode( ":", $consumed_coupon_history[$i]['use_date']);
						$use_date_date = $use_date_str[0].":".$use_date_str[1];
						$use_date_date = str_replace("-", "/", $use_date_date);
					?>
						<li><b><?php echo $use_date_date;?></b>　「<?php echo $consumed_coupon_history[$i]['get_coupon_name'];?>」クーポン使用</li>
					<?php
					}
					?>
					</ul>
				</td>
			</tr>
		</table>
	</div>
</div>

<script>
$(function() {
      
      $('#summernote').summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['paragraph']],
            ['Insert', ['picture','link']],
            ['Misc', ['fullscreen', 'undo']],
        ],
        height: 300,   //set editable area's height
        focus: false    //set focus editable area after Initialize summernote
      });
      
      var saveFlag = true;
      $('.note-toolbar').on("click", function(e){
          // ツールバーがクリックされた場合は保存を回避する
          saveFlag = false;
      });
      
      $('.note-editable').on("click", function(e){
          // 編集領域がクリックされたので、保存フラグをON
          saveFlag = true;
      });
      $('.note-editable').on("blur", function(e){
          // 保存開始
          setTimeout(function() {
              if (saveFlag) {
                  postKarte();
              } else {
                  saveFlag = true;
              }
          }, 100);
      });
});

/**
 * カルテ情報をサーバへポストする
 * @returns {undefined}
 */
var postKarte = function() {
//	var content = $('textarea[name="content"]').html($('#summernote').code());
                // カルテの内容を取得
                var karteValue = $('#summernote').code();
                var textByteSize = gf_GetByte(karteValue);
                
                // データサイズチェック DBのディスク容量 1MBの制限(1024*1024*1)
                if (textByteSize >= 1024*1024*1) {
                    // DBの保存制限以上のデータサイズの場合、POSTしない
                    // 保存できない旨のトースト通知
                    laquu.Toast.show({
                        message: "保存できませんでした。カルテ情報のMAXサイズを超えています。"
                    });
                    
                    return;
                }
                console.log(karteValue);
                
                // カルテの内容のサイズチェックする?
                // サイズが大きすぎると負荷もかかるし、カラムサイズに収まらないかも。
                
                // カルテの内容をPOSTする
                $.ajax({
                    type: "POST",        
                    url: "./sys_controller/CustomerDetailController.php",
                    data: {
                        action : "save_karte",
                        karte_value : karteValue
                    }, 
                    success: function(data) {
                        if (data.length > 0) {
                            // 保存しました。の旨をふわっと出したい。
                            var jsonpData = JSON.parse(data);
                            
                            // 保存した旨のトースト通知
                            laquu.Toast.show({
                                message: "カルテを保存しました。"
                            });
                        }
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                                                // 保存した旨のトースト通知
                        laquu.Toast.show({
                            message: "カルテの保存ができませんでした。"
                        });
                        console.log("XMLHttpRequest : " + XMLHttpRequest.status);
                        console.log("textStatus : " + textStatus);
                        console.log("errorThrown : " + errorThrown.message);
                    }
                });

}

/* バイト数を取得する */
function gf_GetByte(aStr){
    if(aStr.length == 0){return 0;}
    var count = 0;
    var Str = "";
    for(var i=0;i < aStr.length;i++){
        Str = aStr.charAt(i);
        Str = escape(Str);
        if( Str.length  < 4 ){
            count = count + 1;
        }else{
            count = count + 2;
        }
    }
    return count;
}
</script>

<script>
$(function() {
	
	$('#stamp_btn').click(function(){
		console.log('stamp_btn');
		// スタンプ付与ボタンがクリックされた場合
		var strTitle = "スタンプ付与確認";
		var strCostrTitlemment = "OKするとスタンプを１個付与します。<br>スタンプを付与しますか?<br>";
		
		// 確認ダイアログを作成
		$( "#show_dialog" ).html(strCostrTitlemment);
		$( "#show_dialog" ).dialog({
				modal: true,
				title: strTitle,
				buttons: {
					"OK": function() {
						$( this ).dialog( "close" );
						window.location.href='./?p=customer&stamp=1&user_id=<?php echo $user_id; ?>&rally_id=<?php echo $rally_id; ?>';
					},
					"キャンセル": function() {
						$( this ).dialog( "close" );
					}
				}
		});
	});

});
</script>

<?php
require './pc/footer.php';
?>