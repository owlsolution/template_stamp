<div class="box">
		<h2> <img src="images/icon1.gif" width="16" height="16"> 問合せ履歴 </h2>
		<div class="container">
			<table id="rsv_list_table" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr align="center">
									<th width="10%">問合せNo</th>
									<th width="20%">受付日</th>
									<th width="20%">氏名</th>
									<th width="10%">状態</th>
									<th width="30%"></th>
							</tr>
						<?php foreach ($history_list as $key => $inq): ?>
							<tr>
							<td align="center"><?= 'RSV'.sprintf("%05d", $inq['inquiry_id']); ?></td>
								<td align="center"><?= explode(' ', $rsv['create_date'])[0]; ?></td>
								<td align="center"><?= $inq['name']; ?></td>
								<td align="center"><?= InquiryModel::$STATUS_TEXTS[$inq['status']]; ?></td>
								<td align="center"><a href="?p=inquiry_detail&check_id=<?= $inq['inquiry_id'];?>">確認</a><br><br><a href="?p=inquiry_detail&dele_id=<?= $inq['inquiry_id'];?>" onClick="return confirm('削除しますか?')">削除</a></td>
							</tr>
						<?php endforeach; ?>
			</table>
		</div>
</div>
