<?php
require './sp/header.php';
?>


<style type="text/css">
.ui-tabs{ border: none; padding: 0px; }
.ui-tabs .ui-widget .ui-widget-content .ui-corner-all{
   border: none; padding: 0px;
}
.ui-widget-header{ background: #ffffff; border: none; }
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active .ui-state-focus {
   border: 1px solid #0092D6; background: #0092D6;
}
.ui-tabs-panel .ui-widget-content .ui-corner-bottom{
   background: #ffffff;
}
.ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited{
   color: #ffffff !important;
}
.ui-state-default a, .ui-state-default a:link, .ui-state-default a:visited{
   color: #c0c0c0;
}
.ui-state-hover a, .ui-state-hover a:hover{
   color: #0092D6;
}

.comment_area {
   background: #ffffff !important;
}

.ui-tabs .ui-tabs-panel {
    padding: 0px !important;
}
</style>

<input type="hidden" id="selected_tab"  value="0">
<script type="text/javascript">
	
	function getUrlVars()
	{
		var vars = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		for(var i = 0; i < hashes.length; i++)
		{
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	}
	
    $(function(){
        // タブ生成
		var selected_index = $('#selected_tab').val();
		var tab_index = getUrlVars()["tab_index"];
		if (tab_index > 0) {
			// スケジュール設定削除の場合、表示するタブはスケジュール配信にする
			selected_index = tab_index;
		}
        $('#tabs').tabs({
//            active: $('#selected_tab').val(),
            active: selected_index,
        } );
        
        $( "#tabs" ).tabs({
            beforeActivate: function( event, ui ) {
                // インデックス番号は次のように取得可能
                $('#selected_tab').val(ui.newTab.index());
            }
        });

        $('#dialog_link, ul#icons li').hover(
            function() { $(this).addClass('ui-state-hover'); },
            function() { $(this).removeClass('ui-state-hover'); }
        );
        // タブ表示
        $('#tabs').css("display","block");

        if($('#selected_tab').val() == 0) {
            // お知らせタブの場合、どの配信タイプを取得
            var value = $("input[name='delivery_set']:checked").val();
            // 表示を調整
            checkradio(value);
        }

    });
</script>

<div class="box">
    <h2> <img src="images/icon1.gif" width="16" height="16"> 配信機能 </h2>
    <div id="tabs" style="display:none;">
		<font size="2">
        <ul>
            <?php if ($to_user_id == "") {  // 通常と個人宛で表示を変更する?>
            <li><a href="#tabs-1">お知らせ</a></li>
            <li><a href="#tabs-2">ﾌﾞﾛｸﾞ</a></li>
            <li><a href="#tabs-3">ﾁﾗｼ</a></li>
            <li><a href="#tabs-5">ｽｹｼﾞ..</a></li>
            <?php } else { ?>
            <li><a href="#tabs-4">個人宛</a></li>
            <?php } ?>
        </ul>
		</font>
        
        <?php if ($to_user_id == "") {  // 通常と個人宛で表示を変更する?>
        <!-- -------- -->
        <!-- お知らせ>
        <!-- -------- -->
        <div id="tabs-1" class="comment_area">
            <form method="POST" action="./?p=notice" name="form2" enctype="multipart/form-data">
                <div class="container">
                        <ul>
                                参加者へ各種お知らせ配信ができます。
                        </ul>
                        <div class="f_box">
						<div class="container1">
								<h3> お知らせ配信設定 </h3>
								<ul class="clearfix">
									<li>
										<input type="radio" name="delivery_set" value="all"  onclick="removeCheckBoxToUserListPopup();checkradio('all');" checked> 一斉配信
									</li>
									<li>
										<input type="radio" name="delivery_set" value="simple" onclick="removeCheckBoxToUserListPopup();checkradio('simple');"> 簡単配信
									</li>
									<li>
										<input type="radio" name="delivery_set" value="individual" onclick="addCheckBoxToUserListPopup();checkradio('individual');"> 個別配信
									</li>
								</ul>
							</div>

                                <!-- 個別配信 -->
                                <div class="container2" id="individual_menu"  style="display:none">
                                        <h3> 個別配信絞り込み </h3>
                                        <ul class="clearfix">
                                                <li class="clearfix">
                                                        <p class="titel">・性別 <span style="color:red;">※1</span></p>
                                                        <p class="text">
                                                                <input type="radio" name="individual_sex" value="all" <?php if($individual_sex == "all"){ echo "checked"; }?> onclick="individual_check()"> 全て　　　
                                                                <input type="radio" name="individual_sex" value="man" <?php if($individual_sex == "man"){ echo "checked"; }?> onclick="individual_check()"> 男性　　　
                                                                <input type="radio" name="individual_sex" value="woman" <?php if($individual_sex == "woman"){ echo "checked"; }?> onclick="individual_check()"> 女性
                                                        </p>
                                                </li>
                                                <li class="clearfix even">
                                                        <p class="titel">・誕生日 <span style="color:red;">※1</span></p>
                                                        <p class="text" >
                                                                <select name="individual_birthday_start" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=1;$i<=12;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                月 〜
                                                                <select name="individual_birthday_end" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=1;$i<=12;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>" <?php if($i == 12){ echo "selected";}?>><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                月
                                                        </p>
                                                </li>
                                                <li class="clearfix">
                                                        <p class="titel">・地域 <span style="color:red;">※1</span></p>
                                                        <p class="text">
														<select name="individual_region" onChange="individual_check()">
															<option value=""><?php echo ALL_REGION_LABEL;?></option>
															<?php
																$region_group_list = json_decode(REGION_LIST,true);
																foreach ($region_group_list as $group) {
																	// １つのグループを取り出す
																	if(!empty($group['gname'])) {
																		echo '<optgroup label="'.$group['gname'].'">';
																	}

																	// 地域アイテムのループ
																	$region_list = explode(",", $group['regions']);
																	foreach ( $region_list as $region_item) {
																		$elem = ($individual_region == $region_item) ? "selected" : "";
																		echo '<option value="'.$region_item.'" '.$elem.'>'.$region_item.'</option>';
																	}

																	if(!empty($group['gname'])) {
																		echo '</optgroup>';
																	}
																}
															?>
														</select>
                                                        </p>
                                                </li>
                                                <li class="clearfix even">
                                                        <p class="titel">・最終来店日</p>
                                                        <p class="text" align="center">
                                                                <select name="last_year_start" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=2013;$i<=date("Y");$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                年
                                                                <select name="last_month_start" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=1;$i<=12;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                月
                                                                <select name="last_day_start" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=1;$i<=31;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                日<br>
                                                                ↓
                                                                <br>
                                                                <select name="last_year_end" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=2013;$i<=date("Y");$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>" <?php if($i == date("Y")){ echo "selected";}?>><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                年
                                                                <select name="last_month_end" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=1;$i<=12;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>" <?php if($i == date("n")){ echo "selected";}?>><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                月
                                                                <select name="last_day_end" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=1;$i<=31;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>" <?php if($i == date("j")){ echo "selected";}?>><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                日
                                                        </p>
                                                </li>
                                                <li class="clearfix">
                                                        <p class="titel">・累計スタンプ数</p>
                                                        <p class="text">
                                                                <input type="text" name="total_stamp_num" id = "individual_total_stamp_num" size="10" onChange="individual_check()" value="0"> 個
                                                                <select name="total_stamp_terms" onChange="individual_check()">
                                                                        <option value="or_more">以上</option>
                                                                        <option value="downward">以下</option>
                                                                </select>
                                                        </p>
                                                </li>
												<li class="clearfix even">
													<p class="titel">・キーワード検索</p>
													<p class="text">
														<input type='text' id='search_keyword' name='search_keyword' size='26' onChange="individual_check()" value='<?php echo stripslashes($keyword); ?>'>
														<br>※[顧客名][カルテ情報]をキーワード検索します。
													</p>
												</li>
												<?php
												if ($_SESSION["branchId"] == 0) {
												?>
												<li class="clearfix">
													<p class="titel">・店舗一覧</p>
														<p class="text">
															<select name="branch_list" onChange="individual_check()">
																<?php
																//店舗一覧を取得
																$branch_list = $noticeComp->select_branch_list();
																foreach($branch_list as $branch){
																	error_log("branch_id result : ".$branch['branch_id']);
																?>
																	<option value="<?php echo $branch['branch_id'];?>"><?php echo $branch['branch_name'];?></option>
																<?php
																}
																?>
															</select>
														</p>
												</li>
												<?php
												}
												?>
                                        </ul>
                                        <p style="color:red; margin:10px;">※1 参加者がプロフィール登録している場合のみ反映されます。</p>
                                </div>
                                <!-- 簡単配信 -->
                                <div class="container1" id="simple_menu" style="display:none">
                                        <h3> お知らせしたい人を選んで下さい </h3>
                                        <ul class="clearfix">
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox1" value="last_month" onclick="simple_check()"> 先月誕生日の人 <span style="color:red;">※1</span>
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox2" value="this_month" onclick="simple_check()"> 今月誕生日の人 <span style="color:red;">※1</span>
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox3" value="next_month" onclick="simple_check()"> 来月誕生日の人 <span style="color:red;">※1</span>
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox4" value="man" onclick="simple_check()"> 男性 <span style="color:red;">※1</span>
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox5" value="woman" onclick="simple_check()"> 女性 <span style="color:red;">※1</span>
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox6" value="yesterday_come" onclick="simple_check()"> 昨日来店した人
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox7" value="month_come" onclick="simple_check()"> 1ヶ月以上来店してない人
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox8" value="this_month_come" onclick="simple_check()"> 今月来店した人
                                                </li>
                                        </ul>
                                        <p style="color:red; margin:10px;">※1 参加者がプロフィール登録している場合のみ反映されます。</p>
                                </div>
                                <div class="container1" id="format_menu" style="display:none">
                                        <h3> お知らせ形式 </h3>
                                        <ul class="clearfix">
                                                <li>
                                                        <input type="checkbox" name="format[]" id="format_push" onclick="format_check()" value="push" checked> プッシュ通知 <span style="color:red;">※2</span>
                                                </li>
                                                <li>
                                                        <input type="checkbox" name="format[]" id="format_mail" onclick="format_check()" value="mail" checked> メール送信 <span style="color:red;">※3</span>
                                                </li>
                                        </ul>
                                        <p style="color:red; margin:10px;">※2 アプリの方対象<br>※3 主にガラケーの方対象</p>
                                </div>
                                <?php 
                                    // 配信予約を指定するメニューを読み込み
                                    require './sp/page/notice_form_reserve.php';
                                ?>
                                <?php 
                                    // SNS連動のメニューを読み込み
                                    //require './sp/page/notice_form_sns.php';
                                ?>
                                <?php 
                                    // 仮投稿を指定するメニューを読み込み
                                    require './sp/page/notice_form_save.php';
                                ?>

                                <div class="f_digit">
                                        <div class="titel"> お知らせを送る人数 </div>
                                        <div class="digit"> <span id = "search_data" onclick="search_data('notice');"> <?php echo $all_customer;?> </span> <strong>/<?php echo $all_customer?></strong> <input type="hidden" name = "delivery_num" id = "delivery_num" value = "<?php echo $all_customer;?>" ></div>
                                </div>
                        </div>
                </div>
				<p style="color:red; margin:10px;">※絵文字は非対応です。配信が正常に行われない可能性がありますのでご注意ください。<br>顔文字は問題ありません。</p>
                <div class="box" style="">
                        <div class="f_box">
                                <div class="container1">
                                        <h3> タイトル (全角50文字以内) </h3>
                                        <ul class="clearfix">
                                                <li>
                                                        <input name="notice_title" type="text" placeholder="タイトルを入力してください。" value="" class="f_text1">
                                                </li>
                                        </ul>
                                </div>
                                <div class="container1">
                                        <h3> 本文 (全角1000文字以内) </h3>
                                        <ul class="clearfix">
                                                <li>
														<input style="margin-bottom:10px;" onclick="$('#notice_image_up1').click();" type="button" value="ファイルを選択">
														<input type="file" name="notice_image" id="notice_image_up1" style="margin-bottom:0px;font-size: 0px;display:none;"><br>
                                                        <textarea class="f_text2" name="notice_date" id="notice_date1" placeholder="配信の内容をここに入力してください。"></textarea>
                                                </li>
                                        </ul>
                                        <div class="btn">
                                                <input name="check" type="submit" value="確 認">
                                        </div>
										<input type="hidden" name="exclusion_user_id" id="exclusion_user_id" value="">
                                        <input type="hidden" name = "delivery_type" value = "notice" >
                                </div>
                        </div>
                </div>
            </form>
        </div>
        <div id="tabs-2" class="comment_area">
        <!-- -------- -->
        <!-- ブログ配信 -->
        <!-- -------- -->
           <form method="POST" action="./?p=notice" name="form2" enctype="multipart/form-data">
                <div class="container">
                        <ul>
                                参加者へ向けたブログ配信ができます。
                        </ul>
                        <div class="f_box">
							<div class="container1">
									<h3> ブログ配信者指定 </h3>
									<p class="titel">&nbsp;&nbsp;&nbsp;&nbsp;
											<select name="staff_admin_id">
												<option value="0">-</option>
												<?php foreach ($staff_list as $staff) { ?>
												  <option value='<?php echo $staff['admin_id'];?>'><?php echo $staff['nickname'];?></option>
												<?php } ?>
											</select>
									<a href="./?p=staff" style="margin: 0px 6px;color:blue;">[スタッフ一覧の追加/編集]</a><br>
							</div>
						</div>
                        <div class="f_box">
                                <?php 
                                    // 配信予約を指定するメニューを読み込み
                                    require './sp/page/notice_form_reserve.php';
                                ?>
                                <?php 
                                    // SNS連動のメニューを読み込み
                                    //require './sp/page/notice_form_sns.php';
                                ?>
                                <?php 
                                    // 仮投稿を指定するメニューを読み込み
                                    require './sp/page/notice_form_save.php';
                                ?>
                        </div>
				<p style="color:red; margin:10px;">※絵文字は非対応です。配信が正常に行われない可能性がありますのでご注意ください。<br>顔文字は問題ありません。</p>
                <div class="box" style="">
                        <div class="f_box">
                                <div class="container1">
                                        <h3> タイトル (全角50文字以内) </h3>
                                        <ul class="clearfix">
                                                <li>
                                                        <input name="notice_title" type="text" placeholder="ブログのタイトルを入力してください。" class="f_text1">
                                                </li>
                                        </ul>
                                </div>
                                <div class="container1">
                                        <h3> 本文 (全角1000文字以内) </h3>
                                        <ul class="clearfix">
                                                <li>
														<input style="margin-bottom:10px;" onclick="$('#notice_image_up2').click();" type="button" value="ファイルを選択">
														<input type="file" name="notice_image" id="notice_image_up2" style="margin-bottom:0px;font-size: 0px;display:none;"><br>
                                                        <textarea class="f_text2" name="notice_date" id="notice_date2" placeholder="配信の内容をここに入力してください。"></textarea>
                                                </li>
                                        </ul>
                                        <div class="btn">
                                                <input name="check" type="submit" value="確 認">
                                        </div>
                                        <input type="hidden" name = "delivery_type" value = "blog" >
                                        <input type="hidden" name="delivery_set" value="blog">
                                </div>
                        </div>
                </div>
            </div>
            </form>
        </div>
        <!-- -------- -->
        <!-- チラシ配信 -->
        <!-- -------- -->
        <div id="tabs-3" class="comment_area">
            <form method="POST" action="./?p=notice" name="form2" enctype="multipart/form-data">
                <div class="container">
                            <ul>
                                    参加者へチラシ配信ができます。
                            </ul>
                        <div class="f_box">
                                <?php 
                                    // 配信予約を指定するメニューを読み込み
                                    require './sp/page/notice_form_reserve.php';
                                ?>
                        </div>
					<p style="color:red; margin:10px;">※絵文字は非対応です。配信が正常に行われない可能性がありますのでご注意ください。<br>顔文字は問題ありません。</p>
                    <div class="box">
                            <div class="f_box">
                                    <div class="container1">
                                            <h3> タイトル (全角50文字以内) </h3>
                                            <ul class="clearfix">
                                                    <li>
                                                            <input name="notice_title" type="text" placeholder="タイトルを入力してください。"  class="f_text1">
                                                    </li>
                                            </ul>
                                    </div>
                                    <div class="container1">
                                            <h3 id="notice_comments_title"> 本文 </h3>
                                            <ul class="clearfix">
                                                    <li>
														<input type="file" name="notice_image" id="notice_image_up3" style="margin-bottom:10px;"><br>
                                                            <div id="annotation" style="display: none;" style="display: block; color: red; font-size: 12px;width: 583px;">&nbsp;&nbsp;※添付可能なファイル形式はJPEG形式(.jpg .jpeg) / PNG形式(.png) / GIF形式(.gif)です。</div>
                                                            <input id="leaflets_file_name" name="leaflets_file_name" type="hidden" value="">
                                                    </li>
                                            </ul>
                                            <div class="btn">
                                                    <input name="check" type="submit" value="確 認">
                                            </div>
                                            <input type="hidden" name = "delivery_type" value = "leaflets" >
                                            <input type="hidden" name="delivery_set" value="leaflets">
                                    </div>
                            </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- -------- -->
        <!-- チラシ配信(終) -->
        <!-- -------- -->
		
        <div id="tabs-5" class="comment_area">
        <!-- -------- -->
        <!-- スケジュール配信 -->
        <!-- -------- -->
            <form method="POST" action="./?p=notice" name="form2" enctype="multipart/form-data">
                <div class="container">
                        <ul>
                            お知らせをスケジューリングして配信ができます。
                        </ul>
                        <div class="f_box">
                                <!-- 個別配信 -->
                                <div class="container2" id=""  style="">
										<h3> タイプ設定 </h3>
										<ul class="clearfix">
												<li class="clearfix">
														<p class="titel"><input type="radio" name="schedule_type" value="new_user_push_1" checked onclick=""> 新規入会後配信</p>
														<p class="text">新規入会時点から
															<select name="new_user_time_difference" onChange="">
																	<option value="3">3分</option>
																	<option value="5">5分</option>
																	<option value="10">10分</option>
																	<option value="15">15分</option>
																	<option value="20">20分</option>
																	<option value="30">30分</option>
																	<option value="45">45分</option>
																	<option value="60">60分</option>
																	<option value="90">90分</option>
																	<option value="120">120分</option>
																	<option value="180">3時間</option>
																	<option value="240">4時間</option>
																	<option value="300">5時間</option>
																	<option value="360">6時間</option>
																	<option value="480">8時間</option>
																	<option value="600">10時間</option>
																	<option value="720">12時間</option>
																	<option value="900">15時間</option>
																	<option value="1080">18時間</option>
																	<option value="1260">21時間</option>
															</select>後に配信</p>
												</li>
												<li class="clearfix even">
														<p class="titel"><input type="radio" name="schedule_type" value="new_user_push_2" onclick="individual_check()"> 新規入会後配信②</p>
														<p class="text">
															 新規入会日から
															 <select name="new_user_day_difference" onChange="">
																	<option value="1">1</option>
																	<option value="2">2</option>
																	<option value="3">3</option>
																	<option value="4">4</option>
																	<option value="5">5</option>
																	<option value="6">6</option>
																	<option value="7">7</option>
																	<option value="8">8</option>
																	<option value="9">9</option>
																	<option value="10">10</option>
																	<option value="15">15</option>
																	<option value="20">20</option>
																	<option value="25">25</option>
																	<option value="30">30</option>
																	<option value="40">40</option>
																	<option value="50">50</option>
																	<option value="60">60</option>
																	<option value="70">70</option>
																	<option value="80">80</option>
																	<option value="90">90</option>
																	<option value="100">100</option>
															</select>日後に配信　　　
														</p>
												</li>
												<li class="clearfix">
														<p class="titel"><input type="radio" name="schedule_type" value="last_stamp_push_1"> ｽﾀﾝﾌﾟ付与後配信</p>
														<p class="text">最新スタンプ付与日から
															<select name="last_stamp_after_day" onChange="">
																	<option value="1">1</option>
																	<option value="2">2</option>
																	<option value="3">3</option>
																	<option value="4">4</option>
																	<option value="5">5</option>
																	<option value="6">6</option>
																	<option value="7">7</option>
																	<option value="8">8</option>
																	<option value="9">9</option>
																	<option value="10">10</option>
																	<option value="15">15</option>
																	<option value="20">20</option>
																	<option value="25">25</option>
																	<option value="30">30</option>
																	<option value="40">40</option>
																	<option value="50">50</option>
																	<option value="60">60</option>
																	<option value="70">70</option>
																	<option value="80">80</option>
																	<option value="90">90</option>
																	<option value="100">100</option>
															</select>日後に配信
														</p>
												</li>
												<li class="clearfix even">
														<p class="titel"><input type="radio" name="schedule_type" value="last_stamp_push_2"> ｽﾀﾝﾌﾟ付与後配信②</p>
														<p class="text">累計
															<select name="total_stamps" onChange="">
																	<option value="1">1</option>
																	<option value="2">2</option>
																	<option value="3">3</option>
																	<option value="4">4</option>
																	<option value="5">5</option>
																	<option value="6">6</option>
																	<option value="7">7</option>
																	<option value="8">8</option>
																	<option value="9">9</option>
																	<option value="10">10</option>
																	<option value="15">15</option>
																	<option value="20">20</option>
																	<option value="25">25</option>
																	<option value="30">30</option>
																	<option value="40">40</option>
																	<option value="50">50</option>
																	<option value="60">60</option>
																	<option value="70">70</option>
																	<option value="80">80</option>
																	<option value="90">90</option>
																	<option value="100">100</option>
															</select>個目のスタンプ付与日から
															<select name="total_stamp_after_day" onChange="">
																	<option value="1">1</option>
																	<option value="2">2</option>
																	<option value="3">3</option>
																	<option value="4">4</option>
																	<option value="5">5</option>
																	<option value="6">6</option>
																	<option value="7">7</option>
																	<option value="8">8</option>
																	<option value="9">9</option>
																	<option value="10">10</option>
																	<option value="15">15</option>
																	<option value="20">20</option>
																	<option value="25">25</option>
																	<option value="30">30</option>
																	<option value="40">40</option>
																	<option value="50">50</option>
																	<option value="60">60</option>
																	<option value="70">70</option>
																	<option value="80">80</option>
																	<option value="90">90</option>
																	<option value="100">100</option>
															</select>日後に配信
														</p>
												</li>
												<li class="clearfix">
														<p class="titel"><input type="radio" name="schedule_type" value="birthday_push_1"> 誕生日前配信</p>
														<p class="text">誕生日の
															<select name="before_day_of_birth" onChange="">
																<option value="1">1</option>
																<option value="2">2</option>
																<option value="3">3</option>
																<option value="4">4</option>
																<option value="5">5</option>
																<option value="6">6</option>
																<option value="7">7</option>
																<option value="8">8</option>
																<option value="9">9</option>
																<option value="10">10</option>
																<option value="15">15</option>
																<option value="20">20</option>
																<option value="25">25</option>
																<option value="30">30</option>
																<option value="40">40</option>
																<option value="50">50</option>
																<option value="60">60</option>
																<option value="70">70</option>
																<option value="80">80</option>
																<option value="90">90</option>
																<option value="100">100</option>
															</select>日前に配信
														</p>
												</li>
												<li class="clearfix even">
														<p class="titel"><input type="radio" name="schedule_type" value="birthday_push_2"> 誕生日前配信②</p>
														<p class="text">誕生日月の1日に配信</p>
												</li>
												<li class="clearfix">
														<p class="titel"><input type="radio" name="schedule_type" value="weekly_push"> 週刊配信</p>
														<p class="text">毎週
															<select name="day_of_the_week" onChange="">
																<option value="1">月曜日</option>
																<option value="2">火曜日</option>
																<option value="3">水曜日</option>
																<option value="4">木曜日</option>
																<option value="5">金曜日</option>
																<option value="6">土曜日</option>
																<option value="0">日曜日</option>
															</select>に配信
														</p>
												</li>
												<li class="clearfix even">
														<p class="titel"><input type="radio" name="schedule_type" value="monthly_push"> 月刊配信</p>
														<p class="text">毎月
															<select name="monthly_push_day_of_the_month" onChange="">
																<?php
																for($i=1; $i<=31; $i++){
																?>
																	<option value="<?php echo $i;?>" <?php if($i == 1){ echo "selected";}?>><?php echo $i;?></option>
																<?php
																}
																?>
																<option value="last_day">月末</option>
															</select>日に配信　<span style="color:red;">※1</span>
														</p>
												</li>
												<li class="clearfix">
														<p class="titel"><input type="radio" name="schedule_type" value="last_month_stamp_push"> 前月来店配信</p>
														<p class="text">前月に
															<select name="last_month_stamp_num" onChange="">
																<option value="1">1</option>
																<option value="2">2</option>
																<option value="3">3</option>
																<option value="4">4</option>
																<option value="5">5</option>
																<option value="6">6</option>
																<option value="7">7</option>
																<option value="8">8</option>
																<option value="9">9</option>
																<option value="10">10</option>
															</select>回以上スタンプ付与した人に毎月
															<select name="last_month_push_day" onChange="">
																<?php
																for($i=1; $i<=31; $i++){
																?>
																	<option value="<?php echo $i;?>" <?php if($i == 1){ echo "selected";}?>><?php echo $i;?></option>
																<?php
																}
																?>
																<option value="last_day">月末</option>
															</select>日に配信　
															<span style="color:red;">※1</span>
														</p>
												</li>
										</ul>
										<p style="color:red; margin:10px;">※1 
										配信月に設定の日にちが無い場合は配信されません。月末に配信したい場合は、月末を選択してください。</p>                                </div>
								<div class="container1" id="format_menu">
									<h3> お知らせ形式 </h3>
									<ul class="clearfix">
										<li>
											<input type="checkbox" name="format[]" id="format_push" onclick="" value="push" checked> プッシュ通知 <span style="color:red;">※2</span>
										</li>
										<li>
											<input type="checkbox" name="format[]" id="format_mail" onclick="" value="mail" checked> メール送信 <span style="color:red;">※3</span>
										</li>
									</ul>
									<p style="color:red; margin:10px;">※2 アプリの方対象<br>※3 主にガラケーの方対象</p>
								</div>
								<div class="container1">
										<h3> 配信時間指定 </h3>
										<br>
										<p class="push_time_menu">
												&nbsp&nbsp
												<select name="push_hour" class="push_hour">
														<?php
														for($i=0; $i<24; $i++){
														?>
														<option value="<?php echo $i;?>" <?php if($i == date("H")){ echo "selected";}?>><?php echo $i;?></option>
														<?php
														}
														?>
												</select>
												時 
												<select name="push_minute" class="push_minute">
														<?php
														$period = 30;
														for($i=0; $i<=1; $i++){
														?>
														<option value="<?php echo $i*$period;?>"><?php echo $i*$period;?></option>
														<?php
														}
														?>
												</select>
												分
										</p>
										<p style="color:red; margin:10px;">※[新規入会後配信]の場合、配信時間指定は無視されます。</p>
										<br>
								</div>
                        </div>
                </div>
				<p style="color:red; margin:10px;">※絵文字は非対応です。配信が正常に行われない可能性がありますのでご注意ください。<br>顔文字は問題ありません。</p>
                <div class="box" style="">
                        <div class="f_box">
                                <div class="container1">
                                        <h3> タイトル (全角50文字以内) </h3>
                                        <ul class="clearfix">
                                                <li>
                                                        <input name="notice_title" type="text" placeholder="タイトルを入力してください。" value="" class="f_text1">
                                                </li>
                                        </ul>
                                </div>
                            <div class="container1">
                                        <h3> 本文 (全角1000文字以内) </h3>
                                        <ul class="clearfix">
                                                <li>
														<input style="margin-bottom:10px;" onclick="$('#notice_image_up5').click();" type="button" value="ファイルを選択">
														<input type="file" name="notice_image" id="notice_image_up5" style="margin-bottom:0px;font-size: 0px;display:none;"><br>
                                                        <textarea class="f_text2" name="notice_date" id="notice_date5" placeholder="配信の内容をここに入力してください。"></textarea>
                                                </li>
                                        </ul>
                                        <div class="btn">
                                                <input name="schedule_check" type="submit" value="確 認">
                                        </div>
                                        <input type="hidden" name = "delivery_type" value = "notice" >
										<input type="hidden" name="exclusion_user_id" id="exclusion_user_id" value="">
                                </div>
                        </div>
                </div>
            </form>
			<br>
			<?php
			require './sp/page/notice_schedule_list.php';
			?>

        </div>

        <?php } else { ?>
        <div id="tabs-4" class="comment_area">
        <!-- -------- -->
        <!-- 個人宛配信 -->
        <!-- -------- -->
        <form method="POST" action="./?p=notice&to_user_id=<?php echo $to_user_id;?>" name="form2" enctype="multipart/form-data">
				<?php if(isset($_REQUEST['reserve_id'])): ?>
					<input type="hidden" name="reserve_id" value="<?= $_REQUEST['reserve_id']; ?>">
				<?php endif; ?>
				<?php if(isset($_REQUEST['inquiry_id'])): ?>
					<input type="hidden" name="inquiry_id" value="<?= $_REQUEST['inquiry_id']; ?>">
				<?php endif; ?>
                <div class="container">
                        <ul>
                                参加者へ向けた個人宛の配信ができます。
                        </ul>
                        <div class="f_box">
                                <?php 
                                    // 配信予約を指定するメニューを読み込み
                                    require './sp/page/notice_form_reserve.php';
                                ?>
                    </div>
					<p style="color:red; margin:10px;">※絵文字は非対応です。配信が正常に行われない可能性がありますのでご注意ください。<br>顔文字は問題ありません。</p>
                    <div class="box">
                            <div class="f_box">
                                    <div class="container1">
                                            <h3> タイトル (全角50文字以内) </h3>
                                            <ul class="clearfix">
                                                    <li>
                                                            <input name="notice_title" type="text" placeholder="配信のタイトルをここに入力してください。" value="" class="f_text1">
                                                    </li>
                                            </ul>
                                    </div>
                                    <div class="container1">
                                            <h3> 本文 (全角1000文字以内) </h3>
                                            <ul class="clearfix">
                                                    <li>
														<input style="margin-bottom:10px;" onclick="$('#notice_image_up4').click();" type="button" value="ファイルを選択">
														<input type="file" name="notice_image" id="notice_image_up4" style="margin-bottom:0px;font-size: 0px;display:none;"><br>
                                                            <textarea class="f_text2" name="notice_date" id="notice_date4" placeholder="配信の内容をここに入力してください。" ></textarea>
                                                    </li>
                                            </ul>
                                            <div class="btn">
                                                    <input name="check" type="submit" value="確 認">
                                            </div>
                                            <input type="hidden" name = "delivery_type" value = "to_person" >
                                            <input type="hidden" name="delivery_set" value="to_person">
                                            <input type="hidden" name="format[]" id="format_push" value="push" checked>
                                            <input type="hidden" name="format[]" id="format_mail" value="mail" checked>
                                    </div>
                            </div>
                    </div>
                </div>
            </form>
        <!-- -------- -->
        <!-- 個人宛配信(終) -->
        <!-- -------- -->
        </div>
        <?php } ?>
    </div>
</div>

<?php
// 履歴表示の読み込み
if($to_user_id != ''){
    require './sp/page/notice_history_to_person.php';
} else {
    require './sp/page/notice_history_pager.php';
}
?>

<div id="user_list">
        <div class="container">
                <table id="user_list_table" width="100%" border="0" cellpadding="5" cellspacing="0">
                        <tr id="user_list_header" align="center">
								<th width="10%">顧客ID</th>
								<th width="20%">顧客名</th>
								<th width="20%">新規登録日</th>
								<th width="20%">最終スタンプ日</th>
								<th width="10%">スタンプ数</th>
								<th width="10%">累計スタンプ数</th>
                        </tr>
                        <?php
                        $count_user_list = count($user_list);
                        for ($i = 0; $i < $count_user_list; $i++) {
                        ?>
                        <tr class="user_list_data_tr">
                                <td align="center"><?php echo USER_ID.sprintf("%05d", $user_list[$i]['user_id']); ?></td>
                                <td><?php echo empty($user_list[$i]['user_name']) ? "-" : $user_list[$i]['user_name']; ?></td>
								<?php $add_date = str_replace("-", "/", $user_list[$i]['add_date']);?>
								<?php $sign_up_date = explode(" ", $add_date) ;?>
								<td align="center"><?php echo $sign_up_date[0]; ?></td>
								<?php $last_stamp_date = str_replace("-", "/", $user_list[$i]['last_stamp_date']);?>
								<?php $result_last_stamp_date = explode(" ", $last_stamp_date) ;?>
								<td align="center"><?php echo $result_last_stamp_date[0]; ?></td>
                                <td align="center"><?php echo $user_list[$i]['stamp_num'];?></td>
                                <td align="center"><?php echo $user_list[$i]['total_stamp_num'];?></td>
                        </tr>
                        <?php
                        }
                        ?>
                </table>
        </div>
</div>

<script>
//    $('#user_list').dialog({
//        autoOpen: false,
//        height: 300,
//        width: 320,
//        title: 'お知らせ配信対象一覧',
//        closeOnEscape: false,
//        modal: true,
//        position: [0,0],
//        buttons: {
//            "OK": function(){
//            $(this).dialog('close');
//            }
//        }
//    });
	var buttons_val = {};
	var button_name = ['閉じる','対象者更新'];
	var user_list_title = ['お知らせ配信対象一覧' , '詳細絞り込み'];
	var type_num = 0;
	function addCheckBoxToUserListPopup(){
		type_num = 1;
		changeButtonName();
	}
	function removeCheckBoxToUserListPopup(){
		type_num = 0;
		changeButtonName();
	}
	function changeButtonName(){
		buttons_val = {};
		console.log('type_num'+type_num);
		buttons_val[button_name[type_num]] = function(){
			if(type_num === 1){
				var exclusion_user_id = [];
				//var user_id = $('.user_id_checkbox').attr("id");
				var user_list_num = 0;
				$(".user_id_checkbox").each(function(i, elem) {
					if ($(elem).prop('checked')) {
						user_list_num++;
					} else {
						//console.log($(elem).attr("id")); 
						exclusion_user_id.push($(elem).attr("id"));
					}
				});
				$("#search_data").html(user_list_num);
				$("#delivery_num").val(user_list_num);
				console.log("exclusion_user_id"+exclusion_user_id);
				$('#exclusion_user_id').val(exclusion_user_id.join());
			}
			$(this).dialog('close');
			
			console.log($('#exclusion_user_id').val());
		}
		
		$('#user_list').dialog({
			autoOpen: false,
			height: 400,
			width: 600,
			title: user_list_title[type_num],
			closeOnEscape: false,
			modal: true,
			position: [0,0],
			buttons: buttons_val
		});
	}
	$('#user_list').dialog({
		autoOpen: false,
		height: 400,
		width: 880,
		title: 'お知らせ配信対象一覧',
		closeOnEscape: false,
		modal: true,
		position: [0,0],
		buttons: {
			"OK": function(){
			$(this).dialog('close');
			}
		}
	});
	
	$('#allCheck').click(function(){
		var items = $('.user_id_checkbox');
		if($(this).is(':checked')) { //全選択・全解除がcheckedだったら
			$(items).prop('checked', true); //アイテムを全部checkedにする
		} else { //全選択・全解除がcheckedじゃなかったら
			$(items).prop('checked', false); //アイテムを全部checkedはずす
		}
	});	
</script>

<?php
require './sp/footer.php';
?>