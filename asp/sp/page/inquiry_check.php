<?php
require './sp/header.php';
?>
<div class="box">
	<h2> <img src="images/icon6.gif" width="16" height="16"> お問い合わせ </h2>
	<div class="container">
		<ul>
			ご質問・お問い合わせ等ありましたらこちらからお願いします。
		</ul>
	</div>
	<div class="box">
		<div class="f_box">
			<form method="POST" action="./?p=inquiry" name="form2">
				<div class="container1">
					<h3> お名前 </h3>
					<ul class="clearfix">
						<li>
							<?php echo $people_name;?><input type="hidden" name="people_name"  value="<?php echo $people_name;?>">
						</li>
						<?php if($error['people_name'] == "ng"){ ?><span style=" color:#FF0000;" >※お名前を入力してください。</span><?php } ?>
					</ul>
				</div>
				<div class="container1">
					<h3> メールアドレス </h3>
					<ul class="clearfix">
						<li>
							<?php echo $mail_address;?><input type="hidden" name="mail_address"  value="<?php echo $mail_address;?>">
						</li>
						<?php if($error['mail_address'] == "ng"){ ?><span style=" color:#FF0000;" >※メールアドレスを入力してください。</span><?php } ?>
					</ul>
				</div>
				<div class="container1">
					<h3> 内容 </h3>
					<ul class="clearfix">
						<li>
							<?php echo $content;?><input type="hidden" name="content" class="f_text1" value="<?php echo $content;?>">
						</li>
					</ul>
					<div class="btn">
						<input type="submit" name="return" value="戻る"><?php if(empty($error)){?><input type="submit" name="transmission" value="送信"><?php }?>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
require './sp/footer.php';
?>