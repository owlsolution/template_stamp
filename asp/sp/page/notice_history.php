<div class="box">
        <h2> <img src="images/icon1.gif" width="16" height="16"> 配信履歴 </h2>
        <div class="container">
                <table width="100%" border="0" cellpadding="5" cellspacing="0">
                        <tr align="center">
                                <th width="15%">配信日</th>
                                <th width="30%">タイトル</th>
                                <th width="10%">状態</th>
                                <th width="15%"></th>
                        </tr>
                        <?php
                        $db = db_connect();
                        $send_admin_id = ADMIN_ID;
                        if ($_SESSION["branchFlag"] == 2) {
                            // 支店でログインしている場合
                            $send_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
                        } else if ($_SESSION["branchFlag"] == 1) {
                            // オーナーの場合
                            if ($_SESSION["branchId"] != 0) {
                                $send_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
                            }
                            else {
                                // 支店で送信したお知らせも対象に入れる
                                $branch_ids = getChildAdminIds($db, ADMIN_ID);
                                foreach ($branch_ids as $value) {
                                    $send_admin_id .= ",".$value;
                                }
                            }
                        }

						// 店員さんのadmin_idを追加
						$rally_date = rally_select($db , "admin_id = ".ADMIN_ID);
						$rally = mysql_fetch_array($rally_date);
						$where = "rally_id = '".$rally['rally_id']."' AND status = '1'";
						$staff_data = staff_select($db , $where);
						while ($staff = mysql_fetch_array($staff_data)){
							$send_admin_id .= ",".$staff['admin_id'];
						}

                        // PUSH送信状況を取得
                        $push_status = $noticeComp->get_push_status($send_admin_id);

                        $where = "admin_id IN( ".$send_admin_id.") ORDER BY notice_id DESC";
                        $notice_date = notice_select($db , $where);
                        while ($notice = mysql_fetch_array($notice_date)){
                        ?>
                        <tr>
                        <?php
                                $db = db_connect();
                                $where = "rally_id = ".$notice['rally_id'];
                                $rally_date = rally_select($db , $where);
                                $rally = mysql_fetch_array($rally_date);
                                $add_date_str = explode( ":", $notice['acceptance_datetime']);
                                $add_date_date = $add_date_str[0].":".$add_date_str[1];
                                $add_date_replace = str_replace("-", "/", $add_date_date);
                                
                                // ステータスチェック
                                // 送信中/送信済/予約
                                $status = "送信済";
                                foreach ($push_status as $value) {
                                    if ($value['notice_id'] == $notice['notice_id']) {
                                        // 合致した場合ステータスを参照
                                        if (($value['status'] == '0') || ($value['status'] == '1')) {
                                            $status = "送信中";
                                        } else if ($value['status'] == '4') {
                                            $status = "予約";
                                        }
                                    }
                                }
                                
                                ?>
                                <td align="center"><?php echo $add_date_replace; ?></td>
                                <td><?php echo $notice['notice_title']; ?></td>
                                <td align="center"><?php echo $status;?></td>
                                <td align="center"><p><a href="?p=notice&check_id=<?php echo $notice['notice_id'];?>">確認</a></p><p><a href="?p=notice&edit_id=<?php echo $notice['notice_id'];?>">編集</a></p> <a href="?p=notice&dele_id=<?php echo $notice['notice_id'];?>" onClick="return confirm('削除しますか?')">削除</a></td>
                        </tr>
                        <?php
                        }
                        ?>
                </table>
        </div>
</div>
