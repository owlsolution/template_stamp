<?php
require './sp/header.php';
?>

<style type="text/css">

.btn input {
	text-align:center;
	width:200px;
	background:#0092D6;
	padding:5px 0px;
	border:0px;
	border-radius: 15px;        /* CSS3草案 */
    -webkit-border-radius: 15px;    /* Safari,Google Chrome用 */
    -moz-border-radius: 15px;   /* Firefox用 */
	font-weight:bold;
	color:#FFF;
	margin:0px 10px;
}

</style>

<script type="text/javascript">
	$(function(){
	});
</script>

<div class="box">
	<h2> <img src="images/icon11.gif" width="16" height="16"> 予約受付確認 </h2>
    <ul>
        受付られた予約情報を確認できます。
    </ul>
	<br>
	
	<table width="100%"  border="0" cellpadding="0" cellspacing="0">
		<tr align="center">
			<th width="10%" colspan="2">名前</th>
			<td width="25%" align="center" valign="middle" colspan="2"><?= $reserve['name'] ?></td>
			<th width="10%">受付日時</th>
			<td width="20%" align="center" valign="middle"><?= $reserve['create_date'] ?></td>
		</tr>
		<tr align="center" bgcolor="#ffffff">
			<th width="10%" colspan="2">電話番号</th>
			<td width="25%" align="center" valign="middle" colspan="4"><?= $reserve['phone'] ?></td>
		</tr>
		<tr align="center" bgcolor="#ffffff">
			<th width="10%" colspan="2">受付先</th>
			<?php $branch = $adminModel->get_target_branch($reserve['branch_id'], $branch_list); ?>
			<td width="25%" align="center" valign="middle" colspan="4"><?= isset($branch['name']) ? $branch['name'] : '本部' ?></td>
		</tr>
		<tr align="center" bgcolor="#ffffff">
			<th colspan="2" >第１希望日時</th>
			<td  align="center" valign="middle" colspan="2"><?= $reserve['first_datetime'] == '0000-00-00 00:00:00' ? '-' : $reserve['first_datetime'] ?></td>
			<th width="10%">状態</th>
			<td width="25%" align="center" valign="middle"><?= ReserveModel::$STATUS_TEXTS[$reserve['status']]; ?></td>
		</tr>
		<tr align="center" bgcolor="#ffffff">
			<th colspan="2" >第２希望日時</th>
			<td  align="center" valign="middle" colspan="2"><?= $reserve['second_datetime'] == '0000-00-00 00:00:00' ? '-' : $reserve['second_datetime'] ?></td>
			<th  colspan="2" ></th>
		</tr>
		<tr align="center" bgcolor="#ffffff">
			<th colspan="2" >第３希望日時</th>
			<td  align="center" valign="middle" colspan="2"><?= $reserve['third_datetime'] == '0000-00-00 00:00:00' ? '-' : $reserve['third_datetime'] ?></td>
			<th  colspan="2" ></th>
		</tr>
		<tr align="center" bgcolor="#ffffff">
			<th colspan="6" >予約内容</th>
		</tr>
		<tr align="center" bgcolor="#ffffff">
			<tr align="center">
				<td height="150"  align="left" valign="top"colspan="6" >
					<?= nl2br($reserve['description']) ?>
				</td>
			</tr>
		</tr>
	</table>
	<br>
	<form method="get" action="./?p=reserve" name="form2" >
		<div class="btn">
			<input type="hidden" name="p" value="notice" />
			<input type="hidden" name="to_user_id" value="<?= $reserve['user_id']; ?>" />
			<input type="hidden" name="reserve_id" value="<?= $reserve['reserve_id']; ?>" />
			<input type="submit" name="rsvreply" value="お知らせ配信で返信する" />
		</div>
		<p style="color:red;text-align:center;margin-top: 10px">※お知らせ配信の個人宛配信ページへ遷移します。ご予約された方へ個人宛てにお知らせが発行できます。</p>
	</form>
	
</div>


<?php
// 対象ユーザのお問合せ履歴を表示する
    require './sp/page/reserve_history_to_person.php';
?>

<?php
require './sp/footer.php';
?>