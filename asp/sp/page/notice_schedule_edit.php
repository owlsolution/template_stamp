<?php
require './sp/header.php';
?>
<div class="box">
    <form id="form2" method="POST" action="./?p=notice<?php echo empty($edit_schedule_id) ? '' : "&edit_schedule_id=".$edit_schedule_id; ?>" name="form2" enctype="multipart/form-data">
		<input type="hidden" name="exclusion_user_id" id="exclusion_user_id" value="<?php echo $exclusion_user_id;?>">
		<h2> <img src="images/icon1.gif" width="16" height="16"> 配信機能 </h2>
		<div class="container">
			<ul>
				ここで、参加者へ各種お知らせ配信ができます。
				<p style="color: #FF0000">送信内容は下記の通りで宜しいですか？<br />宜しければ送信ボタンを押してください。</p>
			</ul>
			<div class="f_box">
				<div class="container1">
					<h3> 配信設定 </h3>
					<ul class="clearfix">
						<li>
							<?php echo $delivery_set_name;?><input type="hidden" name = "delivery_set" value = "<?php echo $delivery_set;?>" >
						</li>
					</ul>
				</div>
				
				<div class="container2" id="schedule_menu">
					<h3> スケジュール設定内容 </h3>
					<ul class="clearfix">
						<li class="clearfix">
							<p class="titel">・設定タイプ</p>
							<p class="text"><?php echo $schedule_type_name;?></p>
							<input type="hidden" name = "schedule_type" value = "<?php echo $schedule_type ; ?>" >
						</li>
						<li class="clearfix even">
							<p class="titel">・設定内容</p>
							<p class="text"><?php echo $schedule_type_comment;?></p>
							<input type="hidden" name = "new_user_time_difference" value = "<?php echo $new_user_time_difference ; ?>" >
							<input type="hidden" name = "new_user_day_difference" value = "<?php echo $new_user_day_difference ; ?>" >
							<input type="hidden" name = "last_stamp_after_day" value = "<?php echo $last_stamp_after_day ; ?>" >
							<input type="hidden" name = "total_stamps" value = "<?php echo $total_stamps ; ?>" >
							<input type="hidden" name = "total_stamp_after_day" value = "<?php echo $total_stamp_after_day ; ?>" >
							<input type="hidden" name = "before_day_of_birth" value = "<?php echo $before_day_of_birth ; ?>" >
							<input type="hidden" name = "day_of_the_week" value = "<?php echo $day_of_the_week ; ?>" >
							<input type="hidden" name = "monthly_push_day_of_the_month" value = "<?php echo $monthly_push_day_of_the_month ; ?>" >
							<input type="hidden" name = "last_month_stamp_num" value = "<?php echo $last_month_stamp_num ; ?>" >
							<input type="hidden" name = "last_month_push_day" value = "<?php echo $last_month_push_day ; ?>" >
						</li>
					</ul>
				</div>

				<div class="container2" id="schedule_status">
					<h3> 配信状態設定 </h3>
					<ul class="clearfix">
						<li class="clearfix">
								<input type="radio" name="schedule_status" value="0" <?php if($schedule_status == "0"){ echo "checked"; }?> > 実施　　　
								<input type="radio" name="schedule_status" value="1" <?php if($schedule_status == "1"){ echo "checked"; }?> > 停止　　　
						</li>
						<p class="titel"><span style="color:red;">&nbsp&nbsp※[停止]にすると配信されなくなります。一時的に配信を止めたい際などにご利用ください。</span></p>
					</ul>
				</div>

				<div class="container1">
					<h3> お知らせ形式 </h3>
					<ul class="clearfix">
						<?php
						if($format_no == '1'){
						?>
						<li>
							 プッシュ通知/メール送信
						</li>
						<?php
						} else if($format_no == '2'){
						?>
						<li>
							 プッシュ通知のみ
						</li>
						<?php
						} else if($format_no == '3'){
						?>
						<li>
							 メール送信のみ
						</li>
						<?php
						} else {
						?>
						<li>
							 形式未選択
						</li>
						<?php
						}
						?>
					</ul>
					<?php
					foreach ($format as $val) {
					?>
					<input type="hidden" name = "format[]" value = "<?php echo $val?>" >
					<?php
					}
					?>
				</div>
				
				<div class="container1">
					<h3> 配信時間設定 </h3>
						<p class="time_menu">
							&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $push_hour; ?>
							<input type="hidden" name="push_hour" value="<?php echo $push_hour; ?>">
							時 <?php echo $push_minute; ?>
							<input type="hidden" name="push_minute" value="<?php echo $push_minute; ?>">
							分
							</p>
						<p style="color:red; margin:10px;">※[新規入会後配信]の場合、配信時間指定は無視されます。</p>
					<br>
				</div>
			</div>
		</div>
		<div class="box">
			<div class="f_box">
				<div class="container1">
					<h3> タイトル (全角50文字以内) </h3>
					<ul class="clearfix">
						<li>
							<input name="notice_title" type="input" class="f_text1" value="<?php echo htmlspecialchars($schedule_title);?>">
						</li>
					</ul>
				</div>
				<div class="container1">
					<h3> 本文 </h3>
					<ul class="clearfix">
						<li>
							<?php 
								// 入力フォームに絵文字が入っていたらremoveEmoji関数で削除する
								$schedule_content = Util::removeEmoji($schedule_content);
							?>
							<input style="margin-bottom:10px;" onclick="$('#notice_image_up5').click();" type="button" value="ファイルを選択">
							<input type="file" name="notice_image" id="notice_image_up5" style="margin-bottom:0px;font-size: 0px;display:none;"><br>
							<textarea class="f_text2" name="notice_date" id="notice_date5" placeholder="配信の内容をここに入力してください。"><?php echo htmlspecialchars($schedule_content);?></textarea>
						</li>
					</ul>
					<div class="btn">
						<input id="edit" name="schedule_edit" type="submit" value="更新"> <input id="return"　name="return" type="submit" value="戻る" />
					</div>
				</div>
			</div>
		</div>
	</form>
	<?php
	// 確認のリンクを押下して遷移してきた場合のみ表示
	if(!empty($edit_schedule_id)){
		// 履歴表示の読み込み
			require './sp/page/notice_schedule_list.php';
	}
	?>
	
</div>

<?php
require './sp/footer.php';
?>

<script>
    // キャンセルボタンが押された場合
    $('#return').click(function(event){
        window.history.back();
        event.preventDefault();
    });

</script>
