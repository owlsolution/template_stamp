<?php
require './sp/header.php';
?>
<div class="box">
	<h2> <img src="images/icon6.gif" width="16" height="16"> お問い合わせ </h2>
	<div class="container">
		<ul>
			ご質問・お問い合わせ等ありましたらこちらからお願いします。
		</ul>
	</div>
	<div class="box">
		<div class="f_box">
			<form method="POST" action="./?p=inquiry" name="form2">
				<div class="container1">
					<h3> お名前 </h3>
					<ul class="clearfix">
						<li>
							<input type="text" name="people_name" class="f_text1" value="<?php echo $people_name;?>">
						</li>
					</ul>
				</div>
				<div class="container1">
					<h3> メールアドレス </h3>
					<ul class="clearfix">
						<li>
							<input type="text" name="mail_address" class="f_text1" value="<?php echo $mail_address;?>">
						</li>
					</ul>
				</div>
				<div class="container1">
					<h3> 内容 </h3>
					<ul class="clearfix">
						<li>
							<textarea name="content" class="f_text2"><?php echo $content;?></textarea>
						</li>
					</ul>
					<div class="btn">
						<input type="submit" name="input_check" value="確 認">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<?php
require './sp/footer.php';
?>