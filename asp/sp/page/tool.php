<?php
require './sp/header.php';
?>

<div class="box">
	<div id = "total_present_num" style = "display: none">15</div>
	<h2> <img src="./images/icon9.png" width="16" height="16"> ツール </h2>
	<div class="container">
		<ul>
			アプリを使う上で役立つツールがダウンロードできます。
		</ul>
		<div class="f_box">
				<div class="container1">
					<h3> 新規登録用QRコード </h3>
					<ul class="clearfix">
						<div align="center"><img src="http://chart.apis.google.com/chart?chld=H&chs=490x490&cht=qr&chl=<?= str_replace("&", "%26", $qr_code_data); ?>" width="200px" height="200px" alt="QRコード"></div>   
						<div class="btn">
							<a href="http://chart.apis.google.com/chart?chld=H&chs=490x490&cht=qr&chl=<?= str_replace("&", "%26", $qr_code_data); ?>" download="qrcode.png"><input value="ダウンロード"></a> 
						</div>  
					</ul>
				</div>

				<div class="container1">
					<h3> 新規登録用URL</h3>
						■こちらのURLをFacebookやLINEなどに記載する事で、スマートフォンからのアプリ誘導に役立ちます。<br><br>
						<ul class="clearfix">
							<input id="foo" type="text" size="30" value="<?= $qr_code_data; ?>">
						</ul>
				</div>
				
				<div class="container1">
					<h3> 最新の使用説明書</h3>
					■最新の取扱説明書のデータを閲覧することができます。<br><br>
					<div class="clearfix">
						<div align="center">
							<img src="../../../manual/manual_01.jpg" width="100%">
						</div>
						<div class="btn">
							<a href="../../../document/manual.pdf" download>
								<input value="ダウンロード">
							</a> 
						</div>
					</div>
				</div>
				<div class="container1">
					<h3> ポップデータ </h3>
					■来店されるお客様へアプリを導入いただく為に役立ちます。
					<ul class="clearfix">
					
				<?php if($ai_flag_off) : ?>
					・準備中です。
				<?php else: ?>
					<?php if ($A2_1) : ?>
						・A2表面　<a href="../../../manual/<?= FILE_NAME; ?>/a2_1.pdf" download>PDF</a> / <a href="../../../manual/<?= FILE_NAME; ?>/a2_1.ai" download>ai</a><br> 
					<?php endif; ?>
					<?php if ($A4_1) : ?>
						・A4表面　<a href="../../../manual/<?= FILE_NAME; ?>/a4_1.pdf" download>PDF</a> / <a href="../../../manual/<?= FILE_NAME; ?>/a4_1.ai" download>ai</a><br> 
					<?php endif; ?>
					<?php if ($A4_2) : ?>
						・A4裏面　<a href="../../../manual/<?= FILE_NAME; ?>/a4_2.pdf" download>PDF</a> / <a href="../../../manual/<?= FILE_NAME; ?>/a4_2.ai" download>ai</a><br> 
					<?php endif; ?>
					<?php if ($A6_1) : ?>
						・A6表面　<a href="../../../manual/<?= FILE_NAME; ?>/a6_1.pdf" download>PDF</a> / <a href="../../../manual/<?= FILE_NAME; ?>/a6_1.ai" download>ai</a><br> 
					<?php endif; ?>
					<?php if ($A6_2) : ?>
						・A6裏面　<a href="../../../manual/<?= FILE_NAME; ?>/a6_2.pdf" download>PDF</a> / <a href="../../../manual/<?= FILE_NAME; ?>/a6_2.ai" download>ai</a><br> 
					<?php endif; ?>
						</li>
				<?php endif; ?>
						
					</ul>
				</div>
		</div>
	</div>
</div>

<?php
require './sp/footer.php';
?>
