<?php
require './sp/header.php';
?>
<div class="box">
	<h2> <img src="images/icon10.gif" width="16" height="16"> 初回プレゼントクーポン </h2>
	<div id = "total_present_num" style = "display: none"><?php echo count($gift_coupon_array);?></div>
	<div class="container">
		<ul>
			ここで、参加者へ初回プレゼントクーポンをプレゼントできます。
		</ul>
		<div class="f_box">
			<form method="POST" action="./?p=first_present" name="form2">
				<div class="container1">
					<h3> クーポン名（28文字以内）</h3>
					<ul class="clearfix">
						<li>
							<input type="text" class="f_text1" name="limit_coupon_name" >
						</li>
					</ul>
				</div>
				<div class="container1">
					<h3> 有効期限設定 </h3>
					<ul class="clearfix">
						<p style="padding-left:10px;">
                                                        発行から
							<input type="text" size="10" name="coupon_deadline_day" > 日まで有効
						</p>
					</ul>
				</div>
				<div class="btn">
					<input type="submit" name="check" value="確 認" onclick="return presentDataCheck(document.form2.limit_coupon_name.value , 28 , 1);">
				</div>
			</form>
		</div>
	</div>
</div>
<div class="box">
	<h2> <img src="images/icon3.gif" width="16" height="16"> 配信履歴 </h2>
	<div class="container">
		<table width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr align="center">
				<th width="31%">クーポン名</th>
				<th width="5%">有効期限</th>
				<th width="8%">発行数</th>
				<th width="12%">利用数</th>
				<th width="10%"></th>
			</tr>
			<?php
			$db = db_connect();
			foreach ($gift_coupon_array as $value){
				//発行数を表示
				$where_issue_coupon = "coupon_id = ".$value['id'];
				$issue_coupon = get_firstcoupon_count($db , $where_issue_coupon);
				$issue_count = mysql_result($issue_coupon , 0 , "first_couopn");
				//利用数を表示
				$where_used_coupon = "coupon_id = ".$value['id']." AND get_coupon_state = 1";
				$used_coupon = get_firstcoupon_count($db , $where_used_coupon);
				$used_count = mysql_result($used_coupon , 0 , "first_couopn");
			?>
			<tr>
				<td><?php echo $value['coupon_name'];?></td>
				<td align="center"><?php echo $value['coupon_deadline_day'];?></td>
				<td align="center"><?php echo $issue_count;?></td>
				<td align="center"><?php echo $used_count;?>/<?php echo $issue_count;?></td>
				<td align="center"><a href="?p=first_present&dele_id=<?php echo $value['id'];?>" onClick="return confirm('削除しますか?')">削除</a></td>
			</tr>
			<?php
			}
			db_close( $db );
			?>
		</table>
	</div>
</div>
<?php
require './sp/footer.php';
?>