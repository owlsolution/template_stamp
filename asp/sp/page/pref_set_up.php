<?php
require './sp/header.php';
?>


<div class="box">
	<div class="container">
		<div class="f_box">
			<h2> <img src="images/icon10.gif" width="16" height="16"> 設定管理</h2>
			<br>
	<h3> スタンプ／クーポンの有効期限設定</h3>
	<form method="POST" action="?p=pref" name="form2">
		<ul>・スタンプ有効期限</ul>
		<ul>
			　ユーザーがスタンプを付与されてから <input name="stamp_day" type="text" value="<?php echo $stamp_day;?>" id="stamp_day" size="5" /> 日間　
		</ul><br />
		<ul>・クーポン有効期限</ul>
		<ul>
			　ユーザーがクーポンを取得してから <input name="goal_day" type="text" value="<?php echo $goal_day;?>" id="goal_day" size="5" /> 日間　
		</ul><br />
		<h3> スタンプ設定</h3>
		<ul>・１日１回のみスタンプを付与する</ul>
		<ul>
			<input type="radio" name="one_times_a_day" value="1" <?php if($one_times_a_day == 1 || $one_times_a_day == ""){?> checked <?php }?>> はい　
			<input type="radio" name="one_times_a_day" value="2" <?php if($one_times_a_day == 2){?> checked <?php }?>> いいえ
		</ul><br />
		<ul>・新規登録時スタンプ数</ul>
		<ul>
			<select name="new_stamp_num" id="new_stamp_num">
				<?php
				for($s=0;$s<21;$s++){
				?>
				<option value="<?php echo $s;?>" <?php if($s == $new_stamp_num){?> selected <?php }?>><?php echo $s;?></option>
				<?php
				}
				?>
			</select>個
		</ul>
		<ul style="font-size: 12px;">*新規登録時に、ユーザーにプレゼントするスタンプ数</ul>
		<br>
		<h3> プロフィール登録チェック</h3>
		<ul>・ユーザがプロフィールを登録しているかチェックをする</ul>
		<ul>
			<input type="radio" name="user_profile_force_check" value="1" <?php if($user_profile_force_check == 1 || $user_profile_force_check == ""){?> checked <?php }?>onClick="profile_force_flag('1')" > はい　
			<input type="radio" name="user_profile_force_check" value="2" <?php if($user_profile_force_check == 2){?> checked <?php }?> onClick="profile_force_flag('2')"> いいえ
		</ul>
		<br />
		<ul class="profile_force_area">・プロフィール登録要求度の設定</ul>
		<ul class="profile_force_area">
			【・ユーザがプロフィールを登録しているかチェックをする】設定が「はい」の場合、以下の設定が可能です。<br>
			<ul class="profile_force_area" style="font-size: 12px;">*プロフィール未入力の場合</ul>
			<input type="radio" class="profile_force" name="profile_force" value="2" <?php if($profile_force == 2){?> checked <?php }?> <?php echo $disabled;?>> アプリ起動時にプロフィール入力をポップアップで促す。<br>
			<input type="radio" class="profile_force" name="profile_force" value="3" <?php if($profile_force == 3){?> checked <?php }?> <?php echo $disabled;?>> クーポンを取得時にプロフィール入力をポップアップで促す。<br>
			<input type="radio" class="profile_force" name="profile_force" value="5" <?php if($profile_force == 5){?> checked <?php }?> <?php echo $disabled;?>> アプリ起動時とユーザーがクーポンを取得する際に、プロフィール入力をポップアップで促す。<br>
		</ul><br />

		<br />
		<div align="center">
			<p style="color:red; margin: 10px;"><?php echo $change_param_reslt;?></p>
			<ul class="btm_body">
				<div class="btn">
					<input name="change_param" type="submit" class="btm" value="変 更" />
				</div>
			</ul>
		</div>
	</form>
		</div>
	</div>
</div>
<div class="box">
	<div class="container">
		<div class="f_box">
			<h3> 店員設定 </h3>
				<ul class="clearfix">
					<a href="./?p=staff" style="margin: 0px 6px;color:blue;">【スタッフ一覧の追加/編集】</a>
				</ul>
				<ul style="font-size: 12px; color: red;">*店員の情報を追加したり、編集する場合は、上記のリンクからお願いします。</ul>
			<br>
			<h3> スタンプ／クーポン 期限切れ事前通知設定</h3>
				<ul></ul>
				<ul class="clearfix">
					<a href="./?p=push_set" style="margin: 0px 6px;color:blue;">【プッシュ通知時間設定】</a>
				</ul>
				<ul style="font-size: 12px; color: red;">*期限が切れる前に、事前に通知をしたい場合は、上記のリンクからお願いします。</ul>
			<br>
			<h3> 初回プレゼントクーポン設定</h3>
				<ul></ul>
				<ul class="clearfix">
					<a href="./?p=first_present" style="margin: 0px 6px;color:blue;">【初回プレゼントクーポン設定】</a>
				</ul>
				<ul style="font-size: 12px; color: red;">*新規インストール時のみ発行される、初回プレゼントクーポンの設定は、上記のリンクからお願いします。</ul>
			<br>
<!--			<h3> お知らせ配信SNS連携</h3>
				<ul></ul>
				<ul class="clearfix">
					<a href="./?p=sns_clear" style="margin: 0px 6px;color:blue;">リセット</a>
				</ul>
				<ul style="font-size: 12px; color: red;">アカウントとの連携をクリアします。</ul>
			<br>-->
		</div>
	</div>
</div>
<?php
require './sp/footer.php';
?>
<script>
	$(function(){
		profile_force_flag();
	});
	function profile_force_flag(flag) {
		radioCheck = $("input[name='user_profile_force_check']:checked").val();
		if(typeof flag == "undefined" && radioCheck == 2){
			$('.profile_force').attr("disabled", "disabled");
			$('.profile_force').removeAttr("checked", "checked");
			$('.profile_force_area').css("color", "silver");
		}else{
			if(flag == "1"){
				$('.profile_force').removeAttr("disabled", "disabled");
				$('.profile_force_area').css("color", "");
			}else if (flag == "2"){
				$('.profile_force').attr("disabled", "disabled");
				$('.profile_force').removeAttr("checked", "checked");
				$('.profile_force_area').css("color", "silver");
			}
		}
	}
</script>
