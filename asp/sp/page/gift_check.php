<?php
require './sp/header.php';
?>
<div class="box">
	<h2> <img src="images/icon3.gif" width="16" height="16"> プレゼントクーポン </h2>
	<div class="container">
		<ul>
			ここで、参加者へクーポンをプレゼントできます。
			<?php
			if(empty($check_id)){
			?>
			<p style="color: #FF0000">送信内容は下記の通りで宜しいですか？<br />宜しければ発行ボタンを押してください。</p>
			<?php
			}
			?>
		</ul>
		<div class="f_box">
			<form method="POST" action="./?p=gift<?php echo $to_user_id != '' ? "&to_user_id=".$to_user_id : ''; ?>" name="form2" enctype="multipart/form-data">
				<div class="container1">
					<h3> 配信設定 </h3>
					<ul class="clearfix">
						<li>
							<?php echo $delivery_set_name;?><input type="hidden" name = "delivery_set" value = "<?php echo $delivery_set;?>" >
						</li>
					</ul>
				</div>
				
				<?php
				if($delivery_set == "individual"){
				?>
				<div class="container2" id="individual_menu">
					<h3> 個別配信絞り込み </h3>
					<ul class="clearfix">
						<li class="clearfix">
							<p class="titel">・性別</p>
							<p class="text">
								<?php echo $individual_sex_date;?><input type="hidden" name = "individual_sex" value = "<?php echo $individual_sex;?>" >
							</p>
						</li>
						<li class="clearfix even">
							<p class="titel">・誕生日</p>
							<p class="text"><?php echo $individual_birthday_start ; ?> 月　〜　<?php echo $individual_birthday_end ; ?> 月</p>
							<input type="hidden" name = "individual_birthday_start" value = "<?php echo $individual_birthday_start ; ?>" >
							<input type="hidden" name = "individual_birthday_end" value = "<?php echo $individual_birthday_end ; ?>" >
						</li>
						<li class="clearfix">
							<p class="titel">・地域</p>
							<?php
							if(empty($individual_region)){
								$individual_string = "全ての都道府県";
							} else {
								$individual_string = $individual_region;
							}
							?>
							<p class="text"> <?php echo $individual_string;?> </p>
							<input type="hidden" name = "individual_region" value = "<?php echo $individual_region;?>" >
						</li>
						<li class="clearfix">
							<p class="titel">・最終来店日</p>
							<p class="text"> <?php echo $last_year_start;?> 年 <?php echo $last_month_start; ?> 月 <?php echo $last_day_start;?> 日　〜　 <?php echo $last_year_end;?> 年 <?php echo $last_month_end; ?> 月 <?php echo $last_day_end;?> 日 </p>
							<input type="hidden" name = "last_start" value = "<?php echo $last_year_start;?>-<?php echo $last_month_start; ?>-<?php echo $last_day_start;?>" >
							<input type="hidden" name = "last_end" value = "<?php echo $last_year_end;?>-<?php echo $last_month_end; ?>-<?php echo $last_day_end;?>" >
						</li>
						<li class="clearfix even">
							<p class="titel">・累計スタンプ数</p>
							<p class="text"><?php echo $total_stamp_num ; ?>個　<?php if($total_stamp_terms == "or_more"){ echo "以上";} else if($total_stamp_terms == "downward"){ echo "以下";}?></p>
							<input type="hidden" name = "total_stamp_num" value = "<?php echo $total_stamp_num;?>" >
							<input type="hidden" name = "total_stamp_terms" value = "<?php echo $total_stamp_terms;?>" >
						</li>
					</ul>
				</div>
				<?php
				}
				if($delivery_set == "simple"){
				?>
				<!-- 簡単配信 -->
				<div class="container1" id="simple_menu">
					<h3> お知らせしたい人 </h3>
					<ul class="clearfix">
						<?php
						if(in_array("last_month", $simple_check)){
						?>
						<li>
							 先月誕生日の人
						</li>
						<?php
						}
						if(in_array("this_month", $simple_check)){
						?>
						<li>
							 今月誕生日の人
						</li>
						<?php
						}
						if(in_array("next_month", $simple_check)){
						?>
						<li>
							 来月誕生日の人
						</li>
						<?php
						}
						if(in_array("man", $simple_check)){
						?>
						<li>
							男性
						</li>
						<?php
						}
						if(in_array("woman", $simple_check)){
						?>
						<li>
							 女性
						</li>
						<?php
						}
						if(in_array("yesterday_come", $simple_check)){
						?>
						<li>
							 昨日来店した人
						</li>
						<?php
						}
						if(in_array("month_come", $simple_check)){
						?>
						<li>
							 1ヶ月以上来店してないの人
						</li>
						<?php
						}
						if(in_array("this_month_come", $simple_check)){
						?>
						<li>
							 今月来店した人
						</li>
						<?php
						}
						?>
					</ul>
					<?php
					foreach ($simple_check as $val) {
					?>
					<input type="hidden" name = "simple_check[]" value = "<?php echo $val;?>" >
					<?php
					}
					?>

				</div>
				<?php
				}
				if($delivery_set != "individual" && $delivery_set != "simple"){
					foreach ($format as $val) {
					?>
					<input type="hidden" name = "format[]" value = "<?php echo $val?>" >
					<?php
					}
				}
				?>
		<div class="container1">
					<h3> 有効期限設定 </h3>
					<ul class="clearfix">
						<p style="padding-left:10px; margin-bottom:10px;">
							<input type="hidden" name="coupon_way" value="<?php echo $coupon_way;?>" >
							<?php echo $coupon_year_start;?>年<?php echo $coupon_month_start;?>月<?php echo $coupon_day_start;?>日<?php echo $coupon_hour_start;?>時<?php echo $coupon_minute_start;?>分　〜　<?php echo $coupon_year_end;?>年<?php echo $coupon_month_end;?>月<?php echo $coupon_day_end;?>日<?php echo $coupon_hour_end;?>時00分まで有効
							<input type="hidden" name="coupon_year_start" value="<?php echo $coupon_year_start;?>" >
							<input type="hidden" name="coupon_month_start" value="<?php echo $coupon_month_start;?>" >
							<input type="hidden" name="coupon_day_start" value="<?php echo $coupon_day_start;?>" >
							<input type="hidden" name="coupon_hour_start" value="<?php echo $coupon_hour_start;?>" >
							<input type="hidden" name="coupon_minute_start" value="<?php echo $coupon_minute_start;?>" >
							<input type="hidden" name="coupon_year_end" value="<?php echo $coupon_year_end;?>" >
							<input type="hidden" name="coupon_month_end" value="<?php echo $coupon_month_end;?>" >
							<input type="hidden" name="coupon_day_end" value="<?php echo $coupon_day_end;?>" >
							<input type="hidden" name="coupon_hour_end" value="<?php echo $coupon_hour_end;?>" >
						</p>
					</ul>
					<ul class="clearfix">
						<p style="padding-left:10px; margin-bottom:10px;">
							<?php if (($push_check == '1') || ($push_check == 'ready_push')) : ?>
								<input type="hidden" name="push_check" value="<?php echo $push_check;?>" >プッシュする
							<?php else: ?>
								<input type="hidden" name="push_check" value="<?php echo $push_check;?>" >プッシュしない
							<?php endif; ?>
						</p>
					</ul>
				</div>
				<div class="f_digit">
					<div class="titel"> お知らせを送る人数 </div>
					<div class="digit"><span id = "search_data"> <?php echo $delivery_num ;?> </span><strong>/<?php echo $all_customer?></strong> <input type="hidden" name = "delivery_num" id = "delivery_num" value = "<?php echo $delivery_num ;?>" > </div>
				</div>
				<div class="container1">
					<h3> クーポン名（全角７５文字以内）</h3>
					<ul class="clearfix">
						<li>
							<?php $limit_coupon_name = Util::removeEmoji($limit_coupon_name);?>
							<?php echo $limit_coupon_name;?><input type="hidden" name = "limit_coupon_name" value = "<?php echo $limit_coupon_name;?>" >
						</li>
					</ul>
				</div>
				<div class="container1">
					<h3> プレゼントクーポンを画像で発行する。</h3>
						<li><input name="present_up_img" type="hidden" value="<?php echo $present_up_img;?>" /></li>
						<li><input name="issue_present_coupon_check" type="hidden" value="<?php echo $coupon_isuue_flag;?>" /></li>
						<ul class="clearfix">
						<?php
						if($coupon_isuue_flag == "i"){
						?>
							<li><img src="./../img_coupon/<?php echo $present_up_img;?>" width="133"  height="104"><input name="present_up_img" type="hidden" value="<?php echo $present_up_img;?>" /></li>
						<?php
						} else {
						?>
							<li>画像なし</li>
						<?php
						}
						?>
						</ul>
				</div>
				<?php
				if(empty($check_id)){
				?>
				<div class="btn">
					<input name="setting" type="submit" value="発行"> <input name="return" type="submit" value="キャンセル" />
					<input type="hidden" name = "delivery_type" value = "<?php echo $delivery_type ?>" >
				</div>
				<?php
				}
				?>
			</form>
		</div>
	</div>
</div>
<?php
// 確認のリンクを押下して遷移してきた場合のみ表示
if(!empty($check_id)){
    // 履歴表示の読み込み
    if($to_user_id != ''){
        require './sp/page/gift_history_to_person.php';
    } else {
        require './sp/page/gift_history.php';
    }
}
?>
<?php
require './sp/footer.php';
?>
<script>
    // キャンセルボタンが押された場合
    $('#return').click(function(event){
        window.history.back();
        event.preventDefault();
    });

</script>
