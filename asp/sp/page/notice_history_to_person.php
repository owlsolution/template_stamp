<div class="box">
        <h2> <img src="images/icon1.gif" width="16" height="16"> 配信履歴 </h2>
        <div class="container">
                <table width="100%" border="0" cellpadding="5" cellspacing="0">
                        <tr align="center">
                                <th width="15%">配信日</th>
                                <th width="30%">タイトル</th>
                                <th width="10%">状態</th>
                                <th width="15%"></th>
                        </tr>
                        <?php
						$db = db_connect();
						// 支店機能 通常/オーナーはADMIN_ID ,支店権限の場合は支店のadmin_idで検索
						if ($_SESSION["branchFlag"] == OWNER) {
							$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, ADMIN_ID, OWNER);
						} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] == 0)){
							// 全支店管理アカウントで全支店管理アカウントモードの場合
							$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, ADMIN_ID, BRANCHES_OWNER);
						} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] != 0)){
							// 全支店管理アカウントで全支店アカウントを選択時は選択した支店管理アカウントに限定した機能
							$branch_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
							// 選択中の支店管理アカウントの配下のスタッフのadmin_idを取得する
							$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $branch_admin_id, BRANCH_MANAGER);
						} else if ($_SESSION["branchFlag"] == BRANCH_MANAGER) {
							// 支店管理アカウントでログインしている場合
							$branch_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
							// 支店管理アカウントにブラ下がるスタッフを取得
							$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $branch_admin_id, BRANCHES_OWNER);
						} else if (($_SESSION["branchFlag"] == ORG_MANAGER) && ($_SESSION["branchId"] != 0)){
							$branch_admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
							// 選択中の支店管理アカウントの配下のスタッフのadmin_idを取得する
							$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $branch_admin_id, BRANCH_MANAGER);
						} else if ($_SESSION["branchFlag"] == ORG_MANAGER) {
							// 組織管理アカウントなので、組織IDを元に組織にぶら下がる支店のadmin_idを取ってくる
							$org = $adminModel->get_organization_by_id($db, $_SESSION["orgId"]);
							// 組織IDからadmin_idを取得
							// admin_idとタイプを渡して、ぶら下がるadmin_idのリストを返却するメソッドを呼出す
							$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $org['admin_id'], ORG_MANAGER);
						} else if ($_SESSION["branchFlag"] == STAFF) {
							// スタッフのadmin_idを設定
							$staff = $adminModel->get_staff_by_id($db, $_SESSION["staffId"]);
							$admin_id_list[] = $staff['admin_id'];
						}
						$admin_id_list = array_filter($admin_id_list, "strlen");
						$send_admin_id = implode(',', $admin_id_list);
						
						// 全スタッフを取得
						$rally_id = $adminModel->get_rally_id($db, ADMIN_ID);
						$staff_data_list = $adminModel->get_staff_by_rally_id($db, $rally_id);
						
						// PUSH送信状況を取得
						$push_status = $noticeComp->get_push_status($send_admin_id);
						
						// ユーザへ見せるお知らせのIDのリストを取得する
						// notice_read_historyからユーザ宛になっているnotice_idを取得
						$where = "user_id = '".$to_user_id."' ";
						$notice_history_rows = notice_read_history_select($db, $where);
						$where_notice_id = "";
						while ($notice_history_row = mysql_fetch_array($notice_history_rows)){
							if (empty($where_notice_id)) {
								$where_notice_id = $notice_history_row['notice_id'];
							} else {
								$where_notice_id .= ", ".$notice_history_row['notice_id'];
							}
						}
                        $where = "notice_id IN( ".$where_notice_id.") ORDER BY notice_id DESC";
                        $notice_date = notice_select($db , $where);
                        
                        while ($notice = mysql_fetch_array($notice_date)){
                        ?>
                        <tr>
                                <?php
                                $db = db_connect();
                                $add_date_str = explode( ":", $notice['acceptance_datetime']);
                                $add_date_date = $add_date_str[0].":".$add_date_str[1];
                                $add_date_replace = str_replace("-", "/", $add_date_date);
                                
                                // ステータスチェック
                                // 送信中/送信済/予約
                                $status = "送信済";
                                foreach ($push_status as $value) {
                                    if ($value['notice_id'] == $notice['notice_id']) {
                                        // 合致した場合ステータスを参照
                                        if (($value['status'] == '0') || ($value['status'] == '1')) {
                                            $status = "送信中";
                                        } else if ($value['status'] == '4') {
                                            $status = "予約";
                                        }
                                    }
                                }
                                
                                ?>
                                <td align="center"><?php echo $add_date_replace; ?></td>
                                <td><?php echo $notice['notice_title']; ?></td>
                                <td align="center"><?php echo $status;?></td>
                                <td align="center"><p><a href="?p=notice&check_id=<?php echo $notice['notice_id'];?>&to_user_id=<?php echo $to_user_id?>">確認</a></p><p><a href="?p=notice&edit_id=<?php echo $notice['notice_id'];?>&to_user_id=<?php echo $to_user_id?>">編集</a></p><a href="?p=notice&dele_id=<?php echo $notice['notice_id'];?>&to_user_id=<?php echo $to_user_id?>" onClick="return confirm('削除しますか?')">削除</a></td>
                        </tr>
                        <?php
                        }
                        ?>
                </table>
        </div>
</div>
