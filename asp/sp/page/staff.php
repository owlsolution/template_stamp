<?php
require './sp/header.php';
?>
<div class="box">
	<form method="post" action="./?p=staff" name="form_admin_roll" >
		<h2> <img src="images/icon10.gif" width="16" height="16"> 管理者アカウント(アプリ連携)</h2>
		<br>
		・ここでは「お知らせ配信」「ブログ配信」に関する管理者の登録を行います。<br />
・ご登録いただく事で、管理画面にログインする事なくアプリから直接「お知らせ配信」「ブログ配信」「仮投稿管理」が行えるようになります。<br /><br />
		
		
		<li><strong>【使い方説明】</strong></li>
		①：下記の[編集]をクリックし管理対象者のスマートフォンにインストールしたアプリに記載されている「顧客ID」を入力して[更新]を押してください。
		<br />
		<p style="color:red; margin:5px;">※「顧客ID」は「その他」⇒「プロフィール」上部のID欄に記載の英数字です。（例：<?= USER_ID; ?>00000）</p>
		②：管理対象者のアプリの「店舗情報」コーナーの上部に、「仮投稿管理」、「お知らせ（ブログ）配信」ボタンが表示されます。<br />
		<br />
		<p style="color:#000000; margin:5px;"><strong>仮投稿管理</strong>：事前に仮投稿しておいた配信を[送信・編集・削除]する事ができます。また、下部の「店員設定（アプリ連携）」にて設定した担当者からの仮投稿も同様の処理を行えます。<br /></p>
		<strong>お知らせ（ブログ）配信</strong>：「お知らせ配信」や「ブログ配信」を行う事が出来ます。<br />
		<p style="color:red; margin:5px;">※個別配信並びにスケジュール配信の設定は行えません。</p>

		<?php if (!empty($admin_account_id)) : ?>
		<li><情報入力></li>
		管理アカウント名：<?= $account_name; ?>
		
		<div>
			顧客ID：<input name="account_user_id" type="text" id="user_id" size="20" value="<?php echo empty($account_user_id) ? '' : USER_ID.sprintf("%05d", $account_user_id) ?>" />
		</div>
			<div style="margin-top: 10px;">
				アイコン画像： <a herf="" id="admin_igm_clear" style="margin: 0px 6px;color:blue;">[設定解除]</a> 
			<br><input type="file" name="ic_image" id="admin_ic_image_up" style="margin-bottom:10px;" /><br>
						<img src="<?php echo "./../staff_images/".$admin_img_path ?>" class="admin_img_ic" style="<?php echo empty($admin_img_path)? "width:0px; height:0px;" : ""?>"><br>
						<input type="hidden" name="admin_ic_image_filename" id="admin_ic_image_filename" value="<?php echo $admin_img_path;?>" />
			</div>
		
		<div class="btn">
			<?php if (isset($error_message_admin)) : ?>
				<p style="color:red; margin:10px;">エラー:<?php echo $error_message_admin; ?></p>
			<?php endif;?>
			
			<input id="setting" name="adminupdate" type="submit" value="更 新" /><input name="return" type="submit" class="btm" value="戻 る" onclick="javascript:window.history.back(-1);return false;"/>
		</div>
		<?php endif;?>

		<br>
		<br>
			<table id="myTable" width="100%" border="0" cellpadding="5" cellspacing="0">
				<thead>
					<tr align="center">
						<th width="24%">名前</th>
						<th width="10%">アイコン画像</th>
						<th width="12%">顧客ID[アプリ連携用]</th>
						<th width="12%">操作<br></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($admin_list as $admin_account): ?>
				<tr bgcolor="#FFFFFF">
					<td align="center" style="padding: 3px;"><font style="font-size:15px;"><?php echo empty($admin_account['branch_name']) ? '' : '['.$admin_account['branch_name'].']' ?><?php echo $admin_account['name'];?></font></td>
					<td align="center" style="padding: 3px;"><img style="width: 60px; height: 60px;" src='<?php echo "./../staff_images/".(!empty($admin_account['img_path']) ? $admin_account['img_path']:"person.png");?>'></td>
					<td align="center" style="padding: 3px;"><font style="font-size:15px;"><?php echo empty($admin_account['user_id']) ? '未設定' : USER_ID.sprintf("%05d", $admin_account['user_id']) ?></font></td>
					<td align="center" style="padding: 3px;"><a href="?p=staff&admin_edit=<?php echo $admin_account['admin_id']?>">編集</a></td>
				</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		<input name="admin_edit_id" type="hidden"  id="admin_edit_id" value="<?php echo $admin_account_id ?>" />
	</form>
</div>
<div class="box">
	<form method="post" action="./?p=staff" name="form2" >
		<h2> <img src="images/icon10.gif" width="16" height="16"> 店員設定</h2>
		<br>
		<li><店員情報入力></li>
		名前：<input name="staff_name" type="text" id="staff_name" size="20" value="<?php echo $nickname ?>" />
		<?php if(count($branch_list) > 1) : ?>
		<span style="margin-left: 10px;padding:10px;">支店名：
				<?php if (empty($edit_id)){ ?>
				<select name="branch_id">
					  <option value="0"> - </option>
					  <?php foreach ($branch_list as $branch) { ?>
						  <option value='<?php echo $branch['id'];?>'><?php echo $branch['name'];?></option>
					  <?php } ?>
				</select>
				<?php } else { ?>
				<!-- 編集画面でセレクトタグに支店名を表示させて、選択できないようにする。 -->
				<select name="branch_id">
						<option value='<?php echo $branch_id;?>'><?php echo $selected_branch_name;?></option>
				</select>
				<?php } ?>
		</span>
		<?php endif;?>
		<p style="color:red;">※絵文字は非対応です。</p><br>
		<?php if($is_disp_entry_user_id): ?>
			<div>
				顧客ID：<input name="user_id" type="text" id="user_id" size="20" value="<?php echo empty($user_id) ? '' : USER_ID.sprintf("%05d", $user_id) ?>" />
				<p style="color:red;">※対象店員のアプリに記載されている「顧客ID」を入力する事で、[ブログの仮登録]が管理画面にログインする事なく行えるようになります。</p>
				<p style="color:red;">※[ブログの仮登録]ボタンは、[店舗情報]欄上部に表示されます。</p>
			</div>
		<?php endif;?>
		<div style="margin-top: 10px;">
			アイコン画像： <a herf="" id="igm_clear" style="margin: 0px 6px;color:blue;">[設定解除]</a> 
		<br><input type="file" name="ic_image" id="ic_image_up" style="margin-bottom:10px;" /><br>
					<img src="<?php echo "./../staff_images/".$img_path ?>" class="img_ic" style="<?php echo empty($img_path)? "width:0px; height:0px;" : ""?>"><br>
					<input type="hidden" name="ic_image_filename" id="ic_image_filename" value="<?php echo $img_path;?>" />
		</div>
        
			<div class="btn">
		<?php if (isset($error_message)) : ?>
			<p style="color:red; margin:10px;">エラー:<?php echo $error_message; ?></p>
		<?php endif;?>
				<?php if (empty($edit_id)){ ?>
					<input id="setting" name="setting" type="submit" value="追 加" /><input name="return" type="submit" class="btm" value="戻 る" onclick="javascript:window.history.back(-1);return false;"/>
				<?php } else {?>
					<input id="setting" name="update" type="submit" value="更 新" /><input name="return" type="submit" class="btm" value="戻 る" onclick="javascript:window.history.back(-1);return false;"/>
				<?php } ?>
			</div>

		<li><?php echo $message; ?></li>

		<input name="branch_no" type="hidden" value="<?php echo $branch_no?>" />
		<input name="edit_id" type="hidden"  id="edit_id" value="<?php echo $edit_id ?>" />
	</form><br><br>
			<table id="myTable" width="100%" border="0" cellpadding="5" cellspacing="0">
				<thead>
					<tr align="center">
						<th width="12%">名前</th>
						<th width="12%">支店名</th>
						<th width="10%">アイコン画像</th>
						<th width="12%">顧客ID[アプリ連携用]</th>
						<th width="12%">操作<br></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($staff_list as $staff): ?>
				<tr bgcolor="#FFFFFF">
					<td align="center" style="padding: 3px;"><font style="font-size:15px;"><?php echo $staff['nickname'];?></font></td>
					<td align="center" style="padding: 3px;"><font style="font-size:15px;"><?php echo $staff['branch_name'];?></font></td>
					<td align="center" style="padding: 3px;"><img style="width: 60px; height: 60px;" src='<?php echo "./../staff_images/".(!empty($staff['img_path']) ? $staff['img_path']:"person.png");?>'></td>
					<td align="center" style="padding: 3px;"><font style="font-size:15px;"><?php echo empty($staff['user_id']) ? '-' : USER_ID.sprintf("%05d", $staff['user_id']) ?></font></td>
					<td align="center" style="padding: 3px;"><a href="./?p=staff&delete=<?php echo $staff['admin_id'];?>" onClick="return confirm('このアカウントを削除されますと、過去に配信したブログ記事が削除されますが宜しいでしょうか？')">削除 </a>| <a href="?p=staff&edit=<?php echo $staff['admin_id']?>">編集</a></td>
				</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>

<script type="text/javascript">
	$(function(){
		// タブ生成
		$('#igm_clear').click(function(){
			// ファイルをクリア
			$('input[type=file]').val('');
			$('#ic_image_filename').val('');
			$('.img_ic').attr("src","./").css("width:0px; height:0px;");
		});
	});
</script>

<?php
require './sp/footer.php';
?>