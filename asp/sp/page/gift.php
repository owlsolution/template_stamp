<?php
require './sp/header.php';
?>
<style type="text/css">
.ui-tabs{ border: none; padding: 0px; }
.ui-tabs .ui-widget .ui-widget-content .ui-corner-all{
   border: none; padding: 0px;
}
.ui-widget-header{ background: #ffffff; border: none; }
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active .ui-state-focus {
   border: 1px solid #0092D6; background: #0092D6;
}
.ui-tabs-panel .ui-widget-content .ui-corner-bottom{
    background: #ffffff;
}
.ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited{
   color: #ffffff !important;
}
.ui-state-default a, .ui-state-default a:link, .ui-state-default a:visited{
   color: #c0c0c0;
}
.ui-state-hover a, .ui-state-hover a:hover{
   color: #0092D6;
}

.comment_area {
   background: #ffffff !important;
}

.ui-tabs .ui-tabs-panel {
    padding: 0px !important;
}
</style>

<script type="text/javascript">
    $(function(){
        // タブ生成
        $('#tabs').tabs();
        $('#dialog_link, ul#icons li').hover(
            function() { $(this).addClass('ui-state-hover'); },
            function() { $(this).removeClass('ui-state-hover'); }
        );
        // タブ表示
        $('#tabs').css("display","block");
    });
</script>

<div class="box">
	<div id = "total_present_num" style = "display: none"><?php echo $all_gift_coupon_num;?></div>
	<h2> <img src="images/icon3.gif" width="16" height="16"> プレゼントクーポン </h2>
	    <div id="tabs" style="display:none;">
	        <ul>
	            <?php if ($to_user_id == "") {  // 通常と個人宛で表示を変更する?>
<!--	            <li><a href="#tabs-1">プレゼントクーポン発行</a></li>-->
	            <?php } else { ?>
	           <li><a href="#tabs-4">個人宛プレゼント発行</a></li>
	            <?php } ?>
		    </ul>
		</div>
	<div class="container">
		<ul>
			ここで、参加者へクーポンをプレゼントできます。
		</ul>
		<div class="f_box">
		<?php if ($to_user_id == "") {  // 通常と個人宛で表示を変更する?>
			<form method="POST" action="./?p=gift" name="form2" enctype="multipart/form-data">
				<div class="container1">
                                        <h3> プレゼント配信設定 </h3>
                                        <ul class="clearfix">
                                                <li>
                                                        <input type="radio" name="delivery_set" value="all"  onclick="checkradio('all' , 'gift');" checked> 一斉配信
                                                </li>
                                                <li>
                                                        <input type="radio" name="delivery_set" value="simple" onclick="checkradio('simple' , 'gift');"> 簡単配信
                                                </li>
                                                <li>
                                                        <input type="radio" name="delivery_set" value="individual" onclick="checkradio('individual' , 'gift');"> 個別配信
                                                </li>
                                        </ul>
				</div>
                                <!-- 個別配信 -->
                                <div class="container2" id="individual_menu"  style="display:none">
                                        <h3> 個別配信絞り込み </h3>
                                        <ul class="clearfix">
                                                <li class="clearfix">
                                                        <p class="titel">・性別 <span style="color:red;">※1</span></p>
                                                        <p class="text">
                                                                <input type="radio" name="individual_sex" value="all" <?php if($individual_sex == "all"){ echo "checked"; }?> onclick="individual_check('gift')"> 全て　　　
                                                                <input type="radio" name="individual_sex" value="man" <?php if($individual_sex == "man"){ echo "checked"; }?> onclick="individual_check('gift')"> 男性　　　
                                                                <input type="radio" name="individual_sex" value="woman" <?php if($individual_sex == "woman"){ echo "checked"; }?> onclick="individual_check('gift')"> 女性
                                                        </p>
                                                </li>
                                                <li class="clearfix even">
                                                        <p class="titel">・誕生日 <span style="color:red;">※1</span></p>
                                                        <p class="text">
                                                                <select name="individual_birthday_start" onChange="individual_check('gift')">
                                                                        <?php
                                                                        for($i=1;$i<=12;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                月　〜　
                                                                <select name="individual_birthday_end" onChange="individual_check('gift')">
                                                                        <?php
                                                                        for($i=1;$i<=12;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>" <?php if($i == 12){ echo "selected";}?>><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                月
                                                        </p>
                                                </li>
                                                <li class="clearfix">
                                                        <p class="titel">・地域 <span style="color:red;">※1</span></p>
                                                        <p class="text">
                                                                <select name="individual_region" onChange="individual_check('gift')">
																	<option value=""><?php echo ALL_REGION_LABEL;?></option>
																	<?php
																		$region_group_list = json_decode(REGION_LIST,true);
																		foreach ($region_group_list as $group) {
																			// １つのグループを取り出す
																			if(!empty($group['gname'])) {
																				echo '<optgroup label="'.$group['gname'].'">';
																			}

																			// 地域アイテムのループ
																			$region_list = explode(",", $group['regions']);
																			foreach ( $region_list as $region_item) {
																				$elem = ($individual_region == $region_item) ? "selected" : "";
																				echo '<option value="'.$region_item.'" '.$elem.'>'.$region_item.'</option>';
																			}

																			if(!empty($group['gname'])) {
																				echo '</optgroup>';
																			}
																		}
																	?>
                                                                </select>
                                                        </p>
                                                </li>
                                                <li class="clearfix">
                                                        <p class="titel">・最終来店日</p>
                                                        <p class="text">
                                                                <select name="last_year_start" onChange="individual_check('gift')">
                                                                        <?php
                                                                        for($i=2013;$i<=date("Y");$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>" ><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                年
                                                                <select name="last_month_start" onChange="individual_check('gift')">
                                                                        <?php
                                                                        for($i=1;$i<=12;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                月
                                                                <select name="last_day_start" onChange="individual_check('gift')">
                                                                        <?php
                                                                        for($i=1;$i<=31;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                日　〜　
                                                                <select name="last_year_end" onChange="individual_check('gift')">
                                                                        <?php
                                                                        for($i=2013;$i<=date("Y");$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>" <?php if($i == date("Y")){ echo "selected";}?>><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                年
                                                                <select name="last_month_end" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=1;$i<=12;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>" <?php if($i == date("n")){ echo "selected";}?>><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                月
                                                                <select name="last_day_end" onChange="individual_check()">
                                                                        <?php
                                                                        for($i=1;$i<=31;$i++){
                                                                        ?>
                                                                        <option value="<?php echo $i;?>" <?php if($i == date("j")){ echo "selected";}?>><?php echo $i;?></option>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                </select>
                                                                日
                                                        </p>
                                                </li>
                                                <li class="clearfix even">
                                                        <p class="titel">・累計スタンプ数</p>
                                                        <p class="text">
                                                                <input type="text" name="total_stamp_num" id = "individual_total_stamp_num" size="10" onChange="individual_check('gift')" value="0"> 個　
                                                                <select name="total_stamp_terms" onChange="individual_check('gift')">
                                                                        <option value="or_more">以上</option>
                                                                        <option value="downward">以下</option>
                                                                </select>
                                                        </p>
                                                </li>
                                        </ul>
                                        <p style="color:red; margin:10px;">※1 参加者がプロフィール登録している場合のみ反映されます。</p>
                                </div>
                                <!-- 簡単配信 -->
                                <div class="container1" id="simple_menu" style="display:none">
                                        <h3> プレゼントを送りたい人を選んで下さい </h3>
                                        <ul class="clearfix">
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox1" value="last_month" onclick="simple_check('gift')"> 先月誕生日の人 <span style="color:red;">※1</span>
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox2" value="this_month" onclick="simple_check('gift')"> 今月誕生日の人 <span style="color:red;">※1</span>
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox3" value="next_month" onclick="simple_check('gift')"> 来月誕生日の人 <span style="color:red;">※1</span>
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox4" value="man" onclick="simple_check('gift')"> 男性 <span style="color:red;">※1</span>
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox5" value="woman" onclick="simple_check('gift')"> 女性 <span style="color:red;">※1</span>
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox6" value="yesterday_come" onclick="simple_check('gift')"> 昨日来店した人
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox7" value="month_come" onclick="simple_check('gift')"> 1ヶ月以上来店してない人
                                                </li>
                                                <li>
                                                        <input type="radio" name="simple_check[]" id="simple_checkbox8" value="this_month_come" onclick="simple_check('gift')"> 今月来店した人
                                                </li>
                                        </ul>
                                        <p style="color:red; margin:10px;">※1 参加者がプロフィール登録している場合のみ反映されます。</p>
                                </div>

                                <div class="f_digit">
                                        <div class="titel"> プレゼントを送る人数 </div>
                                        <div class="digit"><a href="#"><span id = "search_data" onclick="search_data('gift');"><?php echo $all_customer;?></span></a><strong>/<?php echo $all_customer?></strong> <input type="hidden" name = "delivery_num" id = "delivery_num"  value = "<?php echo $all_customer;?>" ></div>

                                </div>
								<p style="color:red; margin:10px;">※絵文字は非対応です。配信が正常に行われない可能性がありますのでご注意ください。<br>顔文字は問題ありません。</p>

				<div class="container1">
					<h3> クーポン名（全角75文字以内）</h3>
					<ul class="clearfix">
						<li>
							<input type="text" class="f_text1" name="limit_coupon_name">
						</li>
					</ul>
				</div>
				<!-- プレゼントクーポンを画像で発行する。 -->
				<div class="container1">
					<h3> プレゼントクーポンを画像で発行する。</h3>
					<p style="font-size: 12px; color:red; margin:10px;">※クーポンの画像サイズは「横600 x 縦500」でお願いします。</p>
					<div class="clearfix">
						<ul>
							<input type="radio" class="issue_present_coupon_check" name="issue_present_coupon_check" value="t" checked onClick="isuue_info_flag('t')"> テキストで発行する
							<input type="radio" class="issue_present_coupon_check" name="issue_present_coupon_check" value="i"  onClick="isuue_info_flag('i')"> 画像で発行する
						</ul>
						<br>
						<ul>
						<input type="file" class="present_up_img" id="present_up_img" name="present_up_img" size="30">
						</ul>
						<p style="font-size: 12px; color:red; margin:10px;">※選択する画像のファイル名は半角英数字、及び一部記号(注1)のみに限ります。(全角漢字/カナはNG)</p>
						<p style="font-size: 12px; color:red; margin:10px;">注1:ドット(.)、アンダーバー(_)、ハイフン(-)</p>
					</div>
				</div>
				<div class="container1">
					<h3> 有効期限設定 </h3>
					<ul class="clearfix">
						<p style="padding-left:10px; margin-bottom:10px;">
							<input type="radio" name="coupon_way" value="period" checked>　
							<select name="coupon_year_start">
							<?php
							for($i=date("Y");$i<=date("Y",strtotime("+2 year"));$i++){
							?>
								<option value="<?php echo $i;?>" <?php if($i == date("Y")){ echo "selected"; } ?>><?php echo $i;?></option>
							<?php
							}
							?>
							</select>
							年
							<select name="coupon_month_start">
								<?php
								for($i=1;$i<=12;$i++){
								?>
								<option value="<?php echo $i;?>" <?php if($i == date("n")){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
								}
								?>
							</select>
							月
							<select name="coupon_day_start">
								<?php
								for($i=1;$i<=31;$i++){
								?>
								<option value="<?php echo $i;?>" <?php if($i == date("j")){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
								}
								?>
							</select>
							日<br>
							<div style="margin-bottom: 10px"Align="center" ><select name="coupon_hour_start">
								<?php
								for($i=0;$i<=23;$i++){
								?>
								<option value="<?php echo $i;?>" <?php if($i == date("H")){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
								}
								?>
							</select>
							時
							<select name="coupon_minute_start">
								<option value="00">00</option>
								<option value="15">15</option>
								<option value="30">30</option>
								<option value="45">45</option>
							</select>
							分　〜　<br>
							</div>
							 <div style="margin-bottom: 10px"Align="center" ><select name="coupon_year_end">
							<?php
							for($i=date("Y");$i<=date("Y",strtotime("+2 year"));$i++){
							?>
								<option value="<?php echo $i;?>" <?php if($i == date("Y")){ echo "selected"; } ?>><?php echo $i;?></option>
							<?php
							}
							?>
							</select>
							年
							<select name="coupon_month_end">
								<?php
								for($i=1;$i<=12;$i++){
								?>
								<option value="<?php echo $i;?>" <?php if($i == date("n")){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
								}
								?>
							</select>
							月
							<select name="coupon_day_end">
								<?php
								for($i=1;$i<=31;$i++){
								?>
								<option value="<?php echo $i;?>" <?php if($i == date("j")){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
								}
								?>
							</select>
							日
							</div>
							<div style="margin-bottom: 10px;margin-left: 75px" ><select name="coupon_hour_end">
								<?php
								for($i=0;$i<=23;$i++){
								?>
								<option value="<?php echo $i;?>" <?php if($i == date("H")){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
								}
								?>
							</select>
							時
							00分まで有効
							</div>
						</p>
						<p style="padding-left:10px;">
							<input type="checkbox" name="push_check" value="ready_push" />　プッシュ通知をする
							<p style="font-size: 12px">　　　※チェックをつけると、プッシュ通知でユーザーにお知らせできます。</p>
						</p>
						<div class="container1" id="format_menu" style="display:none"></div>
					</ul>
				</div>
				<div class="btn">
					<input type="submit" name="check" value="確 認" onclick="return presentDataCheck(document.form2.limit_coupon_name.value , 28 , 10);">
				</div>
				<input type="hidden" name = "delivery_type" value = "gift" >
			</form>
			</div>
		</div>

		<?php } else { ?>
		<!--個人宛てプレゼントクーポン発行-->
		<form method="POST" action="./?p=gift&to_user_id=<?php echo $to_user_id;?>" name="form2" enctype="multipart/form-data">
			<p style="color:red; margin:10px;">※絵文字は非対応です。配信が正常に行われない可能性がありますのでご注意ください。<br>顔文字は問題ありません。</p>
			<div class="container1">
				<h3> クーポン名（全角75文字以内）</h3>
					<ul class="clearfix">
						<li>
							<input type="text" class="f_text1" name="limit_coupon_name">
						</li>
					</ul>
			</div>
			<!-- プレゼントクーポンを画像で発行する。 -->
			<div class="container1">
				<h3> プレゼントクーポンを画像で発行する。</h3>
				<p style="font-size: 12px; color:red; margin:10px;">※クーポンの画像サイズは「横600 x 縦500」でお願いします。</p>
				<div class="clearfix">
					<ul>
						<input type="radio" class="issue_present_coupon_check" name="issue_present_coupon_check" value="t" checked onClick="isuue_info_flag('t')"> テキストで発行する
						<input type="radio" class="issue_present_coupon_check" name="issue_present_coupon_check" value="i"  onClick="isuue_info_flag('i')"> 画像で発行する
					</ul>
					<br>
					<ul>
					<input type="file" class="present_up_img" id="present_up_img" name="present_up_img" size="30">
					</ul>
					<p style="font-size: 12px; color:red; margin:10px;">※選択する画像のファイル名は半角英数字、及び一部記号(注1)のみに限ります。(全角漢字/カナはNG)</p>
					<p style="font-size: 12px; color:red; margin:10px;">注1:ドット(.)、アンダーバー(_)、ハイフン(-)</p>
				</div>
			</div>
			<div class="container1">
				<h3> 有効期限設定 </h3>
					<ul class="clearfix">
						<p style="padding-left:10px; margin-bottom:10px;">
							<input type="radio" name="coupon_way" value="period" checked>　
							<select name="coupon_year_start">
							<?php
							for($i=date("Y");$i<=date("Y",strtotime("+2 year"));$i++){
							?>
								<option value="<?php echo $i;?>" <?php if($i == date("Y")){ echo "selected"; } ?>><?php echo $i;?></option>
							<?php
							}
							?>
							</select>
							年
							<select name="coupon_month_start">
								<?php
								for($i=1;$i<=12;$i++){
								?>
								<option value="<?php echo $i;?>" <?php if($i == date("n")){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
								}
								?>
							</select>
							月
							<select name="coupon_day_start">
								<?php
								for($i=1;$i<=31;$i++){
								?>
								<option value="<?php echo $i;?>" <?php if($i == date("j")){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
								}
								?>
							</select>
							日<br>
							<div style="margin-bottom: 10px"Align="center" ><select name="coupon_hour_start">
								<?php
								for($i=0;$i<=23;$i++){
								?>
								<option value="<?php echo $i;?>" <?php if($i == date("H")){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
								}
								?>
							</select>
							時
							<select name="coupon_minute_start">
								<option value="00">00</option>
								<option value="15">15</option>
								<option value="30">30</option>
								<option value="45">45</option>
							</select>
							分　〜　<br>
							</div>
							 <div style="margin-bottom: 10px"Align="center" ><select name="coupon_year_end">
							<?php
							for($i=date("Y");$i<=date("Y",strtotime("+2 year"));$i++){
							?>
								<option value="<?php echo $i;?>" <?php if($i == date("Y")){ echo "selected"; } ?>><?php echo $i;?></option>
							<?php
							}
							?>
							</select>
							年
							<select name="coupon_month_end">
								<?php
								for($i=1;$i<=12;$i++){
								?>
								<option value="<?php echo $i;?>" <?php if($i == date("n")){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
								}
								?>
							</select>
							月
							<select name="coupon_day_end">
								<?php
								for($i=1;$i<=31;$i++){
								?>
								<option value="<?php echo $i;?>" <?php if($i == date("j")){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
								}
								?>
							</select>
							日
							</div>
							<div style="margin-bottom: 10px;margin-left: 75px" ><select name="coupon_hour_end">
								<?php
								for($i=0;$i<=23;$i++){
								?>
								<option value="<?php echo $i;?>" <?php if($i == date("H")){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
								}
								?>
							</select>
							時
							00分まで有効
							</div>
						</p>
						<p style="padding-left:10px;">
							<input type="checkbox" name="push_check" value="ready_push" />　プッシュ通知をする
							<p style="font-size: 12px">　　　※チェックをつけると、プッシュ通知でユーザーにお知らせできます。</p>
						</p>
						<div class="container1" id="format_menu" style="display:none"></div>
					</ul>
				</div>
				<div class="btn">
					<input type="submit" name="check" value="確 認" onclick="return presentDataCheck(document.form2.limit_coupon_name.value , 75 , 10);">
				</div>
				<input type="hidden" name = "delivery_type" value = "to_person" >
				<input type="hidden" name="delivery_set" value="to_person">
			</div>
            </form>
		</div>
		<?php } ?>
</div>
<?php
// 履歴表示の読み込み
if($to_user_id != ''){
    require './sp/page/gift_history_to_person.php';
} else {
    require './sp/page/gift_history.php';
}
?>

<div id="user_list" style="display:none;">
        <div class="container">
                <table id="user_list_table" width="100%" border="0" cellpadding="5" cellspacing="0">
                        <tr align="center">
                                <th width="20%">顧客ID</th>
                                <th width="20%">顧客名</th>
                                <th width="20%">最終スタンプ日</th>
                                <th width="10%">スタンプ数</th>
                                <th width="10%">累計スタンプ数</th>
                        </tr>
                        <?php
                        $count_user_list = count($user_list);
                        for ($i = 0; $i < $count_user_list; $i++) {
                        ?>
                        <tr class="user_list_data_tr">
                                <td align="center"><?php echo USER_ID.sprintf("%05d", $user_list[$i]['user_id']); ?></td>
                                <td><?php echo empty($user_list[$i]['user_name']) ? "-" : $user_list[$i]['user_name']; ?></td>
								<?php $last_stamp_date = str_replace("-", "/", $user_list[$i]['last_stamp_date']);?>
								<?php $result_last_stamp_date = explode(" ", $last_stamp_date) ;?>
                                <td align="center"><?php echo $result_last_stamp_date[0]; ?></td>
                                <td align="center"><?php echo $user_list[$i]['stamp_num'];?></td>
                                <td align="center"><?php echo $user_list[$i]['total_stamp_num'];?></td>
                        </tr>
                        <?php
                        }
                        ?>
                </table>
        </div>
</div>
<script>
    $('#user_list').dialog({
        autoOpen: false,
        height: 300,
        width: 320,
        title: 'プレゼントクーポン発行対象一覧',
        closeOnEscape: false,
        modal: true,
        position: [0,0],
        buttons: {
            "OK": function(){
            $(this).dialog('close');
            }
        }
    });

    $('#search_data').click(function(){
            $('#user_list').dialog('open');
    });
	//テキストでプレゼントクーポンを発行する場合、ファイル選択を選択できないようにする。
	$(function(){
		isuue_info_flag();
	});
	function isuue_info_flag(flag) {
		radioCheck = $("input[name='issue_present_coupon_check']:checked").val();
		if(typeof flag == "undefined" && radioCheck == 't'){
			$('.present_up_img').attr("disabled", "disabled");
			$('.present_up_img').removeAttr("checked", "checked");
			$('.present_up_img').css("color", "silver");
		} else {
			if(flag == 't'){
				console.log('textチェック');
				$('.present_up_img').attr("disabled", "disabled");
				$('.present_up_img').removeAttr("checked", "checked");
				$('.present_up_img').css("color", "silver");
			} else {
				console.log('imageチェック');
				$('.present_up_img').removeAttr("disabled", "disabled");
				$('.present_up_img').css("color", "");
			}
		}
		
	}
</script>
<?php
require './sp/footer.php';
?>