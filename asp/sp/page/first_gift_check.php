<?php
require './sp/header.php';
?>
<div class="box">
	<h2> <img src="images/icon10.gif" width="16" height="16"> 初回プレゼントクーポン </h2>
	<div class="container">
		<ul>
			ここで、参加者へ初回プレゼントクーポンをプレゼントできます。
			<?php
			if(empty($check_id)){
			?>
			<p style="color: #FF0000">送信内容は下記の通りで宜しいですか？<br />宜しければ発行ボタンを押してください。</p>
			<?php
			}
			?>
		</ul>
		<div class="f_box">
			<form method="POST" action="./?p=first_present" name="form2">
				<div class="container1">
					<h3> クーポン名（28文字以内）</h3>
					<ul class="clearfix">
						<li>
							<?php echo $limit_coupon_name;?><input type="hidden" name = "limit_coupon_name" value = "<?php echo $limit_coupon_name;?>" >
						</li>
					</ul>
				</div>
				<div class="container1">
					<h3> 有効期限設定 </h3>
					<ul class="clearfix">
						<p style="padding-left:10px; margin-bottom:10px;">
							発行から<?php echo $coupon_deadline_day;?>日まで有効
							<input type="hidden" name="coupon_deadline_day" value="<?php echo $coupon_deadline_day;?>" >
						</p>
					</ul>
				</div>
				<?php
				if(empty($check_id)){
				?>
				<div class="btn">
					<input name="setting" type="submit" value="発行"> <input name="return" type="submit" value="キャンセル" />
				</div>
				<?php
				}
				?>
			</form>
		</div>
	</div>
</div>
<?php
if(!empty($check_id)){
?>
<div class="box">
	<h2> <img src="images/icon3.gif" width="16" height="16"> 配信履歴 </h2>
	<div class="container">
		<table width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr align="center">
				<th width="40%">クーポン名</th>
				<th width="10%">発行数</th>
				<th width="10%">利用数</th>
				<th width="15%"></th>
			</tr>
			<?php
			foreach ($gift_coupon_array as $value){
			?>
			<tr>
				<td><?php echo $value['coupon_name'];?></td>
				<td align="center"><?php echo $value['issue_num'];?></td>
				<td align="center"><?php echo $value['use_num'];?>/<?php echo $value['issue_num'];?></td>
				<td align="center"><a href="?p=first_present&dele_id=<?php echo $value['id'];?>" onClick="return confirm('削除しますか?')">削除</a></td>
			</tr>
			<?php
			}
			?>
		</table>
	</div>
</div>
<?php
}
?>
<?php
require './sp/footer.php';
?>