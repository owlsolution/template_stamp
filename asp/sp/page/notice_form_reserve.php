<div class="container1">
        <h3> 配信時間指定 </h3>
        <p class="titel">&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="checkbox" name="reserve_check" class="reserve_check" value="1" <?= empty($reserve_check) ? '' : 'checked' ?>>
        	チェックすると配信する時間を指定できます。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 12px;">※チェックしない場合、即時配信されます。
        </p>
        <br>
        <p class="time_menu">
                &nbsp;&nbsp;&nbsp;&nbsp;
				<?php
					$reserve_year = empty($reserve_year) ? date("Y") : $reserve_year;
					$reserve_month = empty($reserve_month) ? date("n") : $reserve_month;
					$reserve_day = empty($reserve_day) ? date("j") : $reserve_day;
					$reserve_hour = empty($reserve_hour) ? date("H") : $reserve_hour;
					$reserve_minute = empty($reserve_minute) ? '0' : $reserve_minute;
				?>
                <select name="reserve_year" class="reserve_year" disabled>
                        <?php
                        $current_year = date("Y");
                        for( $i=$current_year; $i <= ($current_year+2); $i++){
                        ?>
                        <option value="<?php echo $i;?>" <?php if($i == $reserve_year){ echo "selected";}?>><?php echo $i;?></option>
                        <?php
                        }
                        ?>
                </select>
                年
                <select name="reserve_month" class="reserve_month" disabled>
                        <?php
                        for($i=1;$i<=12;$i++){
                        ?>
                        <option value="<?php echo $i;?>" <?php if($i == $reserve_month){ echo "selected";}?>><?php echo $i;?></option>
                        <?php
                        }
                        ?>
                </select>
                月
                <select name="reserve_day" class="reserve_day" disabled>
                        <?php
                        for($i=1;$i<=31;$i++){
                        ?>
                        <option value="<?php echo $i;?>" <?php if($i == $reserve_day){ echo "selected";}?>><?php echo $i;?></option>
                        <?php
                        }
                        ?>
                </select>
                日　
                <select name="reserve_hour" class="reserve_hour" disabled>
                        <?php
                        for($i=0; $i<24; $i++){
                        ?>
                        <option value="<?php echo $i;?>" <?php if($i == $reserve_hour){ echo "selected";}?>><?php echo $i;?></option>
                        <?php
                        }
                        ?>
                </select>
                時 
                <select name="reserve_minute" class="reserve_minute" disabled>
                        <?php
                        $period = 15;
                        for($i=0; $i<=3; $i++){
                        ?>
                        <option value="<?php echo $i*$period;?>" <?php if($i*$period == $reserve_minute){ echo "selected";}?>><?php echo $i*$period;?></option>
                        <?php
                        }
                        ?>
                </select>
                分
        </p>
        <br>
</div>

<script>
$(function() {
    // 初期設定
    if ($('.reserve_check').is(':checked')) {
        // チェックされたら時間指定のメニューを有効化
        $('.reserve_year, .reserve_month, .reserve_day, .reserve_hour, .reserve_minute').removeAttr("disabled");
        // 別タブの予約チェックボックスもON
        $('.reserve_check').attr("checked", "checked");
        // 有効色にする
        $('.time_menu').css("color", "");
//        // SNS連動チェックはOFFにする
//        $('.sns_btn').removeAttr("checked");
//        // SNS連動チェックは無効にする
//        $('.sns_btn').prop('disabled',true);
//        $('.sns_text').css("color", "silver");
	} else {
        // チェックを外されたらメニューを無効化
        $('.reserve_year, .reserve_month, .reserve_day, .reserve_hour, .reserve_minute').attr("disabled", "disabled");
        // 別タブの予約チェックボックスも OFF
        $('.reserve_check').removeAttr("checked");
        // 無効色にする
        $('.time_menu').css("color", "silver");
//        // SNS連動チェックは有効にする
//        $('.sns_btn').removeAttr("disabled");
//        $('.sns_text').css("color", "");
	}

    // checkboxがチェック
    $('.reserve_check').change(function(){
        console.log("change.");
            if ($(this).is(':checked')) {
                // チェックされたら時間指定のメニューを有効化
                $('.reserve_year, .reserve_month, .reserve_day, .reserve_hour, .reserve_minute').removeAttr("disabled");
                // 別タブの予約チェックボックスもON
                $('.reserve_check').attr("checked", "checked");
                // 有効色にする
                $('.time_menu').css("color", "");
                
//                // SNS連動チェックはOFFにする
//                $('.sns_btn').removeAttr("checked");
//                // SNS連動チェックはOFFにする
//                $('.sns_btn').prop('disabled',true);
//                $('.sns_text').css("color", "silver");
				
            } else {
                // チェックを外されたらメニューを無効化
                $('.reserve_year, .reserve_month, .reserve_day, .reserve_hour, .reserve_minute').attr("disabled", "disabled");
                // 別タブの予約チェックボックスも OFF
                $('.reserve_check').removeAttr("checked");
                // 無効色にする
                $('.time_menu').css("color", "silver");
//                // SNS連動チェックは有効にする
//                $('.sns_btn').removeAttr("disabled");
//                $('.sns_text').css("color", "");
            }
    });
});
</script>
