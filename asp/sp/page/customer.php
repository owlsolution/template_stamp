<?php
$now_year = date('Y');
$rally_name_date = "全スタンプ";
require './sp/header.php';
?>
<div class="box">
	<h2> <img src="images/icon7.gif" width="16" height="16"> 顧客管理一覧 </h2>
	<div class="container">
		<ul>
			スタンプラリー参加者の情報が見れます。
		</ul>
		<div class="f_box">
			<form method="POST" action="./?p=customer" name="form2">
				<div class="container2">
					<h3> 絞り込み検索 </h3>
					<ul class="clearfix">
						<li class="clearfix">
							<p class="titel">・対象スタンプ</p>
							<p class="text"><?php echo rally_name_get(ADMIN_ID);?></p>
						</li>
						<li class="clearfix even">
							<p class="titel">・日付</p>
							<p class="text" align="center">
								<select name="conditions_select" id="select">
									<option value="0" <?php if(0 == $conditions_select){ echo "selected"; }?>>新規登録日</option>
									<option value="1" <?php if(1 == $conditions_select){ echo "selected"; }?>>最終ｽﾀﾝﾌﾟ日</option>
								</select>
								<br>
								<br>
								<select name="years_select" id="select">
								<?php
								for($i = 2013 ; $i <= $now_year ; $i++){
								?>
									<option value="<?php echo $i;?>" <?php if($i == $years_select){ echo "selected"; }?>><?php echo $i;?></option>
								<?php
								}
								?>
								</select>
								年
								<select name="month_select" id="select">
								<?php
								for($i = 1; $i <= 12 ; $i++){
								?>
									<option value="<?php echo $i;?>" <?php if($i == $month_select){ echo "selected"; }?>><?php echo $i;?></option>
								<?php
								}
								?>
								</select>
								月
								<select name="day_select" id="select">
								<?php
								for($i = 1; $i <= 31 ; $i++){
								?>
									<option value="<?php echo $i;?>" <?php if($i == $day_select){ echo "selected"; }?>><?php echo $i;?></option>
								<?php
								}
								?>
								</select>
								日<br>
								↓<br>
								<select name="end_years_select" id="select">
								<?php
								for($i = 2013; $i <= $now_year; $i++){
								?>
									<option value="<?php echo $i;?>" <?php if($i == $end_years_select){ echo "selected"; }?>><?php echo $i;?></option>
								<?php
								}
								?>
								</select>
								年
								<select name="end_month_select" id="select">
								<?php
								for($i = 1; $i <= 12 ; $i++){
								?>
									<option value="<?php echo $i;?>" <?php if($i == $end_month_select){ echo "selected"; }?>><?php echo $i;?></option>
								<?php
								}
								?>
								</select>
								月
								<select name="end_day_select" id="select">
								<?php
								for($i = 1; $i <= 31 ; $i++){
								?>
									<option value="<?php echo $i;?>" <?php if($i == $end_day_select){ echo "selected"; }?>><?php echo $i;?></option>
								<?php
								}
								?>
								</select>
								日
							</p>
						</li>
						<li class="clearfix">
							<p class="titel">・顧客ID検索 </p>
							<input type='text' id='search_user_id' name='search_user_id' size='18' value='<?php echo stripslashes($search_user_id); ?>'>
							※[顧客ID]で検索します。
						</li>
						<li class="clearfix even">
							<p class="titel">・キーワード検索 </p>
							<input type='text' id='search_keyword' name='search_keyword' size='28' value='<?php echo $keyword; ?>'>
							<br>※[顧客名][カルテ情報]をキーワード検索します。
						</li>
					</ul>
				</div>
				<div class="btn">
					<input name="search" type="submit" class="btm" value="検索" />　　<input name="reset" type="submit" class="btm" value="リセット" />
				</div>
			</form>
		</div>
	</div>
</div>
<div class="box">
	<h2> <img src="images/icon7.gif" width="16" height="16"> 検索結果 <?php if(!isset($_POST['search'])){ echo '(全表示)';}?></h2>
	<div class="container">
		<!--ページネーション-->
		<form method="GET" action="./?p=customer" name="user_sort">
			・ページ最大表示行数：
			<select name="search_user_num" id="search_user_num">
				<option id="search_num" value="100" <?php if(100 == $per_page){ echo "selected"; }?>>100</option>
				<option id="search_num" value="500" <?php if(500 == $per_page){ echo "selected"; }?>>500</option>
				<option id="search_num" value="1000"<?php if(1000 == $per_page){ echo "selected"; }?>>1000</option>
			</select>
		<br><br>
		<center>
			<?php
				echo paging($page, $total_page , $sort_num , $change_order , $per_page);
			?>
			<p class="page_message">　全<strong><?php echo $total_count;?></strong>件中 <strong><?php echo $record_index + 1;?>~<?php echo $record_index + count($larry_user_array);?></strong>件目を表示</p>
		</center>
		<?php
		if(isset($_POST['search'])){
		?>
		<li>検索条件【対象スタンプ：<?php echo rally_name_get(ADMIN_ID);?>　日付：<?php echo $years_select;?>年<?php echo $month_select;?>月<?php echo $day_select;?>日 〜 <?php echo $end_years_select;?>年<?php echo $end_month_select;?>月<?php echo $end_day_select;?>日　<?php if(0 == $conditions_select){echo "新規登録日" ;} else {echo "最終ｽﾀﾝﾌﾟ日" ;}?>】</li>
		<?php
		}
		?>
		<?php
		//<p style="padding:5px; font-size:10px;">▼ … 昇順　｜　▲ … 降順</p>
		?>
		<table id="myTable" width="100%" border="0" cellpadding="5" cellspacing="0">
			<thead>
				<tr align="center">
					<th width="20%">顧客ID</th>
					<th width="25%">顧客名</th>
					<th width="25%">新規登録日／<br>最終スタンプ日</th>
					<th width="25%"></th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach( $larry_user_array as $key => $value) {
					// 日付を表示する
					$add_date = Util::get_rally_of_day($value['add_date'] , $value['rally_id']);
					$last_stamp_date = Util::get_rally_of_day($value['last_stamp_date'] , $value['rally_id']);
					// 日付の比較をして<tr>タグの背景色を変える
					$last_login_date = Util::get_rally_of_day($value['last_login_date'] , $value['rally_id']);
					$color_type = getUserColor($last_login_date[0]."-".$last_login_date[1]."-".$last_login_date[2]);
					//最終スタンプ日
					$final_stamp_date = "";
					if($value['last_stamp_date'] == "0000-00-00 00:00:00"){
						$final_stamp_date = "-";
					} else {
						$final_stamp_date = $last_stamp_date[0]."/".$last_stamp_date[1]."/".$last_stamp_date[2];
					}
				?>
				    <tr bgcolor="<?php echo $color_type;?>">
					    <td align="center"><?php echo USER_ID.sprintf("%05d", $value['user_id']);?></td>
					    <td><?php echo empty($value['user_name']) ? "-" : $value['user_name'];?></td>
					    <td align="center"><?php echo $add_date[0]."/".$add_date[1]."/".$add_date[2];?><br><?php echo $final_stamp_date;?></td>
					    <td align="center" bgcolor="FFFFFF"><p><a href="?p=customer&user_id=<?php echo $value['user_id']?>&rally_id=<?php echo $value['rally_id'];?>">詳細</a></p><a href="./?p=customer&rally_id=<?php echo $value['rally_id'];?>&delete=<?php echo $value['user_id'];?>" onClick="return confirm('削除しますか?')">削除</a></td>
				    </tr>
				<?php
				}
				?>
			</tbody>
		</table>
		</form>
		<!--ページネーション-->
		<center>
			<?php
			echo paging($page, $total_page , $sort_num , $change_order , $per_page);
				?>
		</center>
	</div>
</div>
<script>
$(function($) {
  $('#search_user_num').change(function() {
	var search_num = $(this).val();
	var get_search_num = "./?p=customer&search_user_num="+search_num;
	location.href = location.href.split('?')[0]+get_search_num;
  });
});
</script>
<?php
require './sp/footer.php';
?>