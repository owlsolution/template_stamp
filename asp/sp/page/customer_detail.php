<?php
require './sp/header.php';
?>

<!-- jQuery UI -->
<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/start/jquery-ui.css" rel="stylesheet">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<div class="box">
	<h2> <img src="images/icon7.gif" width="16" height="16"> 顧客情報 </h2>
	
	<div class="container">
		<div class="clearfix" style="margin-bottom:10px;">
			<div>
				<br><br>
				<p style="margin-bottom:10px; border-bottom:#CCC 1px dotted; padding-bottom:5px;">顧客ID/ <font style="color:#06F;font-size:13px;"><?php echo USER_ID.sprintf("%05d", $user_id);?></font></p>
				<ul style="float: right;">
						<p><span style="float: right; margin-top: -10px;"><button class="btn" name="to_gift_btn" value="to_gift_btn" onclick="location.href='./?p=gift&to_user_id=<?php echo $user_id; ?>'"> ﾌﾟﾚｾﾞﾝﾄｸｰﾎﾟﾝ発行 </button></span></p><br><br>
						<p><span style="float: right; margin-top: -10px;"><button class="btn" name="to_notice_btn" value="to_notice_btn" onclick="location.href='./?p=notice&to_user_id=<?php echo $user_id; ?>'"> 個人宛にお知らせ </button></span></p><br><br>
						<p><span style="float: right; margin-top: -10px;"><button id="stamp_btn" class="btn" name="stamp_btn" value="stamp_btn" > 1ｽﾀﾝﾌﾟ付与 </button></span></p>
				</ul>
				<div id="show_dialog"></div>
				<p style="font-size:18px; font-weight:bold; margin-bottom:10px;"> <?php echo $detail_user['user_name'];?><strong style="font-size:14px; color:#999;">様</strong></p>
				<table width="100%"  border="0" cellpadding="0" cellspacing="0">
					<tr align="center">
						<th width="20%">性別</th>
						<td width="30%" align="center" valign="middle"><?php if($detail_user['sex'] == 1){ echo "男";} else if($detail_user['sex'] == 2){ echo "女";}?></td>
						<th width="20%">生年月日</th>
						<?php $birth_date = str_replace("-", "/", $detail_user['birth_date']);?>
						<td width="30%" align="center" valign="middle"><?php if($detail_user['birth_date'] != "" && $detail_user['birth_date'] != "0000-00-00"){ echo $birth_date;}?></td>
					</tr>
					<tr align="center" bgcolor="#ffffff">
						<th >地域</th>
						<td align="center" valign="middle"><?php echo $detail_user['region'];?></td>
						<th >新規登録<br>日</th>
						<!--日付を表示する。-->
						<?php $add_date = Util::get_rally_of_day($detail_user['add_date2'] , $detail_user['rally_id']);?>
						<td align="center" valign="middle"><?php echo $add_date[0]."/".$add_date[1]."/".$add_date[2];?></td>
					</tr>
					<tr align="center" bgcolor="#ffffff">
						<th>累計<br>ｽﾀﾝﾌﾟ数</th>
						<td  align="center" valign="middle"><?php echo $detail_user['total_stamp_num'];?></td>
						<th>現在<br>ｽﾀﾝﾌﾟ数</th>
						<td  align="center" valign="middle"><?php echo $detail_user['stamp_num'];?></td>
					</tr>
					<tr align="center" bgcolor="#ffffff">
						<th>最終<br>ｽﾀﾝﾌﾟ日</th>
						<!--日付を表示する。-->
						<?php $last_stamp_date = Util::get_rally_of_day($detail_user['last_stamp_date'] , $detail_user['rally_id']);?>
						<td  align="center" valign="middle"><?php echo $last_stamp_date[0]."/".$last_stamp_date[1]."/".$last_stamp_date[2];?></td>
						<th>累計取得<br>ｸｰﾎﾟﾝ数</th>
						<td  align="center" valign="middle"><?php echo $all_coupon;?></td>
					</tr>
					<tr align="center" bgcolor="#ffffff">
						<th>現在取得<br>ｸｰﾎﾟﾝ数</th>
						<td  align="center" valign="middle"><?php echo $now_coupon;?></td>
						<th></th>
						<td align="center" valign="middle"></td>
					</tr>
				</table>
			</div>
		</div>

		<br>
		<h2>カルテ情報</h2>
		<div class="box">
			<div class="container">
				<?php echo $karte; ?>
			</div>
		</div>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0" style="margin-bottom:10px;">
			<tr align="center" >
				<th width="100%" bgcolor="#ffb202">スタンプ履歴</th>
			</tr>
			<tr align="center" >
				<td align="left" valign="top">
					<ul class="log_box">
					<?php
					//スタンプ履歴
					$db = db_connect();
					$where = "user_id = ".$user_id." AND rally_id = ".$rally_id." ORDER BY stamp_history_id DESC";
					$stamp_history_date = stamp_history_select_date($db , $where);
					$count_hist = 1;
					while ($stamp_history = mysql_fetch_array($stamp_history_date)){
						$add_date_str = explode( ":", $stamp_history['add_date']);
						$add_date_date = $add_date_str[0].":".$add_date_str[1];
						$add_date_date = str_replace("-", "/", $add_date_date);
					?>
						<li><b><?php echo $add_date_date;?></b><br>スタンプ <?php echo $stamp_history['stamp_num'];?> 個付与</li>
					<?php
						$count_hist++;
					}
					db_close( $db );
					?>
					</ul>
				</td>
			</tr>
		</table>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0" >
			<tr align="center" >
				<th width="50%" bgcolor="#ffb202">クーポン発行履歴</td>
				<th width="50%" bgcolor="#ffb202">クーポン使用履歴</td>
			</tr>
			<tr align="center">
				<td width="400" align="left" valign="top">
					<ul class="log_box">
					<?php
					//景品履歴
					$db = db_connect();
					$where = "user_id = ".$user_id." AND rally_id = ".$rally_id."";
					$order = "get_coupon_id DESC";
					$get_coupon_date = get_coupon_select($db ,$where , $order);
					$get_coupon_num = 1;
					while ($get_coupon = mysql_fetch_array($get_coupon_date)){
						$acquisition_date_str = explode( ":", $get_coupon['acquisition_date']);
						$acquisition_date_date = $acquisition_date_str[0].":".$acquisition_date_str[1];
						$acquisition_date_date = str_replace("-", "/", $acquisition_date_date);
					?>
						<li><b><?php echo $acquisition_date_date;?></b><br>「<?php echo $get_coupon['get_coupon_name'];?>」クーポン取得</li>
					<?php
					}
					db_close( $db );
					?>
					</ul>
				</td>
				<td width="400"  align="left" valign="top">
					<ul class="log_box">
					<?php
					//景品履歴
					$db = db_connect();
					$where = "user_id = ".$user_id." AND rally_id = ".$rally_id." AND get_coupon_state = 1";
					$order = "get_coupon_id DESC";
					$get_coupon_date = get_coupon_select($db ,$where , $order);
					$get_coupon_num = 1;
					while ($get_coupon = mysql_fetch_array($get_coupon_date)){
						$use_date_str = explode( ":", $get_coupon['use_date']);
						$use_date_date = $use_date_str[0].":".$use_date_str[1];
						$use_date_date = str_replace("-", "/", $use_date_date);
					?>
						<li><b><?php echo $use_date_date;?></b><br>「<?php echo $get_coupon['get_coupon_name'];?>」クーポン使用
					</li>
					<?php
					}
					db_close( $db );
					?>
					</ul>
				</td>
			</tr>
		</table>
	</div>
</div>
<script>
$(function() {
    
    $('#stamp_btn').click(function(){
        console.log('stamp_btn');
        // スタンプ付与ボタンがクリックされた場合
        var strTitle = "スタンプ付与確認";
        var strCostrTitlemment = "OKするとスタンプを１個付与します。<br>スタンプを付与しますか?<br>";
        
        // 確認ダイアログを作成
        $( "#show_dialog" ).html(strCostrTitlemment);
        $( "#show_dialog" ).dialog({
                modal: true,
                title: strTitle,
                buttons: {
                    "OK": function() {
                        $( this ).dialog( "close" );
                        window.location.href='./?p=customer&stamp=1&user_id=<?php echo $user_id; ?>&rally_id=<?php echo $rally_id; ?>';
                    },
                    "キャンセル": function() {
                        $( this ).dialog( "close" );
                    }
                }
        });
    });

});
</script>


<?php
require './sp/footer.php';
?>