<?php
require_once('./../common/model/AdminModel.php');
$adminModel = new AdminModel();

$msg = "";
if(isset($_POST["login"])){
	$id = $_POST["id"];
	$pass = $_POST["pass"];
	if($id == "" || $pass == ""){
		$msg = input_error();
	} else {
		$db = db_connect();
		$admin_id =  login_check($db , $id , $pass);
		db_close( $db );
		
		if ($admin_id == 0) {
			// Admin権限
			$owner_admin_id = $admin_id;
			$_SESSION["branchFlag"] = 0;
		} else {
			$db = db_connect();
			$admin_type = $adminModel->get_admin_type($db, $admin_id);
			$_SESSION["branchFlag"] = $admin_type;
			if ($admin_type == OWNER) {
				// オーナーアカウント
				$owner_admin_id = $admin_id;
			} else if ($admin_type == BRANCHES_OWNER) {
				// 支店オーナーアカウント
				$owner_admin_id = $admin_id;
				// オーナーなので支店IDは0
				$_SESSION["branchId"] = 0;
			} else if ($admin_type == ORG_MANAGER) {
				// 組織アカウント
				$organization = $adminModel->get_organization_by_admin_id($db, $admin_id);
				if ($organization) {
					$owner_admin_id = $organization['owner_admin_id'];
					// 組織IDを保持
					$_SESSION["orgId"] = $organization['id'];
				} else {
					error_log("login org account unmatch. admin_id:".$admin_id);
					$owner_admin_id = -1;
				}
			} else if ($admin_type == BRANCH_MANAGER) {
				// 支店アカウント
				$owner_admin_id = getOwnerAdminId($db, $admin_id);
				// 支店IDを保持する
				$_SESSION["branchId"] = getBranchIdByChildAdminId($db, $admin_id);
			} else if ($admin_type == STAFF) {
				// STAFF
				// staff_idを保持する
				$staff = $adminModel->get_staff_by_admin_id($db, $admin_id);
				if (!empty($staff)) {
					$_SESSION["staffId"] = $staff['id'];
				} else {
					error_log("login staff account unmatch. admin_id:".$admin_id);
					$owner_admin_id = -1;
				}
			}
			db_close($db);
//			// ログインアカウントの属性をチェック
//			if ($_SESSION["branchFlag"] != 3) {
//				// loginアカウントが支店用Adminの場合、オーナーのIDを取得して
//				// オーナーのadmin_idでログインする
//				$db = db_connect();
//				$owner_admin_id = getOwnerAdminId($db, $login);
//				if (!empty($owner_admin_id)) {
//					// 親がいるので支店
//				} else {
//					// 親がいないので、自身がオーナー
//					$owner_admin_id = $login;
//					
//					// 自身が支店を持つオーナー or １店舗のみなので支店がぶら下がっているかでどちらかを判定する
//					$child_list = getChildAdminIds ($db , $owner_admin_id);
//					
//					if(count($child_list) == 0) {
//						// 子がいないので、１店舗のみ（支店機能なし）
//						$_SESSION["branchFlag"] = 0;
//					} else {
//						// 子がいるので支店機能のオーナー
//						$_SESSION["branchId"] = 0;
//						$_SESSION["branchFlag"] = 1;
//					}
//				}
//				db_close( $db );
//			} else {
//				// スタッフ
//			}
		}
		
//		echo '$login:'.$login."<br>";
//		echo '$owner_admin_id:'.$owner_admin_id."<br>";
//		echo '$_SESSION["branchId"]:'.$_SESSION["branchId"]."<br>";
//		echo '$_SESSION["branchFlag"]:'.$_SESSION["branchFlag"]."<br>";
		
		if(($owner_admin_id != "" && $owner_admin_id == 0) || ($owner_admin_id != "" && $owner_admin_id == ADMIN_ID)){
			$now_time = date('Y-m-d');
			$db = db_connect();
			login_up($db , $admin_id , $now_time);
			db_close( $db );
			$auto = $_POST['auto_login'];
			if($auto == "on"){
				setcookie("userName" , $owner_admin_id);
			}
			$user = user_set($owner_admin_id);
			header('Location:index.php');
		} else {
			$msg = id_pass_error();
		}
	}
}

include './sp/login_header.php';
?>
<div id="container_all">
	<div class="box" style="width:280px; margin:0px auto; margin-top:30px;">
		<div align="center" style="margin-bottom:20px;">
			<img src="images/login.gif" width="150" height="150">
		</div>
		<form method="POST" action="./">
			<div class="nwes_digit_r">
				<ul>
					<li class="clearfix" style="padding-top:0px; margin-bottom:10px;">
						<?php echo $msg ;?>
					</li>
					<li class="clearfix" style="padding-top:0px; margin-bottom:10px;">
						<p class="time">ユーザーID</p>
						<p class="text"><input type="text" name="id" class="f_text1" style="width:100%;"></p>
					</li>
					<li class="clearfix" style="padding-top:0px; margin-bottom:10px;">
						<p class="time">パスワード</p>
						<p class="text"><input type="password" name="pass" class="f_text1" style="width:100%;"></p>
					</li>
				</ul>
				<div class="btn">
					<input type="submit" name="login" value="ログイン">
				</div>
			</div>
		</form>
	</div>
</div>
<?php include './sp/login_footer.php';?>