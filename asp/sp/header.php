<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.5">
<link rel="stylesheet" href="./css/sp_style.css?ver=1.0" />
<link rel="stylesheet" href="./css/jquery-ui-1.10.4.custom.css" />
<script type="text/javascript" src="./js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="./js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="./js/jquery.upload-1.0.2.js"></script>
<script type="text/javascript" src="./js/jquery-ui-1.10.4.custom.min.js"></script>
<script type="text/javascript" src="./js/jquery.blockUI.js?ver=6.02"></script>
<script type="text/javascript" src="./js/script.js?ver=6.02"></script>
<title>OWLSTAMAPアプリ管理画面</title>
</head>
<body>
<div id="container_all">
	<div id="header" class="clearfix">
		<h1>
			<img src="images/logo.png" width="100%">
		</h1>
			<form method="post" action="./index.php?p=<?php echo $p ?>">
				<div class="shop_nemu">
				<p style="text-align: right;"><a href="./?p=inquiry">&#9993;お問合せ</a></p>
				<?php if ($_SESSION["branchFlag"] == BRANCH_MANAGER) : ?>
					店舗名 : <input type="text" name="name" size="40" value="<?php echo (empty($org['name']) ? "[".STAMP_RALLY_NAME."]" : $org['name'])." ".$branch_info['name'];?>">
				<?php elseif ($_SESSION["branchFlag"] == BRANCHES_OWNER) : ?>
					<?php
						$db = db_connect();
						$branch_list = $adminModel->get_branch_by_owner_admin_id($db, ADMIN_ID); 
						db_close($db);
					?>
					店舗名 : <select name="branch" onchange="submit(this.form)">
							<option value="0">全て</option>
								<?php foreach ($branch_list as $branch) { ?>
								<option value='<?php echo $branch['id']; ?>' <?php echo $_SESSION["branchId"] == $branch['id'] ? 'selected' : ''; ?>><?php echo $branch['name']; ?></option>
								<?php } ?>
							</select>
				<?php elseif ($_SESSION["branchFlag"] == ORG_MANAGER) : ?>
					<?php
						$db = db_connect();
						$branch_list = $adminModel->get_branch_by_organization_id($db, $_SESSION["orgId"]); 
						db_close($db);
					?>
					店舗名 : <select name="branch" onchange="submit(this.form)">
							<option value="0"><?php echo $org['short_name']; ?> 全て</option>
								<?php foreach ($branch_list as $branch) { ?>
								<option value='<?php echo $branch['id']; ?>' <?php echo $_SESSION["branchId"] == $branch['id'] ? 'selected' : ''; ?>><?php echo $org['short_name']." ".$branch['name']; ?></option>
								<?php } ?>
							</select>
				<?php elseif ($_SESSION["branchFlag"] == STAFF) : ?>
					店舗名 : <input type="text" name="name" size="40" value="<?php echo STAMP_RALLY_NAME." ".$staff['nickname'];?>">
				<?php else: ?>
					店舗名 : <input type="text" name="name" size="40" value="<?php echo STAMP_RALLY_NAME;?>">
				<?php endif; ?>
			</form>
	</div>
	<div id="menu_box" class="box">
		<ul class="clearfix">
			<li>
				<a href="./">
					<img src="images/menu1.png" width="100%">
				</a>
			</li>
			<li>
				<a href="./?p=notice">
					<img src="images/menu2.png" width="100%">
				</a>
			</li>
			<li>
				<a href="./?p=customer">
					<img src="images/menu4.png" width="100%">
				</a>
			</li>
			<li>
				<a href="./?p=gift">
					<img src="images/menu6.png" width="100%">
				</a>
			</li>
			<li>
				<a href="./?p=tool">
					<img src="images/menu7.png?ver=1.0" width="100%">
				</a>
			</li>
			<li>
			<?php if( defined ('RESERVE_MODE') && RESERVE_MODE == '1' ) : ?>
				<a href="./?p=inquiry">
					<img src="images/menu8.png" width="100%">
				</a>
			<?php else : ?>
				<a href="./?p=reserve">
					<img src="images/menu11.png" width="100%">
				</a>
			<?php endif;?>
			</li>
			<li>
				<a href="./?p=pref">
					<img src="images/menu10.png" width="100%">
				</a>
			</li>
			<li>
				<a href="./?p=out">
					<img src="images/menu9.png" width="100%">
				</a>
			</li>
		</ul>
	</div>