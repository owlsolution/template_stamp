(function($){
    $.fn.pager = function(options){
        
        var settings = $.extend( {
            'page_max' : 10
            ,'row' : ['']
            ,'th'  : ['']
        }, options); 
 
/*
 * データの読み込み
 */
    var reading = function(div,nowpage){ 
    
        // 
        var push_type = $('.push_type:checked').map(function() {
          return $(this).val();
        }).get();
        
        var page_max = settings.page_max;
        
        var start = 0;
        if( nowpage > 1 ){
            start = (page_max*nowpage) - page_max+1; //2ページ目以降は次の開始位置を計算
        }
        var position = (start == 0) ? 1 : start;
        
        console.log("page_max="+page_max+"&start="+start);
        console.log(push_type);
        $.ajax({
          type: "POST",
          url: "./sys_controller/NoticePager.php",
          data: {
              "page_max":page_max,
              "start":start,
              "push_type":push_type,
          },
          async:false,
          success: function(res){
              $(".notice_list_row").remove();
              if (res.length > 0){
                  var jsonpData = JSON.parse(res);
                  var len = jsonpData.notice_total_num;
                  var noticedata = jsonpData.data;
                  console.log(len);
                  console.log(noticedata.length);
                  for(var i = 0; i < noticedata.length; i++){
                        // おしらせ配信履歴へ追加する
                      $("#notice_list_table").append('<tr class="notice_list_row">'+noticedata[i]+'</tr>');
                  }
                  
                  page_icon (nowpage,len);
                  var len = len.toString().replace( /([0-9]+?)(?=(?:[0-9]{3})+$)/g , '$1,' ); //3桁ごとにカンマ 全483件中 1～20件を表示
                  if (len > 0) {
                      $("#ajax_pager").append('<p class="page_message">　全<strong>'+len+'</strong>件中 <strong>'+position+"~"+(position+noticedata.length-1)+'</strong>件を表示</p>');
                  } else {
                      $("#ajax_pager").append('<p class="page_message">　全<strong>'+len+'</strong>件');
                  }
              }
          },
          // 通信失敗時の処理
          error: function(xhr, textStatus, error) {
              console.log("error");
          }
        });

    };
    
/*
 * ページアイコンの作成
 */
    var page_icon = function(nowpage,len){ 

      var p = 1; //ページ開始位置

    //7ページ以上ならば、開始ページを1加算
      if(nowpage > 6){
        p = nowpage-5;
      }

//      $("#ajax_pager").html('<img src="loader2.gif" />'); 
      
      var page = len/settings.page_max; //全件数をページ表示件数で割る
          page = Math.ceil(page); //小数点切り上げ
      
      if (page == 0) {
          page = 1;
      }
      console.log("page:"+page);
      var html = '';
      
        if( nowpage > 1){
          var backpage = nowpage-1; //前のページに戻る数
          html += '<a href="#"  id="page1">&lt;&lt;最初へ</a>';
          html += '<a href="#"  id="page'+backpage+'">&lt;前へ</a>';
        }
          
          for(var i=0; i<6; i++){ //最大6ページ分のアイコン
            if(nowpage == p){
              html += '<p class="nowpage">'+p+'</p>'; //現在のページならば
            }else{
              html += '<a href="#" id="page'+p+'">'+p+'</a>';
            }
            if(p >= page) break; //ページ数より大きくなったら停止
            p++;
          }
          
          var nextpage = nowpage+1;
          var endpage  = page;
          
          if( endpage != nowpage){
            html += '<a href="#"  id="page'+nextpage+'">次へ&gt;</a>';
            if(page >5) html += '<a href="#" id="page'+endpage+'">最後へ&gt;&gt;</a>'; //6ページ以上あるならば
          }
          
        $("#ajax_pager").html(html); //ページアイコンの表示
    };
    
/*
 * ページをクリックしたら
 */
    $(document).on('click','#ajax_pager a',function(e){
      var page= $(this).attr("id");
          page = page.match(/page(\d+)/)[1]|0; //page123 からpageを除去*数値にする
          reading(this,page);
      e.preventDefault();
    });
    
    
/*
 * 履歴のタイプをクリックした場合
 */
    $(document).on('click','.push_type',function(e){
        console.log("click push_type");
        reading("#pager",1);
    });
    
/*
 * <div id="pager"> 以下に要素の追加
 */
    $("#pager").prepend('<div id="ajax_pager"></div>');
    reading("#pager",1);
};

})(jQuery)