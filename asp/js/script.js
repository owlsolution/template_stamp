/*
 * ソートの記述
 */
$(document).ready(function(){
	$("#myTable").tablesorter();
});

/*
 * 画像アップロード記述
 */
var toDoubleDigits = function(num) {
  num += "";
  if (num.length === 1) {
    num = "0" + num;
  }
 return num;
};

// 日付をYYYY/MM/DD HH:DD:MI:SS形式で取得
var yyyymmddhhmiss = function() {
	var date = new Date();
	var yyyy = date.getFullYear();
	var mm = toDoubleDigits(date.getMonth() + 1);
	var dd = toDoubleDigits(date.getDate());
	return yyyy + '-' + mm + '-' + dd;
};
jQuery(function($){
	// スタッフのサムネイル設定
	$('#ic_image_up').change(function() {
		$('#ic_image_up').upload('./sys_controller/StaffUploadController.php', function(res) {
                        if (res.length>0){
                                var jsonpData = JSON.parse(res);
                                $(".img_ic").attr("src","./../staff_images/"+ jsonpData.filename);
                                $(".img_ic" ).css( "width", "" );
                                $(".img_ic" ).css( "height", "" );
                                $("#ic_image_filename").attr("value",jsonpData.filename);
                        }
		}, 'html');
	});
	// 管理アカウント側のサムネイル設定
	$('#admin_ic_image_up').change(function() {
		$('#admin_ic_image_up').upload('./sys_controller/StaffUploadController.php', function(res) {
                        if (res.length>0){
                                var jsonpData = JSON.parse(res);
                                $(".admin_img_ic").attr("src","./../staff_images/"+ jsonpData.filename);
                                $(".admin_img_ic" ).css( "width", "" );
                                $(".admin_img_ic" ).css( "height", "" );
                                $("#admin_ic_image_filename").attr("value",jsonpData.filename);
                        }
		}, 'html');
	});

        $('#notice_image_up1').change(function() {
            if(readChkImgFile('#notice_image_up1')){
                fileUpload('#notice_image_up1');
            }else{
                return false;
            }
        });
        $('#notice_image_up2').change(function() {
            if(readChkImgFile('#notice_image_up2')){
                fileUpload('#notice_image_up2');
            }else{
                return false;
            }
        });
        $('#notice_image_up3').change(function() {
            if(readChkImgFile('#notice_image_up3')){
                fileUpload('#notice_image_up3');
            }else{
                return false;
            }
        });
        $('#notice_image_up4').change(function() {
            if(readChkImgFile('#notice_image_up4')){
                fileUpload('#notice_image_up4');
            }else{
                return false;
            }
        });
        $('#notice_image_up5').change(function() {
            if(readChkImgFile('#notice_image_up5')){
                fileUpload('#notice_image_up5');
            }else{
                return false;
            }
        });
        $('#present_up_img').change(function() {
            if(readChkImgFile('#present_up_img')){
                return true;
            }else{
                return false;
            }
        });
});

/* file アップロード*/
function fileUpload(type) {
	var str = yyyymmddhhmiss();
	// 画面を処理中画面でガードする
//	if (type == '#notice_image_up3'){
		timer = setTimeout(function(){
			$.blockUI({
				message: $('<H1">画像をアップロード中です...</H1><BR><img id="displayBox" src="./images/loadingp.gif" width="50" height="50"/>'), 
				css: { 
					//↓画像の縦の大きさを指定
					top:  (jQuery(window).height() - 300) /2 + 'px',
					//↓画像の横の大きさを指定
					left: (jQuery(window).width()/2 - 160) + 'px',
					//↓これも画像の横の大きさを指定
					width: '300px',
					border: 'none', 
					padding: '15px', 
					backgroundColor: '#fff', 
					'-webkit-border-radius': '10px', 
					'-moz-border-radius': '10px', 
					color: '#000'
				}
			});
		}, 200);
//	}
	
	$(type).upload('./sys_controller/NoticeUploadController.php', function(res) {

		$(type).after($(type).clone(true));
		$(type).remove();

		if (res.length>0){
			clearTimeout(timer);
			$.unblockUI();

			var jsonpData = JSON.parse(res);
			var id = "#notice_date";
			if (type == '#notice_image_up1') {
			    // チラシ配信以外
			    var text = $(id+"1").val();
			    if(text != ""){
				    text = text + "\n";
			    }
			    $(id+"1").val(text + "<img #" + jsonpData.filename + "#>");
			}else if (type == '#notice_image_up2') {
			    var text = $(id+"2").val();
			    if(text != ""){
				    text = text + "\n";
			    }
			    // チラシ配信以外
			    $(id+"2").val(text + "<img #" + jsonpData.filename + "#>");
			}else if (type == '#notice_image_up3') {
			    // ポップアップを閉じる
					// チラシ配信にチェックされている場合
					$("#leaflets_file_name").val(jsonpData.filename);                  
			}else if (type == '#notice_image_up4') {
			    var text = $(id+"4").val();
			    if(text != ""){
				    text = text + "\n";
			    }
			    // チラシ配信以外
			    $(id+"4").val(text + "<img #" + jsonpData.filename + "#>");
			}else if (type == '#notice_image_up5') {
			    var text = $(id+"5").val();
			    if(text != ""){
				    text = text + "\n";
			    }
			    $(id+"5").val(text + "<img #" + jsonpData.filename + "#>");
			}
		} else {
			// ポップアップを閉じる
			clearTimeout(timer);
			$.unblockUI();
			for (var i = 1; i <= 4; i++) {
				$("#notice_image_up"+i).val("");
			}
			error_popup_timer = setTimeout(function(){
				window.alert("ファイルのアップロードに失敗しました。\n再度アップロードをお願いします。");
			}, 400);
		}
	}, 'html');
}


/*
 * 一斉配信
 * opt_value = 配信パターンの切り替え（お知らせ配信、プレゼントクーポン発行）
 *             ※デフォルト値：お知らせ配信(notice)
 */
function all_check(opt_value , is_user_list){
	var option = opt_value || "notice";
	var search_val= "all";
	var url = "./sys_controller/NoticeDbController.php";
	if(option != "notice"){
	    url = "./sys_controller/GiftDbController.php";
	}
	if (is_user_list === 'true') {
            timer = setTimeout(function(){
                $.blockUI({
                    message: $('<H1">データ取得中です...</H1><BR><img id="displayBox" src="./images/loadingp.gif" width="50" height="50"/>'), 
                    css: { 
                        //↓画像の縦の大きさを指定
                        top:  (jQuery(window).height() - 300) /2 + 'px',
                        //↓画像の横の大きさを指定
                        left: (jQuery(window).width()/2 - 200) + 'px',
                        //↓これも画像の横の大きさを指定
                        width: '300',
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#fff', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        color: '#000'
                    }
                });
            }, 200);
        }
        console.log("call");
        $.ajax({
            url: url,
            type: "POST",
            timeout:60000,
            data: {
                selection : search_val,
                is_user_list : is_user_list
            },
            // 応答後
            complete: function(xhr, textStatus) {
                console.log("complete");
                // 送信中ポップアップ解除
                if (is_user_list === 'true') {
                    clearTimeout(timer);
                    $.unblockUI();
                }
            },
            // 通信成功時の処理
            success: function(result) {
                console.log("success");
                if (is_user_list === 'true') {
                    clearTimeout(timer);
                    $.unblockUI();
                }
//        console.log("call end");
                if (result.length>0){
                        var data_num = receiveData(result);
                        $("#search_data").html(data_num); /* 結果出力用のDIVにHTML文字列を設定する */
                        $("#delivery_num").val(data_num); /* 結果出力用のDIVにHTML文字列を設定する */
                        // コールバック関数が定義されている場合、呼び出す
                        if (is_user_list === 'true') {
                                var timer1 = setTimeout(function(){
                                        // ダイアログを出す
                                        $('#user_list').dialog('open');
                                }, 100);
                        }
                }
            },
            // 通信失敗時の処理
            error: function(xhr, textStatus, error) {
                console.log("error");
                if (is_user_list === 'true') {
                    clearTimeout(timer);
                    $.unblockUI();
                }
            }
        });

//	$.post(url , 
//            {
//                selection : search_val,
//                is_user_list : is_user_list
//            },
//            function(data){ /* $.post(url, [data], [callback(data, textStatus, XMLHttpRequest)], [dataType]) */
//        	if (is_user_list === 'true') {
//                    clearTimeout(timer);
//                    $.unblockUI();
//                }
//		if (data.length>0){
////			data_num = data_num.replace(/[\n\r]/g,"");
//                        var data_num = receiveData(data);
//			$("#search_data").html(data_num); /* 結果出力用のDIVにHTML文字列を設定する */
//			$("#delivery_num").val(data_num); /* 結果出力用のDIVにHTML文字列を設定する */
//			// コールバック関数が定義されている場合、呼び出す
//			if (is_user_list === 'true') {
//                                timer = setTimeout(function(){
//                                        // ダイアログを出す
//                                        $('#user_list').dialog('open');
//                                }, 100);
//			}
//		}
//	});
}

/*
 * 簡単配信
 * opt_value = 配信パターンの切り替え（お知らせ配信、プレゼントクーポン発行）
 *             ※デフォルト値：お知らせ配信(notice)
 */
function simple_check(opt_value, is_user_list){
	var option = opt_value || "notice";
	var search_val= "simple";
	var simple_checkbox1 = "";
	var simple_checkbox2 = "";
	var simple_checkbox3 = "";
	var simple_checkbox4 = "";
	var simple_checkbox5 = "";
	var simple_checkbox6 = "";
	var simple_checkbox7 = "";
	var simple_checkbox8 = "";
	var format_push = "";
	var format_mail = "";
	if($("#simple_checkbox1").attr("checked")){
		simple_checkbox1 = "checked";
	}
	if($("#simple_checkbox2").attr("checked")){
		simple_checkbox2 = "checked";
	}
	if($("#simple_checkbox3").attr("checked")){
		simple_checkbox3 = "checked";
	}
	if($("#simple_checkbox4").attr("checked")){
		simple_checkbox4 = "checked";
	}
	if($("#simple_checkbox5").attr("checked")){
		simple_checkbox5 = "checked";
	}
	if($("#simple_checkbox6").attr("checked")){
		simple_checkbox6 = "checked";
	}
	if($("#simple_checkbox7").attr("checked")){
		simple_checkbox7 = "checked";
	}
	if($("#simple_checkbox8").attr("checked")){
		simple_checkbox8 = "checked";
	}
	if($("#format_push").attr("checked")){
		format_push = "checked";
	}
	if($("#format_mail").attr("checked")){
		format_mail = "checked";
	}
	
	var url = "./sys_controller/NoticeDbController.php";
	if(option != "notice"){
	    url = "./sys_controller/GiftDbController.php";
	}
        
	if (is_user_list === 'true') {
            timer = setTimeout(function(){
                $.blockUI({
                    message: $('<H1">データ取得中です...</H1><BR><img id="displayBox" src="./images/loadingp.gif" width="50" height="50"/>'), 
                    css: { 
                        //↓画像の縦の大きさを指定
                        top:  (jQuery(window).height() - 300) /2 + 'px',
                        //↓画像の横の大きさを指定
                        left: (jQuery(window).width()/2 - 200) + 'px',
                        //↓これも画像の横の大きさを指定
                        width: '300',
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#fff', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        color: '#000'
                    }
                });
            }, 200);
        }
    
        $.ajax({
            url: url,
            type: "POST",
            timeout:60000,
            data: {
		selection : search_val ,
		is_user_list : is_user_list,
		simple_terms1 : simple_checkbox1 ,
		simple_terms2 : simple_checkbox2 ,
		simple_terms3 : simple_checkbox3 ,
		simple_terms4 : simple_checkbox4 ,
		simple_terms5 : simple_checkbox5 ,
		simple_terms6 : simple_checkbox6 ,
		simple_terms7 : simple_checkbox7 ,
		simple_terms8 : simple_checkbox8 ,
		format_push : format_push ,
		format_mail : format_mail
            },
            // 応答後
            complete: function(xhr, textStatus) {
                console.log("complete");
                // 送信中ポップアップ解除
                if (is_user_list === 'true') {
                    clearTimeout(timer);
                    $.unblockUI();
                }
            },
            // 通信成功時の処理
            success: function(result) {
                console.log("success");
                if (is_user_list === 'true') {
                    clearTimeout(timer);
                    $.unblockUI();
                }
                if (result.length>0){
                        var data_num = receiveData(result);
                        $("#search_data").html(data_num); /* 結果出力用のDIVにHTML文字列を設定する */
                        $("#delivery_num").val(data_num); /* 結果出力用のDIVにHTML文字列を設定する */
                        // コールバック関数が定義されている場合、呼び出す
                        if (is_user_list === 'true') {
                                var timer1 = setTimeout(function(){
                                        // ダイアログを出す
                                        $('#user_list').dialog('open');
                                }, 100);
                        }
                }
            },
            // 通信失敗時の処理
            error: function(xhr, textStatus, error) {
                console.log("error");
                if (is_user_list === 'true') {
                    clearTimeout(timer);
                    $.unblockUI();
                }
            }
        });
}
/*
 * 個別配信
 * opt_value = 配信パターンの切り替え（お知らせ配信、プレゼントクーポン発行）
 * ※デフォルト値：お知らせ配信(notice)
 * is_user_list = ユーザリストをサーバから取得する場合に'true'を設定する
 * callback = コールバック関数を設定する
 */
function individual_check(opt_value, is_user_list){
	var option = opt_value || "notice";
	var search_val= "individual";
	var radioSexList = document.getElementsByName("individual_sex");
	if (radioSexList[0].checked) {
		var individual_sex = "all";
	} else if (radioSexList[1].checked) {
		var individual_sex = "man";
	} else if (radioSexList[2].checked) {
		var individual_sex = "woman";
	}

	var individual_birthday_start = $("select[name='individual_birthday_start']").val();  //誕生日 (始)
	var individual_birthday_end   = $("select[name='individual_birthday_end']").val();    //誕生日 (終)

	var individual_region = $("select[name='individual_region']").val();  //地域

	var last_year_start  = $("select[name='last_year_start']").val();                        //最終来店日 年 (始)
	var last_month_start = $("select[name='last_month_start']").val();                       //最終来店日 月 (始)
	var last_day_start   = $("select[name='last_day_start']").val();                         //最終来店日 日 (始)
	var last_start       = last_year_start + "-" + last_month_start + "-" + last_day_start;  //最終来店日 (始)

	var last_year_end  = $("select[name='last_year_end']").val();                    //最終来店日 年 (始)
	var last_month_end = $("select[name='last_month_end']").val();                   //最終来店日 月 (終)
	var last_day_end   = $("select[name='last_day_end']").val();                     //最終来店日 日 (終)
	var last_end       = last_year_end + "-" + last_month_end + "-" + last_day_end;  //最終来店日 (終)

	var total_stamp_num = $("#individual_total_stamp_num").val();  //累計スタンプ数

	var format_push = "";
	var format_mail = "";

	if($("#format_push").attr("checked")){
		format_push = "checked";
	}
	if($("#format_mail").attr("checked")){
		format_mail = "checked";
	}

	var total_stamp_terms   = $("select[name='total_stamp_terms']").val();                     //累計スタンプ数条件
	
	var branch_num = $("select[name='branch_list']").val();                     //店舗一覧選択
	var search_keyword = $("#search_keyword").val();                     //キーワード検索
	console.log("search_keyword"+search_keyword);
	
	var option = opt_value || "notice";
	var url = "./sys_controller/NoticeDbController.php";
	if(option != "notice"){
	    url = "./sys_controller/GiftDbController.php";
	}
        var timer = null;
	if (is_user_list === 'true') {
            timer = setTimeout(function(){
                $.blockUI({
                    message: $('<H1">データ取得中です...</H1><BR><img id="displayBox" src="./images/loadingp.gif" width="50" height="50"/>'), 
                    css: { 
                        //↓画像の縦の大きさを指定
                        top:  (jQuery(window).height() - 300) /2 + 'px',
                        //↓画像の横の大きさを指定
                        left: (jQuery(window).width()/2 - 200) + 'px',
                        //↓これも画像の横の大きさを指定
                        width: '300',
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#fff', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        color: '#000'
                    }
                });
            }, 200);
        }

        $.ajax({
            url: url,
            type: "POST",
            timeout:60000,
            data: {
		selection : search_val ,
		is_user_list : is_user_list,
		individual_terms1 : individual_sex ,             //性別
		individual_terms2 : individual_birthday_start ,  //誕生日start
		individual_terms3 : individual_birthday_end ,    //誕生日 (終)
		individual_terms4 : individual_region ,          //地域
		individual_terms7 : last_start ,                 //最終来店日 (始)
		individual_terms8 : last_end ,                   //最終来店日 (終)
		individual_terms9 : total_stamp_num ,            //累計スタンプ数
		individual_terms10 : total_stamp_terms ,         //累計スタンプ数条件
		format_push : format_push ,
		format_mail : format_mail ,
		branch_num : branch_num ,
		search_keyword : search_keyword
            },
            // 応答後
            complete: function(xhr, textStatus) {
                console.log("complete");
                // 送信中ポップアップ解除
                if (is_user_list === 'true') {
                    clearTimeout(timer);
                    $.unblockUI();
                }
            },
            // 通信成功時の処理
            success: function(result) {
                console.log("success");
                if (is_user_list === 'true') {
                    clearTimeout(timer);
                    $.unblockUI();
                }
                if (result.length>0){
                        var data_num = null;
                        if (option === "gift") {
                            data_num = receiveData(result);
                        } else {
                            data_num = receiveDataIndividual(result);
                        }
                        $("#search_data").html(data_num); /* 結果出力用のDIVにHTML文字列を設定する */
                        $("#delivery_num").val(data_num); /* 結果出力用のDIVにHTML文字列を設定する */
			//チェックをはずしたuser_idをリセット
			$('#exclusion_user_id').val("");
                        // コールバック関数が定義されている場合、呼び出す
                        if (is_user_list === 'true') {
                                var timer1 = setTimeout(function(){
                                        // ダイアログを出す
                                        $('#user_list').dialog('open');
                                }, 100);
                        }
                }
            },
            // 通信失敗時の処理
            error: function(xhr, textStatus, error) {
                console.log("error");
                if (is_user_list === 'true') {
                    clearTimeout(timer);
                    $.unblockUI();
                }
            }
        });

//	$.post( url , 
//        {
//		selection : search_val ,
//		is_user_list : is_user_list,
//		individual_terms1 : individual_sex ,             //性別
//		individual_terms2 : individual_birthday_start ,  //誕生日start
//		individual_terms3 : individual_birthday_end ,    //誕生日 (終)
//		individual_terms4 : individual_region ,          //地域
//		individual_terms7 : last_start ,                 //最終来店日 (始)
//		individual_terms8 : last_end ,                   //最終来店日 (終)
//		individual_terms9 : total_stamp_num ,            //累計スタンプ数
//		individual_terms10 : total_stamp_terms ,         //累計スタンプ数条件
//		format_push : format_push ,
//		format_mail : format_mail
//	}, function(data){ /* $.post(url, [data], [callback(data, textStatus, XMLHttpRequest)], [dataType]) */
//        	if (is_user_list === 'true') {
//                    clearTimeout(timer);
//                    $.unblockUI();
//                }
//		if (data.length>0){
////			data_num = data_num.replace(/[\n\r]/g,"");
//			var data_num = receiveData(data);
//			//alert(data_num);
//			$("#search_data").html(data_num); /* 結果出力用のDIVにHTML文字列を設定する */
//			$("#delivery_num").val(data_num); /* 結果出力用のDIVにHTML文字列を設定する */
//			// コールバック関数が定義されている場合、呼び出す
//			if (is_user_list === 'true') {
//                                timer = setTimeout(function(){
//                                        // ダイアログを出す
//                                        $('#user_list').dialog('open');
//                                }, 100);
//			}
//		}
//		
//	});
}
/*
 * お知らせ配信表示切り替え記述
 * content = 配信パターンの切り替え(一斉配信、簡単配信、個別配信)
 * opt_value = 配信パターンの切り替え（お知らせ配信、プレゼントクーポン発行）
 *             ※デフォルト値：お知らせ配信(notice)
 */
function checkradio( content , opt_value ) {
	var option = opt_value || "notice";
	
	if(content == "all"){  //一斉送信
		all_check(option);
		document.getElementById('simple_menu').style.display = 'none';
		document.getElementById('individual_menu').style.display = 'none';
		document.getElementById('format_menu').style.display = 'none';
		document.getElementById('save_menu').style.display = 'block';
//		document.getElementById('sns_menu').style.display = 'block';
	} else if(content == "simple"){
		simple_check(option);
		document.getElementById('simple_menu').style.display = 'block';
		document.getElementById('individual_menu').style.display = 'none';
		document.getElementById('format_menu').style.display = 'block';
		document.getElementById('save_menu').style.display = 'none';
//		document.getElementById('sns_menu').style.display = 'none';
	} else if(content == "individual"){
		individual_check(option);
		document.getElementById('simple_menu').style.display = 'none';
		document.getElementById('individual_menu').style.display = 'block';
		document.getElementById('format_menu').style.display = 'block';
		document.getElementById('save_menu').style.display = 'none';
//		document.getElementById('sns_menu').style.display = 'none';
	}
}
function format_check() {
	if(document.getElementById('simple_menu').style.display == 'block'){
		simple_check();
	}
	if(document.getElementById('individual_menu').style.display == 'block'){
		individual_check();
	}
}

function receiveData(data) {
    // 全選択/解除の非表示
    $('.allCheck').css("display", "none");
    
    var jsonpData = JSON.parse(data);
    console.log("jsonpData : "+jsonpData);
    //チェックボックスタイトル
    $("#checkbox_header")[0] ? $("#checkbox_header").remove():"";
    // 要素を削除
    $(".user_list_data_tr").remove();
    if (jsonpData.data != null) {
        // 要素を受け取ったデータで再構築
        var userdata = jsonpData.data;
        for(var i = 0; i < userdata.length; i++){
            // 顧客ID
            var user_id = $("<td/>").attr("align","center").attr("valign","top").append(jsonpData.id + ("0000"+userdata[i].user_id).slice(-5));
            // 顧客名
            var user_name = $("<td/>").attr("align","left").attr("valign","top").append(userdata[i].user_name != "" ? userdata[i].user_name : "-");
            // 登録日
            var add_date = $("<td/>").attr("align","center").attr("valign","top").append(userdata[i].add_date.replace(/-/g,"/"));
            // 最終アクセス日
            var last_stamp_date = $("<td/>").attr("align","center").attr("valign","top").append(userdata[i].last_stamp_date.replace(/-/g,"/"));
            // スタンプ数
            var stamp_num = $("<td/>").attr("align","center").attr("valign","top").append(userdata[i].stamp_num);
            // 累計スタンプ数
            var total_stamp_num = $("<td/>").attr("align","center").attr("valign","top").append(userdata[i].total_stamp_num);
            // ユーザ行を生成
            var user_line = $("<tr/>").attr("align","center").attr("class", "user_list_data_tr")
                        .append(user_id)
                        .append(user_name)
                        .append(add_date)
                        .append(last_stamp_date)
                        .append(stamp_num)
                        .append(total_stamp_num);
            // クーポン一覧へ追加する
            $("#user_list_table").append(user_line);
        }
    }
    
    console.log("jsonpData.user_num"+jsonpData.user_num);
    return jsonpData.user_num;
}
function receiveDataIndividual(data) {
    var jsonpData = JSON.parse(data);
    console.log("jsonpData : "+jsonpData);
    
    // 要素を削除
    $(".user_list_data_tr").remove();

    $("#checkbox_header")[0] ? $("#checkbox_header").remove():"";
    $("#user_list_header").prepend('<th id="checkbox_header" width="4%"/>').prepend(' ');
    
    // 全選択/解除の表示
    $('.allCheck').removeAttr("style");

    if (jsonpData.data != null) {
        // 要素を受け取ったデータで再構築
        var userdata = jsonpData.data;
        for(var i = 0; i < userdata.length; i++){
	    // チェックボックス
	    var user_chk_box = $("<td/>").attr("align","center").attr("valign","top").append($('<input type="checkbox" />').attr({class: 'user_id_checkbox'}).attr({id: userdata[i].user_id}).prop("checked", true ));
            // 顧客ID
            var user_id = $("<td/>").attr("align","center").attr("valign","top").append(jsonpData.id + ("0000"+userdata[i].user_id).slice(-5));
            // 顧客名
            var user_name = $("<td/>").attr("align","left").attr("valign","top").append(userdata[i].user_name != "" ? userdata[i].user_name : "-");
            // 登録日
            var add_date = $("<td/>").attr("align","center").attr("valign","top").append(userdata[i].add_date.replace(/-/g,"/"));
            // 最終アクセス日
            var last_stamp_date = $("<td/>").attr("align","center").attr("valign","top").append(userdata[i].last_stamp_date.replace(/-/g,"/"));
            // スタンプ数
            var stamp_num = $("<td/>").attr("align","center").attr("valign","top").append(userdata[i].stamp_num);
            // 累計スタンプ数
            var total_stamp_num = $("<td/>").attr("align","center").attr("valign","top").append(userdata[i].total_stamp_num);
            // ユーザ行を生成
            var user_line = $("<tr/>").attr("align","center").attr("class", "user_list_data_tr")
                        .append(user_chk_box)
                        .append(user_id)
                        .append(user_name)
                        .append(add_date)
                        .append(last_stamp_date)
                        .append(stamp_num)
                        .append(total_stamp_num);

            // クーポン一覧へ追加する
            $("#user_list_table").append(user_line);
        }
	var exclusion_user_id = $('#exclusion_user_id').val().split(',');
	console.log("exclusion_user_id : "+exclusion_user_id);
	if(exclusion_user_id != null && exclusion_user_id.length > 0){
	    for (var i = 0; i < exclusion_user_id.length; i++){
		$('#'+exclusion_user_id[i]).prop("checked", false );
	    }
	}
    }
    
    return jsonpData.user_num;
}
/*
 * テキスト入力領域が空または入力数をオーバーした場合
 * エラーポップアップを表示。
 */
function text_area_chk( value , max_val ){
        if(value == ""){
            window.alert("クーポン名が未入力です。");
            return false;
        } else if(value.length > max_val){
            window.alert("入力制限を超えています。\n"+max_val+"文字以内で入力してください。");
            return false;
        }
}
/**
 * 画像サイズ（バイト数）チェック
 * ファイル名（拡張子）チェック
 * @param {type} type
 * @returns {Boolean}
 */
function readChkImgFile(type) {
    var fileData = $(type).prop('files')[0];
    //console.log("fileData : "+fileData.size+"  filename : "+fileData.name); 

    // 画像ファイル拡張子を取得
    var imgExtension = fileData.name.split(".").pop().toLowerCase();
    //console.log("fileSize : "+fileSize+" resultImgExtension : "+imgExtension);

    // 許可される拡張子
    var arrayExtensions = ["jpg" , "jpeg" , "png" , "gif"];
    //console.log("拡張子チェック : "+arrayExtensions[1]);

    // 許可される拡張子とマッチしているかチェック
    var extensionFlag = false;
    for(var i = 0; i < arrayExtensions.length; i++){
        if(imgExtension == arrayExtensions[i]){
            extensionFlag = true;
            break;
        }
    }
    //拡張子がマッチしていたら、ファイルサイズチェックをする
    if(extensionFlag == true){
        // ファイルサイズ：KB表示（切り捨て）
        var fileSize = Math.floor(fileData.size/1024);
        //アップロードできる画像データサイズの最大サイズ
        var fileSizeMax = 1024*10;
        if(fileSizeMax < fileSize){
            setTimeout(function(){
                window.alert("ファイルサイズが大きすぎます。(10MBまでのファイルを選択してください)");
            });
            return false;
        }else{
            return true;
        }
    }else{
        setTimeout(function(){
            window.alert("サポート対象外のファイルが選択されました。");
        });
        return false;
    }
}
function presentDataCheck( value , max_val , limit_coupon){
    var max_present = $("#total_present_num").text();
//    if(max_present < limit_coupon){
//        return text_area_chk( value , max_val);
//    }else{
//        window.alert("クーポンがいっぱいです。\n配信履歴からクーポンを削除してもう一度送信をしてください。");
//        return false;
//    }
    return text_area_chk( value , max_val);

}
/**
 * 条件に該当するユーザリストを検索して取得する
 * @param {type} opt_value
 * @returns {undefined}
 */
function search_data(opt_value) {
        var option = opt_value || "notice";
        // 条件に合致するユーザ一覧を取得する
        
        var radioVal = $("input:radio[name='delivery_set']:checked").val();
        if (radioVal === 'all') {
                all_check(option, 'true');
        } else if (radioVal === 'simple') {
                simple_check(option, 'true');
        } else if (radioVal === 'individual') {
                individual_check(option, 'true');
        }
        
        console.log("call1");
}