<?php
require_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'config.php');
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'ad_config.php');

if (!isset($_GET['issue'])) {
	$ua = $_SERVER['HTTP_USER_AGENT'];
	if ((strpos($ua, 'Android') !== false) && (strpos($ua, 'Mobile') !== false) || (strpos($ua, 'iPhone') !== false) || (strpos($ua, 'Windows Phone') !== false)) {
		// スマートフォンからアクセスされた場合
		echo('<center><img src="../sp_images/era.png"  width="100%"/></center>');
		exit;
	} else {
		echo('<center><img src="../fp_images/era_fp.jpg" /></center>');
		exit;
	}

}
//スタンプタイプをGET
// type = A : 既存
// type = B : スタンプ減抑止アプリ
$stamp_type = $_GET['type'];
$issue = $_GET['issue'];
$issue = base64_decode($issue);
if (Device::is_featurephone_browser()) {
	if(STAMP_TYPE == 'B') {
		header("Location:./fp/stamp_update_b.php?guid=ON&issue={$issue}");
	}else {
		header("Location:./fp/stamp_update.php?guid=ON&issue={$issue}");
	}
	exit;
} else if (Device::is_ios_browser()) {
	if (!isset($_GET['id'])) {
		header("Location:".APPLE_STORE_URL);
		exit;
	}
	$id = $_GET['id'];
	$ident = $_GET['ident'];
	if(isset($stamp_type)){
		header("Location:./sp/stamp_update_b.php?issue={$issue}&id={$id}&ident={$ident}");
	}else{
		header("Location:./sp/stamp_update.php?issue={$issue}&id={$id}&ident={$ident}");
	}
	exit;
} else if (Device::is_android_browser()) {
	if (!isset($_GET['id'])) {
		header("Location:".GOOGLE_PLAY_URL);
		exit;
	}
	$id = $_GET['id'];
	$ident = $_GET['ident'];
	$issue = $_GET['issue'];
	if(isset($stamp_type)){
		header("Location:./sp/stamp_update_b.php?issue={$issue}&id={$id}&ident={$ident}");
	}else{
		header("Location:./sp/stamp_update.php?issue={$issue}&id={$id}&ident={$ident}");
	}
	exit;
} else {
	if (!isset($_GET['id'])) {
		if(isset($stamp_type)){
			header("Location:./sp/stamp_update_b.php?issue={$issue}");
		}else{
			header("Location:./sp/stamp_update.php?issue={$issue}");
		}
		exit;
	} else {
		$id = $_GET['id'];
		$ident = $_GET['ident'];
		if(isset($stamp_type)){
			header("Location:./sp/stamp_update_b.php?issue={$issue}&id={$id}&ident={$ident}");
		}else{
			header("Location:./sp/stamp_update.php?issue={$issue}&id={$id}&ident={$ident}");
		}
		exit;
	}
}
