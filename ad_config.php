<?php
define('ADMIN_ID', '219');
//最初のスタンプ数
define("NEW_NUM", '1');

define("USER_ID", 'TP');

define("FILE_NAME", 'template');

define("STAMP_RALLY_NAME", 'テンプレート');

// スタンプの付与タイプ。Aが消費型 Bが累計型
// A or B
define('STAMP_TYPE', 'B');

// 0:個々の店舗でセット表示
// 1:融合表示 個々の店舗情報の独自タグにインデックスが付く（例：＃XXX_1#）
//   デザインは先頭の店舗のものを使用する
define('SHOP_VIEW_TYPE', '0');

//AppleStoreのURL
define('APPLE_STORE_URL', 'http://owl-solution.jp/stamp/sp_images/under_development.png');
//Androidの google play URL
define('GOOGLE_PLAY_URL', 'http://owl-solution.jp/stamp/sp_images/under_development.png');

//お知らせ配信メール設定
define('NOTICE_MAIL_FROM_ADDRESS', 'info@owl-solution.jp');
define('NOTICE_MAIL_FROM_NAME', 'スタンプラリーシステム');

define('ALL_REGION_LABEL', '全ての都道府県');
//define('ALL_REGION_LABEL', '全ての地域');

// 地域リスト
// groupを空にすると、PC条件リスト等でgroup名が非表示になる
define('REGION_LIST',
<<<EOD
[
	{
		"gname":"北海道・東北",
		"regions":"北海道,青森県,秋田県,岩手県,山形県,宮城県,福島県"
	},
	{
		"gname":"甲信越・北陸",
		"regions":"山梨県,長野県,新潟県,富山県,石川県,福井県"
	},
	{
		"gname":"関東",
		"regions":"茨城県,栃木県,群馬県,埼玉県,千葉県,東京都,神奈川県"
	},
	{
		"gname":"東海",
		"regions":"愛知県,静岡県,岐阜県,三重県"
	},
	{
		"gname":"関西",
		"regions":"大阪府,兵庫県,京都府,滋賀県,奈良県,和歌山県"
	},
	{
		"gname":"中国",
		"regions":"岡山県,広島県,鳥取県,島根県,山口県"
	},
	{
		"gname":"四国",
		"regions":"徳島県,香川県,愛媛県,高知県"
	},
	{
		"gname":"九州・沖縄",
		"regions":"福岡県,佐賀県,長崎県,熊本県,大分県,宮崎県,鹿児島県,沖縄県"
	}
]
EOD
);

//define('REGION_LIST',
//<<<EOD
//[
//	{
//		"gname":"",
//		"regions":"福岡県,県外,中国,韓国,台湾"
//	}
//]
//EOD
//);


// FACEBOOK投稿用ID情報
define('FACEBOOK_APP_ID', '1643545275937975');
// FACEBOOK投稿用App Secret
define('FACEBOOK_APP_SECRET', '88c9c4b7dd93e9ad2c77d50525c208c6');
// FACEBOOK投稿先PAGE_ID
define('FACEBOOK_PAGE_ID', '676256562385733');

// Twitter連携情報
define('TWITTER_API_KEY', 'WMAsbMVt8bS3c69b4CGyxeCV0');
define('TWITTER_API_SECRET', '4CUCXV8VAPlOZ95GCuaTIKIyXohNX04Wx6QUd6d39s7I9timWR');

// TwitterアカウントのOAuth情報
define('TWITTER_ACCESS_TOKEN', '2359402074-LGYFf2FEOC86RhkRV3EuqymbYMEB1Lua43UvwtV');
define('TWITTER_ACCESS_SECRET', 'z3GAOwoEEWuM0mBHx34IqzsWLFxGDCtN6inEwORwoMyVf');

require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'version.php');

// サービス終了フラグ
// 0:運用中  1:終了
define('SERVICE_END', '0');

// 予約機能 0:簡易予約 1:予約管理システム連動
define('RESERVE_MODE', '0');

// コードの有効QRコードであることを判定するキーコード
define("ETERNAL_SALT_KEY","HiZ6x-PqLU64!");

?>