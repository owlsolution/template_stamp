<?php
require_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'config.php');
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'ad_config.php');

if (!isset($_GET['rally_id']) || !isset($_GET['store'])) {
	$ua = $_SERVER['HTTP_USER_AGENT'];
	if ((strpos($ua, 'Android') !== false) && (strpos($ua, 'Mobile') !== false) || (strpos($ua, 'iPhone') !== false) || (strpos($ua, 'Windows Phone') !== false)) {
		// スマートフォンからアクセスされた場合
		echo('<center><img src="../sp_images/era.png"  width="100%"/></center>');
		exit;
	} else {
		echo('<center><img src="../fp_images/era_fp.jpg" /></center>');
		exit;
	}
}
$rally_id = $_GET['rally_id'];
$store = $_GET['store'];

if (Device::is_featurephone_browser()) {
	header("Location:./fp/new_stamp.php?guid=ON&rally_id={$rally_id}&store={$store}");
	exit;
} else if (Device::is_ios_browser()) {
	header("Location:".APPLE_STORE_URL);
	exit;
} else if (Device::is_android_browser()) {
	header("Location:".GOOGLE_PLAY_URL);
	exit;
} else {
	header("Location:./sp/index.php?rally_id={$rally_id}&store={$store}");
	exit;
}
