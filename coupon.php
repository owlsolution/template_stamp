<?php
require_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'config.php');

$issue = $_GET['issue'];

if (Device::is_featurephone_browser()) {
	$coupon_id = $_GET['coupon'];
	$store_id = $_GET['store'];
	header("Location:./fp/coupon_update.php?guid=ON&issue={$issue}");
	exit;
} else if (Device::is_ios_browser()) {
	$coupon_id = $_GET['coupon'];
	$store_id = $_GET['store'];
	$id = $_GET['id'];
	$ident = $_GET['ident'];
	header("Location:./sp/coupon_update.php?issue={$issue}&id={$id}&ident={$ident}");
	exit;
} else if (Device::is_android_browser()) {
	$coupon_id = $_GET['coupon'];
	$store_id = $_GET['store'];
	$id = $_GET['id'];
	$ident = $_GET['ident'];
	header("Location:./sp/coupon_update.php?issue={$issue}&id={$id}&ident={$ident}");
	exit;
} else {
	$coupon_id = $_GET['coupon'];
	$store_id = $_GET['store'];
	header("Location:./sp/coupon_update.php?issue={$issue}");
	exit;
}
?>