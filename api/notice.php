<?php
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'api_base.php');
require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'common'. DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'RallyUserModel.php');
require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'common'. DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'AdminModel.php');
require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'common'. DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'UserModel.php');
require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'common'. DIRECTORY_SEPARATOR . 'component' . DIRECTORY_SEPARATOR . 'NoticeComponent.php');

class ApiNotice extends BaseApi{
	
	public $rallyUserModel;
	public $noticeComp;
	public $userModel;
	public $adminModel;
	
	function __construct() {
		parent::__construct();
		$this->rallyUserModel = new RallyUserModel();
		$this->adminModel = new AdminModel();
		$this->userModel = new UserModel();
		$this->noticeComp = new NoticeComponent();
	}

	/**
	 * 個人宛お知らせ通知
	 * 
	 * ['admin_id']:admin ID
	 * ['user_id']:ユーザID
	 * ['title']:タイトル(pushの本文にもなる)
	 * ['context']:お知らせの内容
	 * 
	 * @return
	 */
	public function notice_to_one() {
		// jsonで受け取った値を変数に格納
		$admin_id = isset($this->post_info['admin_id']) ? $this->post_info['admin_id'] : '0';
		$user_id = isset($this->post_info['user_id']) ? $this->post_info['user_id'] : '0';
		$user_id = preg_replace('/[^0-9]/', '', $user_id);
		$divide_num = isset($this->post_info['divide_num']) ? $this->post_info['divide_num'] : '0';
		$title = isset($this->post_info['title']) ? $this->post_info['title'] : '';
		$context = isset($this->post_info['context']) ? $this->post_info['context'] : '';
		
		print_r($this->post_info['divide_num']);
		echo "\n------------------\n";
		print_r($this->post_info['admin_id']);
		echo "\n------------------\n";
		print_r($user_id);
		echo "\n------------------\n";
		print_r($this->post_info['title']);
		echo "\n------------------\n";
		print_r($this->post_info['context']);
		echo "\n------------------\n";
		
		if (empty($user_id) && !empty($divide_num)) {
			// 個体識別IDからuser_idを取得する
			$db = db_connect();
			$user = $this->userModel->find_by_devide_num($db, $divide_num);
			
			echo "\nuser\n";
			print_r($user);
			
			$user_id = $user['user_id'];
			print_r("gene user_id:".$user_id);
			db_close($db);
			if (empty($user_id)) {return false;}
		}
		
		$db = db_connect();
		$rally_id = $this->noticeComp->get_rally_id($db);
		db_close($db);
		
		if (isset($rally_id) && !empty($user_id)) {
			$db = db_connect();
			$r_user_info = $this->rallyUserModel->get_userinfo_by_user_id_and_rally_id($db, $user_id, $rally_id);
			db_close($db);
		}
		
		if ($r_user_info) {
			
			// 対象ユーザが存在するので、お知らせを作成してお知らせを配信する。
			// おしらせ作成
			error_log("r_user_info:".print_r($r_user_info, true));
			
			// 送信時点での送信可能な総ユーザ数を取得する。
			$send_admin = 0;
			$count_users = 1;
			$delivery_set_no = '7';			// 
			$notice_type = '5';				// 予約
			$now_day = date('Y-m-d H:i:s');	//配信日時
			
			$db = db_connect();
			$set = "rally_id = '".$rally_id."' , notice_content  = '".$context."', admin_id  = '".$admin_id."', notice_data  = '".$now_day."', notice_title  = '".$title."', delivery_set  = '6',format  = '1', simple_last_month  = '',simple_this_month  = '', simple_next_month  = '', simple_sex  = '', yesterday_come  = '', month_come  = '', this_month_come  = '', individual_sex  = '', individual_birthday_start  = '', individual_birthday_end  = '', individual_region  = '', first_start  = '', first_end  = '', last_start  = '', last_end  = '', total_stamp_num  = '', total_stamp_terms  = '', delivery_num  = '".$count_users."', notice_type  = '".$notice_type."', thumbnail  = '', total_user_count  = '".$count_users."', acceptance_datetime = now(), notice_schedule_id = '0'";
			notice_create($db, $set);
			$notice_id = mysql_insert_id();
			db_close( $db );
			
			// 
			// ユーザリストを渡してpush or メールする共通関数にユーザリストを渡す
			
			$notice_data = array(
				'notice_id' => $notice_id,
				'notice_title' => $title,
				'notice_content' => $context,
				'now_day' => $now_day,
				'delivery_set' => $delivery_set_no,
				'format' => '1',
				'simple_last_month' => '',
				'simple_this_month' => '',
				'simple_next_month' => '',
				'simple_sex' => '',
				'month_come' => '',
				'this_month_come' => '',
				'yesterday_come' => '',
				'individual_sex' => '',
				'individual_birthday_start' => '',
				'individual_birthday_end' => '',
				'individual_region' => '',
				'first_start' => '',
				'first_end' => '',
				'last_start' => '',
				'last_end' => '',
				'total_stamp_num' => '',
				'total_stamp_terms' => '',
				'to_user_id' => '',
				'reserve_datetime' => '',
				'send_admin' => $admin_id ,
				'branch_id' => '' , // 個別配信での支店登録済み/未登録の条件
				'keyword' => '' , 
				'exclusion_user_id' => '',
				'user_id_list' => $user_id
			);
			
			$p_admin_id = ADMIN_ID;
			
			print_r($notice_data);
			Util::notice_message_immediate($p_admin_id, $notice_data, $user_id);
		}
		
		return true;
	}

	public function notice_to_any() {
		// jsonで受け取った値を変数に格納
		$admin_id = isset($this->post_info['admin_id']) ? $this->post_info['admin_id'] : '0';
		$user_id = isset($this->post_info['user_id']) ? $this->post_info['user_id'] : '0';
		$divide_num = isset($this->post_info['divide_num']) ? $this->post_info['divide_num'] : '0';
		$title = isset($this->post_info['title']) ? $this->post_info['title'] : '';
		$context = isset($this->post_info['context']) ? $this->post_info['context'] : '';
		
//		print_r($this->post_info['divide_num']);
//		echo "\n------------------\n";
//		print_r($this->post_info['admin_id']);
//		echo "\n------------------\n";
//		print_r($this->post_info['user_id']);
//		echo "\n------------------\n";
//		print_r($this->post_info['title']);
//		echo "\n------------------\n";
//		print_r($this->post_info['context']);
//		echo "\n------------------\n";
		
		// 顧客IDの先頭ヘッダが付いていたらトリムする
		if (is_array($user_id)) {
			$user_ids = $user_id;
			$user_id = [];
			foreach ($user_ids as $val) {
				$user_id[] = preg_replace('/[^0-9]/', '', $val);
			}
		} else {
			$user_id = preg_replace('/[^0-9]/', '', $user_id);
		}
		
		if (empty($user_id) && !empty($divide_num)) {
			// 個体識別IDからuser_idを取得する
			$db = db_connect();
			$user = $this->userModel->find_by_devide_num($db, $divide_num);
			
			print_r($user);
			
			$user_id = $user['user_id'];
			print_r("gene user_id:".$user_id);
			db_close($db);
			if (empty($user_id)) {return false;}
		}
		
		$db = db_connect();
		$rally_id = $this->noticeComp->get_rally_id($db);
		db_close($db);
		
		if (isset($rally_id) && !empty($user_id)) {
			$db = db_connect();
			$r_user_info = $this->rallyUserModel->get_users_by_user_id_and_rally_id($db, $user_id, $rally_id);
			db_close($db);
		}
		
		echo("r_user_info:".print_r($r_user_info, true));
		
		$user_id = [];
		foreach ($r_user_info as $value) {
			// 当該店舗で存在するuser_idのみ抽出
			$user_id[] = $value['user_id'];
		}
		
		if (isset($user_id) && count($user_id) > 0) {
			// 対象ユーザが存在するので、お知らせを作成してお知らせを配信する。
			
			// 支店IDを取得する
			$db = db_connect();
			$branch_id = $this->adminModel->get_branch_id_by_admin_id($db, $admin_id);
			db_close($db);
			
			error_log("branch_id:".print_r($branch_id, true));
			
			// 送信時点での送信可能な総ユーザ数を取得する。
			$db = db_connect();
			$count_users = $this->rallyUserModel->get_user_count($db, $rally_id, $branch_id);
			db_close($db);
			
			$deliverly_num = count($user_id);
			$count_users = empty($count_users) ? '0' : $count_users;
			$delivery_set_no = '7';			// 
			$notice_type = '5';				// 予約
			$now_day = date('Y-m-d H:i:s');	//配信日時
			
			$db = db_connect();
			$set = "rally_id = '".$rally_id."', "
				."notice_content  = '".$context."', "
				."admin_id  = '".$admin_id."', "
				."notice_data  = '".$now_day."', "
				."notice_title  = '".$title."', "
				."delivery_set  = '6',"
				."format  = '1', "
				."simple_last_month  = '',"
				."simple_this_month  = '', "
				."simple_next_month  = '', "
				."simple_sex  = '', "
				."yesterday_come  = '', "
				."month_come  = '', "
				."this_month_come  = '', "
				."individual_sex  = '', "
				."individual_birthday_start  = '', "
				."individual_birthday_end  = '', "
				."individual_region  = '', "
				."first_start  = '', "
				."first_end  = '', "
				."last_start  = '', "
				."last_end  = '', "
				."total_stamp_num  = '', "
				."total_stamp_terms  = '', "
				."delivery_num  = '".$deliverly_num."', "
				."notice_type  = '".$notice_type."', "
				."thumbnail  = '', "
				."total_user_count  = '".$count_users."', "
				."acceptance_datetime = now(), "
				."notice_schedule_id = '0'";
			
			notice_create($db, $set);
			$notice_id = mysql_insert_id();
			db_close( $db );
			if (empty($notice_id)) {
				print_r('error insert notice data.');
				return;
			}
			
			// 
			// ユーザリストを渡してpush or メールする共通関数にユーザリストを渡す
			
			$notice_data = array(
				'notice_id' => $notice_id,
				'notice_title' => $title,
				'notice_content' => $context,
				'now_day' => $now_day,
				'delivery_set' => $delivery_set_no,
				'format' => '1',
				'send_admin' => $admin_id ,
				'branch_id' => '' , // 個別配信での支店登録済み/未登録の条件
				'user_id_list' => $user_id
			);
			
			$p_admin_id = ADMIN_ID;
			
			print_r($notice_data);
			Util::notice_message_immediate($p_admin_id, $notice_data, $user_id);
		}
		
		return true;
	}

}

$apiNotice = new ApiNotice();
call_user_func_array( array($apiNotice, $func), array());
