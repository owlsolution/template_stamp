<?php
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'api_base.php');
//require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'common'. DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'RallyUserModel.php');
require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'common'. DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'UserModel.php');
//require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'common'. DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'RallyModel.php');
//require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'common'. DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'AdminModel.php');

class ApiUser extends BaseApi{
	
	public $rallyUserModel;
	public $userModel;
	public $rallyModel;
	public $adminModel;
		
	function __construct() {
		parent::__construct();
//		$this->rallyUserModel = new RallyUserModel();
		$this->userModel = new UserModel();
//		$this->rallyModel = new RallyModel();
//		$this->adminModel = new AdminModel();
	}

	/**
	 * ユーザ識別IDから対象の店舗のadmin_idを取得する
	 * ['id']:admin ID
	 * 
	 * @return
	 */
	public function get_admin_id() {
		
		// jsonで受け取った値を変数に格納
		$divide_num = isset($this->post_info['divide_num']) ? $this->post_info['divide_num'] : '0';
		$rally_id = isset($this->post_info['rally_id']) ? $this->post_info['rally_id'] : '0';
		
		print_r($this->post_info['divide_num']);
		
		// ユーザ情報取得
		$db = db_connect();
		$user = $this->userModel->find_by_devide_num($db, $divide_num);
		$rally_user = $this->rallyUserModel->get_rally_user_by_user_id_and_rally_id($db, $user['user_id'], $rally_id);
		
		// rally_idを元に、その店舗が支店対応かそうで無いかを判断する
		// オーナーのadmin_idを取得する
		$rally = $this->rallyModel->find_by_rally_id($db, $rally_user['rally_id']);
		
		$admin_type = $this->adminModel->get_admin_type($db, $rally['admin_id']);
		if ($admin_type == OWNER) {
			// オーナーアカウント
			// admin_id確定、支店一覧不要;
		} else if ($admin_type == BRANCHES_OWNER) {
			// 統括アカウント
			// branch_idからadmin_idを特定に取り掛かる、支店一覧は必要;
			
		} else {
			// 支店アカウント
		}
		db_close($db);
		
		return true;
	}

	/**
	 * ユーザ識別IDから対象の店舗のadmin_idを取得する
	 * ['id']:admin ID
	 * 
	 * @return
	 */
	public function get_user() {
		
		// jsonで受け取った値を変数に格納
		$divide_num = isset($this->post_info['divide_num']) ? $this->post_info['divide_num'] : '0';
		
		error_log($this->post_info['divide_num']);
		
		// ユーザ情報取得
		$db = db_connect();
		$user = $this->userModel->find_by_devide_num($db, $divide_num);
		db_close($db);
		
		$user = empty($user) ? "" : $user;
		
		$user['user_id'] = USER_ID.sprintf("%05d", $user['user_id']);
		$return_array = array( 
			"user" => $user,
		);

		error_log(json_encode( $return_array ));
		header("Content-Type: application/json; charset=utf-8");
		print json_encode($return_array);
	}
	

}

$apiNotice = new ApiUser();
call_user_func_array( array($apiNotice, $func), array());
