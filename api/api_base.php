<?php

require_once(dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'config.php');
require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'ad_config.php');

/**
 * Apiの基底クラス
 */
class BaseApi {

	public $post_info;

	function __construct() {
		// JSONデータ受取
		$json_string = file_get_contents('php://input');
		$this->post_info = json_decode($json_string, true);
	}
	
	/**
	 * 第１引数の配列データから特定のkeyの値に合致する最初のデータを取得する
	 * @param type $data_list
	 * @param type $col_name キー要素名
	 * @param type $key キー
	 * @return boolean
	 */
	public function get_row_by_id($data_list, $col_name, $key) {
		foreach ($data_list as $row) {
			if ($row[$col_name] == $key) {
				return $row;
			}
		}
		return false;
	}
	

}
