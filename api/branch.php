<?php
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'api_base.php');
require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'common'. DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'RallyUserModel.php');
require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'common'. DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'AdminModel.php');


class ApiUser extends BaseApi{
	
	public $rallyUserModel;
	public $adminModel;
	
	function __construct() {
		parent::__construct();
		$this->rallyUserModel = new RallyUserModel();
		$this->adminModel = new AdminModel();
	}

	/**
	 * 支店情報取得
	 * 
	 * ['admin_id']:admin ID
	 * 
	 * @return
	 */
	public function info() {
		
		// jsonで受け取った値を変数に格納
		$admin_id = isset($this->post_info['admin_id']) ? $this->post_info['admin_id'] : '0';
		$user_id = isset($this->post_info['user_id']) ? $this->post_info['user_id'] : '0';
		$title = isset($this->post_info['title']) ? $this->post_info['title'] : '';
		$context = isset($this->post_info['context']) ? $this->post_info['context'] : '';
		
		print_r($this->post_info['admin_id']);
		echo "\n------------------\n";
		print_r($this->post_info['user_id']);
		echo "\n------------------\n";
		print_r($this->post_info['title']);
		echo "\n------------------\n";
		print_r($this->post_info['context']);
		echo "\n------------------\n";
		
		$db = db_connect();
		$rally_id = $this->noticeComp->get_rally_id($db);
		db_close($db);
		
		if (isset($rally_id) && !empty($user_id)) {
			$db = db_connect();
			$r_user_info = $this->rallyUserModel->get_userinfo_by_user_id_and_rally_id($db, $user_id, $rally_id);
			db_close($db);
		}
		
		return true;
	}

}

$apiNotice = new ApiUser();
call_user_func_array( array($apiNotice, $func), array());
