<?php
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'api_base.php');
require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'common'. DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'RallyUserModel.php');
require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'common'. DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'AdminModel.php');
require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'common'. DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'UserModel.php');
require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'common'. DIRECTORY_SEPARATOR . 'component' . DIRECTORY_SEPARATOR . 'NoticeComponent.php');

class ApiTsrest extends BaseApi{
	
	public $rallyUserModel;
	public $noticeComp;
	public $userModel;
	public $adminModel;
	
	function __construct() {
		parent::__construct();
		$this->rallyUserModel = new RallyUserModel();
		$this->adminModel = new AdminModel();
		$this->userModel = new UserModel();
		$this->noticeComp = new NoticeComponent();
	}
	
	public function demo() {
		
		$userData = [
			'demolist' =>
			[
				[
					'id' => '1',
					'title' => 'タイトル1',
					'detail' => 'title detail 1',
				],
				[
					'id' => '2',
					'title' => 'タイトル2',
					'detail' => 'title detail 2',
				],
			]
		];
		
		//jsonとして出力
		header('Content-type: application/json');
		echo json_encode($userData);
	}

	public function notice() {
		
		// アプリから取得を要求された時点で既読と皆す
		// 画面に表示させたらという判定が可能か？負荷がかかりそう。
		
		$page = filter_input(INPUT_GET, 'id');;
		if ($page == '0') {
			$userData = [
				'noticelist' =>
				[
					[
						'notice_id' => '100',
						'rally_id' => '1',
						'admin_id' => '90',
						'notice_data' => '2017/12/25 12:00',
						'notice_title' => 'タイトル1',
						'notice_content' => 'title detail 1',
					],
					[
						'notice_id' => '99',
						'rally_id' => '1',
						'admin_id' => '90',
						'notice_data' => '2017/12/25 12:00',
						'notice_title' => 'タイトル2',
						'notice_content' => 'title detail 2',
					],
					[
						'notice_id' => '98',
						'rally_id' => '1',
						'admin_id' => '90',
						'notice_data' => '2017/12/26 12:00',
						'notice_title' => 'タイトル3',
						'notice_content' => 'title detail 3',
					],
					[
						'notice_id' => '97',
						'rally_id' => '1',
						'admin_id' => '90',
						'notice_data' => '2017/12/26 12:00',
						'notice_title' => 'タイトル4',
						'notice_content' => 'title detail 4',
					],
					[
						'notice_id' => '96',
						'rally_id' => '1',
						'admin_id' => '90',
						'notice_data' => '2017/12/26 12:00',
						'notice_title' => 'タイトル5',
						'notice_content' => 'title detail 5',
					],
				]
			];
			
		} else if ($page == 96) {
			$userData = [
				'noticelist' =>
				[
					[
						'notice_id' => '95',
						'rally_id' => '1',
						'admin_id' => '90',
						'notice_data' => '2017/12/21 12:00',
						'notice_title' => 'タイトル6',
						'notice_content' => 'title detail 6',
					],
					[
						'notice_id' => '94',
						'rally_id' => '1',
						'admin_id' => '90',
						'notice_data' => '2017/12/21 12:00',
						'notice_title' => 'タイトル7',
						'notice_content' => 'title detail 7',
					],
					[
						'notice_id' => '93',
						'rally_id' => '1',
						'admin_id' => '90',
						'notice_data' => '2017/12/20 12:00',
						'notice_title' => 'タイトル8',
						'notice_content' => 'title detail 8',
					],
					[
						'notice_id' => '92',
						'rally_id' => '1',
						'admin_id' => '90',
						'notice_data' => '2017/12/20 12:00',
						'notice_title' => 'タイトル9',
						'notice_content' => 'title detail 9',
					],
					[
						'notice_id' => '96',
						'rally_id' => '1',
						'admin_id' => '90',
						'notice_data' => '2017/12/20 12:00',
						'notice_title' => 'タイトル10',
						'notice_content' => 'title detail 10',
					]
				]
			];
			
		} else {
			$userData = [
				'noticelist' =>[]
			];
		}
//		$userData = 
//			[
//				'id' => '1',
//				'title' => 'タイトル1',
//				'detail' => 'title detail 1',
//			];
		
		//jsonとして出力
		header('Content-type: application/json');
		echo json_encode($userData);
	}
	
	public function notice_to_any() {
		// jsonで受け取った値を変数に格納
		$admin_id = isset($this->post_info['admin_id']) ? $this->post_info['admin_id'] : '0';
		$user_id = isset($this->post_info['user_id']) ? $this->post_info['user_id'] : '0';
		$divide_num = isset($this->post_info['divide_num']) ? $this->post_info['divide_num'] : '0';
		$title = isset($this->post_info['title']) ? $this->post_info['title'] : '';
		$context = isset($this->post_info['context']) ? $this->post_info['context'] : '';
		
//		print_r($this->post_info['divide_num']);
//		echo "\n------------------\n";
//		print_r($this->post_info['admin_id']);
//		echo "\n------------------\n";
//		print_r($this->post_info['user_id']);
//		echo "\n------------------\n";
//		print_r($this->post_info['title']);
//		echo "\n------------------\n";
//		print_r($this->post_info['context']);
//		echo "\n------------------\n";
		
		// 顧客IDの先頭ヘッダが付いていたらトリムする
		if (is_array($user_id)) {
			$user_ids = $user_id;
			$user_id = [];
			foreach ($user_ids as $val) {
				$user_id[] = preg_replace('/[^0-9]/', '', $val);
			}
		} else {
			$user_id = preg_replace('/[^0-9]/', '', $user_id);
		}
		
		if (empty($user_id) && !empty($divide_num)) {
			// 個体識別IDからuser_idを取得する
			$db = db_connect();
			$user = $this->userModel->find_by_devide_num($db, $divide_num);
			
			print_r($user);
			
			$user_id = $user['user_id'];
			print_r("gene user_id:".$user_id);
			db_close($db);
			if (empty($user_id)) {return false;}
		}
		
		$db = db_connect();
		$rally_id = $this->noticeComp->get_rally_id($db);
		db_close($db);
		
		if (isset($rally_id) && !empty($user_id)) {
			$db = db_connect();
			$r_user_info = $this->rallyUserModel->get_users_by_user_id_and_rally_id($db, $user_id, $rally_id);
			db_close($db);
		}
		
		echo("r_user_info:".print_r($r_user_info, true));
		
		$user_id = [];
		foreach ($r_user_info as $value) {
			// 当該店舗で存在するuser_idのみ抽出
			$user_id[] = $value['user_id'];
		}
		
		if (isset($user_id) && count($user_id) > 0) {
			// 対象ユーザが存在するので、お知らせを作成してお知らせを配信する。
			
			// 支店IDを取得する
			$db = db_connect();
			$branch_id = $this->adminModel->get_branch_id_by_admin_id($db, $admin_id);
			db_close($db);
			
			error_log("branch_id:".print_r($branch_id, true));
			
			// 送信時点での送信可能な総ユーザ数を取得する。
			$db = db_connect();
			$count_users = $this->rallyUserModel->get_user_count($db, $rally_id, $branch_id);
			db_close($db);
			
			$deliverly_num = count($user_id);
			$count_users = empty($count_users) ? '0' : $count_users;
			$delivery_set_no = '7';			// 
			$notice_type = '5';				// 予約
			$now_day = date('Y-m-d H:i:s');	//配信日時
			
			$db = db_connect();
			$set = "rally_id = '".$rally_id."', "
				."notice_content  = '".$context."', "
				."admin_id  = '".$admin_id."', "
				."notice_data  = '".$now_day."', "
				."notice_title  = '".$title."', "
				."delivery_set  = '6',"
				."format  = '1', "
				."simple_last_month  = '',"
				."simple_this_month  = '', "
				."simple_next_month  = '', "
				."simple_sex  = '', "
				."yesterday_come  = '', "
				."month_come  = '', "
				."this_month_come  = '', "
				."individual_sex  = '', "
				."individual_birthday_start  = '', "
				."individual_birthday_end  = '', "
				."individual_region  = '', "
				."first_start  = '', "
				."first_end  = '', "
				."last_start  = '', "
				."last_end  = '', "
				."total_stamp_num  = '', "
				."total_stamp_terms  = '', "
				."delivery_num  = '".$deliverly_num."', "
				."notice_type  = '".$notice_type."', "
				."thumbnail  = '', "
				."total_user_count  = '".$count_users."', "
				."acceptance_datetime = now(), "
				."notice_schedule_id = '0'";
			
			notice_create($db, $set);
			$notice_id = mysql_insert_id();
			db_close( $db );
			if (empty($notice_id)) {
				print_r('error insert notice data.');
				return;
			}
			
			// 
			// ユーザリストを渡してpush or メールする共通関数にユーザリストを渡す
			
			$notice_data = array(
				'notice_id' => $notice_id,
				'notice_title' => $title,
				'notice_content' => $context,
				'now_day' => $now_day,
				'delivery_set' => $delivery_set_no,
				'format' => '1',
				'send_admin' => $admin_id ,
				'branch_id' => '' , // 個別配信での支店登録済み/未登録の条件
				'user_id_list' => $user_id
			);
			
			$p_admin_id = ADMIN_ID;
			
			print_r($notice_data);
			Util::notice_message_immediate($p_admin_id, $notice_data, $user_id);
		}
		
		return true;
	}

}

$apiNotice = new ApiTsrest();
call_user_func_array( array($apiNotice, $func), array());
