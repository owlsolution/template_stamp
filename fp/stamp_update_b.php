<?php
require_once('./../../config.php');
require_once('./../ad_config.php');
require "./syntax.php";
//個体識別番号取得関数
function mobileId() {
	if (isset($_SERVER['HTTP_X_DCMGUID'])) {  //ドコモ
		$mobile_id = $_SERVER['HTTP_X_DCMGUID'];
	} else if (isset($_SERVER['HTTP_X_UP_SUBNO'])) {  //au
		$mobile_id = $_SERVER['HTTP_X_UP_SUBNO'];
	} else if (isset($_SERVER['HTTP_X_JPHONE_UID'])) { //ソフトバンク
		$mobile_id = $_SERVER['HTTP_X_JPHONE_UID'];
	}
	return $mobile_id;
}
$ident_id = mobileId();
if($ident_id == ""){
	exit;
}
$issue = $_GET['issue'];  //ラリー暗号
//$issue = base64_decode($issue);
$str = split(":",$issue);
$rally_id = $str[0];  //ラリーID
$ident_id .=  $rally_id;  //個体識別 = 個体識別ID+ラリーID
$stri = split(",", $str[1]);
$issue_date = $stri[0];  //日付
$strin = split(";", $stri[1]);
$issue_pass = $strin[0];//パス
$string = split("%", $strin[1]);
$s_num = $string[0];  //個数
$strings = split("/", $string[1]);
$shop_num = $strings[0];  //店番
$kind = $strings[1];// スタンプ種別
if($ident_id != ""){
	$user_id = get_identification_id($ident_id);
} else {
	exit;
}
if($user_id == ""){
	$store = 1;
	header("Location:./new_stamp.php?guid=ON&rally_id={$rally_id}&store={$store}");
	exit;
}

/*
 * スタンプ付与する前のユーザーが持っているスタンプ数を取得する
 */
$rally_user_fp = user_information_acquisition($rally_id , $user_id);
//スタンプ数
$stamp_num = $rally_user_fp['stamp_num'];

//店舗のスタンプMAX数を取得
$rally_fp = larry_information_acquisition($rally_id);
$stamp_max = $rally_fp['stamp_max'];

//$issue_date = date('Y-m-d H:i:s');
//$up_result = Stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num);
if ($kind == 2) {
	// 種別が2の場合、月極共通スタンプ(月に１回、１ユーザ、１回、全店舗共通)
	// $stamp_num = ユーザーの所持スタンプ数
	$up_result = monthly_stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num, $kind, $stamp_num);
} else if ($kind == 3) {
	// 種別が3の場合、有効期限日数指定スタンプ(１週間に１回、１ユーザ、１回、全店舗共通)
	$up_result = day_duration_stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num, $kind, $stamp_num);
} else if ($kind == 4) {
	// 種別が4の場合、有効期限無期限 複数ユーザが同じQRコードを利用可能 、１時間で再発行可能、全店舗共通
	$up_result = eternal_duration_stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num, $kind, $stamp_num);
} else {
	// デフォでは通常のスタンプ追加処理
	$up_result = Stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num , $stamp_num);
}

if($up_result == "ok"){
	//スタンプ合計数
	$update_stamp_num = $stamp_num+$s_num;
	//現在日時
	$now_date = date('Y-m-d');  //今日の日付
	$now_date2 = date('Y-m-d H:i:s');
	
	
	//クーポン発行
	$get_coupon_state = 0;
	$goal = page_setup_acquisition($rally_id);
	$get_user_coupon = "";
	for($i = $stamp_num +1; $i <= $update_stamp_num; $i++){
		// スタンプ最大数で割ったあまりが、現時点のスタンプ数。周回でリセットするため
		$current_stamp_pos = (($i-1)%$stamp_max)+1;
		for($coupon_no = 1; $coupon_no <= 10; $coupon_no++){
			$get_coupon_name = $goal['goal_title_'.$coupon_no];   //クーポン名
			$coupon_stamp_num = $goal['goal_stamp_num_'.$coupon_no];   //クーポンスタンプ数
			$get_coupon_description = $goal['goal_description_'.$coupon_no];  //クーポン内容
			$get_coupon_img = $goal['img_name_'.$coupon_no];  //クーポン画像名
			if($current_stamp_pos == $coupon_stamp_num){
				$get_user_info[] = new_coupon_b($rally_id , $user_id , $coupon_stamp_num , $get_coupon_name , $get_coupon_description , $get_coupon_img , $get_coupon_state , $now_date2 , $now_date);
			}
		}
	}
	$congrats_text_flg = "0";
	//スタンプ数がMAXを超えた時、スタンプをリセットする。
	if($update_stamp_num > $stamp_max){
		//リセット後のスタンプ数
		$reset_stamp_num = $update_stamp_num%$stamp_max;
		$db = db_connect();
		$set = "stamp_num = ".$reset_stamp_num." , last_stamp_date = '".$now_date2."'";
		$where = "user_id = ".$user_id." AND rally_id = ".$rally_id." AND admin_id = ".ADMIN_ID;
		rally_user_up($db , $set ,$where);
		db_close( $db );
//		//スタンプリセット後、クーポン取得スタンプ数に到達していたら、クーポンを発行
//		for($i = 1; $i <= $reset_stamp_num; $i++){
//			for($coupon_no = 1; $coupon_no <= 10; $coupon_no++){
//				$get_coupon_name = $goal['goal_title_'.$coupon_no];   //クーポン名
//				$coupon_stamp_num = $goal['goal_stamp_num_'.$coupon_no];   //クーポンスタンプ数
//				$get_coupon_description = $goal['goal_description_'.$coupon_no];   //クーポン内容
//				$get_coupon_img = $goal['img_name_'.$coupon_no];   //クーポン画像名
//				if($i == $coupon_stamp_num){
//					$get_user_info[] .= new_coupon_b($rally_id , $user_id , $coupon_stamp_num , $get_coupon_name , $get_coupon_description , $get_coupon_img , $get_coupon_state , $now_date2 , $now_date);
//				}
//			}
//		}
		//スタンプMAX数を超えた時におめでとうポップアップを表示させる
		if($update_stamp_num > $stamp_max){
			$congrats_text_flg = "1";
		}
	}
	for($i = 0 ; $i < count($get_user_info); $i++){
		$coupon_num_array[] = $get_user_info[$i];
	}
}

require "./page/stamp.php";