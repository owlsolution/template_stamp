<?php
require_once('./../../config.php');
require_once('./../ad_config.php');
require "./syntax.php";
//個体識別番号取得関数
function mobileId() {
	if (isset($_SERVER['HTTP_X_DCMGUID'])) {  //ドコモ
		$mobile_id = $_SERVER['HTTP_X_DCMGUID'];
	} else if (isset($_SERVER['HTTP_X_UP_SUBNO'])) {  //au
		$mobile_id = $_SERVER['HTTP_X_UP_SUBNO'];
	} else if (isset($_SERVER['HTTP_X_JPHONE_UID'])) { //ソフトバンク
		$mobile_id = $_SERVER['HTTP_X_JPHONE_UID'];
	}
	return $mobile_id;
}
$ident_id = mobileId();
if($ident_id == ""){
	exit;
}
$issue = $_GET['issue'];  //ラリー暗号
//$issue = base64_decode($issue);
$str = split(":",$issue);
$rally_id = $str[0];  //ラリーID
$ident_id .=  $rally_id;  //個体識別 = 個体識別ID+ラリーID
$stri = split(",", $str[1]);
$issue_date = $stri[0];  //日付
$strin = split(";", $stri[1]);
$issue_pass = $strin[0];//パス
$string = split("%", $strin[1]);
$s_num = $string[0];  //個数
$strings = split("/", $string[1]);
$shop_num = $strings[0];  //店番
$kind = $strings[1];// スタンプ種別
if($ident_id != ""){
	$user_id = get_identification_id($ident_id);
} else {
	exit;
}
if($user_id == ""){
	$store = 1;
	header("Location:./new_stamp.php?guid=ON&rally_id={$rally_id}&store={$store}");
	exit;
}

/*
 * スタンプ付与する前のユーザーが持っているスタンプ数を取得する
 */
$rally_user_fp = user_information_acquisition($rally_id , $user_id);
//スタンプ数
$stamp_num_fp = $rally_user_fp['stamp_num'];

//店舗のスタンプMAX数を取得
$rally_fp = larry_information_acquisition($rally_id);
$stamp_max_fp = $rally_fp['stamp_max'];

//$issue_date = date('Y-m-d H:i:s');
//$up_result = Stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num);

if ($kind == 2) {
	// 種別が2の場合、月極共通スタンプ(月に１回、１ユーザ、１回、全店舗共通)
	// $stamp_num = ユーザーの所持スタンプ数
	$up_result = monthly_stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num, $kind, $stamp_num_fp);
} else if ($kind == 3) {
	// 種別が3の場合、有効期限日数指定スタンプ(１週間に１回、１ユーザ、１回、全店舗共通)
	$up_result = day_duration_stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num, $kind, $stamp_num_fp);
} else {
	// デフォでは通常のスタンプ追加処理
	$up_result = Stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num , $stamp_num_fp);
}

require "./page/stamp.php";
