<?php
//＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊ページ情報取得＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
function larry_page_acquisition($rally_id){
	$db = db_connect();
	$where = "rally_id = ".$rally_id;
	$stamp_page_date = stamp_page_select($db , $where);
	$stamp_page = mysql_fetch_array($stamp_page_date);
	db_close( $db );
	return ($stamp_page);
}
//＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊ユーザー情報取得＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
function user_information_acquisition($rally_id , $user_id){
	$db = db_connect();
	$where = "rally_id = ".$rally_id." AND user_id =".$user_id;
	$rally_user_date = rally_user_select($db , $where);
	$rally_user = mysql_fetch_array($rally_user_date);
	db_close( $db );
	return ($rally_user);
}
//＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊ラリー情報取得＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
function larry_information_acquisition($rally_id){
	$db = db_connect();
	$where = "rally_id = ".$rally_id;
	$rally_date = rally_select($db , $where);
	$rally = mysql_fetch_array($rally_date);
	db_close( $db );
	return ($rally);
}
//＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊クーポン設定取得＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
function page_setup_acquisition($rally_id){
	$db = db_connect();
	$where = "rally_id = ".$rally_id;
	$goal_date = goal_select($db , $where);
	$goal = mysql_fetch_array($goal_date);
	db_close( $db );
	return ($goal);
}
//＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊お知らせ情報＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
function  notice_information_acquisition($rally_id , $str_notice , $user_id , $notice_note){
	
	// 支店を含むadmin idのリストを取得
	$db = db_connect();
	$admin_list = array();
	$admin_list[] = ADMIN_ID;

	// user_idでbranch_idを取得する。
	$where_branch_id = "user_id = ".$user_id;
	$rally_user_data = rally_user_select($db , $where_branch_id);
	$user_info = mysql_fetch_array($rally_user_data);

	/*
	 * 支店で配信しているお知らせ
	 */
	if($user_info['branch_id'] != NULL){
	  $branch_id = $user_info['branch_id'];
	  // 支店のadmin_id
	  $admin_list[] = getChildAdminIdByBranchId($db, $branch_id);
	}

	/*
	 * 店員で配信しているお知らせ
	 */
	// 店員のadmin_idを追加
	$branch_id_list[] = "0";
	if($user_info['branch_id'] != NULL){
	  //ユーザーがプロフィール登録済の場合
	  //本部と支店に所属している店員のadmin_idを取得
	  $branch_id_list[] = $user_info['branch_id'];
	}
	$where = "rally_id = '".$rally_id."' AND status = '1' AND branch_id IN (".implode(",", $branch_id_list).") ";
	$staff_data = staff_select($db , $where);
	while ($staff = mysql_fetch_array($staff_data)){
	  $admin_list[] = $staff['admin_id'];
	}

	// admin idの条件生成
	$where_admin = "admin_id IN (".implode(",", $admin_list).")";
	db_close( $db );
	
	$notice_where = "";
	if(!empty($notice_note)){
		$notice_where .= $where_admin." AND (notice_type IN(2) OR notice_id IN(".$notice_note.")) AND notice_data < now() ";
	}else {
		$notice_where .= $where_admin." AND (notice_type IN(2)) AND notice_data < now() ";
	}
	
	$notice_content = "";
	$db = db_connect();
	$where = $notice_where." ORDER BY notice_id DESC LIMIT 0 , 3";
	$notice_date = notice_select($db , $where);
	while ($notice = mysql_fetch_array($notice_date)){
		$notice_medium = "<a href = './?guid=ON&p=notice&rally_id=".$rally_id."&user_id=".$user_id."' >".$notice['notice_title']."</a>";
		$notice_content .= str_replace("#notice#", $notice_medium , $str_notice);
	}
	db_close( $db );
	return ($notice_content);
}
//＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊スタンプ台設定＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
function ink_pad_set($rally_id , $stamp_num){
	$db = db_connect();
	$where = "rally_id = ".$rally_id." AND is_stamp = 0";
	$order = "stamp_th";
	$stamp_date = stamp_select_fp($db , $where , $order);
	$num = 1;
	while ($stamp = mysql_fetch_array($stamp_date)){
		if($stamp_num < $num){
			$img[$num] = "./../stamp_before_fp/".$stamp['before_img_name'];
		} else {
			$img[$num] = "./../stamp_after_fp/".$stamp['after_img_name'];
		}
		$num++;
	}
	db_close( $db );
	return ($img);
}
//クーポン発行させる
function new_coupon($rally_id , $user_id , $stamp_num , $get_coupon_name , $get_coupon_description , $get_coupon_img , $get_coupon_state , $now_date2 , $now_date){
	$rally_date = larry_information_acquisition($rally_id);
	$goal_day = $rally_date['goal_day'];
	$coupon_use_end =  date("Y-m-d H:i:s", strtotime("+".$goal_day." day"));// クーポン使用終了日
	$db = db_connect();
	$into = $rally_id." , ".$user_id." , ".$stamp_num." , '".$get_coupon_name."' , '".$get_coupon_description."' , '".$get_coupon_img."' , ".$get_coupon_state." , '".$now_date2."' , '".$now_date."' , '".$now_date2."' , '".$coupon_use_end."'";
	coupon_insert($db , $into);
	db_close( $db );
	$db = db_connect();
	$where = "rally_id = ".$rally_id." AND user_id = ".$user_id;
	$set = "stamp_num = stamp_num - ".$stamp_num;
	rally_user_up($db , $set ,$where);
	db_close( $db );
}
//クーポン発行させる（typeB版）
function new_coupon_b($rally_id , $user_id , $stamp_num , $get_coupon_name , $get_coupon_description , $get_coupon_img , $get_coupon_state , $now_date2 , $now_date){
	$rally_date = larry_information_acquisition($rally_id);
	$goal_day = $rally_date['goal_day'];
	$coupon_use_end =  date("Y-m-d H:i:s", strtotime("+".$goal_day." day"));// クーポン使用終了日
	$db = db_connect();
	$into = $rally_id." , ".$user_id." , ".$stamp_num." , '".$get_coupon_name."' , '".$get_coupon_description."' , '".$get_coupon_img."' , ".$get_coupon_state." , '".$now_date2."' , '".$now_date."' , '".$now_date2."' , '".$coupon_use_end."'";
	coupon_insert($db , $into);
	db_close( $db );
	return $stamp_num;
}
//取得済みクーポン情報取得
function get_coupon_information($db , $rally_id , $user_id){
	// 現在時刻を取得
	$now_date = date("Y-m-d H:i:s");
	// 本日の最初の時間を取得
	$date_start_time = Util::get_rally_start_time_of_day($now_date, $rally_id);
	// 本日の最終時間を取得
	$date_end_time = Util::get_rally_end_time_of_day($now_date, $rally_id);
	$where = "rally_id = ".$rally_id." AND user_id = ".$user_id." AND get_coupon_state = 0 AND coupon_use_start <= '".$date_end_time."' AND coupon_use_end >= '".$date_start_time."'";
	$order = "acquisition_date";
	$get_coupon_date = get_coupon_select($db ,$where , $order);
	return ($get_coupon_date);
}
//個体識別検索
function get_identification_id($ident_id){
	$db = db_connect();
	$where = "divide_num = '".$ident_id."'";
	$get_identification_date = get_identification_select($db ,$where);
	$get_identification = mysql_fetch_array($get_identification_date);
	$get_identification_id = $get_identification['user_id'];
	//echo $get_identification_id;
	db_close( $db );
	return ($get_identification_id);
}

function get_rally_user_id($user_id , $rally_id , $classification, $device_id = ""){
	$db = db_connect();
	$where = "user_id = ".$user_id." AND rally_id = ".$rally_id." AND is_auth = 0";
	$rally_user_date = rally_user_select($db , $where);
	$rally_user_num = mysql_num_rows($rally_user_date);
	db_close( $db );
	$now_date = date('Y-m-d');
	$now_date2 = date('Y-m-d H:i:s');
	if($rally_user_num == 0){
		// ユーザが存在しない場合、ユーザを作成する
		$db = db_connect();
		$into = $user_id." , ".ADMIN_ID." , ".$rally_id." , 0 , 0 , '".$now_date2."' , '".$now_date."' , '".$now_date."' , 2 , '".$now_date2."'";
		rally_user_insert($db , $into);
		$user_primary_key = mysql_insert_id();
		error_log("user_primary_key : ".$user_primary_key);
		db_close( $db );
		
		// ラリー設定で初期スタンプ付与が必要であれば、付与する
		$new_stamp_num = check_new_stamp_num($rally_id);
		if ($new_stamp_num > 0) {
			$db = db_connect();
			$set = "stamp_num = ".$new_stamp_num." , total_stamp_num = ".$new_stamp_num." , last_stamp_date = '".$now_date2."'";
			$where = "rally_user_id = ".$user_primary_key;
			rally_user_up($db , $set ,$where);
			db_close( $db );
			
			$db = db_connect();
			$issue_date = date('Y-m-d H:i:s');
			$shop_num = 1;
			$issue_pass = uniqid();
			$into = $user_id." , ".$rally_id." , '".$issue_pass."' , '".$issue_date."' , ".$new_stamp_num." , ".$shop_num;
			stamp_history_insert($db , $into);
			db_close( $db );
		}
		// 初回プレゼントクーポンの登録処理
		if(!empty($device_id)) {
			// uuidをデバイステーブルを検索
			$db = db_connect();
			$where = "rally_id = '".$rally_id."' AND device_id = '".$device_id."'";
			$device_data = device_select($db , $where);
			$device  = mysql_fetch_array($device_data);
			db_close($db);
			
			error_log("device_count:".$device['install_count']);
			if ($device['install_count'] == 0) {
				// 合致するidがなければ付与
				// 初期クーポンの登録処理
				first_gift_insert($user_id , $rally_id);
				error_log("device_count1:".$device['install_count']);
			}
			
			if ($device['install_count'] >= 0) {
				// uuidをデバイステーブルに保存
				// 端末情報を保存する
				$db = db_connect();
				$where = "rally_id = '".$rally_id."' AND device_id = '".Util::sanitize_sql($device_id)."'";
				$set = "install_count = install_count+1, user_id = '".$user_id."'";
				device_update($db, $set, $where);
				db_close($db);
			}
		}
	}
}


// 初回プレゼントクーポンの生成
function first_gift_insert($user_id , $rally_id){
        // 初回クーポン情報の取得
        $db = db_connect();
        $where = " rally_id = ".$rally_id." AND first_flag = 1";
        $gift_coupon_date = gift_coupon_select($db , $where);
        while ($row = mysql_fetch_array($gift_coupon_date)){
            $gift_coupon_array[] = $row;
        }
        db_close( $db );
        
        // 初回クーポン情報がある場合
        if(count($gift_coupon_array) != 0){
            // id設定
            $present_coupon_id = $gift_coupon_array[0]['id'];
            // クーポン期限の算出＆設定
            $coupon_start = date("Y-m-d H:i:s");
            $coupon_end = date("Y-m-d H:i:s",strtotime("+".($gift_coupon_array[0]['coupon_deadline_day'])." day"));
            
            $db = db_connect();
            $into = $rally_id." , '".$user_id."' , '".$gift_coupon_array[0]['coupon_name']."' , 'gift_coupon.jpg' , '".date("Y-m-d")."' , 2 , '".$coupon_start."' , '".$coupon_end."' , '".date("Y-m-d H:i:s")."' , '".$gift_coupon_array[0]['disp_type']."' , ".$present_coupon_id."";
            gift_coupon_user_insert($db , $into);
            db_close( $db );
        }
}


function issue_up($user_id , $rally_id){
	$db = db_connect();
	$where = "user_id = ".$user_id." AND rally_id = ".$rally_id." AND admin_id = ".ADMIN_ID;
	rally_user_delete($db , $where);
	db_close( $db );
}
//スタンプ更新
function Stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num){
	// 現時刻を取得
	$add_date = date('Y-m-d H:i:s');
	
	// １日１回付与チェックフラグを取得
	$db = db_connect();
	$where = "rally_id = ".$rally_id ;
	$rally_date = rally_select($db , $where);
	$rally = mysql_fetch_array($rally_date);
	$one_times_a_day = $rally['one_times_a_day'];
	db_close( $db );

	//１日１回付与チェック
	if($one_times_a_day == 1){
		// １日１回しか付与できない設定の場合、
		// すでにスタンプを取得しているかを判断する

		// ラリーユーザ情報から最終スタンプ日時を取得する
		$db = db_connect();
		$where = "rally_id = ".$rally_id." AND user_id = '".$user_id."' LIMIT 0,1";
		$rally_user_data = rally_user_select($db, $where);
		$rally_user = mysql_fetch_array($rally_user_data);
		db_close( $db );
		
		// 最終スタンプ日時から、最終スタンプ日を算出
		$last_stamp_parts = Util::get_rally_of_day($rally_user['last_stamp_date'], $rally_id);
		$last_stamp_day = $last_stamp_parts[0]."-".$last_stamp_parts[1]."-".$last_stamp_parts[2];
		// 本日取得
		$today_parts = Util::get_rally_of_day($add_date, $rally_id);
		$today = $today_parts[0]."-".$today_parts[1]."-".$today_parts[2];
		// 最終スタンプ日と本日が同日かつ、合計スタンプ数が1以上ならスタンプは押せない。(合計スタンプ数が0の場合初期スタンプ0の当日なので押せるようにする)
		if (($last_stamp_day == $today) && ($rally_user['total_stamp_num'] > 0)) {
			$result = "one_day_ng";
			return ($result);
		}
	}
	
	// ワンタイムパスワードチェック
	$db = db_connect();
	$where = "rally_id = ".$rally_id." AND stamp_pass = '".$issue_pass.$issue_date."'";
	$check_count = stamp_history_select($db , $where);
	db_close( $db );
	if($check_count > 0){
		// 使用済みのQRコードの場合
		$result = "qr_ng";
		return ($result);
	}
	
	// 各チェックを通過しているのでスタンプ付与する
	// スタンプ履歴へインサート
	$db = db_connect();
	$into = $user_id." , ".$rally_id." , '".$issue_pass.$issue_date."' , '".$add_date."' , ".$s_num." , ".$shop_num;
	stamp_history_insert($db , $into);
	db_close( $db );
	// ラリーユーザのスタンプ数のカウントアップと最終スタンプ日時の更新
	$db = db_connect();
	$set = "stamp_num = stamp_num + ".$s_num." , total_stamp_num = total_stamp_num + ".$s_num." , last_stamp_date = '".$add_date."'";
	$where = "user_id = ".$user_id." AND rally_id = ".$rally_id." AND admin_id = ".ADMIN_ID;
	rally_user_up($db , $set ,$where);
	db_close( $db );
	$result = "ok";
	return ($result);
}
/**
 * 月別スタンプ更新
 * @param type $user_id
 * @param type $rally_id
 * @param type $issue_date スタンプ対象日
 * @param type $issue_pass パス一意に特定するキー
 * @param type $s_num スタンプ付与数
 * @param type $shop_num 未使用
 * @param type $kind スタンプのタイプ
 * @param type $stamp_num 付与前のユーザ保持スタンプ数
 * @return type
 */
function monthly_stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num, $kind, $stamp_num){
	$exec_month = date('Y-n');
	//プロフィール登録要求度の設定
	$profile_force = get_profile_force($rally_id);
	error_log("プロフィール要求度チェック : ".$profile_force);
    //プロフィール登録チェック
	$db = db_connect();
	$where = "user_id = ".$user_id;
	$all_user = all_user_select($db , $where);
	$user = mysql_fetch_array($all_user);
	db_close( $db );
	$user_check = "end";
	if(empty($user['user_name']) || empty($user['sex'])){
	    // 名前、誕生日、地域、性別のいずれかが未設定の場合、YETを設定する
	    $user_check = "yet";
	}
	$goal = page_setup_acquisition($rally_id);
	$coupon_first_stamp_num = $goal['goal_stamp_num_1'];
	//スタンプ合計数
	$update_stamp_num = $stamp_num+$s_num;
	if(strtotime($exec_month) < strtotime($issue_date)){
    	// 今の月とQRコードの月が一致しなければ、使用不可
        $result = "future_date_qr_ng";
        return ($result);
    } else if(strtotime($exec_month) > strtotime($issue_date)){
        // 現在日付より未来の日付は、使用不可
        $result = "ago_date_qr_ng";
        return ($result);
    } else if(strtotime($exec_month) != strtotime($issue_date)){
        // 現在日付より未来の日付は、使用不可
        $result = "not_date_qr_ng";
        return ($result);
    } else if($profile_force == 5){
		if($user_check == "yet" && $update_stamp_num >= $coupon_first_stamp_num){
	    	//プロフィール未登録の場合
			$result = "unregistered_profile";
			return ($result);
		} else {
			// 当月のMonthlyStampを使用済かどうかをチェック
			$db = db_connect();
			$where = "rally_id = ".$rally_id." AND user_id = '".$user_id."' AND kind = '2' AND stamp_pass = '".$issue_pass."'";
			$check_count = stamp_history_select($db , $where);
			db_close( $db );
			if($check_count == 0){
		            // 当月使用可能なQRコードが未使用なので追加する
		            $db = db_connect();
		            $add_date = date('Y-m-d H:i:s');
		            $into = $user_id." , ".$rally_id." , '".$issue_pass."' , '".$add_date."' , ".$s_num." , ".$shop_num.", '".$kind."'";
		            stamp_history_monthly_insert($db , $into);
		            db_close( $db );
		            //$issue_pass = date('Y-m-d');
		            $db = db_connect();
		            $set = "stamp_num = stamp_num + ".$s_num." , total_stamp_num = total_stamp_num + ".$s_num." , last_stamp_date = '".$add_date."'";
		            $where = "user_id = ".$user_id." AND rally_id = ".$rally_id." AND admin_id = ".ADMIN_ID;
		            rally_user_up($db , $set ,$where);
		            db_close( $db );
		            $result = "ok";
		            return ($result);
			} else {
					$result = "chk_cnt_qr_ng";
					return ($result);
			}
		}
	} else {	
		// 当月のMonthlyStampを使用済かどうかをチェック
		$db = db_connect();
		$where = "rally_id = ".$rally_id." AND user_id = '".$user_id."' AND kind = '2' AND stamp_pass = '".$issue_pass."'";
		$check_count = stamp_history_select($db , $where);
		db_close( $db );
		if($check_count == 0){
	            // 当月使用可能なQRコードが未使用なので追加する
	            $db = db_connect();
	            $add_date = date('Y-m-d H:i:s');
	            $into = $user_id." , ".$rally_id." , '".$issue_pass."' , '".$add_date."' , ".$s_num." , ".$shop_num.", '".$kind."'";
	            stamp_history_monthly_insert($db , $into);
	            db_close( $db );
	            //$issue_pass = date('Y-m-d');
	            $db = db_connect();
	            $set = "stamp_num = stamp_num + ".$s_num." , total_stamp_num = total_stamp_num + ".$s_num." , last_stamp_date = '".$add_date."'";
	            $where = "user_id = ".$user_id." AND rally_id = ".$rally_id." AND admin_id = ".ADMIN_ID;
	            rally_user_up($db , $set ,$where);
	            db_close( $db );
	            $result = "ok";
	            return ($result);
		} else {
				$result = "chk_cnt_qr_ng";
				return ($result);
		}
	}
}

/**
 * 日数指定スタンプ更新
 * @param type $user_id
 * @param type $rally_id
 * @param type $issue_date QRコード有効期限開始日
 * @param type $issue_pass 日数
 * @param type $s_num 取得スタンプ数
 * @param type $shop_num 未使用
 * @param type $kind 種別
 * @param type $stamp_num 付与前のスタンプ数
 * @return type
 */
function day_duration_stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num, $kind, $stamp_num){
	$exec_month = date('Y-m-d');
	
	// QRコード期限日
	$limited_date = date("Y-m-d", strtotime("+".($issue_pass - 1)." day", strtotime($issue_date)));
	
	//プロフィール登録チェック
	$db = db_connect();
	$where = "user_id = ".$user_id;
	$all_user = all_user_select($db , $where);
	$user = mysql_fetch_array($all_user);
	db_close( $db );
	
	$user_check = "end";
	if(empty($user['user_name']) || empty($user['sex'])){
		// 名前、誕生日、地域、性別のいずれかが未設定の場合、YETを設定する
		$user_check = "yet";
	}
	
	$goal = page_setup_acquisition($rally_id);
	$coupon_first_stamp_num = $goal['goal_stamp_num_1'];
	
	//スタンプ合計数
	if(strtotime($issue_date) > strtotime($exec_month)){
		// まだ開始していない
		$result = "day_duration_future_date_qr_ng";
		return ($result);
	} else if( strtotime($limited_date) < strtotime($exec_month)){
		// 期限を過ぎている
		$result = "day_duration_ago_date_qr_ng";
		return ($result);
	}
	
	// ユーザが今回と同じ起点日のQRコードを使用済かどうかをチェック
	$db = db_connect();
	$where = "rally_id = ".$rally_id." AND user_id = '".$user_id."' AND kind = '".$kind."' AND stamp_pass = '".$issue_date."#".$issue_pass."'";
	error_log($where);
	$check_count = stamp_history_select($db, $where);
	error_log($check_count);
	db_close( $db );
	
	if ($check_count == 0) {
		// 使用可能なQRコードが未使用なので追加する
		$db = db_connect();
		$add_date = date('Y-m-d H:i:s');
		$into = $user_id . " , " . $rally_id . " , '"  .$issue_date."#".$issue_pass . "' , '" . $add_date . "' , " . $s_num . " , " . $shop_num . ", '" . $kind . "'";
		stamp_history_monthly_insert($db, $into);
		db_close($db);

		//$issue_pass = date('Y-m-d');
		$db = db_connect();
		$set = "stamp_num = stamp_num + " . $s_num . " , total_stamp_num = total_stamp_num + " . $s_num . " , last_stamp_date = '" . $add_date . "'";
		$where = "user_id = " . $user_id . " AND rally_id = " . $rally_id . " AND admin_id = " . ADMIN_ID;
		rally_user_up($db, $set, $where);
		db_close($db);
		$result = "ok";
		return ($result);
	} else {
		$result = "chk_cnt_qr_ng";
		return ($result);
	}
}

/**
 * 期限なし、１時間で再発行可能スタンプ更新
 * @param type $user_id
 * @param type $rally_id
 * @param type $issue_date キーコード
 * @param type $issue_pass 無効期間
 * @param type $s_num 取得スタンプ数
 * @param type $shop_num 未使用
 * @param type $kind 種別
 * @return type
 */
function eternal_duration_stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num, $kind, $stamp_num) {
	
//	$key = "HiZ6x-PqLU64!";
	if( ETERNAL_SALT_KEY != $issue_date) {
		// 無効なQRコード
		$result = "eternal_duration_invalid_qr_ng";
		return ($result);
	}
	
	$exec_month = date('Y-m-d');
	
	// QRコードの再発行不可期限日から逆算した最終発行日時。これよりも古ければ、発行可能。
	$limited_date = date("Y-m-d H:i:s", strtotime("-".($issue_pass)." hour"));
	
	//プロフィール登録チェック
	$db = db_connect();
	$where = "user_id = ".$user_id;
	$all_user = all_user_select($db , $where);
	$user = mysql_fetch_array($all_user);
	db_close( $db );
	
	$user_check = "end";
	if(empty($user['user_name']) || empty($user['sex'])){
		// 名前、誕生日、地域、性別のいずれかが未設定の場合、YETを設定する
		$user_check = "yet";
	}
	
	$goal = page_setup_acquisition($rally_id);
	$coupon_first_stamp_num = $goal['goal_stamp_num_1'];
	
	// ユーザが制限時間内にスタンプを取得しているかどうかをチェック
	$db = db_connect();
	$where = "rally_id = ".$rally_id." AND user_id = '".$user_id."' AND kind = '".$kind."' AND stamp_pass = '"."#".$issue_date."' AND add_date > '".$limited_date."'";
	error_log("pancake!:".$where);
	$check_count = stamp_history_select($db, $where);
	error_log($check_count);
	db_close( $db );
	
	if ($check_count == 0) {
		// 発行していないのであれば、インサートする
		$db = db_connect();
		$add_date = date('Y-m-d H:i:s');
		$into = $user_id . " , " . $rally_id . " , '" . "#".$issue_date . "' , '" . $add_date . "' , " . $s_num . " , " . $shop_num . ", '" . $kind . "'";
		stamp_history_monthly_insert($db, $into);
		db_close($db);

		//$issue_pass = date('Y-m-d');
		$db = db_connect();
		$set = "stamp_num = stamp_num + " . $s_num . " , total_stamp_num = total_stamp_num + " . $s_num . " , last_stamp_date = '" . $add_date . "'";
		$where = "user_id = " . $user_id . " AND rally_id = " . $rally_id . " AND admin_id = " . ADMIN_ID;
		rally_user_up($db, $set, $where);
		db_close($db);
		$result = "ok";
		return ($result);
	} else {
		// 制限時間
		$result = "chk_limited_qr_ng";
		return ($result);
	}
}

//機種変の為の情報保存
function Model_change($user_id , $id , $pass){
	$now_date = date('Y-m-d H:i:s');
	$db = db_connect();
	$into = $user_id." , '".$id."' , '".$pass."' , '".$now_date."'";
	model_change_insert($db , $into);
	db_close( $db );
}
//プロフィールを登録
function profile_make($rally_id , $user_id , $ident_id , $classification , $mail , $name , $sex , $birth_date , $region){
	$db = db_connect();
	$where = "user_id = ".$user_id." AND divide_num = '".$ident_id."'";
	$set = "mail_addr = '".$mail."' , user_name = '".$name."' , sex =".$sex." , birth_date = '".$birth_date."' , region = '".$region."'";
	user_up($db , $set , $where);
	db_close( $db );
}
//プロフィール検索
function profile_search($rally_id , $ident_id){
	$db = db_connect();
	$where = "divide_num = '".$ident_id."' AND admin_id =".ADMIN_ID;
	$all_user_date = all_user_select($db , $where);
	$all_user = mysql_fetch_array($all_user_date);
	db_close( $db );
	return ($all_user);
}
//機種変のカウント確認
function model_change_count_search($inp_id , $inp_pass){
	$db = db_connect();
	$where = "inp_id = '".$inp_id."' AND inp_pass = '".$inp_pass."'";
	$model_change_count_date = model_change_count($db , $where);
	$model_change_count = mysql_fetch_array($model_change_count_date);
	db_close( $db );
	$count = $model_change_count['count(*)'];
	return ($count);
}
//機種変の処理
function model_change_date_search($inp_id , $inp_pass , $ident_id){
	//変更前のIDを取得
	$db = db_connect();
	$where = "inp_id = '".$inp_id."' AND inp_pass = '".$inp_pass."'";
	$model_change_date = model_change_select_date($db , $where);
	$model_change = mysql_fetch_array($model_change_date);
	$before_user_id = $model_change['user_id'];  //過去のID
	db_close( $db );
	//今の情報を取得
	$db = db_connect();
	$where = "divide_num = '".$ident_id."'";
	$user_date = all_user_select($db , $where);
	$user = mysql_fetch_array($user_date);
	$push_pass = $user['push_pass'];  //今のプッシュパス
	$after_user_id = $user['user_id'];  //新しいユーザーID
	db_close( $db );
	//echo $push_pass;
	if($before_user_id != $after_user_id && $before_user_id != "" && $after_user_id != ""){
		//今のIDを削除
		$db = db_connect();
		$where = "user_id = ".$after_user_id;
		delete_user($db , $where);
		db_close( $db );
		//ラリー削除
		$db = db_connect();
		$where = "user_id = ".$after_user_id;
		rally_user_delete($db , $where);
		db_close( $db );
		
		$db = db_connect();
		$where = "user_id = ".$before_user_id;
		$set = "classification = '2'";
		rally_user_update($db, $set, $where);
		db_close( $db );
		
		//情報変更
		$db = db_connect();
		$where = "user_id = ".$before_user_id;
		$set = "divide_num = '".$ident_id."' , push_pass = '".$push_pass."'";
		user_up($db , $set , $where);
		db_close( $db );
	}

}
//新規スタンプ数
function check_new_stamp_num($rally_id){
	$db = db_connect();
	$where = "rally_id = '".$rally_id."'";
	$rally_date = rally_select($db , $where);
	$rally = mysql_fetch_array($rally_date);
	db_close( $db );
	$new_stamp_num = $rally['new_stamp_num'];
	return ($new_stamp_num);
}
//ユーザー名取得
function user_name_check($user_id){
	$db = db_connect();
	$where = "rally_id = '".$rally_id."'";
	$all_user_date = all_user_select($db , $where);
	$all_user = mysql_fetch_array($all_user_date);
	db_close( $db );
	$user_name = $all_user['user_name'];
	return ($user_name);
}
//プロファイル強要度の取得
function get_profile_force($rally_id){
	$db = db_connect();
	$where = "rally_id = '".$rally_id."'";
	$rally_date = rally_select($db , $where);
	$rally = mysql_fetch_array($rally_date);
	db_close( $db );
	$value = $rally['profile_force'];
	return ($value);
}
