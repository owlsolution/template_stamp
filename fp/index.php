<?php
require_once('./../../config.php');
require_once('./../ad_config.php');
require "./syntax.php";
$p = $_GET['p'];  //ページ選択
$rally_id = $_GET['rally_id'];  //ラリーナンバー
  //個体識別ID

/*
 * URLにguid=ONを入れないと取得できない
 */
//個体識別番号取得関数
function mobileId() {
	if (isset($_SERVER['HTTP_X_DCMGUID'])) {  //ドコモ
		$mobile_id = $_SERVER['HTTP_X_DCMGUID'];
	} else if (isset($_SERVER['HTTP_X_UP_SUBNO'])) {  //au
		$mobile_id = $_SERVER['HTTP_X_UP_SUBNO'];
	} else if (isset($_SERVER['HTTP_X_JPHONE_UID'])) { //ソフトバンク
		$mobile_id = $_SERVER['HTTP_X_JPHONE_UID'];
	}
	return $mobile_id;
}
$ident_id = mobileId();
//個体識別IDを検索
if($ident_id != ""){
        $ident_id .=  $rally_id;  //個体識別 = 個体識別ID+ラリーID
	$user_id = get_identification_id($ident_id);
} else if($ident_id == ""){
	$user_id = $_GET['user_id'];
}
/**
 * 最終ログイン日時を更新する
 */
$db = db_connect();
$login_date_update = user_login_date_update($db , $user_id , $rally_id);
db_close($db);

//有効期限のチェック
require_once('./expiration_date.php');

if($ident_id != ""){
	switch ($p){
		case '':  //トップ(スタンプページ)
			$sys = "./page/stamp.php";
			break;
		case 'application': //クーポン発行ページ
			$sys = "./page/application.php";
			break;
		case 'coupon': //取得済クーポンページ
			$sys = "./page/coupon.php";
			break;
		case 'obtain': //クーポン取得の確認ページ
			$sys = "./page/obtain.php";
			break;
		case 'get_goods': //クーポン使用ページ
			$sys = "./page/get_goods.php";
			break;
		case 'notice':  //お知らせページ
			$sys = "./page/notice.php";
			break;
		case 'qr_get_goods': //QRコードからクーポンを使用するページ
			$sys = "./page/qr_get_goods.php";
			break;
		case 'withdrawal': //退会ページ
			$sys = "./page/withdrawal.php";
			break;
		case 'withdrawal_end': //退会ページ
			$sys = "./page/withdrawal_end.php";
			break;
		case 'terms': //利用規約ページ
			$sys = "./page/terms.php";
			break;
		case 'privacy': //プライバシー・ポリシー
			$sys = "./page/privacy.php";
			break;
		case 'shop': //お店情報ページ
			$sys = "./page/shop.php";
			break;
		case 'help': //ヘルプページ
			$sys = "./page/help.php";
				break;
		default:
			break;
	}
	require $sys;
}