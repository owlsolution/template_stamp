<?php
/*
 * クーポン取得の確認ページ
*/
$coupon_no = $_GET['coupon_no'];
$goal = page_setup_acquisition($rally_id);
$get_coupon_name = $goal['goal_title_'.$coupon_no];   //クーポン名
$stamp_num = $goal['goal_stamp_num_'.$coupon_no];   //クーポンスタンプ数
$get_coupon_description = $goal['goal_description_'.$coupon_no];  //クーポン内容
$get_coupon_img = $goal['img_name_'.$coupon_no];  //クーポン画像名
$get_coupon_state = 0;
$now_date = date('Y-m-d');  //今日の日付
$now_date2 = date('Y-m-d H:i:s');
/*
echo $coupon_no;
echo $get_coupon_name."<br>";
echo $stamp_num."<br>";
echo $get_coupon_description."<br>";
echo $get_coupon_img."<br>";
echo $now_date."<br>";
*/
$user_rally_date = user_information_acquisition($rally_id , $user_id);
if($user_rally_date['stamp_num'] >= $stamp_num){
	new_coupon($rally_id , $user_id , $stamp_num , $get_coupon_name , $get_coupon_description , $get_coupon_img , $get_coupon_state , $now_date2 , $now_date);
	$stamp_page = larry_page_acquisition($rally_id);
	$future_header_page = $stamp_page['future_header_page'];  //スマホへッダー
	require "./page/header.php";
	echo $future_header_page;
?>
	<center>
		<br>
		<br>
		<font size="1">「<font color="red"><?php echo $get_coupon_name;?></font>」<br>クーポンを取得しました。</font>
		<br>
		<br>
		<br>
		<a href="./?guid=ON&p=coupon&rally_id=<?php echo $rally_id;?>&user_id=<?php echo $user_id;?>">取得済みクーポン一覧</a>
	</center>
<?php
	require "./page/footer.php";
} else {
	header('Location:./?guid=ON&p=coupon&rally_id='.$rally_id.'&user_id='.$user_id);
}
?>