<?php
/*
 * スタンプページ
 */
if($s_num == ""){
	$up_stamp_num = "";
} else {
	$up_stamp_num = $s_num;
}
$stamp_page = larry_page_acquisition($rally_id);
$future_stamp_page = $stamp_page['future_stamp_page'];  //フューチャーフォンスタンプページ
$title_page = $stamp_page['title_stamp_page'];  //スタンプページタイトル
$future_header_page = $stamp_page['future_header_page'];  //フューチャーフォンへッダー
$future_footer_page = $stamp_page['future_footer_page'];  //フューチャーフォンフッター
$rally_user = user_information_acquisition($rally_id , $user_id);
$stamp_num = $rally_user['stamp_num'];  //スタンプ数
$total_stamp_num = $rally_user['total_stamp_num'];  //トータルスタンプ数

// ユーザに配信されたお知らせ
$db = db_connect();
$where = "user_id = '".$rally_user['user_id']."'";
$notice_history_rows = notice_read_history_select($db, $where);
$notice_note = "";
while ($notice_history_row = mysql_fetch_array($notice_history_rows)){
	if ($notice_note != "") {
		$notice_note .= ",";
	}
	$notice_note .= $notice_history_row['notice_id'];
}
db_close( $db );

$coupon_stamp_num = null;
$rally = larry_information_acquisition($rally_id);
$stamp_max = $rally['stamp_max'];  //スタンプMAX数
$goal = page_setup_acquisition($rally_id);
for($t=1; $t<=12 ; $t++){
	$goal_stamp_num[$t] = $goal['goal_stamp_num_'.$t];
	if($goal_stamp_num[$t] == 0){
		$goal_stamp_num[$t] = $stamp_max;
	}
	$coupon_title[$t] = $goal['goal_title_'.$t];  //クーポンタイトル
	$coupon_stamp_num[$t] = $goal['goal_stamp_num_'.$t];  //クーポンスタンプ数
	$coupon_image_url[$t] = $goal['img_name_'.$t];  //クーポンイメージ
	$coupon_content[$t] = $goal['goal_description_'.$t];  //クーポン内容
}

$flg = preg_match_all("/<#notice_loop#>(.*)<\/#notice_loop#>/s",$future_stamp_page,$str_match);
$str_notice = $str_match[1][0];
$notice_content = notice_information_acquisition($rally_id , $str_notice , $user_id , $notice_note);
$img= ink_pad_set($rally_id , $stamp_num);

$db = db_connect();

//画像の置換
for($i = 1; $i <= $stamp_max ; $i++){
	$future_stamp_page = str_replace("#stamp".$i."#", $img[$i] , $future_stamp_page);
}
//スタンプ数の置換
$future_stamp_page = str_replace("#stamp_num#", $stamp_num , $future_stamp_page);
//お知らせ置換　#notice#
$future_page_content_1 = explode("<#notice_loop#>", $future_stamp_page);
$future_page_content_2 = explode("</#notice_loop#>", $future_page_content_1[1]);
$future_stamp_page = $future_page_content_1[0]."".$notice_content."".$future_page_content_2[1];

//応募ページリンク
if($stamp_num >= $goal_stamp_num[1] || $stamp_num >= $goal_stamp_num[2] || $stamp_num >= $goal_stamp_num[3] || $stamp_num >= $goal_stamp_num[4] || $stamp_num >= $goal_stamp_num[5] || $stamp_num >= $goal_stamp_num[6] || $stamp_num >= $goal_stamp_num[7] || $stamp_num >= $goal_stamp_num[8] || $stamp_num >= $goal_stamp_num[9] || $stamp_num >= $goal_stamp_num[10]){
	$future_stamp_page = str_replace("<#application_display#>", "" , $future_stamp_page);
	$future_stamp_page = str_replace("</#application_display#>", "" , $future_stamp_page);
	$future_stamp_page = str_replace("#application#", "./?guid=ON&p=application&rally_id=".$rally_id."&user_id=".$user_id , $future_stamp_page);
} else {
	$future_page_content_1 = explode("<#application_display#>", $future_stamp_page);
	$future_page_content_2 = explode("</#application_display#>", $future_page_content_1[1]);
	$future_stamp_page = $future_page_content_1[0]."".$future_page_content_2[1];
}
//クーポン一覧
$future_stamp_page = str_replace("#coupon#", "./?guid=ON&p=coupon&rally_id=".$rally_id."&user_id=".$user_id , $future_stamp_page);
for($i=1;$i<=12;$i++){
	$future_stamp_page = str_replace("#coupon_title_".$i."#", $coupon_title[$i] , $future_stamp_page);
	$future_stamp_page = str_replace("#coupon_stamp_num_".$i."#", $coupon_stamp_num[$i] , $future_stamp_page);
	$future_stamp_page = str_replace("#coupon_image_url_".$i."#", $coupon_image_url[$i] , $future_stamp_page);
	$future_stamp_page = str_replace("#coupon_content_".$i."#", $coupon_content[$i] , $future_stamp_page);
}
//設定ページリンク
$future_stamp_page = str_replace("#setting#", "./../overall/setting.php?guid=ON" , $future_stamp_page);
$future_stamp_page = str_replace("#summary#", "./../overall/?guid=ON" , $future_stamp_page); //スタンプラリー一覧URL

if($stamp_num >= $stamp_max){
	$warning = "<center><font color='red' size='1'>スタンプが貯まっています！<br>クーポンを発行しましょう！</span></center>";
} else {
	$warning = "";
}
$future_stamp_page = str_replace("#warning#", $warning , $future_stamp_page);

if($up_stamp_num != ""){
	if($up_result == "one_day_ng"){
		$stamp_information = "<center><font color='red' size='1'>スタンプは１日１回付与されました。</font></center>";
	} else if($up_result == "qr_ng"){
		$stamp_information = "<center><font color='red' size='1'>このQRコードは無効です。</font></center>";
	} else if($up_result == "day_duration_future_date_qr_ng"){
		$stamp_information = "<center><font color='red' size='1'>QRコードは利用期間ではありません。[1]</font></center>";
	} else if($up_result == "day_duration_ago_date_qr_ng"){
		$stamp_information = "<center><font color='red' size='1'>QRコードは利用期間を過ぎています。[2]</font></center>";
	} else if($up_result == "chk_cnt_qr_ng"){
		$stamp_information = "<center><font color='red' size='1'>既に使用されたQRコードです。</font></center>";
	} else if($up_result == "eternal_duration_invalid_qr_ng"){
		$stamp_information = "<center><font color='red' size='1'>無効なQRコードです。</font></center>";
	} else if($up_result == "chk_limited_qr_ng"){
		$stamp_information = "<center><font color='red' size='1'>QRコード連続使用制限です。しばらく時間をおいてから再度読み込んでください。</font></center>";
	} else {
		if(STAMP_TYPE == 'B') {
			if($congrats_text_flg == "1"){
				// スタンプ達成時
				$stamp_information .= "<center><font color='red' size='1'>スタンプ達成おめでとうございます！次も頑張ってください！</font></center>";
			}
			// スタンプについて
			$stamp_information .= "<center><font color='red' size='1'>スタンプが".$up_stamp_num."個追加されました。</font></center>";
			// クーポン取得について
			$coupon_num = "";
			if($coupon_num_array != ""){
				foreach ($coupon_num_array as $get_coupon_num) {
						$coupon_num .= "<center><font color='red' size='1'>「スタンプ".$get_coupon_num."個目のクーポン」を取得しました。</font></center>";
				}
			}
			$stamp_information .= $coupon_num;
		} else {
			$stamp_information .= "<center><font color='red' size='1'>スタンプが".$up_stamp_num."個追加されました。</font></center>";
		}
	}
} else {
	$stamp_information = "";
}
$future_stamp_page = str_replace("#stamp_up_num#", $stamp_information , $future_stamp_page);
if(STAMP_TYPE == 'B') {
    /* スタンプ周回数の表示
     * 合計スタンプ数とMAXスタンプ数を計算して、周回数を表示させる。
     */
    $stamp_round_num = ceil($total_stamp_num/$stamp_max);
    $future_stamp_page = str_replace("#stamp_round_num#", $stamp_round_num , $future_stamp_page);
}

$count = 1;
for($i=1;$i<=12;$i++){
	if($stamp_num < $coupon_stamp_num[$i]){
		if($count == 1){
			$remaining = $coupon_stamp_num[$i] - $stamp_num;
			$coupon_notice = "<center><font color='red' size='1'>後".$remaining."個で「".$coupon_title[$i]."」が取得できます。</span></center>";
			$count++;
		}
	}
}
$future_stamp_page = str_replace("#coupon_notice#", $coupon_notice , $future_stamp_page);
if(empty($user_name)){
	$user_name = USER_ID.sprintf("%05d", $user_id);
}
$future_stamp_page = str_replace("#user_name#", $user_name , $future_stamp_page);

//******************************フッター情報******************************
$future_footer_page = str_replace("#home#", "./?guid=ON&rally_id=".$rally_id , $future_footer_page);
$future_footer_page = str_replace("#summary#", "./../overall/?guid=ON" , $future_footer_page);
$future_footer_page = str_replace("#setting#", "./../overall/setting.php?guid=ON" , $future_footer_page);
$future_footer_page = str_replace("#withdrawal#", "./?guid=ON&p=withdrawal&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);
$future_footer_page = str_replace("#help#", "./?guid=ON&p=help&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);
$future_footer_page = str_replace("#terms#", "./?guid=ON&p=terms&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);
$future_footer_page = str_replace("#privacy_policy#", "./?guid=ON&p=privacy&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);
$future_footer_page = str_replace("#shop_information#", "./?guid=ON&p=shop&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);

require "./page/header.php";
echo $future_header_page;
echo $future_stamp_page;
echo $future_footer_page;
require "./page/footer.php";