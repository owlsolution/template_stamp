<?php
function doctype($carrier) {

	print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");

	if($carrier == "DoCoMo") {

		print ("<!DOCTYPE html PUBLIC \"-//i-mode group (ja)//DTD XHTML i-XHTML(Locale/Ver.=ja/2.3)1.0//EN\" \"i-xhtml_4ja_10.dtd\">\n");

	}
	elseif($carrier == "SoftBank") {

		print ("<!DOCTYPE html PUBLIC \"-//J-PHONE//DTD XHTML Basic 1.0 Plus//EN\" \"xhtml-basic10-plus.dtd\">\n");

	}
	elseif($carrier == "EZweb") {

		print ("<!DOCTYPE html PUBLIC \"-//OPENWAVE//DTD XHTML 1.0//EN\" \"http://www.openwave.com/DTD/xhtml-basic.dtd\">\n");

	}
	else {

		print
		("<!DOCTYPE html PUBLIC \"-//WAPFORUM//DTD XHTML Mobile 1.0//EN\" \"http://www.wapforum.org/DTD/xhtml-mobile10.dtd\">\n");

	} // if($carrier == "DoCoMo") {

}
$agent = $_SERVER['HTTP_USER_AGENT'];
if(ereg("^DoCoMo", $agent)){
	$carrier = "DoCoMo";
}else if(ereg("^J-PHONE|^Vodafone|^SoftBank", $agent)){
	$carrier = "SoftBank";
}else if(ereg("^UP.Browser|^KDDI", $agent)){
	$carrier = "EZweb";
}
doctype($carrier);
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1, minimum-scale=1 , maximum-scale=1">
<title><?php echo $title_page;?></title>
<link rel="stylesheet" href="text.css" />
</head>
<body style="width:240px;">