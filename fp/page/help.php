<?php
/*
 * ヘルプページ
 */
$stamp_page = larry_page_acquisition($rally_id);
$use_page_date = $stamp_page['use_page_date'];  //スマホへッダー
require "./page/header.php";
?>
<h1>
	<img src="./../../fp_images/s_taitle5.jpg">
</h1>
<img src="./../../fp_images/back.gif" height="15px" width="100%">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td bgcolor="#CCCCCC"><font size="1">・スタンプの貯め方</font></td>
	</tr>
	<tr>
		<td>
			<font size="1">
				<font color="#FF6600">①</font>お店にある専用端末から、スタンプ取得QRコードを読み込んでください。
			</font><br />
			<font size="1">
				<font color="#FF6600">②</font>正しく読み込みこんだらスタンプが取得できます。
			</font><br />
		</td>
	</tr>
	<tr>
		<td bgcolor="#CCCCCC"><font size="1">・クーポンの取得方法(携帯)</font></td>
	</tr>
	<tr>
		<td>
			<font size="1">
				<font color="#FF6600">①</font>クーポンが貯まるとスタンプカードの上に表示される「クーポンを取得する」をクリックしてください。
			</font><br />
			<font size="1">
				<font color="#FF6600">②</font>クーポン発行ページが表示されますので、取得できるクーポンをタッチしてください。
			</font><br />
			<font size="1">
				<font color="#FF6600">③</font>クーポン取得に必要な個数に満たしていたらスタンプを消費してクーポンを発行します
			</font><br />
		</td>
	</tr>
	<tr>
		<td bgcolor="#CCCCCC"><font size="1">・クーポンの取得方法(店舗端末)</font></td>
	</tr>
	<tr>
		<td>
			<font size="1">
				<font color="#FF6600">①</font>店舗端末から取得されるクーポンを選択してください
			</font><br />
			<font size="1">
				<font color="#FF6600">②</font>QRコードを読み込んでください。
			</font><br />
			<font size="1">
				<font color="#FF6600">③</font>クーポン取得に必要な個数に満たしていたらスタンプを消費してクーポンを発行します。
			</font><br />
		</td>
	</tr>
	<tr>
		<td bgcolor="#CCCCCC"><font size="1">・クーポンの使い方</font></td>
	</tr>
	<tr>
		<td>
			<font size="1">
				<font color="#FF6600">①</font>メニューにある「取得済みクーポン」をタッチしてください。
			</font><br />
			<font size="1">
				<font color="#FF6600">②</font>使用したいクーポンの「使用する」をクリックしてください。
			</font><br />
			<font size="1">
				<font color="#FF6600">③</font>確認画面をお店の従業員におみせください。
			</font><br />
		</td>
	</tr>
</table>
<img src="./../../fp_images/back.gif" height="10px" width="100%">
<br>
<br>
<font size="1" color="">メニュー</font><br>
<font size="1" color="">┗<a href="./?guid=ON&rally_id=<?php echo $rally_id;?>"> <font size="1">スタンプカード</font> </a> </font> <br>
<?php
require "./page/footer.php";
?>