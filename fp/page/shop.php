<?php
/*
 * お店情報
 */
$db = db_connect();
$child_list = getChildAdminIds ($db , ADMIN_ID);
if (count($child_list) > 0) {
	$admin_ids = ADMIN_ID.",".implode(",", $child_list);
	$where = "admin_id IN (".$admin_ids.")";
}else {
	$where = "admin_id = '".ADMIN_ID."'";
}
$shop_date = shop_select($db , $where);
db_close( $db );
$title_page = "お店情報";  //スタンプページタイトル
require "./page/header.php";
?>
<h1>
	<img src="./../../fp_images/s_taitle2.jpg">
</h1>
	<img src="./../../fp_images/back.gif" height="15px" width="100%">
<?php
foreach( $shop_date as $key => $value) {
	/*
	 * 新しい電話番号、営業時間の情報を取得
	 * なければ古い情報を取得
	 */
	if(empty($value['phone_number'])){
		$store_tel = $value['tel'];
	} else {
		$store_tel = $value['phone_number'];
	}
	if(empty($value['business_hours'])){
		$store_business = $value['business_start']."~".$value['business_end'];
	} else {
		$store_business = $value['business_hours'];
	}
?>
<h2><font size="1"><?php echo $value['store_name']?></font></h2>
<hr>
<img src="./../../fp_images/back.gif" height="10px" width="100%">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td bgcolor="#CCCCCC"><font size="1">・住所</font></td>
	</tr>
	<tr>
		<td><font size="1"><?php echo $value['store_address']?></font></td>
	</tr>
	<tr>
		<td bgcolor="#CCCCCC"><font size="1">・営業時間</font></td>
	</tr>
	<tr>
		<td><font size="1"><?php echo $store_business ?></font></td>
	</tr>
	<tr>
		<td bgcolor="#CCCCCC"><font size="1">・電話番号</font></td>
	</tr>
	<tr>
		<td><font size="1"><a href="tel:<?php echo $store_tel?>"><?php echo $store_tel?></a></font></td>
	</tr>
	<tr>
		<td bgcolor="#CCCCCC"><font size="1">・定休日</font></td>
	</tr>
	<tr>
		<td><font size="1"><?php echo $value['store_rest']?></font></td>
	</tr>
</table>
<img src="./../../fp_images/back.gif" height="15px" width="100%">
<?php
}
?>
<font size="1" color="">メニュー</font><br>
<font size="1" color="">┗<a href="./?guid=ON&rally_id=<?php echo $rally_id;?>"> <font size="1">スタンプカード</font> </a> </font> <br>
<?php
require "./page/footer.php";
?>