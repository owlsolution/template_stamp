<?php
/*
 *取得クーポンページ
 */
$stamp_page = larry_page_acquisition($rally_id);
$future_header_page = $stamp_page['future_header_page'];  //フューチャーフォンヘッダー
$future_acq_coupon_page = $stamp_page['future_acq_coupon_page'];  //フューチャーフォン取得クーポンページ
$title_page = $stamp_page['title_acq_coupon_page'];  //取得クーポンページタイトル
$future_footer_page = $stamp_page['future_footer_page'];  //フューチャーフォンフッター
$db = db_connect();
$get_coupon_date = get_coupon_information($db , $rally_id , $user_id);
$t = 1;
while ($get_coupon = mysql_fetch_array($get_coupon_date)){
	$get_coupon_id[$t] = $get_coupon['get_coupon_id'];  //取得済みクーポンID
	$stamp_num[$t] = $get_coupon['stamp_num'];  //クーポンのスタンプ数
	if($get_coupon['gift_check'] == 0){
		$get_coupon_name[$t] = $get_coupon['get_coupon_name'];  //クーポン名
	} else {
		$get_coupon_name[$t] = "[プレゼント]".$get_coupon['get_coupon_name'];  //クーポン名
	}
	$get_coupon_description[$t] = $get_coupon['get_coupon_description'];  //クーポン内容
	$coupon_use_end_list = explode(":", $get_coupon['coupon_use_end']);  //クーポン期限
	$coupon_use_end[$t] = $coupon_use_end_list[0].":".$coupon_use_end_list[1];  //クーポン期限
	$get_coupon_img[$t] = $get_coupon['get_coupon_img'];  //クーポン画像
	if($get_coupon['gift_check'] == 0){
		$acquisition_date[$t] = $get_coupon['acquisition_date'];  //クーポン取得日
	}
	$t++;
}
db_close( $db );
if(strstr($future_acq_coupon_page, "<#acquisition_coupon#>")){
	$flg = preg_match_all("/<#acquisition_coupon#>(.*)<\/#acquisition_coupon#>/s",$future_acq_coupon_page,$str_match);
	$str_notice = $str_match[1][0];

	$future_acq_coupon_content = "";
	for($i= 1; $i<$t; $i++){
		$future_acq_coupon_page_date = str_replace("#acquisition_date#", $acquisition_date[$i] , $str_notice);
		$future_acq_coupon_page_date = str_replace("#acquisition_coupon_title#", $get_coupon_name[$i] , $future_acq_coupon_page_date);
		$future_acq_coupon_page_date = str_replace("#acquisition_image_url#", "./../img_coupon/".$get_coupon_img[$i] , $future_acq_coupon_page_date);
		$future_acq_coupon_page_date = str_replace("#acquisition_coupon_content#", $get_coupon_description[$i] , $future_acq_coupon_page_date);
		$future_acq_coupon_page_date = str_replace("#acquisition_coupon_use_end#", $coupon_use_end[$i] , $future_acq_coupon_page_date);
		$future_acq_coupon_page_date = str_replace("#acquisition_stamp_num#", $stamp_num[$i] , $future_acq_coupon_page_date);
		$future_acq_coupon_page_date = str_replace("#acquisition_use_url#", "./?guid=ON&p=get_goods&rally_id=".$rally_id."&id=".$ident_id."&goods=$get_coupon_id[$i]" , $future_acq_coupon_page_date);
		$future_acq_coupon_content .= $future_acq_coupon_page_date;
	}

	$future_acq_coupon_page_content_1 = explode("<#acquisition_coupon#>", $future_acq_coupon_page);
	$future_acq_coupon_page_content_2 = explode("</#acquisition_coupon#>", $future_acq_coupon_page_content_1[1]);
	$future_acq_coupon_content = $future_acq_coupon_page_content_1[0]."".$future_acq_coupon_content."".$future_acq_coupon_page_content_2[1];
	$future_acq_coupon_content = str_replace("#summary#", "./../overall/?guid=ON" , $future_acq_coupon_content); //スタンプラリー一覧URL
	$future_acq_coupon_content = str_replace("#home#", "./?guid=ON&rally_id=".$rally_id , $future_acq_coupon_content); //ホームURL

}
if($t == 1){
	$future_acq_coupon_content = $future_acq_coupon_content."<font color='red' size='1'>現在取得しているクーポンがありません。</font><br>";
}

//******************************フッター情報******************************
$future_footer_page = str_replace("#home#", "./?guid=ON&rally_id=".$rally_id , $future_footer_page);
$future_footer_page = str_replace("#summary#", "./../overall/?guid=ON" , $future_footer_page);
$future_footer_page = str_replace("#setting#", "./../overall/setting.php?guid=ON" , $future_footer_page);
$future_footer_page = str_replace("#withdrawal#", "./?guid=ON&p=withdrawal&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);
$future_footer_page = str_replace("#help#", "./?guid=ON&p=help&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);
$future_footer_page = str_replace("#terms#", "./?guid=ON&p=terms&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);
$future_footer_page = str_replace("#privacy_policy#", "./?guid=ON&p=privacy&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);
$future_footer_page = str_replace("#shop_information#", "./?guid=ON&p=shop&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);

require "./page/header.php";
echo $future_header_page;
echo $future_acq_coupon_content;
echo $future_footer_page;
require "./page/footer.php";
?>