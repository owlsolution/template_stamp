<?php
/*
 * お知らせページ
 */
$stamp_page = larry_page_acquisition($rally_id);
$future_notice_page = $stamp_page['future_notice_page'];  //フューチャーフォンお知らせページ
$title_page = $stamp_page['title_notice_page'];  //お知らせページタイトル
$future_header_page = $stamp_page['future_header_page'];  //フューチャーフォンへッダー
$future_footer_page = $stamp_page['future_footer_page'];  //フューチャーフォンフッター
$flg = preg_match_all("/<#notice_loop#>(.*)<\/#notice_loop#>/s",$future_notice_page,$str_match);
$str_notice_initial = $str_match[1][0];

// ラリー情報を取得
$db = db_connect();
$rally = get_one_rally_by_rally_id($db, $rally_id);
db_close($db);

$rally_user = user_information_acquisition($rally_id , $user_id);
$db = db_connect();
$where = "user_id = '".$user_id."'";
$notice_history_rows = notice_read_history_select($db, $where);

$notice_note_array = array();
while ($notice_history_row = mysql_fetch_array($notice_history_rows)){
	$notice_note_array[] = $notice_history_row['notice_id'];
}
db_close( $db );

$notice_where = "";
$notice_count = count($notice_note_array);
if(!empty($notice_count)){
	$notice_where .= " OR notice_id IN (".implode(",", $notice_note_array).") ";
}

// 支店を含むadmin idのリストを取得
$db = db_connect();
$admin_list = array();
$admin_list[] = ADMIN_ID;

// user_idでbranch_idを取得する。
$where_branch_id = "user_id = ".$user_id;
$rally_user_data = rally_user_select($db , $where_branch_id);
$user_info = mysql_fetch_array($rally_user_data);

/*
 * 支店で配信しているお知らせ
 */
if($user_info['branch_id'] != NULL){
  $branch_id = $user_info['branch_id'];
  // 支店のadmin_id
  $admin_list[] = getChildAdminIdByBranchId($db, $branch_id);
}

/*
 * 店員で配信しているお知らせ
 */
// 店員のadmin_idを追加
$branch_id_list[] = "0";
if($user_info['branch_id'] != NULL){
  //ユーザーがプロフィール登録済の場合
  //本部と支店に所属している店員のadmin_idを取得
  $branch_id_list[] = $user_info['branch_id'];
}
$where = "rally_id = '".$rally_id."' AND status = '1' AND branch_id IN (".implode(",", $branch_id_list).") ";
$staff_data = staff_select($db , $where);
while ($staff = mysql_fetch_array($staff_data)){
  $admin_list[] = $staff['admin_id'];
}

// admin idの条件生成
$where_admin = "admin_id IN (".implode(",", $admin_list).")";

$notice_content = "";
$db = db_connect();
//＊＊＊＊＊＊＊＊＊＊お知らせ情報＊＊＊＊＊＊＊＊＊＊
$where = $where_admin." AND (notice_type IN(2) ".$notice_where." ) AND notice_data < now() AND not transition = 1 ORDER BY notice_id DESC LIMIT 0 , 5";
$notice_date = notice_select($db , $where);
while ($notice = mysql_fetch_array($notice_date)){
	$pieces = explode(" ", $notice['notice_data']);
	$pieces_str = explode("-", $pieces[0]);
	$pieces_string = $pieces_str[0]."/".$pieces_str[1]."/".$pieces_str[2];
	$pieces_str1 = explode(":", $pieces[1]);
	$pieces_string1 = $pieces_str1[0].":".$pieces_str1[1];
	$notice_data = $pieces_string." ".$pieces_string1;
	$str_notice = str_replace("#notice_data#", $notice_data , $str_notice_initial);
	$str_notice = str_replace("#notice_title#", $notice['notice_title'] , $str_notice);
	$notice_str = $notice['notice_content'];
	$img_src_start = '<img src="./../notice_images_fp/';
	$img_src_end = '">';
	$notice_str = str_replace("<img #", $img_src_start, $notice_str);
	$notice_str = str_replace("#>", $img_src_end, $notice_str);
	$str_notice = str_replace("#notice_content#", nl2br($notice_str) , $str_notice);
	$notice_content .= $str_notice;
}
$smart_notice_content_1 = explode("<#notice_loop#>", $future_notice_page);
$smart_notice_content_2 = explode("</#notice_loop#>", $smart_notice_content_1[1]);
$future_notice_page_content = $smart_notice_content_1[0]."".$notice_content."".$smart_notice_content_2[1];
$future_notice_page_content = str_replace("#summary#", "./../overall/?guid=ON" , $future_notice_page_content); //スタンプラリー一覧URL
$future_notice_page_content = str_replace("#home#", "./?guid=ON&rally_id=".$rally_id , $future_notice_page_content); //ホームURL

//******************************フッター情報******************************
$future_footer_page = str_replace("#home#", "./?guid=ON&rally_id=".$rally_id , $future_footer_page);
$future_footer_page = str_replace("#summary#", "./../overall/?guid=ON" , $future_footer_page);
$future_footer_page = str_replace("#setting#", "./../overall/setting.php?guid=ON" , $future_footer_page);
$future_footer_page = str_replace("#withdrawal#", "./?guid=ON&p=withdrawal&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);
$future_footer_page = str_replace("#help#", "./?guid=ON&p=help&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);
$future_footer_page = str_replace("#terms#", "./?guid=ON&p=terms&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);
$future_footer_page = str_replace("#privacy_policy#", "./?guid=ON&p=privacy&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);
$future_footer_page = str_replace("#shop_information#", "./?guid=ON&p=shop&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);

require "./page/header.php";
echo $future_header_page;
echo $future_notice_page_content;
echo $future_footer_page;
require "./page/footer.php";
db_close( $db );
?>