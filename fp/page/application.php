<?php
/*
 * クーポン発行ページ
 */
$rally_user = user_information_acquisition($rally_id , $user_id);//ユーザー情報取得
$user_stamp_num = $rally_user['stamp_num'];  //ユーザーのスタンプ数
$stamp_page = larry_page_acquisition($rally_id);  //ページ情報取得
$title_page = $stamp_page['title_coupon_page'];  //クーポン一覧（発行）ページタイトル
$coupon_page = $stamp_page['future_coupon_page'];  //クーポン一覧（発行）ページ
$future_header_page = $stamp_page['future_header_page'];  //フューチャーフォンヘッダー
$future_footer_page = $stamp_page['future_footer_page'];  //フューチャーフォンフッター
$goal = page_setup_acquisition($rally_id);  //クーポン設定取得
if(strstr($coupon_page, "<#coupon_display_A#>")){  //＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊取得出来るクーポンのみ表示させるループ表示＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
	$flg = preg_match_all("/<#coupon_display_A#>(.*)<\/#coupon_display_A#>/s",$coupon_page,$str_match);
	$str_notice = $str_match[1][0];
	$coupon_content = "";
	for($i = 1; $i <= 10; $i++){
		if($goal['goal_stamp_num_'.$i] <= $user_stamp_num && $goal['goal_stamp_num_'.$i] != 0){
			$coupon_page_date = $str_notice;
			$coupon_page_date = str_replace("#stamp_num#", $goal['goal_stamp_num_'.$i] , $coupon_page_date);
			$coupon_page_date = str_replace("#image_url#", "./../img_coupon/".$goal['img_name_'.$i] , $coupon_page_date);
			$coupon_page_date = str_replace("#coupon_title#", $goal['goal_title_'.$i] , $coupon_page_date);
			$coupon_page_date = str_replace("#coupon_content#", $goal['goal_description_'.$i] , $coupon_page_date);
			$coupon_page_date = str_replace("<#application_link#>", "" , $coupon_page_date);
			$coupon_page_date = str_replace("</#application_link#>", "" , $coupon_page_date);
			$coupon_page_date = str_replace("#application#", "./?guid=ON&p=obtain&rally_id=".$rally_id."&user_id=".$user_id."&coupon_no=".$i , $coupon_page_date);
			$coupon_content .= '<div class="stamp_box_on">'.$coupon_page_date.'</div>';
		}
	}
	$future_acq_coupon_page_content_1 = explode("<#coupon_display_A#>", $coupon_page);
	$future_acq_coupon_page_content_2 = explode("</#coupon_display_A#>", $coupon_page);
	$coupon_content = $future_acq_coupon_page_content_1[0]."".$coupon_content."".$future_acq_coupon_page_content_2[1];
	$coupon_content = str_replace("#summary#", "" , $coupon_content); //スタンプラリー一覧URL
	$coupon_content = str_replace("#home#", "./?guid=ON&rally_id=".$rally_id , $coupon_content); //ホームURL


} else if(strstr($coupon_page, "<#coupon_display_B#>")){  //＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊全クーポン表示させ取得出来るクーポンのみボタン表示させるループ表示＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊＊
	$flg = preg_match_all("/<#coupon_display_B#>(.*)<\/#coupon_display_B#>/s",$coupon_page,$str_match);
	$str_notice = $str_match[1][0];

	$coupon_content = "";
	for($i = 1; $i <= 10; $i++){
		if($goal['goal_stamp_num_'.$i] <= $user_stamp_num && $goal['goal_stamp_num_'.$i] != 0){
			$coupon_page_date = $str_notice;
			$coupon_page_date = str_replace("#stamp_num#", $goal['goal_stamp_num_'.$i] , $coupon_page_date);
			$coupon_page_date = str_replace("#image_url#", "./../img_coupon/".$goal['img_name_'.$i] , $coupon_page_date);
			$coupon_page_date = str_replace("#coupon_title#", $goal['goal_title_'.$i] , $coupon_page_date);
			$coupon_page_date = str_replace("#coupon_content#", $goal['goal_description_'.$i] , $coupon_page_date);
			$coupon_page_date = str_replace("<#application_link#>", "" , $coupon_page_date);
			$coupon_page_date = str_replace("</#application_link#>", "" , $coupon_page_date);
			$coupon_page_date = str_replace("#application#", "./?guid=ON&p=obtain&rally_id=".$rally_id."&user_id=".$user_id."&coupon_no=".$i , $coupon_page_date);
			$coupon_content .= '<div class="stamp_box_on">'.$coupon_page_date.'</div>';
		} else if($goal['goal_stamp_num_'.$i] != 0){
			$coupon_page_date = $str_notice;
			if(strstr($coupon_page_date, "<#application_link#>")){
				$coupon_page_content_1 = explode("<#application_link#>", $coupon_page_date);
				$coupon_page_content_2 = explode("</#application_link#>", $coupon_page_date);
			}
			$coupon_page_date = $coupon_page_content_1[0]."".$coupon_page_content_2[1];
			$coupon_page_date = str_replace("#stamp_num#", $goal['goal_stamp_num_'.$i] , $coupon_page_date);
			$coupon_page_date = str_replace("#image_url#", "./../img_coupon/".$goal['img_name_'.$i] , $coupon_page_date);
			$coupon_page_date = str_replace("#coupon_title#", $goal['goal_title_'.$i] , $coupon_page_date);
			$coupon_page_date = str_replace("#coupon_content#", $goal['goal_description_'.$i] , $coupon_page_date);
			$coupon_content .= '<div class="stamp_box_off">'.$coupon_page_date.'</div>';
		}
	}
	$future_acq_coupon_page_content_1 = explode("<#coupon_display_B#>", $coupon_page);
	$future_acq_coupon_page_content_2 = explode("</#coupon_display_B#>", $coupon_page);
	$coupon_content = $future_acq_coupon_page_content_1[0]."".$coupon_content."".$future_acq_coupon_page_content_2[1];
	$coupon_content = str_replace("#summary#", "./../overall/?guid=ON" , $coupon_content); //スタンプラリー一覧URL
	$coupon_content = str_replace("#home#", "./?guid=ON&rally_id=".$rally_id , $coupon_content); //ホームURL
}
//******************************フッター情報******************************
$future_footer_page = str_replace("#home#", "./?guid=ON&rally_id=".$rally_id , $future_footer_page);
$future_footer_page = str_replace("#summary#", "./../overall/?guid=ON" , $future_footer_page);
$future_footer_page = str_replace("#setting#", "./../overall/setting.php?guid=ON" , $future_footer_page);
$future_footer_page = str_replace("#withdrawal#", "./?guid=ON&p=withdrawal&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);
$future_footer_page = str_replace("#help#", "./?guid=ON&p=help&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);
$future_footer_page = str_replace("#terms#", "./?guid=ON&p=terms&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);
$future_footer_page = str_replace("#privacy_policy#", "./?guid=ON&p=privacy&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);
$future_footer_page = str_replace("#shop_information#", "./?guid=ON&p=shop&rally_id=".$rally_id."&user_id=".$user_id , $future_footer_page);

require "./page/header.php";
echo $future_header_page;
echo $coupon_content;
echo $future_footer_page;
require "./page/footer.php";
?>