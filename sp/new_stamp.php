<?php
$rally_id = $_GET['rally_id'];
$store = $_GET['store'];
require_once('./../../config.php');
require_once('./../ad_config.php');
require "./syntax.php";
$larry_information = larry_information_acquisition($rally_id);
$download_url = "./download/".$larry_information['download_url'];
//echo $rally_id."<br>";
//echo $store;
/*
$stamp_page = larry_page_acquisition($rally_id);
$smart_header_page = $stamp_page['smart_header_page'];  //スマホへッダー
$smart_top_page = $stamp_page['smart_top_page'];  //スマートフォン取得クーポンページ
$title_page = $stamp_page['title_top_page'];
$smart_top_page_content = str_replace("#download_url#", "./download/".$download_url , $smart_top_page);

require "./page/header.php";
echo $smart_header_page;
echo $smart_top_page_content;
require "./page/footer.php";
*/
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1, minimum-scale=1 , maximum-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />
<title>スタンプラリーについて</title>
<link rel="stylesheet" href="./../../css/style2.css" />
</head>
<body>
<h1>
<img src="./../../sp_images/logo2.png" width="290">
</h1>
<div class="st_box clearfix">
<img src="./../../sp_images/owl-Hair.png" width="80" align="left">
<div class="text_box">
<h2>
<?php echo $larry_information['rally_name'];?>
</h2>
<img src="./../../sp_images/android_app.png" width="85px">
<div align="center">
<a href="<?php echo $download_url;?>">
<img src="./../../sp_images/dl_btn1.png" width="200px">
</a>
</div>
</div>
</div>
<div class="text_container">
<div class="">
<h2>
このアプリに付いて
</h2>
<ul>
このアプリは、Google Playに掲載されていないアプリです。
アプリをインストールするには、「設定」→「アプリケーション」で、「提供元不明のアプリ」のインストールを許可します。「ダウンロード」からダウンロードしたこのアプリをタップして、アプリをインストールしてください。
</ul>
</div>
</div>
</div>
<div style="padding:15px; border-bottom:1px dotted #CCCCCC; margin-bottom:15px;" align="center">
<a href="<?php echo $download_url;?>">
<img src="./../../sp_images/dl_btn2.png" width="280">
</a>
</div>
<div class="st_box clearfix">
<h2>
説明
</h2>
<div class="cushion_box" >
<li class="clearfix">
<h3 style="width:100px; font-size:13px; color:#999; float:left;" align="right">
配信元
</h3>
<div style="margin-left:110px; font-size:13px; ">
アウルソリューション
</div>
</li>
<li class="clearfix">
<h3 style="width:100px; font-size:13px; color:#999; float:left;" align="right">
appについて
</h3>
<div style="margin-left:110px; font-size:13px; ">
<img src="./../../sp_images/android_app.png" width="80px">
</div>
</li>
<li class="clearfix">
<h3 style="width:100px; font-size:13px; color:#999; float:left;" align="right">
appについて
</h3>
<div style="margin-left:110px; font-size:13px; ">
スタンプを貯めてクーポンを貰える！
<br>
アプリをインストールするだけで、スタンプラリーに簡単参加！
</div>
</li>
</div>
</div>
<div class="text_container" align="center" style="padding:10px;">
© owl solution
</div>
</body>
</html>
