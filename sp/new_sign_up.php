<?php
$rally_id = $_GET['rally_id'];
$ident_id = $_GET['id'];
$classification = $_GET['classification'];
$device_id = $_GET['uuid'];
require_once('./../../config.php');
require_once('./../ad_config.php');
require "./syntax.php";

if(!empty($ident_id) && (strlen($ident_id) < 128)) {
	$db = db_connect();
	$add_date = date('Y-m-d h:i:s');
	$into = "'".mysql_real_escape_string($ident_id)."' , '".mysql_real_escape_string($add_date)."'";
	$user = user_insert($db , $into);
	db_close( $db );
	$user_id = Util::get_authenticated_user_id();
	header('Location:./?rally_id='.$rally_id.'&id='.$ident_id.'&ident='.$classification.(empty($device_id) ? "" : "&uuid=".$device_id));
} else {
	exit();
}
