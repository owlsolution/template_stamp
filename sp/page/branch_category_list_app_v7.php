<?php
/*
 * 組織情報一覧取得
 */
require_once('./../common/model/BranchCategoryModel.php');
$branchCategoryModel = new BranchCategoryModel();

$db = db_connect();
// 支店のカテゴリを全て取得する
$branch_category_list = $branchCategoryModel->get_branch_category_by_rally_id($db, $rally_id);
db_close( $db );

foreach( $branch_category_list as $category) {
	$organization_data[] = array(
		"id" => $category['id'],
		"name" => $category['name'],
		"short_name" => $category['short_name'],
		"sub_id" => $category['sub_id'],
	);
}

$return_array = array( 
	"branch_category_data" => $organization_data
);

error_log(json_encode( $return_array ));
header("Content-Type: application/json; charset=utf-8");
print json_encode($return_array);
?>