<?php
require_once('./../common/model/RallyUserModel.php');
require_once('./../common/model/UserModel.php');
require_once('./../common/model/RallyModel.php');
require_once('./../common/model/AdminModel.php');

$rallyUserModel = new RallyUserModel();
$userModel = new UserModel();
$rallyModel = new RallyModel();
$adminModel = new AdminModel();


/*
 *  最新情報取得[アプリ用]
 */

// ラリー情報を取得
$db = db_connect();
$rally = $rallyModel->find_by_rally_id($db, $rally_id);

// ラリーユーザ情報取得
$user_info = $rallyUserModel->get_rally_user_by_user_id_and_rally_id($db, $user_id, $rally_id);

// オーナーadminで店舗が単一店舗か支店あり店舗かを判定する
$admin_type = $adminModel->get_admin_type($db, ADMIN_ID);

$admin_id = ADMIN_ID;
if ($admin_type == BRANCHES_OWNER) {
	// 統括アカウント
	// 支店情報を取得
	if (isset($user_info['branch_id'])) {
		$branch = $adminModel->get_branch_by_id($db, $user_info['branch_id']);
		if (isset($branch['child_admin_id'])) {
			$admin_id = $branch['child_admin_id'];
		}
	}
}
db_close($db);

$return_array = array( 
	"admin_id" => $admin_id,
	"admin_type" => $admin_type,
);

header("Content-Type: application/json; charset=utf-8");
print json_encode($return_array);
?>