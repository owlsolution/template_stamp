<?php
require_once('./../common/model/UserModel.php');
$userModel = new UserModel();

require_once('./../common/model/UploadImageModel.php');
$uploadImageModel = new UploadImageModel();

require_once('./../common/model/FixReserveModel.php');
$fixReserveModel = new FixReserveModel();

if (empty($_GET['id'])) {
	exit;
}

/*
 * ユーザ情報をjson形式で返却[アプリ]
 */
$db = db_connect();
// ユーザ情報取得
$userInfo = $userModel->find_by_devide_num($db, $_GET['id']);
if (empty($userInfo)) {
	exit;
}

$retVal = array();
// 最新の画像設定日取得
$lastUploadImg = $uploadImageModel->last($db, $user_id);
if ($lastUploadImg) {
	// コピーする
	$ret = $uploadImageModel->copy($db, $lastUploadImg['upload_image_id']);
	if ($ret) {
		$retVal['code'] = '0';
		$retVal['message'] = 'success';
	} else {
		$retVal['code'] = '2';
		$retVal['message'] = 'copy is failed.';
	}
	
} else {
	$retVal['code'] = '1';
	$retVal['message'] = 'base data not found.';
}
db_close($db);
error_log(json_encode( $retVal ));

header("Content-Type: application/json; charset=utf-8");
print json_encode($retVal);
