<?php
require_once('./../common/model/UserModel.php');
$UserModel = new UserModel();

/*
 * ユーザ情報をjson形式で返却[アプリ]
 */
$db = db_connect();
//$where = "divide_num = '".$_GET['id']."'";
$userInfo = $UserModel->find_by_devide_num($db, $_GET['id']);

// 未設定の場合、空設定
$userInfo['membership'] = isset($userInfo['membership']) ? $userInfo['membership'] : '';

db_close($db);

error_log(json_encode( $userInfo ));

header("Content-Type: application/json; charset=utf-8");
print json_encode($userInfo);
