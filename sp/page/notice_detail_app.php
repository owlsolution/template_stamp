<?php
/*
 * お知らせページ[アプリ用]
 */
$day_date = $_GET['day_date'];
$day_date = str_replace("h", " ", $day_date);
$day_date = str_replace("i", ":", $day_date);
$stamp_page = larry_page_acquisition($rally_id);
$smart_notice_page = $stamp_page['smart_notice_app_page'];  //スマートお知らせページ
$title_page = $stamp_page['title_notice_page'];  //お知らせページタイトル
$smart_header_page = $stamp_page['smart_header_page'];  //スマホへッダー
$smart_footer_page = $stamp_page['smart_footer_page'];  //スマホフッター

// ラリー情報を取得
$db = db_connect();
$rally = get_one_rally_by_rally_id($db, $rally_id);
db_close($db);

// 支店を含むadmin idのリストを取得
$db = db_connect();
$admin_list = $rally['admin_id'];
// 支店がある場合、支店のadmin_idを取得
$admin_ids = getChildAdminIds($db, $rally['admin_id']);
foreach ($admin_ids as $id) {
    $admin_list .= ",".$id;
}
// 店員さんのadmin_idを追加
$where = "rally_id = ".$rally_id." AND status = '1'";
$staff_data = staff_select($db , $where);
while ($staff = mysql_fetch_array($staff_data)){
    $admin_list .= ",".$staff['admin_id'];
}
db_close( $db );

$notice_content = "";
$db = db_connect();
//＊＊＊＊＊＊＊＊＊＊お知らせ情報＊＊＊＊＊＊＊＊＊＊
if (!empty($rally['admin_id'])) {
	$where = "admin_id IN (".$admin_list.")"." AND notice_data LIKE '%".$day_date."%' ORDER BY notice_id DESC LIMIT 0 , 1";
	//echo $where;
	$notice_date = notice_select($db , $where);
	$notice = mysql_fetch_array($notice_date);
	$notice_day = $notice['notice_data'];
	$notice_title = $notice['notice_title'];
	$notice_content = $notice['notice_content'];
	$notice_id = $notice['notice_id'];
}
db_close( $db );
/*
 * 既読処理
 */
if(!empty($notice_id) && !empty($user_id)){
	// 未読かどうかを判定
	$db = db_connect();
	$readFlag = is_already_read_notice($db, $notice_id, $user_id);
	if ($readFlag == 0) {
		// 未読の場合,既読(1)にUPDATEする
		$where = "notice_id = '".$notice_id."' AND user_id = '".$user_id."'";
		$set = " read_flag = '1'";
		notice_read_history_update($db , $set, $where);
		
		// カウントアップ
		$where = "notice_id = '".$notice_id."'";
		$set = "already_read_num = already_read_num + 1";
		notice_db_edit($db , $set ,$where);

	} else if ($readFlag == -1) {
		// 未登録の場合、既読(2)でインサートする
		$set = "notice_id = '".$notice_id."' , user_id = '".$user_id."' , read_flag = '2'";
		notice_read_history_insert($db , $set);
		
		// カウントアップ
		$where = "notice_id = '".$notice_id."'";
		$set = "already_read_num = already_read_num + 1";
		notice_db_edit($db , $set ,$where);

	}else {
		// 既読なら何もしない
	}
	db_close( $db );
}
$smart_notice_page = str_replace("#notice_data#", $notice_day , $smart_notice_page);
$smart_notice_page = str_replace("#notice_title#", $notice_title , $smart_notice_page);
$img_src_start = '<img src="./../notice_images/';
$img_src_end = '" width="100%">';
$notice_content = str_replace("<img #", $img_src_start, $notice_content);
$notice_content = str_replace("#>", $img_src_end, $notice_content);
$smart_notice_page = str_replace("#notice_content#", nl2br($notice_content) , $smart_notice_page);
require "./page/header.php";
echo $smart_notice_page;
require "./page/footer.php";
?>