<?php
	$error_message = '';
	$error_code = '0';
	
	$keycode = isset($_GET['keycode']) ? $_GET['keycode'] : '';
	if($keycode != ''){
		$count = model_change_count_search_by_pass($keycode, $rally_id);
		if($count != 0){
			model_change_data_move($keycode , $ident_id, $rally_id);
			$error_message = 'OK';
			$error_code = '0';
		} else {
			$error_message = '引き継ぎ元データが存在しないか、もしくは、パスワードが正しくありません。';
			$error_code = '100';
		}
	} else {
		$error_message = 'パスワードが空です';
		$error_code = '200';
	}
	
	$return_array	= array(
		'message'	=> $error_message,
		'code'	=> $error_code,
	);
	
	error_log(print_r($return_array ,true));

	header("Content-Type: application/json; charset=utf-8");
	print json_encode($return_array);
