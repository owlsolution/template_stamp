<?php
/*
 * 組織情報一覧取得
 */
require_once('./../common/model/AdminModel.php');
$adminModel = new AdminModel();

$db = db_connect();
// オーナーIDをキーに配下の組織情報を全て取得する
$org_list = $adminModel->get_organization_by_owner_admin_id($db, ADMIN_ID);

db_close( $db );

foreach( $org_list as $org) {
	$organization_data[] = array(
		"id" => $org['id'],
		"admin_id" => $org['admin_id'],
		"owner_admin_id" => $org['owner_admin_id'],
		"name" => $org['name'],
		"short_name" => $org['short_name'],
		"sub_id" => $org['sub_id'],
	);
}

$return_array = array( 
	"organization_data" => $organization_data
);

error_log(json_encode( $return_array ));
header("Content-Type: application/json; charset=utf-8");
print json_encode($return_array);
?>