<?php
require_once('./../common/model/BranchCategoryModel.php');
$branchCategoryModel = new BranchCategoryModel();

/*
 * 支店情報一覧をjson形式で返却[アプリ]
 */
// ユーザ情報取得
$rally_user = user_information_acquisition($rally_id , $user_id);
$branch_id = $rally_user['branch_id'];

$db = db_connect();
// 支店のカテゴリを全て取得する
$branch_category_list = $branchCategoryModel->get_branch_category_by_rally_id($db, $rally_id);

// 支店名を取得
$branch_list = branch_select_by_owner($db, ADMIN_ID, 'sub_id');
db_close($db);

// 支店がある場合、一覧を返却
$return_array = array();
foreach ($branch_list as $branch) {
	// 支店のカテゴリを取得する
//	$category = $branchCategoryModel->get_row_by_id($branch_category_list, "id", $branch['branch_category_id']);
	$category_name = $category == false ? '' : "[".$category['name']."]";
	// レスポンスにデータをセット
	$return_array[] = array(
		'id' => $branch['id'],
		'name' => $category_name.$branch['name'],
		'sub_id' => $branch['sub_id'],
		'check' => ($branch_id == $branch['id']) ? '1': '0',
	);
}

error_log(json_encode( $return_array ));

header("Content-Type: application/json; charset=utf-8");
print json_encode($return_array);
