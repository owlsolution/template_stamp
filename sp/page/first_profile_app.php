<?php
/*
 * 初期プロフィール有無[アプリ用]
 */

$db = db_connect();
$where = "user_id = ".$user_id;
$all_user = all_user_select($db , $where);
$user = mysql_fetch_array($all_user);
db_close( $db );
$user_check = "0";

//if($user['mail_addr'] == ""){
//if(empty($user['birth_date']) || empty($user['region']) || empty($user['sex'])){
if(empty($user['birth_date']) || empty($user['sex'])){
    // 誕生日、地域、性別のいずれかが未設定の場合、1を設定する
    $user_check = "1";
}

// プロフィール強要度を取得 1: 2: 5:の場合画面起動時のプロフィールポップアップが必要
$profile_force = get_profile_force($rally_id);

// 初期プロフィール有無フラグ
$first_flag = "0";
if (($user_check == "1") && ($profile_force == "1")){
    // user情報が未入力で、且つ、プロフィール強要度が1の場合
    // 初期プロフィール画面へ遷移する必要有り
    $first_flag = "1";
}

$check_sns = check_sns($rally_id);  //SNSのチェック
if ($check_sns == 2) {
    $first_flag = "0";
}

// zipmart固有処理
if (($user_check == "0") && (($profile_force == "2") || ($profile_force == "5"))){
    // 登録済みで、スキップ可能な場合、modeを0にする
    $profile_force = "0";
}

//$flag  初期プロフィール必要有無 0:不要 1:必要
//$mode  プロフィール要求モード 0:default 1:必須モード 2:後でもOK
$return_array	= array(
	'flag'	=> $first_flag,
	'mode'	=> $profile_force,
);

error_log(json_encode( $return_array ));

header("Content-Type: application/json; charset=utf-8");
print json_encode($return_array);


//$stamp_num   スタンプ数
//$user_name   ユーザーID
//$user_check   チェック
//$notice_stamp_date   お知らせ内容
//$mode  プロフィール要求モード 0:default 1:必須モード 2:後でもOK
//echo "stamp,".$stamp_num.",".$user_name.",".$user_check.",".$notice_stamp_date.",".$profile_force."";

?>