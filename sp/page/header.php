<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1, minimum-scale=1 , maximum-scale=2">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="content-style-type" content="text/css" />
<meta name = "viewport" content = "width = device-width">
<meta http-equiv="content-script-type" content="text/javascript" />
<script type="text/javascript" src="/stamp/lib/js/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" href="/stamp/css/admin.css" />
<?php
if(!isset($_GET['id'])){
?>
<link rel="stylesheet" href="/stamp/css/menu.css" />
<?php
}
?>
<title><?php echo $title_page;?></title>
<style type="text/css">
<!--
<?php echo $stamp_page['page_css'];?>
.warning {
	color:red;
	font-size:10px;

}
.error_text_box {
	text-align:center;
}
.stamp_warning {
	color:red;
	font-size:12px;

}
-->
</style>
<body>
<?php require(dirname(__FILE__).DIRECTORY_SEPARATOR.'menu.php'); ?>
