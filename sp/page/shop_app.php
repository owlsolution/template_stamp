<?php
/*
 * お店情報ページ
 */
$db = db_connect();
$admin_list = ADMIN_ID;
// 支店がある場合、支店のadmin_idを取得
$admin_ids = getChildAdminIds($db, ADMIN_ID);
foreach ($admin_ids as $id) {
    $admin_list .= ",".$id;
}

$where = "admin_id IN (".$admin_list.")";
$shop_date = shop_select($db , $where);
db_close( $db );

$store_tel = "";
$store_business = "";
foreach( $shop_date as $key => $value) {
	/*
	 * 新しい電話番号、営業時間の情報を取得
	 * なければ古い情報を取得
	 */
	if(empty($value['phone_number'])){
		$store_tel = $value['tel'];
	} else {
		$store_tel = $value['phone_number'];
	}
	if(empty($value['business_hours'])){
		$store_business = $value['business_start']."~".$value['business_end'];
	} else {
		$store_business = $value['business_hours'];
	}

    if ($admin_list == ADMIN_ID) {
        // 支店機能なし(通常)
		echo count($value).",".$value['store_name'].",".$value['store_address'].",".$store_business.",".$store_tel.",".$value['store_rest'].",".$value['store_prelusion'].",".$value['hp_url'].",".$value['map_url'].",".$value['admin_id'].",";
    } else {
        // 支店機能あり
		//店舗画像を表示させるためにADMIN_IDを渡す
		$line = aaa.",".$value['store_name'].",".$value['store_address'].",".$store_business.",".$store_tel.",".$value['store_rest'].",".$value['store_prelusion'].",".$value['hp_url'].",".$value['map_url'].",".$value['admin_id'];
        echo str_replace("\n", "#n#", $line)."\n";
    }
}
?>