<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />
<meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=0.5, maximum-scale=10.0,user-scalable=yes">

<title><?php echo $title_page;?></title>
<style type="text/css">
#box {
  width: 100%;
}
#box img {
  width: 100%;
}
</style>
<body>
