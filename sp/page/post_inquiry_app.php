<?php
require_once('./../common/model/InquiryModel.php');
require_once('./../common/model/RallyUserModel.php');
require_once('./../common/model/AdminModel.php');
require_once('./../common/model/AlertSettingModel.php');

function error_return($code) {
	$return_array = [];
	$return_array['code'] = $code;
	header("Content-Type: application/json; charset=utf-8");
	print json_encode($return_array);
}

// JSONデータ受取
$json_string = file_get_contents('php://input');
$postinfo = json_decode($json_string, true);

$postinfo['name'] = Util::removeEmoji($postinfo['name']);
$postinfo['phone'] = Util::removeEmoji($postinfo['phone']);
$postinfo['description'] = Util::removeEmoji($postinfo['description']);
$postinfo['branch_id'] = empty($postinfo['branch_id']) ? '0': $postinfo['branch_id'];
$postinfo['kind'] = empty($postinfo['kind']) ? '0': $postinfo['kind'];

error_log("insert1:");
$db = db_connect();
$inquiryModel = new InquiryModel();
$postinfo['rally_id'] = $rally_id;
$postinfo['user_id'] = $user_id;
$inquiry_id = $inquiryModel->insert($db, $postinfo);
if (empty($inquiry_id)) {
	error_return('1');
	return;
}
db_close($db);

$rallyUserModel = new RallyUserModel();
$adminModel = new AdminModel();
$alertSettingModel = new AlertSettingModel();

$db = db_connect();
// ユーザが所属しているadmin_idを取得し、そこから発信するようにする
$user = $rallyUserModel->get_userinfo_by_user_id_and_rally_id($db, $user_id, $rally_id);

// 支店IDから所属するadmin_idを取得/空ならデフォルトのADMIN_ID
$target_admin_id = ADMIN_ID;

// 通知対象のadmin_idがあればリストに追加する
$admin_ids[] = ADMIN_ID;
if (!empty($postinfo['branch_id'])) {
	$branch = $adminModel->get_branch_by_id($db, $postinfo['branch_id']);
	if (!empty($branch['child_admin_id'])) {
		$target_admin_id = $branch['child_admin_id'];
		$admin_ids[] = $target_admin_id;
	}
	
	// 組織配下の場合、その組織アカウントのadmin_idを取得する
	if (!empty($branch['organization_id'])) {
		$org = $adminModel->get_organization_by_id($db, $organization_id);
		$admin_ids[] = $org['admin_id'];
	}
}

db_close($db);

$title = '問合せ受付';
// 本文作成
$context = "ユーザからの問合せを受付けました。\n"
		. "管理画面から確認してください。\n"
		. "----------------------\n"
		. "御予約者 : ".$postinfo['name']."様\n"
		. "電話番号 : ".$postinfo['phone']."\n"
		. "問合せ番号 : (".'INQ'.sprintf("%05d", $inquiry_id).")\n"
		. "内容 : \n"
		.$postinfo['description']."\n"
		. "----------------------\n"
		."<a href=\""."https://".$_SERVER['SERVER_NAME']."/stamp/".FILE_NAME."/asp/?p=reserve\">管理画面へアクセスする</a>\n"
		. "----------------------\n";

// 通知先を特定する
$db = db_connect();
$alert_users = $alertSettingModel->find_by_rally_id_and_admin_ids($db, $rally_id, $admin_ids);
db_close($db);

$user_list = [];
$mail_list = [];
foreach ($alert_users as $alert) {
	if (!empty($alert['account'])) {
		if($alertSettingModel->check_val_is_mail($alert['account'])) {
			// メールアドレスなので
			$mail_list[] = $alert['account'];
		} else {
			$user_list[] = $alertSettingModel->check_val_is_user_id($alert['account'], USER_ID);
		}
	}
}

error_log("user_list:".print_r($user_list,true));
error_log("mail_list:".print_r($mail_list,true));

if (count($user_list) > 0) {
	$alertSettingModel->alert_by_notice($target_admin_id, $user_list, $title, $context);
}
//// メール配信
//$mails = 'uzakiron.27105650@gmail.com';
if (count($mail_list) > 0) {
	$alertSettingModel->alert_by_mail($target_admin_id, $mail_list, $title, $context);
}

// 正常終了
$return_array = [];
$return_array['code'] = '0';
header("Content-Type: application/json; charset=utf-8");
print json_encode($return_array);
