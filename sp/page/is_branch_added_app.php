<?php
/*
 * ユーザ情報 支店登録有無チェック[アプリ用]
 */

// 支店情報登録有無判定
$rally_user = user_information_acquisition($rally_id , $user_id);
if (isset($rally_user) && !empty($rally_user['branch_id'])) {
	$return_flag = '1';
} else {
	$return_flag = '0';
}

//$flag 支店登録 0:未登録 1:登録済み
$return_array	= array(
	'flag'	=> $return_flag,
);

error_log(json_encode( $return_array ));

header("Content-Type: application/json; charset=utf-8");
print json_encode($return_array);
