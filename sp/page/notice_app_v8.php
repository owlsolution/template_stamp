<?php
require_once('./../common/model/AdminModel.php');
/*
 * //お知らせページ[アプリ用]
 */
// ラリー情報を取得
$db = db_connect();
$rally = get_one_rally_by_rally_id($db, $rally_id);
db_close($db);

// 支店を含むadmin idのリストを取得
$db = db_connect();
$admin_list[] = ADMIN_ID;

// user_idでbranch_idを取得する。
$where_branch_id = "user_id = ".$user_id;
$rally_user_data = rally_user_select($db , $where_branch_id);
$user_info = mysql_fetch_array($rally_user_data);

/*
 * 配信対象のAdmin_idを精査
 */
// ラリーIDで全スタッフ取得
$adminModel = new AdminModel();
$staffs = $adminModel->get_staff_by_rally_id($db, $rally_id);

// オーナー直下のスタッフ
$staff_list = $adminModel->filter_staff_by_under_owner($staffs);
if (!empty($staff_list)) $admin_list = array_merge($admin_list, array_column($staff_list, 'admin_id'));

if(!empty($user_info['branch_id'])){
	$branch_id = $user_info['branch_id'];
	
	// 支店情報を取得
	$branch = $adminModel->get_branch_by_id($db, $branch_id);
	
	// 組織情報を取得
	$organization = $adminModel->get_organization_by_id($db, $branch['organization_id']);
	if (!empty($organization['admin_id'])) {
		// 組織アカウントのadmin_id
		$admin_list[] = $organization['admin_id'];
	}
	
	// ユーザが所属する支店のAdmin_idを設定
	if (!empty($branch['child_admin_id'])) {
		$admin_list[] = $branch['child_admin_id'];
	}
	
	// 組織IDから組織直下取得
	$staff_list = null;
	$staff_list = $adminModel->filter_staff_by_organization_id($staffs, $organization['id']);
	if (!empty($staff_list)) $admin_list = array_merge($admin_list, array_column($staff_list, 'admin_id'));
	
	// ブランチidから支店直下のスタップ
	$staff_list = null;
	$staff_list = $adminModel->filter_staff_by_branch_id($staffs, array($branch_id));
	if (!empty($staff_list)) $admin_list = array_merge($admin_list, array_column($staff_list, 'admin_id'));
}


// admin idの条件生成
$in_admins = implode(',', $admin_list);
error_log("in_admins:".$in_admins);
$where_admin = "admin_id IN (". $in_admins.")";
db_close( $db );

// ユーザへ見せるお知らせのIDのリストを取得する
$db = db_connect();
// notice_read_historyからユーザ宛になっているnotice_idを取得
$where = "user_id = '".$user_id."' ";
$notice_history_rows = notice_read_history_select($db, $where);
$where_notice_id = "";
while ($notice_history_row = mysql_fetch_array($notice_history_rows)){
	if (empty($where_notice_id)) {
		$where_notice_id = $notice_history_row['notice_id'];
	} else {
		$where_notice_id .= ", ".$notice_history_row['notice_id'];
	}
}

db_close( $db );

if (empty($where_notice_id)) {
	$where_notice_id_con = "";
	$where_notice_id_con_type_rsv = "";
} else {
	$where_notice_id_con = "OR notice_id IN(".$where_notice_id.") ";
	$where_notice_id_con_type_rsv = "OR ( notice_id IN(".$where_notice_id.") AND notice_type IN(5))";
}

// お知らせ情報からブログ/チラシ全件
// お知らせはIDリストのものを取得する
// 時間がまだ来ていないものは予約なのでまだ配達されていないので対象外にする
$notice_content = "";
$db = db_connect();
//＊＊＊＊＊＊＊＊＊＊お知らせ情報＊＊＊＊＊＊＊＊＊＊
$notice_list = array();
if (!empty($admin_list)) {
	$where = $where_admin." AND ((notice_type IN(1,2) ".$where_notice_id_con.") ".$where_notice_id_con_type_rsv.") AND notice_data < now() AND not transition = 1 ORDER BY notice_data DESC";
	$notice_data = notice_select($db , $where);
	while ($notice = mysql_fetch_array($notice_data)){
		// 0:未読 1:既読
		$read_status = is_already_read_notice($db, $notice['notice_id'], $user_id);
		
		// 送信者取得
		$where = "admin_id = ".$notice['admin_id'];
		$staff_data = staff_select($db , $where);
		$staff = mysql_fetch_array($staff_data);

		$img_path = "";
		if ($notice['notice_type'] == 1) {
			// チラシの場合、チラシのファイル名を取得
			$img_path = "/notice_images_fp/".$notice['thumbnail'];
		} else if ($notice['notice_type'] == 2) {
			if (empty($staff)) {
				$staff = array(
						"nickname"=>"",
						"img_path"=>"person.png",
					);
				$admin = $adminModel->get_admin_by_admin_id($db, $notice['admin_id']);
				if (isset($admin['img_path'])) {
					$staff = array(
							"nickname"=>"",
							"img_path"=>$admin['img_path'],
						);
				}
			}
			if(empty($staff['img_path'])){
				$img_path = "/staff_images/person.png";
			}else{
				$img_path = "/staff_images/".$staff['img_path'];
			}
		} else if ($notice['notice_type'] == 4) {
			// スケジュール配信(4)の場合、おしらせタイプとしてスマホへ送る
			$notice['notice_type'] = 0;
		} else if ($notice['notice_type'] == 5) {
			// スケジュール配信(5)の場合、おしらせタイプとしてスマホへ送る
			$notice['notice_type'] = 0;
		}
		
		$notice_list[] = array(
				"notice_id" => $notice['notice_id'],
				"title" => $notice['notice_title'],
				"time" => date("Y/m/d H:i", strtotime($notice['notice_data'])),
				"new" => $read_status,						// 0：NEW 1:既読
				"type" => strval($notice['notice_type']),	// お知らせ0 チラシ:1 ブログ:2 個人宛:3
				"sender" => empty($staff['nickname']) ? "" : $staff['nickname'],				// 送信者名
				"ic_image" => $img_path,					// 送信者画像
		);
	}

}
db_close( $db );
header("Content-Type: application/json; charset=utf-8");
print(json_encode($notice_list));
?>