<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1, minimum-scale=1 , maximum-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />
<title><?php echo $Title;?></title>
<link rel="stylesheet" href="./../../css/style.css" />
<link rel="stylesheet" href="/stamp/css/admin.css" />
<link rel="stylesheet" href="/stamp/css/menu.css" />
</head>
<body>
<?php require(dirname(__FILE__).DIRECTORY_SEPARATOR.'menu.php'); ?>