<?php
/*
 * 取得済クーポンページ
 */
$stamp_page = larry_page_acquisition($rally_id);
$smart_header_page = $stamp_page['smart_header_page'];  //スマホへッダー
$smart_acq_coupon_page = $stamp_page['smart_acq_coupon_page'];  //スマートフォン取得クーポンページ
$smart_footer_page = $stamp_page['smart_footer_page'];  //スマホフッター
$title_page = $stamp_page['title_acq_coupon_page'];
$db = db_connect();
$get_coupon_date = get_coupon_information($db , $rally_id , $user_id);
$t = 1;
while ($get_coupon = mysql_fetch_array($get_coupon_date)){
	$get_coupon_id[$t] = $get_coupon['get_coupon_id'];  //取得済みクーポンID
	$stamp_num[$t] = $get_coupon['stamp_num'];  //クーポンのスタンプ数
	$get_coupon_name[$t] = $get_coupon['get_coupon_name'];  //クーポン名
	$get_coupon_description[$t] = $get_coupon['get_coupon_description'];  //クーポン内容
	$get_coupon_img[$t] = "../img_coupon/".$get_coupon['get_coupon_img'];  //クーポン画像
	$acquisition_date[$t] = $get_coupon['acquisition_date'];  //クーポン取得日
	$t++;
}
db_close( $db );
$db = db_connect();
$get_coupon_gift_date = get_coupon_information_gift($db , $rally_id , $user_id);
$get_coupon_gift = mysql_fetch_array($get_coupon_gift_date);
$get_coupon_gift_name = $get_coupon_gift['get_coupon_name'];
$get_coupon_gift_id = $get_coupon_gift['get_coupon_id'];
db_close( $db );
if(strstr($smart_acq_coupon_page, "<#acquisition_coupon#>")){
	$flg = preg_match_all("/<#acquisition_coupon#>(.*)<\/#acquisition_coupon#>/s",$smart_acq_coupon_page,$str_match);
	$str_notice = $str_match[1][0];

	$smart_acq_coupon_content = "";
	for($i= 1; $i<$t; $i++){
		$smart_acq_coupon_page_date = str_replace("#acquisition_date#", $acquisition_date[$i] , $str_notice);
		$smart_acq_coupon_page_date = str_replace("#acquisition_coupon_title#", $get_coupon_name[$i] , $smart_acq_coupon_page_date);
		$smart_acq_coupon_page_date = str_replace("#acquisition_image_url#", "./../img_coupon/".$get_coupon_img[$i] , $smart_acq_coupon_page_date);
		$smart_acq_coupon_page_date = str_replace("#acquisition_coupon_content#", $get_coupon_description[$i] , $smart_acq_coupon_page_date);
		$smart_acq_coupon_page_date = str_replace("#acquisition_stamp_num#", $stamp_num[$i] , $smart_acq_coupon_page_date);
		$smart_acq_coupon_page_date = str_replace("#acquisition_use_url#", "./?p=get_goods&rally_id=".$rally_id."&id=".$ident_id."&ident=".$ident."&goods=$get_coupon_id[$i]" , $smart_acq_coupon_page_date);
		$smart_acq_coupon_content .= $smart_acq_coupon_page_date;
	}

	$smart_acq_coupon_page_content_1 = explode("<#acquisition_coupon#>", $smart_acq_coupon_page);
	$smart_acq_coupon_page_content_2 = explode("</#acquisition_coupon#>", $smart_acq_coupon_page_content_1[1]);
	$smart_acq_coupon_content = $smart_acq_coupon_page_content_1[0]."".$smart_acq_coupon_content."".$smart_acq_coupon_page_content_2[1];
}
if(!empty($get_coupon_gift_name)){
	$smart_acq_coupon_content .= '<div style="padding:10px; border-bottom:#FFF 2px solid; padding-bottom:20px; margin-bottom:10px;">';
	$smart_acq_coupon_content .= '<div style="background:url(./../img_coupon/gift.png) no-repeat;background-size:100%;width:290px;height:115px;margin: 0px auto;">';
	$smart_acq_coupon_content .= '<div style="padding:35px 10px;padding-bottom:10px;">';
	$smart_acq_coupon_content .= '<ul style="height:60px;font-size:13px;padding:20px 10px;padding-bottom:10px;text-align: center;">';
	$smart_acq_coupon_content .= $get_coupon_gift_name;
	$smart_acq_coupon_content .= '</ul>';
	$smart_acq_coupon_content .= '</div>';
	$smart_acq_coupon_content .= '</div>';
	$smart_acq_coupon_content .= '<div class="btn" style="margin-top:5px;">';
	$smart_acq_coupon_content .= '<a href="./?p=get_goods&rally_id='.$rally_id.'&id='.$ident_id.'&ident='.$ident.'&goods='.$get_coupon_gift_id.'" >';
	$smart_acq_coupon_content .= '使用する';
	$smart_acq_coupon_content .= '</a>';
	$smart_acq_coupon_content .= '</div>';
	$smart_acq_coupon_content .= '</div>';
}

if($t == 1 && empty($get_coupon_gift_name)){
	$smart_acq_coupon_content = $smart_acq_coupon_content."<div class='error_text_box error_text3'><span class = 'warning'>現在取得しているクーポンがありません。</span></div>";
}

//******************************フッター情報******************************
$smart_footer_page = str_replace("#withdrawal#", "./?p=withdrawal&rally_id=".$rally_id."&id=".$ident_id."&ident=".$ident , $smart_footer_page);
$smart_footer_page = str_replace("#terms#", "./?p=terms&rally_id=".$rally_id."&id=".$ident_id."&ident=".$ident , $smart_footer_page);
$smart_footer_page = str_replace("#privacy_policy#", "./?p=privacy&rally_id=".$rally_id."&id=".$ident_id."&ident=".$ident , $smart_footer_page);
$smart_footer_page = str_replace("#shop_information#", "./?p=shop&rally_id=".$rally_id."&id=".$ident_id."&ident=".$ident , $smart_footer_page);

require "./page/header.php";
echo $smart_header_page;
echo $smart_acq_coupon_content;
echo $smart_footer_page;
require "./page/footer.php";
?>