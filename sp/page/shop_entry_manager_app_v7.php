<?php
/*
 * 管理ユーザ用機能へのアクセスボタンを表示する
 */
// お知らせ部品を読込み
require_once('./../common/component/NoticeComponent.php');
// AdminUserModel読込み
require_once('./../common/model/AdminUserModel.php');

// お知らせ部品
if (!isset($noticeComp)) {
	$noticeComp = new NoticeComponent();
}
// 管理アカウント/ユーザ連携部品
if (!isset($adminUserModel)) {
	$adminUserModel = new AdminUserModel();
}

$stamp_page = larry_page_acquisition($rally_id);
$easy_login_css = $stamp_page['easy_login_css'];  //スマートお知らせページ

echo <<< EOM
<style type="text/css">
<!--
{$easy_login_css}
-->
</style>
EOM;

// ユーザIDをキーに管理ユーザなのかをチェック
$db = db_connect();
$admin = $adminUserModel->get_admin_id_by_user_id($db, $user_id);
if (!empty($admin['admin_id'])) {
	// admin_idからユーザ種別を判定
	$admin_type = $noticeComp->get_admin_type($db, $admin['admin_id']);
	echo '<div class="boxContainer">';
	if (in_array($admin_type, array(OWNER, BRANCHES_OWNER, BRANCH_MANAGER, ORG_MANAGER))) {
		echo '<div class="box "><a class="easyLoginBtn" href="'.'../easy/login.php?p=notice&show_saveonly=1&rally_id='.$rally_id.'&ident=1&app=1&id='.$ident_id.'">仮投稿管理</a></div>';
		echo '<div class="box "><a class="easyLoginBtn" href="'.'../easy/login.php?p=notice&rally_id='.$rally_id.'&ident=1&app=1&id='.$ident_id.'">お知らせ（ブログ）配信</a></div>';
	} else if($admin_type == STAFF) {
		echo '<div class="box "><a class="easyLoginBtn" href="'.'../easy/login.php?p=notice&pre_save=1&rally_id='.$rally_id.'&ident=1&app=1&id='.$ident_id.'">ブログの仮登録</a></div>';
	}
	echo '</div>';
}
db_close( $db );

if(isset($preview)) {
	echo '<div class="boxContainer">';
	if ($preview == '1') {
		echo '<div class="box "><a class="easyLoginBtn" href="'.'../easy/login.php?p=notice&show_saveonly=1&rally_id='.$rally_id.'&ident=1&app=1&id='.$ident_id.'">仮投稿管理</a></div>';
		echo '<div class="box "><a class="easyLoginBtn" href="'.'../easy/login.php?p=notice&rally_id='.$rally_id.'&ident=1&app=1&id='.$ident_id.'">お知らせ（ブログ）配信</a></div>';
	} else {
		echo '<div class="box "><a class="easyLoginBtn" href="'.'../easy/login.php?p=notice&pre_save=1&rally_id='.$rally_id.'&ident=1&app=1&id='.$ident_id.'">ブログの仮登録</a></div>';
	}
	echo '</div>';
}
