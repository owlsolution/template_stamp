<?php
/**
 * スタンプタイプBの処理
 * クーポン自動発行で必要な値をもらい、アプリ側に渡す
 * $coupon_num_array = クーポン消費スタンプ数
 * $congrats_pop_flg = スタンプ数がMAXを超えた時のフラグ
 * $up_result = スタンプ数を更新フラグ
 * $error_dialog_title = ダイアログに表示：スタンプ読み込みエラータイトル
 * $error_dialog_detail = ダイアログに表示：スタンプ読み込みエラー詳細
 */
echo $coupon_num_array.",".$congrats_pop_flg.",".$up_result.",".$error_dialog_title.",".$error_dialog_detail."";