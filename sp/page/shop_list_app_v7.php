<?php
/*
 * お店情報一覧取得
 */
require_once('./../common/model/AdminModel.php');
$adminModel = new AdminModel();

// 組織IDを取得
$org_id = empty($_GET['org_id']) ? "" : $_GET['org_id'];
// カテゴリIDを取得
$category_id = empty($_GET['category_id']) ? "" : $_GET['category_id'];

$db = db_connect();
// ユーザIDから、所属する支店を特定→支店の組織IDから組織配下の店舗一覧を取得する
// 所属がない場合全店舗のadmin_idを取得する

$admin_id = ADMIN_ID;
if (!empty($org_id)) {
	$org = $adminModel->get_organization_by_id($db, $org_id);
	$admin_id = $org['admin_id'];
	$admin_type = $adminModel->get_admin_type($db, $admin_id);
	$admin_id_list = $adminModel->get_all_admin_id_by_own_admin_id($db, $admin_id, $admin_type);
} else if (!empty($category_id)) {
	$branch = $adminModel->get_branch_by_branch_category_id($db, $category_id);
	$admin_id_list = array_column($branch, 'child_admin_id');
}

$admin_id_list = array_filter($admin_id_list, "strlen");
$admin_list = implode(',', $admin_id_list);

error_log("admin_list:".$admin_list);

//$admin_list = ADMIN_ID;
//// 支店がある場合、支店のadmin_idを取得
//$admin_ids = getChildAdminIds($db, ADMIN_ID);
//foreach ($admin_ids as $id) {
//	$admin_list .= ",".$id;
//}

$where = "admin_id IN (".$admin_list.")";
$shop_date = shop_select($db , $where);
db_close( $db );

foreach( $shop_date as $key => $value) {
	$store_info[] = array(
		"id" => $value['id'],
		"admin_id" => $value['admin_id'],
		"store_name" => $value['store_name'],
		"business_start" => $value['business_start'],
		"business_end" => $value['business_end'],
		"store_address" => $value['store_address'],
		"tel" => $value['tel'],
		"store_rest" => $value['store_rest'],
		"store_prelusion" => $value['store_prelusion'],
		"hp_url" => $value['hp_url'],
		"map_url" => $value['map_url'],
		"business_hours" => $value['business_hours'],
		"phone_number" => $value['phone_number'],
	);
}

$return_array = array( 
	"store_info_data" => $store_info
);

error_log(json_encode( $return_array ));
header("Content-Type: application/json; charset=utf-8");
print json_encode($return_array);
?>