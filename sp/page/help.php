<?php
/*
 * ヘルプページ
 */
require "./page/header1.php";
$stamp_page = larry_page_acquisition($rally_id);
$use_page_date = $stamp_page['use_page_date'];  //スマホへッダー
?>
<?php
if(!isset($_GET['app'])){
?>
<h1> <img src="./../../sp_images/s_title4.png" width="290"> </h1>
<?php
}
?>
<h2 class="search_title">
スタンプの貯め方
</h2>
<ul class="stamp_box">
	<li>
		1.アプリのメニューにある「スタンプ読み込み」をタッチしてください。<br>
		<img src="./../../sp_images/h_img1.png" width="100%">
	</li>
	<li>
		2.お店にある専用端末から、スタンプ取得QRコードを読み込んでください。<br>
		<div align="center"><img src="./../../sp_images/h_img3.png" width="50%"></div>
	</li>
	<li>
		3,正しく読み込みこんだらスタンプが取得できます。
	</li>
</ul>
<h2 class="search_title">
クーポンの取得方法(スマホ)
</h2>
<ul class="stamp_box">
	<li>
		1.クーポンが貯まるとスタンプカードの上に表示される「クーポンの取得はコチラ」をタッチしてください。
	</li>
	<li>
		2.クーポン発行ページが表示されますので、取得できるクーポンをタッチしてください。
	</li>
	<li>
		3.クーポン取得に必要な個数に満たしていたらスタンプを消費してクーポンを発行します。
	</li>
</ul>

<h2 class="search_title">
クーポンの取得方法(店舗端末)
</h2>
<ul class="stamp_box">
	<li>
		1.店舗端末から取得されるクーポンを選択してください
	</li>
	<li>
		2.アプリのメニューにある「スタンプ読み込み」からQRコードを読み込んでください。
	</li>
	<li>
		3.クーポン取得に必要な個数に満たしていたらスタンプを消費してクーポンを発行します。
	</li>
</ul>
<h2 class="search_title">
クーポンの使い方
</h2>
<ul class="stamp_box">
	<li>
		1.アプリのメニューにある「取得済みクーポン」をタッチしてください。<br>
		<img src="./../../sp_images/h_img2.png" width="100%">
	</li>
	<li>
		2.使用したいクーポンの「使用する」をタッチしてください。
	</li>
	<li>
		3.確認画面をお店の従業員におみせください。
	</li>
</ul>
<?php
require "./page/footer.php";
?>