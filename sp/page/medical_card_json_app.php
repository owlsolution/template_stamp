<?php
require_once('./../common/model/UserModel.php');
$userModel = new UserModel();

require_once('./../common/model/UploadImageModel.php');
$uploadImageModel = new UploadImageModel();

require_once('./../common/model/FixReserveModel.php');
$fixReserveModel = new FixReserveModel();

if (empty($_GET['id'])) {
	exit;
}

/*
 * ユーザ情報をjson形式で返却[アプリ]
 */
$db = db_connect();
// ユーザ情報取得
$userInfo = $userModel->find_by_devide_num($db, $_GET['id']);
if (empty($userInfo)) {
	exit;
}
// 未設定の場合、空設定
$userInfo['membership'] = isset($userInfo['membership']) ? $userInfo['membership'] : '';
$userInfo['birth_date'] = ($userInfo['birth_date'] != '0000-00-00') ? date('Y/m/d', strtotime($userInfo['birth_date'])) : '00/00/00';


// 最新の画像設定日取得
$lastUploadImg = $uploadImageModel->last($db, $userInfo['user_id']);
$userInfo['upload_image_id'] = $lastUploadImg['upload_image_id'];
$userInfo['upload_image_date'] = date("Y/m/d",strtotime($lastUploadImg['create_at']));
// 画像の更新が必要かをチェック

$userInfo['is_nessesary_upload'] = '0';

if (!$lastUploadImg) {
	// 過去データなし
	$userInfo['is_nessesary_upload'] = '1';
}else if(date('Ym', strtotime($lastUploadImg['create_at'])) < date('Ym')) {
	$userInfo['is_nessesary_upload'] = '2';
}

// 次回治療診察日取得
$nearest_rsv = $fixReserveModel->find_nearest_by_rally_id_and_user_id($db, $rally_id, $userInfo['user_id']);
if($nearest_rsv) {
	$userInfo['next_comming_date'] = date("Y/m/d H:i", strtotime($nearest_rsv['fix_date']));
} else {
	$userInfo['next_comming_date'] = '00/00/00';
}

db_close($db);
error_log(json_encode( $userInfo ));

header("Content-Type: application/json; charset=utf-8");
print json_encode($userInfo);
