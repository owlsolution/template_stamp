<?php
require_once('./../common/model/AdminModel.php');
$adminModel = new AdminModel();

// アプリ連携スクリプト
$script_apl_link =<<<EOF
<script type="text/javascript">
	$(document).ready(function () {
		// 受付ボタンクリック時
		$('.goto_tab').click(function(){
			var action = $(this).attr("id");
			var native_method = "webview://"+action;
			if (navigator.userAgent.indexOf('iPhone') > 0 ){
				document.location = native_method;
			} else if(navigator.userAgent.indexOf('Android') > 0 ){
				alert(native_method);
			}
			// 50ms遅らせてフォーム実行
			setTimeout(function(){
				$('#people_mum_form').submit();
			},50);
		})
	});
</script>
EOF;


/*
 * お知らせページ[アプリ用]
 */
// お知らせIDをキーに
$notice_id = $_GET['notice_id'];
$stamp_page = larry_page_acquisition($rally_id);
$smart_notice_page = $stamp_page['smart_notice_app_page'];  //スマートお知らせページ
$title_page = $stamp_page['title_notice_page'];  //お知らせページタイトル
$smart_header_page = $stamp_page['smart_header_page'];  //スマホへッダー
$smart_footer_page = $stamp_page['smart_footer_page'];  //スマホフッター
$notice_content = "";

/**
 * URL文字列の自動リンク化用コールバック
 *
 * @param $matches
 * @return string
 */
function replaceAutoLinkCb($matches) {
	if (strlen($matches[1])) return $matches[0];
	return '<a href="'.$matches[2].'">'.$matches[2].'</a>';
}

/*
 * お知らせ情報取得
 */
$db = db_connect();
$where = " notice_id ='".$notice_id."' ORDER BY notice_id DESC LIMIT 0 , 1";
$notice_date = notice_select($db , $where);
$notice = mysql_fetch_array($notice_date);
$notice_day = $notice['notice_data'];
$notice_title = $notice['notice_title'];
$notice_content = $notice['notice_content'];
db_close( $db );

/*
 * 既読処理
 */
if(!empty($notice_id) && !empty($user_id)){
	// 未読かどうかを判定
	$db = db_connect();
	$readFlag = is_already_read_notice($db, $notice_id, $user_id);
	if ($readFlag == 0) {
		// 未読の場合,既読(1)にUPDATEする
		$where = "notice_id = '".$notice_id."' AND user_id = '".$user_id."'";
		$set = " read_flag = '1', read_datetime = now()";
		notice_read_history_update($db , $set, $where);
		
		// カウントアップ
		$where = "notice_id = '".$notice_id."'";
		$set = "already_read_num = already_read_num + 1";
		notice_db_edit($db , $set ,$where);

	} else if ($readFlag == -1) {
		// 未登録の場合、既読(2)でインサートする
		$set = "notice_id = '".$notice_id."' , user_id = '".$user_id."' , read_flag = '2'";
		notice_read_history_insert($db , $set);
		
		// カウントアップ
		$where = "notice_id = '".$notice_id."'";
		$set = "already_read_num = already_read_num + 1";
		notice_db_edit($db , $set ,$where);

	}else {
		// 既読なら何もしない
	}
	db_close( $db );
}

// タイプにより表示切替え
if ($notice['notice_type'] == 1) {
    // チラシの場合
    $thumbnail_file_name = $notice['thumbnail'];
    $smart_notice_page = "<div id='box'><img src='./../notice_images/".$thumbnail_file_name."'></div>";
    
    require "./page/header_leafret.php";
    echo $smart_notice_page;
    require "./page/footer.php";
    exit;
}
/*
 * アプリ用詳細のベース
 */
// デフォルトはお知らせのやつ
if ($notice['notice_type'] == 2) {
	// ブログの場合
	$db = db_connect();
	$where = "admin_id = '".$notice['admin_id']."'";
	$staff_data = staff_select($db , $where);
	$icon_image = mysql_fetch_array($staff_data);
	if(!empty($icon_image['img_path'])){
		$icon_path = "../staff_images/".$icon_image['img_path'];
	} else {
		// admin_idの社員がいないので、管理者のサムネイル設定をみる、それでもなければデフォの画像を返す
		$icon_path = "../staff_images/person.png";
		$admin = $adminModel->get_admin_by_admin_id($db, $notice['admin_id']);
		if (!empty($admin['img_path'])) {
			$icon_path = "../staff_images/".$admin['img_path'];
		}
	}
	db_close( $db );
}
//お知らせ配信の場合、デフォルトのアイコンを表示するようにする。
/*
 * ['notice_type'] = 0 == お知らせ
 *				   = 1 == チラシ
 *				   = 2 == ブログ
 *				   = 3 == 個人宛て
 */
if($notice['notice_type'] == 0 || $notice['notice_type'] == 3 || $notice['notice_type'] == 4 || $notice['notice_type'] == 5){
	$smart_notice_page = $stamp_page['smart_notice_app_page'];
	$smart_notice_page = str_replace("#notice_data#", $notice_day , $smart_notice_page);
	$smart_notice_page = str_replace("#notice_title#", $notice_title , $smart_notice_page);
} else if($notice['notice_type'] == 1 || $notice['notice_type'] == 2){
	$smart_notice_page =<<<EOF
	<div class="news-box">
	    <div class="clearfix box">
	    <img src="{$icon_path}" width="60" height="60" align="left" style="border-radius: 10px; -webkit-border-radius: 10px;-moz-border-radius:10px;">
	        <div class="text-box">
	             <p>#notice_data#</p>
	             <h2>
	             #notice_title#
	             </h2>
	        </div>
	    </div>
	      <ul>
	      #notice_content#
	      </ul>
	    </div>
	</div>
EOF;

$smart_notice_page = str_replace("#notice_data#", $notice_day , $smart_notice_page);
$smart_notice_page = str_replace("#notice_title#", $notice_title , $smart_notice_page);
}

//URLにリンクをつける。
$url_pattern = '/((?:= *)?["\']?)?(https?:\/\/[:\.0-9a-zA-Z_%~\/\?&=\-#\+\!$\'()*,;@\[\]]+)/si';
$url_count = preg_match_all($url_pattern , $notice_content, $url_match);

for($i = 0; $i < $url_count; $i++){
	$notice_content = preg_replace_callback($url_pattern , 'replaceAutoLinkCb', $notice_content);
}
//<img # 画像ファイル名 #>を正規表現で抜き出す。
//<img # #>を<a href></a>タグに変換させる。
//リソース参照先の設定
//リンクジャンプで画像拡大するように設定。
$img_count = preg_match_all("/<img #(.+?)#>/", $notice_content, $match);
for($i = 0; $i < $img_count; $i++){
	$img_tag_temp ='<a href = "page/notice_image_display_app.php?img_name='.$match[1][$i].'" ><img src="./../notice_images_m/'.$match[1][$i].'" width="100%"></a>';
	$notice_content = str_replace($match[0][$i], $img_tag_temp, $notice_content);
	//facebook：share用の画像を表示
	$facebook_share_wallpaper = $match[1][0];
}
error_log("share_wall_url = ".$share_wall);
$notice_content = nl2br($notice_content);

//お知らせ記事HTMLのURL
$like_url = DOMAIN.FILE_NAME."/public/notice/notice.php?notice_id=".$notice_id;
//shareした記事の画像を表示
$share_wallpaper = DOMAIN.FILE_NAME."/notice_images/".$facebook_share_wallpaper;

//tweet用URL
//本番環境
//$tweet_url = "http%3A%2F%2Fowl-solution.jp%2Fstamp%2F".FILE_NAME."%2Fpublic%2Fnotice%2Fnotice.php%3Fnotice_id%3D".$notice_id;
//$tweet_url = "http%3A%2F%2Fowl-solution.jp%2Fstamp%2F".FILE_NAME."%2Fpublic%2Fnotice%2Fnotice.php%3Fnotice_id%3D".$notice_id;
$tweet_url = urlencode($like_url);

//お知らせ詳細にSNSボタン機能を追加。
$notice_content .=<<<EOF
<style>
.sns_btn_all{
	margin-bottom: 30px;
}
.sns_btn {
	float: right;
	margin-right : 5px;
}
</style>
<HTML xmlns="http://www.w3.org/1999/xhtml" lang="ja" xml:lang="ja" dir="ltr" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<meta property="og:type" content="article" /> 
	<meta property="og:title" content="{$notice_title}">
	<meta property="og:image" content="{$share_wallpaper}" />
</head>
<body>
	<br>
	<br>
	<div class="sns_btn_all">
	<div class="sns_btn">
		<div class="line-it-button" data-lang="ja" data-type="share-a" data-url="{$like_url}" style="display: none;"></div>
		<script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>
		<script type="text/javascript">LineIt.loadButton();</script>
	</div>
	<div class="sns_btn">
		<a href="https://www.facebook.com/sharer/sharer.php?u={$like_url}" target="_blank"><img src="./../img_other/share-button.png" width="60px;" height="19px;"/></a>
	</div>
	<div class="sns_btn">
		<a href="twitter://post?message={$tweet_url}"><img src="./../img_other/tweet.png" width="60px;" height="21px;"/></a>
	</div>
</body>
</HTML>
</div>
EOF;

$smart_notice_page = str_replace("#notice_content#", $notice_content , $smart_notice_page);

require "./page/header.php";
// リンク用スクリプト出力
echo $script_apl_link;
echo $smart_notice_page;
require "./page/footer.php";
?>
