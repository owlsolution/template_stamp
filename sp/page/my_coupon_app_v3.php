<?php
/*
 * 取得済クーポンページ
 */
$stamp_page = larry_page_acquisition($rally_id);
$smart_header_page = $stamp_page['smart_header_page'];  //スマホへッダー
$smart_acq_coupon_page = $stamp_page['smart_acq_coupon_page'];  //スマートフォン取得クーポンページ
$smart_footer_page = $stamp_page['smart_footer_page'];  //スマホフッター
$title_page = $stamp_page['title_acq_coupon_page'];
/*
 * ラリークーポン情報取得
 */
$db = db_connect();
$where = "rally_id = ".$rally_id;
$goal_date = goal_select($db , $where);
$goal = mysql_fetch_array($goal_date);
db_close( $db );
$coupons = array();
for($s=1;$s<=10;$s++){
	if(!empty($goal['goal_stamp_num_'.$s])){
		//ユーザーのクーポン情報を取得する
		$result_get_coupon = get_user_coupon($goal['goal_stamp_num_'.$s] , $rally_id , $user_id);
		$min_date = is_null($result_get_coupon['min_date']) ? '0000-00-00 00:00:00' : $result_get_coupon['min_date'];
		$date_parts = Util::get_rally_of_day($min_date, $rally_id);
		$coupons[] = array("stamp_num" => $result_get_coupon['coupon_num'] , "coupon_use_end" => $date_parts[0]."-".$date_parts[1]."-".$date_parts[2]);
	}
}

// プレゼントクーポン
$db = db_connect();
$get_coupon_gift_date = get_coupon_information_gift($db , $rally_id , $user_id);

$gift_coupons = array();
while($get_coupon_gift = mysql_fetch_array($get_coupon_gift_date)){
	$use_times = explode(":", $get_coupon_gift['coupon_use_end']);
	$coupon_use_limit = $use_times[0].":".$use_times[1];
	$gift_coupons[] = array( "gift_name" => $get_coupon_gift['get_coupon_name'], "gift_limited_day" => $coupon_use_limit, "get_coupon_id" => $get_coupon_gift['get_coupon_id']);
}
db_close( $db );

// 初回プレゼントクーポン
$db = db_connect();
$first_gift_data = first_get_coupon_information_gift($db , $rally_id , $user_id);
$first_gift_coupons = array();
while($first_gift = mysql_fetch_array($first_gift_data)){
	$use_times = explode(":", $first_gift['coupon_use_end']);
	$coupon_use_limit = $use_times[0].":".$use_times[1];
	$first_gift_coupons[] = array("gift_name" => $first_gift['get_coupon_name'], "gift_limited_day" => $coupon_use_limit, "get_coupon_id" => $first_gift['get_coupon_id']);
}
db_close( $db );

$return_array = array( 
    "gift_coupon" => $gift_coupons,
    "coupon" => $coupons,
    "first_gift_coupon" => $first_gift_coupons
);

error_log(json_encode( $return_array ));
header("Content-Type: application/json; charset=utf-8");
print json_encode($return_array);
?>