<?php
/*
 * お店情報ページ (WebView)
 */
$db = db_connect();
$admin_list = ADMIN_ID;
// 支店がある場合、支店のadmin_idを取得
$admin_ids = getChildAdminIds($db, ADMIN_ID);
foreach ($admin_ids as $id) {
    $admin_list .= ",".$id;
}

$where = "admin_id IN (".$admin_list.")";
$shop_date = shop_select($db , $where);
db_close( $db );

foreach( $shop_date as $key => $value) {
//	if ($admin_list == ADMIN_ID) {
//		// 支店機能なし(通常)
//		$store_info[] = array("admin_id" => $value['admin_id'] , "store_name" => $value['store_name']);
//	} else {
//		// 支店機能あり
//		$store_info[] = array("admin_id" => $value['admin_id'] , "store_name" => $value['store_name']);
//	}
	$store_info[] = array("admin_id" => $value['admin_id'] , "store_name" => $value['store_name']);
}

$return_array = array( 
	"store_info_data" => $store_info
);

error_log(json_encode( $return_array ));
header("Content-Type: application/json; charset=utf-8");
print json_encode($return_array);
?>