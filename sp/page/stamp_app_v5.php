<?php
//**********************//
// スタンプページ[アプリ用] //
//**********************//

/*
 * ユーザ情報を取得
 */
$rally_user = user_information_acquisition($rally_id , $user_id);

/*
 * ユーザ名
 */
$user_name = user_name_check($user_id);

/*
 * プロフィール登録チェック
 */
$db = db_connect();
$where = "user_id = ".$user_id;
$all_user = all_user_select($db , $where);
$user = mysql_fetch_array($all_user);
db_close( $db );
$user_check = "end";
//if(empty($user['user_name']) || empty($user['birth_date']) || empty($user['region']) || empty($user['sex'])){
if(empty($user['user_name'])){
	// 名前、誕生日、地域、性別のいずれかが未設定の場合、YETを設定する
	$user_check = "yet";
}
if(empty($user_name)){
	$user_name = USER_ID.sprintf("%05d", $user_id);
}

/*
 * プロフィール強要度を取得
 */
$profile_force = get_profile_force($rally_id);


/*
 * お知らせタイトルを取得
 */
//$notice_note = $rally_user['notice_note'];  // お知らせIDのリスト
//$notice_title = "";
//if(!empty($notice_note)){
//	$notice_note_array = explode (",", $notice_note);
//	$where = "admin_id = ".$rally['admin_id']." AND notice_id = ".$notice_note_array[0];
//	$db = db_connect();
//	$notice_date = notice_select($db , $where);
//	$notice_array = mysql_fetch_array($notice_date);
//	//お知らせタイトル
//	$notice_title = $notice_array['notice_title'];
//	db_close( $db );
//}
//notice_read_historyテーブルから最新のnotice_idを取得
$db = db_connect();
$where = 'user_id = '.$user_id.' ORDER BY id DESC LIMIT 0,1';
$notice_data = notice_read_history_select($db , $where);
$notice_array = mysql_fetch_array($notice_data);
$notice_id = $notice_array['notice_id'];
db_close( $db );
if(!empty($notice_id)){
	//$notice_note_array = explode (",", $notice_note);
	//$where = "admin_id = ".$rally['admin_id']." AND notice_id = ".$notice_note_array[0];
	//$where = "admin_id = ".$rally['admin_id']." AND notice_id = ".$notice_id;
	$where = "notice_id = ".$notice_id;
	$db = db_connect();
	$notice_date = notice_select($db , $where);
	$notice_array = mysql_fetch_array($notice_date);
	$notice_title = $notice_array['notice_title'];
	db_close( $db );
}
/*
 * ユーザが所持しているスタンプ数
 */
$stamp_num = $rally_user['stamp_num'];

/*
 * 合計スタンプ数とMAXスタンプ数を計算して、周回数を表示させる。（ceil = 小数点以下切り上げ）
 */
$rally = larry_information_acquisition($rally_id);
//店舗が設定しているスタンプMAX数
$stamp_max = $rally['stamp_max'];
//ユーザが取得してきた総合スタンプ数
$total_stamp_num = $rally_user['total_stamp_num'];
//周回数
$stamp_round_num = ceil($total_stamp_num/$stamp_max);
if (empty($stamp_round_num)) {
	$stamp_round_num = '1';
}

/*
 * ラリー定義更新日付を取得
 */
$db = db_connect();
$where = "rally_id = ".$rally_id;
$rally_info = rally_select($db , $where);
$rally = mysql_fetch_array($rally_info);
db_close( $db );
//error_log("ラリー更新日付 : ".$rally['rally_define_datetime']);


/**
 * アクセスアプリのバージョンチェック
 */
$version_up_flag = "0";
// サービス終了フラグONの場合、99を返す
if (SERVICE_END == '1') {
	$profile_force = '99';
} else {
	if ((APL_UPDATE != "0") && isset($_GET['ver'])) {
		$version = $_GET['ver'];
		error_log("version:".$version);
		if ($version < LATEST_VERSION) {
			// アクセスしてきたアプリのバージョンが古い場合、アップデートを促すフラグを設定
			$version_up_flag = APL_UPDATE;
		}
	}
}
/*
 * アプリに必要な情報をjson形式で渡す
 * $user_name   ユーザーID(ユーザネーム)
 * $stamp_num   スタンプ数
 * $stamp_max   店舗が設定しているスタンプMAX数
 * $stamp_round_num   スタンプ周回数
 * $user_check   プロフィール登録チェック
 * $profile_force   プロフィール要求モード 0:default 2:アプリ初期起動時 5:アプリ初期起動時&クーポン取得時
 * $notice_title   お知らせタイトル
 * $stamp_imgs_name   スタンプ情報【stamp_off画像名 , stamp_on画像名】
 * $coupons   クーポン情報【クーポン名 , クーポン画像名】
 */
$return_array = array( 
	"user_name" => $user_name,
	"stamp_num" => $stamp_num,
	"stamp_max" => $stamp_max,
	"stamp_round_num" => $stamp_round_num,
	"user_check" => $user_check,
	"profile_force" => $profile_force,
	"notice_title" => $notice_title,
	"coupon_refix_date" => $rally['rally_define_datetime'],
	"version_up" => $version_up_flag
);

error_log(json_encode( $return_array ));
header("Content-Type: application/json; charset=utf-8");
print json_encode($return_array);
?>