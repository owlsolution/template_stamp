<?php
/*
 * 支店情報[アプリ]
 */

// 支店名を取得
$db = db_connect();
$branch_list = branch_select_by_owner($db, ADMIN_ID);
db_close($db);

// 支店がある場合、一覧を返却
foreach ($branch_list as $branch) {
    echo $branch['id'].",".$branch['name'].",".$branch['sub_id']."\n";
}

?>