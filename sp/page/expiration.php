<?php
/*
 * 有効期限切れページ
 */
$stamp_page = larry_page_acquisition($rally_id);
$smart_expired_page = $stamp_page['smart_expired_page'];  //スマートフォンスタンプページ
$title_page = $stamp_page['title_expired_page'];  //スマートフォンスタンプページタイトル
$smart_header_page = $stamp_page['smart_header_page'];  //スマホへッダー

$smart_expired_page = str_replace("#reissue_url#", "./?rally_id=".$rally_id."&user_id=".$user_id."&issue=on&ident=".$classification , $smart_expired_page);

require "./page/header.php";
echo $smart_header_page;
echo $smart_expired_page;
require "./page/footer.php";
?>