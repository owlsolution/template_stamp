<?php
require_once('./../common/model/AdminModel.php');
/*
 *  最新情報取得[アプリ用]
 */

// ラリー情報を取得
$db = db_connect();
$rally = get_one_rally_by_rally_id($db, $rally_id);
db_close($db);

// 支店を含むadmin idのリストを取得
$db = db_connect();
$admin_list[] = ADMIN_ID;

// user_idでbranch_idを取得する。
$where_branch_id = "user_id = ".$user_id;
$rally_user_data = rally_user_select($db , $where_branch_id);
$user_info = mysql_fetch_array($rally_user_data);
//error_log("user_data_branch_id : ".$user_info['branch_id']);

/*
 * 配信対象のAdmin_idを精査
 */
// ラリーIDで全スタッフ取得
$adminModel = new AdminModel();
$staffs = $adminModel->get_staff_by_rally_id($db, $rally_id);

// オーナー直下のスタッフ
$staff_list = $adminModel->filter_staff_by_under_owner($staffs);
if (!empty($staff_list)) $admin_list = array_merge($admin_list, array_column($staff_list, 'admin_id'));

if(!empty($user_info['branch_id'])){
	$branch_id = $user_info['branch_id'];
	
	// 支店情報を取得
	$branch = $adminModel->get_branch_by_id($db, $branch_id);
	// 組織情報を取得
	$organization = $adminModel->get_organization_by_id($db, $branch['organization_id']);
	
	// 組織アカウントのadmin_id
	if (!empty($organization['admin_id'])) {
		$admin_list[] = $organization['admin_id'];
	}
	
	// 対象のADMIN_IDを設定
	if (!empty($branch['child_admin_id'])) {
		$admin_list[] = $branch['child_admin_id'];
	}
	
	// 組織IDから組織直下取得
	$staff_list = null;
	$staff_list = $adminModel->filter_staff_by_organization_id($staffs, $organization['id']);
	if (!empty($staff_list)) $admin_list = array_merge($admin_list, array_column($staff_list, 'admin_id'));
	
	// ブランチidから支店直下のスタップ
	$staff_list = null;
	$staff_list = $adminModel->filter_staff_by_branch_id($staffs, array($branch_id));
	if (!empty($staff_list)) $admin_list = array_merge($admin_list, array_column($staff_list, 'admin_id'));
}


// admin idの条件生成
$admin_list = array_filter($admin_list, "strlen");
$in_admins = implode(',', $admin_list);
$where_admin = "admin_id IN (". $in_admins.")";
db_close( $db );

// ユーザへ見せるお知らせのIDのリストを取得する
$db = db_connect();
// notice_read_historyからユーザ宛になっているnotice_idを取得
$where = "user_id = '".$user_id."' ";
$notice_history_rows = notice_read_history_select($db, $where);
$where_notice_id = "";
while ($notice_history_row = mysql_fetch_array($notice_history_rows)){
	if (empty($where_notice_id)) {
		$where_notice_id = $notice_history_row['notice_id'];
	} else {
		$where_notice_id .= ", ".$notice_history_row['notice_id'];
	}
}

db_close( $db );

if (empty($where_notice_id)) {
	$where_notice_id = "";
} else {
	$where_notice_id = "OR notice_id IN(".$where_notice_id.")";
}

// お知らせ情報からブログ/チラシ全件
$notice_content = "";
$db = db_connect();
//＊＊＊＊＊＊＊＊＊＊お知らせ情報＊＊＊＊＊＊＊＊＊＊

$notice_list = array();
if (!empty($in_admins)) {
	$read = "";
	$where = $where_admin." AND (notice_type IN(1,2) ".$where_notice_id.") AND notice_data < now() AND not transition = 1 ORDER BY notice_id DESC";
	$notice_data = notice_select($db , $where);
	while ($notice = mysql_fetch_array($notice_data)){
		$read = "1";
		// 0:未読 1:既読 -1:未配布(インストール前に配信されたブログとチラシ)
		$read_status = is_already_read_notice($db, $notice['notice_id'], $user_id);
		
		if (empty($notice_last)) {
			// 当該ユーザに配信した最後お知らせ
			// idがアプリ保持のidより新しくてもread_statusが1なら新着とは見なさない
			$notice_last = array(
					"id" => $notice['notice_id'],
					"title" => $notice['notice_title'],
					"read" => ($read_status == "0") ? $read_status :"1",
					"view" => ($read_status == "0") ? $read_status :"1",
			);
		}
		if ($read_status == "0") {
			// 一つでも未読があったらreadフラグを未読にしてブレーク
			$read = "0";
			break;
		}
	}
	if(!empty($read)) {
		$notice_last["read"] = $read;
	}
}
if(empty($notice_last)) {
	$notice_last["id"] = "0";
	$notice_last["title"] = "no title";
	$notice_last["read"] = "1";
	$notice_last["view"] = "1";
}
db_close( $db );

// 現在有効な最新のプレゼントクーポンを取得する

// 初回プレゼントクーポン
$db = db_connect();
$first_gift_data = first_get_coupon_information_gift($db , $rally_id , $user_id);
$first_gift_coupons = array();
//error_log("初回プレゼントクーポン : ".$goal['first_present_img_name']);
while($first_gift = mysql_fetch_array($first_gift_data)){
	$present_last = array(
				"id" => $first_gift['get_coupon_id'],
				"title" => $first_gift['get_coupon_name'],
				"read" => "0",
				"view" => "0",
	);
	break;
}
db_close( $db );

// プレゼントクーポン
$db = db_connect();
$get_coupon_gift_date = get_coupon_information_gift($db , $rally_id , $user_id);
$gift_coupons = array();
while($get_coupon_gift = mysql_fetch_array($get_coupon_gift_date)){
//	error_log("get_coupon_gift : ".$get_coupon_gift);
//	$use_times = explode(":", $get_coupon_gift['coupon_use_end']);
//	$coupon_use_limit = $use_times[0].":".$use_times[1];
//	$gift_coupons[] = array( "gift_name" => $get_coupon_gift['get_coupon_name'], "gift_limited_day" => $coupon_use_limit, "get_coupon_id" => $get_coupon_gift['get_coupon_id'] , "gift_img_name" => $get_coupon_gift['get_coupon_img'] , "coupon_isuue_flag" => $get_coupon_gift['coupon_isuue_flag']);
	
	$present_last = array(
				"id" => $get_coupon_gift['get_coupon_id'],
				"title" => $get_coupon_gift['get_coupon_name'],
				"read" => "0",
				"view" => "0",
	);
	break;
}
db_close( $db );

$return_array = array( 
	"notice" => $notice_last,
	"present" => $present_last
);

header("Content-Type: application/json; charset=utf-8");
print json_encode($return_array);
?>