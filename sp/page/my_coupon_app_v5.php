<?php
/*
 * 取得済クーポンページ
 */
$stamp_page = larry_page_acquisition($rally_id);
$smart_header_page = $stamp_page['smart_header_page'];  //スマホへッダー
$smart_acq_coupon_page = $stamp_page['smart_acq_coupon_page'];  //スマートフォン取得クーポンページ
$smart_footer_page = $stamp_page['smart_footer_page'];  //スマホフッター
$title_page = $stamp_page['title_acq_coupon_page'];
/*
 * ラリークーポン情報取得
 */
$db = db_connect();
$where = "rally_id = ".$rally_id;
$goal_date = goal_select($db , $where);
$goal = mysql_fetch_array($goal_date);
db_close( $db );
$coupons = array();
for($s=1;$s<=10;$s++){
	if($goal['goal_stamp_num_'.$s] != 0){
		//ユーザーのクーポン情報を取得する
		$result_get_coupon = get_user_coupon($goal['goal_stamp_num_'.$s] , $rally_id , $user_id);
		$min_date = is_null($result_get_coupon['min_date']) ? '0000-00-00 00:00:00' : $result_get_coupon['min_date'];
		$date_parts = Util::get_rally_of_day($min_date, $rally_id);
		//クーポン名とクーポン画像名をアプリに追加で渡す
		$coupons[] = array("stamp_num" => $result_get_coupon['coupon_num'] , "coupon_use_end" => $date_parts[0]."-".$date_parts[1]."-".$date_parts[2] , "coupon_name" => $goal['goal_title_'.$s] , "coupon_image_name" => $goal['img_name_'.$s] , "id" => $s);
	}
}

// プレゼントクーポン
$db = db_connect();
$get_coupon_gift_date = get_coupon_information_gift($db , $rally_id , $user_id);
$gift_coupons = array();
$cnt = 0;
while($get_coupon_gift = mysql_fetch_array($get_coupon_gift_date)){
	error_log("get_coupon_gift : ".$get_coupon_gift);
	$use_times = explode(":", $get_coupon_gift['coupon_use_end']);
	$coupon_use_limit = $use_times[0].":".$use_times[1];
	$gift_coupons[] = array( "gift_name" => $get_coupon_gift['get_coupon_name'], "gift_limited_day" => $coupon_use_limit, "get_coupon_id" => $get_coupon_gift['get_coupon_id'] , "gift_img_name" => urlencode($get_coupon_gift['get_coupon_img']) , "coupon_isuue_flag" => $get_coupon_gift['coupon_isuue_flag']);
	if ($cnt > 10) {
		break;
	}
	$cnt++;
}
db_close( $db );

// 初回プレゼントクーポン
$db = db_connect();
$first_gift_data = first_get_coupon_information_gift($db , $rally_id , $user_id);
$first_gift_coupons = array();
//error_log("初回プレゼントクーポン : ".$goal['first_present_img_name']);
while($first_gift = mysql_fetch_array($first_gift_data)){
	$use_times = explode(":", $first_gift['coupon_use_end']);
	$coupon_use_limit = $use_times[0].":".$use_times[1];
	$first_gift_coupons[] = array("gift_name" => $first_gift['get_coupon_name'], "gift_limited_day" => $coupon_use_limit, "get_coupon_id" => $first_gift['get_coupon_id'] , "first_gift_img_name" => $goal['first_present_img_name'], "coupon_isuue_flag" => $first_gift['coupon_isuue_flag']);
}
db_close( $db );
/*
 * ラリー定義更新日付を取得
 */
$db = db_connect();
$where = "rally_id = ".$rally_id;
$rally_info = rally_select($db , $where);
$rally = mysql_fetch_array($rally_info);
db_close( $db );
//error_log("ラリー更新日付 : ".$rally['rally_define_datetime']);
$return_array = array( 
    "gift_coupon" => $gift_coupons,
    "coupon" => $coupons,
    "first_gift_coupon" => $first_gift_coupons,
    "coupon_refix_date" => $rally['rally_define_datetime']
);

error_log(json_encode( $return_array ));
header("Content-Type: application/json; charset=utf-8");
print json_encode($return_array);
?>