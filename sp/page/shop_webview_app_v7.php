<?php
/*
 * 店舗情報WebView
 */
require "./page/header_store_webview.php";

$db = db_connect();
if (empty($_GET['admin_id'])) {
	$admin_id = ADMIN_ID;
	// 支店がある場合、支店のadmin_idを取得
	$admin_ids = getChildAdminIds($db, ADMIN_ID);
	foreach ($admin_ids as $id) {
			$admin_id .= ",".$id;
	}
} else {
	$admin_id = $_GET['admin_id'];
}
$where = "admin_id IN(".$admin_id.")";
$shop_date = shop_select($db , $where);
db_close( $db );

require "./page/shop_entry_manager_app_v7.php";

// union:0 店舗それぞれ。
// union:1 複数の店舗情報をまとめて処理する  #XXX_1#のように通番で処理する。デザインは先頭のデザインのみ使用
$union = SHOP_VIEW_TYPE;

$store_tel = "";
$store_business = "";

if ($union == 1) {
	$cnt = 1;
	$store_info_design = "";
	foreach( $shop_date as $key => $store_info) {
		if ($cnt == 1) {
			// 先頭のデザインを取得して保持
			$store_info_design = $store_info['store_info_design'];
		}
		
		if(empty($store_info['phone_number'])){
			$store_tel = $store_info['tel'];
		} else {
			$store_tel = $store_info['phone_number'];
		}
		if(empty($store_info['business_hours'])){
			$store_business = $store_info['business_start']."~".$store_info['business_end'];
		} else {
			$store_business = $store_info['business_hours'];
		}
		$index = "_".$cnt;
		$store_key_list = array("#store_name".$index."#","#store_address".$index."#","#map_url".$index."#","#business_hours".$index."#","#tel".$index."#","#store_rest".$index."#","#hp_url".$index."#","#store_prelusion".$index."#");
		$store_value_list = array($store_info['store_name'] , nl2br(htmlspecialchars($store_info['store_address'])) , $store_info['map_url'] , nl2br(htmlspecialchars($store_business)) , $store_tel , nl2br(htmlspecialchars($store_info['store_rest'])) , $store_info['hp_url'] , nl2br(htmlspecialchars($store_info['store_prelusion'])));
		//独自タグの置換
		$store_info_design = str_replace($store_key_list, $store_value_list, $store_info_design);
		$cnt++;
	}
	echo $store_info_design;
}else {
	foreach( $shop_date as $key => $store_info) {
		if(empty($store_info['phone_number'])){
			$store_tel = $store_info['tel'];
		} else {
			$store_tel = $store_info['phone_number'];
		}
		if(empty($store_info['business_hours'])){
			$store_business = $store_info['business_start']."~".$store_info['business_end'];
		} else {
			$store_business = $store_info['business_hours'];
		}
		$store_key_list = array("#store_name#","#store_address#","#map_url#","#business_hours#","#tel#","#store_rest#","#hp_url#","#store_prelusion#");
		$store_value_list = array($store_info['store_name'] , $store_info['store_address'] , $store_info['map_url'] , nl2br(htmlspecialchars($store_business)) , $store_tel , nl2br(htmlspecialchars($store_info['store_rest'])) , $store_info['hp_url'] , nl2br(htmlspecialchars($store_info['store_prelusion'])));
		//独自タグの置換
		$store_info_design = str_replace($store_key_list, $store_value_list, $store_info['store_info_design']);
		echo $store_info_design;
		$cnt++;
	}
	
}


require "./page/footer.php";
