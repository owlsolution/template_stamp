<?php
/*
 * 機種変ID&PASS入力ページ
 */
$stamp_page = larry_page_acquisition($rally_id);
$smart_stamp_page = $stamp_page['smart_stamp_page'];  //スマートフォンスタンプページ
$smart_header_page = $stamp_page['smart_header_page'];  //スマホへッダー
$Title = "機種変更のIDとパスワードの入力";
require 'header1.php';
?>
<?php
if(!isset($_GET['app'])){
?>
<h1> <img src="./../../setting_images/title4.png" width="290"> </h1>
<?php
}
?>
<h2 class="search_title">
機種変更のIDとパスワードの入力
</h2>
<ul class="stamp_box">
<?php
//echo $smart_header_page;
if(isset($_POST['submit'])){
	$inp_id = $_POST['id'];
	$inp_pass = $_POST['pass'];
	if($inp_id != "" && $inp_pass != ""){
		$count = model_change_count_search($inp_id , $inp_pass);
		if($count != 0){
			model_change_date_search($inp_id , $inp_pass , $ident_id);
			$Mobile_Change = "OK";
		} else {
			$msg = input_error();
		}
	} else {
		$msg = input_error();
	}
}
if($Mobile_Change != "OK"){
?>
	<form method="POST" action="./?p=id_input&rally_id=<?php echo $rally_id;?>&id=<?php echo $ident_id;?>&ident=<?php echo $classification;?>">
	<li><p><span style = 'color:red; font-size:12px;'>発行されたIDとパスワードを入力してください。</span></p></li>
	<p><?php echo $msg ;?></p>
	<li>
    	<p style="margin-bottom:5px;">ID</p>
        <input type="text" name="id">
    </li>
    <li>
    	<p style="margin-bottom:5px;">パスワード</p>
       <input type="text" name="pass">
    </li>
    <br>
    <br>
    <div align="center">
    <input type="submit" class="btn" name="submit" style="width:80%" value="送信する">
    </div>
    <br>
    <br>
    </form>
<?php
} else {
?>
<li>
	<center>
	<br>
	<p>内容を登録しました。</p>
	<br>
	</center>
</li>
<?php
}
?>
</ul>
<br><br><br>
</body>
</html>