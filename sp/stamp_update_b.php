<?php
require_once('./../../config.php');
require_once('./../ad_config.php');
require "./syntax.php";

//  ID:日付,パス;個数+識別?個体識別~店番
/*
 * $user_id          ユーザーID
 * $rally_id         ラリーID
 * $issue_date       日付
 * $issue_pass       パスワード
 * $s_num            個数
 * $classification   識別
 * $shop_num         店番
 */
$ident_id = $_GET['id'];//個体識別ID
$issue = $_GET['issue'];  //ラリー暗号
if (Device::is_android_browser()) {
	$issue = base64_decode($issue);
}
$ident = $_GET['ident'];//識別
$str = split(":",$issue);
//$str = explode(":", $issue);
$rally_id = $str[0];  //ラリーID
$stri = split(",", $str[1]);
$issue_date = $stri[0];  //日付
$strin = split(";", $stri[1]);
$issue_pass = $strin[0];//パス
$string = split("%", $strin[1]);
$s_num = $string[0];  //個数
$strings = split("/", $string[1]);
$shop_num = $strings[0];  //店番
$classification = $strings[1];//識別
$kind = $strings[1];// スタンプ種別
$basename = basename(dirname(dirname(__FILE__)));//スタンプフォルダ名
/*
echo "a".$rally_id."<br>";
echo "b".$issue_date."<br>";
echo "c".$issue_pass."<br>";
echo "d".$s_num."<br>";
echo "e".$classification."<br>";
*/

//＊＊＊＊＊＊＊＊＊＊＊＊＊ユーザーID取得＊＊＊＊＊＊＊＊＊＊＊＊＊
$user_id = Util::get_authenticated_user_id();
if (empty($user_id)) {
        // 個体識別名を渡さずにここへ来ると、非対称機種と判定して、画面を見せる
	header('Location:./unsupport.php');
//	header("Location:".DOMAIN."sp_login.php?rally_id=".$rally_id."&basename=".rawurlencode($basename)."&redirect=".rawurlencode($_SERVER['REQUEST_URI']).Util::rebuild_get_params());
	exit;
}


//スタンプ付与する前のユーザーが持っているスタンプ数を取得する 
$rally_user = user_information_acquisition($rally_id , $user_id);
//スタンプ数
$stamp_num = $rally_user['stamp_num'];

//店舗のスタンプMAX数を取得
$rally = larry_information_acquisition($rally_id);
$stamp_max = $rally['stamp_max'];

$user_information_acquisition_date = user_information_acquisition($rally_id , $user_id);
if (empty($user_information_acquisition_date)) {
	if ($ident_id != "") {
		require "./page/header.php";
		echo('<center><img src="'.DOMAIN.'/sp_images/era2.png"  width="100%"/></center>');
		require "./page/footer.php";
		exit;
	}
	$store = 1;
	header("Location:./index.php?rally_id={$rally_id}&store={$store}");
	exit;
}

//プロフィール登録要求度の設定
$profile_force = get_profile_force($rally_id);
error_log("プロフィール要求度チェック : ".$profile_force);

//プロフィール登録チェック
$db = db_connect();
$where = "user_id = ".$user_id;
$all_user = all_user_select($db , $where);
$user = mysql_fetch_array($all_user);
db_close( $db );

$user_check = "end";
if(empty($user['user_name'])){
    // 名前、誕生日、地域、性別のいずれかが未設定の場合、YETを設定する
    $user_check = "yet";
}

//スタンプ合計数
$update_stamp_num = $stamp_num+$s_num;
$goal = page_setup_acquisition($rally_id);
//クーポンの一番目の取得スタンプ数を取得
$coupon_first_stamp_num = $goal['goal_stamp_num_1'];

if((($profile_force == "3") || ($profile_force == "5")) && ($user_check == "yet") && ($update_stamp_num >= $coupon_first_stamp_num)) {
	// プロフィールチェックあり、かつ、プロフィール未登録の場合、かつ、１つ目クーポン発行数に達する場合
	// スタンプ付与はしない
	$up_result = "unregistered_profile";
} else {
	if ($kind == 2) {
		// 種別が2の場合、月極共通スタンプ(月に１回、１ユーザ、１回、全店舗共通)
		// $stamp_num = ユーザーの所持スタンプ数
		$up_result = monthly_stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num, $kind, $stamp_num);
	} else if ($kind == 3) {
		// 種別が3の場合、有効期限日数指定スタンプ(期間中に１回、１ユーザ、１回、全店舗共通)
		$up_result = day_duration_stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num, $kind, $stamp_num);
	} else if ($kind == 4) {
		// 種別が4の場合、有効期限無期限 複数ユーザが同じQRコードを利用可能 、１時間で再発行可能、全店舗共通
		$up_result = eternal_duration_stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num, $kind, $stamp_num);
	} else if ($kind == 5) {
		// 種別が5の場合、来店チェックイン処理 複数ユーザが同じQRコードを利用可能、各店舗
		$up_result = checkin_update($user_id , $rally_id , $issue_date , $issue_pass);
	} else if ($kind == 6) {
		// 種別が6の場合、共通スタンプ指定期間中毎日利用可能(期間中、１ユーザあたり１日１回付与可能、複数人利用可能)
		$up_result = everyday_duration_stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num, $kind, $stamp_num);
	} else {
		
		// デフォでは通常のスタンプ追加処理
		$up_result = Stamp_update_b($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num , $stamp_num);
	}
}
$congrats_pop_flg = "0";
if($up_result == "ok"){
	//スタンプ合計数
	$update_stamp_num = $stamp_num+$s_num;
	
	$now_date = date('Y-m-d');   //今日の日付
	$now_date2 = date('Y-m-d H:i:s');   //今日の日時

	$congrats_pop_flg = "0";

	//クーポン発行
	$get_coupon_state = 0;
	$goal = page_setup_acquisition($rally_id);
	
	for($i = $stamp_num +1; $i <= $update_stamp_num; $i++){
		// スタンプ最大数で割ったあまりが、現時点のスタンプ数。周回でリセットするため
		$current_stamp_pos = (($i-1)%$stamp_max)+1;
		for($coupon_no = 1; $coupon_no <= 10; $coupon_no++){
			$get_coupon_name = $goal['goal_title_'.$coupon_no];   //クーポン名
			$coupon_stamp_num = $goal['goal_stamp_num_'.$coupon_no];   //クーポンスタンプ数
			$get_coupon_description = $goal['goal_description_'.$coupon_no];   //クーポン内容
			$get_coupon_img = $goal['img_name_'.$coupon_no];   //クーポン画像名
			if($current_stamp_pos == $coupon_stamp_num){
				$get_user_info[] = new_coupon_b($rally_id , $user_id , $coupon_stamp_num , $get_coupon_name , $get_coupon_description , $get_coupon_img , $get_coupon_state , $now_date2 , $now_date);
			}
		}
	}
	
	//スタンプ数がMAXを超えた時、スタンプをリセットする。
	if($update_stamp_num > $stamp_max){
		//リセット後のスタンプ数
		$reset_stamp_num = $update_stamp_num%$stamp_max;
		$db = db_connect();
		$set = "stamp_num = ".$reset_stamp_num." , last_stamp_date = '".$now_date2."'";
		$where = "user_id = ".$user_id." AND rally_id = ".$rally_id." AND admin_id = ".ADMIN_ID;
		rally_user_up($db , $set ,$where);
		db_close( $db );
//		//スタンプリセット後、クーポン取得スタンプ数に到達していたら、クーポンを発行
//		for($i = 1; $i <= $reset_stamp_num; $i++){
//			for($coupon_no = 1; $coupon_no <= 10; $coupon_no++){
//				$get_coupon_name = $goal['goal_title_'.$coupon_no];   //クーポン名
//				$coupon_stamp_num = $goal['goal_stamp_num_'.$coupon_no];   //クーポンスタンプ数
//				$get_coupon_description = $goal['goal_description_'.$coupon_no];   //クーポン内容
//				$get_coupon_img = $goal['img_name_'.$coupon_no];   //クーポン画像名
//				if($i == $coupon_stamp_num){
//					$get_user_info[] .= new_coupon_b($rally_id , $user_id , $coupon_stamp_num , $get_coupon_name , $get_coupon_description , $get_coupon_img , $get_coupon_state , $now_date2 , $now_date);
//				}
//			}
//		}
		//スタンプMAX数を超えた時におめでとうポップアップを表示させる
		$congrats_pop_flg = "1";
	}
	
	$coupon_num_array = $get_user_info[0];
	if($coupon_num_array != ""){
		$get_coupon_flg = true;
	} else {
		$get_coupon_flg = false;
	}
	if($get_coupon_flg === true){
		for($i = 1 ; $i < count($get_user_info); $i++){
			$coupon_num_array .= "&amp".$get_user_info[$i];
			error_log("クーポン消費スタンプ数 : ".$coupon_num_array);
		}
	}
} else if($up_result == "future_date_qr_ng"){
	$error_dialog_title = "使用できるQRコードではありません。";
	$error_dialog_detail = "今月使用できるQRコードでもう一度、読み込みをしてください。";
} else if($up_result == "not_date_qr_ng"){
	$error_dialog_title = "今月使用できるQRコードではありません。";
	$error_dialog_detail = "新しいQRコードでもう一度読み込みしてください。";
} else if($up_result == "chk_cnt_qr_ng"){
	$error_dialog_title = "既に使用されたQRコードです。";
	$error_dialog_detail = "新しいQRコードでもう一度読み込みしてください。";
} else if($up_result == "ago_date_qr_ng"){
	$error_dialog_title = "過去に発行されたQRコードです。";
	$error_dialog_detail = "新しいQRコードで、もう一度読み込みしてください。";	
} else if($up_result == "one_day_ng"){
	$error_dialog_title = "今日は既にスタンプが付与されています。";
	$error_dialog_detail = "後日、QRコードを読み込み、スタンプの付与をしてください。";
} else if($up_result == "qr_ng"){
	$error_dialog_title = "既に使用されたQRコードです。";
	$error_dialog_detail = "新しいQRコードでもう一度読み込みをしてください。";
} else if($up_result == "day_duration_future_date_qr_ng"){
	$error_dialog_title = "QRコードは有効期間に達していません。";
	$error_dialog_detail = "読込んだQRコードは今日の時点で、有効期間に達していません。利用期間をご確認ください。";
} else if($up_result == "day_duration_ago_date_qr_ng"){
	$error_dialog_title = "QRコードは利用期間を過ぎています。";
	$error_dialog_detail = "有効なQRコードでもう一度読み込みをしてください。";
} else if($up_result == "eternal_duration_invalid_qr_ng"){
	$error_dialog_title = "QRコードは無効です。";
	$error_dialog_detail = "読込んだQRコードは無効なQRコードのため使用できません。";
} else if($up_result == "chk_limited_qr_ng"){
	$error_dialog_title = "QRコード連続使用制限";
	$error_dialog_detail = "QRコードは１度利用するとすぐには利用できません。しばらく時間をおいてやり直してください。";
} else if($up_result == "checkin_invalid_qr_ng"){
	$error_dialog_title = "QRコードチェックインエラー";
	$error_dialog_detail = "無効なQRコードが読込まれました";
} else if($up_result == "checkin_not_targetdata_today"){
	$error_dialog_title = "QRコードチェックインエラー";
	$error_dialog_detail = "本日は診察予定日ではありません。受付にてご確認をお願い致します。";
}

error_log("アップデートフラグ : ".$up_result);
require "./page/stamp_b.php";
?>