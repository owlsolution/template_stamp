<?php
require_once('./../../config.php');
require_once('./../ad_config.php');
require "./syntax.php";

$rally_id = $_GET['rally_id'];

$user_id = Util::get_authenticated_user_id();

// 存在しないユーザ
if (empty($user_id)) {
	// ユーザIDが無い場合、以降の処理はしない
	error_log("exit(); userid : empty");
	exit();
}
//error_log("userid : ".$user_id);

$img_upload_dir = "./../upload_images/";

// ファイルを一意にするためのプレフィックス
$file_name = date('YmdHis').'_'.uniqid();

// 将来的には共通化したいが、時間がかかりそうなので分けておく。
if($_FILES['upload_image']){
	// iOSはupload_imageにてtype=fileとして処理できる
	$file_name .= '_'.$_FILES["upload_image"]["name"];
//	move_uploaded_file($_FILES['upload_image']['tmp_name'], '/tmp/test.jpg');
	if (move_uploaded_file($_FILES["upload_image"]["tmp_name"], $img_upload_dir.$file_name) != true) {
		$ary = array(
				'code'=>"501", 
				'message'=>"failed save file", 
			);
		$json = json_encode($ary);
		header("Content-Type: application/json; charset=utf-8");
		print($json);
		return;
	}
	if(chmod($img_upload_dir.$file_name, 0644) != true){
		$ary = array(
				'code'=>"502", 
				'message'=>"failed change file mode", 
			);
		$json = json_encode($ary);
		header("Content-Type: application/json; charset=utf-8");
		print($json);
		return;
	}
	$ary = array(
		'code'=>"1", 
		'message'=>"post file success", 
		);
} else {
	// Androidはbodyへの直ストリームで画像データが飛んでくる
	$file_name .= '_data.jpg';
	$data = file_get_contents("php://input");
	$fp = fopen($img_upload_dir.$file_name , 'wb');
	fwrite($fp, $data);
	fclose($fp);
	if(chmod($img_upload_dir.$file_name, 0644) != true){
		$ary = array(
			'code'=>"502", 
			'message'=>"failed change file mode", 
			);
		$json = json_encode($ary);
		header("Content-Type: application/json; charset=utf-8");
		print($json);
		return;
	}
	
	$ary = array(
		'code'=>"0", 
		'message'=>"upload success", 
		);
}

//$ary['name'] = $img_upload_dir.$file_name;
// uploadした画像を管理するためレコード作成する。
// user_id rally_id 
$db = db_connect();
$set = "rally_id = '".$rally_id."', user_id = '".$user_id."', path_name = '".$img_upload_dir.$file_name."', name = '".$file_name."' , create_at = now(), update_at = now()";
upload_image_create($db, $set);
db_close($db);

$json = json_encode($ary);
header("Content-Type: application/json; charset=utf-8");
print($json);

