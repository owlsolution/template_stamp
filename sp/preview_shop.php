<?php
require_once('./../../config.php');
require_once('./../ad_config.php');
require "./syntax.php";

require_once('./../common/model/AdminModel.php');

$adminModel = new AdminModel();

$p = $_GET['p'];
$admin_id = empty($_GET['admin_id']) ? 0 : $_GET['admin_id'];

$db = db_connect();
$rally_id = $adminModel->get_rally_id($db, $admin_id);
db_close($db);

switch ($p){
	case 'shop_app': //お店情報ページ[アプリ]
		$sys = "./page/shop_app.php";
		break;
	case 'shop_webview_app_v5': // 店舗情報WebView表示v5[アプリ]
		if(isset($_GET['mode'])) {
			$preview = $_GET['mode'];
		}
		$sys = "./page/shop_webview_app_v5.php";
		break;
	case 'shop_app_v5': // 店舗情報v5[WebView][アプリ]
		$sys = "./page/shop_app_v5.php";
		break;
	default:
		break;
}

require $sys;