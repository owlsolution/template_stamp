<?php
require_once('./../../config.php');
require_once('./../ad_config.php');
require "./syntax.php";

$p = $_GET['p'];
$rally_id = $_GET['rally_id'];
$ident_id = $_GET['id'];
$ident = $_GET['ident'];
$classification = Util::get_classification();
$issue = $_GET['issue'];
$device_id = $_GET['uuid'];

// rally_idのバリデーションチェック
if (empty($rally_id) || !is_numeric($rally_id)) {
	// 数値以外が含まれている場合、処理しない
	error_log("attack rally_id sql injection:".$rally_id);
	header('Location:./unsupport.php');
	exit;
}

if(empty($_GET['id']) || (strlen($_GET['id']) >= 128)) {
	// idが異常値の場合処理しない
	error_log("attack id sql injection:".$_GET['id']);
	header('Location:./unsupport.php');
	exit;
}

error_log("get_result : ".$p);

$basename = basename(dirname(dirname(__FILE__)));
// ユーザ作成かどうかをチェック
// ユーザ重複作成の防止
if(!empty($_SESSION[$ident_id])){
	// ONだと生成中なので、waitする
	for ($count = 0; $count < 10;$count++) {
		error_log("session uuid : ".$_SESSION[$ident_id]." id:".$ident_id);
		if (empty($_SESSION[$ident_id])) {
			// ユーザができたらクリアされたら少し待って再開
			usleep(100000);
			break;
		} else {
			// 500msを10回秒待つ
			usleep(500000);
		}
	}
	unset($_SESSION[$ident_id]);
}
$user_id = Util::get_authenticated_user_id();

//初めてユーザー登録の処理
if(empty($user_id) && empty($_SESSION[$ident_id])){
	$_SESSION[$ident_id] = "on";
	
	// 個体識別無しなので、非対象と判断する
	if (Device::is_android_appli()) {
		// uuidをデバイステーブルに保存
		$db = db_connect();
		$add_date = date('Y-m-d h:i:s');
//		$into = "'".$ident_id."' , '".$add_date."'";
		$into = "'".Util::sanitize_sql($ident_id)."' , '".$add_date."'";
		$user = user_insert($db , $into);
		db_close( $db );
		if (!empty($device_id)) {
			// 端末情報を保存する
			$db = db_connect();
			$set = "rally_id = '".$rally_id."', user_id = '".$user['user_id']."', divide_num = '".Util::sanitize_sql($_GET['id'])."', device_id = '".Util::sanitize_sql($_GET['uuid'])."' , create_date = now()";
			device_insert($db, $set);
			db_close($db);
			error_log("incert:".$_GET['id']);
		}
		
		// ユーザID再取得
		$user_id = Util::get_authenticated_user_id();
		
		// Androidアプリの場合はユーザを登録してスタンプ付与
		unset($_SESSION[$ident_id]);
	} else {
		// ルートが違うのでセッションクリアする
		unset($_SESSION[$ident_id]);
		
		// 上記以外の場合はユーザ情報設定ページにリダイレクト
		header('Location:../../sp_login.php?rally_id='.$rally_id.'&basename='.rawurlencode($basename).'&redirect='.rawurlencode($_SERVER['REQUEST_URI']));
		exit;
	}
}

if (empty($user_id)) {
	// ユーザIDが無い場合、以降の処理はしない
	error_log("exit(); userid : empty");
	exit();
}
error_log("userid : ".$user_id);

//ユーザー登録は有り（スタンプラリー登録）
get_rally_user_id($user_id , $rally_id , $classification, $device_id);

//有効期限のチェック
require_once('./expiration_date.php');

/**
 * 最終ログイン日時を更新する
 */
$db = db_connect();
$login_date_update = user_login_date_update($db , $user_id , $rally_id);
db_close($db);

if (empty($p)) {
	// アクセスのみ
	exit();
}

switch ($p){
	case 'obtain': //クーポン取得の確認ページ
		$sys = "./page/obtain.php";
		break;
	case 'id_issuance': //機種変ID&PASS発行ページ
		$sys = "./page/id_issuance.php";
		break;
	case 'id_input': //機種変ID&PASS入力ページ
		$sys = "./page/id_input.php";
		break;
	case 'get_goods': //クーポン使用ページ
		$sys = "./page/get_goods.php";
		break;
	case 'pass_change': //パスワード変更ページ
		$sys = "./page/pass_change.php";
		break;
	case 'qr_get_goods': //QRコードからクーポンを使用するページ
		$sys = "./page/qr_get_goods.php";
		break;
	case 'withdrawal': //退会ページ
		$sys = "./page/withdrawal.php";
		break;
	case 'withdrawal_end': //退会ページ
		$sys = "./page/withdrawal_end.php";
		break;
	case 'terms': //利用規約ページ
		$sys = "./page/terms.php";
		break;
	case 'privacy': //プライバシー・ポリシー
		$sys = "./page/privacy.php";
		break;
	case 'help': //ヘルプページ
		$sys = "./page/help.php";
		break;
	case 'stamp_app': //トップ(スタンプページ)[アプリ用]
		$sys = "./page/stamp_app.php";
		break;
	case 'stamp_app_v5': //トップ(スタンプページ)v5[アプリ用]
		$sys = "./page/stamp_app_v5.php";
		break;
	case 'notice_app_v3': //お知らせページv3[アプリ用]
		$sys = "./page/notice_app_v3.php";
		break;
	case 'notice_app_v8': //お知らせページv8[アプリ用]
		$sys = "./page/notice_app_v8.php";
		break;
	case 'notice_detail_app_v3': //お知らせページ詳細v3[アプリ用]
		$sys = "./page/notice_detail_app_v3.php";
		break;
	case 'notice_detail_app_v4': //お知らせページ詳細v4[アプリ用]
		$sys = "./page/notice_detail_app_v4.php";
		break;
	case 'notice_image_display_app': //お知らせ詳細の元画像表示[アプリ用]
		$sys = "./page/notice_image_display_app.php";
		break;
	case 'shop_app': //お店情報ページ[アプリ]
		$sys = "./page/shop_app.php";
		break;
	case 'my_coupon_app': //Myクーポンページ[アプリ]
		$sys = "./page/my_coupon_app.php";
		break;
	case 'my_coupon_app_v3': //Myクーポンページv3[アプリ]
		$sys = "./page/my_coupon_app_v3.php";
		break;
	case 'my_coupon_app_v5': //Myクーポンページv5[アプリ]
		$sys = "./page/my_coupon_app_v5.php";
		break;
	case 'my_coupon_app_v7': //Myクーポンページv7[アプリ]
		$sys = "./page/my_coupon_app_v7.php";
		break;
	case 'use_coupon_app': //クーポン使用ページ[アプリ]
		$sys = "./page/use_coupon_app.php";
		break;
	case 'use_coupon_app_v3': //Myクーポンページv3[アプリ]
		$sys = "./page/use_coupon_app_v3.php";
		break;
	case 'profile_app': //プロフィールページ[アプリ]
		$sys = "./page/profile_app.php";
		break;
	case 'profile_change_app': //プロフィール変更ページ[アプリ]
		$sys = "./page/profile_change_app.php";
		break;
	case 'use_qr_coupon_app': //クーポン使用ページ[アプリ]
		$sys = "./page/use_qr_coupon_app.php";
		break;
	case 'use_qr_coupon_app_b': //クーポン使用ページ[アプリ](typeB)
		$sys = "./page/use_qr_coupon_app_b.php";
		break;
	case 'coupon_info_app_v5': // クーポン情報v5[アプリ]
		$sys = "./page/coupon_info_app_v5.php";
		break;
	case 'stamp_image_name_v5': // スタンプon画像名、スタンプoff画像名v5[アプリ]
		$sys = "./page/stamp_image_name_v5.php";
		break;
	case 'store_images_preview_v5': // 店舗画像名v5[アプリ]
		$sys = "./page/store_images_preview_v5.php";
		break;
	case 'shop_webview_app_v5': // 店舗情報WebView表示v5[アプリ]
		$sys = "./page/shop_webview_app_v5.php";
		break;
	case 'shop_webview_app_v7': // 店舗情報WebView表示v7[アプリ]
		$sys = "./page/shop_webview_app_v7.php";
		break;
	case 'shop_app_v5': // 店舗情報v5[WebView][アプリ]
		$sys = "./page/shop_app_v5.php";
		break;
	case 'new_check_app_v5': // 最新情報取得[アプリ]
		$sys = "./page/new_check_app_v5.php";
		break;
	case 'shop_list_app_v7': // 店舗情報のリスト取得[アプリ]
		$sys = "./page/shop_list_app_v7.php";
		break;
	case 'org_list_app_v7': // 組織一覧取得[アプリ]
		$sys = "./page/org_list_app_v7.php";
		break;
	case 'branch_category_list_app_v7': // 支店カテゴリ一覧取得[アプリ]
		$sys = "./page/branch_category_list_app_v7.php";
		break;
	case 'first_profile_app': // 初期プロフィール有無フラグ[アプリ]
		$sys = "./page/first_profile_app.php";
		break;
	case 'is_branch_added_app': // 支店登録有無チェック[アプリ]
		$sys = "./page/is_branch_added_app.php";
		break;
	case 'update_user_branch_app': // ユーザが所属する支店を更新[アプリ]
		$sys = "./page/update_user_branch_app.php";
		break;
	case 'branch_json_app': // 支店一覧情報[アプリ]
		$sys = "./page/branch_json_app.php";
		break;
	case 'reserve_app_v8': // 支店一覧情報[アプリ]
		$sys = "./page/reserve_app_v8.php";
		break;
	case 'post_reserve_app': // 予約情報保存[アプリ]
		$sys = "./page/post_reserve_app.php";
		break;
	case 'post_inquiry_app': // 問合せ情報保存[アプリ]
		$sys = "./page/post_inquiry_app.php";
		break;
	case 'renewal_app': // 支店一覧情報[アプリ]
		$sys = "./page/renewal_app.php";
		break;
	case 'user_json_app': // ユーザ情報[アプリ]
		$sys = "./page/user_json_app.php";
		break;
	case 'medical_card_json_app': // 診察カード情報[アプリ]
		$sys = "./page/medical_card_json_app.php";
		break;
	case 'resend_upload_img_json_app': // 画像再送[アプリ]
		$sys = "./page/resend_upload_img_json_app.php";
		break;
	default:
		break;
}

require $sys;