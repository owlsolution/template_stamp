<?php
require_once('./../../config.php');
require_once('./../ad_config.php');
require "./syntax.php";

//  ID:日付,パス;個数+識別?個体識別~店番
$ident_id = $_GET['id'];//個体識別ID
$issue = $_GET['issue'];  //ラリー暗号
if (Device::is_android_browser()) {
	$issue = base64_decode($issue);
}
$ident = $_GET['ident'];//識別
$str = split(":",$issue);
//$str = explode(":", $issue);
$rally_id = $str[0];  //ラリーID
$stri = split(",", $str[1]);
$issue_date = $stri[0];  //日付
$strin = split(";", $stri[1]);
$issue_pass = $strin[0];//パス
$string = split("%", $strin[1]);
$s_num = $string[0];  //個数
$strings = split("/", $string[1]);
$shop_num = $strings[0];  //店番
//$classification = $strings[1];//識別
$kind = $strings[1];// スタンプ種別
$basename = basename(dirname(dirname(__FILE__)));//スタンプフォルダ名
/*
echo "a".$rally_id."<br>";
echo "b".$issue_date."<br>";
echo "c".$issue_pass."<br>";
echo "d".$s_num."<br>";
echo "e".$classification."<br>";
*/

//スタンプ付与する前のユーザーが持っているスタンプ数を取得する 
$rally_user = user_information_acquisition($rally_id , $user_id);
//スタンプ数
$stamp_num = $rally_user['stamp_num'];

//＊＊＊＊＊＊＊＊＊＊＊＊＊ユーザーID取得＊＊＊＊＊＊＊＊＊＊＊＊＊
$user_id = Util::get_authenticated_user_id();
if (empty($user_id)) {
        // 個体識別名を渡さずにここへ来ると、非対称機種と判定して、画面を見せる
	header('Location:./unsupport.php');
//	header("Location:".DOMAIN."sp_login.php?rally_id=".$rally_id."&basename=".rawurlencode($basename)."&redirect=".rawurlencode($_SERVER['REQUEST_URI']).Util::rebuild_get_params());
	exit;
}
/*
 * $user_id          ユーザーID
 * $rally_id         ラリーID
 * $issue_date       日付
 * $issue_pass       パスワード
 * $s_num            個数
 * $classification   識別
 * $shop_num         店番
 */
$user_information_acquisition_date = user_information_acquisition($rally_id , $user_id);
if (empty($user_information_acquisition_date)) {
	if ($ident_id != "") {
		require "./page/header.php";
		echo('<center><img src="'.DOMAIN.'/sp_images/era2.png"  width="100%"/></center>');
		require "./page/footer.php";
		exit;
	}
	$store = 1;
	header("Location:./index.php?rally_id={$rally_id}&store={$store}");
	exit;
}

if ($kind == 2) {
	// 種別が2の場合、月極共通スタンプ(月に１回、１ユーザ、１回、全店舗共通)
	$up_result = monthly_stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num, $kind, $stamp_num);
} else if ($kind == 3) {
	// 種別が3の場合、有効期限日数指定スタンプ(月に１回、１ユーザ、１回、全店舗共通)
	$up_result = day_duration_stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num, $kind, $stamp_num);
} else {
	// デフォでは通常のスタンプ追加処理
	$up_result = Stamp_update($user_id , $rally_id , $issue_date , $issue_pass , $s_num , $shop_num);
}
if ($up_result == "ok"){

} else if($up_result == "future_date_qr_ng"){
	$error_dialog_title = "使用できるQRコードではありません。";
	$error_dialog_detail = "今月使用できるQRコードでもう一度、読み込みをしてください。";
} else if($up_result == "not_date_qr_ng"){
	$error_dialog_title = "今月使用できるQRコードではありません。";
	$error_dialog_detail = "新しいQRコードでもう一度読み込みしてください。";
} else if($up_result == "chk_cnt_qr_ng"){
	$error_dialog_title = "既に使用されたQRコードです。";
	$error_dialog_detail = "新しいQRコードでもう一度読み込みしてください。";
} else if($up_result == "ago_date_qr_ng"){
	$error_dialog_title = "過去に発行されたQRコードです。";
	$error_dialog_detail = "新しいQRコードで、もう一度読み込みしてください。";	
} else if($up_result == "one_day_ng"){
	$error_dialog_title = "今日は既にスタンプが付与されています。";
	$error_dialog_detail = "後日、QRコードを読み込み、スタンプの付与をしてください。";
} else if($up_result == "qr_ng"){
	$error_dialog_title = "既に使用されたQRコードです。";
	$error_dialog_detail = "新しいQRコードでもう一度読み込みをしてください。";
} else if($up_result == "day_duration_future_date_qr_ng"){
	$error_dialog_title = "QRコードは有効期間に達していません。";
	$error_dialog_detail = "読込んだQRコードは今日の時点で、有効期間に達していません。利用期間をご確認ください。";
} else if($up_result == "day_duration_ago_date_qr_ng"){
	$error_dialog_title = "QRコードは利用期間を過ぎています。";
	$error_dialog_detail = "有効なQRコードでもう一度読み込みをしてください。";
}

require "./page/stamp.php";
?>