<?php
//お知らせ詳細：HTML表示
require_once './../../../db_func.php';
require_once './../../../config.php';
require_once './../../ad_config.php';

$notice_id = $_GET['notice_id'];
$where = "notice_id = ".$notice_id;
$db = db_connect();
$notice_data = notice_select($db , $where);
$notice = mysql_fetch_array($notice_data);

//お知らせ日時
$notice_datetime = $notice["notice_data"];
//お知らせタイトル
$notice_title = $notice["notice_title"];
//お知らせの詳細
$notice_content = $notice["notice_content"];
db_close($db);

/**
 * URL文字列の自動リンク化用コールバック
 *
 * @param $matches
 * @return string
 */
function replaceAutoLinkCb($matches) {
	if (strlen($matches[1])) return $matches[0];
	return '<a href="'.$matches[2].'" target="_blank">'.$matches[2].'</a>';
}

//お知らせアイコンを表示する。
$db = db_connect();
$where = "admin_id = ".ADMIN_ID;
$rally_date = rally_select($db , $where);
$rally = mysql_fetch_array($rally_date);
$rally_id = $rally['rally_id'];
$where = "rally_id = ".$rally_id;
$stamp_page_date = stamp_page_select($db , $where);
$stamp_page = mysql_fetch_array($stamp_page_date);
$smart_notice_page = $stamp_page['smart_notice_app_page'];  //スマートお知らせページ
//smart_notice_app_pageを正規表現でアイコンURLのみを抽出する。
preg_match('/<img.*>/i', $smart_notice_page, $store_icon_tag);
preg_match('/http.*?(\.gif|\.png|\.jpg|\.jpeg$|\.bmp)/i', $store_icon_tag[0], $store_icon);
//print_r("店舗アイコン画像URL : ".$store_icon[0]);

db_close($db);

//改行タグの挿入：お知らせ詳細
$notice_content = nl2br($notice_content);

//URLにリンクをつける。
$url_pattern = '/((?:= *)?["\']?)?(https?:\/\/[:\.0-9a-zA-Z_%~\/\?&=\-#\+\!$\'()*,;@\[\]]+)/si';
$url_count = preg_match_all($url_pattern , $notice_content, $url_match);

for($i = 0; $i < $url_count; $i++){
	$notice_content = preg_replace_callback($url_pattern , 'replaceAutoLinkCb', $notice_content);
}
//<img # 画像ファイル名 #>を正規表現で抜き出す。
$img_file_name_list = array();
$img_count = preg_match_all("/<img #(.+?)#>/", $notice_content, $match);
for($i = 0; $i < $img_count; $i++){
	$img_tag_temp ='<div class="css-box-shadow"><img src="./../../notice_images/'.$match[1][$i].'" width="100%"></div>';
	//$img_tag_temp ='<img src="./../../notice_images/'.$match[1][$i].'" width="100%">';
	$notice_content = str_replace($match[0][$i], $img_tag_temp, $notice_content);
	$facebook_share_wallpaper = $match[1][0];
	$img_file_name_list[] = $match[1][0];
}
//お知らせ記事HTMLのURL
$like_url = DOMAIN.FILE_NAME."/public/notice/notice.php?notice_id=".$notice_id;
//shareした記事の画像を表示
$share_wallpaper = DOMAIN.FILE_NAME."/notice_images/".$facebook_share_wallpaper;

if (!empty(count($img_file_name_list) > 0)) {
	foreach ($img_file_name_list as $img_file_name) {
		$twitter_image[] = DOMAIN.FILE_NAME."/notice_images/".$img_file_name;
	}
}else {
	$twitter_image[] = DOMAIN.FILE_NAME."/img_other/120.png";
}

// メタタグ用の詳細説明の生成
$og_description = strip_tags($notice_content);
if (!empty($og_description)) {
	$og_description = ereg_replace("[\n|\r|\nr|\t]","",$og_description);
	mb_language('Japanese');
	mb_internal_encoding('UTF-8');
	$og_description = mb_substr($og_description, 0, 100);
}

//tweet用URL
//本番環境
//$tweet_url = "http%3A%2F%2Fowl-solution.jp%2Fstamp%2F".FILE_NAME."%2Fpublic%2Fnotice%2Fnotice.php%3Fnotice_id%3D".$notice_id;
$tweet_url = urlencode($like_url);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<HTML xmlns="http://www.w3.org/1999/xhtml" lang="ja" xml:lang="ja" dir="ltr" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="minimum-scale=0.25, width=device-width, initial-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta property="og:type" content="article" /> 
	<meta property="og:title" content="<?= $notice_title;?>">
	<meta property="og:image" content="<?= $twitter_image[0];?>" />
	
	<!-- Facebook：この内容は他のSNSにも使用されます。 -->
	<meta property="og:locale" content="ja_JP">
	<meta property="og:type" content="article">
	<meta property="og:site_name" content="<?= STAMP_RALLY_NAME; ?>">
	<meta property="og:description" content="<?= $og_description; ?>">

	<!-- Twitter Cards -->
	<?php if($img_count > 0): ?>
		<meta name="twitter:card" content="gallery">
		<?php $i = 0 ?>
		<?php foreach ($twitter_image as $img_file_name): ?>
			<meta name="twitter:image<?= $i++;?>" content="<?= $img_file_name;?>">
		<?php endforeach;?>
	<?php else : ?>
		<meta name="twitter:card" content="summary">
	<?php endif;?>
	
	<!--お知らせタイトルを表示-->
	<title><?php echo $notice_title;?></title>
	<style type="text/css">
		<!--
		body {
			margin-left: 0px;
			margin-top: 0px;
			margin-right: 0px;
			margin-bottom: 0px;
			background-color: #ECFAFF;
		}
		.text {
		font-weight: bold;
		font-size: medium;
		color: #800000
		}
		.text2 {
		font-weight: bold;
		font-size: large;
		color: #333333;
		}
		.text3 {
		font-size: large;
		color: #000000;
		margin-top:5;

		}
		.text4 {
		font-size: medium;
		color: #000000;
		margin-top:5;
		border-color:#666666;
		border-bottom:dashed 1px #000000;
		border-collapse: collapse;
		}
		.text5 {
		font-size: smaller;
		color: #000000;
		}
		.mainbox {
		border-color:#666666;
		border-top:solid 1px #000000;
		border-collapse: collapse;
		}
		.css-box-shadow {
		  width:95%;
		  text-align: center;
		  position: relative;
		  margin-left : auto ; margin-right : auto ;
		  -webkit-box-shadow: 1px 1px 4px rgba(0,0,0,.5);
		  -moz-box-shadow: 1px 2px 4px rgba(0,0,0,.5);
		  box-shadow: 1px 1px 4px rgba(0,0,0,.5);
		  padding: 2px;
		  background: white;
		}
		-->
	</style>
</head>
	<body>
		<table border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#FFFFFF">
		<tr align="right">
			<td colspan="2"><img src="./../../../sp_images/logo.jpg" width="100"></td>
		</tr>
		<tr>
			<!--店舗アイコンの表示-->
			<td width="100" height="100" rowspan="2"><img src="<?php echo $store_icon[0];?>"></td>
			<!--お知らせ配信日時を表示-->
			<td width="320" height="5" align="left" valign="top"><span class="text"><?php echo $notice_datetime;?></span></td>
		</tr>
		<!--お知らせタイトルを表示-->
		<tr>
			<td align="left" valign="top">
				<span class="text2">
					<?php echo $notice_title;?>
				</span>
			</td>
		</tr>
		<!--お知らせ詳細内容を表示-->
		<tr>
			<td colspan="2" width="420">
				<div class="mainbox">
					<?php echo $notice_content;?>
				</div>
					<br>
					<br>
					<div Align="center">
					<script type="text/javascript" src="//media.line.me/js/line-button.js?v=20140411" ></script><script type="text/javascript">new media_line_me.LineButton({"pc":false,"lang":"ja","type":"a","text":"<?php echo $like_url;?>","withUrl":false});
					</script>
					<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $like_url;?>" target="_blank"><img src="./../../img_other/share-button.png" width="60px;" height="19px;"/></a>
					<a href="twitter://post?message=<?php echo $tweet_url;?>"><img src="./../../img_other/tweet.png" width="60px;" height="21px;"/></a>
					</div>
			</td>
		</tr>
		<!--店舗情報を表示-->
		<tr>
			<td colspan="2" width="420">
		<?php
		/*
		 * お店情報ページ
		 */
		$db = db_connect();
		$admin_list = ADMIN_ID;
		// 支店がある場合、支店のadmin_idを取得
		$admin_ids = getChildAdminIds($db, ADMIN_ID);
		foreach ($admin_ids as $id) {
			$admin_list .= ",".$id;
		}
		$where = "admin_id IN (".$admin_list.")";
		$shop_date = shop_select($db , $where);
		db_close( $db );

		foreach( $shop_date as $key => $value) {
			// 営業時間の新カラムが空の場合、旧カラムを使用
			$business_hours = $value['business_hours'];
			if(empty($business_hours) ) {
				$business_start = $value['business_start']."~".$value['business_end'];
			}
			// 電話番号の新カラムが空の場合、旧カラムを使用
			$phone_number = isset($value['phone_number'])  ? $value['phone_number'] : $value['tel'];
			
			//営業時間に改行タグを挿入
			$business_start = nl2br($business_start);
			//店舗情報に改行タグを挿入
			$value['store_prelusion'] = nl2br($value['store_prelusion']);
			if ($admin_list == ADMIN_ID) {
				// 支店機能なし(通常)
		?>
				<p><div class="text4">【店舗名】<br><?php echo $value['store_name'];?></div></p>
				<p><div class="text4">【住所】<br><a href="<?php echo $value['map_url'];?>" target="_blank"><?php echo $value['store_address'];?></a></div></p>
				<p><div class="text4">【営業時間】<br><?php echo $business_hours;?></div></p>
				<p><div class="text4">【電話番号】<br><?php echo $phone_number;?></div></p>
				<p><div class="text4">【定休日】<br><?php echo $value['store_rest'];?></div></p>
				<p><div class="text4">【紹介文】<br><?php echo $value['store_prelusion'];?></div></p>
		<?php
			} else {
				// 支店機能あり
		?>
				<p><div class="text4">【店舗名】<br><?php echo $value['store_name'];?></div></p>
				<p><div class="text4">【住所】<br><a href="<?php echo $value['map_url'];?>" target="_blank"><?php echo $value['store_address'];?></a></div></p>
				<p><div class="text4">【営業時間】<br><?php echo $business_hours;?></div></p>
				<p><div class="text4">【電話番号】<br><?php echo $phone_number;?></div></p>
				<p><div class="text4">【定休日】<br><?php echo $value['store_rest'];?></div></p>
				<p><div class="text4">【紹介文】<br><?php echo $value['store_prelusion'];?></div></p>
		<?php
			}
		}
		?>
			</td>
		</tr>
		<tr align="center">
		<td colspan="2" width="420"> <span class="text5">Copyright © 2015 OWL SOLUTION All Rights Reserved. <br>
			Powered by <a href="http://webnics.jp/">webnics </a>. </span></td>
		</tr>
	</body>
</HTML>