<?php
//お知らせ詳細：HTML表示
require_once './../../../db_func.php';
require_once './../../../config.php';
require_once './../../ad_config.php';

$db = db_connect();
	$rally_date = rally_select($db , "admin_id = ".ADMIN_ID);
	$rally = mysql_fetch_array($rally_date);
	$rally_id = $rally['rally_id'];

	$stamp_page_date = stamp_page_select($db , "rally_id = ".$rally_id);
	$stamp_page = mysql_fetch_array($stamp_page_date);
db_close( $db );
$use_page_date = $stamp_page['use_page_date'];  //スマホへッダー

$messase = '';
if(isset($_POST['transmission'])){
	$people_name = $_POST['people_name'];
	$mail_address = $_POST['mail_address'];
	$content = $_POST['content'];
	mb_language("japanese");
	
	$previous = SUPPORT_EMAIL;//宛先
	$mail_title = "アプリユーザからお問い合わせがありました。";//題名
	$message  = "スタンプカードアプリ名 : ".STAMP_RALLY_NAME."\n"; //メーセージ
	$message .= "お名前 : ".$people_name."\n";
	$message .= "メールアドレス : ".$mail_address."\n";
	$message .= "お問い合わせ内容 : ".$content."\n";
	$message .= "送信日時 : ".date( "Y/m/d (D) H:i:s", time() )."\n";
	$sender = $mail_address; //差出人
	$header = "From:".mb_encode_mimeheader($encSender, "ISO-2022-JP")."<".$sender.">";
	mb_send_mail($previous,$mail_title,$message,$header);
	
//	$previous = $mail_address;//宛先
//	$mail_title = "お問い合わせを受付ました。";//題名
//	$message  = "================================================================\n"; //メーセージ
//	$message .= "お問い合わせ内容の確認\n"; //メーセージ
//	$message .= "================================================================\n";
//	$message .= "この度はご質問・お問い合わせいただき、誠にありがとうございます。\n\n";
//	$message .= "下記の通りお申し込みを受け付けました。\n\n";
//	$message .= "****************************************\n\n";
//	$message .= "お名前 : ".$people_name."\n";
//	$message .= "メールアドレス : ".$mail_address."\n";
//	$message .= "お問い合わせ内容 : ".$content."\n\n";
//	$message .= "****************************************\n\n";
//	$message .= "担当より内容を確認後、返信させて頂きますので、今しばらくお待ちください。\n\n";
//	$message .= "================================================================\n\n";
//	$message .= "アウルソリューション\n";
//	$message .= "http://".HOST_NAME."\n";
//	$message .= "TEL 092-980-2138\n";
//	$message .= "MAIL ".NOTICE_MAIL_FROM_ADDRESS."\n\n";
//	$message .= "================================================================\n\n";
//	$sender = SUPPORT_EMAIL; //差出人
//	
//	$header = "From:".mb_encode_mimeheader($encSender, "ISO-2022-JP")."<".$sender.">";
//	Util::send_mail($previous,$mail_title,$message,$header);
	
	$messase = '受付ました。';
}

$background_color = '#482215';

?>
<html>
	<head>
		<style>
input.btn {
	background:<?= $background_color;?>;
	padding:5px 0px;
	text-align:center;
	width:200px;
	color:#FFF;
	border:0px;
	-webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
	margin:0 10px;
}
.f_box h3 {
	background:<?= $background_color;?> bottom;
	color:#FFF;
	padding:3px 15px;
	border-radius: 5px;        /* CSS3草案 */
	-webkit-border-radius: 5px;    /* Safari,Google Chrome用 */
	-moz-border-radius: 5px;   /* Firefox用 */
	margin-bottom:10px;
	font-weight:500;
	font-size:12px;
}
.f_box ul {
	padding:0 10px;
}
.container1 ul li {
	padding:0 10px;
	margin-bottom:8px;
	list-style:none;
}
.f_text1 {
	width:100%;
	font-size:18px;
	border:#CCC 1px solid;
	border-radius: 5px;        /* CSS3草案 */
	-webkit-border-radius: 5px;    /* Safari,Google Chrome用 */
	-moz-border-radius: 5px;   /* Firefox用 */
}
.f_text2 {
	width:100%;
	height:300px;
	border:#CCC 1px solid;
	border-radius: 5px;        /* CSS3草案 */
	-webkit-border-radius: 5px;    /* Safari,Google Chrome用 */
	-moz-border-radius: 5px;   /* Firefox用 */
}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="box" style="text-align: center;">
				<h2><?php echo $use_page_date;?></h2>
				<?php if (empty($_GET['off'])) { ?>
					<a href="/?rally_id=<?= $rally_id ?>&store=1" target="_blank" style="margin: auto;"><img src="../../img_other/120.png" width="100" border="0"></a>
				<?php } ?>
			</div>
			<div class="box">
				<div style="text-align: center;">
					<h3>アプリ関連のお問い合わせ・ご意見・ご要望はこちら</h3>
				</div>
			</div>
			<div class="box">
				<div class="f_box" >
					<?php if(empty($messase)): ?>
						<form method="POST" action="./?inquiry" name="form2">
							<div class="container1">
								<h3> お名前 </h3>
								<ul class="clearfix">
									<li>
										<input type="text" name="people_name" class="f_text1" value="">
									</li>
								</ul>
							</div>
							<div class="container1">
								<h3> メールアドレス </h3>
								<ul class="clearfix">
									<li>
										<input type="text" name="mail_address" class="f_text1" value="">
									</li>
								</ul>
							</div>
							<div class="container1">
								<h3> 内容 </h3>
								<ul class="clearfix">
									<li>
										<textarea name="content" class="f_text2"></textarea>
									</li>
								</ul>
								<div class="btn" style="text-align: center;">
									<input type="submit" name="transmission" class="btn" value="送 信">
								</div>
							</div>
						</form>
					<?php else: ?>
						<div><?= $messase ?></div>
					<?php endif;?>
				</div>
			</div>
		</div>
	</body>
</html>
