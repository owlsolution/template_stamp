<?php
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'BaseModel.php');

/**
 * uploadした画像イメージ情報にアクセスするクラス
 */
class UploadImageModel extends BaseModel{

	function __construct() {
	}
	
	/**
	 * upload画像情報を取得する
	 * @param type $db
	 * @param type $id
	 * @return boolean
	 */
	public function find_by_id($db, $upload_image_id) {
		if (empty($upload_image_id)) {
			return false;
		}
		
		$datalist = [];
		$where = "upload_image_id = '".$upload_image_id."'";
		$data = upload_image_select($db , $where);
		while ($row = mysql_fetch_array($data)){
			$datalist[] = $row;
		}
		if (!empty($datalist[0])) {
			return $datalist[0];
		}
		return false;
	}
	
	/**
	 * 最新のupload画像情報を取得する
	 * @param type $db
	 * @param type $id
	 * @return boolean
	 */
	public function last($db, $user_id) {
		$datalist = [];
		$where = " user_id = '".$user_id."' ORDER BY upload_image_id DESC LIMIT 1";
		$data = upload_image_select($db , $where);
		while ($row = mysql_fetch_array($data)){
			$datalist[] = $row;
		}
		if (!empty($datalist[0])) {
			return $datalist[0];
		}
		return false;
	}
	
	
	
	/**
	 * ユーザIDを指定して画像情報を取得する
	 * @param type $db
	 * @param type $rally_id
	 * @param type $user_id
	 * @return boolean
	 */
	public function find_by_rally_id_and_user_id($db, $rally_id, $user_id) {
		if (empty($rally_id) || empty($user_id)) {
			return false;
		}
		
		$datalist = [];
		$where = "rally_id = '" . $rally_id . "' AND user_id = '".$user_id."'";
		$data = upload_image_select($db , $where);
		while ($row = mysql_fetch_array($data)){
			$datalist[] = $row;
		}
		if (!empty($datalist)) {
			return $datalist;
		}
		return false;
	}
	
	/**
	 * データ作成
	 * @param type $db
	 * @param type $setdata
	 * @return type
	 */
	public function insert($db, $setdata) {
		$set = "rally_id = '".$setdata['rally_id']."',".
			"user_id = '".$setdata['user_id']."',".
			"path_name = '".$setdata['path_name']."',".
			"name = '".$setdata['name']."',".
			" update_at = now()".",".
			" create_at = now()";
		
		upload_image_create($db, $set);
		$reserve_id = mysql_insert_id();
		return $reserve_id;
	}
	
	/**
	 * データCOPY
	 * @param type $db
	 * @param type $upload_image_id
	 * @return コピー先のデータのid
	 */
	public function copy($db, $upload_image_id) {
		
		$setdata = $this->find_by_id($db, $upload_image_id);
		if ($this->insert($db, $setdata)) {
			return mysql_insert_id();
		}
		return false;
	}
	
	/**
	 * upload画像情報の削除
	 * @param type $db
	 * @param type $upload_image_id
	 */
	public function delete($db, $upload_image_id) {
		$where = "upload_image_id = " . $upload_image_id . "";
		upload_image_delete($db, $where);
	}
	
	
}
