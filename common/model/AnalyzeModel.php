<?php
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'BaseModel.php');

class AnalyzeModel extends BaseModel{
	
	// 月別の新規登録数一覧
	public $newUserNumByMonth = 0;
	
	function __construct($newUserNumByMonth) {
		$this->newUserNumByMonth = $newUserNumByMonth;
	}
	
	/**
	 * 指定した日までののべユーザ数を取得
	 * @param type $day
	 * @return type
	 */
	public function getTotalUserCountByDay($day) {
		$total_num = 0;
		foreach ($this->newUserNumByMonth as $key => $row) {
			if ($row['target_day'] <= $day) {
				$total_num += $row['count'];
			} else {
//				echo $row['target_day']."  ".$row['count']."\n";
			}
		}
		return $total_num;
	}
	
	/**
	 * アクティブユーザ数を取得する
	 * @param type $db
	 * @param type $admin_id_list
	 * @param type $start_day
	 * @param type $end_day
	 * @return type
	 */
	public function getActiveUserNum($db , $admin_id_list, $start_day, $end_day) {
		$data = active_user_count_by_day($db , $admin_id_list, $start_day, $end_day);
		$row = mysql_fetch_array($data);
		if (!is_null($row['count'])) {
			return $row['count'];
		} else {
			false;
		}
	}

	/**
	 * 実効顧客(実際に来店している顧客)数を取得する
	 * @param type $db
	 * @param type $admin_id_list
	 * @param type $start_day
	 * @param type $end_day
	 * @return type
	 */
	public function getActualCustomerNum($db , $rally_id, $start_day, $end_day) {
		$data = actual_user_count_by_day($db , $rally_id, $start_day, $end_day);
		$count = mysql_num_rows ( $data );
		if ($count) {
			return $count;
		} else {
			return 0;
		}
	}
	
	
	/**
	 * 動向データを保存する
	 * @param type $db
	 * @param type $admin_id_list
	 * @param type $start_day
	 * @param type $end_day
	 * @return type
	 */
	public function saveRrend($db , $rally_id, $admin_id, $trend_data) {
		
		$trend = $this->getRrendByRallyId($db , $rally_id, $trend_data['target_date']);
		if ($trend) {
			// 存在するのでUPDATEする。
			$set = "rally_id = '".$rally_id."',"
				."admin_id  = '".$admin_id."',"
				."target_date  = '".$trend_data['target_date']."',"	// Y-m-d
				."total_num  = '".$trend_data['total_num']."',"
				."active_num  = '".$trend_data['active_num']."',"
				."active_num_30  = '".$trend_data['active_num_30']."',"
				."active_num_60  = '".$trend_data['active_num_60']."',"
				."active_num_90  = '".$trend_data['active_num_90']."',"
				."visit_num_30  = '".$trend_data['visit_num_30']."',"
				."visit_num_60  = '".$trend_data['visit_num_60']."',"
				."visit_num_90  = '".$trend_data['visit_num_90']."',"
				."visit_num_180  = '".$trend_data['visit_num_180']."',"
				."modified = now()";
			$where = "rally_id = '" . $rally_id . "' AND target_date = '".$trend_data['target_date']."'";
			trend_update($db, $set, $where);
			error_log("update rally_id:".$rally_id." date:".$trend_data['target_date']);
			return;
		} else {
			$set = "rally_id = '".$rally_id."',"
				."admin_id  = '".$admin_id."',"
				."target_date  = '".$trend_data['target_date']."',"	// Y-m-d
				."total_num  = '".$trend_data['total_num']."',"
				."active_num  = '".$trend_data['active_num']."',"
				."visit_num  = '".$trend_data['visit_num']."',"
				."active_num_30  = '".$trend_data['active_num_30']."',"
				."active_num_60  = '".$trend_data['active_num_60']."',"
				."active_num_90  = '".$trend_data['active_num_90']."',"
				."visit_num_30  = '".$trend_data['visit_num_30']."',"
				."visit_num_60  = '".$trend_data['visit_num_60']."',"
				."visit_num_90  = '".$trend_data['visit_num_90']."',"
				."visit_num_180  = '".$trend_data['visit_num_180']."',"
				."created = now(),"
				."modified = now()";
			trend_create($db, $set);
			$trend_id = mysql_insert_id();
			error_log("trend_id:".$trend_id);
		}
	}
	
	/**
	 * 動向データをrally_idをキーに取得する
	 * @param type $db
	 * @param type $rally_id
	 * @return type
	 */
	public function getRrendByRallyId($db , $rally_id, $target_date) {
		$where = "rally_id = '" . $rally_id . "' AND target_date = '".$target_date."'";
		$data = trend_select($db , $where);
		$trend = mysql_fetch_array($data);
		if ($trend) {
			return $trend;
		}
		return false;
	}
	
	
}
