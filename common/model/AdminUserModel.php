<?php
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'BaseModel.php');

/**
 * AdminUserテーブルへのアクセスクラス
 */
class AdminUserModel extends BaseModel{

	function __construct() {
	}

	/**
	 * ユーザIDに連携するadmin_idを取得する
	 * admin_userテーブル参照
	 * @return type
	 */
	public function get_admin_id_by_user_id($db, $user_id) {
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		$where = "user_id = '" . $user_id . "' LIMIT 0,1";
		$data = admin_user_select($db, $where);
		$admin = mysql_fetch_array($data);

		if (isset($isDbOpen)) {
			db_close($db);
		}
		return $admin;
	}

	/**
	 * admin_idに連携する情報を取得する
	 * admin_userテーブル参照
	 * @return type
	 */
	public function get_adminuser_by_admin_id($db, $admin_id) {
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		$where = "admin_id = '" . $admin_id . "' LIMIT 0,1";
		$data = admin_user_select($db, $where);
		$admin = mysql_fetch_array($data);

		if (isset($isDbOpen)) {
			db_close($db);
		}
		return $admin;
	}
	
	
}
