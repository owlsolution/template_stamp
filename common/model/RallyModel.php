<?php
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'BaseModel.php');

class RallyModel extends BaseModel{

	function __construct() {
	}

	/**
	 * rally_idを渡してラリー情報を取得する
	 * @param type $branch_id
	 * @return
	 */
	public function find_by_rally_id($db, $rally_id = null) {
		
		if ($rally_id == null) {
			return false;
		}
		
		//すべての人数取得
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		$rally = get_one_rally_by_rally_id($db, $rally_id);
		if (isset($isDbOpen)) {
			db_close($db);
		}
		if (empty($rally)) return false;
		return $rally;
	}

	/**
	 * admin_idを渡してラリー情報を取得する
	 * @param type $admin_id
	 * @return
	 */
	public function find_by_admin_id($db, $admin_id = null) {
		
		if ($admin_id == null) {
			return false;
		}
		
		//すべての人数取得
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		
		$rally_date = rally_select($db, "admin_id = " . ADMIN_ID);
		$rally = mysql_fetch_array($rally_date);
			
		if (isset($isDbOpen)) {
			db_close($db);
		}
		if (empty($rally)) return false;
		return $rally;
	}
	
	/**
	 * rally_idを渡してラリー情報を取得する
	 * @param type $branch_id
	 * @return
	 */
	public function find_all($db) {
		
		//すべての人数取得
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		$rallys = get_all_rally($db);
		if (isset($isDbOpen)) {
			db_close($db);
		}
		if (empty($rallys)) return false;
		return $rallys;
	}
	
	/**
	 * rally_idを渡して有効なラリー情報を取得する
	 * @return
	 */
	public function find_active_all($db) {
		
		//すべての人数取得
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		$rallys = get_active_all_rally($db);
		if (isset($isDbOpen)) {
			db_close($db);
		}
		if (empty($rallys)) return false;
		return $rallys;
	}
	
}
