<?php
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'BaseModel.php');

class RallyUserModel extends BaseModel{

	function __construct() {
	}

	/**
	 * 店舗の全ラリーユーザを取得
	 * 利用条件：ad_config.phpを読み込み済みであること。読み込んでいない場合falseを返す
	 * @param type $branch_id
	 * @return
	 */
	public function find_all($db, $branch_id = null) {
		if (!defined(ADMIN_ID)) {
			return false;
		}
		
		//すべての人数取得
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		
		// 支店IDを保持している場合は支店内のユーザが対象になる
		if ($branch_id == null) {
			$where = " u.user_id = r.user_id AND r.admin_id = " . ADMIN_ID;
		} else {
			$where = " u.user_id = r.user_id AND r.admin_id = " . ADMIN_ID . " AND r.branch_id IN(" . $branch_id.")";
		}
		$rally_user_date = all_user_information_select($db, $where);
		$user_list = $this->get_dbdata_array($rally_user_date);
		
		if (isset($isDbOpen)) {
			db_close($db);
		}
		return $user_list;
	}

	/**
	 * AdminのタイプをORG(組織アカウント)かどうかを判定する
	 * @param type $admin_id
	 * @return type
	 */
	public function is_admin_type_org($db, $admin_id) {
		return false;
	}

	/**
	 * AdminのタイプをSTAFFかどうかを判定する
	 * @param type $admin_id
	 * @return type
	 */
	public function is_admin_type_staff($db, $admin_id) {
		$where = "admin_id = '" . $admin_id . "' LIMIT 0,1";
		$data = staff_select($db, $where);
		$staff = mysql_fetch_array($data);
		if ($staff) {
			return true;
		}
		return false;
	}
	
	/**
	* ユーザ情報取得
	*/
	function get_userinfo_by_user_id_and_rally_id($db, $user_id, $rally_id) {
		$where = "a.user_id = b.user_id AND a.rally_id = ".$rally_id." AND b.user_id = '".$user_id."' ";
		$detail_user_date = detail_user_select($db , $where);
		$detail_user = mysql_fetch_array($detail_user_date);
		return $detail_user;
	}
	
	/**
	* ユーザ情報取得(配列)
	*/
	function get_users_by_user_id_and_rally_id($db, $user_id, $rally_id) {
		if(is_array($user_id)) {
			$where = "a.user_id = b.user_id AND a.rally_id = ".$rally_id." AND b.user_id IN(".implode(",", $user_id).") ";
		} else {
			$where = "a.user_id = b.user_id AND a.rally_id = ".$rally_id." AND b.user_id = '".$user_id."' ";
		}
		$detail_user_date = detail_user_select($db , $where);
		while ($row = mysql_fetch_array($detail_user_date)){
			$users[] = $row;
		}
		if (empty($users)) {
			return false;
		}
		return $users;
	}
	
	/**
	* ラリーユーザ情報取得
	*/
	function get_rally_user_by_user_id_and_rally_id($db, $user_id, $rally_id) {
		$where = "user_id = '".$user_id."' AND rally_id = '".$rally_id."'";
		$detail_user_date = rally_user_select($db , $where);
		$detail_user = mysql_fetch_array($detail_user_date);
		return $detail_user;
	}
	
	/**
	 * ラリーユーザ数を取得
	 * @param type $db
	 * @param type $rally_id
	 * @param type $branch_id
	 * @return type
	 */
	function get_user_count($db, $rally_id, $branch_id) {
		$where = "rally_id = '".$rally_id."'";
		if (!empty($branch_id)) {
			$where .= " AND branch_id = '".$branch_id."'";
		}
		$rally_user_date = rally_user_select($db , $where);
		return mysql_num_rows($rally_user_date);
	}

}
