<?php
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'BaseModel.php');
require_once(dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'component'.DIRECTORY_SEPARATOR.'NoticeComponent.php');
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'AdminModel.php');
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'RallyModel.php');

class AlertSettingModel extends BaseModel{

	function __construct() {
	}

	/**
	 * rally_idを渡してアラート情報情報を取得する
	 * @param type $db
	 * @param type $rally_id
	 * @param type $admin_id
	 * @return boolean
	 */
	public function find_by_rally_id($db, $rally_id, $admin_id) {
		if (empty($rally_id) || empty($admin_id)) {
			return false;
		}
		
		$datalist = [];
		$where = "rally_id = '" . $rally_id . "' AND admin_id = '".$admin_id."'";
		$data = alert_setting_select($db , $where, 'no');
		while ($row = mysql_fetch_array($data)){
			$datalist[] = $row;
		}
		if (!empty($datalist)) {
			return $datalist;
		}
		return false;
	}
	
	/**
	 * rally_idとadmin_idの配列渡して対象のアラート情報情報を取得する
	 * @param type $db
	 * @param type $rally_id
	 * @param type $admin_ids
	 * @return boolean
	 */
	public function find_by_rally_id_and_admin_ids($db, $rally_id, $admin_ids) {
		if (empty($admin_ids) || !is_array($admin_ids) || count($admin_ids) == 0 ) {
			return false;
		}
		
		$datalist = [];
		$where = "rally_id = '" . $rally_id . "' AND admin_id IN(". implode(',', $admin_ids).")";
		$data = alert_setting_select($db , $where, 'no');
		while ($row = mysql_fetch_array($data)){
			$datalist[] = $row;
		}
		if (!empty($datalist)) {
			return $datalist;
		}
		return false;
	}
	
	/**
	 * アラート情報情報を取得する
	 * @param type $db
	 * @param type $rally_id
	 * @param type $admin_id
	 * @param type $no
	 * @return boolean
	 */
	public function get_alert_setting_by_rally_id_and_no($db, $rally_id, $admin_id, $no) {
		if (empty($rally_id) || empty($admin_id)|| empty($no) ) {
			return false;
		}
		
		$where = "rally_id = '" . $rally_id ."' AND admin_id = '".$admin_id."' AND no = '".$no."'";
		$data = alert_setting_select($db , $where);
		while ($row = mysql_fetch_array($data)){
			$dataval = $row;
		}
		if (!empty($dataval)) {
			return $dataval;
		}
		return false;
	}
	
	/**
	 * データをインサート
	 * @param type $db
	 * @param type $setdata
	 */
	public function insert($db, $setdata) {
		$set = "rally_id = '".$setdata['rally_id']."',".
			"admin_id = '".$setdata['admin_id']."',".
			"no = '".$setdata['no']."',".
			"account = '".Util::sanitize_sql($setdata['account'])."',".
			"create_date = now(),".
			"modified_date = now()";

		alert_setting_insert($db, $set);
	}
	
	/**
	 * アップデート
	 * @param type $db
	 * @param type $setdata
	 */
	public function update($db, $setdata) {
	var_dump("call:update.".$setdata);
		
		$set = "rally_id = '".$setdata['rally_id']."',".
			"admin_id = '".$setdata['admin_id']."',".
			"no = '".$setdata['no']."',".
			"account = '".Util::sanitize_sql($setdata['account'])."',".
			"modified_date = now()";
		
		$where = "rally_id = '".$setdata['rally_id']."' AND "."no = '".$setdata['no']."'";
		
		alert_setting_update($db, $set, $where);
	}
	
	/**
	 * 保存(既存データがあればUPDATE/なければINSERTする)
	 * @param type $db
	 * @param type $setdata
	 * @return boolean or id
	 */
	public function save($db, $setdata) {
		var_dump("save1");
		
		// データの有無チェック
		$data = $this->get_alert_setting_by_rally_id_and_no($db, $setdata['rally_id'], $setdata['admin_id'], $setdata['no']);
		if (!$data) {
			$this->insert($db, $setdata);
		} else {
		var_dump("update!!");
			$this->update($db, $setdata);
		}
		$data = $this->get_alert_setting_by_rally_id_and_no($db, $setdata['rally_id'], $setdata['admin_id'], $setdata['no']);
		if (empty($data)) {
			return false;
		}
		return $data['id'];
	}
	
	/**
	 * お知らせを使用した通知を実行
	 * @param type $param
	 */
	public function alert_by_notice($admin_id, $user_ids, $title, $context) {
		$noticeComp = new NoticeComponent();
		// push配信
		$notice_info = [
				'admin_id' => $admin_id,
				'title' => $title,
				'context' => nl2br($context),
				'notice_type' => '5',	// 受付
				'user_ids' => $user_ids,
		];
		$noticeComp->notice_to_any($notice_info);

	}
	
	/**
	 * お知らせを使用した通知を実行
	 * ad_config読み込み前提メソッド
	 * @param type $param
	 */
	public function alert_by_mail($admin_id, $mails, $title, $context) {
		$adminModel = new AdminModel();
		$rallyModel = new RallyModel();
		
		$db = db_connect();
		$rally = $rallyModel->find_by_admin_id($db, ADMIN_ID);
		$admin = $adminModel->get_admin_by_admin_id($db, ADMIN_ID);
		db_close($db);
		
		if (empty($rally)) {
			return;
		}
		error_log(print_r($rally, true));
		$file_name = $admin['file_name'];
		$rally_name = $rally['rally_name'];
		
		error_log("rally_name:".$rally['rally_name']);
		
		foreach ($mails as $mail) {
			Util::user_notification_to_mail(0, $title, $context , $mail , $rally_name, $file_name);
		}
	}
	
}
