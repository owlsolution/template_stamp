<?php
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'BaseModel.php');

class UserModel extends BaseModel{

	function __construct() {
	}

	/**
	 * ユーザのデバイス番号からユーザを特定してユーザ情報を返す
	 * @param type $branch_id
	 * @return
	 */
	public function find_by_devide_num($db, $devide_num = null) {
		
		if ($devide_num == null) {
			return false;
		}
		
		//すべての人数取得
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		$user_info = get_one_user_by_divide_num($db, $devide_num);
		if (isset($isDbOpen)) {
			db_close($db);
		}
		if (empty($user_info)) return false;
		return $user_info;
	}
	
	/**
	 * ユーザのデバイス番号からユーザを特定してユーザ情報を返す
	 * @param type $branch_id
	 * @return
	 */
	public function find_by_uer_id($db, $user_id = null) {
		
		if ($user_id == null) {
			return false;
		}
		
		//すべての人数取得
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		$user_info = get_one_user_by_user_id($db, $user_id);
		if (isset($isDbOpen)) {
			db_close($db);
		}
		if (empty($user_info)) return false;
		return $user_info;
	}
	
	
}
