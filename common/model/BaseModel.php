<?php

require_once(dirname(dirname(dirname(dirname(__FILE__)))) . DIRECTORY_SEPARATOR . 'config.php');
$adconfig_file = dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . 'ad_config.php';
if (file_exists($adconfig_file)) {
	require_once($adconfig_file);
}

/**
 * Componentの基底クラス
 */
class BaseModel {

	/**
	 * DBアクセス結果の情報を取得する
	 * @param type $data_rows
	 * @return array
	 */
	public function get_dbdata_array($data_rows) {
		$data_list = array();  // 空配列宣言
		while ($row = mysql_fetch_array($data_rows, MYSQL_ASSOC)) {
			$data_list[] = $row;
		}
		return $data_list;
	}
	
	/**
	 * 第１引数の配列データから特定のkeyの値に合致する最初のデータを取得する
	 * @param type $data_list
	 * @param type $col_name キー要素名
	 * @param type $key キー
	 * @return boolean
	 */
	public function get_row_by_id($data_list, $col_name, $key) {
		foreach ($data_list as $row) {
			if ($row[$col_name] == $key) {
				return $row;
			}
		}
		return false;
	}
	
	/**
	 * メールアドレスとして問題無い形式かチェックする
	 * @param type $mail
	 */
	public function check_val_is_mail($mail) {
		if (preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $mail)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * ユーザIDとして問題無い形式かチェックする
	 * @param type $mail
	 */
	public function check_val_is_user_id($id, $prefix = null) {
		
		if (isset($prefix)) {
			// プレフィックスの整合性チェック
			if(!preg_match('/'.$prefix.'/',$id)){
				//$subjectのなかにbcが含まれていない場合
				// プレフィックスとuser_idがマッチしない
				return false;
			}
		}
		
		// 数値かどうか確認
		$id = str_replace($prefix, '', trim($id));
		if (!is_numeric($id)) {
			return false;
		}
		return intval($id);
	}
	
}
