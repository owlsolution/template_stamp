<?php
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'BaseModel.php');

class InquiryModel extends BaseModel{

	// ステータス
	public static $INQUIRY_STATUS_ACCEPT = 0;	// 受付中
	public static $INQUIRY_STATUS_REPLY = 1;	// 返信済
	public static $INQUIRY_STATUS_CLOSE = 2;	// クローズ
	
	public static $STATUS_TEXTS = [
		'0' => '受付中',
		'1' => '返信済',
		'2' => 'クローズ',
	];
	
	// 歯科ステータス アラートチェック
	public static $READ_STATUS = [
		'0' => '未読',
		'1' => '既読',
	];
	
	// 受付種別
	public static $KIND_TEXTS_SIKA = [
		'0' => 'ー',
		'1' => '新規予約',
		'2' => '変更',
		'3' => '変更・キャンセル',
		'4' => '遅延連絡',
		'5' => 'その他',
	];
	
	function __construct() {
	}

	/**
	 * rally_idを渡してラリー情報を取得する
	 * @param type $branch_id
	 * @return
	 */
	public function find_by_rally_id($db, $rally_id = null) {
		if ($rally_id == null) {
			return false;
		}
		
		$datalist = [];
		$where = "inquiry.rally_id = '" . $rally_id . "'";
		$data = inquiry_select($db , $where);
		while ($row = mysql_fetch_array($data)){
			$datalist[] = $row;
		}
		if (!empty($datalist)) {
			return $datalist;
		}
		return false;
	}
	
	/**
	 * 問合せ情報を取得する
	 * @param type $db
	 * @param type $rally_id
	 * @param type $id
	 * @return boolean
	 */
	public function find_by_rally_id_and_id($db, $rally_id, $id) {
		if (empty($rally_id) || empty($id)) {
			return false;
		}
		
		$datalist = [];
		$where = "inquiry.rally_id = '" . $rally_id . "' AND inquiry.inquiry_id = '".$id."'";
		$data = inquiry_select($db , $where);
		while ($row = mysql_fetch_array($data)){
			$datalist[] = $row;
		}
		if (!empty($datalist[0])) {
			return $datalist[0];
		}
		return false;
	}
	
	/**
	 * ユーザIDを指定して問合せ情報を取得する
	 * @param type $db
	 * @param type $rally_id
	 * @param type $user_id
	 * @return boolean
	 */
	public function find_by_rally_id_and_user_id($db, $rally_id, $user_id, $branch_id=null, $sort=null) {
		if (empty($rally_id) || empty($user_id)) {
			return false;
		}
		
		$datalist = [];
		$where = "inquiry.rally_id = '" . $rally_id . "' AND inquiry.user_id = '".$user_id."'";
		if (!empty($branch_id)) {
			$where .= " AND inquiry.branch_id = '".$branch_id."'";
		}
		$data = inquiry_select($db , $where, $sort);
		while ($row = mysql_fetch_array($data)){
			$datalist[] = $row;
		}
		if (!empty($datalist)) {
			return $datalist;
		}
		return false;
	}
	
	
	/**
	 * 支店idの配列を指定して問合せ情報を取得する
	 * @param type $db
	 * @param type $rally_id
	 * @param type $branch_ids
	 * @return boolean
	 */
	public function find_by_rally_id_and_branch_ids($db, $rally_id, $branch_ids) {
		if (empty($rally_id)) {
			return false;
		}
		if (empty($branch_ids) && !is_array($branch_ids)) {
			return false;
		}
		
		$datalist = [];
		$where = "rally_id = '" . $rally_id . "' AND branch_id IN(".implode(',', $branch_ids).")";
		$data = inquiry_select($db , $where);
		while ($row = mysql_fetch_array($data)){
			$datalist[] = $row;
		}
		if (!empty($datalist)) {
			return $datalist;
		}
		return false;
	}
	
	
	public function insert($db, $setdata) {
		$set = "rally_id = '".$setdata['rally_id']."',".
			"user_id = '".$setdata['user_id']."',".
			"name = '".Util::sanitize_sql($setdata['name'])."',".
			"phone = '".Util::sanitize_sql($setdata['phone'])."',".
			"description = '".Util::sanitize_sql($setdata['description'])."',".
			"branch_id = '".$setdata['branch_id']."',".
			"kind = '".$setdata['kind']."',".
			"modified_date = now()".",".
			"create_date = now()";
		inquiry_insert($db, $set);
		$last_id = mysql_insert_id();
		return $last_id;
	}
	
	/**
	 * 問合せの削除
	 * @param type $db
	 * @param type $id
	 */
	public function delete($db, $id) {
		inquiry_delete($db, $id);
	}

	/**
	 * 状態変更
	 * @param type $db
	 * @param type $inquiry_id
	 * @param type $status
	 */
	public function change_status($db, $inquiry_id, $status) {
		$where = "inquiry_id = '" . $inquiry_id . "'";
		$set = "status = '".$status."'";
		inquiry_update($db , $set, $where);
	}
	
	/**
	 * 既読変更
	 * @param type $db
	 * @param type $inquiry_id
	 * @param type $status
	 */
	public function comp_read($db, $inquiry_id) {
		$read = 1;
		$where = "inquiry_id = '" . $inquiry_id . "'";
		$set = "is_read = '".$read."'";
		inquiry_update($db , $set, $where);
	}
	
}
