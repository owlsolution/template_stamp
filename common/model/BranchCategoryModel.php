<?php
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'BaseModel.php');

/**
 * 支店カテゴリ関連のデータ取得用モデル
 */
class BranchCategoryModel extends BaseModel{

	function __construct() {
	}

	/**
	 * rally_idから支店カテゴリ情報を取得する
	 * @param type $owner_admin_id
	 * @return type
	 */
	public function get_branch_category_by_rally_id($db, $rally_id) {
		$where = "rally_id = '" . $rally_id . "'";
		$order = 'sub_id';
		$data = branch_category_select($db , $where ,$order);
		while ($row = mysql_fetch_array($data)){
			$branch_category_list[] = $row;
		}
		if (!empty($branch_category_list)) {
			return $branch_category_list;
		}
		return false;
	}
	
	/**
	 * IDから支店カテゴリ情報を取得する
	 * @param type $branch_id
	 * @return type
	 */
	public function get_branch_category_by_id($db, $branch_category_id) {
		$where = "id = '" . $branch_category_id . "'";
		$order = 'sub_id';
		$data = branch_category_select($db , $where ,$order);
		$row = mysql_fetch_array($data);
		if (!empty($row)) {
			return $row;
		}
		return false;
	}
}
