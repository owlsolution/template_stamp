<?php
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'BaseModel.php');

/**
 * Admin関連のデータ取得用モデル
 */
class AdminModel extends BaseModel{

	function __construct() {
	}

	
	/**
	 * Admin情報を取得する
	 * @param type $db
	 * @param type $admin_id
	 * @return boolean
	 */
	public function get_admin_by_admin_id($db, $admin_id) {
		$where = "admin_id = '" . $admin_id . "' LIMIT 0,1";
		$data = admin_select_date($db, $where);
		$admin = mysql_fetch_array($data);
		if ($admin) {
			return $admin;
		}
		return false;
	}
	
	/**
	 * Admin情報を取得する
	 * @param type $db
	 * @return boolean
	 */
	public function update_admin($db, $set , $where) {
		$res = management_admin_up($db , $set , $where);
		if ($res) {
			return $res;
		}
		return false;
	}
	
	/**
	 * Adminのタイプを取得する
	 * @param type $admin_id
	 * @return type -1:いずれにも該当しない
	 */
	public function get_admin_type($db, $admin_id) {

		// スタッフかどうか判定
		if ($this->is_admin_type_staff($db, $admin_id)) {
			return STAFF;
		}

		// 組織アカウントかどうか判定
		if ($this->is_admin_type_org($db, $admin_id)) {
			return ORG_MANAGER;
		}

		$child_list = getChildAdminIds($db, $admin_id);
		$owner_admin_id = getOwnerAdminId($db, $admin_id);

		// タイプ判定
		if ((count($child_list) == 0) && empty($owner_admin_id)) {
			// 単独オーナー 支店アカウントも親も持たない
			return OWNER;
		} else if ((count($child_list) != 0) && empty($owner_admin_id)) {
			// 支店を持つオーナー 支店アカウントを持つも親も持たない
			return BRANCHES_OWNER;
		} else if (!empty($owner_admin_id)) {
			// 支店管理アカウント 支店を持たず、親がいる
			return BRANCH_MANAGER;
		}
		return -1;
	}
	
	/**
	 * 支店アカウントのadmin_idを指定して支店IDを取得する
	 * @param type $db
	 * @param type $admin_id
	 */
	public function get_branch_id_by_admin_id($db, $admin_id) {
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		$branch_id = getBranchIdByChildAdminId($db, $admin_id);
		if (isset($isDbOpen)) {
			db_close($db);
		}
		if (empty($branch_id)) {
			return false;
		}
		return $branch_id;
	}
	
	/**
	 * AdminのタイプをORG(組織アカウント)かどうかを判定する
	 * @param type $admin_id
	 * @return type
	 */
	public function is_admin_type_org($db, $admin_id) {
		if($this->get_organization_by_admin_id($db, $admin_id)) {
			return true;
		}
		return false;
	}

	/**
	 * AdminのタイプをSTAFFかどうかを判定する
	 * @param type $admin_id
	 * @return type
	 */
	public function is_admin_type_staff($db, $admin_id) {
		$where = "admin_id = '" . $admin_id . "' LIMIT 0,1";
		$data = staff_select($db, $where);
		$staff = mysql_fetch_array($data);
		if ($staff) {
			return true;
		}
		return false;
	}
	
	/**
	 * AdminIDからSTAFF情報を取得する
	 * @param type $admin_id
	 * @return type
	 */
	public function get_staff_by_admin_id($db, $admin_id) {
		$where = "admin_id = '" . $admin_id . "' LIMIT 0,1";
		$data = staff_select($db, $where);
		$staff = mysql_fetch_array($data);
		if ($staff) {
			return $staff;
		}
		return false;
	}
	
	/**
	 * 店員IDからSTAFF情報を取得する
	 * @param type $staff_id
	 * @return success:$staff, erorr:false
	 */
	public function get_staff_by_id($db, $staff_id) {
		$where = "id = '" . $staff_id . "' LIMIT 0,1";
		$data = staff_select($db, $where);
		$staff = mysql_fetch_array($data);
		if ($staff) {
			return $staff;
		}
		return false;
	}
	
	/**
	 * rally_idからSTAFF情報を取得する
	 * @param type $rally_id
	 * @return type
	 */
	public function get_staff_by_rally_id($db, $rally_id) {
		$where = "rally_id = '" . $rally_id . "' AND status = '1'";
		$data = staff_select($db, $where);
		while ($row = mysql_fetch_array($data)){
			$data_list[] = $row;
		}
		if (!empty($data_list)) {
			return $data_list;
		}
		return false;
	}
	
	/**
	 * branch_idからSTAFFリストを取得する
	 * @param type $branch_ids
	 * @return type
	 */
	public function get_staff_by_branch_id($db, $branch_ids ) {
		$where = "branch_id  IN(" . $branch_ids . ") AND status = '1'";
		$data = staff_select($db, $where);
		while ($staff = mysql_fetch_array($data)){
			$data_list[] = $staff;
		}
		if (!empty($data_list)) {
			return $data_list;
		}
		return false;
	}
	
	/**
	 * organization_idからSTAFFリストを取得する
	 * @param type $organization_id
	 * @return type
	 */
	public function get_staff_by_organization_id($db, $organization_ids ) {
		$where = "organization_id IN(" . $organization_ids . ") AND status = '1'";
		$data = staff_select($db, $where);
		while ($staff = mysql_fetch_array($data)){
			$data_list[] = $staff;
		}
		if (!empty($data_list)) {
			return $data_list;
		}
		return false;
	}
	
	/**
	 * rally_idからSTAFFのadmin_idのリストを取得する
	 * @param type $rally_id
	 * @return type
	 */
	public function get_staff_admin_ids_by_rally_id($db, $rally_id) {
		$where = "rally_id = '" . $rally_id . "' AND status = '1'";
		$data = staff_select($db, $where);
		while ($staff = mysql_fetch_array($data)){
			$admin_ids[] = $staff['admin_id'];
		}
		if (!empty($admin_ids)) {
			return $admin_ids;
		}
		return false;
	}
	
	/**
	 * 支店IDからSTAFFのadmin_idのリストを取得する
	 * @param type $branch_id
	 * @return type
	 */
	public function get_staff_admin_ids_by_branch_id($db, $branch_id) {
		$where = "branch_id IN(" . $branch_id . ") AND status = '1'";
		$data = staff_select($db, $where);
		while ($staff = mysql_fetch_array($data)){
			$admin_ids[] = $staff['admin_id'];
		}
		if (!empty($admin_ids)) {
			return $admin_ids;
		}
		return false;
	}
	
	
	/**
	 * オーナーのadmin_idから組織情報を取得する
	 * @param type $owner_admin_id
	 * @return type
	 */
	public function get_organization_by_owner_admin_id($db, $owner_admin_id) {
		$where = "owner_admin_id = '" . $owner_admin_id . "'";
		$order = 'sub_id';
		$data = organization_select($db , $where ,$order);
		while ($organization = mysql_fetch_array($data)){
			$organization_list[] = $organization;
		}
		if (!empty($organization_list)) {
			return $organization_list;
		}
		return false;
	}
	
	
	/**
	 * IDから支店情報を取得する
	 * @param type $branch_id
	 * @return type
	 */
	public function get_branch_by_id($db, $branch_id) {
		$where = "id = '" . $branch_id . "'";
		$order = 'sub_id';
		$data = branch_select($db , $where ,$order);
		$row = mysql_fetch_array($data);
		if (!empty($row)) {
			return $row;
		}
		return false;
	}
	
	/**
	 * 組織IDから支店情報を取得する
	 * @param type $owner_admin_id
	 * @return type
	 */
	public function get_branch_by_organization_id($db, $organization_id) {
		$where = "organization_id = '" . $organization_id . "'";
		$order = 'sub_id';
		$data = branch_select($db , $where ,$order);
		while ($row = mysql_fetch_array($data)){
			$data_list[] = $row;
		}
		if (!empty($data_list)) {
			return $data_list;
		}
		return false;
	}
	
	/**
	 * OWNERのadmin_idから支店情報のリストを取得する
	 * @param type $owner_admin_id
	 * @return type
	 */
	public function get_branch_by_owner_admin_id($db, $owner_admin_id) {
		$where = "parent_admin_id = '" . $owner_admin_id . "'";
		$order = 'sub_id';
		$data = branch_select($db , $where ,$order);
		while ($row = mysql_fetch_array($data)){
			$data_list[] = $row;
		}
		if (!empty($data_list)) {
			return $data_list;
		}
		return false;
	}
	
	/**
	 * 支店カテゴリIDから支店情報を取得する
	 * @param type $owner_admin_id
	 * @return type
	 */
	public function get_branch_by_branch_category_id($db, $branch_category_id) {
		$where = "branch_category_id = '" . $branch_category_id . "'";
		$order = 'sub_id';
		$data = branch_select($db , $where ,$order);
		while ($row = mysql_fetch_array($data)){
			$data_list[] = $row;
		}
		if (!empty($data_list)) {
			return $data_list;
		}
		return false;
	}
	
	/**
	 * スタッフリストデータから組織IDが合致するスタッフのみ抽出する
	 * @param type $organization_id
	 * @return type
	 */
	public function filter_staff_by_organization_id($staffs, $organization_id) {
		foreach ($staffs as $staff) {
			if ((is_array($organization_id) && (in_array($staff['organization_id'], $organization_id)))) {
				$data_list[] = $staff;
			} else if ((is_numeric($organization_id) && ( $staff['organization_id'] == $organization_id))) {
				$data_list[] = $staff;
			}
		}
		if (!empty($data_list)) {
			return $data_list;
		}
		return false;
	}
	
	/**
	 * スタッフリストデータから支店IDが合致するスタッフのみ抽出する
	 * @param type $branch_id
	 * @return type
	 */
	public function filter_staff_by_branch_id($staffs, $branch_id) {
		foreach ($staffs as $staff) {
			if ((is_array($branch_id) && (in_array($staff['branch_id'], $branch_id)))) {
				$data_list[] = $staff;
			} else if ((is_numeric($branch_id) && ( $staff['branch_id'] == $branch_id))) {
				$data_list[] = $staff;
			}
		}
		if (!empty($data_list)) {
			return $data_list;
		}
		return false;
	}
	
	/**
	 * スタッフリストデータから支店IDが合致するスタッフのみ抽出する
	 * @param type $branch_id
	 * @return type
	 */
	public function filter_staff_by_under_owner($staffs) {
		foreach ($staffs as $staff) {
			// 支店0,組織0はオーナー直下のスタッフ
			if (($staff['branch_id'] == "0") && ($staff['organization_id'] == "0")) {
				$data_list[] = $staff;
			}
		}
		if (!empty($data_list)) {
			return $data_list;
		}
		return false;
	}
	
	/**
	 * 組織アカウントのadmin_idから組織情報を取得する
	 * @param type $admin_id
	 * @return type
	 */
	public function get_organization_by_admin_id($db, $admin_id) {
		$where = "admin_id = '" . $admin_id . "'";
		$order = "sub_id LIMIT 0,1";
		$data = organization_select($db , $where ,$order);
		$row = mysql_fetch_array($data);
		if (!empty($row)) {
			return $row;
		}
		return false;
	}
	
	/**
	 * OrgIDから組織情報を取得する
	 * @param type $admin_id
	 * @return 組織情報 or false
	 */
	public function get_organization_by_id($db, $organization_id) {
		$where = "id = '" . $organization_id . "'";
		$order = "sub_id LIMIT 0,1";
		$data = organization_select($db , $where ,$order);
		$row = mysql_fetch_array($data);
		if (!empty($row)) {
			return $row;
		}
		return false;
	}
	
	/**
	 * 支店のAdminIDから支店情報を取得する
	 * @param type $admin_id
	 * @return type
	 */
	public function get_branch_by_admin_id($db, $admin_id) {
		$where = "child_admin_id = '" . $admin_id . "'";
		$order = "sub_id LIMIT 0,1";
		$data = branch_select($db , $where ,$order);
		$branch = mysql_fetch_array($data);
		if ($branch) {
			return $branch;
		}
		return false;
	}
	
	/**
	 * 組織のAdminIDから配下支店リスト情報を取得する
	 * @param type $admin_id
	 * @return type
	 */
	public function get_branch_by_org_admin_id($db, $admin_id) {
		$org = $this->get_organization_by_admin_id($db, $admin_id);
		if (!$org) {
			return false;
		}
		$branch_list = $this->get_branch_by_organization_id($db, $org['id']);
		
		if ($branch_list) {
			return $branch_list;
		}
		return false;
	}
	
	public function get_rally_id($db, $admin_id) {
		$where = "admin_id = '".$admin_id."'";
		$data = rally_select($db , $where);
		$rally = mysql_fetch_array($data);
		if(!empty($rally['rally_id'])) {
			return $rally['rally_id'];
		}
		return null;
	}
	
	/**
	 * 自身のadmin_idを指定して
	 * その配下に属している全てのadmin_idのリストを取得する
	 * @param type $db
	 * @param type $admin_id
	 */
	public function get_all_admin_id_by_own_admin_id($db, $admin_id, $admin_type) {
		
		if (empty($admin_id)) {
			return false;
		}
		
		$admin_id_list[] = $admin_id;
		if ($admin_type == OWNER) {
			// オーナーアカウント
			// 店員のadmin_idを追加
			$rally_id = $this->get_rally_id($db, $admin_id);
			$staff_admin_id_list = $this->get_staff_admin_ids_by_rally_id($db, $rally_id);
		} else if ($admin_type == BRANCHES_OWNER) {
			// 全支店管理アカウント
			// 全ての組織アカウントのadmin_idを取得
			$org_list = $this->get_organization_by_owner_admin_id($db, $admin_id);
			foreach($org_list as $org){
				$org_admin_id_list[] = $org['admin_id'];
			}
			
			// 全ての支店管理アカウントのadmin_idを取得
			$branch_list = branch_select_by_owner($db, $admin_id);
			foreach($branch_list as $branch){
				$branch_admin_id_list[] = $branch['child_admin_id'];
			}
			
			// 全てのスタッフ(オーナー,組織,支店直属)のadmin_idを取得
			$rally_id = $this->get_rally_id($db, $admin_id);
			$staff_admin_id_list = $this->get_staff_admin_ids_by_rally_id($db, $rally_id);
		} else if ($admin_type == ORG_MANAGER) {
			// 組織アカウント
			//    組織admin_idから組織IDを取得
			$org = $this->get_organization_by_admin_id($db, $admin_id);
			// 組織直下の支店管理アカウントを取得
			error_log("org['id']:".$org['id']);
			$staff_direct_under_org = $this->get_staff_by_organization_id($db, $org['id']);
			$staff_admin_id_list = array_column($staff_direct_under_org, 'admin_id');
			
			error_log("staff_admin_id_list:".print_r($staff_admin_id_list, true));
			
			//    組織IDからbranchテーブルから関連の支店アカウントを取得
			$branch_list = $this->get_branch_by_organization_id($db, $org['id']);
			if ($branch_list) {
				$branch_admin_id_list = array_column($branch_list, 'child_admin_id');
				// 組織アカウント直属のスタッフのadmin_idを取得
				foreach ($branch_list as $branch) {
					// 配下の支店に所属するスタッフのadmin_idを取得
					$staff_list = $this->get_staff_by_branch_id($db, $branch['id']);
					if ($staff_list) {
						$new_staff_admin_id_list = array_column($staff_list, 'admin_id');
						$staff_admin_id_list = (isset($staff_admin_id_list)) ? array_merge($staff_admin_id_list, $new_staff_admin_id_list) : $new_staff_admin_id_list;
					}
				}
			}
		} else if ($admin_type == BRANCH_MANAGER) {
			// 支店アカウント
			// 配下の支店に所属するスタッフのadmin_idを取得
			$branch_id = getBranchIdByChildAdminId($db, $admin_id);
			$staff_admin_id_list = $this->get_staff_admin_ids_by_branch_id($db, $branch_id);
		} else if ($admin_type == STAFF) {
			// STAFF
			// STAFF配下には自身しかいないので、初期設定したadmin_idのみ
		}
		
		// 組織のadmin_idリストを取得している場合、マージする
		if (!empty($org_admin_id_list)) {
			$admin_id_list = array_merge($admin_id_list, $org_admin_id_list);
		}
		// 支店のadmin_idを取得している場合、マージする
		if (!empty($branch_admin_id_list)) {
			$admin_id_list = array_merge($admin_id_list, $branch_admin_id_list);
		}
		// スタッフのadmin_idを取得している場合、マージする
		if (!empty($staff_admin_id_list)) {
			$admin_id_list = array_merge($admin_id_list, $staff_admin_id_list);
		}
		return $admin_id_list;
	}
	
	/**
	 * 自身のadmin_idを指定して
	 * その配下に属しているスタッフを除くのadmin_idのリストを取得する
	 * @param type $db
	 * @param type $admin_id
	 */
	public function get_admin_id_exclude_staff_by_own_admin_id($db, $admin_id, $admin_type) {
		
		if (empty($admin_id)) {
			return false;
		}
		
		$admin_id_list[] = $admin_id;
		if ($admin_type == OWNER) {
			// オーナーアカウント
			// 初期設定したadmin_idのみ
		} else if ($admin_type == BRANCHES_OWNER) {
			// 全支店管理アカウント
			// 全ての組織アカウントのadmin_idを取得
			$org_list = $this->get_organization_by_owner_admin_id($db, $admin_id);
			foreach($org_list as $org){
				$org_admin_id_list[] = $org['admin_id'];
			}
			
			// 全ての支店管理アカウントのadmin_idを取得
			$branch_list = branch_select_by_owner($db, $admin_id);
			foreach($branch_list as $branch){
				$branch_admin_id_list[] = $branch['child_admin_id'];
			}
		} else if ($admin_type == ORG_MANAGER) {
			// 組織アカウント
			// 組織配下の支店管理アカウントを取得
			//    組織admin_idから組織IDを取得
			$org = $this->get_organization_by_admin_id($db, $admin_id);
			//    組織IDからbranchテーブルから関連の支店アカウントを取得
			$branch_list = $this->get_branch_by_organization_id($db, $org['id']);
			if ($branch_list) {
				$branch_admin_id_list = array_column($branch_list, 'child_admin_id');
			}
		} else if ($admin_type == BRANCH_MANAGER) {
			// 支店アカウント
			// 初期設定したadmin_idのみ
		} else if ($admin_type == STAFF) {
			// STAFF
			// STAFF配下には自身しかいないので、初期設定したadmin_idのみ
		}
		
		// 組織のadmin_idリストを取得している場合、マージする
		if (!empty($org_admin_id_list)) {
			$admin_id_list = array_merge($admin_id_list, $org_admin_id_list);
		}
		// 支店のadmin_idを取得している場合、マージする
		if (!empty($branch_admin_id_list)) {
			$admin_id_list = array_merge($admin_id_list, $branch_admin_id_list);
		}
		return $admin_id_list;
	}
	
	/**
	 * ログインしているアカウントのadmin_idを取得する
	 * 管理画面からのみ使用可能
	 * @param type $db
	 * @param type $admin_id
	 * @return boolean
	 */
	public function get_admin_limited_manage($db) {
		
		$admin_id = ADMIN_ID;
		if ($_SESSION["branchFlag"] == OWNER) {
			// 全員
		} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] == 0)){
			// 複数ブランチのオーナー
		} else if (($_SESSION["branchFlag"] == BRANCHES_OWNER) && ($_SESSION["branchId"] != 0)){
			// 複数ブランチのオーナーだけと、１つのブランチを選択
			$admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
		} else if ($_SESSION["branchFlag"] == BRANCH_MANAGER) {
			// 複数ブランチの１つの管理アカウント
			$admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
		} else if (($_SESSION["branchFlag"] == ORG_MANAGER) && ($_SESSION["branchId"] != 0)){
			// 組織アカウントで１つのブランチを選択
			$admin_id = getChildAdminIdByBranchId($db, $_SESSION["branchId"]);
		} else if ($_SESSION["branchFlag"] == ORG_MANAGER) {
			// 組織アカウント
			$org = $this->get_organization_by_id($db, $_SESSION["orgId"]);
			// 組織IDからadmin_idを取得
			$admin_id = $org['admin_id'];
		} else if ($_SESSION["branchFlag"] == STAFF) {
			// スタッフアカウント
			$staff = $this->get_staff_by_id($db, $_SESSION["staffId"]);
			$admin_id = $staff['admin_id'];
		}
		return $admin_id;
	}
	
	/**
	 * 支店情報のリストからターゲットの支店情報を取り出す
	 */
	public function get_target_branch($branch_id, $branch_list) {
		if (empty($branch_list)) {
			return false;
		}
		foreach ($branch_list as $branch) {
			if ($branch['id'] == $branch_id) {
				return $branch;
			}
		}
		return false;
	}
	
}
