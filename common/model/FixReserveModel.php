<?php
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'BaseModel.php');

/**
 * 簡易予約で届けられた予定を加味して、
 * 確定した予約データを保持するデータへのアクセスクラス
 */
class FixReserveModel extends BaseModel{

	function __construct() {
	}


	/**
	 * 予約情報を取得する
	 * @param type $db
	 * @param type $id
	 * @return boolean
	 */
	public function find_by_rally_id_and_id($db, $id) {
		if (empty($id)) {
			return false;
		}
		
		$datalist = [];
		$where = "rsv_fixed_id = '".$id."'";
		$data = fixed_reserve_select($db , $where);
		while ($row = mysql_fetch_array($data)){
			$datalist[] = $row;
		}
		if (!empty($datalist[0])) {
			return $datalist[0];
		}
		return false;
	}
	
	/**
	 * ユーザIDを指定して確定予約情報を取得する
	 * @param type $db
	 * @param type $rally_id
	 * @param type $user_id
	 * @return boolean
	 */
	public function find_by_rally_id_and_user_id($db, $rally_id, $user_id) {
		if (empty($rally_id) || empty($user_id)) {
			return false;
		}
		
		$datalist = [];
		$where = "rally_id = '" . $rally_id . "' AND user_id = '".$user_id."'";
		$data = fixed_reserve_select($db , $where);
		while ($row = mysql_fetch_array($data)){
			$datalist[] = $row;
		}
		if (!empty($datalist)) {
			return $datalist;
		}
		return false;
	}
	
	
	/**
	 * ユーザIDを指定して確定予約情報を取得する
	 * 来院していなくて、今日よりも新しくて、且つ、直近のデータ
	 * @param type $db
	 * @param type $rally_id
	 * @param type $user_id
	 * @return boolean
	 */
	public function find_nearest_by_rally_id_and_user_id($db, $rally_id, $user_id) {
		if (empty($rally_id) || empty($user_id)) {
			return false;
		}
		
		$datalist = [];
		$where = "rally_id = '" . $rally_id . "' AND user_id = '".$user_id."' AND fix_date >= '".date('Y-m-d 00:00:00')."' ORDER BY fix_date LIMIT 1";
		$data = fixed_reserve_select($db , $where);
		while ($row = mysql_fetch_array($data)){
			$datalist[] = $row;
		}
		if (!empty($datalist[0])) {
			return $datalist[0];
		}
		return false;
	}
	
	/**
	 * ユーザIDを指定して本日の予約情報を取得する(重複する場合は古い方)
	 * @param type $db
	 * @param type $rally_id
	 * @param type $user_id
	 * @return boolean
	 */
	public function find_todaydata_by_rally_id_and_user_id($db, $rally_id, $user_id) {
		if (empty($rally_id) || empty($user_id)) {
			return false;
		}
		
		$datalist = [];
		$where = "rally_id = '" . $rally_id . "' AND user_id = '".$user_id."' AND DATE_FORMAT(fix_date , '%Y%m%d' ) = '".date('Ymd')."' ORDER BY fix_date LIMIT 1";
		$data = fixed_reserve_select($db , $where);
		while ($row = mysql_fetch_array($data)){
			$datalist[] = $row;
		}
		if (!empty($datalist[0])) {
			return $datalist[0];
		}
		return false;
	}
	
	/**
	 * 指定日のデータを取得する
	 * 来院していなくて、今日よりも新しくて、且つ、直近のデータ
	 * @param type $db
	 * @param type $rally_id
	 * @param type $datetime
	 * @return boolean
	 */
	public function find_by_rally_id_and_datetime($db, $rally_id, $datetime) {
		if (empty($rally_id) || empty($datetime)) {
			return false;
		}
		
		$datalist = [];
		$where = "rally_id = '" . $rally_id . "' AND fix_date <= '".date('Y-m-d H:i:59', strtotime($datetime))."' AND fix_date >= '".date('Y-m-d H:i:00', strtotime($datetime))."' ORDER BY rsv_fixed_id";
		$data = fixed_reserve_select($db , $where);
		while ($row = mysql_fetch_array($data)){
			$datalist[] = $row;
		}
		if (!empty($datalist)) {
			return $datalist;
		}
		return false;
	}
	
	/**
	 * チェックイン要求が来ているデータ
	 * ただし３ヶ月以内
	 * @param type $db
	 * @param type $rally_id
	 * @param type $datetime
	 * @return boolean
	 */
	public function find_checkin_by_rally_id($db, $rally_id) {
		if (empty($rally_id)) {
			return false;
		}
		
		$datalist = [];
		$where = "rally_id = '" . $rally_id . "' AND checkin_date >= '".date('Y-m-d H:i:59', strtotime(date()."-3 month"))."' AND come_date IS NULL ORDER BY fix_date";
		$data = fixed_reserve_select($db , $where);
		while ($row = mysql_fetch_array($data)){
			$datalist[] = $row;
		}
		if (!empty($datalist)) {
			return $datalist;
		}
		return false;
	}
	
	
	/**
	 * 時間になってもまだチェクインしていない状態のデータ
	 * @param type $db
	 * @param type $rally_id
	 * @param type $datetime
	 * @return boolean
	 */
	public function find_passed_by_rally_id($db, $rally_id) {
		if (empty($rally_id)) {
			return false;
		}
		
		$datalist = [];
		// 1回プッシュしたら対象外
		$where = "rally_id = '" . $rally_id . "' AND fix_date < '".date('Y-m-d H:i:00')."' AND delaypush_count < 1 AND checkin_date IS NULL AND come_date IS NULL ORDER BY rsv_fixed_id";
		$data = fixed_reserve_select($db , $where);
		while ($row = mysql_fetch_array($data)){
			$datalist[] = $row;
		}
		if (!empty($datalist)) {
			return $datalist;
		}
		return false;
	}
	
	/**
	 * データ作成
	 * @param type $db
	 * @param type $setdata
	 * @return type
	 */
	public function insert($db, $setdata) {
		$set = "rally_id = '".$setdata['rally_id']."',".
			"user_id = '".$setdata['user_id']."',".
			"fix_date = '".$setdata['fix_date']."',".
			" update_at = now()".",".
			" create_at = now()";
		
		fixed_reserve_create($db, $set);
		$reserve_id = mysql_insert_id();
		return $reserve_id;
	}
	
	/**
	 * 確定予約の削除
	 * @param type $db
	 * @param type $rsv_fixed_id
	 */
	public function delete($db, $rsv_fixed_id) {
		$where = "rsv_fixed_id = " . $rsv_fixed_id . "";
		fixed_reserve_delete($db, $where);
	}
	
	
	/**
	 * 確定日時を変更
	 * @param type $db
	 * @param type $rsv_fixed_id
	 * @param type $fix_date
	 */
	public function set_fixdate($db, $rsv_fixed_id, $fix_date) {
		$where = "rsv_fixed_id = " . $rsv_fixed_id . "";
		$set = "fix_date = ".$fix_date."";
		return fixed_reserve_update($db , $set, $where);
	}
	
	/**
	 * 確定日時を変更
	 * @param type $db
	 * @param type $rsv_fixed_id
	 * @param type $come_date
	 */
	public function set_come_date($db, $rsv_fixed_id, $come_date) {
		$where = "rsv_fixed_id = " . $rsv_fixed_id . "";
		$set = "come_date = ".$come_date."";
		return fixed_reserve_update($db , $set, $where);
	}
	
	/**
	 * ユーザがチェックインした日時を変更
	 * @param type $db
	 * @param type $rsv_fixed_id
	 * @param type $checkin_date
	 */
	public function set_checkin_date($db, $rsv_fixed_id, $checkin_date) {
		$where = "rsv_fixed_id = " . $rsv_fixed_id . "";
		$set = "checkin_date = ".$checkin_date."";
		return fixed_reserve_update($db , $set, $where);
	}
	
	/**
	 * 遅刻通知カウントを更新する
	 * @param type $db
	 * @param type $rsv_fixed_id
	 * @param type $delaypush_count
	 */
	public function set_delaypush_count($db, $rsv_fixed_id, $delaypush_count) {
		$where = "rsv_fixed_id = " . $rsv_fixed_id . "";
		$set = "delaypush_count = ".$delaypush_count."";
		return fixed_reserve_update($db , $set, $where);
	}
	
}
