<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'BaseComponent.php');
require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'AdminModel.php');
require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'RallyUserModel.php');
require_once(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'model' . DIRECTORY_SEPARATOR . 'AdminUserModel.php');

/**
 * お知らせ配信機能部品
 */
class NoticeComponent extends BaseComponent {
	
	public $schedule_list = array(
		'new_user_push_1' => array('name' => '新規入会後配信', 'comment' => '新規入会時点から#X1#後に配信'),
		'new_user_push_2' => array('name' => '新規入会後配信②', 'comment' => '新規入会日から#X1#日後に配信'),
		'last_stamp_push_1' => array('name' => 'ｽﾀﾝﾌﾟ付与後配信', 'comment' => '最新スタンプ付与日から#X1#日後に配信'),
		'last_stamp_push_2' => array('name' => ' ｽﾀﾝﾌﾟ付与後配信②', 'comment' => '累計#X1#個目のスタンプ付与日から#X2#日後に配信'),
		'birthday_push_1' => array('name' => '誕生日前配信', 'comment' => '誕生日の#X1#日前に配信'),
		'birthday_push_2' => array('name' => '誕生日前配信②', 'comment' => '誕生日月の1日に配信'),
		'weekly_push' => array('name' => '週刊配信', 'comment' => '毎週#X1#に配信'),
		'monthly_push' => array('name' => '月刊配信', 'comment' => '毎月#X1#に配信'),
		'last_month_stamp_push' => array('name' => '前月来店配信', 'comment' => '前月に#X1#回以上スタンプ付与した人に 毎月#X2#日に配信'),
	);
	
	public $weekly = array(
		'0' => '日曜日',
		'1' => '月曜日',
		'2' => '火曜日',
		'3' => '水曜日',
		'4' => '木曜日',
		'5' => '金曜日',
		'6' => '土曜日',
	);
	// 管理アカウントモデル
	private $adminModel = null;
	// 管理アカウントモデル
	private $rallyUserModel = null;
	// 
	private $adminUserModel = null;
	
	private $rally_data = null;
	
	function __construct() {
		$this->adminModel = new AdminModel();
		$this->rallyUserModel = new RallyUserModel();
		$this->adminUserModel = new AdminUserModel();
	}
	
	/**
	 * ラリーIDの取得
	 * @param type $db
	 * @return type
	 */
	public function get_rally_id($db) {
		if ($this->rally_data == null) {
			if (!isset($db)) {
				$db = db_connect();
				$isDbOpen = true;
			}
			$rally_date = rally_select($db, "admin_id = " . ADMIN_ID);
			$this->rally_data = mysql_fetch_array($rally_date);
			if (isset($isDbOpen)) {
				db_close($db);
			}
		}
		return $this->rally_data['rally_id'];
	}
	
	/**
	 * PUSHの送信状況を取得する
	 * @param type $send_admin_id カンマ区切りのadmin_id
	 */
	public function get_push_status($send_admin_id) {
		$post = array(
			"admin_id" => $send_admin_id
		);

		// JSONを文字列に変換
		$post_str = json_encode($post);
		$url = PUSH_SERVER . "?p=check_status";
		$response = $this->postJson($url, $post_str);
		return json_decode($response, true);
	}

	/**
	 * PUSHのキャンセル要求
	 * @param type $send_admin_id カンマ区切りのadmin_id
	 */
	public function cancel_push($notice_id) {
		$post = array(
			"mode" => "notice",
			"notice_id" => $notice_id,
		);
		// JSONを文字列に変換
		$post_str = json_encode($post);
		$url = PUSH_SERVER . "?p=cancel";
		$response = $this->postJson($url, $post_str);
		return json_decode($response, true);
	}
	
	/**
	 * お知らせデータを更新する
	 * @param type $db
	 * @param type $edit_id
	 * @param type $notice_title
	 * @param type $notice_data
	 */
	public function edit_notice($db, $edit_id, $notice_title, $notice_data) {
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		
		$where = "notice_id = '".$edit_id."'";
		$set = "notice_title = '".$notice_title."' , notice_content = '".$notice_data."'";
		notice_db_edit($db , $set ,$where);
		
		if (isset($isDbOpen)) {
			db_close($db);
		}
	}
	
	/**
	 * お知らせスケジュールを更新する
	 * @param type $db
	 * @param type $edit_id
	 * @param type $notice_title
	 * @param type $notice_data
	 */
	public function edit_schedule_notice($db, $edit_schedule_id, $notice_title, $notice_data, $schedule_status) {
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		
		$where = "id = '".$edit_schedule_id."'";
		$set = "title = '".$notice_title."' , content = '".$notice_data."' , status = '".$schedule_status."' ";
		notice_schedule_update($db , $set ,$where);
		
		if (isset($isDbOpen)) {
			db_close($db);
		}
	}
	
	/**
	 * お知らせを削除する
	 * 関連する情報も削除
	 * @param type $notice_id
	 */
	public function delete_notice($db, $notice_id) {
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}

		$where = "notice_id =" . $notice_id;
		notice_dele($db, $where);
		// 履歴から削除
		notice_read_history_delete($db, $where);

		if (isset($isDbOpen)) {
			db_close($db);
		}
		// PUSHサーバへキャンセル要求
		$this->cancel_push($notice_id);
	}

	/**
	 * お知らせスケジュールを削除する
	 * 関連する情報も削除
	 * @param type $dele_schedule_id
	 */
	public function delete_notice_schedule($db, $dele_schedule_id) {
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		$where = "id =" . $dele_schedule_id;
		notice_schedule_delete($db, $where);
		if (isset($isDbOpen)) {
			db_close($db);
		}
	}

	/**
	 * 管理アカウントのスコープ範囲内の全ユーザを取得
	 * @param type $admin_id
	 */
	public function find_all_scope_user($db, $admin_id) {
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		
		$branch_id = $this->get_branch_id_by_related_admin_id($db, $admin_id);
		
		// ログインしているアカウントのスコープ範囲内のすべての顧客人数取得
		$user_list = $this->rallyUserModel->find_all($db, $branch_id);
		
		if (isset($isDbOpen)) {
			db_close($db);
		}
		return $user_list;
	}
	
	/**
	 * Admin_idから支店IDを取得する
	 * branchが存在しない場合、nullを返す
	 * @param type $db
	 * @param type $admin_id
	 * @return type 
	 */
	public function get_branch_id_by_related_admin_id($db, $admin_id) {
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		$branch_id = null;
		$admin_type = $this->adminModel->get_admin_type($db, $admin_id);
		if (in_array($admin_type, array(OWNER, BRANCHES_OWNER))) {
			$branch_id = null;
		} else if ($admin_type == BRANCH_MANAGER) {
			$branch_id = $this->adminModel->get_branch_id_by_admin_id($db, $admin_id);
		} else if ($admin_type == ORG_MANAGER) {
			// 組織IDから対象のブランチidを取得する
			$org = $this->adminModel->get_organization_by_admin_id($db, $admin_id);
			$branch_list = $this->adminModel->get_branch_by_organization_id($db, $org["id"]);
			$branch_id_list = array_column($branch_list, 'id');
			$branch_id = implode(",", $branch_id_list);
		} else if($admin_type == STAFF) {
			// 店員が支店に所属している場合,その支店ID
			$staff = $this->adminModel->get_staff_by_admin_id($db, $admin_id);
			$branch_id = $staff['branch_id'];
			if (empty($branch_id)) {
				// 店員がオーナー直下の場合、対象は全ユーザなのでnull指定
				$branch_id = null;
			}
		}
		if (isset($isDbOpen)) {
			db_close($db);
		}
		return $branch_id;
	}
	
	/**
	 * admin_idが所属する支店、オーナー配下の店員情報一覧を取得する
	 * @param type $db
	 * @param type $rally_id
	 * @param type $send_admin_id
	 * @return type
	 */
	public function get_all_staff_list($db, $rally_id, $send_admin_id) {
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		$staff_branch_id = $this->get_branch_id_by_related_admin_id($db, $send_admin_id);
		$where = "rally_id = " . $rally_id . " AND status = '1' AND branch_id IN(" . (empty($staff_branch_id) ? '0' : $staff_branch_id).")";
		$staff_data = staff_select($db, $where);
		$staff_list = array();
		while ($staff = mysql_fetch_array($staff_data)) {
			$staff_list[] = $staff;
		}
		
		if (isset($isDbOpen)) {
			db_close($db);
		}
		
		return $staff_list;
	}
	
	/**
	 * 支店名の取得
	 * 指定なし、店舗未登録を追加
	 */
	public function select_branch_list($db = null) {
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		//店舗一覧を取得
		$branch_list = branch_select_by_owner($db, ADMIN_ID);
		if (isset($isDbOpen)) {
			db_close($db);
		}

		$branch_select_menu = array();
		$branch_select_menu[] = array("branch_id" => "0", "branch_name" => "指定なし");
		foreach ($branch_list as $branch) {
			$branch_select_menu[] = array("branch_id" => $branch['id'], "branch_name" => $branch['name']);
		}
		//配列の最後に、支店を登録していないユーザを絞り込む為に、配列を追加する。
		$branch_select_menu[] = array("branch_id" => "-1", "branch_name" => "店舗未登録");

		return $branch_select_menu;
	}

	/**
	 * 選択可能な支店名一覧の取得
	 * 指定なし、店舗未登録を追加
	 */
	public function select_branch_list_by_admin_type($db = null, $admintype, $branch_id, $org_id) {
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		// 選択可能な支店一覧を取得
		if ($admintype == OWNER) {
			// 全員
			$branch_list = branch_select_by_owner($db, ADMIN_ID);
		} else if (($admintype == BRANCHES_OWNER) && ($branch_id == 0)){
			$branch_list = branch_select_by_owner($db, ADMIN_ID);
		} else if (($admintype == BRANCHES_OWNER) && ($branch_id != 0)){
			// 選択中の支店しか選択できない
			$branch_list[] = $this->adminModel->get_branch_by_id($db, $branch_id);
		} else if ($admintype == BRANCH_MANAGER) {
			// ログイン中の支店しか選択できない
			$branch_list[] = $this->adminModel->get_branch_by_id($db, $branch_id);
		} else if ($admintype == ORG_MANAGER) {
			// 組織IDから組織に属する支店を取得
			$branch_list = $this->adminModel->get_branch_by_organization_id($db, $org_id);
		} else if ($admintype == STAFF) {
			// スタッフが所属する支店の支店IDを取得する
			// 現状未対応
		}
		
		if (isset($isDbOpen)) {
			db_close($db);
		}

		$branch_select_menu = array();
		$branch_select_menu[] = array("branch_id" => "0", "branch_name" => "指定なし");
		foreach ($branch_list as $branch) {
			$branch_select_menu[] = array("branch_id" => $branch['id'], "branch_name" => $branch['name']);
		}
		//配列の最後に、支店を登録していないユーザを絞り込む為に、配列を追加する。
		$branch_select_menu[] = array("branch_id" => "-1", "branch_name" => "店舗未登録");

		return $branch_select_menu;
	}
	
	/**
	 * 支店IDから支店名を取得する
	 * @param type $db
	 * @param type $branch_id
	 * @return type $branch_name
	 */
	public function get_storename_by_branch_id($db, $branch_id) {
		$sql = "SELECT name from branch WHERE id = " . $branch_id;
		$result = mysql_query($sql, $db);
		$branch = mysql_fetch_array($result);
		$branch_name = $branch['name'];
		return $branch_name;
	}

	/**
	 * Adminのタイプを取得する
	 * @param type $admin_id
	 * @return type -1:いずれにも該当しない
	 */
	public function get_admin_type($db, $admin_id) {
		return $this->adminModel->get_admin_type($db, $admin_id);
	}

	/**
	 * 配信直前処理
	 * セッションに情報をセットする
	 * @param type $notice_id
	 */
	public function pre_notice($notice_id) {

		$db = db_connect();
		$where = "notice_id = '" . $notice_id . "'";
		$data = notice_select($db, $where);
		$notice = mysql_fetch_array($data);
		db_close($db);
		if ($notice) {
			/**
			 *
			 * $notice_title               お知らせタイトル
			 * $notice_date                お知らせ内容
			 * ADMIN_ID                    adminID
			 * $now_day                    受付日時
			 * $delivery_num               配信人数
			 * $delivery_set_no            お知らせ配信設定 (1:一斉配信 , 2:簡単配信 , 3:個別配信)
			 * $format_no                  お知らせ形式 (1:プッシュ配信/メール , 2:プッシュ配信 , 3:メール)
			 * $simple_last_month          先月誕生日の人 (0:チェック無 , 1:チェック)
			 * $simple_this_month          今月誕生日の人 (0:チェック無 , 1:チェック)
			 * $simple_next_month          来月誕生日の人 (0:チェック無 , 1:チェック)
			 * $simple_sex                 性別(0:指定なし , 1:両方 , 2:男性 , 3:女性)
			 * $yesterday_come             昨日来店した人 (0:チェック無 , 1:チェック)
			 * $month_come                 1ヶ月以上来店してないの人
			 * $this_month_come            今月以上来店してないの人
			 * $individual_sex_no          性別 (1:両方 , 2:男性 , 3:女性)
			 * $individual_birthday_start  誕生日月 (始)
			 * $individual_birthday_end    誕生日月 (終)
			 * $individual_region          地域
			 * $first_start                初回登録日 (始)
			 * $first_end                  初回登録日 (終)
			 * $last_start                 最終来店日 (始)
			 * $last_end                   最終来店日 (終)
			 * $total_stamp_num            累計スタンプ数
			 * $total_stamp_terms          累計スタンプ数条件 (1:以上 , 2:以下)
			 * $to_user_id                 個人宛配信時の配信対象
			 * $reserve_datetime           予約日時
			 * $send_admin                 送信者のadmin_id
			 * $branch_id                  個別配信で設定する検索条件で選択した支店のID
			 * $keyword                    個別配信で設定する検索条件　キーワード
			 * $exclusion_user_id          個別配信で設定する 配信対象から除外したuser_id(配信対象一覧でチェックボックスを外したuserのID)
			 *
			 * @var unknown_type
			 */
			$_SESSION['asp']['notice']['data'] = array(
				'notice_id' => $notice['notice_id'],
				'notice_title' => $notice['notice_title'],
				'notice_content' => $notice['notice_content'],
				'now_day' => $notice['acceptance_datetime'],
				'delivery_set' => $notice['delivery_set'],
				'format' => $notice['format'],
				'simple_last_month' => $notice['simple_last_month'],
				'simple_this_month' => $notice['simple_this_month'],
				'simple_next_month' => $notice['simple_next_month'],
				'simple_sex' => $notice['simple_sex'],
				'month_come' => $notice['month_come'],
				'this_month_come' => $notice['this_month_come'],
				'yesterday_come' => $notice['yesterday_come'],
				'individual_sex' => $notice['individual_sex'],
				'individual_birthday_start' => $notice['individual_birthday_start'],
				'individual_birthday_end' => $notice['individual_birthday_end'],
				'individual_region' => $notice['individual_region'],
				'first_start' => $notice['first_start'],
				'first_end' => $notice['first_end'],
				'last_start' => $notice['last_start'],
				'last_end' => $notice['last_end'],
				'total_stamp_num' => $notice['total_stamp_num'],
				'total_stamp_terms' => $notice['total_stamp_terms'],
				'to_user_id' => $notice['to_user_id'],
				'reserve_datetime' => $notice['notice_data'],
				'send_admin' => $notice['admin_id'],
				'branch_id' => $notice['condition_branch_id'],
				'keyword' => $notice['keyword'],
				'exclusion_user_id' => $notice['exclusion_user_id'],
				'sns_fb_check' => $notice['sns_fb_check'],
				'sns_tw_check' => $notice['sns_tw_check']
			);

			// お知らせ配信前のステータスを準備完了へ変更
			$_SESSION['asp']['notice']['transition'] = 'complete';
		}
	}
	
	/**
	 * 配信仮保存直前処理
	 * セッションに情報をセットする
	 * @param type $notice_id
	 */
	public function pre_notice_save($notice_id) {
		$_SESSION['asp']['notice']['transition'] = 'save';
	}
	
	
	/**
	 * スケジュール配信時のコメントを置換する
	 * @param type $schedule_type_comment
	 * @param type $param1
	 * @param type $param2
	 * @return type
	 */
	public function convertScheduleCommentTag($schedule_list, $schedule_type, $param1 = "", $param2 = "") {
		$typeinfo = $schedule_list[$schedule_type];

		// コメント置換対比リストの作成
		$table = array(
			'#X1#' => $param1,
			'#X2#' => $param2
		);
		$search = array_keys($table);
		$replace = array_values($table);
		return str_replace($search, $replace, $typeinfo['comment']);
	}

	
	/**
	 * お知らせ本文からimgタグで指定された画像ファイル名を抽出する
	 * @param type $notice_content
	 */
	public function get_img_file_names($notice_content) {
		// 本文内の画像ファイル名を取得
		$img_file_names = null;
		if ($notice_content) {
			$img_count = preg_match_all("/<img #(.+?)#>/", $notice_content, $match);
			for($i = 0; $i < $img_count; $i++){
				$img_file_names[] = $match[1][$i];
			}
		}
		return $img_file_names;
	}
	
	
	/**
	 * お知らせ本文からimgタグで指定された画像ファイル名を抽出する
	 * @param type $notice_content
	 */
	public function is_feed_sns($notice_content) {
		// 本文内の画像ファイル名を取得
		$site_check = '';
		if (($notice['sns_fb_check'] == '1') && ($notice['sns_tw_check'] == '1')) {
			$site_check = '0';
		} else if (($notice['sns_fb_check'] == '1') && ($notice['sns_tw_check'] == '0')) {
			$site_check = '1';
		} else if (($notice['sns_fb_check'] == '0') && ($notice['sns_tw_check'] == '0')) {
			$site_check = '2';
		}
		return $img_file_names;
	}
	
	/**
	 * お知らせ本文からimgタグで指定された画像ファイル名を抽出する
	 * @param type $notice お知らせ情報
	 */
	public function get_feed_sns_type($notice) {
		// 本文内の画像ファイル名を取得
		$site_check = -1;
		if (($notice['sns_fb_check'] == '1') && ($notice['sns_tw_check'] == '1')) {
			$site_check = '0';
		} else if (($notice['sns_fb_check'] == '1') && ($notice['sns_tw_check'] == '0')) {
			$site_check = '1';
		} else if (($notice['sns_fb_check'] == '0') && ($notice['sns_tw_check'] == '1')) {
			$site_check = '2';
		}
		return $site_check;
	}
	
	
	/**
	 * 送信者のadmin_idから、送信者の親adminと連携するユーザIDを取得する
	 * @param type $db 必須
	 * @param type $admin_id
	 * @return type
	 */
	public function get_user_id_by_admin_id($db, $admin_id) {
		// 親アプリへ仮投稿を通知する
		$admin_type = $this->get_admin_type($db, $admin_id);
		if ($admin_type == STAFF) {
			// スタッフが所属する支店を取得
			$branch_id = $this->get_branch_id_by_related_admin_id($db, $admin_id);
			if (!empty($branch_id)) {
				// 支店に所属している場合、
				$parent_admin_id = getChildAdminIdByBranchId($db , $branch_id);
			} else {
				// 親直に所属
				$parent_admin_id = ADMIN_ID;
			}
			if (!empty($parent_admin_id)) {
				// 親admin_idからアプリ連携しているユーザIDを取得する
				$admin_user = $this->adminUserModel->get_adminuser_by_admin_id($db, $parent_admin_id);
				if (!empty($admin_user['user_id'])) {
					return $admin_user['user_id'];
				}
			}
		}
		return null;
	}
	
	/**
	 * スタッフのAdmin_idから支店IDを取得する
	 * スタッフでないadmin_idを指定した場合、nullを返す
	 * @param type $db
	 * @param type $admin_id
	 * @return type
	 */
	public function get_branch_id_by_staff_admin_id($db, $admin_id) {
		if (!isset($db)) {
			$db = db_connect();
			$isDbOpen = true;
		}
		$branch_id = null;
		$admin_type = $this->adminModel->get_admin_type($db, $admin_id);
		if (in_array($admin_type, array(OWNER, BRANCHES_OWNER))) {
			$branch_id = null;
		} else if ($admin_type == BRANCH_MANAGER) {
			$branch_id = $this->adminModel->get_branch_id_by_admin_id($db, $admin_id);
		} else if($admin_type == STAFF) {
			// 店員が支店に所属している場合,その支店ID
			$staff = $this->adminModel->get_staff_by_admin_id($db, $admin_id);
			$branch_id = $staff['branch_id'];
			if (empty($branch_id)) {
				// 店員がオーナー直下の場合、対象は全ユーザなのでnull指定
				$branch_id = null;
			}
		}
		if (isset($isDbOpen)) {
			db_close($db);
		}
		return $branch_id;
	}
	
	/**
	 * お知らせを複数人にpush配信する処理
	 * @param type $post_info
	 * @return boolean
	 * 
	 * $post_info = [
	 *		'admin_id' => '',
	 *		'title' => '',
	 *		'context' => '',
	 *		'notice_type' => '',
	 *		'user_ids' => ['',''],
	 * ]
	 * 
	 */
	public function notice_to_any($post_info) {
		// jsonで受け取った値を変数に格納
		$admin_id = isset($post_info['admin_id']) ? $post_info['admin_id'] : '0';
		$user_ids = isset($post_info['user_ids']) ? $post_info['user_ids'] : null;
		$title = isset($post_info['title']) ? $post_info['title'] : '';
		$context = isset($post_info['context']) ? $post_info['context'] : '';
		$notice_type = isset($post_info['notice_type']) ? $post_info['notice_type'] : '5';
		
		if (empty($user_ids)) {
			return;
		}
		
		$db = db_connect();
		$rally_id = $this->get_rally_id($db);
		db_close($db);
		
		if (isset($rally_id) && !empty($user_ids)) {
			$db = db_connect();
			$r_user_info = $this->rallyUserModel->get_users_by_user_id_and_rally_id($db, $user_ids, $rally_id);
			db_close($db);
		}
		
		$user_ids = [];
		foreach ($r_user_info as $value) {
			// 当該店舗で存在するuser_idのみ抽出
			$user_ids[] = $value['user_id'];
		}
		
		error_log(print_r($user_ids, true));
		
		if (isset($user_ids) && count($user_ids) > 0) {
			$db = db_connect();
			// 対象ユーザが存在するので、お知らせを作成してお知らせを配信する。
			
			// 支店IDを取得する
			$branch_id = $this->adminModel->get_branch_id_by_admin_id($db, $admin_id);
			
			// 送信時点での送信可能な総ユーザ数を取得する。
			$count_users = $this->rallyUserModel->get_user_count($db, $rally_id, $branch_id);
			
			$deliverly_num = count($user_ids);
			$count_users = empty($count_users) ? '0' : $count_users;
			$delivery_set_no = '7';			// 予約
			$now_day = date('Y-m-d H:i:s');	//配信日時
			
			$set = "rally_id = '".$rally_id."', "
				."notice_content  = '".$context."', "
				."admin_id  = '".$admin_id."', "
				."notice_data  = '".$now_day."', "
				."notice_title  = '".$title."', "
				."delivery_set  = '6',"
				."format  = '1', "
				."simple_last_month  = '',"
				."simple_this_month  = '', "
				."simple_next_month  = '', "
				."simple_sex  = '', "
				."yesterday_come  = '', "
				."month_come  = '', "
				."this_month_come  = '', "
				."individual_sex  = '', "
				."individual_birthday_start  = '', "
				."individual_birthday_end  = '', "
				."individual_region  = '', "
				."first_start  = '', "
				."first_end  = '', "
				."last_start  = '', "
				."last_end  = '', "
				."total_stamp_num  = '', "
				."total_stamp_terms  = '', "
				."delivery_num  = '".$deliverly_num."', "
				."notice_type  = '".$notice_type."', "
				."thumbnail  = '', "
				."total_user_count  = '".$count_users."', "
				."acceptance_datetime = now(), "
				."notice_schedule_id = '0'";
			
			notice_create($db, $set);
			$notice_id = mysql_insert_id();
			
			if (empty($notice_id)) {
				error_log('error insert notice data.');
				return;
			}
			
			// 
			// ユーザリストを渡してpush or メールする共通関数にユーザリストを渡す
			
			$notice_data = array(
				'notice_id' => $notice_id,
				'notice_title' => $title,
				'notice_content' => $context,
				'now_day' => $now_day,
				'delivery_set' => $delivery_set_no,
				'format' => '1',
				'send_admin' => $admin_id ,
				'branch_id' => '' , // 個別配信での支店登録済み/未登録の条件
				'user_id_list' => $user_ids
			);
			
			$p_admin_id = ADMIN_ID;
			db_close($db);
			
			Util::notice_message_immediate($p_admin_id, $notice_data, $user_ids);
		}
		
		return true;
	}
	
}
