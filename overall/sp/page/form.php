<?php
$now_years = date('Y');
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<meta name="viewport"
	content="width=device-width, user-scalable=yes, initial-scale=1, minimum-scale=1 , maximum-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />
<title>スタンプラリー参加ページ</title>
<link rel="stylesheet" href="./../css/style.css" />
</head>
<body>
	<h1>
		<img src="./../sp_images/title1.png" width="290">
	</h1>
	<h2 class="search_title">登録フォーム</h2>
	<center><?php echo $msg;?></center>
	<form action="./sign_up.php" method="post" >
		<ul class="stamp_box">
			<li>
				<p style="margin-bottom: 5px;">名前</p> <input type="text"
				name="user_name" value="<?php echo $profile['user_name'];?>">
			</li>
			<li>
				<p style="margin-bottom: 5px;">パスワード</p> <input type="password"
				name="password" value="">
			</li>
			<li>
				<p style="margin-bottom: 5px;">パスワード(確認)</p> <input type="password"
				name="password_ck" value="">
			</li>
			<li>
				<p style="margin-bottom: 5px;">性別</p> <input value="1" type="radio"
				name="sex" <?php if($profile['sex'] == 1){ echo "checked"; }?>> 男 <input
				type="radio" name="sex" value="2"
				<?php if($profile['sex'] == 2){ echo "checked"; }?>> 女
			</li>
			<li>
				<p style="margin-bottom: 5px;">生年月日</p> <SELECT name="years">
					<OPTION value=""></OPTION>
					<?php
					for($i=$now_years;$i>=1700;$i--){
						?>
					<OPTION value="<?php echo $i;?>"
					<?php if($i == $years){ echo "selected";}?>>
						<?php echo $i;?>
					</OPTION>
					<?php
					}
					?>
			</SELECT>年 <SELECT name="month">
					<OPTION value=""></OPTION>
					<?php
					for($i=1;$i<=12;$i++){
						?>
					<OPTION value="<?php echo $i;?>"
					<?php if($i == $month){ echo "selected";}?>>
						<?php echo $i;?>
					</OPTION>
					<?php
					}
					?>
			</SELECT>月 <SELECT name="day">
					<OPTION value=""></OPTION>
					<?php
					for($i=1;$i<=31;$i++){
						?>
					<OPTION value="<?php echo $i;?>"
					<?php if($i == $day){ echo "selected";}?>>
						<?php echo $i;?>
					</OPTION>
					<?php
					}
					?>
			</SELECT>日
			</li>
			<li>
				<p style="margin-bottom: 5px;">地域</p> <select name="region">
					<option value="">都道府県をお選びください。</option>
					<optgroup label="北海道・東北">
						<option value="北海道"
						<?php if($profile['region'] == "北海道"){ echo "selected";}?>>北海道</option>
						<option value="青森県"
						<?php if($profile['region'] == "青森県"){ echo "selected";}?>>青森県</option>
						<option value="秋田県"
						<?php if($profile['region'] == "秋田県"){ echo "selected";}?>>秋田県</option>
						<option value="岩手県"
						<?php if($profile['region'] == "岩手県"){ echo "selected";}?>>岩手県</option>
						<option value="山形県"
						<?php if($profile['region'] == "山形県"){ echo "selected";}?>>山形県</option>
						<option value="宮城県"
						<?php if($profile['region'] == "宮城県"){ echo "selected";}?>>宮城県</option>
						<option value="福島県"
						<?php if($profile['region'] == "福島県"){ echo "selected";}?>>福島県</option>
					</optgroup>
					<optgroup label="甲信越・北陸">
						<option value="山梨県"
						<?php if($profile['region'] == "山梨県"){ echo "selected";}?>>山梨県</option>
						<option value="長野県"
						<?php if($profile['region'] == "長野県"){ echo "selected";}?>>長野県</option>
						<option value="新潟県"
						<?php if($profile['region'] == "新潟県"){ echo "selected";}?>>新潟県</option>
						<option value="富山県"
						<?php if($profile['region'] == "富山県"){ echo "selected";}?>>富山県</option>
						<option value="石川県"
						<?php if($profile['region'] == "石川県"){ echo "selected";}?>>石川県</option>
						<option value="福井県"
						<?php if($profile['region'] == "福井県"){ echo "selected";}?>>福井県</option>
					</optgroup>
					<optgroup label="関東">
						<option value="茨城県"
						<?php if($profile['region'] == "茨城県"){ echo "selected";}?>>茨城県</option>
						<option value="栃木県"
						<?php if($profile['region'] == "栃木県"){ echo "selected";}?>>栃木県</option>
						<option value="群馬県"
						<?php if($profile['region'] == "群馬県"){ echo "selected";}?>>群馬県</option>
						<option value="埼玉県"
						<?php if($profile['region'] == "埼玉県"){ echo "selected";}?>>埼玉県</option>
						<option value="千葉県"
						<?php if($profile['region'] == "千葉県"){ echo "selected";}?>>千葉県</option>
						<option value="東京都"
						<?php if($profile['region'] == "東京都"){ echo "selected";}?>>東京都</option>
						<option value="神奈川県">神奈川県</option>
					</optgroup>
					<optgroup label="東海">
						<option value="愛知県"
						<?php if($profile['region'] == "愛知県"){ echo "selected";}?>>愛知県</option>
						<option value="静岡県"
						<?php if($profile['region'] == "静岡県"){ echo "selected";}?>>静岡県</option>
						<option value="岐阜県"
						<?php if($profile['region'] == "岐阜県"){ echo "selected";}?>>岐阜県</option>
						<option value="三重県"
						<?php if($profile['region'] == "三重県"){ echo "selected";}?>>三重県</option>
					</optgroup>
					<optgroup label="関西">
						<option value="大阪府"
						<?php if($profile['region'] == "大阪府"){ echo "selected";}?>>大阪府</option>
						<option value="兵庫県"
						<?php if($profile['region'] == "兵庫県"){ echo "selected";}?>>兵庫県</option>
						<option value="京都府"
						<?php if($profile['region'] == "京都府"){ echo "selected";}?>>京都府</option>
						<option value="滋賀県"
						<?php if($profile['region'] == "滋賀県"){ echo "selected";}?>>滋賀県</option>
						<option value="奈良県"
						<?php if($profile['region'] == "奈良県"){ echo "selected";}?>>奈良県</option>
						<option value="和歌山県"
						<?php if($profile['region'] == "和歌山県"){ echo "selected";}?>>和歌山県</option>
					</optgroup>
					<optgroup label="中国">
						<option value="岡山県"
						<?php if($profile['region'] == "岡山県"){ echo "selected";}?>>岡山県</option>
						<option value="広島県"
						<?php if($profile['region'] == "広島県"){ echo "selected";}?>>広島県</option>
						<option value="鳥取県"
						<?php if($profile['region'] == "鳥取県"){ echo "selected";}?>>鳥取県</option>
						<option value="島根県"
						<?php if($profile['region'] == "島根県"){ echo "selected";}?>>島根県</option>
						<option value="山口県"
						<?php if($profile['region'] == "山口県"){ echo "selected";}?>>山口県</option>
					</optgroup>
					<optgroup label="四国">
						<option value="徳島県"
						<?php if($profile['region'] == "徳島県"){ echo "selected";}?>>徳島県</option>
						<option value="香川県"
						<?php if($profile['region'] == "香川県"){ echo "selected";}?>>香川県</option>
						<option value="愛媛県"
						<?php if($profile['region'] == "愛媛県"){ echo "selected";}?>>愛媛県</option>
						<option value="高知県"
						<?php if($profile['region'] == "高知県"){ echo "selected";}?>>高知県</option>
					</optgroup>
					<optgroup label="九州・沖縄">
						<option value="福岡県"
						<?php if($profile['region'] == "福岡県"){ echo "selected";}?>>福岡県</option>
						<option value="佐賀県"
						<?php if($profile['region'] == "佐賀県"){ echo "selected";}?>>佐賀県</option>
						<option value="長崎県"
						<?php if($profile['region'] == "長崎県"){ echo "selected";}?>>長崎県</option>
						<option value="熊本県"
						<?php if($profile['region'] == "熊本県"){ echo "selected";}?>>熊本県</option>
						<option value="大分県"
						<?php if($profile['region'] == "大分県"){ echo "selected";}?>>大分県</option>
						<option value="宮崎県"
						<?php if($profile['region'] == "宮崎県"){ echo "selected";}?>>宮崎県</option>
						<option value="鹿児島県"
						<?php if($profile['region'] == "鹿児島県"){ echo "selected";}?>>鹿児島県</option>
						<option value="沖縄県"
						<?php if($profile['region'] == "沖縄県"){ echo "selected";}?>>沖縄県</option>
					</optgroup>
			</select>
			</li>
			<br>
			<br>
			<input type="hidden" name="pass" value="<?php echo $pass;?>">
			<div align="center">
				<input type="submit" name="submit" value="送信する">
			</div>
			<br>
			<br>
			<br>
		</ul>
	</form>
</body>
</html>
