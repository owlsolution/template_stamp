<?php
$now_years = date('Y');
function doctype($carrier) {

	print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");

	if($carrier == "DoCoMo") {

		print ("<!DOCTYPE html PUBLIC \"-//i-mode group (ja)//DTD XHTML i-XHTML(Locale/Ver.=ja/2.3)1.0//EN\" \"i-xhtml_4ja_10.dtd\">\n");

	}
	elseif($carrier == "SoftBank") {

		print ("<!DOCTYPE html PUBLIC \"-//J-PHONE//DTD XHTML Basic 1.0 Plus//EN\" \"xhtml-basic10-plus.dtd\">\n");

	}
	elseif($carrier == "EZweb") {

		print ("<!DOCTYPE html PUBLIC \"-//OPENWAVE//DTD XHTML 1.0//EN\" \"http://www.openwave.com/DTD/xhtml-basic.dtd\">\n");

	}
	else {

		print
		("<!DOCTYPE html PUBLIC \"-//WAPFORUM//DTD XHTML Mobile 1.0//EN\" \"http://www.wapforum.org/DTD/xhtml-mobile10.dtd\">\n");

	} // if($carrier == "DoCoMo") {

}
$agent = $_SERVER['HTTP_USER_AGENT'];
if(ereg("^DoCoMo", $agent)){
	$carrier = "DoCoMo";
}else if(ereg("^J-PHONE|^Vodafone|^SoftBank", $agent)){
	$carrier = "SoftBank";
}else if(ereg("^UP.Browser|^KDDI", $agent)){
	$carrier = "EZweb";
}
doctype($carrier);

// ラリー情報取得
$db = db_connect();
$where = " admin_id='".ADMIN_ID."'";
$tmp = rally_select($db, $where);
$rally = mysql_fetch_array($tmp);
db_close( $db );
$rally_id .= $rally['rally_id'];

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, user-scalable=yes, initial-scale=1, minimum-scale=1 , maximum-scale=1">
<title>プロフィール設定</title>
</head>
<body style="width: 240px;">
	<h1>
		<img src="./../../fp_images/s_taitle4.jpg">
	</h1>
	<img src="./../../fp_images/back.gif" height="15px" width="100%">
	<center>
	<h2><font size="1" color=""><?php echo $h2;?></font></h2>
	</center>
	<hr>
	<img src="./../../fp_images/back.gif" height="10px" width="100%">
	<center><?php echo $msg;?></center>
	<form action="<?php echo $action;?>" method="post" enctype="multipart/form-data;charset=Shift_JIS">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td bgcolor="#CCCCCC"><font size="1">・名前</font></td>
			</tr>
			<tr>
				<td><img src="./../../fp_images/back.gif" height="5px" width="100%"> <input
					type="text" name="user_name" value="<?php echo $profile['user_name'];?>"> <img
					src="./../../fp_images/back.gif" height="15px" width="100%">
				</td>
			</tr>
			<tr>
				<td bgcolor="#CCCCCC"><font size="1"> ・性別 </font></td>
			</tr>
			<tr>
				<td><img src="./../../fp_images/back.gif" height="5px" width="100%"> <input
					value="1" type="radio" name="sex"
					<?php if($profile['sex'] == 1){ echo "checked"; }?>> 男 <input
					type="radio" name="sex" value="2"
					<?php if($profile['sex'] == 2){ echo "checked"; }?>> 女 <img
					src="./../../fp_images/back.gif" height="15px" width="100%">
				</td>
			</tr>

			<tr>
				<td bgcolor="#CCCCCC"><font size="1"> ・生年月日 </font></td>
			</tr>
			<tr>
				<td><img src="./../../fp_images/back.gif" height="5px" width="100%"> <SELECT
					name="years">
						<OPTION value=""></OPTION>
						<?php
						for($i=$now_years;$i>=1700;$i--){
							?>
						<OPTION value="<?php echo $i;?>"
						<?php if($i == $years){ echo "selected";}?>>
							<?php echo $i;?>
						</OPTION>
						<?php
						}
						?>
				</SELECT> 年 <SELECT name="month">
						<OPTION value=""></OPTION>
						<?php
						for($i=1;$i<=12;$i++){
							?>
						<OPTION value="<?php echo $i;?>"
						<?php if($i == $month){ echo "selected";}?>>
							<?php echo $i;?>
						</OPTION>
						<?php
						}
						?>
				</SELECT> 月 <SELECT name="day">
						<OPTION value=""></OPTION>
						<?php
						for($i=1;$i<=31;$i++){
							?>
						<OPTION value="<?php echo $i;?>"
						<?php if($i == $day){ echo "selected";}?>>
							<?php echo $i;?>
						</OPTION>
						<?php
						}
						?>
				</SELECT> 日 <img src="./../../fp_images/back.gif" height="15px"
					width="100%">
				</td>
			</tr>

			<tr>
				<td bgcolor="#CCCCCC"><font size="1"> ・地域 </font></td>
			</tr>
			<tr>
				<td><img src="./../../fp_images/back.gif" height="5px" width="100%"> <select
					name="region">
						<option value="">地域をお選びください。</option>
						<?php
							$region_group_list = json_decode(REGION_LIST,true);
							foreach ($region_group_list as $group) {
								// １つのグループを取り出す
								if(!empty($group['gname'])) {
									echo '<optgroup label="'.$group['gname'].'">';
								}
								
								// 地域アイテムのループ
								$region_list = explode(",", $group['regions']);
								foreach ( $region_list as $region_item) {
									$elem = ($profile['region'] == $region_item) ? "selected" : "";
									echo '<option value="'.$region_item.'" '.$elem.'>'.$region_item.'</option>';
								}
								
								if(!empty($group['gname'])) {
									echo '</optgroup>';
								}
							}
						?>
				</select> <img src="./../../fp_images/back.gif" height="15px"
					width="100%">
				</td>
                        <?php if (!empty($branch_list)) { ?>
			<tr>
				<td bgcolor="#CCCCCC"><font size="1"> ・店舗 </font></td>
			</tr>
			<tr>
				<td><img src="./../../fp_images/back.gif" height="5px" width="100%"> <select name="branch">
						<?php
							foreach ($branch_list as $branch) {
						?>
								<option value=<?php echo $branch['id'];?> 
                                                                    <?php if($rally_user['branch_id'] == $branch['id']){ echo "selected";}?> ><?php echo $branch_name = $branch['name'] ?></option>
						<?php
						}
						?>
						</select> <img src="./../../fp_images/back.gif" height="15px"
					width="100%">
				</td>
			</tr>
        		<?php
                	}
                        ?>
		</table>
		<input type="hidden" name="pass" value="<?php echo $pass;?>">
		<input type="submit" name="submit" value="送信する">
	</form>
	<img src="./../../fp_images/back.gif" height="15px" width="100%">
	<br>
	<font size="1" color="">メニュー</font><br>
	<font size="1" color="">┗<a href="./../fp/?guid=ON&rally_id=<?php echo $rally_id;?>"> <font size="1">スタンプカード</font> </a> </font> <br>
	<br>
</body>
</html>


