<?php
function doctype($carrier) {

	print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");

	if($carrier == "DoCoMo") {

		print ("<!DOCTYPE html PUBLIC \"-//i-mode group (ja)//DTD XHTML i-XHTML(Locale/Ver.=ja/2.3)1.0//EN\" \"i-xhtml_4ja_10.dtd\">\n");

	}
	elseif($carrier == "SoftBank") {

		print ("<!DOCTYPE html PUBLIC \"-//J-PHONE//DTD XHTML Basic 1.0 Plus//EN\" \"xhtml-basic10-plus.dtd\">\n");

	}
	elseif($carrier == "EZweb") {

		print ("<!DOCTYPE html PUBLIC \"-//OPENWAVE//DTD XHTML 1.0//EN\" \"http://www.openwave.com/DTD/xhtml-basic.dtd\">\n");

	}
	else {

		print
		("<!DOCTYPE html PUBLIC \"-//WAPFORUM//DTD XHTML Mobile 1.0//EN\" \"http://www.wapforum.org/DTD/xhtml-mobile10.dtd\">\n");

	} // if($carrier == "DoCoMo") {

}
$agent = $_SERVER['HTTP_USER_AGENT'];
if(ereg("^DoCoMo", $agent)){
	$carrier = "DoCoMo";
}else if(ereg("^J-PHONE|^Vodafone|^SoftBank", $agent)){
	$carrier = "SoftBank";
}else if(ereg("^UP.Browser|^KDDI", $agent)){
	$carrier = "EZweb";
}
doctype($carrier);
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, user-scalable=yes, initial-scale=1, minimum-scale=1 , maximum-scale=1">
<title>IDとパスワード発行</title>
</head>
<body style="width: 240px;">
	<h1>
		<img src="./../../fp_images/s_taitle4.jpg">
	</h1>
	<img src="./../../fp_images/back.gif" height="15px" width="100%">
	<center>
		<h2><font size="1" color="">機種変更のIDとパスワード入力</font></h2>
	</center>
	<hr>
	<img src="./../../fp_images/back.gif" height="10px" width="100%">
	<center>
	<p><font color="red" size="1">発行されたIDとパスワードを入力してください。</font></p>
	<p><font color="red" size="1">IDとパスワードの有効期限は１ヶ月間となっております。</font></p>
	<img src="./../../fp_images/back.gif" height="10px" width="100%">
	<hr>

	<img src="./../../fp_images/back.gif" height="10px" width="100%">
	<?php echo $msg;?>
	<form action="./setting.php?guid=ON&p=input" method="post" enctype="multipart/form-data">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td bgcolor="#CCCCCC"><font size="1">・ID</font></td>
			</tr>
			<tr>
				<td><img src="./../../fp_images/back.gif" height="5px" width="100%"> <input
					type="text" name="id" value="<?php echo $id;?>"> <img
					src="./../../fp_images/back.gif" height="15px" width="100%">
				</td>
			</tr>
			<tr>
				<td bgcolor="#CCCCCC"><font size="1"> ・パスワード </font></td>
			</tr>
			<tr>
				<td><img src="./../../fp_images/back.gif" height="5px" width="100%"> <input
					type="text" name="pass" value="<?php echo $pass;?>"> <img
					src="./../../fp_images/back.gif" height="15px" width="100%">
				</td>
			</tr>
			<tr>
				<td>
					<img src="./../../fp_images/back.gif" height="5px" width="100%">
					<input type="submit" name="submit" value="送信する">
					<img src="./../../fp_images/back.gif" height="5px" width="100%">
				</td>
			</tr>
		</table>
	</form>
	</center>
	<img src="./../../fp_images/back.gif" height="10px" width="100%">
	<br>
	<br>
</body>
</html>