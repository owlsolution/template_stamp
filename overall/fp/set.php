<?php
$p = $_GET['p'];  //ページの内容
function mobileId() {
	if (isset($_SERVER['HTTP_X_DCMGUID'])) {  //ドコモ
		$mobile_id = $_SERVER['HTTP_X_DCMGUID'];
	} else if (isset($_SERVER['HTTP_X_UP_SUBNO'])) {  //au
		$mobile_id = $_SERVER['HTTP_X_UP_SUBNO'];
	} else if (isset($_SERVER['HTTP_X_JPHONE_UID'])) { //ソフトバンク
		$mobile_id = $_SERVER['HTTP_X_JPHONE_UID'];
	}
	return $mobile_id;
}
//$ident_id = "NQQ1INi";
$ident_id = mobileId();

// ラリー情報取得
$db = db_connect();
$where = " admin_id='".ADMIN_ID."'";
$tmp = rally_select($db, $where);
$rally = mysql_fetch_array($tmp);
db_close( $db );

$ident_id .= $rally['rally_id'];    // 個体識別 = 個体識別ID + ラリーID

//echo $ident_id;
if($p== ""){  //プロフィールの変更
	require './fp/page/setting_form.php';
} else if($p== "profile"){
	if(isset($_POST['submit'])){
		if($_POST['user_name'] && $_POST['sex'] && $_POST['years'] && $_POST['month'] && $_POST['day'] && $_POST['region'] != ""){
			$db = db_connect();
			$where = "divide_num = '".$ident_id."'";
			//絵文字が入っている場合は絵文字を削除
			$user_name = Util::removeEmoji($_POST['user_name']);
			$set = "user_name = '".$user_name."' , sex = '".$_POST['sex']."' , birth_date = '".$_POST['years']."-".$_POST['month']."-".$_POST['day']."' , region = '".$_POST['region']."' , password = '".$_POST['password']."'";
			user_up($db , $set , $where);
                        
                        // 支店機能がある場合
                        if(!empty($_POST['branch'])){
                            $where = "user.divide_num = '".$ident_id."' AND rally_user.rally_id =".$_GET['rally_id'];
                            $set = " rally_user.branch_id = '".$_POST['branch']."'";
                            rally_user_up_by_userinfo($db , $set , $where);
                        }
                        
			db_close( $db );
			require './fp/page/end.php';
		} else {
			$profile['user_name'] = $_POST['user_name'];
			$profile['sex'] = $_POST['sex'];
			$years = $_POST['years'];
			$month = $_POST['month'];
			$day = $_POST['day'];
			$profile['region'] = $_POST['region'];
			$pass = $_POST['pass'];
			$msg = "<font color='red' size='1px'>入力ミスがあります</font>";
			require './fp/page/form.php';
		}
	} else {
                
                // ユーザID取得
                $db = db_connect();
                $where = "divide_num = '".$ident_id."'";
                $user_date = get_identification_select($db , $where);
                $user = mysql_fetch_array($user_date);
                $user_id = $user['user_id'];
                //echo $user_id."<br>";
                db_close( $db );

		$action = "./setting.php?guid=ON&p=profile&rally_id=".$rally['rally_id'];
		$db = db_connect();
		$where = "divide_num = '".$ident_id."'";
		$profile_date = all_user_select($db , $where);
		$profile = mysql_fetch_array($profile_date);
		//print_r($profile);
		db_close( $db );
		$birth_date = $profile['birth_date'];
		$birth = split("-",$birth_date);
		$years = $birth[0];
		$month = $birth[1];
		$day = $birth[2];
		$h2 = "プロフィール変更";//h2文言
                                
                // rally_user情報を取得
                $rally_user = user_information_acquisition($rally['rally_id'] , $user_id);
                
                // 支店情報を取得
                $db = db_connect();
                $branch_list = branch_select_by_owner($db, ADMIN_ID);
                db_close($db);
                
		require './fp/page/form.php';
	}
} else if($p == "issue"){  //IDとパスワードの発行
	function getRandomString($nLengthRequired = 9){
		$sCharList = "0123456789";
		mt_srand();
		$sRes = "";
		for($i = 0; $i < $nLengthRequired; $i++)
			$sRes .= $sCharList{mt_rand(0, strlen($sCharList) - 1)};
		return $sRes;
	}
	$db = db_connect();
	$where = "divide_num = '".$ident_id."'";
	$user_date = get_identification_select($db , $where);
	$user = mysql_fetch_array($user_date);
	$user_id = $user['user_id'];
	//echo $user_id."<br>";
	db_close( $db );
	$db = db_connect();
	$day_date = date('Y-m-d',strtotime("-1 month"));
	//echo $day_date;
	$where = "user_id = ".$user_id." AND add_date >  '".$day_date."'";
	$model_change_date = model_change_select_date($db , $where);
	$model_change = mysql_fetch_array($model_change_date);
	db_close( $db );
	if($model_change != "") {
		$ID = $model_change['inp_id'];
		$PASS = $model_change['inp_pass'];
	} else {
		$db = db_connect();
		//$user_id
//		$ID = mt_rand();
		$ID = USER_ID.sprintf("%05d", $user_id);
		$PASS = getRandomString();
		$now_date = date('Y-m-d H:i:s');
		$into = $user_id." , '".$ID."' , '".$PASS."' , '".$now_date."'";
		model_change_insert($db , $into);
		db_close( $db );
	}
	require './fp/page/issue.php';
} else if($p == "input"){
	if(isset($_POST['submit'])){
		if($_POST['id'] && $_POST['pass'] != ""){
			//ユーザーIDの取得
			$db = db_connect();
			$where = "divide_num = '".$ident_id."'";
			$user_date = get_identification_select($db , $where);
			$user = mysql_fetch_array($user_date);
			db_close( $db );
			$user_id = $user['user_id'];
			//ユーザーIDの取得　--end--
			$id = $_POST['id'];
			$pass = $_POST['pass'];
			//モバイル変更テーブル情報の取得
			$db = db_connect();
                        $day_date = date('Y-m-d',strtotime("-1 month"));
			$where = "inp_id = '".$_POST['id']."' AND inp_pass = '".$_POST['pass']."' AND user_id != '".$user_id."' AND add_date >'".$day_date."'";
			$model_change_date = model_change_select_date($db , $where);
			$model_change = mysql_fetch_array($model_change_date);
			//モバイル変更テーブル情報の取得  --end--
			db_close( $db );
			if($model_change != ""){  //ヒットしたら
				//ユーザーの削除
				$db = db_connect();
				$where = "user_id = ".$user_id;
				delete_user($db , $where);
				db_close( $db );
				//ユーザーの削除 --end--
				//ラリーユーザーの削除
				$db = db_connect();
				rally_user_delete($db , $where);
				db_close( $db );
				
				// クラシフィケーションを更新
				$db = db_connect();
				$where = "user_id = ".$model_change['user_id'];
				$set = "classification = '2'";
				rally_user_update($db, $set, $where);
				db_close( $db );
				
				//ラリーユーザーの削除  --end--
				//ユーザー情報編集
				$db = db_connect();
				$where = "user_id = ".$model_change['user_id'];
				$set = "divide_num = '".$ident_id."'";
				user_up($db , $set , $where);
				db_close( $db );
				
				//過去のプシュテーブルを削除
				$where = "user_id = ".$model_change['user_id'];
				$db = db_connect();
				delete_android_push($db , $where);
				delete_iphone_push($db , $where);
				db_close( $db );
				
				//ユーザー情報編集  --end--
				require './fp/page/end.php';
			} else {  //ヒットしなかったら
				$msg = "<font size='1' color='red'>入力ミスがあります。</font>";
				require './fp/page/input.php';
			}

		} else {
			$msg = "<font size='1' color='red'>入力ミスがあります。</font>";
			require './fp/page/input.php';
		}
	} else {
		$msg = "";
		require './fp/page/input.php';
	}
} else if($p == "firstinput"){
        // まだユーザ作成される前の状態で、機種変更になる場合
	if(isset($_POST['submit'])){
		if($_POST['id'] && $_POST['pass'] != ""){
			$id = $_POST['id'];
			$pass = $_POST['pass'];
			//モバイル変更テーブル情報の取得
			$db = db_connect();
                        // 発行した機種変更パスの有効期限が１っ月
                        $day_date = date('Y-m-d',strtotime("-1 month"));
			$where = "inp_id = '".$_POST['id']."' AND inp_pass = '".$_POST['pass']."' AND add_date >'".$day_date."'";
			$model_change_date = model_change_select_date($db , $where);
			$model_change = mysql_fetch_array($model_change_date);
			//モバイル変更テーブル情報の取得  --end--
			db_close( $db );
			if($model_change != ""){  //ヒットしたら
				//ユーザー情報編集
				$db = db_connect();
				$where = "user_id = ".$model_change['user_id'];
				$set = "divide_num = '".$ident_id."'";
				user_up($db , $set , $where);
				db_close( $db );
				
				// クラシフィケーションを更新
				$db = db_connect();
				$where = "user_id = ".$model_change['user_id'];
				$set = "classification = '2'";
				rally_user_update($db, $set, $where);
				db_close( $db );
				
				//過去のプシュテーブルを削除
				$where = "user_id = ".$model_change['user_id'];
				$db = db_connect();
				delete_android_push($db , $where);
				delete_iphone_push($db , $where);
				db_close( $db );
				
				//ユーザー情報編集  --end--
				require './fp/page/end.php';
			} else {  //ヒットしなかったら
				$msg = "<font size='1' color='red'>入力ミスがあります。</font>";
				require './fp/page/firstinput.php';
			}

		} else {
			$msg = "<font size='1' color='red'>入力ミスがあります。</font>";
			require './fp/page/firstinput.php';
		}
	} else {
		$msg = "";
		require './fp/page/firstinput.php';
	}

}